package com.ck.jumper.controller;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.jumper.feign.JumperFeignClient;
import com.ck.tools.com.TResult;
import com.ck.jumper.utils.QueryDto;
import com.ck.jumper.utils.QueryUtil;
import com.ck.spring.tools.spring.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


/**
 * 依赖与 QueryUtil 工具构建的 公共跨服务查询接口
 *
 * @author Cyk
 * @version 1.0
 * @since 2023/3/15 16:53
 **/
@Slf4j
@RequestMapping
public class JumperQueryController implements JumperFeignClient {

    /**
     * 查询分页数据
     *
     * @param mapperBeanName Mapper 在容器中的BeanName
     * @param dto            查询条件
     * @param <T>            返回值类型
     * @return 分页数据
     */
    @ResponseBody
    @Override
    public <T> TResult<Page<T>> queryPage(@PathVariable(value = "mapperBeanName") String mapperBeanName, @RequestBody(required = false) QueryDto<?> dto) {

        BaseMapper<T> mapper = getBaseMapper(mapperBeanName);

        if (mapper != null) {

            Page<T> page = QueryUtil.queryPage(mapper, dto);
            // 返回数据对象
            return TResult.success(page);
        }
        // 返回方法不存在提示
        return TResult.build(0, "无效的请求地址");
    }

    private static <T> BaseMapper<T> getBaseMapper(String name) {
        try {
            return SpringContextUtil.getApplicationContext().getBean(name, BaseMapper.class);
        } catch (Exception e) {
            try {
                if (!name.endsWith("Mapper")) name += "Mapper";
                return SpringContextUtil.getApplicationContext().getBean(name, BaseMapper.class);
            } catch (Exception ignored) {
                try {
                    return SpringContextUtil.getApplicationContext().getBean(name.replace("Mapper", "Dao"), BaseMapper.class);
                } catch (Exception ignored2) {
                    log.error("获取BaseMapper对象失败", e);
                    return null;
                }
            }
        }
    }
}
