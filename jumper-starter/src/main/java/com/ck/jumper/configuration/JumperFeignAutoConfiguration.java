package com.ck.jumper.configuration;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.jumper.feign.JumperFeign;
import com.ck.jumper.utils.QueryDto;
import com.ck.jumper.utils.QueryUtil;
import com.ck.spring.tools.spring.FeignClientUtils;
import com.ck.spring.tools.spring.SpringContextUtil;
import com.ck.tools.com.TResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JumperAutoConfiguration
 * @author cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
@Slf4j
@ConditionalOnProperty(prefix = "jumper", name = "enable", havingValue = "true")
@Configuration
@ConditionalOnClass({QueryUtil.class, QueryDto.class, TResult.class, Page.class, FeignClientUtils.class, SpringContextUtil.class})
public class JumperFeignAutoConfiguration {

    /**
     * 提供 JumperFeign 注入
     */
    @Bean
    @ConditionalOnMissingBean(JumperFeign.class)
    public JumperFeign jumperFeign(ObjectMapper objectMapper) {
        JumperFeign jumperFeign = new JumperFeign(objectMapper);
        log.info("JumperFeign [{}]", jumperFeign);
        return jumperFeign;
    }
}
