package com.ck.jumper.configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.ck.jumper.controller.JumperQueryController;
import com.ck.spring.tools.configuration.SpringToolsAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JumperAutoConfiguration
 * @author cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
@Slf4j
@ConditionalOnProperty(prefix = "jumper", name = "enable", havingValue = "true")
@Configuration
@ConditionalOnClass({SpringToolsAutoConfiguration.class, MybatisPlusAutoConfiguration.class})
@AutoConfigureAfter({SpringToolsAutoConfiguration.class, MybatisPlusAutoConfiguration.class})
public class JumperAutoConfiguration {
    @Value("${jumper.DbType:mysql}")
    private String dbType;

    public JumperAutoConfiguration() {
        String version = "V1.0";

        System.out.println( "   ||      _  _  _ __       \n"+
                            "   || || || || || '_ |      \n"+
                            "   || || || || || |_)|      \n"+
                            ",\\/ | |＼／| || || --`      \n"+
                            " ＼／   ＼／      |_|   ___JUMPER ["+version+"]");
    }

    /**
     * 依赖与 QueryUtil 工具构建的 公共数据查询接口
     */
    @Bean
    @ConditionalOnMissingBean(JumperQueryController.class)
    public JumperQueryController jumperQueryController() {
        JumperQueryController exceptionHandler = new JumperQueryController();
        log.info("JumperQueryController [{}]", exceptionHandler);
        return exceptionHandler;
    }

    /**
     * MyBatisPlus  分页支持
     */
    @ConditionalOnMissingBean(MybatisPlusInterceptor.class)
    @Bean("mybatisPlusInterceptor")
    public MybatisPlusInterceptor mybatisPlusInterceptor() {

        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(dbType == null ? DbType.MYSQL : DbType.valueOf(dbType)));
        log.info("MybatisPlusInterceptor [{}]", interceptor);
        return interceptor;
    }
}
