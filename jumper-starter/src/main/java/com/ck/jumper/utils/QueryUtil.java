package com.ck.jumper.utils;

import com.baomidou.mybatisplus.core.conditions.interfaces.Compare;
import com.baomidou.mybatisplus.core.conditions.interfaces.Func;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.core.toolkit.support.SerializedLambda;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ck.tools.reflect.ClassUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.property.PropertyNamer;
import org.springframework.core.GenericTypeResolver;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * mybatis plus LambdaQuery 查询公共方法
 *
 * @author Cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
@Slf4j
public final class QueryUtil {

    /**
     * 构建分页对象
     *
     * @param <T> 实体泛型
     * @return 分页对象
     */
    public static <T> Page<T> buildPage(Integer current, Integer size) {
        return buildPage(Long.valueOf(current), Long.valueOf(size));
    }

    /**
     * 构建分页对象
     *
     * @param <T> 实体泛型
     * @return 分页对象
     */
    public static <T> Page<T> buildPage(Long current, Long size) {
        return new Page<>(Optional.ofNullable(current).orElse(1L), Optional.ofNullable(size).orElse(20L));
    }

    /**
     * 构建分页对象
     *
     * @param <T> 实体泛型
     * @return 分页对象
     */
    public static <Dto, T> Page<Dto> buildPage(Page<T> page, Function<T, Dto> convert) {
        if (page == null) return null;
        Page<Dto> result = new Page<>(page.getCurrent(), page.getSize(), page.getTotal());
        result.setPages(page.getPages());
        result.setRecords(convertList(page.getRecords(), convert));
        return result;
    }

    /**
     * 结果集转换Dto对象
     *
     * @param data    查询结果集
     * @param convert Po 转 Dto逻辑
     * @param <Dto>   DTO泛型
     * @param <T>     PO泛型
     * @return Dto对象
     */
    public static <Dto, T> List<Dto> convertList(List<T> data, Function<T, Dto> convert) {
        if (data == null || convert == null) return null;
        return data.stream().map(convert).collect(Collectors.toList());
    }


    /**
     * 直接使用 IService<R> 进行分页查询并返回Dto分页结果
     *
     * @param service 执行查询的 DBService
     * @param dto     查询条件
     * @param <T>     po泛型
     * @return 分页查询结果
     */
    public static <T, Dto> Page<T> queryPage(IService<T> service, QueryDto<Dto> dto) {
        return queryPage(service, dto, null);
    }

    /**
     * 直接使用 IService<R> 进行分页查询并返回Dto分页结果
     *
     * @param service 执行查询的 DBService
     * @param dto     查询条件
     * @param <T>     po泛型
     * @return 分页查询结果
     */
    public static <T, Dto> Page<T> queryPage(IService<T> service, QueryDto<Dto> dto, Consumer<LambdaQueryChainWrapper<T>> consumerLambdaQueryWrapper) {
        LambdaQueryChainWrapper<T> lambdaQueryWrapper = setWrapper(service.lambdaQuery(), dto, service.getEntityClass());
        if (consumerLambdaQueryWrapper != null) {
            consumerLambdaQueryWrapper.accept(lambdaQueryWrapper);
        }
        return lambdaQueryWrapper.page(QueryUtil.buildPage(dto.getCurrent(), dto.getSize()));
    }

    /**
     * 直接使用 BaseMapper<T> 进行查询并返回Dto分页结果
     *
     * @param mapper 执行查询的 BaseMapper<T>
     * @param dto    查询条件
     * @param <T>    po泛型
     * @return 分页查询结果
     */
    public static <T, Dto> Page<T> queryPage(BaseMapper<T> mapper, QueryDto<Dto> dto) {
        return queryPage(mapper, dto, null);
    }

    /**
     * 直接使用 BaseMapper<T> 进行查询并返回Dto分页结果
     *
     * @param mapper 执行查询的 BaseMapper<T>
     * @param dto    查询条件
     * @param <T>    po泛型
     * @return 分页查询结果
     */
    public static <T, Dto> Page<T> queryPage(BaseMapper<T> mapper, QueryDto<Dto> dto, Consumer<LambdaQueryWrapper<T>> consumerLambdaQueryWrapper) {
        Class<T> entityClass = getMapperEntityClass(mapper);
        LambdaQueryWrapper<T> wrapper = setWrapper(new LambdaQueryWrapper<>(), dto, entityClass);
        if (consumerLambdaQueryWrapper != null) {
            consumerLambdaQueryWrapper.accept(wrapper);
        }
        return mapper.selectPage(buildPage(dto.getCurrent(), dto.getSize()), wrapper);
    }

    /**
     * 直接使用 IService<R> 进行查询
     *
     * @param service 执行查询的 DBService
     * @param dto     查询条件
     * @param <T>     po泛型
     * @return 查询结果
     */
    public static <T, Dto> List<T> query(IService<T> service, QueryDto<Dto> dto) {
        return query(service, dto, null);
    }

    /**
     * 直接使用 IService<R> 进行查询
     *
     * @param service 执行查询的 DBService
     * @param dto     查询条件
     * @param <T>     po泛型
     * @return 查询结果
     */
    public static <T, Dto> List<T> query(IService<T> service, QueryDto<Dto> dto, Consumer<LambdaQueryChainWrapper<T>> consumerLambdaQueryWrapper) {
        LambdaQueryChainWrapper<T> lambdaQueryWrapper = setWrapper(service.lambdaQuery(), dto, service.getEntityClass());
        if (consumerLambdaQueryWrapper != null) {
            consumerLambdaQueryWrapper.accept(lambdaQueryWrapper);
        }
        return lambdaQueryWrapper.list();
    }

    /**
     * 直接使用 BaseMapper<T> 进行查询
     *
     * @param mapper 执行查询的 BaseMapper<T>
     * @param dto    查询条件
     * @param <T>    po泛型
     * @return 查询结果
     */
    public static <T, Dto> List<T> query(BaseMapper<T> mapper, QueryDto<Dto> dto) {
        return query(mapper, dto, null);
    }

    /**
     * 直接使用 BaseMapper<T> 进行查询
     *
     * @param mapper 执行查询的 BaseMapper<T>
     * @param dto    查询条件
     * @param <T>    po泛型
     * @return 查询结果
     */
    public static <T, Dto> List<T> query(BaseMapper<T> mapper, QueryDto<Dto> dto, Consumer<LambdaQueryWrapper<T>> consumerLambdaQueryWrapper) {
        Class<T> entityClass = getMapperEntityClass(mapper);
        LambdaQueryWrapper<T> wrapper = setWrapper(new LambdaQueryWrapper<>(), dto, entityClass);
        if (consumerLambdaQueryWrapper != null) {
            consumerLambdaQueryWrapper.accept(wrapper);
        }
        return mapper.selectList(wrapper);
    }

    /**
     * 设置 LambdaQueryChainWrapper eq 和 like 查询条件
     *
     * @param lambdaQueryWrapper 查询 lambdaQueryWrapper 对象
     * @param dto                查询条件
     * @param entityClass        查询的实体类对象
     * @param <T>                po泛型
     */
    public static <T, Dto, R extends Compare<R, SFunction<T, ?>> & Func<R, SFunction<T, ?>> & Query<R, T, SFunction<T, ?>>> R setWrapper(R lambdaQueryWrapper, QueryDto<Dto> dto, Class<T> entityClass) {
        if (ObjectUtils.isNotEmpty(lambdaQueryWrapper) && ObjectUtils.isNotEmpty(entityClass) && dto != null) {

            // Po对象字段名称列表
            Set<String> poFieldNames = ClassUtils.getFieldNames(entityClass);

            // 指定查询的字段 select 值
            if (ObjectUtils.isNotEmpty(dto.getSelect())) {
                // 过滤掉 指定的字段名称 在实体对象中不包含的字段
                dto.getSelect().removeIf(fieldName -> !poFieldNames.contains(fieldName));
                if (ObjectUtils.isNotEmpty(dto.getSelect())) {
                    lambdaQueryWrapper.select(entityClass, field -> dto.getSelect().contains(field.getField().getName()));
                }
            }

            setWrapper(dto.getEq(), lambdaQueryWrapper::eq, entityClass, poFieldNames);
            setWrapper(dto.getNe(), lambdaQueryWrapper::ne, entityClass, poFieldNames);
            setWrapper(dto.getLike(), lambdaQueryWrapper::like, entityClass, poFieldNames);
            setWrapper(dto.getLt(), lambdaQueryWrapper::lt, entityClass, poFieldNames);
            setWrapper(dto.getGt(), lambdaQueryWrapper::gt, entityClass, poFieldNames);
            setWrapper(dto.getLe(), lambdaQueryWrapper::le, entityClass, poFieldNames);
            setWrapper(dto.getGe(), lambdaQueryWrapper::ge, entityClass, poFieldNames);

            if (ObjectUtils.isNotEmpty(dto.getEqMap())) {
                dto.getEqMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.eq(getFieldSFunction(k, entityClass), v);
                    }
                });
            }

            if (ObjectUtils.isNotEmpty(dto.getNeMap())) {
                dto.getNeMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.ne(getFieldSFunction(k, entityClass), v);
                    }
                });
            }
            if (ObjectUtils.isNotEmpty(dto.getLtMap())) {
                dto.getLtMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.lt(getFieldSFunction(k, entityClass), v);
                    }
                });
            }
            if (ObjectUtils.isNotEmpty(dto.getLeMap())) {
                dto.getLeMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.le(getFieldSFunction(k, entityClass), v);
                    }
                });
            }
            if (ObjectUtils.isNotEmpty(dto.getGtMap())) {
                dto.getGtMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.gt(getFieldSFunction(k, entityClass), v);
                    }
                });
            }
            if (ObjectUtils.isNotEmpty(dto.getGeMap())) {
                dto.getGeMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.ge(getFieldSFunction(k, entityClass), v);
                    }
                });
            }
            if (ObjectUtils.isNotEmpty(dto.getLikeMap())) {
                dto.getLikeMap().forEach((k, v) -> {
                    if (poFieldNames.contains(k)) {
                        lambdaQueryWrapper.like(getFieldSFunction(k, entityClass), v);
                    }
                });
            }

            if (ObjectUtils.isNotEmpty(dto.getIn())) {
                dto.getIn().keySet().stream()
                        .filter(name -> poFieldNames.contains(name) && ObjectUtils.isNotEmpty(dto.getIn().get(name)))
                        .forEach(name -> lambdaQueryWrapper.in(getFieldSFunction(name, entityClass), dto.getIn().get(name)));
            }
            if (ObjectUtils.isNotEmpty(dto.getNotIn())) {
                dto.getNotIn().keySet().stream()
                        .filter(name -> poFieldNames.contains(name) && ObjectUtils.isNotEmpty(dto.getIn().get(name)))
                        .forEach(name -> lambdaQueryWrapper.notIn(getFieldSFunction(name, entityClass), dto.getNotIn().get(name)));
            }
            if (ObjectUtils.isNotEmpty(dto.getIsNull())) {
                dto.getIsNull().stream().filter(poFieldNames::contains).forEach(name -> lambdaQueryWrapper.isNull(getFieldSFunction(name, entityClass)));
            }
            if (ObjectUtils.isNotEmpty(dto.getIsNotNull())) {
                dto.getIsNotNull().stream().filter(poFieldNames::contains).forEach(name -> lambdaQueryWrapper.isNotNull(getFieldSFunction(name, entityClass)));
            }

            if (ObjectUtils.isNotEmpty(dto.getOrderByAsc()) && ObjectUtils.isNotEmpty(dto.getOrderByDesc())) {
                /*
                 * 复杂场景下 需要混合排序
                 * 同时指定了升序和降序时 会将 orderByAsc 与 orderByDesc 合并混合执行
                 * 合并规则  按照数组下标  进行组合 舍弃为null的字段。
                 * 如果下标 都不为null 则取 orderByAsc 升序 执行。
                 * 例如：
                 * orderByAsc =  [a,null,b,null,c]
                 * orderByDesc = [null,d,null,e,f]
                 * 混合后的结果：[a⬆,d⬇,b⬆,e⬇,c⬆]  f 将会被舍弃
                 */
                for (int i = 0; i < Math.max(dto.getOrderByAsc().size(), dto.getOrderByDesc().size()); i++) {
                    if (i < dto.getOrderByAsc().size() && dto.getOrderByAsc().get(i) != null && !"".equals(dto.getOrderByAsc().get(i))) {
                        SFunction<T, ?> orderNameSFunc = getFieldSFunction(dto.getOrderByAsc().get(i), entityClass);
                        if (orderNameSFunc != null)
                            lambdaQueryWrapper.orderByAsc(orderNameSFunc);
                    } else if (i < dto.getOrderByDesc().size() && dto.getOrderByDesc().get(i) != null && !"".equals(dto.getOrderByDesc().get(i))) {
                        SFunction<T, ?> orderNameSFunc = getFieldSFunction(dto.getOrderByDesc().get(i), entityClass);
                        if (orderNameSFunc != null)
                            lambdaQueryWrapper.orderByDesc(orderNameSFunc);
                    }
                }
            } else {
                if (ObjectUtils.isNotEmpty(dto.getOrderByAsc())) {
                    dto.getOrderByAsc().stream().filter(poFieldNames::contains).forEach(name -> lambdaQueryWrapper.orderByAsc(getFieldSFunction(name, entityClass)));
                }
                if (ObjectUtils.isNotEmpty(dto.getOrderByDesc())) {
                    dto.getOrderByDesc().stream().filter(poFieldNames::contains).forEach(name -> lambdaQueryWrapper.orderByDesc(getFieldSFunction(name, entityClass)));
                }
            }
        }
        return lambdaQueryWrapper;
    }

    /**
     * 设置查询条件
     *
     * @param conditionBean 条件值对象
     * @param consumer      条件实现函数
     * @param entityClass   Po对象类型
     * @param poFieldNames  Po对象字段名称列表
     */
    private static void setWrapper(Object conditionBean, BiConsumer<SFunction, Object> consumer, Class<?> entityClass, Set<String> poFieldNames) {
        if (ObjectUtils.isNotEmpty(conditionBean) && ObjectUtils.isNotEmpty(consumer) && ObjectUtils.isNotEmpty(poFieldNames)) {
            if (Map.class.isAssignableFrom(conditionBean.getClass())) {
                Map<?, ?> temp = ((Map<?, ?>) conditionBean);
                for (Object key : temp.keySet()) {
                    if (temp.get(key) == null || temp.get(key).toString().trim().isEmpty() || !poFieldNames.contains(key.toString())) {
                        continue;
                    }
                    consumer.accept(getFieldSFunction(key.toString(), entityClass), dateToStr(temp.get(key)));
                }
            } else {
                ClassUtils.getFields(conditionBean.getClass()).stream().filter(i -> poFieldNames.contains(i.getName())).forEach(i -> {
                    try {
                        i.setAccessible(true);
                        Object val = i.get(conditionBean);
                        if (val != null && !val.toString().trim().isEmpty()) {
                            consumer.accept(getFieldSFunction(i.getName(), entityClass), dateToStr(val));
                        }
                    } catch (Throwable e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        }
    }

    /**
     * 获取字段 getSFunction 对象
     *
     * @param fieldName   字段名称
     * @param entityClass 字段所属对象类型
     * @param <T>         实体类类型
     * @return SFunction 对象
     */
    public static <T> SFunction<T, ?> getFieldSFunction(String fieldName, Class<T> entityClass) {
        try {
            return (SFunction<T, ?>) ClassUtils.getFieldMethodHandle(fieldName, entityClass, SFunction.class).invokeExact();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    /**
     * 获取函数表达式字段名称
     *
     * @param name 函数表达式
     * @param <T>  实体类类型
     * @return 字段名称 or null
     */
    public static <T> String getFileName(SFunction<T, ?> name) {
        if (name != null) {
            SerializedLambda lambda = LambdaUtils.resolve(name);
            if (lambda != null) {
                return PropertyNamer.methodToProperty(lambda.getImplMethodName());
            }
            log.info("获取字段名称失败{} SerializedLambda is null", name);
        }
        return null;
    }

    /**
     * 获取泛型类型
     *
     * @param clazz      类
     * @param genericIfc 泛型接口
     * @param index      泛型索引
     * @return 泛型类型
     */
    private static <T> Class<T> getSuperClassGenericType(final Class<?> clazz, final Class<?> genericIfc, final int index) {
        Class<?>[] typeArguments = GenericTypeResolver.resolveTypeArguments(clazz, genericIfc);
        return null == typeArguments ? null : (Class<T>) typeArguments[index];
    }

    /**
     * MyBatis 生成Sql时 无配置  不会转换日期类型
     *
     * @param val 日期类型值
     * @return 转换后的字符串
     */
    private static Object dateToStr(Object val) {
        if (Date.class.isAssignableFrom(val.getClass())) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            val = simpleDateFormat.format(val);
        }
        return val;
    }

    /**
     * 获取 BaseMapper<T> 中T的类型
     *
     * @param mapper BaseMapper
     * @param <T>    Po实体类型
     * @return Po实体类型
     */
    public static <T> Class<T> getMapperEntityClass(BaseMapper<T> mapper) {
        return getSuperClassGenericType(mapper.getClass(), BaseMapper.class, 0);
    }

    /**
     * 构建查询对象
     *
     * @param entityClass 查询的实体类对象
     * @param <T>         Po实体类型
     * @return QueryDto
     */
    public static <T> QueryDto<T> buildQueryDto(Class<T> entityClass) {
        QueryDto<T> result = buildQueryDto();
        if (entityClass != null) {
            result.setEq(ClassUtils.newInstance(entityClass));
            result.setNe(ClassUtils.newInstance(entityClass));
            result.setLike(ClassUtils.newInstance(entityClass));
            result.setLt(ClassUtils.newInstance(entityClass));
            result.setLe(ClassUtils.newInstance(entityClass));
            result.setGt(ClassUtils.newInstance(entityClass));
            result.setGe(ClassUtils.newInstance(entityClass));
            result.setOther(ClassUtils.newInstance(entityClass));
        }
        return result;
    }

    /**
     * 构建查询对象
     *
     * @param <T> Po实体类型
     * @return QueryDto
     */
    public static <T> QueryDto<T> buildQueryDto() {
        return new QueryDto<T>();
    }

    /**
     * 构建查询对象
     *
     * @param eq   等条件
     * @param like 模糊条件
     * @param <T>  Po实体类型
     * @return QueryDto
     */
    @Deprecated
    public static <T> QueryDto<T> buildQueryDto(T eq, T like) {
        return new QueryDto<T>().setEq(eq).setLike(like);
    }

    /**
     * 分页查询并遍历结果集
     *
     * @param mapper   BaseMapper<T>
     * @param queryDto 查询条件
     * @param function 处理任务执行函数
     * @param <E>      返回值类型
     * @param <T>      查询数据实体类型
     * @return 根据Function 的返回值，组装到数组返回
     */
    public static <E, T> List<E> forPage(BaseMapper<T> mapper, QueryDto<T> queryDto, Function<T, E> function) {
        return forPage(mapper, 200, queryDto, function);
    }

    /**
     * 分页查询并遍历结果集
     *
     * @param mapper   BaseMapper<T>
     * @param times    最大查询次数限制
     * @param queryDto 查询条件
     * @param function 处理任务执行函数
     * @param <E>      返回值类型
     * @param <T>      查询数据实体类型
     * @return 根据Function 的返回值，组装到数组返回
     */
    public static <E, T> List<E> forPage(BaseMapper<T> mapper, int times, QueryDto<T> queryDto, Function<T, E> function) {
        if (times < 1) {
            return forPage(mapper, queryDto, function);
        }
        queryDto = queryDto == null ? QueryUtil.buildQueryDto() : queryDto;

        Page<T> pages = queryPage(mapper, queryDto);

        Integer current = queryDto.getCurrent();

        List<E> result = new ArrayList<>();
        for (int i = 0; i < pages.getPages() && i < times; i++) {
            result.addAll(pages.getRecords().stream().map(function).collect(Collectors.toList()));
            pages = QueryUtil.queryPage(mapper, queryDto.setCurrent((int) (pages.getCurrent() + 1)));
        }
        queryDto.setCurrent(current);
        return result;
    }


    /**
     * 更近数据 并记录变更log日志
     *
     * @param mapper    数据Mapper
     * @param mapperLog 变更前日志Mapper
     * @param queryDto  数据条件
     * @param newVal    变更后的新值
     * @param function  数据PO转换日志PO
     * @param <E>       日志PO类型
     * @param <T>       数据PO类型
     * @return 返回变更条数
     */
    public static <E, T> int updateThenLog(BaseMapper<T> mapper, BaseMapper<E> mapperLog, QueryDto<T> queryDto, T newVal, Function<T, E> function) {
        List<T> orgData = query(mapper, queryDto);
        if (orgData != null) {
            orgData.forEach(i -> mapperLog.insert(function.apply(i)));
        }

        return mapper.update(newVal, setWrapper(new LambdaQueryWrapper<>(), queryDto, getMapperEntityClass(mapper)));
    }
}
