package com.ck.jumper.utils;

import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 统一查询条件报文体
 * 请求接口传递查询条件时，规范传参格式使用。
 * current          分页-页码
 * size             分页-每页数据量
 * select           指定查询的字段列表 为空则不设置，用于减少查询字段数量，提升性能。
 * eq               条件 - 等
 * ne               条件 - 不等
 * gt               条件 - 大于
 * ge               条件 - 大于等于
 * lt               条件 - 小于
 * le               条件 - 小于等于
 * in               条件 - 范围包含条件，比如查询指定的 ID 列表
 * notIn            条件 - 范围不包含条件，比如查询指定的 ID 列表
 * isNull           条件 - 字段为 null
 * isNotNull        条件 - 字段不为 null
 * orderByDesc      排序（返回值字段名称） - 倒序的字段
 * orderByAsc       排序（返回值字段名称） - 升序的字段
 * 说明：T 一般是查询结果对象。例如 eq 的所有参数都将按照 等 条件进行查询。
 * 排序字段规则说明：
 * 复杂场景下 需要混合排序
 * 同时指定了升序和降序时 会将 orderByAsc 与 orderByDesc 合并混合执行
 * 合并规则  按照数组下标  进行组合 舍弃为null的字段。
 * 如果下标 都不为null 则取 orderByAsc 升序 执行。
 * 例如：
 * orderByAsc =  [a,null,b,null,c]
 * orderByDesc = [null,d,null,e,f]
 * 混合后的结果：[a⬆,d⬇,b⬆,e⬇,c⬆]  f 将会被舍弃
 *
 * @param <T>
 * @author Cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 */
@Data
@Accessors(chain = true)
public class QueryDto<T> {

    /**
     * 分页-单页数量
     */
    private Integer size = 20;

    /**
     * 分页-页码
     */
    private Integer current = 1;

    /**
     * 查询的字段列表 为空则不设置
     */
    private Set<String> select;

    /**
     * 等条件
     */
    private T eq;

    /**
     * 不等条件
     */
    private T ne;

    /**
     * 包含条件
     */
    private T like;

    /**
     * 小于条件
     */
    private T lt;

    /**
     * 大于条件
     */
    private T gt;

    /**
     * 小于等于条件
     */
    private T le;

    /**
     * 大于等于条件
     */
    private T ge;

    /**
     * 其他条件
     */
    private T other;

    /**
     * 自定义EQ条件 优先级最高
     */
    private Map<String, Object> eqMap;

    /**
     * 自定义NE条件 优先级最高
     */
    private Map<String, Object> neMap;

    /**
     * 自定义lt条件 优先级最高
     */
    private Map<String, Object> ltMap;

    /**
     * 自定义le条件 优先级最高
     */
    private Map<String, Object> leMap;

    /**
     * 自定义gt条件 优先级最高
     */
    private Map<String, Object> gtMap;

    /**
     * 自定义ge条件 优先级最高
     */
    private Map<String, Object> geMap;

    /**
     * 自定义Like条件 优先级最高
     */
    private Map<String, Object> likeMap;

    /**
     * in条件
     */
    private Map<String, Set<?>> in;

    /**
     * in条件
     */
    private Map<String, Set<?>> notIn;

    /**
     * 字段为null条件
     */
    private Set<String> isNull;

    /**
     * 字段不为null条件
     */
    private Set<String> isNotNull;

    /**
     * 排序字段-升序
     * 复杂场景下 需要混合排序
     * 同时指定了升序和降序时 会将 orderByAsc 与 orderByDesc 合并混合执行
     * 合并规则  按照数组下标  进行组合 舍弃为null的字段。
     * 如果下标 都不为null 则取 orderByAsc 升序 执行。
     * 例如：
     * orderByAsc =  [a,null,b,null,c]
     * orderByDesc = [null,d,null,e,f]
     * 混合后的结果：[a⬆,d⬇,b⬆,e⬇,c⬆]  f 将会被舍弃
     * 转化SQL:  ORDER BY a ASC,d DESC,b ASC,e DESC,c ASC
     */
    private List<String> orderByAsc;

    /**
     * 排序字段-降序
     * 复杂场景下 需要混合排序
     * 同时指定了升序和降序时 会将 orderByAsc 与 orderByDesc 合并混合执行
     * 合并规则  按照数组下标  进行组合 舍弃为null的字段。
     * 如果下标 都不为null 则取 orderByAsc 升序 执行。
     * 例如：
     * orderByAsc =  [a,null,b,null,c]
     * orderByDesc = [null,d,null,e,f]
     * 混合后的结果：[a⬆,d⬇,b⬆,e⬇,c⬆]  f 将会被舍弃
     * 转化SQL:  ORDER BY a ASC,d DESC,b ASC,e DESC,c ASC
     */
    private List<String> orderByDesc;

    /**
     * 设置查询的字段列表 为空则不设置
     *
     * @param fields 字段名称列表
     * @return 当前查询对象
     */
    @SafeVarargs
    public final QueryDto<T> select(SFunction<T, ?>... fields) {
        return select(true, fields);
    }

    @SafeVarargs
    public final QueryDto<T> select(Boolean condition, SFunction<T, ?>... fields) {
        if (!Boolean.FALSE.equals(condition) && fields != null && fields.length > 0) {
            return select(condition, Stream.of(fields).map(QueryUtil::getFileName).collect(Collectors.toList()));
        }
        return this;
    }

    /**
     * 设置查询的字段列表 为空则不设置
     *
     * @param fields 字段名称列表
     * @return 当前查询对象
     */
    public QueryDto<T> select(String... fields) {
        return select(true, fields);
    }

    public QueryDto<T> select(Boolean condition, String... fields) {
        if (!Boolean.FALSE.equals(condition) && fields != null && fields.length > 0) {
            return select(condition, Arrays.asList(fields));
        }
        return this;
    }

    /**
     * 设置查询的字段列表 为空则不设置
     *
     * @param fields 字段名称列表
     * @return 当前查询对象
     */
    public QueryDto<T> select(Boolean condition, Collection<String> fields) {
        if (!Boolean.FALSE.equals(condition) && fields != null && !fields.isEmpty()) {
            if (this.select == null)
                this.select = new LinkedHashSet<>(fields);
            else
                this.select.addAll(fields);
        }
        return this;
    }

    /**
     * 添加 等 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> eq(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return eq(name, val);
        return this;
    }

    public QueryDto<T> eq(SFunction<T, ?> name, Object val) {
        if (this.eqMap == null) {
            this.eqMap = new LinkedHashMap<>();
        }
        this.eqMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 不等 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> ne(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return ne(name, val);
        return this;
    }

    public QueryDto<T> ne(SFunction<T, ?> name, Object val) {
        if (this.neMap == null) {
            this.neMap = new LinkedHashMap<>();
        }
        this.neMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 lt 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> lt(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return lt(name, val);
        return this;
    }

    public QueryDto<T> lt(SFunction<T, ?> name, Object val) {
        if (this.ltMap == null) {
            this.ltMap = new LinkedHashMap<>();
        }
        this.ltMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 le 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> le(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return le(name, val);
        return this;
    }

    public QueryDto<T> le(SFunction<T, ?> name, Object val) {
        if (this.leMap == null) {
            this.leMap = new LinkedHashMap<>();
        }
        this.leMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 lt 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> gt(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return gt(name, val);
        return this;
    }

    public QueryDto<T> gt(SFunction<T, ?> name, Object val) {
        if (this.gtMap == null) {
            this.gtMap = new LinkedHashMap<>();
        }
        this.gtMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 le 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> ge(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return ge(name, val);
        return this;
    }

    public QueryDto<T> ge(SFunction<T, ?> name, Object val) {
        if (this.geMap == null) {
            this.geMap = new LinkedHashMap<>();
        }
        this.geMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 IN 条件
     *
     * @param name 字段名称
     * @param val  值
     * @return 当前查询对象
     */
    public QueryDto<T> like(boolean condition, SFunction<T, ?> name, Object val) {
        if (condition)
            return like(name, val);
        return this;
    }

    public QueryDto<T> like(SFunction<T, ?> name, Object val) {
        if (this.likeMap == null) {
            this.likeMap = new LinkedHashMap<>();
        }
        this.likeMap.put(QueryUtil.getFileName(name), val);
        return this;
    }

    /**
     * 添加 IN 条件
     *
     * @param name 字段名称
     * @param in   值
     * @return 当前查询对象
     */
    public QueryDto<T> in(boolean condition, SFunction<T, ?> name, Object... in) {
        if (condition)
            return in(name, in);
        return this;
    }

    public QueryDto<T> in(SFunction<T, ?> name, Object... in) {
        if (in == null) {
            return this;
        }
        return in(name, Arrays.asList(in));
    }

    /**
     * 添加 IN 条件
     *
     * @param name 字段名称
     * @param in   值
     * @return 当前查询对象
     */
    public QueryDto<T> in(boolean condition, SFunction<T, ?> name, Collection in) {
        if (condition)
            return in(name, in);
        return this;
    }

    public QueryDto<T> in(SFunction<T, ?> name, Collection in) {
        if (in == null || in.isEmpty()) {
            return this;
        }
        if (this.in == null) {
            this.in = new TreeMap<>();
        }

        String fieldName = QueryUtil.getFileName(name);
        if (!this.in.containsKey(fieldName)) {
            this.in.put(fieldName, new LinkedHashSet<>());
        }
        this.in.get(fieldName).addAll(in);
        return this;
    }

    /**
     * 添加 IN 条件
     *
     * @param name 字段名称
     * @param in   值
     * @return 当前查询对象
     */
    public QueryDto<T> notIn(boolean condition, SFunction<T, ?> name, Object... in) {
        if (condition)
            return notIn(name, in);
        return this;
    }

    public QueryDto<T> notIn(SFunction<T, ?> name, Object... in) {
        if (in == null) {
            return this;
        }
        return notIn(name, Arrays.asList(in));
    }

    /**
     * 添加 IN 条件
     *
     * @param name 字段名称
     * @param in   值
     * @return 当前查询对象
     */
    public QueryDto<T> notIn(boolean condition, SFunction<T, ?> name, Collection in) {
        if (condition)
            return notIn(name, in);
        return this;
    }

    public QueryDto<T> notIn(SFunction<T, ?> name, Collection in) {
        if (in == null || in.isEmpty()) {
            return this;
        }
        if (this.notIn == null) {
            this.notIn = new TreeMap<>();
        }

        String fieldName = QueryUtil.getFileName(name);
        if (!this.notIn.containsKey(fieldName)) {
            this.notIn.put(fieldName, new LinkedHashSet<>());
        }
        this.notIn.get(fieldName).addAll(in);
        return this;
    }

    /**
     * 添加 为空 字段
     *
     * @param name 字段名称
     * @return 当前查询对象
     */
    @SafeVarargs
    public final QueryDto<T> isNull(boolean condition, SFunction<T, ?>... name) {
        if (condition)
            return isNull(name);
        return this;
    }

    @SafeVarargs
    public final QueryDto<T> isNull(SFunction<T, ?>... name) {
        if (this.isNull == null) {
            this.isNull = new LinkedHashSet<>();
        }

        if (name != null) {
            Stream.of(name).forEach(i -> this.isNull.add(QueryUtil.getFileName(i)));
        }
        return this;
    }

    /**
     * 添加 不为空 字段
     *
     * @param name 字段名称
     * @return 当前查询对象
     */
    @SafeVarargs
    public final QueryDto<T> isNotNull(boolean condition, SFunction<T, ?>... name) {
        if (condition)
            return isNotNull(name);
        return this;
    }

    @SafeVarargs
    public final QueryDto<T> isNotNull(SFunction<T, ?>... name) {
        if (this.isNotNull == null) {
            this.isNotNull = new LinkedHashSet<>();
        }
        if (name != null) {
            Stream.of(name).forEach(i -> this.isNotNull.add(QueryUtil.getFileName(i)));
        }
        return this;
    }

    /**
     * 添加 升序 字段
     *
     * @param name 字段名称
     * @return 当前查询对象
     */
    @SafeVarargs
    public final QueryDto<T> orderByAsc(SFunction<T, ?>... name) {
        if (this.orderByAsc == null) {
            this.orderByAsc = new ArrayList<>();
        }
        if (name != null) {
            Stream.of(name).forEach(i -> this.orderByAsc.add(QueryUtil.getFileName(i)));
        }
        return this;
    }

    /**
     * 添加 降序 字段
     *
     * @param name 字段名称
     * @return 当前查询对象
     */
    @SafeVarargs
    public final QueryDto<T> orderByDesc(SFunction<T, ?>... name) {
        if (this.orderByDesc == null) {
            this.orderByDesc = new ArrayList<>();
        }

        if (name != null) {
            Stream.of(name).forEach(i -> this.orderByDesc.add(QueryUtil.getFileName(i)));
        }
        return this;
    }

    /**
     * 获取In条件参数
     *
     * @param name 字段名
     * @return 设置的值
     */
    public Set<?> getIn(SFunction<T, ?> name) {
        return this.in.get(QueryUtil.getFileName(name));
    }

    /**
     * 获取NotIn条件参数
     *
     * @param name 字段名
     * @return 设置的值
     */
    public Set<?> getNotIn(SFunction<T, ?> name) {
        return this.notIn.get(QueryUtil.getFileName(name));
    }
}
