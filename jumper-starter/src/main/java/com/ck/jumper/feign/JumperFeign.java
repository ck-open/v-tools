package com.ck.jumper.feign;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.tools.com.TResult;
import com.ck.jumper.utils.QueryDto;
import com.ck.jumper.utils.QueryUtil;
import com.ck.spring.tools.configuration.JacksonObjectMapper;
import com.ck.spring.tools.spring.FeignClientUtils;
import com.ck.spring.tools.spring.SpringContextUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.IOException;
import java.util.Objects;

/**
 * 公共接口 JumperFeign 客户端
 * mybatis plus LambdaQuery 查询公共方法
 * @author Cyk
 * @version 1.0
 * @since 2023/03/18 19:47
 **/
public class JumperFeign {
    private ObjectMapper objectMapper;

    public JumperFeign(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        init();
    }

    /**
     * 获取 JumperFeignClient
     *
     * @param serverName 服务名称（ServerId）
     * @param url        服务地址（ServerUrl）
     * @return client
     */
    public static Client getClient(String serverName, String url) {
        return new Client(serverName, url);
    }

    /**
     * 获取 JumperFeignClient
     *
     * @param serverName 服务名称（ServerId）
     * @return client
     */
    public static Client getClient(String serverName) {
        return new Client(serverName, null);
    }

    public static class Client {
        /**
         * 服务地址
         */
        private final String serverUrl;
        /**
         * 服务名称
         */
        private String serverName;
        /**
         * 目标查询Mapper对象Bean名称
         */
        private String mapperBeanName;

        private QueryDto<?> query;

        public Client(String serverName, String serverUrl) {
            this.serverName = serverName;
            this.serverUrl = serverUrl;
        }

        private JumperFeignClient getJumperFeignClient() {
            return this.serverUrl == null ? FeignClientUtils.build(this.serverName, JumperFeignClient.class) : FeignClientUtils.build(this.serverName, this.serverUrl, JumperFeignClient.class);
        }

        private void check() {
            Objects.requireNonNull(this.serverName, "目标服务名称不能为空");
            Objects.requireNonNull(this.mapperBeanName, "目标服务MapperBeanName不能为空");
        }

        private <T> T copyProperties(Object source, Class<T> tClass) {
            return SpringContextUtil.copyBean(source, tClass);
        }

        Client setServerName(String serverName) {
            this.serverName = serverName;
            return this;
        }

        public Client query(String mapperBeanName, QueryDto<?> query) {
            this.mapperBeanName = mapperBeanName;
            this.query = query;
            return this;
        }

        public TResult<Page<Object>> page() {
            return getJumperFeignClient().queryPage(this.mapperBeanName, this.query);
        }

        public <T> TResult<Page<T>> page(Class<T> tClass) {
            check();
            Objects.requireNonNull(tClass, "未指定接收实体类型");
            TResult<Page<Object>> body = getJumperFeignClient().queryPage(this.mapperBeanName, this.query);
            if (body.getData() != null && body.getData().getRecords() != null) {
                return TResult.success(QueryUtil.buildPage(body.getData(), i -> copyProperties(i, tClass)));
            }
            return TResult.build(body.getStatus(), body.getMessage());
        }
    }


    /**
     * 处理 IPage 接口接收返回值 feign 无法序列化的问题
     */
    private void init() {
        if (this.objectMapper == null) {
            this.objectMapper = JacksonObjectMapper.getObjectMapper();
            // 注入到容器
            SpringContextUtil.registerSingleton("jacksonObjectMapper", this.objectMapper);
        }

        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        simpleModule.addDeserializer(IPage.class, new StdDeserializer(IPage.class) {
            @Override
            public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
                JsonNode node = jsonParser.getCodec().readTree(jsonParser);
                String s = node.toString();
                ObjectMapper om = new ObjectMapper();
                return om.readValue(s, Page.class);
            }
        }); // 处理 IPage 接口接收返回值 feign 无法序列化的问题
        this.objectMapper.registerModule(simpleModule);
    }
}
