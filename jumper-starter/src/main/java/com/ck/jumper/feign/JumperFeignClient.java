package com.ck.jumper.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.tools.com.TResult;
import com.ck.jumper.utils.QueryDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface JumperFeignClient {
    @PostMapping("/jumper/page/{mapperBeanName}")
    <T> TResult<Page<T>> queryPage(@PathVariable(value = "mapperBeanName") String mapperBeanName, @RequestBody(required = false) QueryDto<?> dto);
}