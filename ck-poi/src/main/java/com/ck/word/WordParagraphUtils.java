package com.ck.word;

import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.impl.xb.xmlschema.SpaceAttribute;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public abstract class WordParagraphUtils {

    /**
     * 段落文本替换
     *
     * @param paragraphs
     * @param params
     */
    public static void paragraphReplace(List<XWPFParagraph> paragraphs, Map<String, String> params) {
        if (params != null && !params.isEmpty()) {
            for (XWPFParagraph p : paragraphs) {
                for (XWPFRun r : p.getRuns()) {
                    String content = r.getText(r.getTextPosition());
                    if (content != null && params.containsKey(content)) {
                        r.setText(content.replace("keyword", params.get(content)), 0);
                    }
                }
            }
        }
    }

    /**
     * 获取指定下标的段落，没有则向当前文档中追加新建一个段落并返回
     *
     * @param document
     * @param index
     * @return
     */
    public static XWPFParagraph getParagraph(XWPFDocument document, Integer index) {
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        if (index != null && paragraphs != null && paragraphs.size() > index) {
            return paragraphs.get(index);
        } else {
            return document.createParagraph();
        }
    }

    /**
     * 获取指定下标的段落，没有则向当前文档中追加新建一个段落并返回
     *
     * @param document           文档对象
     * @param index              段落下标
     * @param styleName          段落使用的样式名称
     * @param indentationLeft    左侧缩进数量
     * @param indentationHanging 行间距
     * @param paragraphAlignment 对齐方式,默认左对齐
     * @return
     */
    public static XWPFParagraph getParagraph(XWPFDocument document, Integer index, String styleName, Integer indentationLeft, Integer indentationHanging, ParagraphAlignment paragraphAlignment) {
        XWPFParagraph paragraph = getParagraph(document, index);
// 段落的格式,下面及个设置,将使新添加的文字向左对其,无缩进.
        paragraph.setIndentationLeft(indentationLeft == null ? 0 : indentationLeft);  // 段落左侧缩进

        paragraph.setIndentationHanging(indentationHanging == null ? 0 : indentationHanging);
        paragraph.setAlignment(paragraphAlignment == null ? ParagraphAlignment.LEFT : paragraphAlignment);
//         paragraph.setWordWrap( true );
        if (styleName != null && !"".equalsIgnoreCase(styleName))
            paragraph.setStyle(styleName);
        return paragraph;
    }


    /**
     * 获取文本域
     *
     * @param paragraph  段落
     * @param pos        段落中的位置 0：在段落的最开始位置插入
     * @param fontFamily 字体，默认："宋体"
     * @param fontSize   字体大小
     * @param isBold     字体是否加粗
     * @param fontColor  字体颜色，默认不设置
     */
    public static XWPFRun getXWPFRun(XWPFParagraph paragraph, int pos, String fontFamily, int fontSize, Boolean isBold, String fontColor) {
        // 在段落中新插入一个run,这里的run我理解就是一个word文档需要显示的个体,里面可以放文字,参数0代表在段落的最前面插入
        XWPFRun run = paragraph.insertNewRun(pos);
        // 设置run内容
        run.setFontFamily(fontFamily == null || "".equalsIgnoreCase(fontFamily.trim()) ? "宋体" : fontFamily);
        run.setBold(isBold == null ? false : isBold);
        run.setFontSize(fontSize);


        if (fontColor != null && !"".equalsIgnoreCase(fontColor)) {
            run.setColor(fontColor);
        }

        return run;
    }

    /**
     * 获取文本域并插入文本
     *
     * @param paragraph  段落
     * @param pos        段落中的位置 0：在段落的最开始位置插入
     * @param fontFamily 字体，默认："宋体"
     * @param fontSize   字体大小
     * @param isBold     字体是否加粗
     * @param fontColor  字体颜色，默认不设置
     * @param text       文本
     */
    public static XWPFRun getXWPFRun(XWPFParagraph paragraph, int pos, String fontFamily, int fontSize, Boolean isBold, String fontColor, String text) {
        XWPFRun run = getXWPFRun(paragraph, pos, fontFamily, fontSize, isBold, fontColor);
        run.setText(text, pos);
        return run;
    }

    /**
     * 段落打断，强制回车： ⬇
     *
     * @param run
     * @return
     */
    public static XWPFRun addBreak(XWPFRun run) {
        if (run != null) {
            // 手动回车符  ⬇
            run.addBreak(BreakType.TEXT_WRAPPING);
        }
        return run;
    }

    /**
     * 插入回车： \r，并返回回车后的XWPFRun
     *
     * @param doc
     * @return
     */
    public static XWPFRun addEnter(XWPFDocument doc, Integer indentation) {
        XWPFRun run = null;
        if (doc != null) {
            int bodyTextFontSize = 12;  // 文档字体
            XWPFParagraph paragraph = WordParagraphUtils.getParagraph(doc, null, null, indentation, 20, null);
            run = WordParagraphUtils.getXWPFRun(paragraph, 0, null, bodyTextFontSize, null, null);
            run.setText("\r");
        }
        return run;
    }

    /**
     * 设置页眉
     *
     * @param document
     * @param text     页眉文本
     */
    public static void setPageHeader(XWPFDocument document, String text) {
        CTP ctp = CTP.Factory.newInstance();
        XWPFParagraph paragraph = new XWPFParagraph(ctp, document);//段落对象
        ctp.addNewR().addNewT().setStringValue(text);//设置页眉参数
        ctp.addNewR().addNewT().setSpace(SpaceAttribute.Space.PRESERVE);
        CTSectPr sectPr = document.getDocument().getBody().isSetSectPr() ? document.getDocument().getBody().getSectPr() : document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFHeader header = policy.createHeader(STHdrFtr.DEFAULT, new XWPFParagraph[]{paragraph});
        header.setXWPFDocument(document);
    }


    /**
     * 生成页脚
     *
     * @param document
     * @param leftText1
     * @param leftText2
     * @throws Exception
     */
    public static void setPageFooter(XWPFDocument document, String leftText1, String leftText2) {
        /*
         * 生成页脚段落
         * 给段落设置宽度为占满一行
         * 为公司地址和公司电话左对齐，页码右对齐创造条件
         * */
        CTSectPr sectPr = document.getDocument().getBody().addNewSectPr();
        XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(document, sectPr);
        XWPFFooter footer = headerFooterPolicy.createFooter(STHdrFtr.DEFAULT);

        XWPFParagraph paragraph = footer.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.LEFT);
        paragraph.setVerticalAlignment(TextAlignment.CENTER);
        paragraph.setBorderTop(Borders.THICK);
        CTTabStop tabStop = paragraph.getCTP().getPPr().addNewTabs().addNewTab();
        tabStop.setVal(STTabJc.RIGHT);
        int twipsPerInch = 1440;
        tabStop.setPos(BigInteger.valueOf(6 * twipsPerInch));

        /*
         * 给段落创建元素
         * 设置元素字面为公司地址+公司电话
         * */
        XWPFRun run = paragraph.createRun();
        run.setText((leftText1 != null ? leftText1 : "") + (leftText2 != null ? "  " + leftText2 : ""));
        WordStyleUtils.setStyle(run, "仿宋", 10);
        run.addTab();

        /*
         * 生成页码
         * 页码右对齐
         * */
        run = paragraph.createRun();
        run.setText("第");
        WordStyleUtils.setStyle(run, "仿宋", 10);

        run = paragraph.createRun();
        CTFldChar fldChar = run.getCTR().addNewFldChar();
        fldChar.setFldCharType(STFldCharType.Enum.forString("begin"));

        run = paragraph.createRun();
        CTText ctText = run.getCTR().addNewInstrText();
        ctText.setStringValue("PAGE  \\* MERGEFORMAT");
        ctText.setSpace(SpaceAttribute.Space.Enum.forString("preserve"));
        WordStyleUtils.setStyle(run, "仿宋", 10);

        fldChar = run.getCTR().addNewFldChar();
        fldChar.setFldCharType(STFldCharType.Enum.forString("end"));

        run = paragraph.createRun();
        run.setText("页 总共");
        WordStyleUtils.setStyle(run, "仿宋", 10);

        run = paragraph.createRun();
        fldChar = run.getCTR().addNewFldChar();
        fldChar.setFldCharType(STFldCharType.Enum.forString("begin"));

        run = paragraph.createRun();
        ctText = run.getCTR().addNewInstrText();
        ctText.setStringValue("NUMPAGES  \\* MERGEFORMAT ");
        ctText.setSpace(SpaceAttribute.Space.Enum.forString("preserve"));
        WordStyleUtils.setStyle(run, "仿宋", 10);

        fldChar = run.getCTR().addNewFldChar();
        fldChar.setFldCharType(STFldCharType.Enum.forString("end"));

        run = paragraph.createRun();
        run.setText("页");
        WordStyleUtils.setStyle(run, "仿宋", 10);
    }

    /**
     * 设置当前文档最后的段落 对齐方式和左侧缩进
     *
     * @param doc
     * @param alignment       段落的对齐方式 1左 2中 3右 4往上 左 不可写0和负数
     * @param indentationLeft 整段缩进（右移）指定应为从左到右段
     * @return
     */
    public static XWPFParagraph setLastParagraph(XWPFDocument doc, Integer alignment, Integer indentationLeft) {
        XWPFParagraph lastParagraph = null;
        //获取所有的段
        List<XWPFParagraph> paragraphs = doc.getParagraphs();
        if (paragraphs != null && !paragraphs.isEmpty()) {
            lastParagraph = paragraphs.get(paragraphs.size() - 1);
            if (indentationLeft != null)
                lastParagraph.setIndentationLeft(indentationLeft);//---整段缩进（右移）指定应为从左到右段，该段的内容的左边的缘和这一段文字左边的距和右边文本边距和左段权中的那段文本的右边缘之间的缩进,如果省略此属性，则应假定其值为零
            if (alignment != null && alignment > 0)
                lastParagraph.setFontAlignment(1);//---段落的对齐方式 1左 2中 3右 4往上 左 不可写0和负数
        }
        return lastParagraph;
    }

    //setAlignment()指定应适用于此段落中的文本的段落对齐方式。CENTER LEFT...
    //p1.setAlignment(ParagraphAlignment.LEFT);
    //p1.setBorderBetween(Borders.APPLES);
    //p1.setBorderBottom(Borders.APPLES);
    //p1.setBorderLeft(Borders.APPLES);指定应显示在左边页面指定段周围的边界。
    //p1.setBorderRight(Borders.ARCHED_SCALLOPS);指定应显示在右侧的页面指定段周围的边界。
    //p1.setBorderTop(Borders.ARCHED_SCALLOPS);指定应显示上方一组有相同的一组段边界设置的段落的边界。这几个是对段落之间的格式的统一，相当于格式刷
    //p1.setFirstLineIndent(99);//---正文宽度会稍微变窄
    //p1.setFontAlignment(1);//---段落的对齐方式 1左 2中 3右 4往上 左 不可写0和负数
    //p1.setIndentationFirstLine(400);//---首行缩进,指定额外的缩进，应适用于父段的第一行。
    //p1.setIndentationHanging(400);//---首行前进,指定的缩进量，应通过第一行回到开始的文本流的方向上移动缩进从父段的第一行中删除。
    //p1.setIndentationLeft(400);//---整段缩进（右移）指定应为从左到右段，该段的内容的左边的缘和这一段文字左边的距和右边文本边距和左段权中的那段文本的右边缘之间的缩进,如果省略此属性，则应假定其值为零。
    //p1.setIndentationRight(400);//---指定应放置这一段，该段的内容从左到右段的右边缘的正确文本边距和右边文本边距和左段权中的那段文本的右边缘之间的缩进,如果省略此属性，则应假定其值为零。
    //p1.setIndentFromLeft(400);//---整段右移
    //p1.setIndentFromRight(400);
    //p1.setNumID(BigInteger.TEN);
    //p1.setPageBreak(true);//--指定当渲染此分页视图中的文档，这一段的内容都呈现在文档中的新页的开始。
    //p1.setSpacingAfter(6);//--指定应添加在文档中绝对单位这一段的最后一行之后的间距。
    // p1.setSpacingAfterLines(6);//--指定应添加在此线单位在文档中的段落的最后一行之后的间距。
    //p1.setSpacingBefore(6);//--指定应添加上面这一段文档中绝对单位中的第一行的间距。
    //p1.setSpacingBeforeLines(6);//--指定应添加在此线单位在文档中的段落的第一行之前的间距。
    //p1.setSpacingLineRule(LineSpacingRule.AT_LEAST);//--指定行之间的间距如何计算存储在行属性中。
    //p1.setStyle("");//--此方法提供了样式的段落，这非常有用.
    //p1.setVerticalAlignment(TextAlignment.CENTER);//---指定的文本的垂直对齐方式将应用于此段落中的文本
    //p1.setWordWrapped(true);//--此元素指定是否消费者应中断超过一行的文本范围，通过打破这个词 （打破人物等级） 的两行或通过移动到下一行 （在词汇层面上打破） 这个词的拉丁文字。
    // XWPFRun r1=p1.createRun();//p1.createRun()将一个新运行追加到这一段
    //setText(String value)或
    //setText(String value,int pos)
    //value - the literal text which shall be displayed in the document
    //pos - - position in the text array (NB: 0 based)
    //r1.setText(data);
    //r1.setTextPosition(20);//这个相当于设置行间距的，具体这个20是怎么算的，不清楚,此元素指定文本应为此运行在关系到周围非定位文本的默认基线升降的量。不是真正意义上的行间距
    //---This element specifies the amount by which text shall be ★raised or lowered★ for this run in relation to the default baseline of the surrounding non-positioned text.
    //r1.setStrike(true);//---设置删除线的,坑人!!!
    //r1.setStrikeThrough(true);---也是设置删除线，可能有细微的区别吧
    //r1.setEmbossed(true);---变的有重影（变黑了一点）
    //r1.setDoubleStrikethrough(true);---设置双删除线
    //r1.setColor("33CC00");//---设置字体颜色 ★
    //r1.setFontFamily("fantasy");
    //r1.setFontFamily("cursive");//---设置ASCII(0 - 127)字体样式
    //r1.setBold(jiacu);//---"加黑加粗"
    // r1.setFontSize(size);//---字体大小
    // r1.setImprinted(true);//感觉与setEmbossed(true)类似，有重影
    //r1.setItalic(true);//---文本会有倾斜，是一种字体？
    //r1.setShadow(true);//---文本会变粗有重影，与前面两个有重影效果的方法感觉没什么区别
    //r1.setSmallCaps(true);//---改变了  英文字母  的格式
    //r1.setSubscript(VerticalAlign.BASELINE);//---valign垂直对齐的
    //r1.setUnderline(UnderlinePatterns.DASH);//--填underline type设置下划线
    //document.createTable(2, 2);//--创建一个制定行列的表
    //document.enforceReadonlyProtection();//--强制执行制度保护
    //r1.setDocumentbackground(doc, "FDE9D9");//设置页面背景色
    //r1.testSetUnderLineStyle(doc);//设置下划线样式以及突出显示文本
    //r1.addNewPage(doc, BreakType.PAGE);
    //r1.testSetShdStyle(doc);//设置文字底纹
}
