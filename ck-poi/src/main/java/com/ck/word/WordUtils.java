package com.ck.word;


import org.apache.poi.ooxml.POIXMLDocument;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Word相关操作类<br>
 * 基于org.apache.poi的poi-excelant版本3.17。其他版本不适用
 *
 * @author cyk
 * @since 2020-08-18
 */
public abstract class WordUtils {

    /**
     * 创建Document 文档对象
     *
     * @return
     */
    public static XWPFDocument createDocument() {
        XWPFDocument document = new XWPFDocument();
        return document;
    }


    /**
     * 创建新表格,默认一行一列
     *
     * @param document
     * @return
     */
    public static XWPFTable createTable(XWPFDocument document) {
        if (document != null) {
            return document.createTable();
        }
        return null;
    }

    /**
     * 读取word文件
     *
     * @param filePath
     * @throws IOException
     */
    public static XWPFDocument openDocFile(String filePath) throws IOException {
        // 获得word的pack对象
        OPCPackage pack = POIXMLDocument.openPackage(filePath);
        // 获得XWPFDocument对象
        XWPFDocument doc = new XWPFDocument(pack);
        return doc;
    }


    /**
     * 文件导出到本地
     * @param doc
     * @param filePath
     * @param fileName
     */
    public static void export(POIXMLDocument doc, String filePath, String fileName){
        ByteArrayOutputStream data = new ByteArrayOutputStream();

        try {
            doc.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件导出到本地
     *
     * @param request
     * @param response
     * @param doc
     * @param fileName
     */
    public static void export(HttpServletRequest request, HttpServletResponse response, POIXMLDocument doc, String fileName){
        ByteArrayOutputStream data = new ByteArrayOutputStream();

        try {
            doc.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 回车  缩进500
     * @param doc
     */
    public static void addEnter(XWPFDocument doc){
        WordParagraphUtils.addEnter(doc,500);
    }

    /**
     * 文档主体 段落格式
     *
     * @param doc
     * @return
     */
    public static XWPFRun getBodyPXWPFRun(XWPFDocument doc, String... texts) {
        // 文档通用属性
        int bodyTextFontSize = 11;  // 文档字体
        int bodyIndentation = 500;  // 文档缩进
        return getBodyPXWPFRun(doc, bodyTextFontSize, bodyIndentation, 20, texts);
    }


    /**
     * 文档主体 段落格式
     *
     * @param doc
     * @param textFontSize       字体大小
     * @param indentation        缩进
     * @param indentationHanging 行高
     * @param texts              文本
     * @return
     */
    public static XWPFRun getBodyPXWPFRun(XWPFDocument doc, int textFontSize, int indentation, int indentationHanging, String... texts) {
        XWPFParagraph paragraph = WordParagraphUtils.getParagraph(doc, null, null, indentation, indentationHanging, null);
        XWPFRun run = WordParagraphUtils.getXWPFRun(paragraph, 0, null, textFontSize, null, null);
        setText(run, texts);
        return run;
    }

    /**
     * 标题1
     *
     * @param doc
     * @param texts 标题文本
     * @return
     */
    public static XWPFRun getTitle1PXWPFRun(XWPFDocument doc, String... texts) {
        return getTitlePXWPFRun(doc, 1, 20,null, ParagraphAlignment.CENTER, texts);
    }

    /**
     * 标题2
     *
     * @param doc
     * @param texts 标题文本
     * @return
     */
    public static XWPFRun getTitle2PXWPFRun(XWPFDocument doc, String... texts) {
        return getTitlePXWPFRun(doc, 2, 18,100,null, texts);
    }

    /**
     * 标题3
     *
     * @param doc
     * @param texts 标题文本
     * @return
     */
    public static XWPFRun getTitle3PXWPFRun(XWPFDocument doc, String... texts) {
        return getTitlePXWPFRun(doc, 3, 16,200,null, texts);
    }

    /**
     * 标题4
     *
     * @param doc
     * @param texts 标题文本
     * @return
     */
    public static XWPFRun getTitle4PXWPFRun(XWPFDocument doc, String... texts) {
        return getTitlePXWPFRun(doc, 4, 14,300,null, texts);
    }

    /**
     * 标题
     *
     * @param doc
     * @param titleLevel 标题等级
     * @param fontSize   字体大小
     * @param indentation  左侧缩进
     * @param paragraphAlignment  水平对齐
     * @param texts  标题文本
     * @return
     */
    public static XWPFRun getTitlePXWPFRun(XWPFDocument doc, int titleLevel, int fontSize, Integer indentation, ParagraphAlignment paragraphAlignment, String... texts) {
        // 2号标题样式
        String titleStyle = WordStyleUtils.getTitleStyle(doc, titleLevel);

        XWPFParagraph paragraph = WordParagraphUtils.getParagraph(doc, null, titleStyle, indentation, null, paragraphAlignment);
        XWPFRun run = WordParagraphUtils.getXWPFRun(paragraph, 0, null, fontSize, null, null);
        setText(run, texts);
        return run;
    }

    /**
     * 段落中写入文本
     *
     * @param run
     * @param texts
     */
    public static void setText(XWPFRun run, String... texts) {
        // 如果有文本参数则直接写入
        if (texts != null && texts.length > 0) {
            StringBuffer text = new StringBuffer();
            for (String t : texts) {
                text.append(t);
            }
            run.setText(text.toString());
        }
    }

}
