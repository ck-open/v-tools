package com.ck.word;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Word中表格相关操作类<br>
 * 基于org.apache.poi的poi-excelant版本3.17。其他版本不适用
 *
 * @author cyk
 * @since 2020-08-18
 */
public abstract class WordTableUtils {
    /**
     * 创建表格对象
     *
     * @param document  文档
     * @param rowSize 行数
     * @param columnSize  列数
     * @param width  表格总宽度
     * @return 创建表格, 创建后表格至少有1行1列, 设置列宽
     * @param enumValue  数据对齐方式
     * @return
     */
    public static XWPFTable createTable(XWPFDocument document, int rowSize, int columnSize, int width, STJc.Enum enumValue) {
        XWPFTable table = createTable(document,rowSize,columnSize);
        return setTableWidthAndHAlign(table,String.valueOf(width),enumValue);
    }

    /**
     *  创建表格对象
     *
     * @param xdoc  文档
     * @param rowSize 行数
     * @param columnSize  列数
     * @param columnWidths  列宽，null 则不设置，否则按数组一次排列设置
     * @return 创建表格, 创建后表格至少有1行1列, 设置列宽
     */
    public static XWPFTable createTable(XWPFDocument xdoc, int rowSize, int columnSize, int... columnWidths) {
        XWPFTable table = xdoc.createTable(rowSize, columnSize);
        if (columnWidths != null && columnWidths.length > 0) {
            int width = 0;
            for (int item:columnWidths){
                width+=item;
            }
            setTableWidthAndHAlign(table,String.valueOf(width), STJc.CENTER);
            setColWidth(table, columnSize, columnWidths);
        }
        return table;
    }

    /**
     * 设置表格列宽
     *
     * @param table
     * @param columnSize
     * @param columnWidths
     * @return
     */
    public static XWPFTable setColWidth(XWPFTable table, int columnSize, int... columnWidths) {
        if (table != null && columnWidths != null && columnWidths.length > 0){
            CTTbl ttbl = table.getCTTbl();
            CTTblGrid tblGrid = ttbl.addNewTblGrid();
            for (int j = 0, len = Math.min(columnSize, columnWidths.length); j < len; j++) {
                CTTblGridCol gridCol = tblGrid.addNewGridCol();
                gridCol.setW(new BigInteger(String.valueOf(columnWidths[j])));
            }
        }
        return table;
    }


    /**
     * 设置表格总宽度与水平对齐方式
     */
    public static XWPFTable setTableWidthAndHAlign(XWPFTable table, String width, STJc.Enum enumValue) {
        setTableWidth(table,width);
        setTableHAlign(table,enumValue);
        return table;
    }

    /**
     * 设置表格总宽度
     */
    public static XWPFTable setTableWidth(XWPFTable table, String width) {
        CTTblPr tblPr = getTableCTTblPr(table);
        // 表格宽度
        CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr.addNewTblW();
        // 设置宽度
        tblWidth.setW(new BigInteger(width));
        tblWidth.setType(STTblWidth.DXA);
        return table;
    }

    /**
     * 设置表格对齐方式
     */
    public static XWPFTable setTableHAlign(XWPFTable table, STJc.Enum enumValue) {
        CTTblPr tblPr = getTableCTTblPr(table);
        // 表格对齐方式
        if (enumValue != null) {
            CTJc cTJc = tblPr.addNewJc();
            cTJc.setVal(enumValue);
        }
        return table;
    }

    /**
     * 得到Table的CTTblPr, 不存在则新建
     */
    public static CTTblPr getTableCTTblPr(XWPFTable table) {
        CTTbl ttbl = table.getCTTbl();
        // 表格属性
        CTTblPr tblPr = ttbl.getTblPr() == null ? ttbl.addNewTblPr() : ttbl.getTblPr();
        return tblPr;
    }

    /**
     * 设置表格行高
     *
     * @param table
     * @param heigth    高度
     * @param vertical  表格内容的显示方式：居中、靠右...
     */
    public static void setRowHeight(XWPFTable table, int heigth, STVerticalJc.Enum vertical) {
        List<XWPFTableRow> rows = table.getRows();
        for (XWPFTableRow row : rows) {
            CTTrPr trPr = row.getCtRow().addNewTrPr();
            CTHeight ht = trPr.addNewTrHeight();
            ht.setVal(BigInteger.valueOf(heigth));
            List<XWPFTableCell> cells = row.getTableCells();
            for (XWPFTableCell tableCell : cells) {
                CTTcPr cttcpr = tableCell.getCTTc().addNewTcPr();
                cttcpr.addNewVAlign().setVal(vertical);
            }
        }
    }


    /**
     * 跨列 水平合并
     * @param table
     * @param row    所合并的行
     * @param fromCell    起始列
     * @param toCell    终止列
     * @Description
     */
    public static void mergeCellsHorizontal(XWPFTable table, int row, int fromCell, int toCell) {
        for(int cellIndex = fromCell; cellIndex <= toCell; cellIndex++ ) {
            XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
            if(cellIndex == fromCell) {
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.RESTART);
            } else {
                cell.getCTTc().addNewTcPr().addNewHMerge().setVal(STMerge.CONTINUE);
            }
        }
    }
    /**
     * 跨行 垂直合并
     * @param table
     * @param col    合并的列
     * @param fromRow    起始行
     * @param toRow    终止行
     */
    public static void mergeCellsVertically(XWPFTable table, int col, int fromRow, int toRow) {
        for(int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
            XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
            //第一个合并单元格用重启合并值设置
            if(rowIndex == fromRow) {
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            } else {
                //合并第一个单元格的单元被设置为“继续”
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
            }
        }
    }

    /**
     * 合并单元格
     * @param table
     * @param startRow    合并的开始行
     * @param endRow    合并的结束行
     * @param startCol    合并的开始列
     * @param endCol    合并的结束列
     */
    public static void mergeCells(XWPFTable table, int startRow, int endRow, int startCol, int endCol) {
        mergeCellsVertically(table,startCol,startRow,endRow);
        for(int rowIndex = startRow;rowIndex<=endRow;rowIndex++) {
            mergeCellsHorizontal(table, rowIndex, startCol, endCol);
        }
    }

    /**
     * 向行中设置值
     * @param table
     * @param row
     * @param values  行数据
     * @return
     */
    public static XWPFTable setValue(XWPFTable table, int row, Object... values){
        if (values!=null){
            for (int i=0;i<values.length;i++){
                setValue(table,row,i,values[i]);
            }
        }
        return table;
    }

    /**
     * 向单元格中设置值
     * @param table
     * @param row
     * @param col
     * @param value 字符串值或者段落对象 （XWPFParagraph）
     * @return
     */
    public static XWPFTable setValue(XWPFTable table, int row, int col, Object value){
        if (table!=null && value!=null) {
            if (table.getNumberOfRows() <= row) {
                throw new RuntimeException("==> 表格值设置失败 Table set value Exception: row Index Out of Bounds！");
            }
            XWPFTableRow tableRow = table.getRow(row);

            XWPFTableCell cell = tableRow.getCell(col);
            if (cell== null) {
                cell = tableRow.createCell();
//                throw new RuntimeException("==> 表格值设置失败 Table set value Exception: Cell is null Or Index Out of Bounds！");
            }

            if (value instanceof XWPFParagraph){
                cell.setParagraph((XWPFParagraph) value);
            }else {
                cell.setText(value.toString());
            }
        }

        return table;
    }













    /**
     * 删除指定位置的表格，被删除表格后的索引位置
     * @param document
     * @param pos
     */
    public static void deleteTableByIndex(XWPFDocument document, int pos) {
        Iterator<IBodyElement> bodyElement = document.getBodyElementsIterator();
        int eIndex = 0, tableIndex = -1;
        while(bodyElement.hasNext()) {
            IBodyElement element = bodyElement.next();
            BodyElementType elementType = element.getElementType();
            if(elementType == BodyElementType.TABLE) {
                tableIndex++;
                if(tableIndex == pos) {
                    break;
                }
            }
            eIndex++;
        }
        document.removeBodyElement(eIndex);
    }

    /**
     * 表格文本替换
     * @param doc
     * @param params
     */
    public static void tableTextReplace(XWPFDocument doc, Map<String, String> params){
        for (XWPFTable table : doc.getTables()) {
            for (XWPFTableRow row : table.getRows()) {
                for (XWPFTableCell cell : row.getTableCells()) {
                    WordParagraphUtils.paragraphReplace(cell.getParagraphs(), params);
                }
            }
        }
    }
}
