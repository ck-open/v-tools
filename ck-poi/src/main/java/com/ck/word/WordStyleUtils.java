package com.ck.word;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;

/**
 * Word中样式相关操作类<br>
 * 基于org.apache.poi的poi-excelant版本3.17。其他版本不适用
 *
 * @author cyk
 * @since 2020-08-18
 */
public abstract class WordStyleUtils {

    /**
     * 设置文本样式
     * @param run
     * @param fontFamily  字体
     * @param fontSize
     */
    public static XWPFRun setStyle(XWPFRun run, String fontFamily, int fontSize){
        run.setFontFamily(fontFamily);
        run.setFontSize(fontSize);
        return run;
    }

    /**
     * 获取样式对象 XWPFStyle，样式不存在则创建
     * @param document
     * @param styleId  样式ID
     * @return
     */
    public static XWPFStyle getStyle(XWPFDocument document, String styleId) {
        // 当前文档样式池
        XWPFStyles styles = document.createStyles();

        // 样式不存在则创建
        if (styles.getStyle(styleId) == null) {
            CTStyle ctStyle = CTStyle.Factory.newInstance();
            ctStyle.setStyleId(styleId);

            // 设置样式名称
            CTString styleName = CTString.Factory.newInstance();
            styleName.setVal(styleId);
            ctStyle.setName(styleName);

            // 样式显示在格式栏中 style shows up in the formats bar
            CTOnOff onoffnull = CTOnOff.Factory.newInstance();
            ctStyle.setUnhideWhenUsed(onoffnull);
            ctStyle.setQFormat(onoffnull);

            XWPFStyle style = new XWPFStyle(ctStyle);
            style.setType(STStyleType.PARAGRAPH);
            styles.addStyle(style);
        }

        return styles.getStyle(styleId);
    }

    /**
     * 创建标题样式
     *
     * @param document
     * @param titleLevel 标题等级
     */
    public static String getTitleStyle(XWPFDocument document, int titleLevel) {
        // 标题名称：标题 1、标题 2
        String strStyleId = "标题 " + titleLevel;

        CTStyle ctStyle = getStyle(document, strStyleId).getCTStyle();

        // lower number > style is more prominent in the formats bar
        CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
        indentNumber.setVal(BigInteger.valueOf(titleLevel));
        ctStyle.setUiPriority(indentNumber);

        // 定义给定级别的标题 style defines a heading of the given level
        CTPPr ppr = CTPPr.Factory.newInstance();
        ppr.setOutlineLvl(indentNumber);
        ctStyle.setPPr(ppr);

        return strStyleId;
    }
}
