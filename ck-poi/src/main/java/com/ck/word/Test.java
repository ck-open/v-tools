package com.ck.word;

import org.apache.poi.xwpf.usermodel.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) throws Exception {
        WordText();
    }

    public static void WordText() throws Exception {
        String filePath = "C:\\Users\\cyk\\Desktop";
        String fileName = "测试Word.docx";

        // 获得XWPFDocument对象
        XWPFDocument doc = WordUtils.createDocument();
//        WordParagraphUtils.setPageHeader(doc,"这是个页眉");
        WordParagraphUtils.setPageFooter(doc, "123456789", "公司地址");


        // 大标题
        XWPFRun run = WordUtils.getTitle1PXWPFRun(doc, "电解系统能效分析报告");


        // 时间版本
        XWPFParagraph paragraph = WordParagraphUtils.getParagraph(doc, null, null, null, 20, ParagraphAlignment.RIGHT);
        WordParagraphUtils.getXWPFRun(paragraph, 0, null, 10, null, null, "2020-08-18");


        // 报告总览
        WordUtils.getTitle2PXWPFRun(doc, "报告总览");

        run = WordUtils.getBodyPXWPFRun(doc);
        run.setText("电解系统本期总能耗：61524.22kgce  去年同期：123312.kgce  同比：12% ");
        run.setText("槽电压同期增长10%，能效同比降低3%");


        // 能耗分析
        WordUtils.getTitle2PXWPFRun(doc, "能耗分析");

//        XWPFTable table = WordTableUtils.createTable(doc,5,6,8000,STJc.RIGHT);
        XWPFTable table = WordTableUtils.createTable(doc, 5, 4, 1000, 1000, 1000, 2000);
        WordTableUtils.mergeCells(table, 0, 1, 0, 3);
        WordTableUtils.setValue(table, 0, 0, "测试数据");


        // 趋势分析
        WordUtils.getTitle2PXWPFRun(doc, "趋势分析");
        run = WordUtils.getBodyPXWPFRun(doc, "能效数据趋势分析如下");


        // 图表测试
        List<String> legends = Arrays.asList("日期", "当前", "历史","空值");
        List<List> values = new ArrayList<>();
        values.add(Arrays.asList("2020-01", "2020-02", "2020-03", "2020-04", "2020-05", "2020-06", "2020-07", "2020-08", "2020-09"));
        for (int r=0;r<4;r++){
            List val = new ArrayList();
            for (int c=0;c<9;c++){
                val.add(Math.random()*100+1);
            }
            values.add(val);
        }

        int indentationLeft = 500; // 图表缩进

//        XWPFChart chart = ChartUtils.createChartLine(doc, "这是个折线图-折线",indentationLeft,500,200,null, legends, values);


//        ChartUtils.createChartLine(doc, "这是个折线图-弧度",indentationLeft,500,200,true, legends, values);
//        ChartUtils.createChartBar(doc,"这是一个柱状图",indentationLeft,500,200,false,legends,values);
//        ChartUtils.createChartBar(doc,"这是一个条状图",indentationLeft,500,200,true,legends,values);
//
//        ChartUtils.createChartRadar(doc,"这是一个条状图",indentationLeft,500,200,true,legends,values);
//        ChartUtils.createChartScatter(doc,"这是一个散点状图",indentationLeft,500,200,true,legends,values);



        // 原因分析
        WordUtils.getTitle2PXWPFRun(doc, "原因分析");
//        ChartUtils.createChartPie(doc, "这是一个饼图",indentationLeft,500,200, values);

        // 改进意见
        WordUtils.getTitle2PXWPFRun(doc, "改进意见");


        // 文档导出
        WordUtils.export(doc, filePath, fileName);


//        ByteArrayOutputStream pdf = PDFUtils.wordToPDF(doc);
//        FileIOUtils.export(pdf,filePath,"测试.pdf");
//        ByteArrayOutputStream pdf = PDFUtils.excelToPDF(chart.getWorkbook(),ChartUtils.SHEET_NAME_DEFAULT);
//        FileIOUtils.export(pdf,filePath,"测试.pdf");
    }

}
