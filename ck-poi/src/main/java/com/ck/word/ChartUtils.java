package com.ck.word;

import com.ck.exception.OfficeException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFChart;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.util.List;

/**
 * Word中图表相关操作类<br>
 * 基于org.apache.poi的poi-excelant版本3.17。其他版本不适用
 *
 * @author cyk
 * @since 2020-08-18
 */
public abstract class ChartUtils {
    /**
     * 创建图表对象
     *
     * @param doc
     * @return
     */
    public static XWPFChart createChart(XWPFDocument doc) {
        try {
            return doc.createChart();
        } catch (Exception e) {
            throw new OfficeException("Word 图表创建失败：" + e.getMessage());
        }
    }


    /**
     * 创建散点图
     *
     * @param doc
     * @param chartTitle
     * @param indentationLeft 左侧缩进
     * @param isRadian        是否弧度曲线
     * @param width           宽
     * @param height          高
     * @param legends         图例名称列表
     * @param values
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static XWPFChart createChartScatter(XWPFDocument doc, String chartTitle, Integer indentationLeft, Integer width, Integer height, Boolean isRadian, List<String> legends, List<List> values) {
        // 创建图表
        XWPFChart chart = createChart(doc);

        XDDFChartData data = setChartData(chart, ChartTypes.SCATTER, chartTitle, width, height, null, null, legends, values);

        chart.plot(data);
        WordParagraphUtils.setLastParagraph(doc, 2, indentationLeft);
        return chart;

    }

    /**
     * 创建雷达图
     *
     * @param doc
     * @param chartTitle
     * @param indentationLeft 左侧缩进
     * @param isRadian        是否弧度曲线
     * @param width           宽
     * @param height          高
     * @param legends         图例名称列表
     * @param values
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static XWPFChart createChartRadar(XWPFDocument doc, String chartTitle, Integer indentationLeft, Integer width, Integer height, Boolean isRadian, List<String> legends, List<List> values) {
        // 创建图表
        XWPFChart chart = createChart(doc);

        XDDFChartData data = setChartData(chart, ChartTypes.RADAR, chartTitle, width, height, null, null, legends, values);

        chart.plot(data);
        WordParagraphUtils.setLastParagraph(doc, 2, indentationLeft);
        return chart;
    }


    /**
     * 创建折线图
     *
     * @param doc
     * @param chartTitle
     * @param indentationLeft 左侧缩进
     * @param isRadian        是否弧度曲线
     * @param width           宽
     * @param height          高
     * @param legends         图例名称列表
     * @param values
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static XWPFChart createChartLine(XWPFDocument doc, String chartTitle, Integer indentationLeft, Integer width, Integer height, Boolean isRadian, List<String> legends, List<List> values) {
        // 创建图表
        XWPFChart chart = createChart(doc);

        XDDFChartData data = setChartData(chart, ChartTypes.LINE, chartTitle, width, height, null, null, legends, values);

        //line  折线图 样式
        if (isRadian == null || !isRadian)
            if (data instanceof XDDFLineChartData) {
                for (XDDFChartData.Series series : data.getSeries()) {
                    XDDFLineChartData.Series seriesItem = (XDDFLineChartData.Series) series;
                    seriesItem.setSmooth(false);
                    seriesItem.setMarkerStyle(MarkerStyle.DOT);
                }
            }

        chart.plot(data);
        WordParagraphUtils.setLastParagraph(doc, 2, indentationLeft);
        return chart;
    }

    /**
     * 创建柱状图
     *
     * @param doc
     * @param chartTitle
     * @param indentationLeft 左侧缩进
     * @param width           宽
     * @param height          高
     * @param isHorizontal    条形方向是否时横向（水平方向）
     * @param legends
     * @param values
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static XWPFChart createChartBar(XWPFDocument doc, String chartTitle, Integer indentationLeft, Integer width, Integer height, Boolean isHorizontal, List<String> legends, List<List> values) {

        // 创建图表
        XWPFChart chart = createChart(doc);

        XDDFChartData data = setChartData(chart, ChartTypes.BAR, chartTitle, width, height, null, null, legends, values);

        //bar  柱状图
        if (isHorizontal == null || !isHorizontal)
            if (data instanceof XDDFBarChartData) {
                // 设置图表方向，由横向条状改为纵向柱状
                XDDFBarChartData bar = (XDDFBarChartData) data;
                bar.setBarDirection(BarDirection.COL);
            }

        chart.plot(data);
        WordParagraphUtils.setLastParagraph(doc, 2, indentationLeft);
        return chart;
    }


//    /**
//     * 创建饼图
//     *
//     * @param doc
//     * @param chartTitle      图标题
//     * @param chartTitle
//     * @param indentationLeft 左侧缩进
//     * @param width           宽
//     * @param height          高
//     * @param values          数据，第一行为图例列表
//     * @throws IOException
//     * @throws InvalidFormatException
//     */
//    public static XWPFChart createChartPie(XWPFDocument doc, String chartTitle, Integer indentationLeft, Integer width, Integer height, List<List> values) {
//        // 创建图表
//        XWPFChart chart = createChart(doc);
//
//        // 设置标题和宽高
//        setTitleAndWidthAndHeart(chart, chartTitle, width, height);
//
//        // excel 数据源 sheet
//        XSSFSheet sheet = setExcelValue(chart, values);
//
//        // 图例位置
//        XDDFChartLegend legend = chart.getOrAddLegend();
//        legend.setPosition(LegendPosition.TOP_RIGHT);
//
//        XDDFChartData data = new XDDFPieChartData(chart.getCTChart().getPlotArea().addNewPieChart());
//        setSeries(data, sheet);
//
//        data.setVaryColors(true);
//
//        chart.plot(data);
//        WordParagraphUtils.setLastParagraph(doc, 2, indentationLeft);
//        return chart;
//    }


    /**
     * 向内置Excel中赋值
     *
     * @param chart
     * @param chartType  图表类型
     * @param chartTitle 图表名称
     * @param width      宽
     * @param height     高
     * @param xAxisTitle X轴名称
     * @param yAxisTitle Y轴名称
     * @param legends    图例列表
     * @param values     数据列表，每个子列表为一条线
     * @return
     * @throws IOException
     * @throws InvalidFormatException
     */
    private static XDDFChartData setChartData(XWPFChart chart, ChartTypes chartType, String chartTitle, Integer width, Integer height, String xAxisTitle, String yAxisTitle, List<String> legends, List<List> values) {

        // 设置标题和宽高
        setTitleAndWidthAndHeart(chart, chartTitle, width, height);

        // 设置内置Excel数据值
        XSSFSheet sheet = setExcelValue(chart, values);

        // 创建图表内置数据对象
        XDDFChartData data = getChartData(chart, chartType, xAxisTitle, yAxisTitle);
        data.setVaryColors(true);

        setSeries(data, sheet);

        // 控制图例，只有一条数据时，图例显示异常，所以控制一条数据不显示图例
        if (values.size() > 2) {
            XDDFChartLegend legend = chart.getOrAddLegend();
            legend.setPosition(LegendPosition.BOTTOM);  // 图例位置
            setLegends(data, legends);
        }

        return data;
    }

    /**
     * 设置图表标题和宽高
     *
     * @param chart
     * @param chartTitle
     * @param width
     * @param height
     */
    public static void setTitleAndWidthAndHeart(XWPFChart chart, String chartTitle, Integer width, Integer height) {
        if (width != null) {
            chart.setChartWidth(width * 10000);
        }
        if (height != null) {
            chart.setChartHeight(height * 10000);
        }

        if (chartTitle != null)
            chart.setTitleText(chartTitle);  // 图名称
        chart.setTitleOverlay(false);
    }

    /**
     * 设置图表元素数据
     *
     * @param data
     * @param sheet
     */
    public static void setSeries(XDDFChartData data, XSSFSheet sheet) {
        // X坐标数据在内置表格中的位置
        int valueSize = sheet.getRow(sheet.getFirstRowNum()).getLastCellNum() - 1;
        XDDFDataSource<String> cat = XDDFDataSourcesFactory.fromStringCellRange(sheet,
                new CellRangeAddress(0, 0, 0, valueSize));

        // 遍历生成图表数据
        for (int i = sheet.getFirstRowNum() + 1; i <= sheet.getLastRowNum(); i++) {
            XDDFNumericalDataSource<Double> val = XDDFDataSourcesFactory.fromNumericCellRange(sheet,
                    new CellRangeAddress(i, i, 0, valueSize));

            XDDFChartData.Series series = data.addSeries(cat, val);
//            series.setShowLeaderLines(true);  // 是否显示标签数据，但是会连带图例一起显示在图表上，如果为false 图例会发生混乱 所以只能注掉 或不用

        }
    }


    /**
     * 设置数据图例
     *
     * @param data
     * @param legends
     */
    public static void setLegends(XDDFChartData data, List<String> legends) {
        int index = 0;

        for (XDDFChartData.Series series : data.getSeries()) {
            // 设置每条线的图例
            if (legends != null && legends.size() > index && legends.get(index) != null) {
                series.setTitle(legends.get(index), null);
            }
            index++;
        }
    }

    /**
     * 获取ChartData 对象
     *
     * @param chart
     * @param chartType
     * @param xAxisTitle
     * @param yAxisTitle
     * @return
     */
    private static XDDFChartData getChartData(XWPFChart chart, ChartTypes chartType, String xAxisTitle, String yAxisTitle) {
        // Use a category axis for the bottom axis.  为底部轴使用类别轴
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        if (xAxisTitle != null)
            bottomAxis.setTitle(xAxisTitle);  // x轴标识

        // 数据Y轴
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        if (yAxisTitle != null)
            leftAxis.setTitle(yAxisTitle);  // y轴标识
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);  // 十字轴样式
        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);

        return chart.createData(chartType, bottomAxis, leftAxis);
    }

    /**
     * 设置内置单元格数据并返回sheet<br>
     * 每个图表都会内嵌一个Excel文件
     *
     * @param chart
     * @param values
     * @return
     */
    public static final String SHEET_NAME_DEFAULT = "chartValue";

    private static XSSFSheet setExcelValue(XWPFChart chart, List<List> values) {
        String sheetName = SHEET_NAME_DEFAULT;

        XSSFWorkbook workbook = null;
        try {
            workbook = chart.getWorkbook();
        } catch (Exception e) {
            throw new OfficeException("Word 图表内嵌Excel表格获取失败: " + e.getMessage());
        }

        XSSFSheet sheet = workbook.createSheet(sheetName);

        for (int r = 0; r < values.size(); r++) {
            Row row = sheet.createRow(r);
            for (int c = 0; c < values.get(r).size(); c++) {
//                VResolverWrite.setValByType(row.createCell(c), values.get(r).get(c));
            }
        }
        return sheet;
    }

}
