package com.ck.exception;


/**
 * Excel Exception
 *
 * @author cyk
 * @since 2020-01-01
 *
 */
public class OfficeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public OfficeException() {
		super();
	}

	public OfficeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public OfficeException(String message, Throwable cause) {
		super(message, cause);
	}

	public OfficeException(String message) {
		super(message);
	}

	public OfficeException(Throwable cause) {
		super(cause);
	}

}