package com.ck.pdf;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDBorderStyleDictionary;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.text.PDFTextStripper;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.Sides;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class PDFBoxTest {

    private static final URL FontPath = PDFBoxTest.class.getClassLoader().getResource("SourceHanSansCN-Regular.ttf");

    private static final String testFilePath = "C:\\Users\\cyk\\Desktop\\";
    private static final Path testFile = Paths.get(testFilePath, "testPDFBox.pdf");


    public static void main(String[] args) {
        testPDFBox();
        testPDFBoxRead();

    }


    /**
     * 基本操作
     */
    public static void testPDFBoxRead() {
        try (PDDocument document = PDDocument.load(testFile.toFile())) {
            // 创建一个 PDFTextStripper 对象，用于提取文本
            PDFTextStripper pdfStripper = new PDFTextStripper();

            // 提取文本内容
            String text = pdfStripper.getText(document);

            // 打印提取的文本
            System.out.println(text);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * 基本操作
     */
    public static void testPDFBox() {
        try (PDDocument document = new PDDocument()) {
            // 加载支持中文的字体
            PDFont baseFont = FontPath == null ? PDType1Font.HELVETICA_BOLD : PDType0Font.load(document, FontPath.openStream());


            // 创建一个新的 A4 页面
            PDPage page = new PDPage(PDRectangle.A4);
            document.addPage(page);

            // 创建一个内容流对象，用于在页面上绘制内容
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            // 设置字体和字号
            contentStream.setFont(baseFont, 12);

            // 开始文本块
            contentStream.beginText();

            // 设置文本的起始位置
            contentStream.newLineAtOffset(100, 700);

            // 显示文本
            contentStream.showText("Hello, World!  这是使用PDFBox生成的文件");

            // 结束文本块
            contentStream.endText();

            // 关闭内容流
            contentStream.close();

            // 保存 PDF 文件
            document.save(testFile.toFile());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 添加图像到 PDF 文件
     */
    public static void testPDFBoxAddImage() {
        try (PDDocument document = new PDDocument()) {
            // 创建一个新的 A4 页面
            PDPage page = new PDPage(PDRectangle.A4);
            document.addPage(page);

            // 创建一个内容流对象，用于在页面上绘制内容
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            // 加载图像文件
            PDImageXObject pdImage = PDImageXObject.createFromFile("image.jpg", document);

            // 在指定位置绘制图像
            contentStream.drawImage(pdImage, 100, 600, pdImage.getWidth() / 2, pdImage.getHeight() / 2);

            // 关闭内容流
            contentStream.close();

            // 保存 PDF 文件
            document.save(testFile.toFile());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 添加链接到 PDF 文件
     */
    public static void testPDFBoxAddLink() {
        try (PDDocument document = new PDDocument()) {
            // 创建一个新的 A4 页面
            PDPage page = new PDPage(PDRectangle.A4);
            document.addPage(page);

            // 创建一个内容流对象，用于在页面上绘制内容
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            // 设置字体和字号
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);

            // 开始文本块
            contentStream.beginText();

            // 设置文本的起始位置
            contentStream.newLineAtOffset(100, 700);

            // 显示文本
            contentStream.showText("Visit Apache PDFBox");

            // 结束文本块
            contentStream.endText();

            // 关闭内容流
            contentStream.close();

            // 创建一个链接注释
            PDAnnotationLink link = new PDAnnotationLink();

            // 设置链接的边界
            link.setRectangle(new PDRectangle(100, 700, 150, 12));

            // 设置链接的边框样式
            PDBorderStyleDictionary bs = new PDBorderStyleDictionary();
            bs.setWidth(1);
            link.setBorderStyle(bs);

            // 创建一个 URI 动作
            PDActionURI action = new PDActionURI();
            action.setURI("https://pdfbox.apache.org/");

            // 将动作设置到链接注释
            link.setAction(action);

            // 将链接注释添加到页面
            page.getAnnotations().add(link);

            // 保存 PDF 文件
            document.save(testFile.toFile());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 添加水印到 PDF 文件
     */
    public static void testPDFBoxAddWatermark() {
        try (PDDocument document = new PDDocument()) {
            for (PDPage page : document.getPages()) {
                // 创建一个内容流对象，用于在页面上绘制内容
                PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true);

                // 设置字体和字号
                contentStream.setFont(PDType1Font.HELVETICA_BOLD, 60);

                // 设置透明度
                contentStream.setNonStrokingColor(0f, 0f, 0f, 0.2f);

                // 开始文本块

                contentStream.beginText();

                // 设置文本的起始位置
                contentStream.newLineAtOffset(page.getMediaBox().getWidth() / 2, page.getMediaBox().getHeight() / 2);

                // 设置文本旋转角度
                contentStream.setTextRotation(Math.PI / 4, page.getMediaBox().getWidth() / 2, page.getMediaBox().getHeight() / 2);

                // 显示文本
                contentStream.showText("CONFIDENTIAL");

                // 结束文本块
                contentStream.endText();

                // 关闭内容流
                contentStream.close();
            }

            // 保存 PDF 文件
            document.save(testFile.toFile());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 合并多个 PDF 文件
     */
    public static void testPDFBoxMerge() {
        try {
            PDFMergerUtility pdfMergerUtility = new PDFMergerUtility();

            // 添加要合并的 PDF 文件
            pdfMergerUtility.addSource(new File("file1.pdf"));
            pdfMergerUtility.addSource(new File("file2.pdf"));

            // 设置输出文件
            pdfMergerUtility.setDestinationFileName("merged.pdf");

            // 执行合并操作
            pdfMergerUtility.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 分割 PDF 文件
     */
    public static void testPDFBoxSplit() {
        try (PDDocument document = PDDocument.load(new File("example.pdf"))) {
            // 创建一个 Splitter 对象
            Splitter splitter = new Splitter();

            // 分割 PDF 文件
            List<PDDocument> pages = splitter.split(document);

            // 保存每个页面为单独的 PDF 文件
            int i = 1;
            for (PDDocument pd : pages) {
                pd.save("page" + i++ + ".pdf");
                pd.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 加密 PDF 文件
     */
    public static void testPDFBoxEncrypt() {
        try (PDDocument document = PDDocument.load(new File("example.pdf"))) {
            // 创建一个 StandardProtectionPolicy 对象
            StandardProtectionPolicy protectionPolicy = new StandardProtectionPolicy("ownerPassword", "userPassword", null);

            // 设置加密权限
            protectionPolicy.setEncryptionKeyLength(128);
            protectionPolicy.setPermissions(AccessPermission.getOwnerAccessPermission());

            // 应用加密策略
            document.protect(protectionPolicy);

            // 保存加密后的 PDF 文件
            document.save("encrypted.pdf");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 解密 PDF 文件
     */
    public static void testPDFBoxDecrypt() {
        try (PDDocument document = PDDocument.load(new File("encrypted.pdf"), "userPassword")) {
            // 移除加密
            document.setAllSecurityToBeRemoved(true);

            // 保存解密后的 PDF 文件
            document.save("decrypted.pdf");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 基本操作 解密 PDF 文件
     */
    public static void testPDFBoxPrint() {
        try (PDDocument document = PDDocument.load(new File("example.pdf"))) {
            // 获取默认打印服务
            PrintService printService = PrintServiceLookup.lookupDefaultPrintService();

            // 创建 PDFPageable 对象
            PDFPageable pageable = new PDFPageable(document);

            // 创建打印作业
            DocPrintJob printJob = printService.createPrintJob();

            // 创建打印请求属性集
            PrintRequestAttributeSet attributes = new HashPrintRequestAttributeSet();
            attributes.add(new Copies(1));
            attributes.add(Sides.ONE_SIDED);
            attributes.add(OrientationRequested.PORTRAIT);
            attributes.add(MediaSizeName.ISO_A4);

            // 创建 Doc 对象
            Doc doc = new SimpleDoc(pageable, DocFlavor.SERVICE_FORMATTED.PAGEABLE, null);

            // 打印 PDF 文件
            printJob.print(doc, attributes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
