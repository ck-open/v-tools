//package com.ck.pdf;
//
//
//import java.awt.*;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
//public class OpenPDFUtil {
//
//    /**
//     * 默认字体
//     */
//    private static final BaseFont baseFont;
//
//    static {
//        try {
//            /*
//                注册中文字体 否则中文字符将会被忽略 或者 乱码
//                由于 openPDF 默认不支持中文，需要引入字体库 SourceHanSans（开源免费）
//                下载地址：https://github.com/be5invis/source-han-sans-ttf/blob/master/src/SourceHanSans-Regular.ttc
//             */
//            baseFont = BaseFont.createFont("SourceHanSansSC-Regular.otf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//
//    private static final String testFilePath = "C:\\Users\\cyk\\Desktop\\";
//
//
//    public static void main(String[] args) {
//        testOpenPDF();
//        testOpenPDFReader();
//    }
//
//
//    /**
//     * 基本操作
//     */
//    public static void testOpenPDFReader(){
//        try {
//            // 读取原始 PDF 文件
//            PdfReader reader = new PdfReader(testFilePath + "test.pdf");
//
//
//            // 创建 PdfStamper 对象，指定输出文件的路径
//            PdfStamper stamper = new PdfStamper(reader, Files.newOutputStream(Paths.get(testFilePath + "modified.pdf")));
//
//            // 注册中文字体
//            Font chineseFont = new Font(baseFont, 12, Font.NORMAL);
//
//
//
//            // 遍历每一页
//            int numberOfPages = reader.getNumberOfPages();
//            PdfTextExtractor textExtractor = new PdfTextExtractor(reader);
//            for (int i = 1; i <= numberOfPages; i++) {
//
//                PdfObject pageObject = reader.getPageN(1);
//
//                String pageTextd = pageObject.toString();
//
//                byte[] pageContent = reader.getPageContent(i);
//
//                System.out.println(PdfEncodings.convertToString(pageContent, null));
//
//                // 获取当前页的文本内容
//                String pageText = textExtractor.getTextFromPage(i);
//
//                // 定义占位符及其替换内容
//                String placeholder = "PLACEHOLDER";
//                String replacement = "这是替换的内容";
//
//                // 替换占位符
//                if (pageText.contains(placeholder)) {
//                    // 获取 PDF 内容字节流
//                    PdfContentByte canvas = stamper.getOverContent(i);
//
//                    // 计算替换内容的位置
//                    int index = pageText.indexOf(placeholder);
//                    float x = 100; // 默认 x 坐标
//                    float y = 700; // 默认 y 坐标
//
//                    // 这里假设占位符的位置是固定的，实际应用中可能需要根据具体位置计算
//                    ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(replacement, chineseFont), x, y, 0);
//                }
//            }
//
//            // 关闭 PdfStamper 和 PdfReader
//            stamper.close();
//            reader.close();
//
//            System.out.println("PDF modified successfully.");
//        } catch (IOException | DocumentException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    /**
//     * 基本操作
//     */
//    public static void testOpenPDF(){
//        try {
//            // 创建 Document 对象
//            Document document = new Document();
//
//            // 创建 PdfWriter 对象，指定输出文件的路径
//            PdfWriter writer = PdfWriter.getInstance(document, Files.newOutputStream(Paths.get(testFilePath + "test.pdf")));
//
//            // 打开文档，准备写入内容。
//            document.open();
//
//            Font chineseFont = new Font(baseFont, 12, Font.NORMAL);
//
//            // 添加段落
//            document.add(new Paragraph("Hello, World!",chineseFont));
//            document.add(new Paragraph("这是一个使用openPDF创建的简单PDF文档。This is a simple PDF document created using openPDF.",chineseFont));
//
//            // 创建包含中文的段落并设置缩进
//            Paragraph chineseParagraph = new Paragraph("这是一个包含中文的段落。并设置了行间距和缩进。", chineseFont);
//            chineseParagraph.setIndentationLeft(36); // 设置左缩进为 36 点
//            chineseParagraph.setLeading(28.0f); // 设置行间距为 28 点
////            chineseParagraph.setMultipliedLeading(2.0f); // 设置变量leading。生成的前行将被乘以leading *maxFontSize，其中maxFontSize是该行中最大字体的大小。
//            document.add(chineseParagraph);
//
//
//            // 创建默认段落
//            Paragraph defaultParagraph = new Paragraph("这是一个默认的段落。This is a default paragraph.",chineseFont);
//            document.add(defaultParagraph);
//
//            // 创建加粗段落
//            Font boldFont = new Font(baseFont, 12, Font.BOLD);
//            Paragraph boldParagraph = new Paragraph("这是一段粗体。This is a bold paragraph.", boldFont);
//            document.add(boldParagraph);
//
//            // 创建斜体段落
//            Font italicFont = new Font(baseFont, 12, Font.ITALIC);
//            Paragraph italicParagraph = new Paragraph("这是一个斜体段落。This is an italic paragraph.", italicFont);
//            document.add(italicParagraph);
//
//            // 创建带颜色的段落
//            Font coloredFont = new Font(baseFont, 12, Font.NORMAL, Color.BLUE);
//            Paragraph coloredParagraph = new Paragraph("这是一个蓝色的段落。This is a blue paragraph.", coloredFont);
//            document.add(coloredParagraph);
//
//            // 创建多行段落
//            Paragraph multiLineParagraph = new Paragraph();
//            multiLineParagraph.add(new Paragraph("这是多行段落的第一行。This is the first line of a multi-line paragraph.",chineseFont));
//            multiLineParagraph.add(new Paragraph("这是同一段的第二行。This is the second line of the same paragraph.",chineseFont));
//            document.add(multiLineParagraph);
//
//            // 占位符
//            document.add(new Paragraph("PLACEHOLDER",chineseFont));
//
//
//
//
//            // 创建表格
//            PdfPTable table = new PdfPTable(3); // 3 列的表格
//            table.setWidthPercentage(100); // 表格宽度占页面的百分比
//            table.setSpacingBefore(20.0f); // 设置表格上边距为 20 点
//
//            // 添加表头
//            PdfPCell headerCell = new PdfPCell(new Paragraph("表头1", chineseFont));
//            headerCell.setBackgroundColor(Color.LIGHT_GRAY);
//            table.addCell(headerCell);
//
//            headerCell = new PdfPCell(new Paragraph("表头2", chineseFont));
//            headerCell.setBackgroundColor(Color.LIGHT_GRAY);
//            table.addCell(headerCell);
//
//            headerCell = new PdfPCell(new Paragraph("表头3", chineseFont));
//            headerCell.setBackgroundColor(Color.LIGHT_GRAY);
//            table.addCell(headerCell);
//
//            // 添加数据行
//            table.addCell(new Paragraph("数据1-1", chineseFont));
//            table.addCell(new Paragraph("数据1-2", chineseFont));
//            table.addCell(new Paragraph("数据1-3", chineseFont));
//
//            table.addCell(new Paragraph("数据2-1", chineseFont));
//            table.addCell(new Paragraph("数据2-2", chineseFont));
//            table.addCell(new Paragraph("数据2-3", chineseFont));
//
//
//            // 将表格添加到文档
//            document.add(table);
//
//            // 绘制线条
//            PdfContentByte cb = writer.getDirectContent();
//            cb.moveTo(50, 500); // 起点坐标
//            cb.lineTo(550, 500); // 终点坐标
//            cb.stroke(); // 绘制线条
//
//            // 关闭文档
//            document.close();
//
//            System.out.println("PDF created successfully.");
//        } catch (DocumentException | IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
//}
