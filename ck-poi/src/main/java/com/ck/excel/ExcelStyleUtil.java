package com.ck.excel;

import com.ck.excel.annotation.ExcelCell;
import com.ck.excel.annotation.ExcelTable;
import com.ck.excel.annotation.ExcelTitleStyle;
import com.ck.excel.enums.ExcelCellFormatEnum;
import com.ck.excel.enums.ExcelStyleEnum;
import org.apache.poi.ss.usermodel.*;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.time.*;
import java.util.*;


/**
 * Excel样式解析器
 *
 * @author Cyk
 * @version 1.0
 * @since 2022/9/4 18:30
 **/
public final class ExcelStyleUtil {

    /**
     * 解析 {@link ExcelTitleStyle} 注解并创建单元格样式对象
     *
     * @param sheet 工作表对象
     * @param cla   类
     * @return 单元格样式对象
     */
    public static CellStyle setCellStyle(Sheet sheet, Class<? extends @ExcelTable Object> cla) {
        if (cla == null) return null;
        return setCellStyle(sheet, cla.getDeclaredAnnotation(ExcelTable.class));
    }

    /**
     * 解析 {@link ExcelTitleStyle} 注解并创建单元格样式对象
     *
     * @param sheet      工作表对象
     * @param excelTable 注解
     * @return 单元格样式对象
     */
    public static CellStyle setCellStyle(Sheet sheet, ExcelTable excelTable) {
        if (excelTable == null || sheet == null) {
            return null;
        }
        CellStyle style = sheet.getWorkbook().createCellStyle();

        // 边框样式
        setBorder(style, excelTable.border(), excelTable.borderTop(), excelTable.borderRight(), excelTable.borderBottom(), excelTable.borderLeft());

        // 边框颜色
        setBorderColor(style, excelTable.borderColor(), excelTable.borderTopColor(), excelTable.borderRightColor(), excelTable.borderBottomColor(), excelTable.borderLeftColor()
                , excelTable.borderColorCustom(), excelTable.borderTopColorCustom(), excelTable.borderRightColorCustom(), excelTable.borderBottomColorCustom(), excelTable.borderLeftColorCustom());

        // 设置单元格填充
        setFillPatternType(style, excelTable.fillPatternType(), excelTable.fillForegroundColor(), excelTable.fillForegroundColorCustom());

        // 文本垂直水平对齐方式、自动换行、数据格式、文字旋转
        setDataFormat(sheet, style, excelTable.alignmentVertical(), excelTable.alignment(), excelTable.isWrapText(), excelTable.cellFormat(), excelTable.cellFormatCustom(), excelTable.rotation());

        // 设置字体
        setFont(sheet, style, excelTable.fontName(), excelTable.fontSize(), excelTable.fontBold(), excelTable.fontColor(), excelTable.fontColorCustom());
        return style;
    }

    /**
     * 解析 {@link ExcelTitleStyle} 注解并创建单元格样式对象
     *
     * @param sheet      工作表对象
     * @param excelTable sheet表配置注解的类
     * @return 单元格样式 key：FieldName  value： 单元格样式对象
     */
    public static Map<String, CellStyle> setCellStyleTitle(Sheet sheet, Class<? extends @ExcelTable Object> excelTable) {
        if (sheet == null || excelTable == null) {
            throw new NullPointerException("Workbook 与 TableValue 参数不能为空");
        }

        // 记录包含ECell 注解的Field 名称 及对应的单元格样式对象CellStyle
        Map<String, CellStyle> fieldStyle = new LinkedHashMap<>();
        // 表头 及 属性配置信息
        for (Field field : ExcelUtil.getFields(excelTable)) {
            // 读取对象 ECell 注解
            ExcelTitleStyle excelTitleStyle = field.getDeclaredAnnotation(ExcelTitleStyle.class);
            if (excelTitleStyle == null || fieldStyle.containsKey(field.getName())) continue;

            CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
            fieldStyle.put(field.getName(), cellStyle);

            // 边框样式
            setBorder(cellStyle, excelTitleStyle.border(), excelTitleStyle.borderTop(), excelTitleStyle.borderRight(), excelTitleStyle.borderBottom(), excelTitleStyle.borderLeft());

            // 边框颜色
            setBorderColor(cellStyle, excelTitleStyle.borderColor(), excelTitleStyle.borderTopColor(), excelTitleStyle.borderRightColor(), excelTitleStyle.borderBottomColor(), excelTitleStyle.borderLeftColor()
                    , excelTitleStyle.borderColorCustom(), excelTitleStyle.borderTopColorCustom(), excelTitleStyle.borderRightColorCustom(), excelTitleStyle.borderBottomColorCustom(), excelTitleStyle.borderLeftColorCustom());

            // 设置单元格填充
            setFillPatternType(cellStyle, excelTitleStyle.fillPatternType(), excelTitleStyle.fillForegroundColor(), excelTitleStyle.fillForegroundColorCustom());

            // 文本垂直水平对齐方式、自动换行、数据格式、文字旋转
            setDataFormat(sheet, cellStyle, excelTitleStyle.alignmentVertical(), excelTitleStyle.alignment(), excelTitleStyle.isWrapText(), excelTitleStyle.cellFormat(), excelTitleStyle.cellFormatCustom(), excelTitleStyle.rotation());

            // 设置字体
            setFont(sheet, cellStyle, excelTitleStyle.fontName(), excelTitleStyle.fontSize(), excelTitleStyle.fontBold(), excelTitleStyle.fontColor(), excelTitleStyle.fontColorCustom());
        }
        return fieldStyle;
    }

    /**
     * 解析 {@link ExcelTitleStyle} 注解并创建单元格样式对象
     *
     * @param sheet 工作表对象
     * @param cla   表数据对象
     * @return 单元格样式 key：FieldName  value： 单元格样式对象
     */
    public static Map<String, CellStyle> parseCellStyle(Sheet sheet, Class<? extends @ExcelTable Object> cla) {
        if (sheet == null || cla == null) {
            throw new NullPointerException("Workbook 与 TableValue 参数不能为空");
        }

        // 记录包含ECell 注解的Field 名称 及对应的单元格样式对象CellStyle
        Map<String, CellStyle> fieldStyle = new LinkedHashMap<>();
        Set<String> fieldSets = new HashSet<>();
        int index = 0;
        // 表头 及 属性配置信息
        for (Field field : ExcelUtil.getFields(cla)) {
            // 读取对象 ECell 注解
            ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
            if (excelCell == null || excelCell.isRowNumber()) continue;

            if (fieldSets.add(field.getName())) {
                // 设置列宽
                if (excelCell.columnWidth() != -1) {
                    sheet.setColumnWidth(index++, excelCell.columnWidth());
                }
            }

            CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
            fieldStyle.put(field.getName(), cellStyle);

            // 边框样式
            setBorder(cellStyle, excelCell.border(), excelCell.borderTop(), excelCell.borderRight(), excelCell.borderBottom(), excelCell.borderLeft());

            // 边框颜色
            setBorderColor(cellStyle, excelCell.borderColor(), excelCell.borderTopColor(), excelCell.borderRightColor(), excelCell.borderBottomColor(), excelCell.borderLeftColor()
                    , excelCell.borderColorCustom(), excelCell.borderTopColorCustom(), excelCell.borderRightColorCustom(), excelCell.borderBottomColorCustom(), excelCell.borderLeftColorCustom());

            // 设置单元格填充
            setFillPatternType(cellStyle, excelCell.fillPatternType(), excelCell.fillForegroundColor(), excelCell.fillForegroundColorCustom());

            // 日期字段默认格式
            String cellFormatCustom = excelCell.cellFormatCustom();
            if (cellFormatCustom.trim().isEmpty() && (
                    Date.class.isAssignableFrom(field.getType())
                    || LocalDateTime.class.isAssignableFrom(field.getType())
                    || LocalDate.class.isAssignableFrom(field.getType())
                    || LocalTime.class.isAssignableFrom(field.getType())
                    || Timestamp.class.isAssignableFrom(field.getType())
                    || java.sql.Date.class.isAssignableFrom(field.getType())
                    || java.sql.Time.class.isAssignableFrom(field.getType())
                    || java.sql.Timestamp.class.isAssignableFrom(field.getType())
                    || java.util.Date.class.isAssignableFrom(field.getType())
                    || Instant.class.isAssignableFrom(field.getType())
                    || ZonedDateTime.class.isAssignableFrom(field.getType())
                    || OffsetDateTime.class.isAssignableFrom(field.getType())
                    || OffsetTime.class.isAssignableFrom(field.getType())
                    || Period.class.isAssignableFrom(field.getType())
            )){
                cellFormatCustom = ExcelCellFormatEnum.FORMAT_DATE_TIME.getFormat();
            }

            // 文本垂直水平对齐方式、自动换行、数据格式、文字旋转
            setDataFormat(sheet, cellStyle, excelCell.alignmentVertical(), excelCell.alignment(), excelCell.isWrapText(), excelCell.cellFormat()
                    , cellFormatCustom, excelCell.rotation());

            // 设置字体
            setFont(sheet, cellStyle, excelCell.fontName(), excelCell.fontSize(), excelCell.fontBold(), excelCell.fontColor(), excelCell.fontColorCustom());
        }
        return fieldStyle;
    }

    /**
     * 设置字体样式
     *
     * @param sheet           Sheet页
     * @param cellStyle       样式对象
     * @param fontName        字体名称
     * @param fontSize        字体大小
     * @param fontBold        加粗
     * @param fontColor       颜色
     * @param fontColorCustom 自定义颜色
     * @return 单元格样式对象
     */
    public static CellStyle setFont(Sheet sheet, CellStyle cellStyle, String fontName, short fontSize, boolean fontBold, ExcelStyleEnum.Color fontColor, int[] fontColorCustom) {
        // 设置字体
        Font font = sheet.getWorkbook().createFont();
        if (fontName != null && !fontName.isEmpty()) {
            font.setFontName(fontName);
            cellStyle.setFont(font);
        }
        if (fontSize != -1) { // 大小
            font.setFontHeightInPoints(fontSize);
            cellStyle.setFont(font);
        }
        if (fontBold) { // 加粗
            font.setBold(fontBold);
            cellStyle.setFont(font);
        }
        if (!ExcelStyleEnum.Color.AUTOMATIC.equals(fontColor)) { // 字体颜色
            font.setColor(fontColor.getColor().getIndex());
            cellStyle.setFont(font);
        }
        if (fontColorCustom.length > 2) { // 字体颜色
            font.setColor(ExcelStyleEnum.FromColor(fontColorCustom).getIndex());
            cellStyle.setFont(font);
        }
        return cellStyle;
    }

    /**
     * 设置 文本垂直水平对齐方式、自动换行、数据格式、文字旋转
     *
     * @param sheet        Sheet页
     * @param cellStyle    样式对象
     * @param vertical     垂直对齐方式
     * @param horizontal   水平对齐方式
     * @param wrapped      自动换行
     * @param formatEnum   数据格式
     * @param formatCustom 自定义 数据格式
     * @param textRotation 文字旋转角度
     * @return 单元格样式对象
     */
    public static CellStyle setDataFormat(Sheet sheet, CellStyle cellStyle, VerticalAlignment vertical, HorizontalAlignment horizontal
            , boolean wrapped, ExcelCellFormatEnum formatEnum, String formatCustom, short textRotation) {
        // 设置文本居中
        cellStyle.setVerticalAlignment(vertical); // 垂直
        cellStyle.setAlignment(horizontal);  // 水平

        // 设置自动换行
        cellStyle.setWrapText(wrapped);

        // 设置数据格式
        if (!ExcelCellFormatEnum.FORMAT_NONE.equals(formatEnum)) {
            cellStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(formatEnum.getFormat()));
        }
        // 设置数据自定义格式
        if (formatCustom != null && !formatCustom.trim().isEmpty()) {
            cellStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(formatCustom));
        }

        // 设置文字旋转
        if (textRotation != -1) {
            cellStyle.setRotation(textRotation);
        }
        return cellStyle;
    }

    /**
     * 设置单元格填充样式及颜色
     *
     * @param cellStyle                 样式对象
     * @param fillPatternType           填充样式
     * @param fillForegroundColor       填充颜色
     * @param fillForegroundColorCustom 自定义填充颜色
     * @return 单元格样式对象
     */
    public static CellStyle setFillPatternType(CellStyle cellStyle, FillPatternType fillPatternType, ExcelStyleEnum.Color fillForegroundColor, int[] fillForegroundColorCustom) {
        // 设置单元格填充
        if (!FillPatternType.NO_FILL.equals(fillPatternType)) {
            cellStyle.setFillPattern(fillPatternType);

            // 设置单元格填充色
            if (!ExcelStyleEnum.Color.AUTOMATIC.equals(fillForegroundColor)) {
                cellStyle.setFillForegroundColor(fillForegroundColor.getColor().getIndex());
            }
            if (fillForegroundColorCustom.length > 2) {
                cellStyle.setFillForegroundColor(ExcelStyleEnum.FromColor(fillForegroundColorCustom).getIndex());
            }
        }
        return cellStyle;
    }

    /**
     * 设置单元格样式
     *
     * @param cellStyle   样式对象
     * @param borderStyle 默认四边样式
     * @param top         顶部样式
     * @param right       右侧样式
     * @param bottom      底部样式
     * @param left        左侧样式
     * @return 单元格样式对象
     */
    public static CellStyle setBorder(CellStyle cellStyle, ExcelStyleEnum.Border borderStyle
            , ExcelStyleEnum.Border top, ExcelStyleEnum.Border right, ExcelStyleEnum.Border bottom, ExcelStyleEnum.Border left) {
        // 边框样式
        cellStyle.setBorderTop(ExcelStyleEnum.Border.NONE.equals(top) ? borderStyle.getBorderStyle() : top.getBorderStyle()); // 顶部边框
        cellStyle.setBorderRight(ExcelStyleEnum.Border.NONE.equals(right) ? borderStyle.getBorderStyle() : right.getBorderStyle()); // 右边框
        cellStyle.setBorderBottom(ExcelStyleEnum.Border.NONE.equals(bottom) ? borderStyle.getBorderStyle() : bottom.getBorderStyle()); // 底边框
        cellStyle.setBorderLeft(ExcelStyleEnum.Border.NONE.equals(left) ? borderStyle.getBorderStyle() : left.getBorderStyle()); // 左边框
        return cellStyle;
    }

    /**
     * 设置单元格边框颜色
     *
     * @param cellStyle         单元格样式对象
     * @param borderStyle       默认颜色
     * @param top               顶部颜色
     * @param right             右侧颜色
     * @param bottom            底部颜色
     * @param left              左侧颜色
     * @param colorCustom       自定义颜色
     * @param topColorCustom    自定顶部颜色
     * @param rightColorCustom  自定义右侧颜色
     * @param bottomColorCustom 自定义底部颜色
     * @param leftColorCustom   自定义左侧颜色
     * @return 单元格样式对象
     */
    public static CellStyle setBorderColor(CellStyle cellStyle, ExcelStyleEnum.Color borderStyle
            , ExcelStyleEnum.Color top, ExcelStyleEnum.Color right, ExcelStyleEnum.Color bottom, ExcelStyleEnum.Color left
            , int[] colorCustom, int[] topColorCustom, int[] rightColorCustom, int[] bottomColorCustom, int[] leftColorCustom) {


        // 默认颜色
        cellStyle.setTopBorderColor(ExcelStyleEnum.Color.AUTOMATIC.equals(top) ? borderStyle.getColor().getIndex() : top.getColor().getIndex()); // 边框颜色
        cellStyle.setRightBorderColor(ExcelStyleEnum.Color.AUTOMATIC.equals(right) ? borderStyle.getColor().getIndex() : right.getColor().getIndex()); // 边框颜色
        cellStyle.setBottomBorderColor(ExcelStyleEnum.Color.AUTOMATIC.equals(bottom) ? borderStyle.getColor().getIndex() : bottom.getColor().getIndex()); // 边框颜色
        cellStyle.setLeftBorderColor(ExcelStyleEnum.Color.AUTOMATIC.equals(left) ? borderStyle.getColor().getIndex() : left.getColor().getIndex()); // 边框颜色

        // 自定义边框默认颜色
        if (colorCustom.length > 2) {
            cellStyle.setTopBorderColor(ExcelStyleEnum.FromColor(colorCustom).getIndex());
            cellStyle.setRightBorderColor(ExcelStyleEnum.FromColor(colorCustom).getIndex());
            cellStyle.setBottomBorderColor(ExcelStyleEnum.FromColor(colorCustom).getIndex());
            cellStyle.setLeftBorderColor(ExcelStyleEnum.FromColor(colorCustom).getIndex());
        }
        if (topColorCustom.length > 2) {
            cellStyle.setTopBorderColor(ExcelStyleEnum.FromColor(topColorCustom).getIndex()); // 边框颜色
        }
        if (rightColorCustom.length > 2) {
            cellStyle.setRightBorderColor(ExcelStyleEnum.FromColor(rightColorCustom).getIndex()); // 边框颜色
        }
        if (bottomColorCustom.length > 2) {
            cellStyle.setBottomBorderColor(ExcelStyleEnum.FromColor(bottomColorCustom).getIndex()); // 边框颜色
        }
        if (leftColorCustom.length > 2) {
            cellStyle.setLeftBorderColor(ExcelStyleEnum.FromColor(leftColorCustom).getIndex()); // 边框颜色
        }
        return cellStyle;
    }
}
