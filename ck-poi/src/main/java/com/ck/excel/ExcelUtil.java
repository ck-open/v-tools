package com.ck.excel;

import com.ck.excel.annotation.ExcelCell;
import com.ck.excel.annotation.ExcelTable;
import com.ck.excel.enums.ExcelCellFormatEnum;
import com.ck.excel.enums.ExcelTypeEnum;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 基于 POI 的 Excel工具类
 */
public class ExcelUtil {
    /**
     * 将对象转换Sheet数据添加到Workbook
     *
     * @param workbook      工作簿对象
     * @param sheetDataList 工作表对象列表
     * @return Workbook
     */
    @SafeVarargs
    public static Workbook createSheets(Workbook workbook, List<? extends @ExcelTable Object>... sheetDataList) {
        return createSheets(workbook, Arrays.asList(sheetDataList));
    }

    /**
     * 将对象转换Sheet数据添加到Workbook
     *
     * @param workbook      工作簿对象
     * @param sheetDataList 工作表对象列表
     * @return Workbook
     */
    public static Workbook createSheets(Workbook workbook, List<List<? extends @ExcelTable Object>> sheetDataList) {

        if (workbook == null || sheetDataList == null) {
            throw new NullPointerException("Workbook 与 Data 参数不能为空");
        }

        try {
            for (List<?> sheetData : sheetDataList) {
                if (sheetData.isEmpty()) continue;
                Class<? extends @ExcelTable Object> sheetClass = sheetData.get(0).getClass();
                // 读取类注解ETable
                ExcelTable excelTable = sheetClass.getDeclaredAnnotation(ExcelTable.class);
                if (excelTable == null) continue;
                // 创建Sheet 页  如果名称已存在则向后增加序列
                String sheetName = excelTable.sheetName().trim().isEmpty() ? "VExcelData" : excelTable.sheetName();
                if (workbook.getSheet(sheetName) != null) {
                    int index = 0;
                    do {
                        sheetName += ++index;
                    } while (workbook.getSheet(sheetName) != null);
                }

                Sheet sheet = workbook.createSheet(sheetName);

                // 记录包含ECell 注解的Field 名称 及对应的单元格样式对象CellStyle
                Map<String, CellStyle> fieldStyle = ExcelStyleUtil.parseCellStyle(sheet, sheetClass);
                if (fieldStyle.isEmpty()) continue; // 对象参数无VExcelCell注解则跳过本sheet页数据写出

                // 数据第一行 行号小于指定的数据所在行 则按照指定的行号创建
                int firstRowNumber = excelTable.titleRowNumber() == -1 ? sheet.getLastRowNum() : excelTable.titleRowNumber();
                Row row = sheet.createRow(firstRowNumber);

                // 设置标题
                if (!excelTable.value().trim().isEmpty()) {
                    mergedRegion(sheet, row.getRowNum(), row.getRowNum(), 0, fieldStyle.size() - 1);
                    CellStyle style = ExcelStyleUtil.setCellStyle(sheet, excelTable);
                    for (int i = 0; i < fieldStyle.size(); i++)
                        row.createCell(i).setCellStyle(style);
                    setValByType(row.getCell(0), null, excelTable.value());

                    row = sheet.createRow(sheet.getLastRowNum() + 1);
                }

                // 设置表头
                Map<String, CellStyle> titleStyle = ExcelStyleUtil.setCellStyleTitle(sheet, sheetClass);
                for (Field field : ExcelUtil.getFields(sheetData.get(0).getClass())) {
                    if (!fieldStyle.containsKey(field.getName())) continue;
                    // 读取对象 ExcelCell 注解
                    ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);

                    // 设置单元格值
                    Cell cell = row.createCell(row.getLastCellNum() < 0 ? 0 : row.getLastCellNum());
                    setValByType(cell, excelCell, excelCell.value().trim().isEmpty() ? field.getName() : excelCell.value());
                    setCellComment(workbook, sheet, cell, excelCell);
                    cell.setCellStyle(titleStyle.containsKey(field.getName()) ? titleStyle.get(field.getName()) : fieldStyle.get(field.getName()));
                }

                for (Object rowData : sheetData) {
                    row = sheet.createRow(sheet.getLastRowNum() + 1);
                    // 设置行高
                    if (excelTable.rowHeight() != -1) {
                        sheet.getRow(row.getRowNum()).setHeightInPoints(excelTable.rowHeight());
                    }

                    for (Field field : getFields(rowData.getClass())) {
                        ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
                        if (!fieldStyle.containsKey(field.getName())) continue;
                        // 设置单元格值
                        Cell cell = row.createCell(row.getLastCellNum() < 0 ? 0 : row.getLastCellNum());
                        field.setAccessible(true);
                        setValByType(cell, excelCell, field.get(rowData));
                        cell.setCellStyle(fieldStyle.get(field.getName()));
                    }
                }

                // 设置自适应宽度
                setAutoSizeColumn(sheet, sheetData.get(0).getClass());
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException("创建Workbook及Sheet数据异常", e);
        }
        return workbook;
    }

    /**
     * 设置自适应宽度  需要在所有数据组装完成后调用
     *
     * @param sheet Sheet页
     * @param cla   数据实体对象
     */
    public static void setAutoSizeColumn(Sheet sheet, Class<? extends @ExcelTable Object> cla) {
        if (sheet == null || cla == null || sheet instanceof HSSFSheet) return;
        SXSSFSheet sxssfSheet = (SXSSFSheet) sheet;
        sxssfSheet.trackAllColumnsForAutoSizing();
        int index = 0;
        for (Field field : ExcelUtil.getFields(cla)) {
            ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
            if (excelCell != null) {
                if (excelCell.autoSizeColumn()) {
                    sxssfSheet.autoSizeColumn(index);
                }
                index++;
            }
        }
    }

    /**
     * 合并单元格
     * 必须在设置单元格样式之后再进行单元格合并
     *
     * @param sheet         Sheet页
     * @param rowNum        合并范围开始行
     * @param rowNumLast    合并范围结束行
     * @param columnNum     合并范围开始列
     * @param columnNumLast 合并范围结束列
     */
    public static void mergedRegion(Sheet sheet, int rowNum, int rowNumLast, int columnNum, int columnNumLast) {
        try {
            if (rowNumLast < rowNum || columnNumLast < columnNum
                    || (rowNumLast == rowNum && columnNumLast == columnNum)) {
                return;
            }
            sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNumLast, columnNum, columnNumLast));
        } catch (Exception e) {
            throw new RuntimeException("单元格合并失败！  合并范围： " + rowNum + ":" + rowNumLast + " 至 " + columnNum + ":" + columnNumLast, e);
        }
    }

    public static void setCellComment(Workbook workbook, Sheet sheet, Cell cell, ExcelCell excelCell) {
        if (sheet == null || cell == null || excelCell.comment().isEmpty()) return;
        /* 创建批注对象 */
        CreationHelper factory = workbook.getCreationHelper();
        Drawing<?> drawing = sheet.createDrawingPatriarch(); /* 创建或获取绘图 patriarch */

        /* 设置批注框的相对位置，可根据需要调整 */
        ClientAnchor anchor = factory.createClientAnchor();
        anchor.setCol1(cell.getColumnIndex());
        anchor.setCol2(cell.getColumnIndex() + Math.max(1, excelCell.commentCollNum()));
        anchor.setRow1(cell.getRowIndex());
        anchor.setRow2(cell.getRowIndex() + Math.max(1, excelCell.commentRowNum()));

        /* 设置批注内容和作者 */
        Comment comment = drawing.createCellComment(anchor);
        comment.setString(factory.createRichTextString(excelCell.comment())); /* 设置批注文本 */
        if (!excelCell.commentAuthor().isEmpty()) {
            comment.setAuthor(excelCell.commentAuthor()); /* 可选，设置批注作者 */
        }

        /*  关联批注与单元格 */
        cell.setCellComment(comment);
    }

    /**
     * 按数据类型添加数据到单元格
     *
     * @param cell 单元格对象
     * @param val  值
     */
    public static void setValByType(Cell cell, ExcelCell excelCell, Object val) {
        if (cell != null && val != null && !"".equals(val)) {
            val = setValByType(excelCell, val);
            String valTemp = String.valueOf(val);
            if (valTemp.length() < 10 && valTemp.matches("^[+-]?((\\d{1,50})|(0{1}))(\\.\\d{1,})?$")) {
                valTemp = valTemp.replace("+", "");
                cell.setCellValue(Double.parseDouble(valTemp));
            } else {
                cell.setCellValue(valTemp);
            }
        }
    }

    /**
     * 按数据类型添加数据到单元格
     *
     * @param excelCell 单元格注解
     * @param val       值
     */
    public static Object setValByType(ExcelCell excelCell, Object val) {
        if (excelCell != null && val != null) {

            String cellFormatCustom = excelCell.cellFormatCustom().trim().isEmpty() ? null : excelCell.cellFormatCustom();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(cellFormatCustom == null ? ExcelCellFormatEnum.FORMAT_DATE_TIME.getFormat() : excelCell.cellFormatCustom());

            Class<?> cla = val.getClass();

            if (String.class.isAssignableFrom(cla)
                    || Boolean.class.isAssignableFrom(cla) || boolean.class.isAssignableFrom(cla)
                    || char.class.isAssignableFrom(cla)
            ) {
                val = String.valueOf(val);
            } else if (Date.class.isAssignableFrom(cla)) {
                val = simpleDateFormat.format(val);
            } else if (Calendar.class.isAssignableFrom(cla)) {
                val = simpleDateFormat.format(((Calendar) val).getTime());
            } else if (LocalDate.class.isAssignableFrom(cla)) {
                val = simpleDateFormat.format(Date.from(((LocalDate) val).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
            } else if (LocalDateTime.class.isAssignableFrom(cla)) {
                val = simpleDateFormat.format(Date.from(((LocalDateTime) val).atZone(ZoneId.systemDefault()).toInstant()));
            } else if (Number.class.isAssignableFrom(cla) || long.class.isAssignableFrom(cla)
                    || int.class.isAssignableFrom(cla) || float.class.isAssignableFrom(cla)
                    || double.class.isAssignableFrom(cla) || short.class.isAssignableFrom(cla)
            ) {
                if (BigDecimal.class.isAssignableFrom(cla) && BigDecimal.ZERO.compareTo((BigDecimal) val) == 0) {
                    val = BigDecimal.ZERO;
                }
                if (cellFormatCustom != null) {
                    DecimalFormat df = new DecimalFormat(cellFormatCustom);
                    val = df.format(val);
                } else {
                    val = String.valueOf(val);
                }
            } else {
                val = String.valueOf(val);
            }
            String valTemp = String.valueOf(val);
            if (valTemp.length() > 10 && valTemp.matches("^[+-]?((\\d{1,50})|0)$")) {
                val = "`" + valTemp;
            }
        }

        return val;
    }

    /**
     * 数据传输
     *
     * @param bytes  二进制数据
     * @param output 输出流
     * @param mbps   传输速度
     */
    public static void io(byte[] bytes, OutputStream output, int mbps) {
        try {
            mbps = mbps <= 0 ? 1 : Math.min(mbps, 100);
            if (bytes != null) {
                int len = 1024 * mbps;
                for (int i = 0; i < bytes.length; i = i + len) {
                    if (i + len > bytes.length)
                        len = bytes.length - i;
                    output.write(bytes, i, len);
                    output.flush();
                }
            }
            output.flush();
        } catch (IOException e) {
            throw new RuntimeException("数据传输失败！", e);
        }
    }

    /**
     * 数据传输
     *
     * @param inputStream  输入流
     * @param outputStream 输出流
     * @param mbps         传输速度
     */
    public static void io(InputStream inputStream, OutputStream outputStream, int mbps) {
        try {
            mbps = mbps <= 0 ? 1 : Math.min(mbps, 100);

            byte[] buffer = new byte[1024 * mbps];
            int len = inputStream.read(buffer);
            while (len != -1) {
                outputStream.write(buffer, 0, len);
                outputStream.flush();
                len = inputStream.read(buffer);
            }
        } catch (IOException e) {
            throw new RuntimeException("数据传输失败！", e);
        }
    }

    /* ================================================= 以下为 读取表数据 ==================================================================== */

    /**
     * 根据字段与值类型匹配给字段赋值
     *
     * @param field 字段对象
     * @param val   值
     */
    public static void setFieldValueByType(Field field, Object bean, Object val) {
        if (field == null || bean == null || val == null) return;
        try {
            ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
            field.setAccessible(true);
            if (field.getType().isAssignableFrom(val.getClass())) {
                field.set(bean, val);
            } else {
                Class<?> fieldType = field.getType();
                val = parseValueByType(fieldType, val, excelCell);
            }

            field.set(bean, val);
        } catch (IllegalAccessException | ParseException | IllegalArgumentException e) {
            String msg = String.format("字段赋值异常, 对象类型:%s  属性:%s  类型:%s  值:%s", bean.getClass().getName(), field.getName(), field.getType().getName(), val);
            throw new RuntimeException(msg, e);
        }
    }

    /**
     * 解析单元格值
     *
     * @param fieldType 字段类型
     * @param val       单元格值
     * @return 解析后的值
     * @throws ParseException 解析异常
     */
    public static Object parseValueByType(Class<?> fieldType, Object val, ExcelCell excelCell) throws ParseException {
        if (String.class.isAssignableFrom(fieldType)) {
            val = val.toString();

        } else if (Date.class.isAssignableFrom(fieldType)
                || Calendar.class.isAssignableFrom(fieldType)
                || LocalDate.class.isAssignableFrom(fieldType)
                || LocalDateTime.class.isAssignableFrom(fieldType)
        ) {
            Date valDate = null;

            if (Date.class.isAssignableFrom(val.getClass())) {
                valDate = (Date) val;
            } else if (String.class.isAssignableFrom(val.getClass())) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(excelCell.cellFormatCustom().trim().isEmpty() ? ExcelCellFormatEnum.FORMAT_DATE_TIME.getFormat() : excelCell.cellFormatCustom());
                valDate = simpleDateFormat.parse(val.toString());
            }
            val = valDate;

            if (Calendar.class.isAssignableFrom(fieldType)) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(valDate);
                val = calendar;
            } else if (LocalDate.class.isAssignableFrom(fieldType)) {
                val = valDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toLocalDate();
            } else if (LocalDateTime.class.isAssignableFrom(fieldType)) {
                val = valDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            }
        } else if (Long.class.isAssignableFrom(fieldType) || long.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, BigDecimal::longValue);
        } else if (Integer.class.isAssignableFrom(fieldType) || int.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, BigDecimal::intValue);
        } else if (Float.class.isAssignableFrom(fieldType) || float.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, BigDecimal::floatValue);
        } else if (Double.class.isAssignableFrom(fieldType) || double.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, BigDecimal::doubleValue);
        } else if (Short.class.isAssignableFrom(fieldType) || short.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, BigDecimal::shortValue);
        } else if (BigDecimal.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, num -> num);
        } else if (BigInteger.class.isAssignableFrom(fieldType)) {
            val = parseValueByBigDecimal(val, num -> BigInteger.valueOf(num.longValue()));
        } else if (Boolean.class.isAssignableFrom(fieldType) || boolean.class.isAssignableFrom(fieldType)) {
            val = Boolean.valueOf(val.toString());
        } else if (char.class.isAssignableFrom(fieldType)) {
            val = val.toString().charAt(0);
        }
        return val;
    }

    /**
     * 解析单元格值 数字格式转换
     *
     * @param val  值
     * @param func 转换函数
     * @return 转换后的值
     */
    protected static Object parseValueByBigDecimal(Object val, Function<BigDecimal, Object> func) {
        if (val == null || func == null) {
            return val;
        }
        return func.apply(new BigDecimal(val.toString().replace("`", "").replace(",", "")));
    }

    /**
     * 获取指定的单元格列数对应的字母列名
     *
     * @param columnNum 获取的列数字坐标
     * @return 列坐标字母
     */
    public static String getColumnName(int columnNum) {
        String columnName = "";
        if (columnNum > 26) {
            if (columnNum % 26 == 0 && columnNum / 26 < 26) {
                columnName += (char) (columnNum / 26 - 1 + 64); // 字母A 为65
                columnName += 'Z';
            } else {
                columnName += getColumnName(columnNum / 26);
                columnName += (char) (columnNum % 26 + 64); // 字母A 为65
            }
        } else {
            columnName += (char) (columnNum + 64);  // 字母A 为65
        }

        return columnName;
    }

    /**
     * 获取实体类中指定了记录数据行号的字段
     *
     * @param sheetClass sheetClass
     * @return Field
     */
    public static Field getRowNumberField(Class<? extends @ExcelTable Object> sheetClass) {
        // 实体类中指定了记录数据行号的字段  注解 excelCell.isRowNumber() = true 的字段
        Field fieldRowNumber = ExcelUtil.getFields(sheetClass).stream()
                .filter(field -> field.getDeclaredAnnotation(ExcelCell.class).isRowNumber())
                .findFirst().orElse(null);

        if (fieldRowNumber != null) {
            fieldRowNumber.setAccessible(true);
        }

        return fieldRowNumber;
    }

    /**
     * 获取表头字段
     *
     * @param sheetClass 数据类型
     * @param <T>        数据类型
     * @return 字段集合 key:表头名称 value:字段对象
     */
    public static <T extends @ExcelTable Object> Map<String, Field> getSheetClassField(Class<T> sheetClass) {
        Map<String, Field> sheetClassField = new LinkedHashMap<>();

        if (sheetClass != null) {

            for (Field field : ExcelUtil.getFields(sheetClass)) {
                ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);

                if (excelCell == null || excelCell.isRowNumber()) continue;
                field.setAccessible(true);

                String titleName = excelCell.value().trim().isEmpty() ? field.getName() : excelCell.value();
                sheetClassField.put(titleName, field);
            }
        }
        return sheetClassField;
    }

    /**
     * 获取表头字段 以文件顺序排序
     *
     * @param sheetClassField 数据类型
     * @param rowData         文件标题数据行
     * @return 字段集合 key:表头名称位置下标 0为第一个 value:字段对象
     */
    public static Map<Integer, Field> getSheetClassFieldIndex(Map<String, Field> sheetClassField, List<?> rowData) {
        if (sheetClassField == null || sheetClassField.isEmpty() || rowData == null || rowData.isEmpty()) {
            return null;
        }

        Map<Integer, Field> sheetClassFieldByIndex = new LinkedHashMap<>();

        for (int i = 0; i < rowData.size(); i++) {
            if (Objects.nonNull(rowData.get(i))) {
                String titleName = String.valueOf(rowData.get(i));
                if (sheetClassField.containsKey(titleName)) {
                    sheetClassFieldByIndex.put(i, sheetClassField.get(titleName));
                }
            }
        }

        return sheetClassFieldByIndex;
    }

    /**
     * 获取单元格类型
     * 旧版本  cell.getCellType() 返回的是枚举code值
     *
     * @param cellType cell.getCellType() 返回值
     * @return 单元格类型枚举  无法识别返回String 类型
     */
    protected static CellType getCellType(Object cellType) {
        if (cellType != null) {
            if (CellType.class.isAssignableFrom(cellType.getClass())) {
                return (CellType) cellType;
            } else if (Integer.class.isAssignableFrom(cellType.getClass())) {
                // 用于兼容老版本
                return Stream.of(CellType.values()).filter(c -> c.getCode() == (int) cellType).findFirst().orElse(CellType.BLANK);
            }
        }
        return CellType.BLANK;
    }

    /**
     * 获取单元格类型
     * 旧版本  cell.getCellType() 返回的是枚举code值
     *
     * @param cell 单元格对象
     * @return 单元格类型枚举  无法识别返回String 类型
     */
    public static CellType getCellType(Cell cell) {
        if (cell == null) return CellType.BLANK;
        return getCellType(cell.getCellType());
    }

    /**
     * 验证单元格是否是空
     *
     * @param cell 单元格对象
     * @return 返回true为空, false不为空
     */
    public static boolean isCellEmpty(Cell cell) {
        boolean validate = true;

        CellType cellType = getCellType(cell);

        if (!CellType.BLANK.equals(cellType)) {
            if (CellType.NUMERIC.equals(cellType)) {
                validate = false;
            } else if (CellType.FORMULA.equals(cellType) && cell.getCellFormula() != null && !cell.getCellFormula().trim().isEmpty()) {
                return false;
            } else {
                if (!cell.getStringCellValue().trim().isEmpty()) {
                    validate = false;
                }
            }
        }
        return validate;
    }

    /**
     * 解析数据行单元格数据 根据类型转换
     *
     * @param row 数据行
     * @return 解析单元格数据
     */
    public static List<Object> parseRowValueByType(Row row) {
        List<Object> rowData = new ArrayList<>();
        if (row!=null){
            for (int x = 0; x < row.getLastCellNum(); x++) {
                Cell cell = row.getCell(x);
                if (isCellEmpty(cell)) {
                    rowData.add(null);
                } else {
                    Object val = parseCellValueByType(cell);
                    rowData.add(val);
                }
            }
        }
        return rowData;
    }

    /**
     * 解析单元格数据 根据类型转换
     *
     * @param cell 单元格对象
     * @return 解析单元格数据
     */
    public static Object parseCellValueByType(Cell cell) {
        if (cell == null) return null;

        switch (getCellType(cell)) {
            case _NONE: // 空白单元格
            case BLANK: // 空白单元格
            case ERROR: // 异常单元格数据
                return null;
            case STRING: // 字符串(文本)单元格类型
                return cell.getStringCellValue();
            case NUMERIC: // 数值型单元格类型(整数、小数、日期)
                Date date = parseCellTypeOfDate(cell);
                if (date != null) return date;
                return cell.getNumericCellValue();
            case BOOLEAN: // Boolean
                return cell.getBooleanCellValue();
            case FORMULA: // 公式单元格，需要公式计算后得到值
                return parseCellTypeOfFormula(cell);
            default:
                return null;
        }
    }

    /**
     * 解析单元格日期时间数据
     *
     * @param cell 单元格对象
     * @return Date
     */
    public static Date parseCellTypeOfDate(Cell cell) {
        // General 常规
        // 日期时间类型的单元格样式
        List<Integer> dataFormatOfDate = Arrays.asList(14, 31, 57, 58, 164, 176, 177, 178, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213);

        // 获得单元格时间格式样式
        short format = cell.getCellStyle().getDataFormat();
        // 判断是否为日期
        if ((cell.getSheet().getWorkbook() instanceof HSSFWorkbook && HSSFDateUtil.isCellDateFormatted(cell))
                || (cell.getSheet().getWorkbook() instanceof XSSFWorkbook && DateUtil.isCellDateFormatted(cell))
                || dataFormatOfDate.contains((int) format)) {

            return DateUtil.getJavaDate(cell.getNumericCellValue());
        }
        return null;
    }

    /**
     * 解析单元格公式
     *
     * @param cell 单元格对象
     * @return 解析单元格公式
     */
    public static Object parseCellTypeOfFormula(Cell cell) {
        // 公式,解读单元格公式计算后的值
        FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
        CellValue c = evaluator.evaluate(cell);
        switch (getCellType(c)) {
            case STRING: // 字符串
                return c.getStringValue();
            case NUMERIC: // 数字
                return c.getNumberValue();
            case BOOLEAN: // 布尔
                return c.getBooleanValue();
            default:
                return null;
        }
    }


    public static <T extends @ExcelTable Object> List<T> read(ExcelTypeEnum workbookType, Path path, String sheetName, Charset charset, Class<T> sheetClass) {
        try (FileInputStream in = new FileInputStream(path.toFile())) {
            return ExcelUtil.read(workbookType, in, sheetName, charset, null, sheetClass);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Excel 文件读取异常:%s", e.getMessage()), e);
        }
    }

    public static <T extends @ExcelTable Object> List<T> read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Class<T> sheetClass) {
        return read(workbookType, in, sheetName, charset, null, sheetClass);
    }

    /**
     * 读取CSV文件内容<br>
     * 每个元素代表一行数据,数组的每个元素代表每个单元格
     *
     * @param workbookType Excel 文件类型 默认为 XLSX
     * @param in           Excel 文件输入流
     * @param sheetName    sheet页名称(Excel 文件解析使用)
     * @param charset      文件字符集(csv解读使用)
     * @param titleRowNum  标题所在行  0 表示第一行，默认自动识别
     * @param sheetClass   接收数据的实体对象类型
     * @param <T>          返回值数据类型
     * @return 解读结果
     */
    public static <T extends @ExcelTable Object> List<T> read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Integer titleRowNum, Class<T> sheetClass) {
        List<T> result = new ArrayList<>();
        read(workbookType, in, sheetName, charset, titleRowNum, sheetClass, (rowNum, rowData) -> result.add(rowData));
        return result;
    }

    public static <T extends @ExcelTable Object> void read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Class<T> sheetClass, BiConsumer<Long, T> consumerRow) {
        read(workbookType, in, sheetName, charset, null, sheetClass, consumerRow);
    }

    /**
     * 解读指定的Sheet页为指定的对象
     *
     * @param workbookType Excel 文件类型 默认为 XLSX
     * @param in           Excel 文件输入流
     * @param sheetName    sheet页名称(Excel 文件解析使用)
     * @param charset      文件字符集(csv解读使用)
     * @param titleRowNum  标题所在行  0 表示第一行，默认自动识别
     * @param sheetClass   接收数据的实体对象类型
     * @param consumerRow  按行处理
     * @param <T>          返回值数据类型
     */
    public static <T extends @ExcelTable Object> void read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Integer titleRowNum, Class<T> sheetClass, BiConsumer<Long, T> consumerRow) {
        if (ExcelTypeEnum.CSV.equals(workbookType)) {
            readCsv(sheetClass, in, charset, titleRowNum, consumerRow);
        } else {
            readExcel(workbookType, in, sheetName, titleRowNum, sheetClass, consumerRow);
        }
    }

    /**
     * 读取CSV文件内容<br>
     * 每个元素代表一行数据,数组的每个元素代表每个单元格
     *
     * @param sheetClass  数据类型
     * @param in          csv文件输入流
     * @param charset     文件字符集 默认为UTF-8
     * @param titleRowNum 标题所在行  0 表示第一行，默认自动识别
     * @param consumerRow 按行处理
     */
    protected static <T extends @ExcelTable Object> void readCsv(Class<T> sheetClass, InputStream in, Charset charset, Integer titleRowNum, BiConsumer<Long, T> consumerRow) {
        if (in == null || sheetClass == null || consumerRow == null) {
            return;
        }

        ExcelTable excelTable = sheetClass.getDeclaredAnnotation(ExcelTable.class);
        if (excelTable == null) return;

        // 标题所在行
        AtomicInteger titleRowNumAtomic = new AtomicInteger(excelTable.titleRowNumber() == -1 ? 0 : excelTable.titleRowNumber());
        if (titleRowNum != null)
            titleRowNumAtomic.set(titleRowNum);

        // 获取数据对象参数字典  key：表格中标题名称
        Map<String, Field> sheetClassField = getSheetClassField(sheetClass);

        // 实体类中指定了记录数据行号的字段
        Field rowNumberField = getRowNumberField(sheetClass);

        Map<Integer, Field> sheetClassFieldByIndex = new HashMap<>();

        readCsv(in, charset, (rowNum, row) -> {
            if (row != null && !row.isEmpty()) {
                // 标题行
                if (titleRowNum != null && rowNum < titleRowNumAtomic.get()) {
                    return;
                } else if (sheetClassFieldByIndex.isEmpty() && (titleRowNum == null || rowNum == titleRowNumAtomic.get())) {
                    Map<Integer, Field> sheetClassFieldByIndexTemp = getSheetClassFieldIndex(sheetClassField, row);
                    if (sheetClassFieldByIndexTemp != null && !sheetClassFieldByIndexTemp.isEmpty())
                        sheetClassFieldByIndex.putAll(sheetClassFieldByIndexTemp);
                    return;
                }

                if (sheetClassFieldByIndex.isEmpty()) {
                    throw new RuntimeException("解析CSV文件时 表头未找到，请检查表头是否正确");
                }

                T bean = convertBean(sheetClass, rowNumberField, rowNum, sheetClassFieldByIndex, row);
                consumerRow.accept(rowNum, bean);
            }
        });
    }

    /**
     * 读取CSV文件内容<br>
     * 每个元素代表一行数据,数组的每个元素代表每个单元格
     *
     * @param in          csv文件输入流
     * @param charset     文件字符集 默认为GBK
     * @param consumerRow 按行处理
     */
    public static void readCsv(InputStream in, Charset charset, BiConsumer<Long, List<String>> consumerRow) {
        if (in == null) throw new NullPointerException("CSV 文件读取失败，InputStream 为空");
        if (charset == null) charset = Charset.forName("GBK");

        Long rowNum = 0L;
        //创建文件输入流对象，读取.csv文件
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset))) {
            String line = null;
            while ((line = reader.readLine()) != null) {// 遍历文件所有数据
                String[] item = line.split(","); // 以逗号分隔获取所有数据到数组
                consumerRow.accept(rowNum, Arrays.asList(item)); // 处理读取到的行数据
                rowNum++;
            }
        } catch (IOException e) {
            //文件输入流异常
            throw new RuntimeException("CSV 文件读取异常", e);
        }
    }

    /**
     * 解读 Excel 文件为 Workbook 对象
     *
     * @param workbookType        Excel 文件类型
     * @param inputStream         Excel 文件输入流
     * @return Workbook
     */
    public static Workbook readWorkbook(ExcelTypeEnum workbookType, InputStream inputStream) {
        if (inputStream == null)
            throw new NullPointerException("解读 Workbook 工作簿失败，InputStream is null");
        if (workbookType == null)
            throw new NullPointerException("解读 Workbook 工作簿失败，VExcelWorkbookType is null");
        try {
            if (ExcelTypeEnum.XLS_X.equals(workbookType)) {
                return new XSSFWorkbook(inputStream);
            } else if (ExcelTypeEnum.XLS.equals(workbookType)) {
                return new HSSFWorkbook(inputStream);
            }
        } catch (IOException e) {
            throw new RuntimeException("解读 Workbook 工作簿异常", e);
        }
        return null;
    }

    /**
     * 解读指定的Sheet页为指定的对象
     *
     * @param workbookType Excel 文件类型 默认为 XLSX
     * @param in           Excel 文件输入流
     * @param sheetName    sheet页名称
     * @param titleRowNum  标题所在行，默认为第一行
     * @param sheetClass   接收数据的实体对象类型
     * @param consumerRow  按行处理
     * @param <T>          返回值数据类型
     */
    protected static <T extends @ExcelTable Object> void readExcel(ExcelTypeEnum workbookType, InputStream in, String sheetName, Integer titleRowNum, Class<T> sheetClass, BiConsumer<Long, T> consumerRow) {

        if (workbookType == null || in == null || sheetClass == null || consumerRow == null) {
            return;
        }

        ExcelTable excelTable = sheetClass.getDeclaredAnnotation(ExcelTable.class);
        if (excelTable == null) return;

        Workbook workbook = readWorkbook(workbookType, in);
        if (workbook == null)
            throw new NullPointerException("读取工作簿失败，Workbook is null");

        if ((sheetName == null || sheetName.isEmpty()) && !excelTable.sheetName().isEmpty()) {
            sheetName = excelTable.sheetName();
        }
        Sheet sheet = workbook.getSheet(sheetName);
        if (sheet == null)
            throw new NullPointerException(String.format("读取工作表数据失败，Sheet 页[%s]不存在", sheetName));

        // 标题所在行
        if (titleRowNum == null && excelTable.titleRowNumber() != -1)
            titleRowNum = excelTable.titleRowNumber();

        // 获取数据对象参数字典  key：表格中标题名称
        Map<String, Field> sheetClassField = getSheetClassField(sheetClass);

        // 实体类中指定了记录数据行号的字段
        Field rowNumberField = getRowNumberField(sheetClass);

        Map<Integer, Field> sheetClassFieldByIndex = null;

        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (row == null) continue;

            List<Object> rowData = parseRowValueByType(row);

            // 标题行
            if (titleRowNum != null && i < titleRowNum) {
                continue;
            } else if ((sheetClassFieldByIndex == null || sheetClassFieldByIndex.isEmpty()) && (titleRowNum == null || i == titleRowNum)) {
                sheetClassFieldByIndex = getSheetClassFieldIndex(sheetClassField, rowData);
                continue;
            }
            if (sheetClassFieldByIndex == null || sheetClassFieldByIndex.isEmpty()) {
                throw new RuntimeException("解析CSV文件时 表头未找到，请检查表头是否正确");
            }

            T bean = convertBean(sheetClass, rowNumberField, i, sheetClassFieldByIndex, rowData);
            consumerRow.accept((long) i, bean);
        }
    }

    /**
     * 将行数据转换为实体对象
     *
     * @param sheetClass             目标实体
     * @param rowNumberField         行号字段
     * @param rowNumber              行号
     * @param sheetClassFieldByIndex 字段 与行数据下标索引
     * @param rowData                行数据
     * @param <T>                    目标实体类型
     * @return 实体对象
     */
    protected static <T extends @ExcelTable Object> T convertBean(Class<T> sheetClass, Field rowNumberField, long rowNumber, Map<Integer, Field> sheetClassFieldByIndex, List<?> rowData) {
        try {
            T bean = sheetClass.newInstance();

            // 设置数据行号
            if (rowNumberField != null) setFieldValueByType(rowNumberField, bean, rowNumber);

            for (Integer index : sheetClassFieldByIndex.keySet()) {
                if (index < rowData.size()) {
                    Field field = sheetClassFieldByIndex.get(index);
                    Object val = rowData.get(index);
                    setFieldValueByType(field, bean, val);
                }
            }

            return bean;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取数据实例对象所有字段
     *
     * @param entityClass 实体类
     * @return 属性字段列表
     */
    public static Set<Field> getFields(Class<? extends @ExcelTable Object> entityClass) {
        if (entityClass == null || Class.class.isAssignableFrom(entityClass)) return new HashSet<>();
        // 去除重复字段  避免子类重写后 重复添加父类的字段
        Set<String> fieldsDistinct = new HashSet<>();

        Set<Field> result = Stream.of(entityClass.getDeclaredFields())
                .filter(i -> !Modifier.isFinal(i.getModifiers()) && !Modifier.isStatic(i.getModifiers()) && fieldsDistinct.add(i.getName()))
                .collect(Collectors.toCollection(LinkedHashSet::new));

        while (Objects.nonNull(entityClass.getSuperclass()) && !entityClass.getSuperclass().isAssignableFrom(Object.class)) {
            entityClass = entityClass.getSuperclass();
            result.addAll(Stream.of(entityClass.getDeclaredFields())
                    .filter(i -> !Modifier.isFinal(i.getModifiers()) && fieldsDistinct.add(i.getName()))
                    .collect(Collectors.toSet()));
        }

        return result.stream().sorted(Comparator.comparingInt(field -> {
            ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
            return excelCell == null ? Integer.MAX_VALUE : excelCell.order();
        })).collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
