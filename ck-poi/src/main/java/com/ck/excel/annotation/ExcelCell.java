package com.ck.excel.annotation;


import com.ck.excel.enums.ExcelCellFormatEnum;
import com.ck.excel.enums.ExcelStyleEnum;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import java.lang.annotation.*;

/**
 * Excel表格列注解
 * @author Cyk
 * @since 2022/9/4 18:30
 **/
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ExcelCell {
    /**
     * 标题名称 空则取属性名
     */
    String value() default "";

    /**
     * 列排序 由小到大 默认最大值
     */
    int order() default Integer.MAX_VALUE;

    /**
     * 列宽  默认不设置
     */
    int columnWidth() default -1;

    /**
     * 单元格数据格式  默认 常规
     */
    ExcelCellFormatEnum cellFormat() default ExcelCellFormatEnum.FORMAT_NONE;

    /**
     * 单元格数据格式 自定义  默认 无
     */
    String cellFormatCustom() default "";

    /**
     * 文本旋转角度 默认不旋转
     */
    short rotation() default -1;

    /**
     * 水平对齐方式  默认靠左
     */
    HorizontalAlignment alignment() default HorizontalAlignment.LEFT;

    /**
     * 垂直对齐方式  默认居中
     */
    VerticalAlignment alignmentVertical() default VerticalAlignment.CENTER;
    /**
     * 是否文本自动换行  默认不换行
     */
    boolean isWrapText() default false;
    /**
     * 字体  默认不设置
     */
    String fontName() default "宋体";
    /**
     * 字体大小  默认不设置
     */
    short fontSize() default -1;
    /**
     * 字体加粗 默认不加粗
     */
    boolean fontBold() default false;
    /**
     * 字体颜色  默认 不设置
     */
    ExcelStyleEnum.Color fontColor() default ExcelStyleEnum.Color.AUTOMATIC;

    /**
     * 字体颜色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] fontColorCustom() default {};

    /**
     * 单元格边框样式  默认实线
     */
    ExcelStyleEnum.Border border() default ExcelStyleEnum.Border.NONE;

    /**
     * 单元格顶部边框样式  默认无
     */
    ExcelStyleEnum.Border borderTop() default ExcelStyleEnum.Border.NONE;
    /**
     * 单元格右侧边框样式  默认无
     */
    ExcelStyleEnum.Border borderRight() default ExcelStyleEnum.Border.NONE;
    /**
     * 单元格底部边框样式  默认无
     */
    ExcelStyleEnum.Border borderBottom() default ExcelStyleEnum.Border.NONE;
    /**
     * 单元格左侧边框样式  默认无
     */
    ExcelStyleEnum.Border borderLeft() default ExcelStyleEnum.Border.NONE;

    /**
     * 单元格边框颜色  默认黑色
     */
    ExcelStyleEnum.Color borderColor() default ExcelStyleEnum.Color.BLACK;
    /**
     * 边框颜色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] borderColorCustom() default {};

    /**
     * 单元格顶部边框颜色  默认不设置
     */
    ExcelStyleEnum.Color borderTopColor() default ExcelStyleEnum.Color.AUTOMATIC;
    /**
     * 单元格顶部边框颜色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] borderTopColorCustom() default {};
    /**
     * 单元格右侧边框颜色  默认不设置
     */
    ExcelStyleEnum.Color borderRightColor() default ExcelStyleEnum.Color.AUTOMATIC;
    /**
     * 单元格右侧边框颜色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] borderRightColorCustom() default {};
    /**
     * 单元格底部边框颜色  默认不设置
     */
    ExcelStyleEnum.Color borderBottomColor() default ExcelStyleEnum.Color.AUTOMATIC;
    /**
     * 单元格底部边框颜色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] borderBottomColorCustom() default {};
    /**
     * 单元格左侧边框颜色  默认不设置
     */
    ExcelStyleEnum.Color borderLeftColor() default ExcelStyleEnum.Color.AUTOMATIC;
    /**
     * 单元格左侧边框颜色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] borderLeftColorCustom() default {};

    /**
     * 单元格背景填充样式
     */
    FillPatternType fillPatternType() default FillPatternType.NO_FILL;

    /**
     * 单元格背景填充色  默认不设置
     */
    ExcelStyleEnum.Color fillForegroundColor() default ExcelStyleEnum.Color.AUTOMATIC;
    /**
     * 单元格背景填充色自定义  默认 不设置
     * 数组依次 {R,G,B}
     */
    int[] fillForegroundColorCustom() default {};

//    /**
//     * 单元格向下合并行数 默认不合并
//     * @return
//     */
//    int mergedRegionRow() default -1;
//
//    /**
//     * 单元格向右合并列数 默认不合并
//     * @return
//     */
//    int mergedRegionColumn() default -1;

    /**
     * 标记此字段是否用于记录行号的  默认未否
     * 为true时 在写的文件中都不体现此字段，且以上所有注解配置失效
     */
    boolean isRowNumber() default false;

    /**
     * 设置自适应列宽 默认不设置   xls格式不支持此属性，设置后将不生效
     */
    boolean autoSizeColumn() default false;

    /**
     * 单元格批注内容
     * 文本中使用\n 进行换行操作
     */
    String comment() default "";

    /**
     * 单元格批注人
     */
    String commentAuthor() default "system";

    /**
     * 批注显示占用行数
     */
    int commentRowNum() default 2;

    /**
     * 批注显示占用列数
     */
    int commentCollNum() default 1;

}
