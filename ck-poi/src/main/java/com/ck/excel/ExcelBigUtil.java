package com.ck.excel;

import com.ck.excel.annotation.ExcelTable;
import com.ck.excel.enums.ExcelTypeEnum;
import com.monitorjbl.xlsx.StreamingReader;
import com.monitorjbl.xlsx.impl.StreamingSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;

public class ExcelBigUtil extends ExcelUtil{

    /**
     * 解读 Excel 文件为 Workbook 对象
     *
     * @param workbookType        Excel 文件类型
     * @param inputStream         Excel 文件输入流
     * @return Workbook
     */
    public static Workbook readWorkbook(ExcelTypeEnum workbookType, InputStream inputStream) {
        if (inputStream == null)
            throw new NullPointerException("解读 Workbook 工作簿失败，InputStream is null");
        if (workbookType == null)
            throw new NullPointerException("解读 Workbook 工作簿失败，VExcelWorkbookType is null");
        try {
            if (ExcelTypeEnum.XLS_X.equals(workbookType)) {
                // 创建 StreamingReader 对象
              return StreamingReader.builder()
                        .rowCacheSize(100) // 缓存的行数
                        .bufferSize(4096) // 缓存区大小
                        .open(inputStream); // 打开文件输入流
            } else if (ExcelTypeEnum.XLS.equals(workbookType)) {
                return new HSSFWorkbook(inputStream);
            }
        } catch (IOException e) {
            throw new RuntimeException("解读 Workbook 工作簿异常", e);
        }
        return null;
    }



    public static <T extends @ExcelTable Object> List<T> read(ExcelTypeEnum workbookType, Path path, String sheetName, Charset charset, Class<T> sheetClass) {
        try (FileInputStream in = new FileInputStream(path.toFile())) {
            return read(workbookType, in, sheetName, charset, null, sheetClass);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Excel 文件读取异常:%s", e.getMessage()), e);
        }
    }

    public static <T extends @ExcelTable Object> List<T> read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Class<T> sheetClass) {
        return read(workbookType, in, sheetName, charset, null, sheetClass);
    }

    /**
     * 读取CSV文件内容<br>
     * 每个元素代表一行数据,数组的每个元素代表每个单元格
     *
     * @param workbookType Excel 文件类型 默认为 XLSX
     * @param in           Excel 文件输入流
     * @param sheetName    sheet页名称(Excel 文件解析使用)
     * @param charset      文件字符集(csv解读使用)
     * @param titleRowNum  标题所在行  0 表示第一行，默认自动识别
     * @param sheetClass   接收数据的实体对象类型
     * @param <T>          返回值数据类型
     * @return 解读结果
     */
    public static <T extends @ExcelTable Object> List<T> read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Integer titleRowNum, Class<T> sheetClass) {
        List<T> result = new ArrayList<>();
        read(workbookType, in, sheetName, charset, titleRowNum, sheetClass, (rowNum, rowData) -> result.add(rowData));
        return result;
    }

    public static <T extends @ExcelTable Object> void read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Class<T> sheetClass, BiConsumer<Long, T> consumerRow) {
        read(workbookType, in, sheetName, charset, null, sheetClass, consumerRow);
    }

    /**
     * 解读指定的Sheet页为指定的对象
     *
     * @param workbookType Excel 文件类型 默认为 XLSX
     * @param in           Excel 文件输入流
     * @param sheetName    sheet页名称(Excel 文件解析使用)
     * @param charset      文件字符集(csv解读使用)
     * @param titleRowNum  标题所在行  0 表示第一行，默认自动识别
     * @param sheetClass   接收数据的实体对象类型
     * @param consumerRow  按行处理
     * @param <T>          返回值数据类型
     */
    public static <T extends @ExcelTable Object> void read(ExcelTypeEnum workbookType, InputStream in, String sheetName, Charset charset, Integer titleRowNum, Class<T> sheetClass, BiConsumer<Long, T> consumerRow) {
        if (ExcelTypeEnum.CSV.equals(workbookType)) {
            readCsv(sheetClass, in, charset, titleRowNum, consumerRow);
        } else {
            if (workbookType == null || in == null || sheetClass == null || consumerRow == null) {
                return;
            }
            readExcel(workbookType, in, sheetName, titleRowNum, sheetClass, consumerRow);
        }
    }

    /**
     * 解读指定的Sheet页为指定的对象
     *
     * @param workbookType Excel 文件类型 默认为 XLSX
     * @param in           Excel 文件输入流
     * @param sheetName    sheet页名称
     * @param titleRowNum  标题所在行，默认为第一行
     * @param sheetClass   接收数据的实体对象类型
     * @param consumerRow  按行处理
     * @param <T>          返回值数据类型
     */
    protected static <T extends @ExcelTable Object> void readExcel(ExcelTypeEnum workbookType, InputStream in, String sheetName, Integer titleRowNum, Class<T> sheetClass, BiConsumer<Long, T> consumerRow) {

        if (workbookType == null || in == null || sheetClass == null || consumerRow == null) {
            return;
        }

        ExcelTable excelTable = sheetClass.getDeclaredAnnotation(ExcelTable.class);
        if (excelTable == null) return;

        Workbook workbook = readWorkbook(workbookType, in);
        if (workbook == null)
            throw new NullPointerException("读取工作簿失败，Workbook is null");

        if ((sheetName == null || sheetName.isEmpty()) && !excelTable.sheetName().isEmpty()) {
            sheetName = excelTable.sheetName();
        }

        StreamingSheet sheet = (StreamingSheet) workbook.getSheet(sheetName);

        if (sheet == null)
            throw new NullPointerException(String.format("读取工作表数据失败，Sheet 页[%s]不存在", sheetName));

        // 标题所在行
        AtomicInteger titleRowNumAtomic = new AtomicInteger(excelTable.titleRowNumber() == -1 ? 0 : excelTable.titleRowNumber());
        if (titleRowNum != null)
            titleRowNumAtomic.set(titleRowNum);

        // 获取数据对象参数字典  key：表格中标题名称
        Map<String, Field> sheetClassField = getSheetClassField(sheetClass);

        // 实体类中指定了记录数据行号的字段
        Field rowNumberField = getRowNumberField(sheetClass);

        final Map<Integer, Field> sheetClassFieldByIndex = new HashMap<>();

        AtomicLong rowNumberIndex = new AtomicLong(0);
        sheet.iterator().forEachRemaining(row -> {
            if (row == null) return;
            List<Object> rowData = parseRowValueByType(row);

            // 标题行
            if (titleRowNum != null && rowNumberIndex.get() < titleRowNumAtomic.get()) {
                return;
            } else if (sheetClassFieldByIndex.isEmpty() && (titleRowNum == null || rowNumberIndex.get() == titleRowNumAtomic.get())) {
                Map<Integer, Field> sheetClassFieldByIndexTemp = getSheetClassFieldIndex(sheetClassField, rowData);
                if (sheetClassFieldByIndexTemp != null && !sheetClassFieldByIndexTemp.isEmpty())
                    sheetClassFieldByIndex.putAll(sheetClassFieldByIndexTemp);
                return;
            }

            if (sheetClassFieldByIndex.isEmpty()) {
                throw new RuntimeException("解析CSV文件时 表头未找到，请检查表头是否正确");
            }

            T bean = convertBean(sheetClass, rowNumberField, rowNumberIndex.get(), sheetClassFieldByIndex, rowData);
            consumerRow.accept(rowNumberIndex.get(), bean);

            rowNumberIndex.getAndIncrement();
        });
    }
}
