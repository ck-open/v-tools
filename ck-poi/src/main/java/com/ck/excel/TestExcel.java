package com.ck.excel;

import com.ck.excel.annotation.ExcelCell;
import com.ck.excel.annotation.ExcelTable;
import com.ck.excel.annotation.ExcelTitleStyle;
import com.ck.excel.enums.ExcelStyleEnum;
import com.ck.excel.enums.ExcelTypeEnum;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.poi.ss.usermodel.FillPatternType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TestExcel {
    public static void main(String[] args) {
        try (Excel excel = Excel.build("测试Excel")){
//        try (Excel excel = Excel.build("测试Excel", ExcelTypeEnum.CSV)) {

            File file = new File("C:\\Users\\cyk\\Desktop\\" + excel.getFileName());

            //创建文件
            if (file.createNewFile()) {
                // 文件创建成功
                System.out.printf("文件创建创建成功, Path:%s FileName:%s%n", file.getPath().replaceAll(file.getName(), ""), file.getName());
            }
            if (!file.exists()) {
                throw new RuntimeException(String.format("Excel 文件写出失败,文件不存在且无法创建, Path:%s FileName:%s"
                        , file.getPath().replaceAll(file.getName(), ""), file.getName()));
            }

            long start = System.currentTimeMillis();
            try (FileOutputStream out = new FileOutputStream(file)) {
//                excel.setOnlyField(ExcelBean.class, "name", "age", "sex", "certType", "certNo");
//                excel.addIgnoredField(ExcelBean.class, "name", "age", "sex", "certType", "certNo", "address", "emil", "phone", "salary", "birthday", "dateTime");

                excel.addSheetData(5000, index -> {
                    if (index > 3000) return null;
                    return Collections.singletonList(ExcelBean.createTestBean());
                });
                excel.write(out);
                out.flush();
            }

            System.out.println("写出耗时:" + (System.currentTimeMillis() - start)+"    ");

        } catch (IOException e) {
            throw new RuntimeException(String.format("Excel 文件写出异常:%s", e.getMessage()), e);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }


        long start = System.currentTimeMillis();
        List<ExcelBean> excelBean = ExcelUtil.read(ExcelTypeEnum.XLS_X, Paths.get("C:\\Users\\cyk\\Desktop\\测试Excel.xlsx"), null, null, ExcelBean.class);
//        List<ExcelBean> excelBean = ExcelUtil.read(ExcelTypeEnum.CSV, Paths.get("C:\\Users\\cyk\\Desktop\\测试Excel.csv"), null, null, ExcelBean.class);
        System.out.println("读取耗时:" + (System.currentTimeMillis() - start)+"    ");

    }


    @ExcelTable(value = "测试人员列表", sheetName = "测试人员", titleRowNumber = 2, titleName = "设置的标题", border = ExcelStyleEnum.Border.THIN)
    @Data
    @Accessors(chain = true)
    private static class ExcelBean {

        public static ExcelBean createTestBean() {
            return new ExcelBean()
                    .setName("测试人员" + (Math.random() * 50))
                    .setAge((int) (Math.random() * 10))
                    .setSex(Math.random() > 0.5 ? "男" : "女")
                    .setCertType("身份证")
                    .setCertNo("5452132321564531351132")
                    .setAddress("北京市朝阳区随机社区你猜小区不知道单元")
                    .setPhone("4154523232").setSalary(new BigDecimal((int) (Math.random() * 100))).setBirthday(new Date());
        }

        @ExcelTitleStyle(fontColor = ExcelStyleEnum.Color.AQUA)
        @ExcelCell(value = "姓名", fillPatternType = FillPatternType.SOLID_FOREGROUND, fillForegroundColor = ExcelStyleEnum.Color.LIGHT_CORNFLOWER_BLUE)
        private String name;
        @ExcelTitleStyle
        @ExcelCell(value = "年龄",order = 1, fontColor = ExcelStyleEnum.Color.AUTOMATIC)
        private Integer age;
        @ExcelTitleStyle
        @ExcelCell(value = "性别",order = 2, fontColor = ExcelStyleEnum.Color.BLUE, border = ExcelStyleEnum.Border.MEDIUM, comment = "1男\n2女")
        private String sex;
        @ExcelTitleStyle
        @ExcelCell(value = "证件类型",order = 4, fontColor = ExcelStyleEnum.Color.LIGHT_TURQUOISE, border = ExcelStyleEnum.Border.MEDIUM)
        private String certType;
        @ExcelTitleStyle
        @ExcelCell(value = "证件号",order = 3, autoSizeColumn = true, fontColor = ExcelStyleEnum.Color.LIGHT_CORNFLOWER_BLUE)
        private String certNo;
        @ExcelTitleStyle
        @ExcelCell(value = "地址", fontColor = ExcelStyleEnum.Color.ROYAL_BLUE)
        private String address;
        @ExcelTitleStyle
        @ExcelCell(value = "邮箱", fontColor = ExcelStyleEnum.Color.GREY_40_PERCENT)
        private String emil;
        @ExcelTitleStyle
        @ExcelCell(value = "电话", fontColor = ExcelStyleEnum.Color.AQUA)
        private String phone;
        @ExcelCell(value = "工资", fontColor = ExcelStyleEnum.Color.AQUA)
        private BigDecimal salary;
        @ExcelCell(value = "生日", autoSizeColumn = true, fontColor = ExcelStyleEnum.Color.AQUA)
        private Date birthday;
        @ExcelCell(value = "生日2", autoSizeColumn = true, fontColor = ExcelStyleEnum.Color.AQUA)
        private LocalDateTime dateTime = LocalDateTime.now();

        @ExcelCell(isRowNumber = true)
        private int index;
    }
}


