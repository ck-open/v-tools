package com.ck.excel;

import com.ck.excel.annotation.ExcelCell;
import com.ck.excel.annotation.ExcelTable;
import com.ck.excel.enums.ExcelCellFormatEnum;
import com.ck.excel.enums.ExcelTypeEnum;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Excel 操作工具入口
 *
 * @author Cyk
 * @version 1.0
 * @since 2022/9/4 18:30
 * <p>
 * 基于apache poi构建，试用前需引入以下依赖
 * <!--操作Excel依赖jar包 -->
 * <dependency>
 * <groupId>org.apache.poi</groupId>
 * <artifactId>poi-excelant</artifactId>
 * <version>4.1.0</version>
 * </dependency>
 * <dependency>
 * <groupId>org.apache.poi</groupId>
 * <artifactId>poi</artifactId>
 * <!--            <version>3.17</version>-->
 * <version>4.1.0</version>
 * </dependency>
 **/
public class Excel implements AutoCloseable {

    private final Logger log = Logger.getLogger(Excel.class.getName());

    public static Excel build() {
        return build(ExcelTypeEnum.XLS_X);
    }

    public static Excel build(ExcelTypeEnum excelTypeEnum) {
        return build(null, excelTypeEnum);
    }

    public static Excel build(String fileName) {
        return build(fileName, ExcelTypeEnum.XLS_X);
    }

    /**
     * 构建VExcel 对象
     *
     * @param fileName      文件名称
     * @param excelTypeEnum 文件格式 {@link ExcelTypeEnum}
     * @return Excel 数据对象
     */
    public static Excel build(String fileName, ExcelTypeEnum excelTypeEnum) {
        return build(fileName, excelTypeEnum, null);
    }

    /**
     * 构建VExcel 对象
     *
     * @param fileName            文件名称
     * @param excelTypeEnum       文件格式 {@link ExcelTypeEnum}
     * @param rowAccessWindowSize poi 3.8 以后版本用于解决大数据量内存溢出的问题，写出时每100行flush一次清空内存。但是 xls 格式文件不支持。
     * @return Excel 数据对象
     */
    public static Excel build(String fileName, ExcelTypeEnum excelTypeEnum, Integer rowAccessWindowSize) {
        Excel excel = new Excel();

        if (fileName != null && !fileName.isEmpty())
            excel.fileName = fileName;

        if (excelTypeEnum != null)
            excel.workbookType = excelTypeEnum;

        if (rowAccessWindowSize != null)
            excel.rowAccessWindowSize = rowAccessWindowSize;

        return excel;
    }

    /**
     * 需要创建的文件类型和  默认xlsx格式文件
     */
    private String fileName = "ExcelExport";

    /**
     * 需要创建的文件类型和  默认xlsx格式文件
     */
    private ExcelTypeEnum workbookType = ExcelTypeEnum.XLS_X;

    /**
     * 字符集 默认UTF-8
     */
    private Charset charset = Charset.forName("GBK");

    /**
     * poi 3.8 以后版本用于解决大数据量内存溢出的问题，写出时每 rowAccessWindowSize 行 flush 一次清空内存转存到磁盘。
     * 但是 xls 格式文件不支持。
     * 样本数据：5000行
     * 值1000时，  耗时:146805毫秒
     * 值200时，   耗时:28098 毫秒
     * 值100时，   耗时:16417 毫秒
     * 值50时，    耗时:11134 毫秒
     * 值20时，    耗时:7015  毫秒
     * 值10时，    耗时:5936  毫秒
     */
    private int rowAccessWindowSize = 20;

    /**
     * 工作簿
     */
    private Workbook workbook;

    /**
     * csv 文件缓存 仅操作csv文件时使用
     */
    private List<String> csvRowCache;

    /**
     * csv 写出时的临时缓存文件，用于释放大文件对内存的占用
     */
    private File tempFileCsv;

    /**
     * sheet 数据页配置
     */
    private Map<Class<? extends @ExcelTable Object>, List<Sheet>> sheetClassMap;

    /**
     * 忽略的字段 即使有注解也将会被忽略
     */
    private Map<Class<? extends @ExcelTable Object>, Set<String>> sheetIgnoreFieldMap;

    /**
     * 工作薄 样式
     */
    private Map<Class<? extends @ExcelTable Object>, Map<String, CellStyle>> sheetCellStyleMap;

    /**
     * 创建工作簿对象
     */
    private void createWorkbook() {
        if (this.workbook == null) {
            if (this.workbookType == null) {
                throw new RuntimeException("Workbook 创建异常，未指定 WorkbookType");
            }
            if (ExcelTypeEnum.XLS_X.equals(this.workbookType)) {
//            this.workbook = new XSSFWorkbook();
                this.workbook = new SXSSFWorkbook(this.rowAccessWindowSize); // poi 3.8 以后版本用于解决大数据量内存溢出的问题，写出时每100行flush一次清空内存
            } else if (ExcelTypeEnum.XLS.equals(this.workbookType)) {
                this.workbook = new HSSFWorkbook();
            }
        }
    }

    /**
     * 获取文件名
     *
     * @return 文件名 默认：ExcelExport.xlsx
     */
    public String getFileName() {
        if (this.fileName.endsWith(this.workbookType.getFileSuffix())) {
            return this.fileName;
        } else {
            return this.fileName + this.workbookType.getFileSuffix();
        }
    }

    /**
     * 设置文件编码
     *
     * @param charset 文件编码 {@link java.nio.charset.StandardCharsets #StandardCharsets.UTF_8} 或 {@link java.nio.charset.Charset #Charset.forName("GBK")}
     */
    public Excel setCharset(Charset charset) {
        this.charset = charset;
        return this;
    }

    public Excel setOnlyField(Class<? extends @ExcelTable Object> sheetClass, String... onlyFields) {
        if (onlyFields == null || onlyFields.length == 0)
            return this;
        return setOnlyField(sheetClass, Arrays.asList(onlyFields));
    }

    /**
     * 添加sheet只显示的字段
     * 需要在 addSheet() 方法调用前 设置
     *
     * @param sheetClass 数据类型
     * @param onlyFields 只显示的字段
     * @return Excel 处理对象
     */
    public Excel setOnlyField(Class<? extends @ExcelTable Object> sheetClass, Collection<String> onlyFields) {
        if (sheetClass != null && onlyFields != null && !onlyFields.isEmpty()) {
            ExcelUtil.getFields(sheetClass).stream()
                    .filter(field -> !onlyFields.contains(field.getName()))
                    .forEach(field -> addIgnoredField(sheetClass, field.getName()));
        }
        return this;
    }

    public Excel addIgnoredField(Class<? extends @ExcelTable Object> sheetClass, String... ignoredFields) {
        if (ignoredFields == null || ignoredFields.length == 0)
            return this;
        return addIgnoredField(sheetClass, Arrays.asList(ignoredFields));
    }

    /**
     * 添加sheet忽略的字段
     * 需要在 addSheet() 方法调用前 设置
     *
     * @param sheetClass    数据类型
     * @param ignoredFields 忽略的字段
     * @return Excel 处理对象
     */
    public Excel addIgnoredField(Class<? extends @ExcelTable Object> sheetClass, Collection<String> ignoredFields) {
        if (ignoredFields != null && !ignoredFields.isEmpty()) {
            if (this.sheetIgnoreFieldMap == null) this.sheetIgnoreFieldMap = new HashMap<>();

            ignoredFields.forEach(fieldName -> {
                this.sheetIgnoreFieldMap.computeIfAbsent(sheetClass, k -> new HashSet<>())
                        .add(fieldName);
            });
        }

        // 删除记录的sheet页字段样式
        if (this.sheetCellStyleMap != null && this.sheetIgnoreFieldMap != null && this.sheetIgnoreFieldMap.get(sheetClass) != null) {
            if (this.sheetCellStyleMap.containsKey(sheetClass)) {
                this.sheetIgnoreFieldMap.get(sheetClass).forEach(fieldName -> this.sheetCellStyleMap.get(sheetClass).remove(fieldName));
            }
        }

        return this;
    }

    /**
     * 判断是否忽略字段
     *
     * @param sheetClass 数据类型
     * @param fieldName  字段名
     * @return 是否忽略 true 忽略 false 不忽略
     */
    private boolean isIgnoredField(Class<? extends @ExcelTable Object> sheetClass, String fieldName) {
        return this.sheetIgnoreFieldMap != null && this.sheetIgnoreFieldMap.getOrDefault(sheetClass, Collections.emptySet()).contains(fieldName);
    }

    /**
     * 获取 sheet
     *
     * @param sheetClass 数据类型
     * @return sheet
     */
    private Sheet getSheet(Class<? extends @ExcelTable Object> sheetClass) {
        if (this.sheetClassMap == null || this.sheetClassMap.get(sheetClass) == null || (!ExcelTypeEnum.CSV.equals(this.workbookType) && this.sheetClassMap.get(sheetClass).isEmpty())) {
            addSheet(sheetClass);
        }
        // 返回最后一个创建的 sheet
        return ExcelTypeEnum.CSV.equals(this.workbookType) ? null : this.sheetClassMap.get(sheetClass).get(this.sheetClassMap.get(sheetClass).size() - 1);
    }

    @SafeVarargs
    public final Excel addSheet(Class<? extends @ExcelTable Object>... sheetClass) {
        if (sheetClass != null && sheetClass.length > 0)
            return addSheet(Arrays.asList(sheetClass));
        return this;
    }

    /**
     * 添加 sheet 数据类型
     *
     * @param sheetClass 数据类型
     * @return Excel 处理对象
     */
    public Excel addSheet(Collection<Class<? extends @ExcelTable Object>> sheetClass) {
        try {
            if (sheetClass != null && !sheetClass.isEmpty()) {
                if (this.sheetClassMap == null) this.sheetClassMap = new LinkedHashMap<>();
                if (this.sheetCellStyleMap == null) this.sheetCellStyleMap = new LinkedHashMap<>();


                if (ExcelTypeEnum.CSV.equals(this.workbookType)) {
                    if (sheetClass.size() > 1 || this.sheetClassMap.containsKey(sheetClass.iterator().next())) {
                        throw new RuntimeException("CSV 模式下，只支持一个sheet");
                    }

                    Class<? extends @ExcelTable Object> sheetClassTemp = sheetClass.iterator().next();
                    this.sheetClassMap.put(sheetClassTemp, new ArrayList<>());

                    List<String> title = new ArrayList<>();
                    for (Field field : ExcelUtil.getFields(sheetClassTemp)) {
                        // 读取对象 ExcelCell 注解
                        ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);

                        if (excelCell == null || excelCell.isRowNumber() || isIgnoredField(sheetClassTemp, field.getName()))
                            continue;

                        title.add("".equals(excelCell.value()) ? field.getName() : excelCell.value());
                    }

                    if (this.csvRowCache == null) {
                        this.csvRowCache = new ArrayList<>();
                    }
                    if (!title.isEmpty()) {
                        this.csvRowCache.add(String.join(",", title));
                    }

                } else {
                    if (this.workbook == null) {
                        createWorkbook();
                    }

                    sheetClass.forEach(sheetClassItem -> {
                        // 读取类注解
                        ExcelTable excelTable = sheetClassItem.getDeclaredAnnotation(ExcelTable.class);

                        List<Sheet> sheetList = this.sheetClassMap.computeIfAbsent(sheetClassItem, k -> new ArrayList<>());

                        // sheet 页已存在时不再创建  如果行数到达上限 则创建副本
                        Sheet sheet = sheetList.isEmpty() ? null : sheetList.get(sheetList.size() - 1);
                        if (sheet != null) {
                            sheet = this.workbook.createSheet(sheetList.get(0).getSheetName() + "(" + (sheetList.size() + 1) + ")");
                            log.info("sheet 页已存在，创建新页：" + sheet.getSheetName());
                        } else if (excelTable.sheetName() == null || excelTable.sheetName().isEmpty()) {
                            sheet = this.workbook.createSheet();
                        } else {
                            sheet = this.workbook.createSheet(excelTable.sheetName());
                        }
                        sheetList.add(sheet);

                        // 创建sheet页中标题信息

                        // 记录包含Cell 注解的Field 名称 及对应的单元格样式对象CellStyle
                        Map<String, CellStyle> fieldStyle = ExcelStyleUtil.parseCellStyle(sheet, sheetClassItem);
                        this.sheetCellStyleMap.put(sheetClassItem, fieldStyle);
                        addIgnoredField(sheetClassItem);  // 用于检查排除是否有 需要忽悠的字段

                        // 数据第一行 行号小于指定的数据所在行 则按照指定的行号创建
                        int firstRowNumber = Math.max(0, Math.max(sheet.getLastRowNum(), excelTable.titleRowNumber()));
                        Row row = null;

                        // 需要写出的标题字段列表
                        List<Field> titleFields = ExcelUtil.getFields(sheetClassItem).stream()
                                .filter(field -> fieldStyle.containsKey(field.getName()) && !isIgnoredField(sheetClassItem, field.getName()))
                                .collect(Collectors.toList());

                        // 设置了标题行数  并且 行数大于0 则在设置的行数上一行创建一行 用作创建大标题
                        if (excelTable.titleName() != null && !excelTable.titleName().trim().isEmpty() && firstRowNumber > 0) {
                            row = sheet.createRow(--firstRowNumber);

                            // 设置标题
                            ExcelUtil.mergedRegion(sheet, row.getRowNum(), row.getRowNum(), 0, titleFields.size() - 1);
                            CellStyle style = ExcelStyleUtil.setCellStyle(sheet, excelTable);
                            for (int i = 0; i < titleFields.size(); i++)
                                row.createCell(i).setCellStyle(style);
                            ExcelUtil.setValByType(row.getCell(0), null, excelTable.value());

                            row = sheet.createRow(sheet.getLastRowNum() + 1);
                        } else {
                            row = sheet.createRow(firstRowNumber);
                        }

                        // 设置表头
                        Map<String, CellStyle> titleStyle = ExcelStyleUtil.setCellStyleTitle(sheet, sheetClassItem);
                        for (Field field : titleFields) {
                            // 读取对象 ExcelCell 注解
                            ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);

                            if (excelCell == null || excelCell.isRowNumber() || isIgnoredField(sheetClassItem, field.getName()))
                                continue;

                            // 设置单元格值
                            Cell cell = row.createCell(row.getLastCellNum() < 0 ? 0 : row.getLastCellNum());
                            ExcelUtil.setValByType(cell, excelCell, excelCell.value().trim().isEmpty() ? field.getName() : excelCell.value());
                            ExcelUtil.setCellComment(workbook, sheet, cell, excelCell);
                            cell.setCellStyle(titleStyle.containsKey(field.getName()) ? titleStyle.get(field.getName()) : fieldStyle.get(field.getName()));
                        }
                    });
                }
            }
        } catch (Exception e) {
            log.warning("sheet数据写入失败" + e.getMessage());
            closeRunError();
            throw new RuntimeException(e);
        }
        return this;
    }

    /**
     * 添加 sheet 数据
     *
     * @param sheetData 数据
     */
    @SafeVarargs
    public final <T extends @ExcelTable Object> Excel addSheetData(T... sheetData) {
        if (sheetData == null || sheetData.length == 0)
            return this;
        return addSheetData(Arrays.asList(sheetData));
    }

    /**
     * 批量添加 sheet 数据
     *
     * @param batchSize 批次大小  缓存在内存中，超过指定大小，则写入文件  默认为{@link rowAccessWindowSize}
     * @param fun       数据  返回null时结束循环添加  参数为 当前循环的次数 从0开始
     */
    public <T extends @ExcelTable Object> Excel addSheetData(int batchSize, Function<Integer, List<T>> fun) {
        try {
            if (fun != null) {
                List<T> batchData = new ArrayList<>();

                batchSize = Math.max(batchSize, this.rowAccessWindowSize);

                int index = 0;
                while (true) {
                    List<T> data = fun.apply(index++);
                    if (data == null || data.isEmpty()) break;
                    batchData.addAll(data);
                    if (batchData.size() >= batchSize) {
                        addSheetData(batchData);
                        batchData.clear();
                    }
                }
                if (!batchData.isEmpty())
                    addSheetData(batchData);
            }
        } catch (Exception e) {
            log.warning("sheet数据写入失败" + e.getMessage());
            closeRunError();
            throw new RuntimeException(e);
        }
        return this;
    }

    /**
     * 添加 sheet 数据
     *
     * @param sheetData 数据
     */
    public <T extends @ExcelTable Object> Excel addSheetData(List<T> sheetData) {
        try {
            synchronized (this) {
                if (ExcelTypeEnum.CSV.equals(this.workbookType)) {
                    return addSheetDataCsv(sheetData);
                } else {
                    return addSheetDataExcel(sheetData);
                }
            }
        } catch (Exception e) {
            log.warning("sheet数据写入失败" + e.getMessage());
            closeRunError();
            throw new RuntimeException(e);
        }
    }

    /**
     * 添加 sheet 数据
     *
     * @param sheetData 数据
     */
    private <T extends @ExcelTable Object> Excel addSheetDataCsv(List<T> sheetData) {
        if (sheetData != null && !sheetData.isEmpty()) {
            Class<? extends @ExcelTable Object> sheetDataClass = sheetData.get(0).getClass();

            getSheet(sheetDataClass);

            Map<String, SimpleDateFormat> simpleDateFormatMap = new HashMap<>(); // 日期格式
            List<Field> rowCellField = new ArrayList<>();
            for (Field field : ExcelUtil.getFields(sheetDataClass)) {
                ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);

                if (excelCell == null || excelCell.isRowNumber() || isIgnoredField(sheetDataClass, field.getName()))
                    continue;

                field.setAccessible(true);
                rowCellField.add(field);

                if (Date.class.isAssignableFrom(field.getType())) {
                    simpleDateFormatMap.put(field.getName(), new SimpleDateFormat(excelCell.cellFormatCustom().trim().isEmpty() ? ExcelCellFormatEnum.FORMAT_DATE_TIME.getFormat() : excelCell.cellFormatCustom()));
                }
            }

            for (Object rowData : sheetData) {
                try {

                    List<String> title = new ArrayList<>();
                    for (Field field : rowCellField) {
                        ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
                        Object val = field.get(rowData);

                        if (val != null) {
                            if (Date.class.getSimpleName().equals(field.getType().getSimpleName())) {
                                val = simpleDateFormatMap.get(field.getName()).format(val);
                            }
                            val = ExcelUtil.setValByType(excelCell, val);
                            title.add(String.valueOf(val));
                        } else {
                            title.add("");
                        }
                    }

                    if (!title.isEmpty()) {
                        this.csvRowCache.add(String.join(",", title));
                    }

                    if (this.csvRowCache != null && this.csvRowCache.size() >= this.rowAccessWindowSize) {
                        cacheLoadFileCsv();
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return this;
    }

    /**
     * 添加 sheet 数据
     *
     * @param sheetData 数据
     */
    private <T extends @ExcelTable Object> Excel addSheetDataExcel(List<T> sheetData) {

        try {
            if (sheetData != null && !sheetData.isEmpty()) {
                Class<? extends @ExcelTable Object> sheetDataClass = sheetData.get(0).getClass();

                // 读取类注解ETable
                ExcelTable excelTable = sheetDataClass.getDeclaredAnnotation(ExcelTable.class);

                Sheet sheet = getSheet(sheetDataClass);

                if (sheet == null) {
                    return this;
                }

                // 已有行数 加上本次行数 大于等于 sheet 页最大行数则创建副本
                if (sheet.getLastRowNum() + sheetData.size() >= this.workbookType.getRowMaxNum()) {
                    sheet = addSheet(sheetDataClass).getSheet(sheetDataClass);
                }

                // 获取 注解的Field 名称 及对应的单元格样式对象CellStyle
                Map<String, CellStyle> fieldStyle = this.sheetCellStyleMap.get(sheetDataClass);

                for (Object rowData : sheetData) {
                    Row row = sheet.createRow(sheet.getLastRowNum() + 1);
                    // 设置行高
                    if (excelTable.rowHeight() != -1) {
                        sheet.getRow(row.getRowNum()).setHeightInPoints(excelTable.rowHeight());
                    }

                    for (Field field : ExcelUtil.getFields(rowData.getClass())) {

                        if (!fieldStyle.containsKey(field.getName()) || isIgnoredField(sheetDataClass, field.getName()))
                            continue;

                        ExcelCell excelCell = field.getDeclaredAnnotation(ExcelCell.class);
                        // 设置单元格值
                        Cell cell = row.createCell(row.getLastCellNum() < 0 ? 0 : row.getLastCellNum());
                        field.setAccessible(true);
                        ExcelUtil.setValByType(cell, excelCell, field.get(rowData));
                        cell.setCellStyle(fieldStyle.get(field.getName()));
                    }
                }

                // 设置自适应宽度
                ExcelUtil.setAutoSizeColumn(sheet, sheetDataClass);
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException("添加 Sheet 数据异常", e);
        }

        return this;
    }

    /**
     * 文件写出
     *
     * @param file 文件
     */
    public void write(File file) {
        try {
            if (file == null) {
                throw new NullPointerException("Excel 文件写出失败,File is null");
            }
            //创建文件
            if (file.createNewFile()) {
                // 文件创建成功
                log.info(String.format("文件创建创建成功, Path:%s FileName:%s", file.getPath().replaceAll(file.getName(), ""), file.getName()));
            }
            if (!file.exists()) {
                throw new RuntimeException(String.format("Excel 文件写出失败,文件不存在且无法创建, Path:%s FileName:%s"
                        , file.getPath().replaceAll(file.getName(), ""), file.getName()));
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("Excel 文件写出异常:%s", e.getMessage()), e);
        }

        try (FileOutputStream out = new FileOutputStream(file);) {
            write(out);
            out.flush();
        } catch (IOException e) {
            throw new RuntimeException(String.format("Excel 文件写出异常:%s", e.getMessage()), e);
        }
    }

    /**
     * 文件写出
     *
     * @param outputStream 文件写出输出流
     */
    public void write(OutputStream outputStream) {
        try {
            if (ExcelTypeEnum.CSV.equals(this.workbookType)) {
                if (this.tempFileCsv == null || (this.csvRowCache != null && !this.csvRowCache.isEmpty())) {
                    cacheLoadFileCsv();
                }
                if (this.tempFileCsv == null) {
                    throw new RuntimeException("Excel 文件写出异常,未设置 csv 缓存文件为空");
                }

                try (FileInputStream fis = new FileInputStream(this.tempFileCsv)) {
                    ExcelUtil.io(fis, outputStream, 4);
                }
            } else if (this.workbook != null) {
                this.workbook.write(outputStream);
                this.workbook.close();
            }
        } catch (IOException e) {
            throw new RuntimeException("Excel 文件写出异常:" + e.getMessage(), e);
        } finally {
            closeRunError();
        }
    }

    private void cacheLoadFileCsv() {
        try {
            // 创建临时文件
            if (this.tempFileCsv == null) {
                this.tempFileCsv = File.createTempFile("TempDataCsv_" + UUID.randomUUID() + "_" + this.hashCode(), ".tmp");
            }

            // 写入数据到临时文件
            try (FileOutputStream fos = new FileOutputStream(this.tempFileCsv, true)) {
                if (this.csvRowCache != null && !this.csvRowCache.isEmpty()) {
                    for (String row : this.csvRowCache) {
                        row = row + "\r\n";
                        byte[] bytes = row.getBytes(this.charset);
                        ExcelUtil.io(bytes, fos, 4);
                    }
                    this.csvRowCache.clear();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭异常
     */
    private void closeRunError() {
        try {
            this.close();
        } catch (Exception e) {
            log.warning("Excel 关闭异常:" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        this.sheetIgnoreFieldMap = null;
        this.sheetCellStyleMap = null;
        this.sheetClassMap = null;

        if (this.workbook != null) {
            this.workbook.close();
        }

        // 缓存文件不为空 并且 存在 则删除
        if (this.tempFileCsv != null && this.tempFileCsv.exists()) {
            if (this.tempFileCsv.delete()) {
                log.info("临时文件删除成功:" + this.tempFileCsv.getAbsolutePath());
            } else {
                log.info("临时文件删除失败:" + this.tempFileCsv.getAbsolutePath());
            }
        }
    }
}
