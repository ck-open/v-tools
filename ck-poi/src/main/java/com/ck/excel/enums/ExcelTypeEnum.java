package com.ck.excel.enums;

import lombok.Getter;

/**
 * 文档类型
 *
 * @author Cyk
 * @version 1.0
 * @since 2022/9/4 18:30
 **/
@Getter
public enum ExcelTypeEnum {
    XLS("旧版Excel", ".xls", 65536, 256),
    XLS_X("新版Excel", ".xlsx", 1048576, 16384),
    CSV("逗号分割", ".csv", false, -1, -1);


    /**
     * 类型名称
     */
    private final String name;
    /**
     * 类型文件后缀
     */
    private final String fileSuffix;
    /**
     * 文档是否支持样式
     */
    private final boolean isStyle;
    /**
     * 文档支持最大行数
     */
    private final int RowMaxNum;
    /**
     * 文档支持最大列数
     */
    private final int ColumnMaxNum;

    ExcelTypeEnum(String name, String fileSuffix, int rowMaxNum, int columnMaxNum) {
        this(name, fileSuffix, true, rowMaxNum, columnMaxNum);
    }

    ExcelTypeEnum(String name, String fileSuffix, boolean isStyle, int rowMaxNum, int columnMaxNum) {
        this.name = name;
        this.fileSuffix = fileSuffix;
        this.isStyle = isStyle;
        RowMaxNum = rowMaxNum;
        ColumnMaxNum = columnMaxNum;
    }
}
