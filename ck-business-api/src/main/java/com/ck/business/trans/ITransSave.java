package com.ck.business.trans;


import com.ck.business.trans.entity.vo.IBaseBo;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * 保存输入、输出、业务数据对象
 *
 * @param <InDto>  输入数据Dto
 * @param <OutDto> 输出数据Dto
 * @param <Bo>     业务对象
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
public interface ITransSave<InDto, OutDto, Bo extends IBaseBo> {

    /**
     * 获取优先级,多个实例时按照返回值升序排序依次执行
     *
     * @return 优先级
     */
    default int getTransOrder() {
        Order order = this.getClass().getAnnotation(Order.class);
        if (order != null) {
            return order.value();
        }
        return Ordered.LOWEST_PRECEDENCE;
    }

    /**
     * 是否支持
     *
     * @param inDto 输入数据对象
     * @return 是否支持
     */
    boolean isSupport(InDto inDto);

    /**
     * 输入数据对象保存
     *
     * @param inDto 输入数据对象
     * @return 保存成功返回true，否则返回false
     */
    boolean saveInDto(InDto inDto);

    /**
     * 业务数据对象保存
     *
     * @param bo 业务对象
     * @return 保存成功返回true，否则返回false
     */
    boolean saveBo(Bo bo);

    /**
     * 输出数据对象保存
     *
     * @param outDto 输出数据对象
     * @return 保存成功返回true，否则返回false
     */
    boolean saveOutDto(OutDto outDto);
}
