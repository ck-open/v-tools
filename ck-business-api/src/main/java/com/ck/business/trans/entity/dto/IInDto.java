package com.ck.business.trans.entity.dto;

/**
 * 入参参数约束
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
public interface IInDto {

    /**
     * 获取分布式锁的key
     *
     * @return 锁的key
     */
    String getLockKey();
}
