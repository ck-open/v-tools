package com.ck.business.trans;

import com.ck.business.trans.entity.vo.IBaseBo;
import com.ck.tools.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.function.Consumer;
import java.util.function.Function;


/**
 * 基础业务处理流程
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
public interface IBusiness<InDto, OutDto, Bo extends IBaseBo> {

    /**
     * 锁前缀
     *
     * @return 默认 IBusiness:lock:
     */
    default String lockKeyPrefix() {
        return "IBusiness-lock";
    }

    /**
     * 业务处理分布式锁
     *
     * @param inDto 输入数据Dto
     * @return 分布式锁处理结果 true 获取锁成功 false 获取锁失败
     */
    default boolean lock(InDto inDto) {
        return true;
    }

    /**
     * 业务处理幂等性
     *
     * @param inDto 输入数据Dto
     * @return 幂等性处理结果 true 已处理过 false 未处理过
     */
    default boolean idempotent(InDto inDto) {
        return false;
    }

    /**
     * 幂等时处理函数
     * 默认抛出异常，也可以重写返回上次处理结果
     *
     * @param inDto 输入数据Dto
     * @return 幂等处理结果
     */
    default OutDto idempotentFunction(InDto inDto) {
        throw new BusinessException("幂等验证失败");
    }

    /**
     * 保存请求数据
     *
     * @param inDto 输入数据Dto
     * @return 保存请求数据结果 true 保存成功 false 保存失败
     */
    default boolean saveInDto(InDto inDto) {
        return true;
    }

    /**
     * 输入数据转换业务对象
     *
     * @param inDto 输入数据Dto
     * @return 业务对象
     */
    Bo convertBo(InDto inDto);

    /**
     * 初始化业务配置
     *
     * @param bo    业务对象
     * @param inDto 输入数据Dto
     * @return 业务对象
     */
    default Bo initConfig(Bo bo, InDto inDto) {
        return bo;
    }

    /**
     * 请求数据业务检查
     *
     * @param bo    业务对象
     * @param inDto 输入数据Dto
     * @return 业务检查结果 true 通过 false 不通过
     */
    default Bo check(Bo bo, InDto inDto) {
        return bo;
    }

    /**
     * 执行业务处理器
     *
     * @param bo 业务对象
     * @return 业务对象
     */
    Bo doHandle(Bo bo);

    /**
     * 业务对象转换输出数据Dto
     *
     * @param bo    业务对象
     * @param inDto 输入数据Dto
     * @return 输出数据Dto
     */
    OutDto convertOutDto(Bo bo, InDto inDto);

    /**
     * 保存业务处理数据
     *
     * @param bo    业务对象
     * @param inDto 输入数据Dto
     * @return 保存业务处理数据结果 true 保存成功 false 保存失败
     */
    default boolean saveBo(Bo bo, InDto inDto) {
        return true;
    }

    /**
     * 保存输出数据
     *
     * @param inDto  输入数据Dto
     * @param outDto 输出数据Dto
     * @return 保存输出数据结果 true 保存成功 false 保存失败
     */
    default boolean saveOutDto(InDto inDto, OutDto outDto) {
        return true;
    }

    /**
     * 释放分布式锁
     *
     * @param inDto 输入数据Dto
     * @return 释放分布式锁处理结果 true 释放成功 false 释放失败
     */
    default boolean unlock(InDto inDto) {
        return true;
    }

    /**
     * 执行业务流程
     * 获取分布式锁 => 幂等验证处理 => 保存请求数据 => 请求数据Dto转换业务对象Bo => 初始化业务配置 => 请求数据业务检查
     * => 执行业务处理器 => 业务对象转换输出数据Dto => 保存输出数据 => 释放分布式锁 => 保存业务处理数据
     *
     * @param inDto 输入数据Dto
     * @return 输出数据Dto
     */
    default OutDto run(InDto inDto) {
        Logger log = LoggerFactory.getLogger(this.getClass());

        return stopWatch("IBusiness RUN", stopWatch -> {
            Bo bo = null;
            try {
                stopWatch.start("获取分布式锁");
                if (lock(inDto)) {

                    stopWatch.stop();
                    stopWatch.start("幂等验证");
                    if (idempotent(inDto)) {
                        return idempotentFunction(inDto);
                    }

                    stopWatch.stop();
                    stopWatch.start("保存请求数据");
                    if (!saveInDto(inDto)) {
                        throw new BusinessException("保存请求数据失败");
                    }

                    stopWatch.stop();
                    stopWatch.start("请求数据Dto转换业务对象Bo");
                    bo = convertBo(inDto);

                    stopWatch.stop();
                    stopWatch.start("加载配置");
                    if (bo != null && bo.getNextStatus()) {
                        bo = initConfig(bo, inDto);
                    }

                    stopWatch.stop();
                    stopWatch.start("业务规则检查校验");
                    if (bo != null && bo.getNextStatus()) {
                        bo = check(bo, inDto);
                    }

                    stopWatch.stop();
                    stopWatch.start("执行业务处理器");
                    if (bo != null && bo.getNextStatus()) {
                        bo = doHandle(bo);
                    }

                    stopWatch.stop();
                    stopWatch.start("业务对象转换输出数据Dto");
                    OutDto outDto = convertOutDto(bo, inDto);

                    stopWatch.stop();
                    stopWatch.start("保存输出数据");
                    if (!saveOutDto(inDto, outDto)) {
                        log.error("保存输出数据失败");
                    }

                    return outDto;
                } else {
                    throw new BusinessException("分布式锁获取失败");
                }

            } finally {
                if (stopWatch.isRunning()) {
                    stopWatch.stop();
                }
                stopWatch.start("释放分布式锁");
                if (!unlock(inDto)) {
                    log.error("分布式锁释放失败");
                }
                stopWatch.stop();

                stopWatch.start("保存业务对象数据");
                if (!saveBo(bo, inDto)) {
                    log.error("保存业务对象数据失败");
                }
            }
        });
    }


    /**
     * 封装StopWatch
     *
     * @param stopWatchName     StopWatch 名称
     * @param stopWatchConsumer StopWatch 处理逻辑
     */
    default void stopWatch(String stopWatchName, Consumer<StopWatch> stopWatchConsumer) {
        stopWatch(stopWatchName, stopWatch -> {
            stopWatchConsumer.accept(stopWatch);
            return null;
        });
    }

    /**
     * 封装StopWatch
     *
     * @param stopWatchName     StopWatch 名称
     * @param stopWatchConsumer StopWatch 处理逻辑
     * @return 处理结果
     */
    default <R> R stopWatch(String stopWatchName, Function<StopWatch, R> stopWatchConsumer) {
        StopWatch stopWatch = new StopWatch(String.format("%s", stopWatchName));
        try {
            return stopWatchConsumer.apply(stopWatch);
        } finally {
            if (stopWatch.isRunning()) {
                stopWatch.stop();
            }
            if (stopWatchPrint()) {
                Logger log = LoggerFactory.getLogger(this.getClass());
                log.info("{}{} end, Total Time: {}ms", stopWatch.prettyPrint(), stopWatchName, stopWatch.getTotalTimeMillis());
            }
        }
    }

    /**
     * 是否打印StopWatch 处理链路时间切点
     *
     * @return true 打印 false 不打印
     */
    default boolean stopWatchPrint() {
        return false;
    }

    /**
     * Body 密文解密
     *
     * @param body 业务请求数据
     * @return 解密后的数据
     */
    default InDto decode(String body) {
        return null;
    }

    /**
     * 响应Body 报文 加密
     *
     * @param outDto 输出数据Dto
     * @return 加密后的数据
     */
    default String encrypt(OutDto outDto) {
        return null;
    }

    /**
     * 密文传输处理
     *
     * @param body 密文传输报文
     * @return 密文传输报文
     */
    default String runEncryption(String body) {
        return encrypt(run(decode(body)));
    }
}
