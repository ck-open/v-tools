package com.ck.business.trans.entity.vo;

import java.util.List;

/**
 * 基础业务对象 必要数据规范接口
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
public interface IBaseBo {
    /**
     * 业务标签
     */
    String getTag();

    /**
     * 处理状态
     */
    Integer getStatus();

    /**
     * 下一步是否执行 默认执行
     */
    Boolean getNextStatus();

    /**
     * 终止下一步是否执行
     */
    void stopNext();

    /**
     * 每一步的处理结果消息
     */
    List<String> getMessages();

    /**
     * 获取全部处理步骤的消息
     *
     * @return 全部处理步骤的消息以 ， 分割
     */
    default String getMessageAll() {
        if (getMessages() == null || getMessages().isEmpty()) {
            return "";
        }
        return getMessages().stream().reduce((a, b) -> a + "," + b).orElse("");
    }
}
