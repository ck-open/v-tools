package com.ck.business.trans.service;

import com.ck.business.configuration.BusinessBeanContext;
import com.ck.business.trans.IBusiness;
import com.ck.business.trans.ITransConvert;
import com.ck.business.trans.ITransHandler;
import com.ck.business.trans.ITransSave;
import com.ck.business.trans.entity.vo.IBaseBo;
import com.ck.tools.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ResolvableType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 基础业务实现
 *
 * @param <InDto>  输入数据Dto
 * @param <OutDto> 输出数据Dto
 * @param <Bo>     业务对象
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
public abstract class TransBusiness<InDto, OutDto, Bo extends IBaseBo> implements IBusiness<InDto, OutDto, Bo> {
    /**
     * 当前实现类日志对象
     */
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 保存请求数据
     *
     * @param inDto 输入数据Dto
     * @return 保存请求数据结果 true 保存成功 false 保存失败
     */
    @Override
    public boolean saveInDto(InDto inDto) {
        ITransSave<InDto, OutDto, Bo> transSave = getTransSave(inDto);

        if (transSave != null) {
            return transSave.saveInDto(inDto);
        }
        return IBusiness.super.saveInDto(inDto);
    }

    /**
     * 输入数据转换业务对象
     *
     * @param inDto 输入数据Dto
     * @return 业务对象
     */
    @Override
    public Bo convertBo(InDto inDto) {
        log.info("开始转换业务对象......");
        ITransConvert<InDto, OutDto, Bo> transConvert = getTransConvert(inDto);

        Bo result = null;
        if (transConvert != null) {
            result = transConvert.convertBo(inDto);
        } else {
            log.info("未找到 ITransConvert 转换器");
        }

        log.info("转换业务对象完成。");
        return result;
    }

    /**
     * 执行业务处理器
     *
     * @param bo 业务对象
     * @return 业务对象
     */
    @Override
    public Bo doHandle(Bo bo) {
        log.info("开始执行业务处理器......");
        if (bo != null) {
            List<ITransHandler<Bo>> handlerList = BusinessBeanContext.getTransHandlerList(this);

            stopWatch("执行所有 ITransHandler 处理器", stopWatch -> {
                boolean tag = false;
                if (handlerList != null && !handlerList.isEmpty()) {
                    for (ITransHandler<Bo> iTransHandler : handlerList) {
                        if (bo.getNextStatus() && iTransHandler.isSupport(bo)) {
                            tag = true;
                            stopWatch.start(String.format("执行ITransHandler[%s]", iTransHandler.getClass().getSimpleName()));
                            iTransHandler.doTrans(bo);
                            stopWatch.stop();
                        }
                    }
                }
                if (!tag) {
                    throw new BusinessException("未找到 ITransHandler 处理器");
                }
            });
        } else {
            log.info("业务对象 BO 为空");
        }
        log.info("执行业务处理器结束。");
        return bo;
    }

    /**
     * 业务对象转换输出数据Dto
     *
     * @param bo    业务对象
     * @param inDto 输入数据Dto
     * @return 输出数据Dto
     */
    @Override
    public OutDto convertOutDto(Bo bo, InDto inDto) {
        log.info("开始业务对象转换输出数据Dto......");
        ITransConvert<InDto, OutDto, Bo> transConvert = getTransConvert(inDto);

        OutDto result = null;
        if (transConvert != null) {
            result = transConvert.convertOutDto(bo);
        } else {
            log.info("未找到 ITransConvert 转换器");
        }

        log.info("业务对象转换输出数据Dto完成。");
        return result;
    }

    /**
     * 保存输出数据
     *
     * @param inDto  输入数据Dto
     * @param outDto 输出数据Dto
     * @return 保存输出数据结果 true 保存成功 false 保存失败
     */
    @Override
    public boolean saveOutDto(InDto inDto, OutDto outDto) {
        ITransSave<InDto, OutDto, Bo> transSave = getTransSave(inDto);
        if (transSave != null) {
            return transSave.saveOutDto(outDto);
        }
        return IBusiness.super.saveOutDto(inDto, outDto);
    }

    /**
     * 保存业务处理数据
     *
     * @param bo    业务对象
     * @param inDto 输入数据Dto
     * @return 保存业务处理数据结果 true 保存成功 false 保存失败
     */
    @Override
    public boolean saveBo(Bo bo, InDto inDto) {
        ITransSave<InDto, OutDto, Bo> transSave = getTransSave(inDto);
        if (transSave != null) {
            return transSave.saveBo(bo);
        }
        return IBusiness.super.saveBo(bo, inDto);
    }

    /**
     * 获取当前业务转换器
     *
     * @param inDto 输入数据Dto
     * @return 转换器
     */
    protected ITransConvert<InDto, OutDto, Bo> getTransConvert(InDto inDto) {

        List<ITransConvert<InDto, OutDto, Bo>> transConverts = BusinessBeanContext.getTransConvertList(this);
        if (transConverts != null) {

            List<ITransConvert<InDto, OutDto, Bo>> converts = transConverts.stream()
                    .filter(convert -> convert != null && convert.isSupport(inDto)).collect(Collectors.toList());

            if (converts.isEmpty()) {
                ResolvableType resolvableType = BusinessBeanContext.getIBusinessResolvableType(this);
                throw new BusinessException("1000", "未找到 ITransConvert 转换器,无法对 {}_{}_{} 类型数据进行转换", resolvableType.getGeneric(0), resolvableType.getGeneric(1), resolvableType.getGeneric(2));
            }
            if (converts.size() > 1) {
                throw new BusinessException("1001", "存在多个 ITransConvert 转换器,[{}]", converts.stream().map(i -> i.getClass().getSimpleName()).collect(Collectors.joining(",")));
            }
            return converts.get(0);
        }
        return null;
    }

    /**
     * 获取当前业务储存器
     *
     * @param inDto 输入数据Dto
     * @return 储存器
     */
    protected ITransSave<InDto, OutDto, Bo> getTransSave(InDto inDto) {
        List<ITransSave<InDto, OutDto, Bo>> transSaves = BusinessBeanContext.getTransSaveList(this);
        if (transSaves != null) {

            List<ITransSave<InDto, OutDto, Bo>> saveList = transSaves.stream()
                    .filter(convert -> convert != null && convert.isSupport(inDto)).collect(Collectors.toList());

            if (!saveList.isEmpty()) {
                if (saveList.size() > 1) {
                    throw new BusinessException("1001", "存在多个 ITransSave 业务储存器,[{}]", saveList.stream().map(i -> i.getClass().getSimpleName()).collect(Collectors.joining(",")));
                }
                return saveList.get(0);
            } else {
                log.info("未找到 ITransSave 业务储存器,如不需要存储请忽略此消息");
            }
        }
        return null;
    }
}
