package com.ck.business.trans;


import com.ck.business.trans.entity.vo.IBaseBo;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * 业务处理器接口
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
public interface ITransHandler<Bo extends IBaseBo> {

    /**
     * 获取优先级,多个实例时按照返回值升序排序依次执行
     *
     * @return 优先级
     */
    default int getTransOrder() {
        Order order = this.getClass().getAnnotation(Order.class);
        if (order != null) {
            return order.value();
        }
        return Ordered.LOWEST_PRECEDENCE;
    }

    /**
     * 是否支持此操作
     *
     * @param bo 业务对象
     * @return 支持则返回true，否则返回false
     */
    boolean isSupport(Bo bo);

    /**
     * 执行业务
     *
     * @param bo 业务对象
     */
    void doTrans(Bo bo);
}
