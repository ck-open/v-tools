package com.ck.business.trans.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 基础业务对象
 */
@Data
@Accessors(chain = true)
public abstract class BaseBo implements IBaseBo {
    /**
     * 业务标签
     */
    private String tag;
    /**
     * 处理状态 默认状态 1
     */
    private Integer status = 1;
    /**
     * 下一步是否执行 默认执行
     */
    private Boolean nextStatus = true;
    /**
     * 每一步的处理结果消息
     */
    private List<String> messages = new ArrayList<>();

    /**
     * 终止下一步是否执行
     */
    @Override
    public void stopNext() {
        this.nextStatus = false;
    }
}
