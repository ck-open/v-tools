package com.ck.business.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
@AutoConfigureOrder(Integer.MIN_VALUE)
public class BusinessApiAutoConfiguration {

    /**
     * 注入业务bean上下文
     */
    @Bean
    @ConditionalOnMissingBean(BusinessBeanContext.class)
    public BusinessBeanContext methodLogHandler() {
        BusinessBeanContext businessBeanContext = new BusinessBeanContext();
        log.info("BusinessBeanContext [{}]", businessBeanContext);
        return businessBeanContext;
    }
}
