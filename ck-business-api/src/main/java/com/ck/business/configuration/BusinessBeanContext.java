package com.ck.business.configuration;

import com.ck.business.trans.ITransConvert;
import com.ck.business.trans.ITransSave;
import com.ck.business.trans.entity.vo.IBaseBo;
import com.ck.business.trans.IBusiness;
import com.ck.business.trans.ITransHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.ResolvableType;
import org.springframework.lang.NonNull;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * 业务bean上下文
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
@Slf4j
public class BusinessBeanContext implements ApplicationContextAware, CommandLineRunner {

    /**
     * ITransHandler实例集合
     */
    private static final Map<String, List<ITransHandler<?>>> TransHandlerMap = new ConcurrentHashMap<>();
    /**
     * ITransConvert实例集合
     */
    private static final Map<String, List<ITransConvert<?, ?, ?>>> TransConvertMap = new ConcurrentHashMap<>();
    /**
     * ITransConvert实例集合
     */
    private static final Map<String, List<ITransSave<?, ?, ?>>> TransSaveMap = new ConcurrentHashMap<>();

    /**
     * Spring 上下文对象
     */
    protected static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
        BusinessBeanContext.applicationContext = applicationContext;
    }

    @Override
    public void run(String... args) throws Exception {
        Map<String, IBusiness> iTransHandlerMap = applicationContext.getBeansOfType(IBusiness.class);
        iTransHandlerMap.forEach((name, iTransHandler) -> log.info("IBusiness 实例：{}", iTransHandler.getClass().getName()));

        /* 注册所有ITransHandler实例 */
        registerTransHandler();
        TransHandlerMap.forEach((key, value) -> value.forEach(iTransHandler -> log.info("ITransHandler Bo:{} 实例：{}", key, iTransHandler.getClass().getName())));
        /* 注册所有ITransConvert实例 */
        registerTransConvert();
        TransConvertMap.forEach((key, value) -> value.forEach(iTransConvert -> log.info("ITransConvert convert:{} 实例：{}", key, iTransConvert.getClass().getName())));
        /* 注册所有ITransSave实例 */
        registerTransSave();
        TransSaveMap.forEach((key, value) -> value.forEach(iTransSave -> log.info("ITransSave save:{} 实例：{}", key, iTransSave.getClass().getName())));
    }

    /**
     * 注册ITransHandler实例
     */
    private void registerTransHandler() {
        /* 注册所有ITransHandler实例 */
        Map<String, ITransHandler> iTransHandlerMap = applicationContext.getBeansOfType(ITransHandler.class);

        iTransHandlerMap.forEach((name, iTransHandler) -> {
            ResolvableType resolvableType = getInterfaces(iTransHandler, ITransHandler.class);

            Class<?> clazzBo = resolvableType.resolveGeneric(0);

            if (clazzBo != null) {
                String transType = ITransHandler.class.getSimpleName() + "_" + clazzBo.getName();
                if (!TransHandlerMap.containsKey(transType)) {
                    TransHandlerMap.put(transType, new ArrayList<>());
                }

                TransHandlerMap.get(transType).add(iTransHandler);
            }
        });
    }

    /**
     * 注册ITransConvert实例
     */
    private void registerTransConvert() {
        /* 注册所有ITransConvert实例 */
        Map<String, ITransConvert> iTransConvertMap = applicationContext.getBeansOfType(ITransConvert.class);

        iTransConvertMap.forEach((name, iTransConvert) -> {
            ResolvableType resolvableType = getInterfaces(iTransConvert, ITransConvert.class);
            /* InDto_OutDto_Bo */
            String convertType = ITransConvert.class.getSimpleName()
                    + "_" + resolvableType.getGeneric(0)
                    + "_" + resolvableType.getGeneric(1)
                    + "_" + resolvableType.getGeneric(2);
            if (!TransConvertMap.containsKey(convertType)) {
                TransConvertMap.put(convertType, new ArrayList<>());
            }

            TransConvertMap.get(convertType).add(iTransConvert);
        });
    }

    /**
     * 注册ITransSave实例
     */
    private void registerTransSave() {
        /* 注册所有ITransSave实例 */
        Map<String, ITransSave> iTransSaveMap = applicationContext.getBeansOfType(ITransSave.class);

        iTransSaveMap.forEach((name, iTransSave) -> {
            ResolvableType resolvableType = getInterfaces(iTransSave, ITransSave.class);
            /* InDto_OutDto_Bo */
            String saveType = ITransSave.class.getSimpleName() + "_" + resolvableType.getGeneric(0) + "_" + resolvableType.getGeneric(1) + "_" + resolvableType.getGeneric(2);
            if (!TransSaveMap.containsKey(saveType)) {
                TransSaveMap.put(saveType, new ArrayList<>());
            }

            TransSaveMap.get(saveType).add(iTransSave);
        });
    }

    /**
     * 获取接口类型
     *
     * @param object          实现类对象
     * @param interfacesClass 实现的接口类型
     * @return 实现的接口类型 ResolvableType 对象
     */
    private static ResolvableType getInterfaces(Object object, Class<?> interfacesClass) {
        ResolvableType resolvableType = ResolvableType.forClass(object.getClass());
        if (interfacesClass.isAssignableFrom(object.getClass().getSuperclass())) {
            resolvableType = resolvableType.getSuperType();
        } else {
            for (ResolvableType interfaceType : resolvableType.getInterfaces()) {
                if (interfaceType.getRawClass() != null && interfacesClass.isAssignableFrom(interfaceType.getRawClass())) {
                    resolvableType = interfaceType;
                    break;
                }
            }
        }
        return resolvableType;
    }

    public static <InDto, OutDto, Bo extends IBaseBo> ResolvableType getIBusinessResolvableType(IBusiness<InDto, OutDto, Bo> iBusiness) {
        return getInterfaces(iBusiness, IBusiness.class);
    }

    /**
     * 获取ITransHandler注册的实例列表
     *
     * @return ITransHandler列表
     */
    public static <InDto, OutDto, Bo extends IBaseBo> List<ITransHandler<Bo>> getTransHandlerList(IBusiness<InDto, OutDto, Bo> iBusiness) {
        try {
            ResolvableType resolvableTypeThis = getIBusinessResolvableType(iBusiness).getGeneric(2);
            String boType = ITransHandler.class.getSimpleName() + "_" + resolvableTypeThis;

            if (TransHandlerMap.containsKey(boType) && TransHandlerMap.get(boType) != null) {
                return TransHandlerMap.get(boType).stream().map(iTransHandler -> (ITransHandler<Bo>) iTransHandler)
                        .sorted(Comparator.comparingInt(ITransHandler::getTransOrder))
                        .collect(Collectors.toList());

            } else if (!TransHandlerMap.containsKey(boType)) {
                /* 实例未注册过 则全部排查有没有 当前 iBusiness Bo类型的子类 有则注册并返回 */
                List<ITransHandler<?>> list = TransHandlerMap.values().stream()
                        .flatMap(Collection::stream)
                        .filter(iTransHandler ->
                                resolvableTypeThis.isAssignableFrom(
                                        getInterfaces(iTransHandler, ITransHandler.class).getGeneric(0)
                                )
                        )
                        .collect(Collectors.toList());

                TransHandlerMap.put(boType, list);
                return list.stream().map(iTransHandler -> (ITransHandler<Bo>) iTransHandler).collect(Collectors.toList());
            }
        } catch (Exception e) {
            log.error("ITransHandler 转换异常", e);
        }

        return null;
    }

    /**
     * 获取ITransConvertBo注册的实例列表
     *
     * @return ITransConvertBo列表
     */
    public static <InDto, OutDto, Bo extends IBaseBo> List<ITransConvert<InDto, OutDto, Bo>> getTransConvertList(IBusiness<InDto, OutDto, Bo> iBusiness) {
        try {
            /* InDto_OutDto_Bo */
            ResolvableType resolvableTypeThis = getIBusinessResolvableType(iBusiness);
            String convertType = ITransConvert.class.getSimpleName() + "_" + resolvableTypeThis.getGeneric(0) + "_" + resolvableTypeThis.getGeneric(1) + "_" + resolvableTypeThis.getGeneric(2);

            if (TransConvertMap.containsKey(convertType) && TransConvertMap.get(convertType) != null) {
                return TransConvertMap.get(convertType).stream().map(iTransConvert -> (ITransConvert<InDto, OutDto, Bo>) iTransConvert)
                        .sorted(Comparator.comparingInt(ITransConvert::getTransOrder))
                        .collect(Collectors.toList());
            } else if (!TransConvertMap.containsKey(convertType)) {
                /* 实例未注册过 则全部排查有没有 当前 iBusiness InDto_OutDto_Bo类型的子类 有则注册并返回 */
                ResolvableType resolvableTypeInDto = resolvableTypeThis.getGeneric(0);
                ResolvableType resolvableTypeOutDto = resolvableTypeThis.getGeneric(1);
                ResolvableType resolvableTypeBo = resolvableTypeThis.getGeneric(2);
                List<ITransConvert<?, ?, ?>> list = TransConvertMap.values().stream()
                        .flatMap(Collection::stream)
                        .filter(iTransConvert -> {
                            ResolvableType resolvableTypeTemp = getInterfaces(iTransConvert, ITransConvert.class);
                            return resolvableTypeInDto.isAssignableFrom(resolvableTypeTemp.getGeneric(0))
                                    && resolvableTypeOutDto.isAssignableFrom(resolvableTypeTemp.getGeneric(1))
                                    && resolvableTypeBo.isAssignableFrom(resolvableTypeTemp.getGeneric(2));
                        })
                        .collect(Collectors.toList());

                TransConvertMap.put(convertType, list);
                return list.stream().map(iTransConvert -> (ITransConvert<InDto, OutDto, Bo>) iTransConvert).collect(Collectors.toList());
            }
        } catch (Exception e) {
            log.error("ITransConvertBo 转换异常", e);
        }

        return null;
    }

    /**
     * 获取ITransConvertBo注册的实例列表
     *
     * @return ITransConvertBo列表
     */
    public static <InDto, OutDto, Bo extends IBaseBo> List<ITransSave<InDto, OutDto, Bo>> getTransSaveList(IBusiness<InDto, OutDto, Bo> iBusiness) {
        try {
            ResolvableType resolvableTypeThis = getIBusinessResolvableType(iBusiness);
            String saveType = ITransSave.class.getSimpleName() + "_" + resolvableTypeThis.getGeneric(0) + "_" + resolvableTypeThis.getGeneric(1) + "_" + resolvableTypeThis.getGeneric(2);

            if (TransSaveMap.containsKey(saveType) && TransSaveMap.get(saveType) != null) {
                return TransSaveMap.get(saveType).stream().map(iTransSave -> (ITransSave<InDto, OutDto, Bo>) iTransSave)
                        .sorted(Comparator.comparingInt(ITransSave::getTransOrder))
                        .collect(Collectors.toList());
            } else if (!TransConvertMap.containsKey(saveType)) {
                /* 实例未注册过 则全部排查有没有 当前 iBusiness InDto_OutDto_Bo类型的子类 有则注册并返回 */
                ResolvableType resolvableTypeInDto = resolvableTypeThis.getGeneric(0);
                ResolvableType resolvableTypeOutDto = resolvableTypeThis.getGeneric(1);
                ResolvableType resolvableTypeBo = resolvableTypeThis.getGeneric(2);

                List<ITransSave<?, ?, ?>> list = TransSaveMap.values().stream()
                        .flatMap(Collection::stream)
                        .filter(iTransSave -> {
                            ResolvableType resolvableTypeTemp = getInterfaces(iTransSave, ITransSave.class);
                            return resolvableTypeInDto.isAssignableFrom(resolvableTypeTemp.getGeneric(0))
                                    && resolvableTypeOutDto.isAssignableFrom(resolvableTypeTemp.getGeneric(1))
                                    && resolvableTypeBo.isAssignableFrom(resolvableTypeTemp.getGeneric(2));
                        })
                        .collect(Collectors.toList());

                TransSaveMap.put(saveType, list);
                return list.stream().map(iTransSave -> (ITransSave<InDto, OutDto, Bo>) iTransSave).collect(Collectors.toList());
            }
        } catch (Exception e) {
            log.error("ITransSave 转换异常", e);
        }

        return null;
    }

    /**
     * 获取ObjectMapper
     *
     * @return ObjectMapper
     */
    public static ObjectMapper getObjectMapper() {
        try {
            return BusinessBeanContext.applicationContext.getBean(ObjectMapper.class);
        } catch (Exception e) {
            return new ObjectMapper();
        }
    }
}
