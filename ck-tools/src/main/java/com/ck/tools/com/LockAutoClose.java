package com.ck.tools.com;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * 自动释放锁 try-with-resources 支持对象
 */
public class LockAutoClose implements AutoCloseable {
    private final Logger log = Logger.getLogger(LockAutoClose.class.getName());
    /**
     * 实现锁方式
     */
    private LockFunction lockFunction = null;
    /**
     * 解锁方式
     */
    private UnlockFunction unlockFunction = null;
    /**
     * 解锁失败处理
     */
    private Consumer<String> unlockFailFunction = null;
    /**
     * 锁Key
     */
    private String lockKey;
    /**
     * 锁超时时间
     */
    private Long lockTimeout;
    /**
     * 锁状态，获取成功 True
     */
    private Boolean lockStatus = false;

    /**
     * 构建自动释放锁
     * try-with-resources 锁对象
     *
     * @param lockFunction   获取锁逻辑。
     * @param unlockFunction 释放锁逻辑。
     */
    public LockAutoClose(LockFunction lockFunction, UnlockFunction unlockFunction) {
        this.lockFunction = lockFunction;
        this.unlockFunction = unlockFunction;
        if (lockFunction == null || unlockFunction == null) {
            log.info("lockFunction or unlockFunction is null");
        }
    }

    /**
     * 构建自动释放锁
     * try-with-resources 锁对象
     *
     * @param lockFunction   获取锁逻辑。
     * @param unlockFunction 释放锁逻辑。
     * @param unlockFailFunction 解锁失败。
     */
    public LockAutoClose(LockFunction lockFunction, UnlockFunction unlockFunction, Consumer<String> unlockFailFunction) {
        this.lockFunction = lockFunction;
        this.unlockFunction = unlockFunction;
        this.unlockFailFunction = unlockFailFunction;
        if (lockFunction == null || unlockFunction == null) {
            log.info("lockFunction or unlockFunction is null");
        }
    }

    /**
     * 获取锁状态
     *
     * @return 是否获取锁成功，true 获取锁成功，false 获取锁失败
     */
    public boolean lockStatus() {
        return lockStatus != null && lockStatus;
    }

    /**
     * 获取锁
     *
     * @param lockKey     锁键
     * @param lockTimeout 锁超时时间
     * @return 是否获取锁成功，true 获取锁成功，false 获取锁失败
     */
    public LockAutoClose lock(String lockKey, Integer lockTimeout) {
        return lock(null, lockKey, lockTimeout);
    }

    /**
     * 获取锁
     *
     * @param lockKeyPrefix 锁前缀
     * @param lockKey       锁键
     * @param lockTimeout   锁超时时间
     * @return 是否获取锁成功，true 获取锁成功，false 获取锁失败
     */
    public LockAutoClose lock(String lockKeyPrefix, String lockKey, Integer lockTimeout) {
        return lock(lockKeyPrefix, lockKey, null, lockTimeout);
    }

    /**
     * 获取锁
     *
     * @param lockKeyPrefix 锁前缀
     * @param lockKey       锁键
     * @param lockValue     锁值
     * @param lockTimeout   锁超时时间
     * @return 是否获取锁成功，true 获取锁成功，false 获取锁失败
     */
    public LockAutoClose lock(String lockKeyPrefix, String lockKey, String lockValue, Integer lockTimeout) {
        String key = (Objects.nonNull(lockKeyPrefix) ? lockKeyPrefix : "") + lockKey;
        LockAutoClose lockAutoClose = new LockAutoClose(lockFunction, unlockFunction);
        if (!key.trim().isEmpty() && lockAutoClose.lockFunction != null && lockAutoClose.unlockFunction != null) {
            lockAutoClose.lockStatus = lockAutoClose.lockFunction.lock(key, lockValue, lockTimeout);
        }
        return lockAutoClose;
    }

    /**
     * 解锁
     *
     * @return 是否解锁成功 true 解锁成功 false 解锁失败
     */
    public boolean unlock() {
        return lockKey != null && !lockKey.trim().isEmpty() && unlockFunction != null && unlockFunction.unlock(lockKey);
    }

    @Override
    public void close() throws Exception {
        if (!unlock()) {
            log.info("try-with-resources 自动释放锁失败");
            if (unlockFailFunction != null) {
                unlockFailFunction.accept(lockKey);
            }
        }
    }

    /**
     * 获取锁函数
     * 例如redis分布式锁。Boolean lockResult = StringRedisTemplate.opsForValue().setIfAbsent(key, value, seconds, TimeUnit.MINUTES);
     */
    public interface LockFunction {
        /**
         * 获取锁
         *
         * @param lockKey     锁键
         * @param lockValue   锁值
         * @param lockTimeout 超时时间
         * @return 是否获取锁成功，true 获取锁成功，false 获取锁失败
         */
        boolean lock(String lockKey, String lockValue, Integer lockTimeout);
    }

    /**
     * 释放锁函数
     * 例如redis分布式锁。Boolean unlockResult = StringRedisTemplate.opsForValue().getOperations().delete(key);
     */
    public interface UnlockFunction {
        /**
         * 释放锁
         *
         * @param lockKey 锁键
         * @return 是否释放锁成功，true 释放锁成功，false 释放锁失败
         */
        boolean unlock(String lockKey);
    }
}
