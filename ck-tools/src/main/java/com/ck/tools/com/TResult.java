package com.ck.tools.com;

import com.ck.tools.exception.BaseException;
import com.ck.tools.utils.TimeUtil;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * 返回结果信息
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
@Data
@Accessors(chain = true)
public class TResult<T> implements Serializable {
    private static Logger log = Logger.getLogger(TResult.class.getName());

    @Getter
    public enum Code {
        SUCCEED(0, "0", "成功"),
        OPERATE_OK(0, "0", "操作成功"),
        SAVE_OK(0, "0", "保存成功"),
        UPDATE_OK(0, "0", "更新成功"),
        QUERY_OK(0, "0", "查询成功"),
        DEL_OK(0, "0", "删除成功"),
        ERROR(-1, "-1", "未知错误"),
        ERROR_RETRY(69, "69", "处理失败,请重试"),
        LOGIN_ERROR(401, "401", "未登录"),
        AUTH_ERROR(403, "403", "无权限"),
        IP_ERROR(403, "403", "IP验证失败"),
        PARAM_ERROR(400, "400", "参数错误"),
        PARAM_MISSING_ERROR(400, "2001", "缺少参数"),
        PARAM_FORMAT_ERROR(400, "2001", "参数格式错误"),
        OPERATE_ERROR(400, "400", "操作失败"),
        API_ERROR(500, "500", "接口调用失败"),
        SAVE_ERROR(1001, "1001", "保存失败"),
        UPDATE_ERROR(1002, "1002", "更新失败"),
        QUERY_ERROR(1003, "1003", "查询失败"),
        DEL_ERROR(1004, "1004", "删除失败"),
        FILE_UPLOAD_ERROR(3010, "3000", "文件上传失败"),
        FILE_DOWN_ERROR(3011, "3000", "文件下载失败"),
        FILE_TYPE_ERROR(3012, "3000", "不能识别的文件类型");

        private final Integer status;
        private final String code;
        private final String msg;

        Code(Integer status, String code, String msg) {
            this.status = status;
            this.code = code;
            this.msg = msg;
        }

    }


    private Integer status;

    private String code;

    private String message;

    private String time;

    private T data;

    public boolean isSuccess() {
        return Objects.equals(status, Code.SUCCEED.status) || Objects.equals(code, Code.SUCCEED.code);
    }

    @Override
    public String toString() {
        return String.format("TResult{ status=%s, code=%s, message=%s, time=%s, data=%s }", status, code, message, time, data);
    }

    public static <T> TResult<T> success(T data) {
        TResult<T> result = build(Code.SUCCEED.code, Code.SUCCEED.status, "成功");
        return result.setData(data);
    }

    public static <T> TResult<T> success(String msg, String... params) {
        return build(Code.SUCCEED.code, Code.SUCCEED.status, msg, params);
    }

    public static <T> TResult<T> fail(String msg, String... params) {
        return build(Code.ERROR.code, Code.ERROR.status, msg, params);
    }

    public static <T> TResult<T> build(Code code) {
        return build(code.code, code.status, code.getMsg());
    }

    public static <T> TResult<T> build(Code code, String msg, String... param) {
        return build(code.code, code.status, msg, param);
    }

    public static <T> TResult<T> build(Integer status, String msg, String... param) {
        return build(null, status, msg, param);
    }

    public static <T> TResult<T> build(String code, String msg, String... param) {
        return build(code, null, msg, param);
    }

    private static <T> TResult<T> build(String code, Integer status, String msg, String... param) {
        if (msg != null && param != null && param.length > 0) {
            if (msg.contains("{}")) {
                for (Object o : param) {
                    msg = msg.replaceFirst("\\{}", String.valueOf(o));
                }
            }
        }
        return new TResult<T>().setStatus(status).setCode(code).setMessage(msg).setTime(TimeUtil.formatDate_S(new Date()));
    }

//    /**
//     * 将异常信息按照统一报文格式写出
//     *
//     * @param response 响应
//     * @param ex       异常对象
//     */
//    public static void writerException(HttpServletResponse response, Throwable ex) {
//        TResult<String> resultBody = TResult.resolveException(ex);
//        response.setStatus(resultBody.getStatus());
//        try {
//            response.setContentType("application/json;charset=UTF-8");
//            response.setCharacterEncoding(StandardCharsets.UTF_8.name());
//            response.getWriter().print(JsonUtils.toJson(resultBody));
//            response.getWriter().flush();
//            response.getWriter().close();
//        } catch (IOException e) {
//            log.warning("响应异常信息失败  error:" + e.getMessage());
//            throw new RuntimeException(e);
//
//        }
//    }

    /**
     * 静态解析异常。可以直接调用
     *
     * @param ex 异常对象
     * @return 转换后的响应对象
     */
    public static <T> TResult<T> resolveException(Throwable ex) {
        if (BaseException.class.isAssignableFrom(ex.getClass())) {
            return build(((BaseException) ex).getCode(), ex.getMessage());
        }
        String message = ex.getMessage();
        String className = ex.getClass().getSimpleName();

        switch (className) {
            /* 账号错误 */
            case "UsernameNotFoundException":
                return build(Code.API_ERROR, "找不到用户({})", "username_not_found");
            case "AccountExpiredException":
                return build(Code.API_ERROR, "账户已过期({})", "account_expired");
            case "LockedException":
                return build(Code.API_ERROR, "账户已锁定({})", "account_locked");
            case "DisabledException":
                return build(Code.API_ERROR, "账户被禁用({})", "account_disabled");
            case "BadCredentialsException":
                return build(Code.API_ERROR, "认证信息损坏({})", "bad_credentials");
            case "CredentialsExpiredException":
                return build(Code.API_ERROR, "认证已过期({})", "expired_credentials");

            /* oauth2返回码 */
            case "InvalidClientException":
                return build(Code.API_ERROR, "无效的客户端({})", "invalid_client");
            case "UnauthorizedClientException":
                return build(Code.API_ERROR, "未经授权的客户端({})", "unauthorized_client");
            case "InsufficientAuthenticationException":
                return build(Code.API_ERROR, "权限不足({})", "insufficient_authentication");
            case "AuthenticationCredentialsNotFoundException":
                return build(Code.API_ERROR, "未找到授权信息({})", "AuthenticationCredentialsNotFound");

            case "InvalidGrantException": /* 账号错误 */
                if ("Bad credentials".contains(message)) {
                    return build(Code.API_ERROR, "认证信息损坏({})", "bad_credentials");
                } else if ("User is disabled".contains(message)) {
                    return build(Code.API_ERROR, "账户被禁用({})", "account_disabled");
                } else if ("User account is locked".contains(message)) {
                    return build(Code.API_ERROR, "账户已锁定({})", "account_locked");
                } else {
                    return build(Code.API_ERROR, "无效的授权({})", "invalid_grant");
                }

            case "InvalidScopeException":
                return build(Code.API_ERROR, "无效的授权范围({})", "invalid_scope");
            case "InvalidTokenException":
                return build(Code.API_ERROR, "无效的Token({})", "invalid_token");
            case "InvalidRequestException":
                return build(Code.API_ERROR, "无效的请求({})", "invalid_request");
            case "RedirectMismatchException":
                return build(Code.API_ERROR, "重定向地址不匹配({})", "redirect_uri_mismatch");
            case "UnsupportedGrantTypeException":
                return build(Code.API_ERROR, "授权类型不支持({})", "unsupported_grant_type");
            case "UnsupportedResponseTypeException":
                return build(Code.API_ERROR, "响应类型不支持({})", "unsupported_response_type");
            case "UserDeniedAuthorizationException":
                return build(Code.API_ERROR, "用户拒绝授权({})", "user_denied_authorization");
            case "AccessDeniedException":
                if (Arrays.asList("access_denied_black_limited", "access_denied_white_limited", "access_denied_authority_expired", "access_denied_updating", "access_denied_disabled", "access_denied_not_open")
                        .contains(message)) {
                    return build(Code.API_ERROR, message);
                } else {
                    return build(Code.API_ERROR, "access_denied");
                }

                /* 请求错误 */
            case "HttpMessageNotReadableException":
            case "TypeMismatchException":
            case "MissingServletRequestParameterException":
                return build(Code.PARAM_FORMAT_ERROR, "bad_request(参数结构错误无法解析)");
            case "NoHandlerFoundException":
            case "ResponseStatusException":
                return build(Code.API_ERROR, "not_found");
            case "HttpRequestMethodNotSupportedException":
                return build(Code.API_ERROR, "method_not_allowed");
            case "HttpMediaTypeNotAcceptableException":
                return build(Code.API_ERROR, "media_type_not_acceptable");
            case "MethodArgumentNotValidException":
                return build(Code.PARAM_ERROR, "方法参数无效");
            case "IllegalArgumentException":
            case "ConstraintViolationException":
            case "BindException":
                return build(Code.PARAM_ERROR, "参数错误");
            case "OpenSignatureException":
                if ("too_many_requests".equalsIgnoreCase(message)) {
                    return build(Code.API_ERROR, "too_many_requests");
                } else {
                    return build(Code.API_ERROR, "signature_denied");
                }
            case "CheckException":
                return build(Code.PARAM_ERROR, message);
            case "NullPointerException":
                return build(Code.ERROR, message);
            case "OpenAlertException":
            default:
                return build(Code.API_ERROR, "alert");
        }
    }
}
