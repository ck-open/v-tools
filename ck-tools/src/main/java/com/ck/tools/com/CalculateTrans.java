package com.ck.tools.com;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 计算相关公共方法
 * 所有计算方法 舍入方式 RoundingMode.HALF_UP
 *
 * @author cyk
 */
public interface CalculateTrans {

    /**
     * 默认计算精度
     */
    Integer SCALE = 8;

    /**
     * 计算税费净值
     *
     * @param val     原值
     * @param taxRate 税率
     * @return 不含税净值
     */
    static BigDecimal taxNet(BigDecimal val, BigDecimal taxRate) {
        if (val == null || taxRate == null) {
            return BigDecimal.ZERO;
        }
        return divide(val, BigDecimal.ONE.add(taxRate));
    }

    /**
     * 计算税费净值
     *
     * @param val     原值
     * @param taxRate 税率
     * @return 不含税净值
     */
    default BigDecimal calculateTaxNet(BigDecimal val, BigDecimal taxRate) {
        return taxNet(val, taxRate);
    }

    /**
     * 计算税费
     *
     * @param val     原值
     * @param taxRate 税率
     * @return 税值
     */
    static BigDecimal taxFee(BigDecimal val, BigDecimal taxRate) {
        return subtract(val, taxNet(val, taxRate));
    }

    /**
     * 计算税费
     *
     * @param val     原值
     * @param taxRate 税率
     * @return 税值
     */
    default BigDecimal calculateTaxFee(BigDecimal val, BigDecimal taxRate) {
        return taxFee(val, taxRate);
    }

    /**
     * 尾差值计算并调整
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @param mapper   字段取值映射器
     * @param sorted   调整尾差前进行排序
     * @param dispose  尾差值处理
     * @param <T>      列表对象
     */
    static <T> void adjustment(BigDecimal totalVal, List<T> values, Function<? super T, BigDecimal> mapper, Function<? super T, BigDecimal> sorted, BiConsumer<T, BigDecimal> dispose) {
        if (values == null || values.isEmpty()) {
            return;
        }
        if (sorted != null)
            values.sort(Comparator.comparing(sorted));

        adjustment(totalVal, values, mapper, dispose);
    }

    /**
     * 尾差值计算并调整
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @param mapper   字段取值映射器
     * @param sorted   调整尾差前进行排序
     * @param dispose  尾差值处理
     * @param <T>      列表对象
     */
    default <T> void calculateAdjustment(BigDecimal totalVal, List<T> values, Function<? super T, BigDecimal> mapper, Function<? super T, BigDecimal> sorted, BiConsumer<T, BigDecimal> dispose) {
        adjustment(totalVal, values, mapper, sorted, dispose);
    }

    /**
     * 尾差值计算并调整
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @param mapper   字段取值映射器
     * @param dispose  尾差值处理
     * @param <T>      列表对象
     */
    static <T> void adjustment(BigDecimal totalVal, List<T> values, Function<? super T, BigDecimal> mapper, BiConsumer<T, BigDecimal> dispose) {
        if (mapper == null) {
            throw new RuntimeException("尾差值计算 mapper不能为空");
        }
        if (values == null || values.isEmpty()) {
            return;
        }
        BigDecimal adjustment = adjustment(totalVal, values.stream().map(mapper).collect(Collectors.toList()));
        if (dispose != null) {
            dispose.accept(values.get(values.size() - 1), adjustment);
        }
    }

    /**
     * 尾差值计算并调整
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @param mapper   字段取值映射器
     * @param dispose  尾差值处理
     * @param <T>      列表对象
     */
    default <T> void calculateAdjustment(BigDecimal totalVal, List<T> values, Function<? super T, BigDecimal> mapper, BiConsumer<T, BigDecimal> dispose) {
        adjustment(totalVal, values, mapper, dispose);
    }

    /**
     * 尾差值计算
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @return 尾差值
     */
    static BigDecimal adjustment(BigDecimal totalVal, Collection<BigDecimal> values) {
        if (totalVal == null) {
            throw new RuntimeException("尾差值计算 totalVal不能为空");
        }
        if (values == null || values.isEmpty()) {
            return BigDecimal.ZERO;
        }
        return subtract(totalVal, values.stream().reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    /**
     * 尾差值计算
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @return 尾差值
     */
    default BigDecimal calculateAdjustment(BigDecimal totalVal, Collection<BigDecimal> values) {
        return adjustment(totalVal, values);
    }

    /**
     * 计算 除法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    static BigDecimal divide(BigDecimal val1, BigDecimal val2) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("除数不能为空");
        }
        if (val2.compareTo(BigDecimal.ZERO) == 0) {
            throw new RuntimeException("除数不能为0");
        }
        return scale(val1.divide(val2, SCALE, RoundingMode.HALF_UP), SCALE);
    }

    /**
     * 计算 除法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    default BigDecimal calculateDivide(BigDecimal val1, BigDecimal val2) {
        return divide(val1, val2);
    }

    /**
     * 计算 乘法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    static BigDecimal multiply(BigDecimal val1, BigDecimal val2) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("乘数不能为空");
        }
        return scale(val1.multiply(val2), SCALE);
    }

    /**
     * 计算 乘法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    default BigDecimal calculateMultiply(BigDecimal val1, BigDecimal val2) {
        return multiply(val1, val2);
    }

    /**
     * 计算 减法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    static BigDecimal subtract(BigDecimal val1, BigDecimal val2) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("减法值不能为空");
        }
        return scale(val1.subtract(val2), SCALE);
    }

    /**
     * 计算 减法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    default BigDecimal calculateSubtract(BigDecimal val1, BigDecimal val2) {
        return subtract(val1, val2);
    }

    /**
     * 计算 加法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    static BigDecimal add(BigDecimal val1, BigDecimal val2) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("加法值不能为空");
        }
        return scale(val1.add(val2), SCALE);
    }

    /**
     * 计算 加法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    default BigDecimal calculateAdd(BigDecimal val1, BigDecimal val2) {
        return add(val1, val2);
    }

    /**
     * 设置值精度
     *
     * @param val   值
     * @param scale 保留小数位数
     * @return 舍入结果
     */
    static BigDecimal scale(BigDecimal val, int scale) {
        if (val == null || scale < 0) {
            return val;
        }
        return val.setScale(scale, RoundingMode.HALF_UP);
    }

    /**
     * 设置值精度
     *
     * @param val   值
     * @param scale 保留小数位数
     * @return 舍入结果
     */
    default BigDecimal setScale(BigDecimal val, int scale) {
        return scale(val, scale);
    }
}
