package com.ck.tools.exception;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * 异常对象操作工具
 *
 * @author cyk
 * @since 2021-09-01
 */
public class ExceptionUtil {
    private static final Logger logger = Logger.getLogger(ExceptionUtil.class.getName());

    /**
     * 拼接异常信息
     *
     * @param msg   异常信息模板
     * @param param 异常信息参数
     * @return 拼接后的完整异常信息
     */
    public static String buildMsg(String msg, Object... param) {
        if (msg != null && param != null && param.length > 0) {
            if (msg.contains("{}")) {
                for (Object o : param) {
                    msg = msg.replaceFirst("\\{}", String.valueOf(o));
                }
            } else {
                msg = String.format(msg, param);
            }
        }
        return msg;
    }

    /**
     * 获取异常对象的错误堆栈日志文本
     *
     * @param e             异常对象
     * @param packagePrefix 指定要打印的堆栈日志中包的前缀
     * @return 仿：Exception.printStackTrace(); 的输出日志
     */
    public static String getExceptionStackTraceInfo(Throwable e, String... packagePrefix) {
        return getExceptionStackTraceInfo(e, null, packagePrefix);
    }

    /**
     * 获取异常对象的错误堆栈日志文本
     *
     * @param e             异常对象
     * @param rowNum        要打印的最终堆栈信息行数
     * @param packagePrefix 指定要打印的堆栈日志中包的前缀
     * @return 仿：Exception.printStackTrace(); 的输出日志
     */
    public static String getExceptionStackTraceInfo(Throwable e, Integer rowNum, String... packagePrefix) {
        if (e == null) {
            return null;
        }
        String enter = "\r\n";
        StringBuilder result = new StringBuilder(e.getClass().getName() + ": " + e.getLocalizedMessage() + enter);

        do {
            result.append(getExceptionStackTraceInfo(e.getStackTrace(), rowNum, packagePrefix));

            e = e.getCause();
            if (e != null) {
                result.append("\r\r");
            }
        } while (e != null);

        return result.toString();
    }

    /**
     * 获取异常对象的错误堆栈日志文本
     *
     * @param stackTrace    异常堆栈
     * @param rowNum        要打印的最终堆栈信息行数
     * @param packagePrefix 指定要打印的堆栈日志中包的前缀
     * @return 仿：Exception.printStackTrace(); 的输出日志
     */
    public static <T> String getExceptionStackTraceInfo(T[] stackTrace, Function<T, StackTraceElement> getStackTraceElement, Integer rowNum, String... packagePrefix) {
        if (stackTrace == null || stackTrace.length == 0 || getStackTraceElement == null) {
            return "";
        }

        StackTraceElement[] stackTraceElements = Stream.of(stackTrace).map(getStackTraceElement).toArray(StackTraceElement[]::new);
        return getExceptionStackTraceInfo(stackTraceElements, rowNum, packagePrefix);
    }

    /**
     * 获取异常对象的错误堆栈日志文本
     *
     * @param stackTrace    异常堆栈
     * @param rowNum        要打印的最终堆栈信息行数
     * @param packagePrefix 指定要打印的堆栈日志中包的前缀
     * @return 仿：Exception.printStackTrace(); 的输出日志
     */
    public static String getExceptionStackTraceInfo(StackTraceElement[] stackTrace, Integer rowNum, String... packagePrefix) {
        if (stackTrace == null || stackTrace.length == 0) {
            return "";
        }
        if (rowNum == null || rowNum < 1) rowNum = stackTrace.length;

        StringBuilder result = new StringBuilder();
        for (StackTraceElement ste : stackTrace) {
            if (rowNum <= 0) break;
            boolean off = true;
            if (packagePrefix != null && packagePrefix.length > 0) {
                off = false;
                for (String prefix : packagePrefix) {
                    if (ste.getClassName().startsWith(prefix)) {
                        off = true;
                        break;
                    }
                }
            }
            if (off) {
                if (result.length() > 0){
                    result.append("\r\n").append("\tat ");
                }
                result.append(ste.getClassName()).append(".")
                        .append(ste.getMethodName())
                        .append("(").append(ste.getFileName()).append(":").append(ste.getLineNumber()).append(")");
                rowNum--;
            }
        }

        return result.toString();
    }


    /**
     * 将异常对象的错误堆栈日志文本导出到本地文件
     *
     * @param e         异常对象
     * @param errorData 发生异常的数据文本
     * @param fileName  文件名
     * @param errorPath 文件地址
     */
    public static String exceptionStackTraceExportToFile(Exception e, String errorData, String fileName, String errorPath) {
        try {
            if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
                /* Windows系统时 默认为开发测试启动，将错误日期文件写出到本地 */
                errorPath = new File("").getCanonicalPath() + File.separator + "temp" + File.separator;
            }

            if (!errorPath.endsWith(File.separator)) {
                errorPath += File.separator;
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy" + File.separator + "MM" + File.separator + "dd" + File.separator);
            errorPath += simpleDateFormat.format(new Date());

            File fileDir = new File(errorPath);
            if (!fileDir.exists()) {
                if (!fileDir.mkdirs()) {
                    throw new RuntimeException(String.format("文件创建失败[%s]", errorPath));
                }
            }

            String errorContent = "";
            if (errorData != null && !errorData.trim().isEmpty()) {
                errorContent += "发生异常的数据：\r";
                errorContent += errorData + "\r\r";
            }

            errorContent += "异常堆栈追踪：\r" + getExceptionStackTraceInfo(e);

            fileName += ".txt";
            FileWriter resultFile = getFileWriter(fileName, errorPath);
            PrintWriter print = new PrintWriter(resultFile);
            print.println(errorContent);
            resultFile.close();
        } catch (Exception ex) {
            logger.warning("捕获的异常写出到本地文档失败，发生异常：" + getExceptionStackTraceInfo(ex));
            logger.warning("捕获到未写出的异常：" + getExceptionStackTraceInfo(e));
        }
        return errorPath + fileName;
    }

    /**
     * 获取文件写入对象
     *
     * @param fileName  文件名称
     * @param errorPath 文件路径
     * @return FileWriter
     */
    private static FileWriter getFileWriter(String fileName, String errorPath) {
        return errRuntime(() -> {
            File errorFile = new File(errorPath + fileName);
            if (!errorFile.getParentFile().exists()) {
                if (!errorFile.getParentFile().mkdirs()) {
                    throw new RuntimeException(String.format("文件创建失败[%s]", errorFile.getParentFile().getName()));
                }
            }
            if (!errorFile.exists()) {
                if (!errorFile.createNewFile()) {
                    throw new RuntimeException(String.format("文件创建失败[%s]", errorFile.getName()));
                }
            }
            return new FileWriter(errorFile, false);
        });
    }


    /**
     * 捕获 Exception 异常并抛出 RuntimeException
     *
     * @param errRuntime 需要捕获异常的函数（代码段）
     * @param catchErrs  捕获的异常消费函数
     */
    @SafeVarargs
    public static void errRuntime(ErrRuntimeVoid errRuntime, Consumer<? super Throwable>... catchErrs) {
        errRuntime(() -> {
            errRuntime.catchErr();
            return null;
        }, catchErrs);
    }

    /**
     * 捕获 Exception 异常并抛出 RuntimeException
     *
     * @param errRuntime 需要捕获异常的函数（代码段）
     * @param catchErrs  捕获的异常消费函数
     * @param <Result>   函数执行结果类型
     * @return 函数执行结果
     */
    @SafeVarargs
    public static <Result> Result errRuntime(ErrRuntime<Result> errRuntime, Consumer<? super Throwable>... catchErrs) {
        try {
            return errRuntime.catchErr();
        } catch (Throwable e) {
            if (catchErrs != null) {
                for (Consumer<? super Throwable> consumer : catchErrs) {
                    consumer.accept(e);
                }
            }
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public interface ErrRuntimeVoid {
        void catchErr() throws Exception;
    }

    public interface ErrRuntime<T> {
        T catchErr() throws Exception;
    }


    public static void main(String[] args) {
        try {
            try {
                try {

                    "".substring(0, 10);
                } catch (Exception e) {
                    throw new Exception("自定义1", e);
                }
            } catch (Exception e) {
                throw new RuntimeException("自定义2", e);
            }
        } catch (Exception e) {
            String msg = getExceptionStackTraceInfo(e);
            System.out.println(msg);


        }
    }


}
