package com.ck.tools.exception;

/**
 * 计算异常类
 *
 * @author cyk
 * @since 2020-01-01
 * @since 2022/11/18 15:11
 **/
public class CalculatorException extends BusinessException {

    public CalculatorException(String code, String msg, Object... param) {
        super(code, msg, param);
    }

    public CalculatorException(Throwable cause, String code, String msg, Object... param) {
        super(cause, code, msg, param);
    }

    public CalculatorException(Number code, String msg, Object... param) {
        super(code, msg, param);
    }

    public CalculatorException(Throwable cause, Number code, String msg, Object... param) {
        super(cause, code, msg, param);
    }

    public CalculatorException() {
    }

    public CalculatorException(String message) {
        super(message);
    }

    public CalculatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalculatorException(Throwable cause) {
        super(cause);
    }

    public CalculatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * 函数不存在
     */
    public static class FunctionInExistence extends CalculatorException {
        public FunctionInExistence(String code, String msg, Object... param) {
            super(code, msg, param);
        }

        public FunctionInExistence(Throwable cause, String code, String msg, Object... param) {
            super(cause, code, msg, param);
        }

        public FunctionInExistence(Number code, String msg, Object... param) {
            super(code, msg, param);
        }

        public FunctionInExistence(Throwable cause, Number code, String msg, Object... param) {
            super(cause, code, msg, param);
        }

        public FunctionInExistence() {
        }

        public FunctionInExistence(String message) {
            super(message);
        }

        public FunctionInExistence(String message, Throwable cause) {
            super(message, cause);
        }

        public FunctionInExistence(Throwable cause) {
            super(cause);
        }

        public FunctionInExistence(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

    /**
     * 函数不存在
     */
    public static class FunctionRun extends CalculatorException {
        public FunctionRun(String code, String msg, Object... param) {
            super(code, msg, param);
        }

        public FunctionRun(Throwable cause, String code, String msg, Object... param) {
            super(cause, code, msg, param);
        }

        public FunctionRun(Number code, String msg, Object... param) {
            super(code, msg, param);
        }

        public FunctionRun(Throwable cause, Number code, String msg, Object... param) {
            super(cause, code, msg, param);
        }

        public FunctionRun() {
        }

        public FunctionRun(String message) {
            super(message);
        }

        public FunctionRun(String message, Throwable cause) {
            super(message, cause);
        }

        public FunctionRun(Throwable cause) {
            super(cause);
        }

        public FunctionRun(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
