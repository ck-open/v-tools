package com.ck.tools.reflect.serializable;

import java.io.Serializable;
import java.util.function.BiConsumer;

/**
 * 可序列化的 Consumer 接口
 *
 * @param <T>
 */
public interface SBiConsumer<T,U> extends BiConsumer<T,U>, Serializable {
}
