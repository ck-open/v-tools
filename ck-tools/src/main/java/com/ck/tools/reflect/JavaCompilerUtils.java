package com.ck.tools.reflect;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 动态编译Java源码工具类
 *
 * @author cyk
 * @since 2023/3/31 9:51
 **/
public class JavaCompilerUtils {
    private static final Logger log = Logger.getLogger(JavaCompilerUtils.class.getName());


    private JavaCompilerUtils() {
    }

    /**
     * 编译java源码并加载Class
     *
     * @param sourceCode java源码
     * @return 是否编译成功 true 为成功 false 为失败
     */
    public static boolean compilerFile(String... sourceCode) {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        int result = compiler.run(null, null, null, sourceCode);
        log.info("java source file compilation " + (result == 0 ? "succeed" : "failed"));
        return result == 0;
    }

    /**
     * 编译java源码并加载Class
     *
     * @param sourceCode java源码
     * @return 编译后并加载的Class列表
     */
    public static Map<String, Class<?>> compilerStringAndLoader(String... sourceCode) {
        return compilerString(sourceCode).classLoader().getClassMap();
    }

    /**
     * 编译java源码并加载Class
     *
     * @param sourceCode java源码
     * @return java 源码编译管理器
     */
    public static JavaCompilerManager compilerString(String... sourceCode) {
        /* 构建文件管理器  StandardJavaFileManager实例 */
        JavaFileManager fileManager = new MemoryJavaFileObject.Manager<>(ToolProvider.getSystemJavaCompiler().getStandardFileManager(null, null, StandardCharsets.UTF_8));

        /* 编译源码 并 加载类 */
        return compiler(fileManager, buildJavaFileObject(sourceCode));
    }


    /**
     * 编译java源码
     *
     * @param javaFileObjects 源码文件对象列表
     * @return java 源码编译管理器
     */
    public static JavaCompilerManager compiler(JavaFileManager javaFileManager, List<? extends JavaFileObject> javaFileObjects, String... option) {
        /* 获取编译器 */
        JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();

        /* 构建默认文件管理器  StandardJavaFileManager实例 */
        if (javaFileManager == null) {
            javaFileManager = javaCompiler.getStandardFileManager(null, null, null);
        }

        /* 编译器选项 */
        List<String> options = null;
        if (option != null) {
            options = Arrays.asList(option);
        }

        JavaCompilerManager javaCompilerManager = JavaCompilerManager.build(javaFileManager);

        boolean result = javaCompiler.getTask(null, javaFileManager, diagnostic -> javaCompilerManager.getDiagnostic().add(diagnostic), options, null, javaFileObjects).call();

        javaCompilerManager.setResult(result);

        log.info("java source compilation " + (result ? "succeed" : "failed"));
        try {
            javaFileManager.close();
        } catch (IOException e) {
            log.warning("JavaFileManager close failed  message: " + e.getMessage());
        }

        javaFileObjects.forEach(javaFileObject -> javaCompilerManager.getClassNames().add(MemoryJavaFileObject.removeSuffixes(javaFileObject.getName()).replaceAll("/", ".")));

        return javaCompilerManager;
    }

    /**
     * 字符串源码构建JavaFileObject
     *
     * @param sourceCode java源码
     * @return 源码文件对象列表
     */
    public static List<JavaFileObject> buildJavaFileObject(String... sourceCode) {
        return Stream.of(sourceCode).map(code -> {
            String className = getSourceCodeClassName(code);
            return new MemoryJavaFileObject.CharJavaFileObject(className, code);
        }).collect(Collectors.toList());
    }

    /**
     * 获取源码中的类名称
     *
     * @param sourceCode java源码
     * @return java 类名称
     */
    public static String getSourceCodeClassName(String sourceCode) {
        Objects.requireNonNull(sourceCode, "java source is null");

        if (!sourceCode.startsWith("package ")) {
            /* 源码不是以 package 开头 */
            throw new UnknownFormatFlagsException("source code not package Starts With");
        }
        if (!sourceCode.contains("class") && !sourceCode.contains("interface")) {
            /* 源码不包含class关键字 */
            throw new UnknownFormatFlagsException("source code not contains class flag");
        }
        if (!sourceCode.contains("{")) {
            /* 源码格式错误 */
            throw new UnknownFormatFlagsException("source code not contains { flag");
        }

        String fullName = sourceCode.substring(0, sourceCode.indexOf(";")).replace("package", "").trim();

        String className = sourceCode.substring(sourceCode.indexOf("public"), sourceCode.indexOf("{") + 1);
        if (className.contains("interface")) {
            className = className.substring(className.indexOf("interface"), className.indexOf("{")).replace("interface", "");
        } else {
            className = className.substring(className.indexOf("class"), className.indexOf("{")).replace("class", "");
        }


        StringBuilder s = new StringBuilder();
        for (int i = 0; i < className.length(); i++) {
            char c = className.charAt(i);
            if (s.length() > 0 && (c == ' ' || c == '<')) {
                break;
            }
            if (c >= 'a' || c <= 'Z') {
                s.append(c);
            }
        }
        fullName += "." + s.toString().trim() + JavaFileObject.Kind.SOURCE.extension;
        return fullName;
    }

    public static void main(String[] args) throws ClassNotFoundException {
        String sb = "package com.ck.function.compiler;" +
                "public class TestCompiler {" +
                "private String name;" +
                "private String address;" +
                "public String getName() { return name; }" +
                "public void setName(String name) {this.name = name;}" +
                "public String getAddress() {return address;}" +
                "public void setAddress(String address) {this.address = address;}" +
                "}";

//        compiler(null, buildJavaFileObject(sb.toString()));
//
//        Class<?> c = ClassUtils.getClassLoader().loadClass("com.ck.function.compiler.TestCompiler");

        Map<String, Class<?>> classMap = compilerStringAndLoader(sb);
        System.out.println(classMap);

        classMap.values().forEach(i -> {
            try {
                Class.forName(i.getName());
            } catch (ClassNotFoundException e) {
                log.warning(e.getMessage());
            }
        });
    }
}
