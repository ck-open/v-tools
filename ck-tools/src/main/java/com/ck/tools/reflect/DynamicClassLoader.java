package com.ck.tools.reflect;

import java.util.*;
import java.util.logging.Logger;

/**
 * 动态类加载器
 *
 * @author cyk
 * @since 2023/4/10 14:53
 **/
public class DynamicClassLoader extends ClassLoader {
    private static final Logger log = Logger.getLogger(DynamicClassLoader.class.getName());

    /**
     * 所有编译的class列表
     */
    private static final Map<String, Class<?>> dynamicCompilerClass = Collections.synchronizedMap(new HashMap<>());
    /**
     * 存储编译后的Class文件数据
     */
    private static final Map<String, MemoryJavaFileObject.JavaClassObject> dynamicJavaClassObject = Collections.synchronizedMap(new HashMap<>());


    public DynamicClassLoader() {
        super(ClassLoader.getSystemClassLoader());
    }

    public DynamicClassLoader(ClassLoader classLoader) {
        super(classLoader);
    }

    void addJavaClassObject(String name, MemoryJavaFileObject.JavaClassObject object) {
        dynamicJavaClassObject.put(name, object);
    }

    /**
     * 添加动态编译Class 列表
     *
     * @param dClass class
     */
    private static void addDynamicClass(Class<?> dClass) {
        if (dClass == null) {
            return;
        }
        dynamicCompilerClass.put(dClass.getName(), dClass);
    }

    /**
     * 获取动态Class
     *
     * @param name class名
     * @return class
     */
    public Class<?> getDynamicClass(String name) {
        return dynamicCompilerClass.get(name);
    }

    /**
     * 判断class是否存在
     *
     * @param name class名
     * @return 存在返回true
     */
    public boolean contains(String name) {
        return dynamicCompilerClass.containsKey(name);
    }

    /**
     * 判断class是否存在
     *
     * @param dClass class
     * @return 存在返回true
     */
    public boolean contains(Class<?> dClass) {
        return dynamicCompilerClass.containsValue(dClass);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        /* 有新编译数据 */
        if (dynamicJavaClassObject.containsKey(name)) {
            byte[] bytes = dynamicJavaClassObject.remove(name).getBytes();
            Class<?> dynamicClass = defineClass(name, bytes, 0, bytes.length);
            addDynamicClass(dynamicClass);
            log.info(String.format("dynamic load class succeed  className: %s", name));
            MemoryJavaFileObject.writerClassFile(name, bytes);
            return loadClass(name);
        }
        return super.findClass(name);
    }

    /**
     * 加载Class
     *
     * @param name The <a href="#name">binary name</a> of the class
     * @return the class with the given name
     * @throws ClassNotFoundException if the class cannot be found
     */
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if (dynamicJavaClassObject.containsKey(name)) {
            findClass(name);
        }
        if (contains(name)) {
            return getDynamicClass(name);
        }
        return super.loadClass(name);
    }

}
