package com.ck.tools.reflect;

import javax.tools.*;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.logging.Logger;

/**
 * 基于内存的 JavaFileObject
 *
 * @author cyk
 * @since 2023/4/10 15:16
 **/
public class MemoryJavaFileObject {

    private static final Logger log = Logger.getLogger(MemoryJavaFileObject.class.getName());

    /**
     * 将包路径转换为文件路径
     *
     * @param name 包路径
     * @return 文件路径地址URI
     */
    public static URI toFileURI(String name) {
        String prefix = "memory:///";
        File file = new File(name);
        if (file.exists()) { /* 如果文件存在，返回他的URI */
            return file.toURI();
        } else {

            try {
                final StringBuilder newUri = new StringBuilder();
                newUri.append(prefix);
                newUri.append(name.replace('.', '/'));
                if (name.endsWith(JavaFileObject.Kind.SOURCE.extension)) {
                    newUri.replace(newUri.length() - JavaFileObject.Kind.SOURCE.extension.length(), newUri.length(), JavaFileObject.Kind.SOURCE.extension);
                }
                return URI.create(newUri.toString());
            } catch (Exception exp) {
                return URI.create(prefix + "com/sun/script/java/java_source");
            }
        }
    }

    /**
     * 转换类名 目录结构为包结构 并去掉 .java 或 .class 后缀
     *
     * @param className 类名
     * @return 转换后的类名
     */
    public static String removeSuffixes(String className) {
        if (className != null) {
            if (className.startsWith("/")) {
                className = className.substring(1);
            }
            if (className.endsWith(JavaFileObject.Kind.SOURCE.extension)) {
                className = className.replace(JavaFileObject.Kind.SOURCE.extension, "");
            } else if (className.endsWith(JavaFileObject.Kind.CLASS.extension)) {
                className = className.replace(JavaFileObject.Kind.CLASS.extension, "");
            }
        }
        return className;
    }

    /**
     * 将 二进制 class 写出到class文件
     *
     * @param className 类名
     * @param bytes     Class二进制字节
     */
    public static void writerClassFile(String className, byte[] bytes) {
        URL url = MemoryJavaFileObject.class.getResource(File.separator + className);
        assert url != null;
        String classFileName = url.getPath() + removeSuffixes(className).replaceAll("\\.", File.separator) + JavaFileObject.Kind.CLASS.extension;


        /* 检查包路径 不存在则创建 */
        String path = classFileName.substring(0, classFileName.lastIndexOf(File.separator));
        File classPathFile = new File(path);
        if (!classPathFile.exists()) {
            if (!classPathFile.mkdirs()) {
                log.warning(String.format("class file write failed,create mkdirs failed className: %s filePath: %s", className, path));
            }
        }

        /* 写出 .class 文件 */
        File classFile = new File(classFileName);
        if (classFile.exists()) {
            log.info("class file Overwrite the source file at write out time className: " + classFileName);
        } else {
            log.info("class file write out succeed className: " + classFileName);
        }
        try (FileOutputStream writer = new FileOutputStream(classFile)) {
            writer.write(bytes);
        } catch (IOException e) {
            log.warning("class file write failed className: " + classFileName);
        }
    }


    /**
     * 基于内存的JavaFile管理器
     */
    public static class Manager<M extends JavaFileManager> extends ForwardingJavaFileManager<M> {

        /**
         * 所有编译的class列表
         */
        private static final DynamicClassLoader dynamicClassLoader = new DynamicClassLoader();


        public Manager(M fileManager) {
            super(fileManager);
        }


        /**
         * 编译后加载类
         * <p>
         * 返回一个匿名的SecureClassLoader:
         * 加载由JavaCompiler编译后，保存在ClassJavaFileObject中的byte数组。
         */
        @Override
        public ClassLoader getClassLoader(JavaFileManager.Location location) {
            return new DynamicClassLoader(this.getClass().getClassLoader());
        }

        /**
         * 给编译器提供JavaClassObject，编译器会将编译结果写进去
         */
        @Override
        public JavaFileObject getJavaFileForOutput(JavaFileManager.Location location, String className, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
            JavaClassObject classObject = new JavaClassObject(className, kind);
            dynamicClassLoader.addJavaClassObject(className, classObject);
            return classObject;
        }
    }

    /**
     * 存储源代码
     * 字符串java源代码。JavaFileObject表示
     */
    public static class CharJavaFileObject extends SimpleJavaFileObject {

        /**
         * 类名
         */
        private final String className;
        /**
         * 表示java源代码
         */
        private final CharSequence content;

        public CharJavaFileObject(String className, String content) {
//            super(URI.create("string:///" + className.replaceAll("\\.", "/") + Kind.SOURCE.extension), Kind.SOURCE);
            super(toFileURI(className), JavaFileObject.Kind.SOURCE);
            this.className = className;
            this.content = content;
        }


        /**
         * 获取需要编译的源代码
         *
         * @param ignoreEncodingErrors 是否忽略编码异常
         * @return java源代码
         */
        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return content;
        }

        /**
         * 获取类名
         * @return 类名
         */
        public String getClassName() {
            return className;
        }
    }

    /**
     * 存储编译后的字节码
     */
    public static class JavaClassObject extends SimpleJavaFileObject {

        /**
         * Compiler编译后的byte数据会存在这个ByteArrayOutputStream对象中，
         * 后面可以取出，加载到JVM中。
         */
        private final ByteArrayOutputStream byteArrayOutputStream;

        public JavaClassObject(String className, JavaFileObject.Kind kind) {
            /* super(URI.create("string:///" + className.replaceAll("\\.", "/") + kind.extension), kind); */
            super(toFileURI(className), kind);
            this.byteArrayOutputStream = new ByteArrayOutputStream();
        }

        /**
         * 覆盖父类SimpleJavaFileObject的方法。
         * 该方法提供给编译器结果输出的OutputStream。
         * <p>
         * 编译器完成编译后，会将编译结果输出到该 OutputStream 中，我们随后需要使用它获取编译结果
         *
         * @return OutputStream
         */
        @Override
        public OutputStream openOutputStream() {
            return this.byteArrayOutputStream;
        }

        /**
         * FileManager会使用该方法获取编译后的byte，然后将类加载到JVM
         */
        public byte[] getBytes() {
            return this.byteArrayOutputStream.toByteArray();
        }
    }
}
