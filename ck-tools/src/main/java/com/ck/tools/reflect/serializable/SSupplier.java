package com.ck.tools.reflect.serializable;

import java.io.Serializable;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 可序列化的 Function 接口
 *
 * @param <T>
 */
public interface SSupplier<T> extends Supplier<T>, Serializable {
}
