package com.ck.tools.reflect;

import com.ck.tools.utils.TimeUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Json 格式转换
 *
 * @author cyk
 * @since 2020-01-01
 */
public class JsonUtils {

    /**
     * 对象转Json字符串
     *
     * @param val       对象
     * @param passField 跳过的字段列表
     * @return Json字符串
     */
    public static <T> String toJsonStr(T val, String... passField) {
        return toJsonStr(val, null, false, false, passField);
    }

    /**
     * 对象转Json字符串
     *
     * @param val       对象
     * @param isFormat  是否格式化
     * @param passField 跳过的字段列表
     * @return Json字符串
     */
    public static <T> String toJsonStr(T val, boolean isFormat, String... passField) {
        return toJsonStr(val, null, isFormat, false, passField);
    }

    /**
     * 对象转Json字符串
     *
     * @param val       对象
     * @param pattern   格式，默认：yyyy-MM-dd HH:mm:ss
     * @param passField 跳过的字段列表
     * @return Json字符串
     */
    public static <T> String toJsonStr(T val, String pattern, String... passField) {
        return toJsonStr(val, pattern, false, false, passField);
    }

    /**
     * 对象转Json字符串
     *
     * @param val       对象
     * @param pattern   格式，默认：yyyy-MM-dd HH:mm:ss
     * @param isFormat  是否格式化
     * @param passField 跳过的字段列表
     * @return Json字符串
     */
    public static <T> String toJsonStr(T val, String pattern, boolean isFormat, String... passField) {
        return toJsonStr(val, pattern, isFormat, false, passField);
    }

    /**
     * 对象转Json字符串
     *
     * @param val         对象
     * @param pattern     格式，默认：yyyy-MM-dd HH:mm:ss
     * @param isPrintNull 是否打印 null 值
     * @param isFormat    是否格式化
     * @param passField   跳过的字段列表
     * @return Json字符串
     */
    public static <T> String toJsonStr(T val, String pattern, boolean isFormat, boolean isPrintNull, String... passField) {
        return toJsonStr(val, pattern, isPrintNull, isFormat, passField == null ? null : Stream.of(passField).collect(Collectors.toSet()), new AtomicInteger());
    }

    /**
     * 对象转Json字符串
     *
     * @param val         对象
     * @param pattern     格式，默认：yyyy-MM-dd HH:mm:ss
     * @param passField   跳过的字段列表
     * @param isPrintNull 是否打印 null 值
     * @param isFormat    是否格式化
     * @return Json字符串
     */
    private static String toJsonStr(Object val, String pattern, boolean isPrintNull, boolean isFormat, Set<String> passField, AtomicInteger retractSize) {
        if (val == null) {
            return null;
        }

        StringBuilder json = new StringBuilder();
        AtomicBoolean off = new AtomicBoolean(false);
        AtomicBoolean isEnter = new AtomicBoolean(false);
        if (Collection.class.isAssignableFrom(val.getClass()) || val.getClass().isArray()) {
            // 数组
            json.append("[");
            retractSize.getAndIncrement();

            Collection<Object> collection = null;
            if (val.getClass().isArray() && val instanceof Object[]) {
                collection = Arrays.asList((Object[]) val);
            } else {
                collection = (Collection<Object>) val;
            }
            collection.forEach(i -> {
                String jsonTemp = toJsonStr(i, pattern, isPrintNull, isFormat, passField, retractSize);

                if (!"{}".equals(jsonTemp) && !"[]".equals(jsonTemp)) {
                    if (off.get()) {
                        json.append(",");
                    }
                    setJsonEnter(json, isFormat);
                    setJsonRetract(json, isFormat, retractSize);
                    json.append(jsonTemp);
                    off.set(true);
                    isEnter.set(true);
                }
            });

            retractSize.getAndDecrement();
            if (isEnter.get()) {
                setJsonEnter(json, isFormat);
                setJsonRetract(json, isFormat, retractSize);
            }
            json.append("]");
        } else if (Class.class.isAssignableFrom(val.getClass())) {
            json.append(((Class<?>) val).getName());
        } else if (Field.class.isAssignableFrom(val.getClass())) {
            json.append(((Field) val).getName());
        } else if (!ClassUtils.isBasicType(val.getClass())) {
            json.append("{");
            retractSize.getAndIncrement();

            if (Map.class.isAssignableFrom(val.getClass())) {
                @SuppressWarnings("unchecked")
                Map<Object, Object> map = (Map<Object, Object>) val;
                map.forEach((k, v) -> {
                    if (!passField.contains(k.toString()) && (v != null || isPrintNull)) {
                        String jsonTemp = toJsonStr(v, pattern, isPrintNull, isFormat, passField, retractSize);

                        if (!"{}".equals(jsonTemp) && !"[]".equals(jsonTemp)) {
                            if (off.get()) {
                                json.append(",");
                            }
                            setJsonEnter(json, isFormat);
                            setJsonRetract(json, isFormat, retractSize);

                            json.append("\"").append(k).append("\"").append(":").append(jsonTemp);
                            off.set(true);
                            isEnter.set(true);
                        }
                    }
                });
            } else {
                Set<Field> fields = ClassUtils.getFields(val.getClass());
                fields.forEach(field -> {
                    JsonField jsonField = JsonField.bulidJsonField(field);
                    if (!jsonField.isIgnore) {
                        Object valTemp = jsonField.getter(val);
                        if (!passField.contains(field.getName()) && (valTemp != null || isPrintNull)) {
                            String jsonTemp = toJsonStr(valTemp, jsonField.getFormat() != null ? jsonField.getFormat() : pattern, isPrintNull, isFormat, passField, retractSize);

                            if (!"{}".equals(jsonTemp) && !"[]".equals(jsonTemp)) {
                                if (off.get()) {
                                    json.append(",");
                                }
                                setJsonEnter(json, isFormat);
                                setJsonRetract(json, isFormat, retractSize);
                                json.append("\"").append(jsonField.getName()).append("\"").append(":").append(jsonTemp);
                                off.set(true);
                                isEnter.set(true);
                            }
                        }
                    }
                });
            }
            retractSize.getAndDecrement();
            if (isEnter.get()) {
                setJsonEnter(json, isFormat);
                setJsonRetract(json, isFormat, retractSize);
            }
            json.append("}");
        } else {
            toJsonText(json, val, pattern);
        }

        return json.toString();
    }

    /**
     * 转换为json格式字符串
     *
     * @param text Json字符串
     */
    private static void toJsonText(StringBuilder text, Object val, String pattern) {
        Objects.requireNonNull(text, "text is null");
        Objects.requireNonNull(val, "val is null");

        if (Date.class.isAssignableFrom(val.getClass())) {
            text.append("\"").append(TimeUtil.format((Date) val, pattern)).append("\"");
        } else if (Calendar.class.isAssignableFrom(val.getClass())) {
            text.append("\"").append(TimeUtil.format(((Calendar) val).getTime(), pattern)).append("\"");
        } else if (LocalDate.class.isAssignableFrom(val.getClass())) {
            text.append("\"").append(TimeUtil.format(TimeUtil.parseDate((LocalDate) val), pattern)).append("\"");
        } else if (LocalDateTime.class.isAssignableFrom(val.getClass())) {
            text.append("\"").append(TimeUtil.format(TimeUtil.parseDate((LocalDateTime) val), pattern)).append("\"");
        } else if (Number.class.isAssignableFrom(val.getClass())
                || boolean.class.isAssignableFrom(val.getClass())
                || Boolean.class.isAssignableFrom(val.getClass())) {
            text.append(val);
        } else if (String.class.isAssignableFrom(val.getClass())
                || StringBuilder.class.isAssignableFrom(val.getClass())
                || StringBuffer.class.isAssignableFrom(val.getClass())
                || ClassUtils.isBasicType(val.getClass())
        ) {
            text.append("\"").append(val).append("\"");
        } else {
            throw new RuntimeException("val is not Date or Calendar or String or StringBuilder or StringBuffer");
        }
    }

    /**
     * 设置json格式化回车换行
     *
     * @param text Json字符串
     */
    private static void setJsonEnter(StringBuilder text, boolean isFormat) {
        if (isFormat) {
            text.append("\r\n");
        }
    }

    /**
     * 设置json格式化缩进
     *
     * @param text        Json字符串
     * @param retractSize 缩进次数
     */
    private static void setJsonRetract(StringBuilder text, boolean isFormat, AtomicInteger retractSize) {
        if (isFormat) {
            for (int i = 0; i < retractSize.get(); i++) {
                text.append("\t");
            }
        }
    }

    /**
     * 解析Json字符串为对象
     *
     * @param json Json字符串
     * @return 对象 LinkedHashMap 或 ArrayList
     */
    private static Object parseJsonObject(String json) {
        /* 去除json字符串中的空格、换行、回车和制表符 */
        json = json.replaceAll("\r", "")
                .replaceAll("\n", "")
                .replaceAll("\t", "")
                .replaceAll(" ", "");

        /* 结构标签对应的对象寄存器 */
        Stack<Object> structure = new Stack<>();
        /* 结构标签寄存器 */
        Stack<String> tags = new Stack<>();
        /* 结构标签对应的下标寄存器 */
        Stack<Integer> index = new Stack<>();


        /* 记录 { 和 [ 结构符下标 并创建对应的Map 或 List 对象 */
        for (int i = 0; i < json.length(); i++) {
            char c = json.charAt(i);
            if ('[' == c) {
                tags.push("[");
                structure.push(new ArrayList<>());
                index.push(i);
            } else if ('{' == c) {
                tags.push("{");
                structure.push(new LinkedHashMap<>());
                index.push(i);
            }
        }

        /* 表示值为对象的替换占位标签 */
        String objectTag = "Object->@**" + TimeUtil.format(TimeUtil.now(), TimeUtil.Format_PRC_S) + "**";

        AtomicReference<String> jsonTemp = new AtomicReference<>(json);
        /* 结构临时寄存器 */
        Map<String, Object> structureTemp = new HashMap<>();
        /* 从最后一个结构标签开始遍历 */
        for (int i = structure.size() - 1; i >= 0; i--) {
            /* 结构标签对应的对象 */
            Object o = structure.pop();
            /* 左结构标签 */
            String tagLeft = tags.pop();
            /* 左结构标签下标 */
            int indexLeft = index.pop();
            /* 右结构标签 */
            String tagRight = "{".equals(tagLeft) ? "}" : "]";
            /* 右结构标签下标 */
            int indexRight = jsonTemp.get().indexOf(tagRight, indexLeft);

            /* 截取当前结构标签内容 */
            String jsonItemStr = jsonTemp.get().substring(indexLeft + 1, indexRight);

            String objectTagTemp = objectTag + "-" + i;
            if (indexLeft != 0) {
                /* 当前结构不是根结构 则截取掉本次处理的子结构内容 */
                jsonTemp.set(jsonTemp.get().substring(0, indexLeft) + objectTagTemp + jsonTemp.get().substring(indexRight + 1));
            }

            if (tagLeft.equals("{")) {
                /* 解析Map结构 */
                parseMapParams(jsonItemStr, o, structureTemp, objectTag);
            } else if (tagLeft.equals("[")) {
                /* 解析数组结构 */
                @SuppressWarnings("unchecked")
                Collection<Object> collectionTemp = ((Collection<Object>) o);

                Arrays.asList(jsonItemStr.split(",")).forEach(item -> {
                    if (item.startsWith(objectTag) && structureTemp.containsKey(item)) {
                        collectionTemp.add(structureTemp.get(item));
                        structureTemp.remove(item);
                    } else {
                        collectionTemp.add(parseStrType(item));
                    }
                });
            }

            /* 结构临时寄存器压栈 */
            structureTemp.put(objectTagTemp, o);
        }
        /* 返回根结构对象 */
        return structureTemp.values().iterator().next();
    }

    /**
     * 解析字符串值类型
     *
     * @param str 字符串
     * @return 解析后的值
     */
    public static Object parseStrType(String str) {
        if (str.startsWith("\"") && str.endsWith("\"") && str.length() > 1) {
            return str.substring(1, str.length() - 1);
        } else if (str.matches("^(0|[1-9][0-9]*|-[1-9][0-9]*)(\\.(0|[1-9][0-9]*|-[1-9][0-9]*))?$")) {
            /* 整数或浮点类型 */
            return new BigDecimal(str);
        } else if (str.toLowerCase().matches("true|false")) {
            return Boolean.parseBoolean(str);
        } else if (str.toLowerCase().matches("null")) {
            return null;
        } else {
            /* 字符串类型 */
            return str;
        }
    }

    /**
     * 解析json参数
     *
     * @param jsonItem      json{}[]中的单元参数字符串
     * @param paramObject   对应的对象
     * @param structureTemp json 结构栈
     * @param objectTag     表示值为对象的替换占位标签
     */
    private static void parseMapParams(String jsonItem, Object paramObject, Map<String, Object> structureTemp, String objectTag) {
        if (Map.class.isAssignableFrom(paramObject.getClass())) {
            @SuppressWarnings("unchecked")
            Map<Object, Object> mapTemp = ((Map<Object, Object>) paramObject);
            String[] jsonItems = jsonItem.split(",");
            for (int i = jsonItems.length - 1; i >= 0; i--) {
                String param = jsonItems[i];

                if (param.contains(":")) {
                    String[] keyValue = param.split(":");
                    String key = keyValue[0].trim();
                    String value = keyValue[1].trim();

                    if (value.startsWith(objectTag) && structureTemp.containsKey(value)) {
                        mapTemp.put(parseStrType(key), structureTemp.get(value));
                        structureTemp.remove(value);
                    } else {
                        mapTemp.put(parseStrType(key), parseStrType(value));
                    }
                }
            }
        }
    }


    public static void main(String[] args) {
        String jsonString = "{\n" +
                "  \"outChannelCode\": \"30\",\n" +
                "  \"channelCode\": \"U1521401829\",\n" +
                "  \"channelOrderNo\": \"98f26f4efb804b2fa265b0fdd602880d\",\n" +
                "  \"goodsCode\": \"GJXWY2023300001\",\n" +
                "  \"planCode\": \"KS0301P001\",\n" +
                "  \"businessOrgCode\": \"1101250000\",\n" +
                "  \"businessOrgName\": \"互联网业务部-业务部\",\n" +
                "  \"issueOrgCode\": \"1101250000\",\n" +
                "  \"issueOrgName\": \"互联网业务部-业务部\",\n" +
                "  \"applyTime\": \"2023-08-11 09:00:00\",\n" +
                "  \"effectiveTime\": \"2023-08-14 00:00:00\",\n" +
                "  \"expireTime\": \"2024-08-13 23:59:59\",\n" +
                "  \"applyNum\": 1,\n" +
                "  \"initialPremium\": 19.90,\n" +
                "  \"totalPremium\": 238.800000,\n" +
                "  \"paymentFrequency\": \"MONTHLY\",\n" +
                "  \"policyHolder\": {\n" +
                "    \"holderType\": \"PERSON\",\n" +
                "    \"personalCustomerPerson\": {\n" +
                "      \"name\": \"张德歧\",\n" +
                "      \"firstName\": null,\n" +
                "      \"certType\": \"SHENFENZHENG\",\n" +
                "      \"certNo\": \"211202195801200018\",\n" +
                "      \"gender\": \"MALE\",\n" +
                "      \"birthday\": \"1958-01-20 00:00:00\",\n" +
                "      \"age\": 65,\n" +
                "      \"nationality\": \"CHA\",\n" +
                "      \"jobCode\": null,\n" +
                "      \"jobPosition\": null,\n" +
                "      \"jobClass\": null,\n" +
                "      \"incomeType\": null,\n" +
                "      \"income\": null,\n" +
                "      \"mobile\": \"13704108110\",\n" +
                "      \"email\": null,\n" +
                "      \"addressList\": [],\n" +
                "      \"othPersonalAddressList\": [],\n" +
                "      \"hasSocialInsurance\": null,\n" +
                "      \"socialType\": null,\n" +
                "      \"isCensusTown\": null,\n" +
                "      \"censusAddress\": null,\n" +
                "      \"hasDisabledCert\": null,\n" +
                "      \"disabledCertNo\": null,\n" +
                "      \"originalCertNo\": null,\n" +
                "      \"originalName\": null\n" +
                "    }\n" +
                "  },\n" +
                "  \"policyInsured\": {\n" +
                "    \"name\": \"张德歧\",\n" +
                "    \"certType\": \"SHENFENZHENG\",\n" +
                "    \"certNo\": \"211202195801200018\",\n" +
                "    \"gender\": \"MALE\",\n" +
                "    \"birthday\": \"1958-01-20 00:00:00\",\n" +
                "    \"age\": 65,\n" +
                "    \"nationality\": \"CHA\",\n" +
                "    \"jobCode\": \"\",\n" +
                "    \"hasSocialInsurance\": \"NO\",\n" +
                "    \"socialType\": \"\",\n" +
                "    \"isCensusTown\": \"\",\n" +
                "    \"censusAddress\": \"\",\n" +
                "    \"hasDisabledCert\": \"N\",\n" +
                "    \"disabledCertNo\": \"\",\n" +
                "    \"relationToPH\": \"SELF\",\n" +
                "    \"planCode\": \"KS0301P001\",\n" +
                "    \"beneficiaryList\": [],\n" +
                "    \"personalInsuredSchool\": {\n" +
                "      \"age\": 0\n" +
                "    }\n" +
                "  },\n" +
                "  \"renewal\": \"YES\",\n" +
                "  \"productList\": [\n" +
                "    {\n" +
                "      \"productCode\": \"2719C01\",\n" +
                "      \"chargePeriodType\": \"BY_MONTH\",\n" +
                "      \"coveragePeriodType\": \"PER_MONTH\",\n" +
                "      \"chargeYear\": 1,\n" +
                "      \"coverageYear\": 1,\n" +
                "      \"paymentFrequency\": \"MONTHLY\",\n" +
                "      \"liabilityList\": [\n" +
                "        {\n" +
                "          \"productCode\": \"2719C01\",\n" +
                "          \"liabilityCode\": \"2719C01001\",\n" +
                "          \"sumInsured\": 50000.0,\n" +
                "          \"sumInsuredOptional\": false\n" +
                "        },\n" +
                "        {\n" +
                "          \"productCode\": \"2719C01\",\n" +
                "          \"liabilityCode\": \"2719C01002\",\n" +
                "          \"sumInsured\": 10000.0,\n" +
                "          \"sumInsuredOptional\": false\n" +
                "        }\n" +
                "      ],\n" +
                "      \"sumInsuredOptional\": false\n" +
                "    },\n" +
                "    {\n" +
                "      \"productCode\": \"2719F01\",\n" +
                "      \"chargePeriodType\": \"BY_MONTH\",\n" +
                "      \"coveragePeriodType\": \"PER_MONTH\",\n" +
                "      \"chargeYear\": 1,\n" +
                "      \"coverageYear\": 1,\n" +
                "      \"paymentFrequency\": \"MONTHLY\",\n" +
                "      \"liabilityList\": [\n" +
                "        {\n" +
                "          \"productCode\": \"2719F01\",\n" +
                "          \"liabilityCode\": \"2719F01001\",\n" +
                "          \"sumInsured\": 1000000.0,\n" +
                "          \"sumInsuredOptional\": false\n" +
                "        },\n" +
                "        {\n" +
                "          \"productCode\": \"2719F01\",\n" +
                "          \"liabilityCode\": \"2719F01002\",\n" +
                "          \"sumInsured\": 200000.0,\n" +
                "          \"sumInsuredOptional\": false\n" +
                "        }\n" +
                "      ],\n" +
                "      \"sumInsuredOptional\": false\n" +
                "    }\n" +
                "  ],\n" +
                "  \"isGift\": \"NO\",\n" +
                "  \"installmentNo\": 0,\n" +
                "  \"dueDate\": \"2023-08-14 10:31:45\",\n" +
                "  \"regionCode\": \"\",\n" +
                "  \"salesInfo\": {\n" +
                "    \"channelCode\": \"U1521401829\",\n" +
                "    \"channelName\": \"升华茂林保险销售服务有限公司\",\n" +
                "    \"agreementCode\": \"U1521401829-03\",\n" +
                "    \"agreementName\": \"家享无忧组合产品协议\",\n" +
                "    \"bdSalesCode\": \"1101050000005\",\n" +
                "    \"bdSalesName\": \"武庆\",\n" +
                "    \"businessSource\": \"01\",\n" +
                "    \"channelCategory\": \"200\",\n" +
                "    \"channelDetailCode\": \"230\"\n" +
                "  },\n" +
                "  \"waitingPeriod\": \"\",\n" +
                "  \"hesitationPeriod\": \"\",\n" +
                "  \"gracePeriod\": 0,\n" +
                "  \"sendSMSOption\": \"0\",\n" +
                "  \"sendEmailOption\": \"0\",\n" +
                "  \"immediateEffect\": \"1\",\n" +
                "  \"extraProperties\": {\n" +
                "    \"contract_no\": \"HKS03272023000015290\",\n" +
                "    \"group_no\": \"PG20230301110125A000015291\"\n" +
                "  },\n" +
                "  \"paySkipValidatorFlag\": false\n" +
                "}";

//        jsonString = "[\"contract_no\",\"fgfsd\",\"fdgsdf\",\"dafdsg\"]";
//        Object map = parseJsonObject(jsonString);
//        map = convertObject(map, List.class);
//        System.out.println("Map: " + map);


        TestVo<Integer> testVo = new TestVo<>();
        testVo.setValue("sdafew").setT(58964)
                .setT1(Arrays.asList(5, 89))
                .setT2(new BigDecimal("48.25")).setT3(89).setT4(new Integer[]{9, 8, 6, 7});

        jsonString = toJsonStr(testVo);

        Object object = parseJsonObject(jsonString);

        testVo = convertObject(object, TestVo.class);

        new TypeReference<TestVo<Integer>>() {
        };

        System.out.println("TestVo: " + jsonString);

    }


    public static <T> T convertObject(Object resource, Class<T> tClass) {
        if (Objects.isNull(resource) || Objects.isNull(tClass)) {
            return null;
        }

        T result = null;

        if (Collection.class.isAssignableFrom(resource.getClass()) || resource.getClass().isArray()) {
            /* 集合类型对象 */
            Collection<Object> collectionTemp = (Collection<Object>) resource;

            if (Collection.class.isAssignableFrom(tClass)) {
                result = ClassUtils.newInstance(tClass);
                Collection<Object> collectionItem = (Collection<Object>) result;

                Class<?> type = ClassUtils.getGenerics(tClass, 0);
                if (Objects.nonNull(type)) {
                    for (Object o : collectionTemp) {
                        if (!ClassUtils.isBasicType(type) && !Class.class.isAssignableFrom(type)) {
                            o = convertObject(o, type);
                        } else {
                            o = ClassUtils.parseValueType(type, o);
                        }
                        collectionItem.add(o);
                    }
                }
            } else if (tClass.isArray()) {
                result = ClassUtils.newInstanceArray(tClass, collectionTemp.size());

                Class<?> type = ClassUtils.getClassForArray(tClass);

                Iterator<Object> iterator = collectionTemp.iterator();
                int index = 0;
                while (iterator.hasNext()) {
                    Object o = iterator.next();
                    if (!ClassUtils.isBasicType(type) && !Class.class.isAssignableFrom(type)) {
                        o = convertObject(o, type);
                    } else {
                        o = ClassUtils.parseValueType(type, o);
                    }
                    Array.set(result, index++, o);
                }
            }
        } else if (ClassUtils.isBasicType(tClass) || tClass.isAssignableFrom(Object.class) || Class.class.isAssignableFrom(tClass)) {
            /* 基础值类型 */
            result = tClass.cast(ClassUtils.parseValueType(tClass, resource));
        } else {
            /* 其他类型 */
            result = ClassUtils.newInstance(tClass);

            /* 数据源转换为Map */
            Map<Object, Object> objectMap = null;
            if (Map.class.isAssignableFrom(resource.getClass())) {
                objectMap = (Map<Object, Object>) resource;
            } else {
                Set<Field> fields = ClassUtils.getFields(resource.getClass());
                objectMap = fields.stream().collect(Collectors.toMap(Field::getName, field -> ClassUtils.getValue(field, resource), (k1, k2) -> k1));
            }

            /* 创建新对象并赋值 */
            if (Map.class.isAssignableFrom(tClass)) {
                Map<Object, Object> TempMap = (Map<Object, Object>) result;
                objectMap.forEach((k, v) -> {
                    if (Objects.nonNull(v) && !ClassUtils.isBasicType(v.getClass())) {
                        v = convertObject(v, v.getClass());
                    }
                    TempMap.put(k, v);
                });
            } else {
                Set<Field> fields = ClassUtils.getFields(tClass);
                for (Field field : fields) {
                    JsonField jsonField = JsonField.bulidJsonField(field);

//                    if (jsonField.getIsIgnore()){
//                        continue;
//                    }

                    Object value = objectMap.get(jsonField.getName());

                    if (Objects.nonNull(value)) {
                        if (!ClassUtils.isBasicType(jsonField.getType())) {
                            value = convertObject(value, jsonField.getType());
                        }
                        ClassUtils.setValue(result, field, value);
                    }
                }
            }
        }

        return result;
    }

    @Data
    @Accessors(chain = true)
    public static class TestVo<T> {
        private String value;
        private T t;
        private List<Integer> t1;
        private BigDecimal t2;
        private Integer t3;
        private Integer[] t4;
    }

    @Data
    @Accessors(chain = true)
    public static abstract class TypeReference<T> {
        protected final Type type;

        protected TypeReference() {
            Type superClass = this.getClass().getGenericSuperclass();
            if (superClass instanceof Class) {
                throw new IllegalArgumentException("Internal error: TypeReference constructed without actual type information");
            } else {
                this.type = ((ParameterizedType) superClass).getActualTypeArguments()[0];
            }
        }

//        protected TypeReference<?> getChildTypeReference(){
//
//        }
    }

    /**
     * 获取字段的注解配置
     */
    @Data
    @Accessors(chain = true)
    private static class JsonField {
        /**
         * 字段名称
         */
        private String fieldName;
        /**
         * 字段格式化名称
         */
        private String name;

        /**
         * 字段所属类
         */
        private Class<?> declaringClass;

        /**
         * 字段类型
         */
        private Class<?> type;

        /**
         * 字段格式
         */
        private String format;
        /**
         * 时区
         */
        private String timezone;
        /**
         * 排序
         */
        private Integer ordinal = 0;
        /**
         * 是否忽略字段
         */
        private Boolean isIgnore = false;

        public static JsonField bulidJsonField(Field field) {
            JsonField jsonField = new JsonField()
                    .setFieldName(field.getName())
                    .setName(field.getName())
                    .setType(field.getType())
                    .setDeclaringClass(field.getDeclaringClass());

            try {
                /* fastjson 字段配置 */
                Annotation annotation = field.getAnnotation(ClassUtils.getAnnotationClass("com.alibaba.fastjson.annotation.JSONField"));
                Method method = ClassUtils.getMethod(annotation.getClass(), "name");
                Object returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                    jsonField.setName(String.valueOf(returnValue));
                }
                method = ClassUtils.getMethod(annotation.getClass(), "format");
                returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                    jsonField.setFormat(String.valueOf(returnValue));
                }
                method = ClassUtils.getMethod(annotation.getClass(), "ordinal");
                returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                    jsonField.setOrdinal(Integer.valueOf(String.valueOf(returnValue)));
                }
            } catch (Exception ignored) {
            }

            try {
                /* jackson 格式化配置 */
                Annotation annotation = field.getAnnotation(ClassUtils.getAnnotationClass("com.fasterxml.jackson.annotation.JsonFormat"));
                Method method = ClassUtils.getMethod(annotation.getClass(), "pattern");
                Object returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                    jsonField.setFormat(String.valueOf(returnValue));
                }
                method = ClassUtils.getMethod(annotation.getClass(), "timezone");
                returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                    jsonField.setTimezone(String.valueOf(returnValue));
                }
            } catch (Exception ignored) {
            }

            try {
                /* jackson 字段配置 */
                Annotation annotation = field.getAnnotation(ClassUtils.getAnnotationClass("com.fasterxml.jackson.annotation.JsonProperty"));
                Method method = ClassUtils.getMethod(annotation.getClass(), "value");
                Object returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                    jsonField.setName(String.valueOf(returnValue));
                }
            } catch (Exception ignored) {
            }

            try {
                /* jackson 忽略字段配置 */
                Annotation annotation = field.getDeclaringClass().getAnnotation(ClassUtils.getAnnotationClass("com.fasterxml.jackson.annotation.JsonIgnoreProperties"));
                Method method = ClassUtils.getMethod(annotation.getClass(), "value");
                Object returnValue = method.invoke(annotation);
                if (Objects.nonNull(returnValue) && returnValue.getClass().isArray() && (
                        Arrays.asList((String[]) returnValue).contains(jsonField.getFieldName()) || Arrays.asList((String[]) returnValue).contains(jsonField.getName()))
                ) {
                    jsonField.setIsIgnore(true);
                }
            } catch (Exception ignored) {
            }
            return jsonField;
        }

        /**
         * 执行字段的 Get方法
         *
         * @return Get方法结果
         */
        public Object getter(Object bean) {
            if (Objects.nonNull(bean)) {
                Method method = ClassUtils.getter(this.declaringClass, this.fieldName);
                return ClassUtils.methodInvoke(method, bean);
            }
            return null;
        }

        /**
         * 执行字段的 Set方法
         *
         * @return Set方法结果
         */
        public Object setter(Object bean, Object... args) {
            if (Objects.nonNull(bean)) {
                Method method = ClassUtils.setter(this.declaringClass, this.fieldName);
                return ClassUtils.methodInvoke(method, bean, args);
            }
            return null;
        }
    }
}
