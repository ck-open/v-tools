package com.ck.tools.reflect;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.tools.Diagnostic;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 源码编译管理器
 *
 * @author cyk
 * @since 2023/6/2 13:06
 **/
@Data
@Accessors(chain = true)
public class JavaCompilerManager {
    /**
     * 编译结果
     */
    private boolean result;

    /**
     * 编译异常信息
     */
    private List<Diagnostic<? extends JavaFileObject>> diagnostic = new ArrayList<>();

    /**
     * 加载后的Class
     */
    private Map<String, Class<?>> classMap = new LinkedHashMap<>();

    /**
     * java 源码文件管理器
     */
    private JavaFileManager javaFileManager;

    /**
     * 编译的class 名称列表
     */
    private List<String> classNames = new ArrayList<>();

    /**
     * 二进制 class 文件
     */
    private String classFile;

    private JavaCompilerManager() {
    }

    /**
     * 加载Class类
     */
    public JavaCompilerManager classLoader() {
        this.classNames.forEach(className -> {
            try {
                if (result) {
                    classMap.put(className, this.javaFileManager.getClassLoader(StandardLocation.CLASS_PATH).loadClass(className));
                } else {
                    classMap.put(className, Class.forName(className));
                }
            } catch (Exception e) {
                throw new RuntimeException("java source ClassLoader failed className: " + className, e);
            }
        });
        return this;
    }

    public List<String> getDiagnosticMsg() {
        return this.diagnostic.stream().map(i -> i.getMessage(null)).collect(Collectors.toList());
    }

    public static JavaCompilerManager build(JavaFileManager javaFileManager) {
        return new JavaCompilerManager().setJavaFileManager(javaFileManager);
    }
}
