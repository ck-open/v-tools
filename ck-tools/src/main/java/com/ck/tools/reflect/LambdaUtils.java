
package com.ck.tools.reflect;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Function 工具类
 *
 * @author cyk
 * @since 2021-10-01
 */
public class LambdaUtils {

    /**
     * 根据当前线程缓存SerializedLambda对象
     */
    private static final Map<Class<?>, LambdaUtils.SerializedLambda> serializedLambdaCache = new ConcurrentHashMap<>();


    /**
     * 通过反序列化转换 Function 函数表达式为SerializedLambda 对象并缓存到当前线程中
     *
     * @param func Function 可序列化的函数对象
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     */
    private synchronized static <E, R, T extends Function<E, R> & Serializable> void resolveSLambda(T func) {
        if (!func.getClass().isSynthetic()) {
            throw new RuntimeException("该方法仅能传入  Lambda::getLambda()表达式产生的函数类Function<?, ?>");
        }

        if (isEmpty(func)) {
            Map<String, Class<?>> primClasses = new HashMap<>();
            primClasses.put("java.lang.invoke.SerializedLambda", LambdaUtils.SerializedLambda.class);
            LambdaUtils.SerializedLambda s = ClassUtils.deserialize(ClassUtils.serialize(func), LambdaUtils.SerializedLambda.class, primClasses);
            if (s != null) {
                serializedLambdaCache.put(func.getClass(), s);
            }
        }
        getSerializedLambda(func);
    }

    /**
     * 线程下是否不存在 SerializedLambda 对象
     *
     * @param <E> 函数方法的参数泛型
     * @param <R> 函数方法的返回值泛型
     * @param <T> 函数泛型
     * @return 存在返回true
     */
    private static <E, R, T extends Function<E, R> & Serializable> boolean isEmpty(T func) {
        return !serializedLambdaCache.containsKey(func.getClass());
    }

    /**
     * 获取线程中的 SerializedLambda 可序列化的Lambda对象
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return 可序列化的Lambda对象
     */
    private static <E, R, T extends Function<E, R> & Serializable> LambdaUtils.SerializedLambda getSerializedLambda(T func) {
        if (isEmpty(func)) {
            resolveSLambda(func);
        }
        return serializedLambdaCache.get(func.getClass());
    }

    /**
     * 将字符串中的 / 替换为 .
     *
     * @param str 需要替换的字符串
     * @return 替换后的字符串
     */
    private static String replaceSlash(String str) {
        return str.replace('/', '.');
    }

    /**
     * 获取接口 函数 class 名称
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return 返回 class 名称
     */
    public static <E, R, T extends Function<E, R> & Serializable> String functionalInterfaceClassName(T func) {
        return replaceSlash(getSerializedLambda(func).getFunctionalInterfaceClass());
    }


    /**
     * 获取实现的 class
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return 实现类
     */
    public static <E, R, T extends Function<E, R> & Serializable> Class<?> implClass(T func) {
        return ClassUtils.getClassForName(replaceSlash(getSerializedLambda(func).getImplClass()));
    }

    /**
     * 如果传入的是get 方法则返回此方法的字段名称
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return get方法名去掉get字符后转换的字段名称
     */
    public static <E, R, T extends Function<E, R> & Serializable> String fieldName(T func) {
        String methodName = implMethodName(func);
        if (methodName.startsWith("get")) {
            methodName = methodName.substring(3);
            methodName = methodName.substring(0, 1).toLowerCase() + methodName.substring(1);
        }
        return methodName;
    }

    /**
     * 获取函数方法名称
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return 方法名称
     */
    public static <E, R, T extends Function<E, R> & Serializable> String implMethodName(T func) {
        return getSerializedLambda(func).getImplMethodName();
    }


    /**
     * 获取实例化方法的类型
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return 获取实例化方法的类型
     */
    public static <E, R, T extends Function<E, R> & Serializable> Class<?> instantiatedType(T func) {
        String instantiatedTypeName = replaceSlash(getSerializedLambda(func).getInstantiatedMethodType().substring(2, getSerializedLambda(func).getInstantiatedMethodType().indexOf(';')));
        return ClassUtils.getClassForName(instantiatedTypeName);
    }

    /**
     * 获取字符串形式函数形参
     *
     * @param func 函数
     * @param <E>  函数方法的参数泛型
     * @param <R>  函数方法的返回值泛型
     * @param <T>  函数泛型
     * @return 字符串形式函数形参
     */
    public static <E, R, T extends Function<E, R> & Serializable> String parameterToString(T func) {
        String implName = Objects.requireNonNull(implClass(func)).getName();
        return String.format("%s::%s",
                implName.substring(implName.lastIndexOf('.') + 1),
                getSerializedLambda(func).getImplMethodName());
    }




    /**
     * 这个类是 {@link java.lang.invoke.SerializedLambda} 的镜像类，
     *
     * <p>转换 {@link java.lang.invoke.SerializedLambda} 中的final属性</p>
     * <p>负责将一个支持序列的 Function Lambda表达式 序列化为 SerializedLambda</p>
     *
     */
    @SuppressWarnings("unused")
    private static class SerializedLambda implements Serializable {

        private static final long serialVersionUID = 8025925345765570181L;

        private Class<?> capturingClass;
        private String functionalInterfaceClass;
        private String functionalInterfaceMethodName;
        private String functionalInterfaceMethodSignature;
        private String implClass;
        private String implMethodName;
        private String implMethodSignature;
        private int implMethodKind;
        private String instantiatedMethodType;
        private Object[] capturedArgs;

        public void setCapturingClass(Class<?> capturingClass) {
            this.capturingClass = capturingClass;
        }

        public void setFunctionalInterfaceClass(String functionalInterfaceClass) {
            this.functionalInterfaceClass = functionalInterfaceClass;
        }

        public void setFunctionalInterfaceMethodName(String functionalInterfaceMethodName) {
            this.functionalInterfaceMethodName = functionalInterfaceMethodName;
        }

        public void setFunctionalInterfaceMethodSignature(String functionalInterfaceMethodSignature) {
            this.functionalInterfaceMethodSignature = functionalInterfaceMethodSignature;
        }

        public void setImplClass(String implClass) {
            this.implClass = implClass;
        }

        public void setImplMethodName(String implMethodName) {
            this.implMethodName = implMethodName;
        }

        public void setImplMethodSignature(String implMethodSignature) {
            this.implMethodSignature = implMethodSignature;
        }

        public void setImplMethodKind(int implMethodKind) {
            this.implMethodKind = implMethodKind;
        }

        public void setInstantiatedMethodType(String instantiatedMethodType) {
            this.instantiatedMethodType = instantiatedMethodType;
        }

        public void setCapturedArgs(Object[] capturedArgs) {
            this.capturedArgs = capturedArgs;
        }

        public Class<?> getCapturingClass() {
            return capturingClass;
        }

        public String getFunctionalInterfaceClass() {
            return functionalInterfaceClass;
        }

        public String getFunctionalInterfaceMethodName() {
            return functionalInterfaceMethodName;
        }

        public String getFunctionalInterfaceMethodSignature() {
            return functionalInterfaceMethodSignature;
        }

        public String getImplClass() {
            return implClass;
        }

        public String getImplMethodName() {
            return implMethodName;
        }

        public String getImplMethodSignature() {
            return implMethodSignature;
        }

        public int getImplMethodKind() {
            return implMethodKind;
        }

        public String getInstantiatedMethodType() {
            return instantiatedMethodType;
        }

        public Object[] getCapturedArgs() {
            return capturedArgs;
        }
    }
}
