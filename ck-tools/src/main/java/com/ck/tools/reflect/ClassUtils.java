package com.ck.tools.reflect;

import com.ck.tools.utils.TimeUtil;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.invoke.*;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class 操作工具类
 *
 * @author cyk
 * @since 2020-01-01
 */
public class ClassUtils {
    private static final Logger log = Logger.getLogger(ClassUtils.class.getName());

    private ClassUtils() {
    }

    private static final char PACKAGE_SEPARATOR = '.';
    /**
     * 代理 class 的名称
     */
    private static final List<String> PROXY_CLASS_NAMES = Arrays.asList("net.sf.cglib.proxy.Factory"
            // cglib
            , "org.springframework.cglib.proxy.Factory"
            , "org.springframework.aop.SpringProxy"
            , "org.springframework.aop.framework.Advised"
            , "javassist.util.proxy.ProxyObject"
            // javassist
            , "org.apache.ibatis.binding.MapperProxy"
            , "org.apache.ibatis.javassist.util.proxy.ProxyObject");

    private static final List<Class<?>> basicTypes = Arrays.asList(
            String.class
            , CharSequence.class
            , Character.class
            , Date.class
            , Calendar.class
            , Temporal.class
            , Number.class
            , Boolean.class
            , boolean.class
            , char.class
            , byte.class
            , short.class
            , int.class
            , long.class
            , float.class
            , double.class);


    /**
     * 将给定字节数组序列化为对象。
     *
     * @param object 要序列化的字节数组
     * @return 表示序列化的对象
     */
    public static <T extends Serializable> T deserialize(byte[] object, Class<T> cla) {
        return deserialize(object, cla, null);
    }

    /**
     * 将给定字节数组序列化为对象。
     * 可以将一个对象转换序列化成另一个相同的其他对象
     *
     * @param object    要序列化的字节数组
     * @param cla       返回值类型
     * @param classConf 自定义类型,key(序列号对象的className):value(Class类型)
     * @return 表示序列化的对象
     */
    public static <T extends Serializable> T deserialize(byte[] object, Class<T> cla, Map<String, Class<?>> classConf) {
        if (object == null) {
            return null;
        }
        try (ObjectInputStream objIn = new ObjectInputStream(new ByteArrayInputStream(object)) {
            @Override
            protected Class<?> resolveClass(ObjectStreamClass objectStreamClass) throws IOException, ClassNotFoundException {
                Class<?> clazz = null;
                if (classConf != null && classConf.containsKey(objectStreamClass.getName())) {
                    clazz = classConf.get(objectStreamClass.getName());
                } else {
                    clazz = super.resolveClass(objectStreamClass);
                }
                return clazz;
            }
        }) {
            return cla.cast(objIn.readObject());
        } catch (ClassNotFoundException | IOException e) {
            throw new IllegalArgumentException("反序列化对象失败 ", e);
        }
    }

    /**
     * 将给定对象序列化为字节数组。
     *
     * @param object 要序列化的对象
     * @return 表示对象的字节数组
     */
    public static <T extends Serializable> byte[] serialize(T object) {
        if (object == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException("对象序列化失败: " + object.getClass(), e);
        }
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * 获取数据实例对象所有字段
     *
     * @param entityClass 实体类
     * @return 属性字段列表
     */
    public static Set<String> getFieldNames(Class<?> entityClass) {
        return getFields(entityClass).stream().map(Field::getName).collect(Collectors.toSet());
    }

    /**
     * 获取数据实例对象所有字段
     *
     * @param entityClass 实体类
     * @return 属性字段列表
     */
    public static Set<Field> getFields(Class<?> entityClass) {
        if (entityClass == null || Class.class.isAssignableFrom(entityClass)) return new HashSet<>();

        // 去除重复字段  避免子类重写后 重复添加父类的字段
        Set<String> fieldsDistinct = new HashSet<>();

        Set<Field> result = Stream.of(entityClass.getDeclaredFields())
                .filter(i -> !Modifier.isFinal(i.getModifiers()) && !Modifier.isStatic(i.getModifiers()) && fieldsDistinct.add(i.getName()))
                .collect(Collectors.toCollection(LinkedHashSet::new));

//        while (Objects.nonNull(entityClass.getSuperclass()) && !entityClass.getSuperclass().isAssignableFrom(Object.class)) {
        while (Objects.nonNull(entityClass.getSuperclass())) {
            entityClass = entityClass.getSuperclass();
            result.addAll(Stream.of(entityClass.getDeclaredFields())
                    .filter(i -> !Modifier.isFinal(i.getModifiers()) && fieldsDistinct.add(i.getName()))
                    .collect(Collectors.toSet()));
        }
        return result;
    }

    /**
     * 获取数据实例对象所有字段
     *
     * @param fieldName   字段名
     * @param entityClass 实体类
     * @param setIc       忽略大小写
     * @return 字段
     */
    public static Field getField(String fieldName, Class<?> entityClass, boolean setIc) {
        if (setIc) {
            Set<Field> fields = getFields(entityClass);
            for (Field field : fields) {
                if (field.getName().equalsIgnoreCase(fieldName)) {
                    return field;
                }
            }
            log.warning("获取类[" + entityClass.getName() + "]的属性[" + fieldName + "]时失败");
        } else {
            return getField(fieldName, entityClass);
        }
        return null;
    }

    /**
     * 获取数据实例对象所有字段
     *
     * @param entityClass 实体类
     * @return 字段
     */
    public static Field getField(String fieldName, Class<?> entityClass) {
        if (entityClass == null || fieldName == null || fieldName.trim().isEmpty()) {
            return null;
        }

        do {
            try {
                return entityClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                entityClass = entityClass.getSuperclass();
            }
        } while (Objects.nonNull(entityClass.getSuperclass()));
        log.warning("获取类[" + entityClass.getName() + "]的属性[" + fieldName + "]时失败");

        return null;
    }

    /**
     * 获取类方法
     *
     * @param entityClass 实体类
     * @return 方法列表
     */
    public static Set<Method> getMethods(Class<?> entityClass) {
        return getMethodsAll(entityClass, Modifier::isPublic);
    }

    /**
     * 获取类方法
     *
     * @param entityClass 实体类
     * @return 方法列表
     */
    public static Set<Method> getMethodsAll(Class<?> entityClass, Function<Integer, Boolean> modifier) {
        if (entityClass == null) {
            return null;
        }
        if (modifier != null && !modifier.apply(entityClass.getModifiers())) {
            return null;
        }
        Set<Method> methods = new LinkedHashSet<>();
        do {
            methods.addAll(Arrays.asList(entityClass.getDeclaredMethods()));
            entityClass = entityClass.getSuperclass();
        } while (Objects.nonNull(entityClass.getSuperclass()));
        return methods;
    }

    /**
     * 获取类方法
     *
     * @param entityClass    实体类
     * @param methodName     方法名称
     * @param parameterTypes 参数类型列表
     * @return 方法
     */
    public static Method getMethod(Class<?> entityClass, String methodName, Class<?>... parameterTypes) {
        return getMethodAll(entityClass, methodName, Modifier::isPublic, parameterTypes);
    }

    /**
     * 获取类方法
     *
     * @param entityClass    实体类
     * @param methodName     方法名称
     * @param parameterTypes 参数类型列表
     * @return 方法
     */
    public static Method getMethodAll(Class<?> entityClass, String methodName, Function<Integer, Boolean> modifier, Class<?>... parameterTypes) {
        if (entityClass == null || methodName == null || methodName.trim().isEmpty()) {
            return null;
        }
        if (modifier != null && !modifier.apply(entityClass.getModifiers())) {
            return null;
        }

        Class<?> tempClass = entityClass;
        do {
            try {
                return tempClass.getDeclaredMethod(methodName, parameterTypes);
            } catch (NoSuchMethodException e) {
                tempClass = tempClass.getSuperclass();
            }
        } while (Objects.nonNull(tempClass.getSuperclass()));
        return null;
    }

    /**
     * 执行类方法
     *
     * @param method 方法对象
     * @param bean   所属类对象
     * @param args   方法参数列表
     * @return 方法返回值
     */
    public static Object methodInvoke(Method method, Object bean, Object... args) {
        if (Objects.nonNull(method) && Modifier.isPublic(method.getModifiers())) {
            try {
                return method.invoke(bean, args);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    /**
     * 判断对象是否基本类型或常见类型
     * 是则返回true
     */
    public static boolean isBasicType(Class<?> cla) {
        if (cla != null) {
            for (Class<?> type : basicTypes) {
                if (type.isAssignableFrom(cla)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否是基本类型或基本包装类
     *
     * @param cla 类
     * @return 是基本类型返回true
     */
    public static boolean isBasicClass(Class<?> cla) {
        try {
            return boolean.class.isAssignableFrom(cla) || int.class.isAssignableFrom(cla) || long.class.isAssignableFrom(cla)
                    || double.class.isAssignableFrom(cla) || float.class.isAssignableFrom(cla)
                    || char.class.isAssignableFrom(cla) || short.class.isAssignableFrom(cla);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 获取属性值
     *
     * @param field 属性字段
     * @param o     对象
     * @return 属性字段的值 或 null
     */
    public static Object getValueOrNull(Field field, Object o) {
        try {
            field.setAccessible(true);
            return field.get(o);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取属性值
     *
     * @param field 属性字段
     * @param o     对象
     * @return 属性字段的值 或 null
     */
    public static Object getValue(Field field, Object o) {
        if (field == null || o == null) {
            return null;
        }
        try {
            field.setAccessible(true);
            return field.get(o);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(String.format("获取属性值异常，属性:%s ", field.getName()));
        }
    }

    /**
     * 设置属性值
     *
     * @param bean      对象
     * @param fieldName 属性字段名称
     * @param val       要设置的值
     */
    public static void setValue(Object bean, String fieldName, Object val) {
        setValue(bean, getField(fieldName, bean.getClass()), val);
    }

    /**
     * 设置属性值
     *
     * @param bean  对象
     * @param field 属性字段
     * @param val   要设置的值
     */
    public static void setValue(Object bean, Field field, Object val) {
        if (bean != null && field != null && val != null) {
            try {
                field.setAccessible(true);
                field.set(bean, parseValueType(field, val));
            } catch (IllegalAccessException ignored) {
                throw new IllegalArgumentException(String.format("对象参数设置失败， name:%s  value:%s  Class:%s ", field.getName(), val, bean.getClass().getName()));
            }
        }
    }

    /**
     * 数据类型解析转换
     *
     * @param field 实体字段
     * @param value 需要转换的值
     * @return Object
     * @author Cyk
     * @since 17:28 2022/4/27
     **/
    public static Object parseValueType(Field field, Object value) {
        if (field == null) {
            return value;
        }
        return parseValueType(field.getType(), value);
    }


    /**
     * 数据类型解析转换
     *
     * @param cla   例如：String.class.getName() || Field.getType().getName()
     * @param value new Object()
     * @return Object
     * @author Cyk
     * @since 17:15 2020-11-06
     **/
    public static Object parseValueType(Class<?> cla, Object value) {
        if (cla == null || value == null) {
            return null;
        }
        if (value.getClass().isAssignableFrom(cla)) {
            return value;
        }

        if (String.class.isAssignableFrom(cla)) {
            value = value.toString();
        } else if (Date.class.isAssignableFrom(cla)) {
            value = TimeUtil.parseTime(value.toString());
        } else if (Calendar.class.isAssignableFrom(cla)) {
            value = TimeUtil.parseCalendar(value.toString());
        } else if (LocalDate.class.isAssignableFrom(cla)) {
            value = TimeUtil.formatLocalDate(TimeUtil.parseTime(value.toString()));
        } else if (LocalTime.class.isAssignableFrom(cla)) {
            value = TimeUtil.formatLocalDateTime(TimeUtil.parseTime(value.toString()));
        } else if (LocalDateTime.class.isAssignableFrom(cla)) {
            value = TimeUtil.formatLocalDateTime(TimeUtil.parseTime(value.toString()));
        } else if (Long.class.isAssignableFrom(cla) || long.class.isAssignableFrom(cla)) {
            value = Long.parseLong(value.toString());
        } else if (Integer.class.isAssignableFrom(cla) || int.class.isAssignableFrom(cla)) {
            value = Integer.parseInt(value.toString());
        } else if (Float.class.isAssignableFrom(cla) || float.class.isAssignableFrom(cla)) {
            value = Float.parseFloat(value.toString());
        } else if (Double.class.isAssignableFrom(cla) || double.class.isAssignableFrom(cla)) {
            value = Double.parseDouble(value.toString());
        } else if (Short.class.isAssignableFrom(cla) || short.class.isAssignableFrom(cla)) {
            value = Short.valueOf(value.toString());
        } else if (BigDecimal.class.isAssignableFrom(cla)) {
            value = new BigDecimal(value.toString());
        } else if (BigInteger.class.isAssignableFrom(cla)) {
            value = new BigInteger(value.toString());
        } else if (Boolean.class.isAssignableFrom(cla) || boolean.class.isAssignableFrom(cla)) {
            value = Boolean.valueOf(value.toString());
        } else if (char.class.isAssignableFrom(cla)) {
            value = value.toString();
        }
        return value;
    }

    /**
     * 解析递归名称字符串
     *
     * @param names    类地址名、或其他有分隔符名称列表
     * @param consumer 以 .切分后的每个名称消费函数
     */
    public static void forNames(String names, BiConsumer<String, Boolean> consumer) {
        forNames(names, ".", consumer);
    }

    /**
     * 解析递归名称字符串
     *
     * @param names    类地址名、或其他有分隔符名称列表
     * @param split    分隔符
     * @param consumer 以 .切分后的每个名称消费函数
     */
    public static void forNames(String names, String split, BiConsumer<String, Boolean> consumer) {
        if (names == null || split == null || consumer == null) {
            return;
        }
        while (!names.isEmpty()) {
            String itemName;
            if (names.contains(split)) {
                itemName = names.substring(0, names.indexOf(split));
                names = names.substring(names.indexOf(split) + 1);
            } else {
                itemName = names;
                names = "";
            }
            consumer.accept(itemName, names.isEmpty());
        }
    }

    /**
     * 获取字段的 Get方法
     *
     * @param field 字段
     * @return Get方法
     */
    public static Method getter(Field field) {
        if (field != null) {
            return getter(field.getDeclaringClass(), field.getName());
        }
        return null;
    }

    /**
     * 获取字段的 Get方法
     *
     * @param declaringClass 所属类
     * @param fieldName      字段名称
     * @return Get方法
     */
    public static Method getter(Class<?> declaringClass, String fieldName) {
        if (declaringClass != null && fieldName != null && !fieldName.trim().isEmpty()) {
            Method method = ClassUtils.getMethod(declaringClass, getterName(fieldName));
            if (method == null && fieldName.startsWith("is")) {
                method = ClassUtils.getMethod(declaringClass, fieldName);
            }
            return method;
        }
        return null;
    }

    /**
     * 获取字段的 Get方法名
     *
     * @param field 字段
     * @return Get方法名
     */
    public static String getterName(Field field) {
        if (field != null) {
            return getterName(field.getName());
        }
        return null;
    }

    /**
     * 获取字段的 Get方法名
     *
     * @param fieldName 字段名称
     * @return Get方法名
     */
    public static String getterName(String fieldName) {
        if (fieldName != null && !fieldName.trim().isEmpty()) {
            return "get" + fieldName.substring(0, 1).toUpperCase() + (fieldName.length() > 1 ? fieldName.substring(1) : "");
        }
        return null;
    }

    /**
     * 获取字段的 Set方法
     *
     * @param field 字段
     * @return Set方法
     */
    public static Method setter(Field field) {
        if (field != null) {
            return setter(field.getDeclaringClass(), field.getName());
        }
        return null;
    }

    /**
     * 获取字段的 Set方法
     *
     * @param declaringClass 所属类
     * @param fieldName      字段名称
     * @return Set方法
     */
    public static Method setter(Class<?> declaringClass, String fieldName) {
        if (declaringClass != null && fieldName != null && !fieldName.trim().isEmpty()) {
            Method method = ClassUtils.getMethod(declaringClass, setterName(fieldName));

            if (method == null && fieldName.startsWith("is")) {
                String methodName = fieldName.equals("is") ? fieldName : fieldName.substring(2, 3).toUpperCase() + (fieldName.length() > 3 ? fieldName.substring(3) : "");
                method = ClassUtils.getMethod(declaringClass, methodName);
            }
            return method;
        }
        return null;
    }

    /**
     * 获取字段的 Set方法名
     *
     * @param field 字段
     * @return Set方法名
     */
    public static String setterName(Field field) {
        if (field != null) {
            return setterName(field.getName());
        }
        return null;
    }

    /**
     * 获取字段的 Set方法名
     *
     * @param fieldName 字段名称
     * @return Set方法名
     */
    public static String setterName(String fieldName) {
        if (fieldName != null && !fieldName.trim().isEmpty()) {
            return "set" + fieldName.substring(0, 1).toUpperCase() + (fieldName.length() > 1 ? fieldName.substring(1) : "");
        }
        return null;
    }

    /**
     * 判断是否为代理对象
     *
     * @param clazz 传入 class 对象
     * @return 如果对象class是代理 class，返回 true
     */
    public static boolean isProxy(Class<?> clazz) {
        if (clazz != null) {
            for (Class<?> cls : clazz.getInterfaces()) {
                if (PROXY_CLASS_NAMES.contains(cls.getName())) {
                    return true;
                }
            }
        }
        return Proxy.isProxyClass(clazz);
    }

    /**
     * 获取代理类的原对象
     *
     * @param proxy 代理类对象
     * @param <T>   t   代理类对象类型
     * @return 代理类对象
     */
    public static <T> T getProxyTarget(Object proxy, Class<T> t) {
        return t.cast(getProxyTarget(proxy));
    }

    /**
     * 获取代理类的原对象
     *
     * @param proxy 代理类对象
     * @return 代理类对象
     */
    public static Object getProxyTarget(Object proxy) {
        while (proxy != null && !Class.class.isAssignableFrom(proxy.getClass()) && isProxy(proxy.getClass())) {
            Field hField = getField("CGLIB$CALLBACK_0", proxy.getClass());
            if (hField == null) {
                hField = getField("h", proxy.getClass());
            }

            if (hField != null) {
                Object aopProxy = getValue(hField, proxy);
                if (aopProxy != null) {
                    Field advisedField = getField("advised", aopProxy.getClass());
                    if (advisedField != null) {
                        Object advisedSupport = getValue(advisedField, aopProxy);
                        if (advisedSupport != null) {
                            Field targetSourceField = getField("targetSource", advisedSupport.getClass());
                            if (targetSourceField != null) {
                                Object targetSource = getValue(targetSourceField, advisedSupport);
                                if (targetSource != null) {
                                    Field targetField = getField("target", targetSource.getClass());
                                    proxy = getValue(targetField, targetSource);
                                }
                            }
                        }
                    } else {
                        if ("org.apache.ibatis.binding.MapperProxy".equals(aopProxy.getClass().getName())) {
                            Field mapperInterfaceField = getField("mapperInterface", aopProxy.getClass());
                            if (mapperInterfaceField != null) {
                                proxy = getValue(mapperInterfaceField, aopProxy);
                            }
                        }
                    }
                }
            }
        }
        return proxy;
    }

    /**
     * <p>
     * 获取当前对象的 class
     * </p>
     *
     * @param o 传入
     * @return 如果是代理的class，返回父 class，否则返回自身
     */
    public static Class<?> getUserClass(Object o) {
        Class<?> clazz = o.getClass();
        if (Proxy.isProxyClass(clazz)) {
            return Proxy.getInvocationHandler(o).getClass();
        } else {
            while (isProxy(clazz)) {
                clazz = clazz.getSuperclass();
            }
        }
        return clazz;
    }


    /**
     * <p>
     * 根据指定的 class ， 实例化一个对象，根据构造参数来实例化
     * </p>
     * <p>
     * 在 java9 及其之后的版本 Class.newInstance() 方法已被废弃
     * </p>
     *
     * @param clazz 需要实例化的对象
     * @param <T>   类型，由输入类型决定
     * @return 返回新的实例
     */
    public static <T> T newInstance(Class<T> clazz) {
        try {
            if (List.class.isAssignableFrom(clazz) || Collection.class.isAssignableFrom(clazz)) {
                return clazz.cast(new ArrayList<>());
            } else if (Map.class.isAssignableFrom(clazz)) {
                return clazz.cast(new LinkedHashMap<>());
            } else if (clazz.isArray()) {
                return clazz.cast(Array.newInstance(getClassForArray(clazz), 0));
            }
            Constructor<T> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            return constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                 NoSuchMethodException e) {
            throw new IllegalArgumentException(String.format("实例化对象时出现错误,请尝试给 %s 添加无参的构造方法", clazz.getName()), e);
        }
    }


    /**
     * 构建数组类型对象
     * <p>
     * 在 java9 及其之后的版本 Class.newInstance() 方法已被废弃
     * </p>
     *
     * @param clazz 需要实例化的对象
     * @param size  数组长度，必须指定
     * @param <T>   类型，由输入类型决定
     * @return 返回新的实例
     */
    public static <T> T newInstanceArray(Class<T> clazz, int size) {
        if (clazz.isArray()) {
            return clazz.cast(Array.newInstance(
                    getClassForArray(clazz), Math.max(size, 0)));
        }
        return newInstance(clazz);
    }

    /**
     * 获取注解的Class
     *
     * @param className 注解class名
     * @return Annotation.class
     */
    public static Class<? extends Annotation> getAnnotationClass(String className) {
        return getClassForName(className, Annotation.class);
    }

    /**
     * 根据类名返回 Class
     * <p>
     * 请仅在确定类存在的情况下调用该方法
     * </p>
     *
     * @param name 类名称
     * @return 返回转换后的 Class
     */
    public static <T> Class<? extends T> getClassForName(String name, Class<T> subclass) {
        Class<?> clazz = getClassForName(name);
        if (subclass.isAssignableFrom(clazz)) {
            return clazz.asSubclass(subclass);
        }
        return null;
    }

    /**
     * 根据类名返回 Class
     * <p>
     * 请仅在确定类存在的情况下调用该方法
     * </p>
     *
     * @param name 类名称
     * @return 返回转换后的 Class
     */
    public static Class<?> getClassForName(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            try {
                return getClassLoader().loadClass(name);
            } catch (ClassNotFoundException ex) {
                throw new IllegalArgumentException("找不到指定的class！请仅在明确确定会有 class 的时候，调用该方法.  className: " + name, e);
            }
        }
    }

    /**
     * 获取数组元素的Class
     *
     * @param arrayClass 数组Class
     * @return 数组元素的Class
     */
    public static Class<?> getClassForArray(Class<?> arrayClass) {
        if (arrayClass == null) {
            return null;
        }
        return getClassForName(arrayClass.getName().replace("[L", "").replace(";", ""));
    }

    /**
     * 获取类加载器 ClassLoader
     * <p> 线程类加载器 -> 当前类加载器 -> 系统类加载器</p>
     *
     * @return 类加载器
     */
    public static ClassLoader getClassLoader() {
        ClassLoader cl = null;
        try {
            cl = Thread.currentThread().getContextClassLoader();
        } catch (Throwable ex) {
            /* 线程类加载器获取失败使用当前类加载器. */
            cl = ClassUtils.class.getClassLoader();
            if (cl == null) {
                /* getClassLoader() returning null indicates the bootstrap ClassLoader */
                try {
                    cl = ClassLoader.getSystemClassLoader();
                } catch (Throwable e) {
                    /* Cannot access system ClassLoader - oh well, maybe the caller can live with null... */
                    throw new RuntimeException(e);
                }
            }
        }
        return cl;
    }

    /**
     * 获取Type表述的Class
     *
     * @param type 类型
     * @return Class
     */
    public static Class<?> getTypeClass(Type type) {
        Class<?> tClass = null;

        if (type instanceof GenericArrayType) {
            return Array.newInstance(getTypeClass(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else if (type instanceof ParameterizedType) {
            ParameterizedType pt = (ParameterizedType) type;
            tClass = (Class<?>) pt.getRawType();
        } else if (type instanceof TypeVariable) {
            TypeVariable<?> tType = (TypeVariable<?>) type;
            tClass = tType.getGenericDeclaration().getClass();
        } else {
            tClass = (Class<?>) type;
        }
        return tClass;
    }

    /**
     * 获取Type表述的Class 定义的泛型列表
     *
     * @param type 类型
     * @return Class
     */
    public static List<Class<?>> getTypeClassGenerics(Type type) {
        if (type.getTypeName().contains("<")) {
            String genericsClass = type.getTypeName().substring(type.getTypeName().indexOf("<") + 1, type.getTypeName().lastIndexOf(">"));
            return Stream.of(genericsClass.split(",")).map(ClassUtils::getClassForName).collect(Collectors.toList());
        }
        return null;
    }


    /**
     * 获取字段泛型类型
     *
     * @param field 拥有泛型的类属性字段对象
     * @param index 泛型索引
     * @return 泛型类型
     */
    public static Class<?> getGenerics(Field field, int index) {
        Type[] types = getGenerics(field);
        if (types != null && types.length > index) {
            return getTypeClass(types[index]);
        }
        return null;
    }

    /**
     * 获取字段泛型类型列表
     *
     * @param field 拥有泛型的类属性字段对象
     * @return 泛型类型列表
     */
    public static Type[] getGenerics(Field field) {
        if (field == null) {
            return null;
        }
        return getGenerics(field.getGenericType());
    }

    /**
     * 获取对象泛型类型列表
     * 泛型必须在继承关系中指明真实类型，才能在反射中回去到。
     *
     * @param type  泛型类子类的实例对象
     * @param index 要获取的泛型类型在泛型列表中的位置
     * @return 泛型类型列表
     */
    public static Class<?> getGenerics(Type type, int index) {
        try {
            Type[] types = getGenerics(type);
            if (types != null && types.length >= index) {
                return getTypeClass(types[index]);
            }
        } catch (Exception e) {
            log.warning(e.getMessage());
        }
        return null;
    }

    /**
     * 获取对象泛型类型列表
     * 泛型必须在继承关系中指明真实类型，才能在反射中回去到。
     *
     * @param type 要获取泛型类型的类
     * @return 泛型类型列表
     */
    public static Type[] getGenerics(Type type) {
        if (type == null || isProxy(type.getClass())) {
            return null;
        }

        if (type instanceof Class) {
            return getTypeClass(type).getTypeParameters();
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return parameterizedType.getActualTypeArguments();  /* 参数化类型中可能有多个泛型参数 */
        }

        return null;
    }

    /**
     * 获取对象实现接口 的泛型列表
     *
     * @param o          实现接口的实体对象
     * @param interfaces 指定要获取的接口，不指定则获取实体对象实现的所有接口泛型
     * @return Map<String, Type>  key: 接口className  v:对应接口的泛型列表
     **/
    public static Map<String, Type[]> getGenericInterfaces(Object o, Class<?>... interfaces) {
        Set<String> interfacesClassName = null;

        if (interfaces.length > 0) {
            interfacesClassName = new HashSet<>();
            for (Class<?> a : interfaces) {
                interfacesClassName.add(a.getName());
            }
        }

        Type[] interfacesTypes = o.getClass().getGenericInterfaces();

        Map<String, Type[]> result = new LinkedHashMap<>();
        for (Type t : interfacesTypes) {
            String tName = t.getTypeName();
            if (tName.contains("<")) tName = tName.substring(0, tName.indexOf("<"));
            if (interfacesClassName != null && !interfacesClassName.contains(tName)) {
                continue;
            }

            Type[] genericType = ((ParameterizedType) t).getActualTypeArguments();
            result.put(tName, genericType);
        }
        return result;
    }


    /**
     * 获取某个方法，返回值的泛型的具体类型
     *
     * @return 返回值类型
     */
    public static Class<?> getReturnGeneric(Method method) {
        try {
            Type genericReturnType = method.getGenericReturnType();
            Class<?> returnType = method.getReturnType();
            if (genericReturnType instanceof ParameterizedType) {/* 如果包含泛型 */
                ParameterizedType parameterizedType = (ParameterizedType) genericReturnType; /* 获取真实类型 */
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

                if (returnType.newInstance() instanceof Map && actualTypeArguments.length == 2) {
                    return actualTypeArguments[1].getClass();
                } else if (returnType.newInstance() instanceof Collection && actualTypeArguments.length != 0) {
                    return actualTypeArguments[0].getClass();
                }
            }
            return null;
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("返回值具体类型获取异常", e);
        }
    }

    /**
     * 获取指定字段get方法的 LambdaMethodHandle 对象
     *
     * @param fieldName   字段名称
     * @param entityClass 字段所属类
     * @param invokedType 返回值类型
     * @return LambdaMethodHandle 对象
     */
    public static MethodHandle getFieldMethodHandle(String fieldName, Class<?> entityClass, Class<?> invokedType) {
        final int FLAG_SERIALIZABLE = 1;
        final MethodHandles.Lookup lookup = MethodHandles.lookup();

        if (fieldName != null && entityClass != null && invokedType != null) {
            try {
                fieldName = ClassUtils.getterName(fieldName);

                Method method = ClassUtils.getMethod(entityClass, fieldName);
                if (method != null) {
                    MethodType returnMethodType = MethodType.methodType(method.getReturnType(), entityClass);

                    /* 方法名叫做:getFieldName  转换为 invokedType function interface对象 */
                    final CallSite site = LambdaMetafactory.altMetafactory(lookup,
                            "invoke",
                            MethodType.methodType(invokedType),
                            returnMethodType,
                            lookup.findVirtual(entityClass, fieldName, MethodType.methodType(method.getReturnType())),
                            returnMethodType, FLAG_SERIALIZABLE);

                    /* return (SFunction) site.getTarget().invokeExact(); */
                    return site.getTarget();
                }
            } catch (NoSuchMethodException | IllegalAccessException | LambdaConversionException e) {
                throw new RuntimeException("获取" + fieldName + "字段的 LambdaMethodHandle 对象错误", e);
            }
        }
        return null;
    }

    /**
     * 获取堆栈调用链信息
     *
     * @return 调用链信息
     */
    public static StackTraceElement getStackTraceElement() {
        return getStackTraceElement(3);
    }

    /**
     * 获取堆栈调用链信息
     * <p>
     * 注意调用链索引0通常代表的是Thread.getStackTrace()自身的方法，因此从索引1或2开始查找调用者信息。
     * 下标2 表示此工具方法，下标3表示调用者方法。
     *
     * @param index 调用链索引
     * @return 调用链信息 下标不存在则返回最后一个下标值
     */
    public static StackTraceElement getStackTraceElement(int index) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        if (stackTraceElements.length <= index) {
            index = stackTraceElements.length - 1;
        }
        return stackTraceElements[index];
    }

    /**
     * 获取堆栈调用链信息
     * <p>
     * 注意调用链索引0通常代表的是Thread.getStackTrace()自身的方法，因此从索引1或2开始查找调用者信息。
     * 下标3 表示此工具方法，下标4表示调用者方法。
     *
     * @param index 调用链索引
     * @return 调用方法 下标不存在则返回最后一个下标值。类中方法名重载则返回第一个匹配方法
     */
    public static Method getStackTraceMethod(int index) {
        StackTraceElement stackTraceElement = getStackTraceElement(index);
        if (stackTraceElement != null) {
            try {
                Class<?> entityClass = getClassLoader().loadClass(stackTraceElement.getClassName());
                return getMethodAll(entityClass, stackTraceElement.getMethodName(), null);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }
}
