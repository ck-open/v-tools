package com.ck.tools.enums;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 码表
 */
@Data
@Accessors(chain = true)
public class DictionaryData<T> implements Serializable {

    private static final long serialVersionUID = 5999791587974140346L;

    /**
     * 资源代码
     */
    private String code;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 资源值
     */
    private T value;

    /**
     * 字典资源层级
     */
    private String level;

    /**
     * 父级code
     */
    private String parentCode;
    /**
     * 关联子资源列表
     */
    private List<DictionaryData<T>> children;

    public DictionaryData() {
        super();
    }

    public DictionaryData(String code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 添加字典子项
     *
     * @param dictionaryData 子项字典数据
     */
    public DictionaryData<T> addChild(DictionaryData<T> dictionaryData) {
        if (dictionaryData == null) {
            return this;
        }
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(dictionaryData);
        return this;
    }

    /**
     * 构建字典树模型
     *
     * @param dictionaryData 字典数据列表
     * @return
     */
    public static <T> List<DictionaryData<T>> build(List<DictionaryData<T>> dictionaryData) {
        List<DictionaryData<T>> result = new ArrayList<>();
        if (dictionaryData != null) {
            Map<String, DictionaryData<T>> dictionaryDataMap = dictionaryData.stream()
                    .collect(Collectors.toMap(DictionaryData::getCode, Function.identity()));
            result = dictionaryData.stream().peek(d -> {
                if (dictionaryDataMap.containsKey(d.getParentCode())) {
                    dictionaryDataMap.get(d.getParentCode()).addChild(d);
                }
            }).filter(i -> i.getParentCode() == null).collect(Collectors.toList());
        }
        return result;
    }
}
