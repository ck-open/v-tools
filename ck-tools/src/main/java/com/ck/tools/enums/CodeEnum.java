package com.ck.tools.enums;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 所有枚举的父接口
 */
public interface CodeEnum<C, V> {

    /**
     * @return 获取枚举代码
     */
    C getCode();

    /**
     * @return 获取枚举值
     */
    default V getValue() {
        return null;
    }

    /**
     * @return 获取枚举名称
     */
    default String getName() {
        return Objects.isNull(this.getCode()) ? null : this.getCode().toString();
    }

    /**
     * @return 获取枚举信息
     */
    default String getMsg() {
        return Objects.isNull(this.getCode()) ? null : this.getCode().toString();
    }

    /**
     * @return 获取枚举信息
     */
    default String getMessage() {
        return Objects.isNull(this.getCode()) ? null : this.getCode().toString();
    }

    /**
     * @return 获取枚举描述
     */
    default String getDesc() {
        return this.getName();
    }

    /**
     * @return 获取实例枚举对象
     */
    default CodeEnum<C, V> get() {
        return this;
    }

    /**
     * 根据 枚举Code
     *
     * @param code 枚举代码
     * @return 枚举
     */
    static <Code, Val, T extends CodeEnum<Code, Val>> T getByCode(Class<T> enumClass, Code code) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(i -> Objects.equals(i.getCode(), code))
                .findFirst().orElse(null);
    }

    /**
     * 根据 枚举Code
     *
     * @param code 枚举代码
     * @return 枚举
     */
    static <Code, Val, T extends CodeEnum<Code, Val>> List<T> getListByCode(Class<T> enumClass, Code code) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(i -> Objects.equals(i.getCode(), code))
                .collect(Collectors.toList());
    }

    /**
     * 根据 枚举名称
     *
     * @param name 响应码值
     * @return 枚举
     */
    static <Code, Val, T extends CodeEnum<Code, Val>> T getByName(Class<T> enumClass, String name) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(i -> Objects.equals(i.getName(), name))
                .findFirst().orElse(null);
    }

    /**
     * 根据 枚举名称
     *
     * @param name 响应码值
     * @return 枚举
     */
    static <Code, Val, T extends CodeEnum<Code, Val>> List<T> getListByName(Class<T> enumClass, String name) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(i -> Objects.equals(i.getName(), name))
                .collect(Collectors.toList());
    }

    /**
     * 根据 枚举值
     *
     * @param value 枚举值
     * @return 枚举
     */
    static <Code, Val, T extends CodeEnum<Code, Val>> T getByValue(Class<T> enumClass, Val value) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(i -> Objects.equals(i.getValue(), value))
                .findFirst().orElse(null);
    }

    /**
     * 根据 枚举值
     *
     * @param value 枚举值
     * @return 枚举
     */
    static <Code, Val, T extends CodeEnum<Code, Val>> List<T> getListByValue(Class<T> enumClass, Val value) {
        return Stream.of(enumClass.getEnumConstants())
                .filter(i -> Objects.equals(i.getValue(), value))
                .collect(Collectors.toList());
    }
}
