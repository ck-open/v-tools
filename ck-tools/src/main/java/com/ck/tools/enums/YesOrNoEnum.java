package com.ck.tools.enums;

import lombok.Getter;

@Getter
public enum YesOrNoEnum implements CodeEnum<String,String> {

    YES("Y", 1,"是"),
    NO("N", 2,"否");

    private final String code;
    private final Integer value;
    private final String name;

    YesOrNoEnum(String code, Integer value, String name) {
        this.code = code;
        this.name = name;
        this.value = value;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getValue() {
        return this.code;
    }
}
