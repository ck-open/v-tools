package com.ck.tools.encrypt_decrypt;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * RSA 非对称加解密工具
 *
 * @author cyk
 * @since 2020-01-01
 **/
public class RSAUtil {
    /**
     * 签名加密算法
     * <p>将正文通过sha1哈希加密后,将密文再次通过生成的RSA密钥加密,生成数字签名</p>
     */
    public static final String Algorithm_SHA1WithRSA = "SHA1WithRSA";
    public static final String Algorithm_SSHA256WithRSA = "SHA256WithRSA";


    /**
     * 获取密钥对
     * 密钥格式：PKCS8
     *
     * @return 密钥对
     */
    public static KeyPair getKeyPair() {
        return getKeyPair(512);
    }

    /**
     * 获取密钥对
     * 密钥格式：PKCS8
     *
     * @param keySize 长度。如：512
     * @return 密钥对
     */
    public static KeyPair getKeyPair(int keySize) {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(keySize);
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取私钥
     * 密钥格式：PKCS8
     *
     * @param keyPair 公私钥对象
     * @return 私钥
     */
    public static String getPrivateKey(KeyPair keyPair) {
        return getPrivateKey(keyPair, StandardCharsets.UTF_8);
    }

    /**
     * 获取私钥
     * 密钥格式：PKCS8
     *
     * @param keyPair 公私钥对象
     * @param charset 编码格式
     * @return 私钥
     * @see StandardCharsets
     */
    public static String getPrivateKey(KeyPair keyPair, Charset charset) {
        return new String(Base64.getEncoder().encode(keyPair.getPrivate().getEncoded()), charset);
    }

    /**
     * 获取公钥
     * 密钥格式：PKCS8
     *
     * @param keyPair 公私钥对象
     * @return 公钥
     */
    public static String getPublicKey(KeyPair keyPair) {
        return getPublicKey(keyPair, StandardCharsets.UTF_8);
    }

    /**
     * 获取公钥
     * 密钥格式：PKCS8
     *
     * @param keyPair 公私钥对象
     * @param charset 编码格式
     * @return 公钥
     * @see StandardCharsets
     */
    public static String getPublicKey(KeyPair keyPair, Charset charset) {
        return new String(Base64.getEncoder().encode(keyPair.getPublic().getEncoded()), charset);
    }

    /**
     * 获取私钥
     * 密钥格式：PKCS8
     *
     * @param privateKey 私钥字符串
     * @return 私钥
     */
    public static PrivateKey getPrivateKey(String privateKey) {
        return getPrivateKey(privateKey, StandardCharsets.UTF_8);
    }

    /**
     * 获取私钥
     * 密钥格式：PKCS8
     *
     * @param privateKey 私钥字符串
     * @param charset    编码格式
     * @return 私钥
     * @see StandardCharsets
     */
    public static PrivateKey getPrivateKey(String privateKey, Charset charset) {
        try {
            if (privateKey == null || privateKey.trim().isEmpty()) {
                return null;
            }
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] decodedKey = Base64.getDecoder().decode(privateKey.getBytes(charset));
            assert decodedKey != null;
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decodedKey);
            return keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException("私钥值错误", e);
        }
    }

    /**
     * 获取公钥
     * 密钥格式：PKCS8
     *
     * @param publicKey 公钥字符串
     * @return 公钥
     */
    public static PublicKey getPublicKey(String publicKey) {
        return getPublicKey(publicKey, StandardCharsets.UTF_8);
    }

    /**
     * 获取公钥
     * 密钥格式：PKCS8
     *
     * @param publicKey 公钥字符串
     * @return 公钥
     * @see StandardCharsets
     */
    public static PublicKey getPublicKey(String publicKey, Charset charset) {
        try {
            if (publicKey == null || publicKey.trim().isEmpty()) {
                return null;
            }
            byte[] decodedKey = Base64.getDecoder().decode(publicKey.getBytes(charset));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            assert decodedKey != null;
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decodedKey);
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException("公钥值错误", e);
        }
    }

    /**
     * RSA加密
     * 密钥格式：PKCS8
     *
     * @param data 待加密数据
     * @param key  公私钥
     * @return 密文
     */
    public static String encrypt(String data, Key key) {
        return encrypt(data, key, StandardCharsets.UTF_8);
    }

    /**
     * RSA加密
     * 密钥格式：PKCS8
     *
     * @param data 待加密数据
     * @param key  公私钥
     * @return 密文
     * @see StandardCharsets
     */
    public static String encrypt(String data, Key key, Charset charset) {
        /* RSA非对称加密解密内容长度是有限制的，加密长度不超过117Byte，解密长度不超过128Byte字节数，超出最大字节数需要分组加、解密*/
        int MAX_ENCRYPT_BLOCK = 117;
        try {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            int inputLen = data.getBytes(charset).length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offset = 0;
            byte[] cache;
            int i = 0;
            /* 对数据分段加密*/
            while (inputLen - offset > 0) {
                if (inputLen - offset > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data.getBytes(), offset, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data.getBytes(), offset, inputLen - offset);
                }
                out.write(cache, 0, cache.length);
                i++;
                offset = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            /* 获取加密内容使用base64进行编码,并以UTF-8为标准转化成字符串
             加密后的字符串*/
            return new String(Base64.getEncoder().encode(encryptedData), charset);
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                 BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException("RSA加密 异常", e);
        }
    }

    /**
     * RSA解密
     * 密钥格式：PKCS8
     *
     * @param data 待解密数据
     * @param key  公私钥
     * @return 明文
     */
    public static String decrypt(String data, Key key) {
        return decrypt(data, key, StandardCharsets.UTF_8);
    }

    /**
     * RSA解密
     * 密钥格式：PKCS8
     *
     * @param data 待解密数据
     * @param key  公私钥
     * @return 明文
     * @see StandardCharsets
     */
    public static String decrypt(String data, Key key, Charset charset) {
        /* RSA非对称加密解密内容长度是有限制的，加密长度不超过117Byte，解密长度不超过128Byte字节数，超出最大字节数需要分组加、解密*/
        int MAX_DECRYPT_BLOCK = 128;

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] dataBytes = Base64Util.decodeByte(data);
            int inputLen = dataBytes.length;

            int offset = 0;
            byte[] cache;
            int i = 0;
            /* 对数据分段解密*/
            while (inputLen - offset > 0) {
                if (inputLen - offset > MAX_DECRYPT_BLOCK) {
                    cache = cipher.doFinal(dataBytes, offset, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(dataBytes, offset, inputLen - offset);
                }
                out.write(cache, 0, cache.length);
                i++;
                offset = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            /* 解密后的内容*/
            return new String(decryptedData, charset);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException |
                 IllegalBlockSizeException | IOException | IllegalArgumentException e) {
            throw new RuntimeException("RSA解密 异常", e);
        }
    }

    /**
     * 签名
     * 密钥格式：PKCS8
     *
     * @param data       待签名数据
     * @param privateKey 私钥
     * @return 签名
     */
    public static String sign(String data, PrivateKey privateKey) {
        return sign(data, privateKey, Algorithm_SHA1WithRSA);
    }

    /**
     * 签名
     * 密钥格式：PKCS8
     *
     * @param data       待签名数据
     * @param privateKey 私钥
     * @param algorithm  加密算法名称
     * @return 签名
     */
    public static String sign(String data, PrivateKey privateKey, String algorithm) {
        return sign(data, privateKey, algorithm, StandardCharsets.UTF_8);
    }

    /**
     * 签名
     * 密钥格式：PKCS8
     *
     * @param data       待签名数据
     * @param privateKey 私钥
     * @param algorithm  加密算法名称
     * @return 签名
     * @see StandardCharsets
     */
    public static String sign(String data, PrivateKey privateKey, String algorithm, Charset charset) {
        try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initSign(privateKey);
            signature.update(data.getBytes(charset));
            return new String(Base64.getEncoder().encode(signature.sign()), charset);
        } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            throw new RuntimeException("签名异常", e);
        }
    }

    /**
     * 验签
     * 密钥格式：PKCS8
     *
     * @param srcData   原始字符串
     * @param publicKey 公钥
     * @param sign      签名
     * @return 是否验签通过 true 通过 false 不通过
     */
    public static boolean verify(String srcData, PublicKey publicKey, String sign) {
        return verify(srcData, publicKey, sign, Algorithm_SHA1WithRSA);
    }

    /**
     * 验签
     * 密钥格式：PKCS8
     *
     * @param srcData   原始字符串
     * @param publicKey 公钥
     * @param sign      签名
     * @param algorithm 加密算法名称
     * @return 是否验签通过 true 通过 false 不通过
     */
    public static boolean verify(String srcData, PublicKey publicKey, String sign, String algorithm) {
        return verify(srcData, publicKey, sign, algorithm, StandardCharsets.UTF_8);
    }

    /**
     * 验签
     * 密钥格式：PKCS8
     *
     * @param srcData   原始字符串
     * @param publicKey 公钥
     * @param sign      签名
     * @param algorithm 加密算法名称
     * @return 是否验签通过 true 通过 false 不通过
     * @see StandardCharsets
     */
    public static boolean verify(String srcData, PublicKey publicKey, String sign, String algorithm, Charset charset) {
        try {
            Signature signature = Signature.getInstance(algorithm);
            signature.initVerify(publicKey);
            signature.update(srcData.getBytes(charset));
            return signature.verify(Base64.getDecoder().decode(sign.getBytes(charset)));
        } catch (NoSuchAlgorithmException | SignatureException | InvalidKeyException e) {
            throw new RuntimeException("验签异常", e);
        }
    }

    public static void main(String[] args) {
        // 生成密钥对
        KeyPair keyPair = RSAUtil.getKeyPair(512);
        String privateKey = RSAUtil.getPrivateKey(keyPair);
        String publicKey = RSAUtil.getPublicKey(keyPair);
        System.out.println("私钥:" + privateKey);
        System.out.println("公钥:" + publicKey);
        // RSA加密
        String data = "待加密的文字内容";

        System.out.println("\r---------------------------RSA 私钥签名、加密 公钥解密、验签展示------------------------------");
        // step1 签名
        String sign = RSAUtil.sign(data, RSAUtil.getPrivateKey(privateKey));
        System.out.println("签名:" + sign);
        // step2 加密
        String encryptData = RSAUtil.encrypt(data, RSAUtil.getPrivateKey(privateKey));
        System.out.println("加密后内容:" + encryptData);
        // step3 解密
        String decryptData = RSAUtil.decrypt(encryptData, RSAUtil.getPublicKey(publicKey));
        System.out.println("解密后内容:" + decryptData);
        // step4 验签
        boolean result = RSAUtil.verify(decryptData, RSAUtil.getPublicKey(publicKey), sign);
        System.out.println("验签结果:" + result);

        System.out.println("\r---------------------------RSA 私钥签名、加密 公钥解密、验签展示 SHA256WithRSA------------------------------");

    }
}
