package com.ck.tools.encrypt_decrypt;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;

/**
 * AES 非对称加解密工具
 *
 * @author cyk
 * @since 2020-01-01
 **/
public class AESUtil {
    public static final String Algorithm_AES = "AES";
    public static final String Algorithm_DES = "DES";
    public static final String Algorithm_AES_128 = "AES-128";
    public static final String Algorithm_SHA1PRNG = "SHA1PRNG";
    public static final String Algorithm_CBC = "AES/CBC/PKCS5Padding";
    public static final String Algorithm_ECB = "DES/ECB/PKCS5Padding";

    /**
     * 创建指定算法的密钥
     *
     * @param algorithmKeyGenerator 密钥加密算法，例如： AES、DES、HMAC等
     * @param keySize               密钥长度，（AES支持128, 192, 256位）
     * @return KeyGenerator
     */
    public static String getKey(String algorithmKeyGenerator, int keySize) {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(algorithmKeyGenerator);

            // 初始化此密钥生成器，使其具有确定的密钥大小
            // AES支持128、192、256位密钥
            keyGen.init(keySize);

            SecretKey secretKey = keyGen.generateKey();

            byte[] encoded = secretKey.getEncoded();
            return Base64Util.bytesToHex(encoded);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(algorithmKeyGenerator + "-" + keySize + "获取 KeyGenerator 密钥生成器失败", e);
        }
    }

    /**
     * 创建指定算法的密钥生成器
     *
     * @param algorithmKeyGenerator 密钥加密算法，例如： AES、DES、HMAC等
     * @param algorithmSecureRandom 随机值算法， SHA1PRNG: 这是一个基于SHA-1散列函数的伪随机数生成器，原生支持的算法之一。
     * @param keySize               密钥长度，（AES支持128, 192, 256位）
     * @param password              密码、密钥
     * @param charset               密码、密钥 字符集 默认：StandardCharsets.UTF_8
     * @return KeyGenerator
     * @see StandardCharsets
     */
    public static KeyGenerator getKeyGenerator(String algorithmKeyGenerator, String algorithmSecureRandom, int keySize, String password, Charset charset) {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithmKeyGenerator);
            SecureRandom secureRandom = SecureRandom.getInstance(algorithmSecureRandom);
            secureRandom.setSeed(password.getBytes(charset == null ? StandardCharsets.UTF_8 : charset));
            keyGenerator.init(keySize, secureRandom);
            return keyGenerator;
        } catch (Exception e) {
            throw new RuntimeException(algorithmKeyGenerator + "-" + keySize + "获取 KeyGenerator 密钥生成器失败", e);
        }
    }

    /**
     * 获取密码器
     *
     * @param algorithmKeyGenerator 密钥加密算法，例如： AES、DES、HMAC等
     * @param algorithmSecureRandom 随机值算法， SHA1PRNG: 这是一个基于SHA-1散列函数的伪随机数生成器，原生支持的算法之一。
     * @param algorithmCipher       密码器算法， SHA1PRNG: 这是一个基于SHA-1散列函数的伪随机数生成器，原生支持的算法之一。
     * @param keySize               密钥长度，（AES支持128, 192, 256位）
     * @param opMode                操作方式，Cipher.ENCRYPT_MODE、Cipher.DECRYPT_MODE
     * @param password              密码、密钥
     * @param ivParameter           加密向量IV   使用CBC模式，需要一个向量iv，可增加加密算法的强度。null则为不设置。
     * @param charset               密码、密钥 字符集 默认：StandardCharsets.UTF_8
     * @return Cipher 密码器
     * @see StandardCharsets
     */
    public static Cipher getCipher(String algorithmKeyGenerator, String algorithmSecureRandom, String algorithmCipher, int keySize, int opMode, String password, String ivParameter, Charset charset) {
        try {
            SecretKeySpec keySpec = null;

            if (algorithmSecureRandom == null) {
                keySpec = new SecretKeySpec(password.getBytes(charset), algorithmKeyGenerator);
            } else {
                KeyGenerator keyGenerator = getKeyGenerator(algorithmKeyGenerator, algorithmSecureRandom, keySize, password, charset);
                SecretKey secretKey = keyGenerator.generateKey();
                byte[] enCodeFormat = secretKey.getEncoded();
                keySpec = new SecretKeySpec(enCodeFormat, algorithmKeyGenerator);
            }

            /* 创建密码器 */
            Cipher cipher = Cipher.getInstance(algorithmCipher);

            /* 使用CBC模式，需要一个向量iv，可增加加密算法的强度 */
            if (ivParameter != null && Algorithm_CBC.equals(algorithmCipher)) {
                IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
                cipher.init(opMode, keySpec, iv);
            } else {
                cipher.init(opMode, keySpec);
            }

            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
                 InvalidAlgorithmParameterException e) {
            throw new RuntimeException(algorithmKeyGenerator + "-" + keySize + " 获取 Cipher 加密器失败", e);
        }
    }

    /**
     * AES 加密
     *
     * @param content  文本
     * @param password 密码、密钥
     * @param charset  字符集，默认 StandardCharsets.UTF_8
     * @return 密文
     */
    public static String encrypt(String content, String password, Charset charset) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(charset), Algorithm_AES);
            Cipher cipher = Cipher.getInstance(Algorithm_AES);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);

            byte[] encryptedBytes = cipher.doFinal(content.getBytes(charset));

            return Base64Util.encode(encryptedBytes);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException |
                 InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * AES 解密
     *
     * @param content  文本
     * @param password 密码、密钥
     * @param charset  字符集，默认 StandardCharsets.UTF_8
     * @return 明文
     */
    public static String decrypt(String content, String password, Charset charset) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(charset), Algorithm_AES);
            Cipher cipher = Cipher.getInstance(Algorithm_AES);
            cipher.init(Cipher.DECRYPT_MODE, keySpec);

            byte[] decodedBytes = Base64Util.decodeByte(content, charset);
            byte[] decryptedBytes = cipher.doFinal(decodedBytes);
            return new String(decryptedBytes, charset);
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException |
                 InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * AES-128-SHA1PRNG 加密
     *
     * @param content  文本
     * @param password 密码、密钥
     * @param charset  字符集，默认 StandardCharsets.UTF_8
     * @return 密文
     * @see StandardCharsets
     */
    public static String encryptSHA1PRNG_128_Base64(String content, String password, Charset charset) {
        byte[] result = encryptSHA1PRNG_128(content, password, charset);
//        return new BASE64Encoder().encode(result);
        return Base64Util.encode(result);
    }

    /**
     * AES-128-SHA1PRNG 加密
     *
     * @param content  文本
     * @param password 密码、密钥
     * @param charset  字符集，默认 StandardCharsets.UTF_8
     * @return 密文
     * @see StandardCharsets
     */
    public static byte[] encryptSHA1PRNG_128(String content, String password, Charset charset) {
        try {
            if (charset == null) {
                charset = StandardCharsets.UTF_8;
            }

            /* 创建密码器 */
            Cipher cipher = getCipher(Algorithm_AES, Algorithm_SHA1PRNG, Algorithm_AES, 128, Cipher.ENCRYPT_MODE, password, null, charset);
            /* 加密 */
            byte[] byteContent = content.getBytes(charset);
            return cipher.doFinal(byteContent);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("AES-128-SHA1PRNG 加密失败", e);
        }
    }

    /**
     * AES-128-SHA1PRNG 解密
     *
     * @param content  密文
     * @param password 密码、密钥
     * @param charset  字符集，默认 StandardCharsets.UTF_8
     * @return 明文
     * @see StandardCharsets
     */
    public static String decryptSHA1PRNG_128_Base64(String content, String password, Charset charset) {
        /* BASE64 转换 */
        byte[] contentByte = Base64Util.decodeByte(content, charset);

        return decryptSHA1PRNG_128(contentByte, password, charset);
    }

    /**
     * AES-128-SHA1PRNG 解密
     *
     * @param content  密文
     * @param password 密码、密钥
     * @param charset  字符集，默认 StandardCharsets.UTF_8
     * @return 明文
     * @see StandardCharsets
     */
    public static String decryptSHA1PRNG_128(byte[] content, String password, Charset charset) {
        try {
            if (charset == null) {
                charset = StandardCharsets.UTF_8;
            }
            /* 创建密码器 */
            Cipher cipher = getCipher(Algorithm_AES, Algorithm_SHA1PRNG, Algorithm_AES, 128, Cipher.DECRYPT_MODE, password, null, charset);

            /* 解密 */
            byte[] result = cipher.doFinal(content);

            return new String(result, charset);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("AES-128-SHA1PRNG 解密失败", e);
        }
    }


    /**
     * AES-128-CBC 加密
     *
     * @param content     加密字符串
     * @param password    密钥
     * @param ivParameter 加密向量IV
     * @param charset     加密字符集 默认 StandardCharsets.UTF_8
     * @return 密文
     * @see StandardCharsets
     */
    public static String encryptCBC_128_Base64(String content, String password, String ivParameter, Charset charset) {
        byte[] encrypted = encryptCBC_128(content, password, ivParameter, charset);
        /* 此处使用BASE64做转码。 */
        return Base64Util.encode(encrypted);
    }


    /**
     * AES-128-CBC 加密
     *
     * @param content     加密字符串
     * @param password    密钥
     * @param ivParameter 加密向量IV
     * @param charset     加密字符集 默认 StandardCharsets.UTF_8
     * @return 密文
     * @see StandardCharsets
     */
    public static byte[] encryptCBC_128(String content, String password, String ivParameter, Charset charset) {
        try {
            if (charset == null) {
                charset = StandardCharsets.UTF_8;
            }
            /* 创建密码器 */
            Cipher cipher = getCipher(Algorithm_AES, Algorithm_SHA1PRNG, Algorithm_CBC, 128, Cipher.ENCRYPT_MODE, password, ivParameter, charset);

            return cipher.doFinal(content.getBytes(charset));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException("AES-128-CBC 加密失败", e);
        }
    }

    /**
     * AES-128-CBC 解密
     *
     * @param content     加密字符串
     * @param password    密钥
     * @param ivParameter 加密向量IV
     * @param charset     加密字符集 默认 StandardCharsets.UTF_8
     * @return 明文
     * @see StandardCharsets
     */
    public static String decryptCBC_128_Base64(String content, String password, String ivParameter, Charset charset) {
        /* 先用base64解密 */
        byte[] encrypted = Base64Util.decodeByte(content, charset);
        return decryptCBC_128(encrypted, password, ivParameter, charset);
    }

    /**
     * AES-128-CBC 解密
     *
     * @param content     加密字符串
     * @param password    密钥
     * @param ivParameter 加密向量IV
     * @param charset     加密字符集 默认 StandardCharsets.UTF_8
     * @return 明文
     * @see StandardCharsets
     */
    public static String decryptCBC_128(byte[] content, String password, String ivParameter, Charset charset) {
        try {
            if (charset == null) {
                charset = StandardCharsets.UTF_8;
            }

            /* 创建密码器 */
            Cipher cipher = getCipher(Algorithm_AES, Algorithm_SHA1PRNG, Algorithm_CBC, 128, Cipher.DECRYPT_MODE, password, ivParameter, charset);

            byte[] original = cipher.doFinal(content);
            return new String(original, charset);
        } catch (Exception e) {
            throw new RuntimeException("AES-128-CBC 解密失败", e);
        }
    }


    /**
     * 加密DES(可逆)
     *
     * @param content  明文
     * @param password 密钥
     * @param charset  加密字符集 默认 StandardCharsets.UTF_8
     * @return 密文
     * @see StandardCharsets
     */
    public static String encryptDES(String content, String password, Charset charset) {
        return encryptDES_Base64(content, password, charset, Algorithm_DES);
    }

    /**
     * 解密DES
     *
     * @param content  密文
     * @param password 密钥
     * @param charset  加密字符集 默认 StandardCharsets.UTF_8
     * @return 明文
     * @see StandardCharsets
     */
    public static String decryptDES(String content, String password, Charset charset) {
        return decryptDES_Base64(content, password, charset, Algorithm_DES);
    }

    /**
     * 加密DES(可逆)
     *
     * @param content  明文
     * @param password 密钥
     * @param charset  加密字符集 默认 StandardCharsets.UTF_8
     * @return 密文
     * @see StandardCharsets
     */
    public static String encryptDES_ECB(String content, String password, Charset charset) {
        return encryptDES_Base64(content, password, charset, Algorithm_ECB);
    }

    /**
     * 解密DES
     *
     * @param content  密文
     * @param password 密钥
     * @param charset  加密字符集 默认 StandardCharsets.UTF_8
     * @return 明文
     * @see StandardCharsets
     */
    public static String decryptDES_ECB(String content, String password, Charset charset) {
        return decryptDES_Base64(content, password, charset, Algorithm_ECB);
    }

    /**
     * 解密DES
     *
     * @param content   密文
     * @param password  密钥
     * @param charset   加密字符集 默认 StandardCharsets.UTF_8
     * @param algorithm 加密算法
     * @return 明文
     * @see StandardCharsets
     */
    private static String decryptDES_Base64(String content, String password, Charset charset, String algorithm) {
        byte[] cipherTextBytes = Base64Util.decodeByte(content);
        return decryptDES(cipherTextBytes, password, charset, algorithm);
    }

    /**
     * 解密DES
     *
     * @param content   密文
     * @param password  密钥
     * @param charset   加密字符集 默认 StandardCharsets.UTF_8
     * @param algorithm 加密算法
     * @return 明文
     * @see StandardCharsets
     */
    private static String decryptDES(byte[] content, String password, Charset charset, String algorithm) {
        try {
            /* 创建密码器 */
            Cipher cipher = getCipher(Algorithm_DES, Algorithm_SHA1PRNG, algorithm, 56, Cipher.DECRYPT_MODE, password, null, StandardCharsets.UTF_8);

            byte[] cipherBytes = cipher.doFinal(content);

            return new String(cipherBytes, charset);
        } catch (Exception e) {
            throw new RuntimeException("DES 解密失败", e);
        }
    }

    /**
     * 加密DES(可逆)
     *
     * @param content   明文
     * @param password  密钥
     * @param charset   加密字符集 默认 StandardCharsets.UTF_8
     * @param algorithm 加密算法
     * @return 密文
     * @see StandardCharsets
     */
    private static String encryptDES_Base64(String content, String password, Charset charset, String algorithm) {
        byte[] cipherBytes = encryptDES(content, password, charset, algorithm);
        return Base64Util.encode(cipherBytes);
    }

    /**
     * 加密DES(可逆)
     *
     * @param content   明文
     * @param password  密钥
     * @param charset   加密字符集 默认 StandardCharsets.UTF_8
     * @param algorithm 加密算法
     * @return 密文
     * @see StandardCharsets
     */
    private static byte[] encryptDES(String content, String password, Charset charset, String algorithm) {
        try {
            /* 创建密码器 */
            Cipher cipher = getCipher(Algorithm_DES, Algorithm_SHA1PRNG, algorithm, 56, Cipher.ENCRYPT_MODE, password, null, StandardCharsets.UTF_8);

            return cipher.doFinal(content.getBytes(charset));
        } catch (Exception e) {
            throw new RuntimeException("DES 加密失败", e);
        }
    }

    /**
     * 签名
     *
     * @param md5Key  MD5 key 值
     * @param content 明文报文数据
     * @return 签名结果
     */
    public static String sign_MD5(String md5Key, String content) {
        /*
            sign是MD5签名，签名策略：
                1. 对源明文数据字符串A进行Base64，得到字符串B。
                2. 在字符串B后面拼上MD5-KEY，得到字符串C。
                3. 对字符串C进行MD5，得到的字符串就是sign的值。
         */
        try {
            content = Base64Util.encode(content.getBytes(StandardCharsets.UTF_8));
            content += md5Key;

            byte[] secretBytes = MessageDigest.getInstance("md5").digest(content.getBytes(StandardCharsets.UTF_8));
            content = new BigInteger(1, secretBytes).toString(16);
            return content;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5签名失败:" + e.getMessage(), e);
        }
    }


    public static void main(String[] args) throws Exception {
        String data = "待加密的文字内容";
        String password = "1245678910111213";
        String ivParameter = "7891098546213576";  // 向量需要16个字符及以上
        System.out.println("\r---------------------------AES-128 密钥生成展示------------------------------");
        password = getKey(Algorithm_AES, 128);
//        String ivParameter = getIvParameter();  // 向量需要16个字符及以上
        System.out.println("Generated AES Key: " + password);
        System.out.println("Generated AES ivParameter: " + ivParameter);
        System.out.println("\r---------------------------AES-128 加密、解密展示------------------------------");
        // step1 加密
        String encryptData = encrypt(data, password, StandardCharsets.UTF_8);
        System.out.println("加密后内容:" + encryptData);
        // step2 解密
        String decryptData = decrypt(encryptData, password, StandardCharsets.UTF_8);
        System.out.println("解密后内容:" + decryptData);

        System.out.println("\r---------------------------AES SHA1PRNG_128 加密、解密展示------------------------------");
        // step1 加密
        encryptData = encryptSHA1PRNG_128_Base64(data, password, StandardCharsets.UTF_8);
        System.out.println("加密后内容:" + encryptData);
        // step2 解密
        decryptData = decryptSHA1PRNG_128_Base64(encryptData, password, StandardCharsets.UTF_8);
        System.out.println("解密后内容:" + decryptData);

        System.out.println("\r---------------------------AES AES/CBC/PKCS5Padding 加密、解密展示------------------------------");
        // step1 加密
        encryptData = encryptCBC_128_Base64(data, password, ivParameter, StandardCharsets.UTF_8);
        System.out.println("加密后内容:" + encryptData);
        // step2 解密
        decryptData = decryptCBC_128_Base64(encryptData, password, ivParameter, StandardCharsets.UTF_8);
        System.out.println("解密后内容:" + decryptData);

        System.out.println("\r---------------------------DES SHA1PRNG_128 加密、解密展示------------------------------");
        // step1 加密
        encryptData = encryptDES(data, password, StandardCharsets.UTF_8);
        System.out.println("加密后内容:" + encryptData);
        // step2 解密
        decryptData = decryptDES(encryptData, password, StandardCharsets.UTF_8);
        System.out.println("解密后内容:" + decryptData);

        System.out.println("\r---------------------------DES DES/ECB/PKCS5Padding 加密、解密展示------------------------------");
        // step1 加密
        encryptData = encryptDES_ECB(data, password, StandardCharsets.UTF_8);
        System.out.println("加密后内容:" + encryptData);
        // step2 解密
        decryptData = decryptDES_ECB(encryptData, password, StandardCharsets.UTF_8);
        System.out.println("解密后内容:" + decryptData);


    }
}
