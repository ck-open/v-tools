package com.ck.tools.encrypt_decrypt;

import java.nio.charset.StandardCharsets;

/**
 * 加解密工具测试
 *
 * @author cyk
 * @since 2020-01-01
 **/
public class Test {
    public static void main(String[] args) {
//        testKeyPair();
        testAES();
        testDES();
//        testRSA();
//        System.out.println(Base64Util.getUUID());
//
//        System.out.println(Base64Util.urlEncode("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOJTAsGoAsNGJdzaWm835mtpcY2YxGr4NPjnhQmfUrPdT+Zgi6jmm+olYuygNeB8cOSALmTzgXlef+6SdwfRJVEMYVAO7hqF0Ood9zTOc+kolgnWJtqX54CoodfABt0SNS/bsr6hCAWu17RGnbgBaK+ZaJD3NVqXVXE8E30cYHiQIDAQAB","utf-8"));


        jdAES();
    }

    private static void jdAES() {
        String md5Key = "123456";
        String aesKey = "ABCDEF";
        String cSrc = "bZacGBTodYGZLdDKDScz9Q==";

        String enString = AESUtil.decryptSHA1PRNG_128_Base64(cSrc, aesKey, StandardCharsets.UTF_8);


        enString = Base64Util.decode(enString);
        System.out.println("解密后的字串是：" + enString);
    }





    private static void testDES() {
        String sKey = "1234567890123456";

        // 需要加密的字串
        String cSrc = "需要加密的字串 DES/ECB/PKCS5Padding";
        System.out.println("加密前的字串是：" + cSrc);
        // 加密
        String enString = AESUtil.encryptDES_ECB(cSrc, sKey,StandardCharsets.UTF_8);
        System.out.println("加密后的字串是：" + enString);
        // 解密
        String DeString = AESUtil.decryptDES_ECB(enString, sKey,StandardCharsets.UTF_8);
        System.out.println("解密后的字串是：" + DeString);

        // 需要加密的字串
         cSrc = "需要加密的字串 DES";
        System.out.println("加密前的字串是：" + cSrc);
        // 加密
         enString = AESUtil.encryptDES_ECB(cSrc, sKey,StandardCharsets.UTF_8);
        System.out.println("加密后的字串是：" + enString);
        // 解密
         DeString = AESUtil.decryptDES_ECB(enString, sKey,StandardCharsets.UTF_8);
        System.out.println("解密后的字串是：" + DeString);
    }

    private static void testAES() {
        String sKey = "1234567890123456";
        String ivParameter = "1234567890123456";

//        sKey = "1jdzWuniG6UMtoa3";
//        ivParameter="Gf32xvvLUEVfWeCu";

        // 需要加密的字串
        String cSrc = "需要加密的字串 AES/CBC/PKCS5Padding";
        System.out.println("加密前的字串是：" + cSrc);
        // 加密
        String enString = AESUtil.encryptCBC_128_Base64(cSrc, sKey, ivParameter,StandardCharsets.UTF_8);
        System.out.println("加密后的字串是：" + enString);
        // 解密
//        enString = "";
        String DeString = AESUtil.decryptCBC_128_Base64(enString, sKey, ivParameter,StandardCharsets.UTF_8);
        System.out.println("解密后的字串是：" + DeString);
    }


    public static void testRSA() {
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDEqcU0BMmw1g63uI40QKDoSf4l" +
                "4hFMKf1TOSNknHs9zEzKcAZDvm2zZWRk20XubqBGZxtlhMs7ez5w1JrAX42IjxOq" +
                "XdWx86NfVFCLGniJyA7FbMQ7tge7Q4/oFg+ZodcTpuM5eecJvZwyxYmYf1HzOz4v" +
                "VUhV2S/6XgKmFffwGQIDAQAB";

        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMSpxTQEybDWDre4" +
                "jjRAoOhJ/iXiEUwp/VM5I2Scez3MTMpwBkO+bbNlZGTbRe5uoEZnG2WEyzt7PnDU" +
                "msBfjYiPE6pd1bHzo19UUIsaeInIDsVsxDu2B7tDj+gWD5mh1xOm4zl55wm9nDLF" +
                "iZh/UfM7Pi9VSFXZL/peAqYV9/AZAgMBAAECgYA3ech2Nw+KAagfLUF3/e8l95Fo" +
                "sE++021xZ4HTZMzjDtlulZE+1N3vKB/cz3n3m1883OL+jSojOk3P2DlbINk0eI0e" +
                "0wNuB0RJy5nhVfaB78DAllDva6YP5SDtGZgCJvB4UcvSVdjMK6BiJyPIPv950YO5" +
                "+tb7w8Iyu01KL97cgQJBAO5sT2FA0ibHGdIeLC1KP+a4AJlae1VBn/Fzo9CeidNw" +
                "fVlEWG9eGQH+7UBcDoiKut7my8fc/73iqBSbtOnWnbECQQDTKVZcFa8IhnUQCrnh" +
                "JkImUWe2vcvHw/BgdX9L1nUIXzPpnI0EyU26LizyiHTrMQ9GbU1/zsDnFVe/5T0t" +
                "8YrpAkBi9txALwrMA8rI97M4nWBG4UuGXBNjgRjlvS1bE+N7u26cVg0rNPDGbk2G" +
                "cG+r54HHsh8V97rD08gWvcbrZSURAkAt4a2CFLQThiOpeKsq4MU+zjFa46KDpW91" +
                "hhJCyDZlu0nsWLfLcnIRIjdh+4RsFhzbkKRmQOZpT5nheE3yzakBAkEAhtkrBpTQ" +
                "jM2PBVTuUGxqG6R4kK67gNw2p49rxz+IHStQaCICPFiWPd48Ir4JALsT0xlCjDdV" +
                "iOLkic6r8yme3A==";


        String data = RSAUtil.encrypt("测试使用", RSAUtil.getPublicKey(publicKey));
        System.out.println("testRSA 加密后内容:" + data);
        data = RSAUtil.decrypt(data, RSAUtil.getPrivateKey(privateKey));
        System.out.println("testRSA 解密后内容:" + data);

        System.out.println(Base64Util.urlEncode(privateKey, StandardCharsets.UTF_8));
        System.out.println(Base64Util.urlEncode(publicKey, StandardCharsets.UTF_8));


        String xdPubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDOJTAsGoAsNGJdzaWm835mtpcY2YxGr4NPjnhQmfUrPdT+Zgi6jmm+olYuygNeB8cOSALmTzgXlef+6SdwfRJVEMYVAO7hqF0Ood9zTOc+kolgnWJtqX54CoodfABt0SNS/bsr6hCAWu17RGnbgBaK+ZaJD3NVqXVXE8E30cYHiQIDAQAB";
        String xdPriKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAM4lMCwagCw0Yl3Npabzfma2lxjZjEavg0+OeFCZ9Ss91P5mCLqOab6iVi7KA14Hxw5IAuZPOBeV5/7pJ3B9ElUQxhUA7uGoXQ6h33NM5z6SiWCdYm2pfngKih18AG3RI1L9uyvqEIBa7XtEaduAFor5lokPc1WpdVcTwTfRxgeJAgMBAAECgYAM3XFGL1k0aQiChiUCaEvJKTgAywLgHm/5dRC5JwKP8knqnn+I9P5QcV0jimPvaFjZ4VCdAvCjOC3EUNSvRn7wR2Lb1+BGZZePTdxtHWE2aqJ1W1SvgQTqMsLlPBRPnXo5XH/ng3WEH15ynd5NR035xAluaI0X/y+PsRxE6TlfIQJBAPSYUyXa2yaEqmvIN+ECKALCLLeDdi2YW3Kjahgz0X9V4Y4aTdrHh8y603zXC0Wy8HeOhwGoyciaS8SmjxCMn4UCQQDXweW8xsUreLH8hfVUtyiY/KgUz+R5foJDNXD7TLE9CDoPSHy09qBe99HyVCZg/gNJH4O+tNr6C4916dYaVk01AkBYZ2HOEc8ZmeOaty/zJHtfm9zbqykgi6upwISNINV8Z4bxfHJdO7bKeVANFBBf7a/aFmqXX/EmjxYJioW03o6dAkEAp7ViXJCtJpNU1pNSFZ2hgvmxtSu7zuyVWKSrw8rjYiuI5eRUe13RXsCHgzQB+Ra5exdyEsUGCaL+yosPD73RmQJBALGuM8EQUcBgrpgpeLZ39Ni1DYXYG9aj+u+ar/UL6kI1mCNFgwroO4EVIvXPVxikMxUgiE2tVaBML5nm8VDNJ7s=";

        String channelPubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlyb9aUBaljQP/vjmBFe1mF8HsWSvyfC2NTlpT/V9E+sBxTr8TSkbzJCeeeOEm4LCaVXL0Qz63MZoT24v7AIXTuMdj4jyiM/WJ4tjrWAgnmohNOegfntTto16C3l234vXz4ryWZMR/7W+MXy5B92wPGQEJ0LKFwNEoLspDEWZ7RdE53VH7w6y6sIZUfK+YkXWSwehfKPKlx+lDw3zRJ3/yvMF+U+BAdW/MfECe1GuBnCFKnlMRh3UKczWyXWkL6ItOpYHHJi/jx85op5BWDje2pY9QowzfN94+0DB3T7UvZeweu3zlP6diwAJDzLaFQX8ULfWhY+wfKxIRgs9NoiSAQIDAQAB";
        String channelPriKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCXJv1pQFqWNA/++OYEV7WYXwexZK/J8LY1OWlP9X0T6wHFOvxNKRvMkJ5544SbgsJpVcvRDPrcxmhPbi/sAhdO4x2PiPKIz9Yni2OtYCCeaiE056B+e1O2jXoLeXbfi9fPivJZkxH/tb4xfLkH3bA8ZAQnQsoXA0SguykMRZntF0TndUfvDrLqwhlR8r5iRdZLB6F8o8qXH6UPDfNEnf/K8wX5T4EB1b8x8QJ7Ua4GcIUqeUxGHdQpzNbJdaQvoi06lgccmL+PHzminkFYON7alj1CjDN833j7QMHdPtS9l7B67fOU/p2LAAkPMtoVBfxQt9aFj7B8rEhGCz02iJIBAgMBAAECggEARqOuIpY0v6WtJBfmR3lGIOOokLrhfJrGTLF8CiZMQha+SRJ7/wOLPlsH9SbjPlopyViTXCuYwbzn2tdABigkBHYXxpDV6CJZjzmRZ+FY3S/0POlTFElGojYUJ3CooWiVfyUMhdg5vSuOq0oCny53woFrf32zPHYGiKdvU5Djku1onbDU0Lw8w+5tguuEZ76kZ/lUcccGy5978FFmYpzY/65RHCpvLiLqYyWTtaNT1aQ/9pw4jX9HO9NfdJ9gYFK8r/2f36ZE4hxluAfeOXQfRC/WhPmiw/ReUhxPznG/WgKaa/OaRtAx3inbQ+JuCND7uuKeRe4osP2jLPHPP6AUwQKBgQDUNu3BkLoKaimjGOjCTAwtp71g1oo+k5/uEInAo7lyEwpV0EuUMwLA/HCqUgR4K9pyYV+Oyb8d6f0+Hz0BMD92I2pqlXrD7xV2WzDvyXM3s63NvorRooKcyfd9i6ccMjAyTR2qfLkxv0hlbBbsPHz4BbU63xhTJp3Ghi0/ey/1HQKBgQC2VsgqC6ykfSidZUNLmQZe3J0p/Qf9VLkfrQ+xaHapOs6AzDU2H2osuysqXTLJHsGfrwVaTs00ER2z8ljTJPBUtNtOLrwNRlvgdnzyVAKHfOgDBGwJgiwpeE9voB1oAV/mXqSaUWNnuwlOIhvQEBwekqNyWvhLqC7nCAIhj3yvNQKBgQCqYbeec56LAhWP903Zwcj9VvG7sESqXUhIkUqoOkuIBTWFFIm54QLTA1tJxDQGb98heoCIWf5x/A3xNI98RsqNBX5JON6qNWjb7/dobitti3t99v/ptDp9u8JTMC7penoryLKK0Ty3bkan95Kn9SC42YxaSghzqkt+uvfVQgiNGQKBgGxU6P2aDAt6VNwWosHSe+d2WWXt8IZBhO9d6dn0f7ORvcjmCqNKTNGgrkewMZEuVcliueJquR47IROdY8qmwqcBAN7Vg2K7r7CPlTKAWTRYMJxCT1Hi5gwJb+CZF3+IeYqsJk2NF2s0w5WJTE70k1BSvQsfIzAIDz2yE1oPHvwVAoGAA6e+xQkVH4fMEph55RJIZ5goI4Y76BSvt2N5OKZKd4HtaV+eIhM3SDsVYRLIm9ZquJHMiZQGyUGnsvrKL6AAVNK7eQZCRDk9KQz+0GKOGqku0nOZjUbAu6A2/vtXAaAuFSFx1rUQVVjFulLexkXR3KcztL1Qu2k5pB6Si0K/uwQ=";


        System.out.println(Base64Util.urlEncode(xdPubKey, StandardCharsets.UTF_8));
        System.out.println(Base64Util.urlEncode(xdPriKey, StandardCharsets.UTF_8));


    }
}
