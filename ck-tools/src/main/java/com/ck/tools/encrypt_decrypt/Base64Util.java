package com.ck.tools.encrypt_decrypt;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Base64;
import java.util.Objects;
import java.util.UUID;

/**
 * Base64加解密
 *
 * @author cyk
 * @since 2020-01-01
 */
public class Base64Util {

    /**
     * 获取uuid随机值
     *
     * @return uuid
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }


    /**
     * 字符串进行MD5 加密
     *
     * @param plainText 加密的字符串
     * @return 加密后的字符串
     */
    public static String encodeMD5(String plainText) {
        return encodeMD5(plainText, StandardCharsets.UTF_8);
    }

    /**
     * 字符串进行MD5 加密
     *
     * @param plainText 加密的字符串
     * @return 加密后的字符串
     * @see StandardCharsets
     */
    public static String encodeMD5(String plainText, Charset charset) {
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(plainText.getBytes(charset));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        return new BigInteger(1, secretBytes).toString(16);
    }

    /**
     * Base64 加密
     *
     * @return 密文
     */
    public static String encode(String encryptedData) {
        return encode(encryptedData, StandardCharsets.UTF_8);
    }

    /**
     * Base64 加密
     *
     * @return 密文
     * @see StandardCharsets
     */
    public static String encode(String encryptedData, Charset charset) {
        if (encryptedData == null) return null;
        return new String(encodeByte(encryptedData.getBytes(charset)), charset);
    }

    /**
     * Base64 加密
     *
     * @return 密文
     */
    public static String encode(byte[] encryptedData) {
        if (encryptedData == null) return null;
        return new String(encodeByte(encryptedData), StandardCharsets.UTF_8);
    }

    /**
     * Base64 加密
     *
     * @return 密文
     */
    public static byte[] encodeByte(byte[] encryptedData) {
        if (encryptedData == null) return null;
        return Base64.getEncoder().encode(encryptedData);
    }

    /**
     * Base64 解密
     *
     * @return 明文
     */
    public static String decode(String decoderData) {
        if (decoderData == null) return null;
        return new String(Objects.requireNonNull(decodeByte(decoderData)));
    }

    /**
     * Base64 解密
     *
     * @return 明文
     */
    public static byte[] decodeByte(String decoderData) {
        return decodeByte(decoderData, StandardCharsets.UTF_8);
    }

    /**
     * Base64 解密
     *
     * @return 明文
     * @see StandardCharsets
     */
    public static byte[] decodeByte(String decoderData, Charset charset) {
        if (decoderData == null) return null;
        return Base64.getDecoder().decode(decoderData.getBytes(charset));
    }

    /**
     * URLEncoder.encode
     *
     * @param data    数据
     * @param charset 字符集
     * @return 编译后的字符串
     * @see StandardCharsets
     */
    public static String urlEncode(String data, Charset charset) {
        if (data == null) return null;
        try {
            return URLEncoder.encode(data, charset.name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 散列算法
     *
     * @param str 字符串
     * @return 散列后的字符串
     */
    public static String encodeSHA_256(String str) {
        if (str == null) return null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(str.getBytes());
            byte[] byteData = md.digest();

            /*将字节转换为十六进制格式方法一*/
            StringBuilder sb = new StringBuilder();
            for (byte byteDatum : byteData) {
                sb.append(Integer.toString((byteDatum & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param bytes 字节数组
     * @return 十六进制字符串
     */
    public static String bytesToHex(byte[] bytes) {
        if (bytes == null) return null;
        StringBuilder hexString = new StringBuilder();
        for (byte b : bytes) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}