package com.ck.tools.check_bean.annotation;

import com.ck.tools.exception.CheckException;

import java.lang.annotation.*;

/**
 * 参数检查注解
 *
 * @author cyk
 * @since 15:46 2022/8/2
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CheckFlag {
    /**
     * 检查Flag选项
     */
    String[] value() default {};

    /**
     * 响应第一个消息
     */
    boolean msgSingle() default false;

    /**
     * 校验失败时通过异常抛出，不指定则默认抛出CheckException异常
     *
     * @see CheckException
     */
    Class<? extends Throwable> exception() default CheckException.class;
}