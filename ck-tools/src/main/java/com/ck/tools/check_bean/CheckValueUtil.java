package com.ck.tools.check_bean;


import com.ck.tools.reflect.ClassUtils;

import java.lang.reflect.Field;
import java.util.*;


/**
 * 实体对象属性非空校验工具
 *
 * @author cyk
 * @since 2021-06-06
 */
public class CheckValueUtil {

    /**
     * 递归非空与正则校验
     *
     * @param object 校验的对象
     * @param flag   自定义是否校验标签
     * @return 校验结果
     */
    public static List<CheckResult> checkBeanFieldIsNotNull(Object object, String... flag) {
        return checkBeanFieldIsNotNull(object, Arrays.asList(flag));
    }

    /**
     * 递归非空与正则校验
     *
     * @param object 校验的对象
     * @param flag   自定义是否校验标签
     * @return 校验结果
     */
    public static List<CheckResult> checkBeanFieldIsNotNull(Object object, List<String> flag) {
        Objects.requireNonNull(object, "检查的目标对象不能为空");

        List<CheckResult> result = new ArrayList<>();
        if (Collection.class.isAssignableFrom(object.getClass())) {
            ((Collection<?>) object).forEach(i -> result.addAll(checkBeanFieldIsNotNull(i, flag)));
        } else if (object.getClass().isArray()) {
            Object[] objs = (Object[]) object;
            for (Object obj : objs) {
                result.addAll(checkBeanFieldIsNotNull(obj, flag));
            }
        } else if (!Map.class.isAssignableFrom(object.getClass())) {
            Class<?> cla = object.getClass();
            for (Field field : getFields(cla)) {

                CheckItem checkItem = CheckItem.build(field);
                if (checkItem == null) continue;

                String key = field.getName();
                Object val = getVal(field, object);

                CheckResult checkResult = new CheckResult().setTargetClass(cla).setField(field).setValue(val);

                checkItem.setValue(Optional.ofNullable(checkItem.getValue()).orElse(key));

                Object defaultValue = checkValueIsNotNull(val, checkItem, flag, checkResult);

                if (!checkResult.isSucceed())
                    result.add(checkResult);

                if (val == null && defaultValue != null) {
                    setVal(field, object, defaultValue, key);
                } else if (val != null && checkItem.isChild()) {
                    // 复杂类型进行递归
                    if (Collection.class.isAssignableFrom(val.getClass())) {
                        for (Object item : (Collection) val) {
                            result.addAll(checkBeanFieldIsNotNull(item, flag));
                        }
                    } else if (val.getClass().isArray()) {   // 参数是数组
                        for (Object item : (Object[]) val) {
                            result.addAll(checkBeanFieldIsNotNull(item, flag));
                        }
                    } else if (Map.class.isAssignableFrom(val.getClass())) {
                        for (Object item : ((Map) val).values()) {
                            result.addAll(checkBeanFieldIsNotNull(item, flag));
                        }
                    } else {
                        result.addAll(checkBeanFieldIsNotNull(val, flag));
                    }
                }
            }
        }
        return result;
    }

    /**
     * 递归非空与正则校验
     *
     * @param o         校验的对象
     * @param flag      自定义是否校验标签
     * @param checkItem 校验规则配置
     * @return 校验结果
     */
    public static List<CheckResult> checkBeanFieldIsNotNull(Map<String, Object> o, List<String> flag, CheckItem checkItem) {
        Objects.requireNonNull(o, "检查的目标对象不能为空");
        Objects.requireNonNull(checkItem, "检查的条件对象不能为空");
        Objects.requireNonNull(checkItem.getCheckItemChild(), "检查的条件对象不能为空");

        List<CheckResult> result = new ArrayList<>();

        checkItem.getCheckItemChild().forEach((key, checkItemTemp) -> {
            if (checkItem.getCheckItemChild().containsKey(key)) {
                Object val = o.get(key);
                CheckResult checkResult = new CheckResult().setValue(val);

                if (checkItemTemp.checkIsRequired(flag)) {
                    val = checkValueIsNotNull(val, checkItemTemp, flag, checkResult);
                    if (!checkResult.isSucceed()) {
                        result.add(checkResult);
                    }
                }

                if (val == null && checkItemTemp.getDefaultValue() != null) {
                    o.put(key, val);
                } else if (val != null && isBasicType(val)) {
                    o.put(key, val);
                } else if (val != null && checkItemTemp.isChild() && checkItemTemp.checkIsRequired(flag)) {
                    // 复杂类型进行递归
                    if (Collection.class.isAssignableFrom(val.getClass()) || val.getClass().isArray()) {
                        Collection valTemp;
                        if (val.getClass().isArray()) {
                            assert val instanceof Object[];
                            valTemp = Arrays.asList((Object[]) val);
                        } else {
                            valTemp = (Collection) val;
                        }

                        for (Object item : valTemp) {
                            if (Map.class.isAssignableFrom(item.getClass())) {
                                result.addAll(checkBeanFieldIsNotNull((Map<String, Object>) item, flag, checkItemTemp));
                            }
                        }

                    } else if (Map.class.isAssignableFrom(val.getClass())) {
                        result.addAll(checkBeanFieldIsNotNull((Map<String, Object>) val, flag, checkItemTemp));
                    } else {
                        result.addAll(checkBeanFieldIsNotNull(val, flag));
                    }
                }
            }
        });

        return result;
    }


    /**
     * 非空与正则校验
     *
     * @param val         校验的值
     * @param checkItem   配置的校验规则
     * @param checkResult 校验结果信息对象
     * @param flag        自定义是否校验标签
     * @return 返回校验后的值（配置有默认值则返回默认值）
     */
    private static Object checkValueIsNotNull(Object val, CheckItem checkItem, CheckResult checkResult, String... flag) {
        return checkValueIsNotNull(val, checkItem, Arrays.asList(flag), checkResult);
    }

    private static Object checkValueIsNotNull(Object val, CheckItem checkItem, List<String> flag, CheckResult checkResult) {
        checkResult.setSucceed(true);
        if (checkItem != null) {
            val = checkItem.check(val, checkResult, flag);
            if (checkResult.getMessage() != null) {
                checkResult.setSucceed(false);
                checkResult.setCode(checkItem.getCode());
            }
        }
        return val;
    }


    /**
     * 获取属性值
     *
     * @param field 属性字段
     * @param o     对象
     * @return 属性值
     */
    private static Object getVal(Field field, Object o) {
        return ClassUtils.getValue(field, o);
    }

    /**
     * 为属性赋默认值
     *
     * @param field        属性字段
     * @param o            对象
     * @param defaultValue 默认值
     * @param fieldName    属性名称
     */
    private static void setVal(Field field, Object o, Object defaultValue, String fieldName) {
        try {
            ClassUtils.setValue(o, field, ClassUtils.parseValueType(field, defaultValue));
        } catch (Exception e) {
            throw new RuntimeException(String.format("为属性[%s]赋默认值[%s]异常, 需要的类型为[%s]！", fieldName, defaultValue, field.getType().getSimpleName()));
        }
    }

    /**
     * 判断对象是否基本类型或常见类型
     */
    public static boolean isBasicType(Object o) {
        return ClassUtils.isBasicType(o.getClass());
    }

    /**
     * 获取类的所有属性列表，包含所继承的
     *
     * @param entityClass 类
     * @return 属性列表
     */
    public static Set<Field> getFields(Class<?> entityClass) {
        return ClassUtils.getFields(entityClass);
    }
}
