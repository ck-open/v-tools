package com.ck.tools.check_bean;

import com.ck.tools.check_bean.annotation.CheckValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 检查规则
 *
 * @author cyk
 * @since 2021-06-06
 */
@Data
@Accessors(chain = true)
public class CheckItem {
    private static Logger log = Logger.getLogger(CheckItem.class.getName());

    /**
     * 注解参数构建检查条件的对象
     *
     * @param field 注解参数
     * @return 校验规则对象
     */
    public static CheckItem build(Field field) {
        CheckValue checkValue = field.getAnnotation(CheckValue.class);
        if (checkValue != null) {
            return build(checkValue);
        }

        return ParseValidationApi.build(field);
    }

    /**
     * 注解参数构建检查条件的对象
     *
     * @param checkValue 注解参数
     * @return 校验规则对象
     */
    public static CheckItem build(CheckValue checkValue) {
        return new CheckItem()
                .setValue(checkValue.value().trim().isEmpty() ? null : checkValue.value())
                .setCode(checkValue.code())
                .setRegexp(checkValue.regexp())
                .setDefaultValue("DefaultValue".equals(checkValue.defaultValue()) ? null : checkValue.defaultValue())
                .setRequired(checkValue.required() && !checkValue.isOptional())
                .setEmpty(checkValue.isEmpty())
                .setFlag(Arrays.asList(checkValue.flag()))
                .setChild(checkValue.isChild())
                .setMax(checkValue.max() == -999999999 ? null : checkValue.max())
                .setMin(checkValue.min() == -999999999 ? null : checkValue.min())
                .setName(checkValue.name())
                .setEnumClass(Class.class.isAssignableFrom(checkValue.enumClass()) ? null : checkValue.enumClass())
                .setEnumMethod(Class.class.isAssignableFrom(checkValue.enumClass()) ? null : checkValue.enumMethod())
                .setPastDate(checkValue.pastDate())
                .setPastOrPresentDate(checkValue.pastOrPresentDate())
                .setFutureDate(checkValue.futureDate())
                .setFutureOrPresentDate(checkValue.futureOrPresentDate())
                .setSizeMin(checkValue.sizeMin() == -999999999 ? null : checkValue.sizeMin())
                .setSizeMax(checkValue.sizeMax() == -999999999 ? null : checkValue.sizeMax());
    }

    public static CheckItem build(Map<String, Object> param) {
        Objects.requireNonNull(param, "构建 CheckItem 时，条件对象不能为空");
        CheckItem checkItem = new CheckItem();
        param.forEach((k, v) -> {
            if ("checkItemChild".equalsIgnoreCase(k)) return;
            try {
                Field field = CheckItem.class.getDeclaredField(k);
                field.set(checkItem, v);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                log.info(String.format("构建 CheckItem 时未找到名称为[%s]的条件参数", k));
            }
        });
        if (param.containsKey("checkItemChild") && param.get("checkItemChild") != null
                && Map.class.isAssignableFrom(param.get("checkItemChild").getClass())) {
            checkItem.setCheckItemChild(new HashMap<>());
            ((Map<String, Object>) param.get("checkItemChild"))
                    .forEach((ik, iv) -> {
                        if (iv != null && Map.class.isAssignableFrom(iv.getClass())) {
                            checkItem.getCheckItemChild().put(ik, build((Map<String, Object>) iv));
                        }
                    });

        }
        return checkItem;
    }

    /**
     * 检查失败提示信息
     */
    private String value;

    /**
     * 校验的失败状态代码
     */
    private String code;

    /**
     * 属性名称
     */
    private String name;

    /**
     * 最大值  仅应用与数值类型
     */
    private Double max;

    /**
     * 最小值 仅应用与数值类型
     */
    private Double min;

    /**
     * 为空时的默认值
     */
    private String defaultValue;

    /**
     * 参数正则检查
     */
    private String regexp;

    /**
     * 是否需要子级检查
     */
    private boolean isChild = false;

    /**
     * 是否必传参数
     */
    private boolean required = true;

    /**
     * 值不为null 但是可以为空元素 默认为false 比如空数组、空集合、空字符串
     */
    private boolean isEmpty = false;

    /**
     * 自定义是否校验标签列表
     * 校验标记与指定的标记交集则校验（即使required=false非必传也进行验证）
     */
    private List<String> flag = new ArrayList<>();

    /**
     * 目标枚举类
     */
    private Class<?> enumClass;

    /**
     * 用于验证的枚举方法
     */
    private String enumMethod;

    /**
     * 配置化使用时用于Map层级验证条件用
     */
    private Map<String, CheckItem> checkItemChild;

    /**
     * 必须是一个过去的日期
     */
    private boolean pastDate = false;
    /**
     * 必须是一个将来的日期
     */
    private boolean futureDate = false;

    /**
     * 必须是一个过去的日期 或当前日期
     */
    private boolean pastOrPresentDate = false;
    /**
     * 必须是一个将来的日期 或当前日期
     */
    private boolean futureOrPresentDate = false;
    /**
     * 最小元素个数
     * 应用于数组、集合、字符串长度、Map长度等
     */
    private Integer sizeMin;
    /**
     * 最大元素个数
     * 应用于数组、集合、字符串长度、Map长度等
     */
    private Integer sizeMax;

    /**
     * 检查字段是否必传
     *
     * @param flag 指定的检查标记
     * @return boolean true 需要检查字段规则
     */
    protected boolean checkIsRequired(List<String> flag) {
        // 字段指定了 flag 标记
        if (this.getFlag() != null && !this.getFlag().isEmpty()) {
            // 检查未指定标记 则 不检查字段
            if (flag == null || flag.isEmpty()) {
                return false;
            }
            // 必传 或者 flag 标记中包含指定的标记
            return !Collections.disjoint(this.getFlag(), flag);
        }

        return this.isRequired();
    }

    /**
     * 检查字段
     *
     * @param val         待检查值
     * @param checkResult 校验结果收集器
     * @param flag        指定的检查标记
     * @return Object
     */
    protected Object check(Object val, CheckResult checkResult, List<String> flag) {
        Objects.requireNonNull(this.getValue(), "非空规则校验参数名称必须指定不能为空");
        Objects.requireNonNull(checkResult, "校验结果收集器不能为空");

        checkResult.setRegexp(this.getRegexp());

        if (checkEmpty(val) && !this.isEmpty) {
            val = null;
        }

        if (val == null) {
            if (this.getDefaultValue() != null) {
                val = this.getDefaultValue();
            } else if (checkIsRequired(flag)) {
                checkResult.setMessage(this.getValue() + "为空");
            }
        } else {
            if (this.getEnumClass() != null && this.getEnumClass().isEnum() && !checkEnum(val, this.getEnumClass(), this.getEnumMethod())) {
                /* 枚举校验 */
                checkResult.setMessage(this.getValue() + "值错误");
            } else if (!checkRegex(val, this.getRegexp())) {
                /* 正则校验 */
                checkResult.setMessage(this.getValue() + "规则不符");
            } else if (this.getMax() != null && !checkMax(this.getMax(), val)) {
                checkResult.setMax(this.getMax()).setMessage(String.format("%s大于限定值[%s]", this.getValue(), this.getMax()));
            } else if (this.getMin() != null && !checkMin(this.getMin(), val)) {
                checkResult.setMin(this.getMin()).setMessage(String.format("%s小于限定值[%s]", this.getValue(), this.getMin()));
            } else if (this.getSizeMax() != null && !checkSizeMax(this.getSizeMax(), val)) {
                checkResult.setMessage(String.format("%s长度大于限定值[%s]", this.getValue(), this.getSizeMax()));
            } else if (this.getSizeMin() != null && !checkSizeMin(this.getSizeMin(), val)) {
                checkResult.setMessage(String.format("%s长度小于限定值[%s]", this.getValue(), this.getSizeMin()));
            } else if (this.isPastDate() && !checkPastDate(val)) {
                checkResult.setMessage(String.format("%s必须是一个过去的日期", this.getValue()));
            } else if (this.isPastOrPresentDate() && !checkPastOrPresentDate(val)) {
                checkResult.setMessage(String.format("%s必须是一个过去或当前的日期", this.getValue()));
            } else if (this.isFutureDate() && !checkFutureDate(val)) {
                checkResult.setMessage(String.format("%s必须是一个将来的日期", this.getValue()));
            } else if (this.isFutureOrPresentDate() && !checkFutureOrPresentDate(val)) {
                checkResult.setMessage(String.format("%s必须是一个将来或当前的日期", this.getValue()));
            }
        }
        return val;
    }

    /**
     * 必须是一个将来或当前的日期
     *
     * @return 校验结果 val 大于等于当前时间 返回 true
     */
    private boolean checkFutureOrPresentDate(Object val) {
        Date valTemp = parseDate(val);
        return valTemp != null && !new Date().after(valTemp);
    }

    /**
     * 必须是一个将来或当前的日期
     *
     * @return 校验结果 val 大于当前时间 返回 true
     */
    private boolean checkFutureDate(Object val) {
        Date valTemp = parseDate(val);
        return valTemp != null && new Date().before(valTemp);
    }

    /**
     * 必须是一个过去的日期 或当前日期
     *
     * @return 校验结果 val 小于等于当前时间 返回 true
     */
    private boolean checkPastOrPresentDate(Object val) {
        Date valTemp = parseDate(val);
        return valTemp != null && !new Date().before(valTemp);
    }

    /**
     * 必须是一个过去的日期
     *
     * @return 校验结果 val 小于当前时间 返回 true
     */
    private boolean checkPastDate(Object val) {
        Date valTemp = parseDate(val);
        return valTemp != null && new Date().after(valTemp);
    }

    /**
     * 解析Date类型
     *
     * @param val 日期字符串或 date对象
     * @return 日期对象
     */
    private Date parseDate(Object val) {
        if (val != null) {
            if (val instanceof Date) {
                return (Date) val;
            } else if (val instanceof Instant) {
                return Date.from((Instant) val);
            } else if (val instanceof String) {
                String valTemp = (String) val;

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                try {
                    return simpleDateFormat.parse(valTemp);
                } catch (ParseException e) {
                    simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                    try {
                        return simpleDateFormat.parse(valTemp);
                    } catch (ParseException ex) {
                        simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                        try {
                            return simpleDateFormat.parse(valTemp);
                        } catch (ParseException exc) {
                            valTemp = valTemp.replaceAll("/", "-");
                            if (valTemp.matches("^(\\d{4}-\\d{2}-\\d{2})$")) {
                                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            } else if (valTemp.matches("^(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})$")) {
                                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            } else if (valTemp.matches("^(\\d{4}年\\d{2}月\\d{2}日)$")) {
                                simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
                            } else if (valTemp.matches("^(\\d{4}年\\d{2}月\\d{2}日\\d{2}时\\d{2}分\\d{2}秒)$")) {
                                simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒");
                            }
                            try {
                                return simpleDateFormat.parse(valTemp);
                            } catch (ParseException ignored) {
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 最小元素个数校验
     * 应用于数组、集合、字符串长度、Map长度等
     *
     * @return 校验结果 val 元素个数大于等于sizeMax 返回 true
     */
    private boolean checkSizeMin(Integer sizeMin, Object val) {
        if (val != null && sizeMin != null) {
            if (val instanceof String && ((String) val).length() >= sizeMin) {
                return true;
            } else if (val instanceof Collection && ((Collection<?>) val).size() >= sizeMin) {
                return true;
            } else if (val.getClass().isArray() && Array.getLength(val) >= sizeMin) {
                return true;
            } else return val instanceof Map && ((Map<?, ?>) val).size() >= sizeMin;
        }
        return false;
    }


    /**
     * 最大元素个数校验
     * 应用于数组、集合、字符串长度、Map长度等
     *
     * @return 校验结果 val 元素个数小于等于sizeMax 返回 true
     */
    private boolean checkSizeMax(Integer sizeMax, Object val) {
        if (val != null && sizeMax != null) {
            if (val instanceof String && ((String) val).length() <= sizeMax) {
                return true;
            } else if (val instanceof Collection && ((Collection<?>) val).size() <= sizeMax) {
                return true;
            } else if (val.getClass().isArray() && Array.getLength(val) <= sizeMax) {
                return true;
            } else return val instanceof Map && ((Map<?, ?>) val).size() <= sizeMax;
        }
        return false;
    }

    /**
     * 检查元素非空
     *
     * @return 不为null 为空 返回 true
     */
    protected boolean checkEmpty(Object val) {
        if (val != null) {
            if (val.toString().trim().isEmpty()) {
                return true;
            }
            if (val instanceof Collection && ((Collection<?>) val).isEmpty()) {
                return true;
            }
            if (val instanceof Map && ((Map<?, ?>) val).isEmpty()) {
                return true;
            }

            if (val.getClass().isArray()) {
                assert val instanceof Object[];
                return ((Object[]) val).length < 1;
            }
        }
        return false;
    }

    /**
     * 最大值校验
     *
     * @param max 最大值
     * @param val 当前值
     * @return 校验结果 val 小于 max 值返回 true
     */
    protected static boolean checkMax(double max, Object val) {
        return RegExUtil.checkNumber(String.valueOf(val)) && !(max < Double.parseDouble(String.valueOf(val)));
    }

    /**
     * 最小值校验
     *
     * @param min 最小值
     * @param val 当前值
     * @return 校验结果 val 大于 max 值返回 true
     */
    protected static boolean checkMin(double min, Object val) {
        return RegExUtil.checkNumber(String.valueOf(val)) && !(min > Double.parseDouble(String.valueOf(val)));
    }

    /**
     * 按表达式检查数据对象是否合法
     * <p>值判断基本类型，非基本类型直接返回true</p>
     *
     * @param val   值对象
     * @param regex 正则表达式
     * @return 校验结果
     */
    protected static boolean checkRegex(Object val, String regex) {
        if (regex != null && !regex.trim().isEmpty()) {
            return val.toString().matches(regex);
        }
        return true;
    }

    /**
     * 通过配置的枚举进行校验
     *
     * @param value      值
     * @param cls        枚举类
     * @param methodName 枚举方法
     * @return 校验结果
     */
    protected static boolean checkEnum(Object value, Class<?> cls, String methodName) {
        /* 只要value有值，都要进行校验 */
        if (Objects.nonNull(value)) {
            try {
                Object[] objects = cls.getEnumConstants();
                Method method = cls.getDeclaredMethod(methodName);
                for (Object obj : objects) {
                    /* 使用此注解的枚举类需要重写toString方法，改为需要验证的值 */
                    if (value.equals(method.invoke(obj))) {
                        return true;
                    }
                }
            } catch (Exception e) {
                log.log(Level.WARNING, "对象属性枚举校验异常", e);
            }
        }
        return false;
    }
}
