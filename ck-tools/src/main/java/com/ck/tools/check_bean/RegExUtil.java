package com.ck.tools.check_bean;


import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 正则验证工具类
 *
 * @author cyk
 * @since 2020-01-01
 */
public class RegExUtil {
    /**
     * 手机号码格式匹配
     */
//    public static final String MOBILE_REG = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$";
    public static final String MOBILE_REG = "^(1[3-9][0-9])\\d{8}$";
    /**
     * 座机号码匹配
     */
    public static final String PHONE_REG = "^\\d{3}-\\d{8}|\\d{4}-\\d{7}$";
    /**
     * 汉字匹配
     */
    public static final String CHINESE_REG = "[\\u4e00-\\u9fa5]";
    /**
     * 英文和数字匹配(不区分大小写)
     */
    public static final String LETTER_NUMBER_REG = "^[A-Za-z0-9]+$";

    /**
     * 邮箱email
     */
    public static final String EMAIL_REG = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    /**
     * 日期规则
     */
    public static final String DATE_YMD_REG = "^(\\d{4}-\\d{2}-\\d{2})$";
    /**
     * 时间规则
     */
    public static final String DATE_YMDHMS_REG = "^(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2})$";
    /**
     * 银行卡卡号位数
     */
    public final static String BANK_CARD_NUMBER = "^\\d{16}|\\d{19}$";
    /**
     * 身份证号码位数限制
     */
    public final static String ID_CARD = "^\\d{15}|(\\d{17}[0-9,x,X])$";

    /**
     * 身份证号码 15位格式
     * <p>
     * xxxxxx    yy MM dd   75 0     十五位
     * xxxxxx yyyy MM dd 375 0     十八位
     * 十五位：^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}[0-9Xx]$
     * 十八位：^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$
     * 注释：编码规则顺序从左至右依次为6位数字地址码，6位数字出生年份后两位及日期，3位数字顺序码。
     * 地区：[1-9]\d{5}
     * 年的前两位：(18|19|([23]\d))            1800-3999
     * 年的后两位：\d{2}
     * 月份：((0[1-9])|(10|11|12))
     * 天数：(([0-2][1-9])|10|20|30|31)          闰年不能禁止29+
     * 三位顺序码：\d{3}
     * 两位顺序码：\d{2}
     * 校验码：[0-9Xx]
     * </p>
     */
    public final static String ID_CARD_FORMAT = "(^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}[0-9Xx]$)";

    /**
     * 纯字母
     */
    public static final String LETTER_REG = "^[a-zA-Z]{1,}$";
    /**
     * 数字
     */
    public static final String NUMBER_REG = "^-?((\\d{1,})|(0{1}))(\\.\\d{1,})?$";
    /**
     * 数字  小数点前14后2位
     */
    public static final String NUMBER_FLOAT_14_2_REG = "^-?((\\d{1,14})|(0{1}))(\\.\\d{0,2})?$";
    /**
     * 数字  整数
     */
    public static final String NUMBER_FLOAT_INTEGER_REG = "^(0|[1-9][0-9]*|-[1-9][0-9]*)$";
    /**
     * 数字  整数不含0
     */
    public static final String NUMBER_FLOAT_INTEGER_NOT_0_REG = "^([1-9][0-9]*|-[1-9][0-9]*)$";
    /**
     * 数字  正数
     */
    public static final String NUMBER_FLOAT_POSITIVE_REG = "^([0-9]*)$";
    /**
     * 数字  正数不含0
     */
    public static final String NUMBER_FLOAT_POSITIVE_NOT_0_REG = "^([1-9][0-9]*)$";
    /**
     * 数字  负数
     */
    public static final String NUMBER_FLOAT_MINUS_REG = "^0|(-[0-9]*)(\\.\\d{1,})?$";
    /**
     * 数字  负数不含0
     */
    public static final String NUMBER_FLOAT_MINUS_NOT_0_REG = "^(-[0-9]*)(\\.\\d{1,})?$";

    /**
     * 车牌照正则
     */
    public static final String CAR_PLATE = "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$";

    /**
     * 用户名正则，4到16位（字母，数字，下划线，减号）
     */
    public static final String USERNAME = "^[a-zA-Z0-9_-]{4,16}$";
    /**
     * 密码强度正则，最少6位，包括至少1个大写字母，1个小写字母，1个数字，1个特殊字符
     */
    public static final String PASSWORD = "^.*(?=.{6,})(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*? ]).*$";

    /**
     * 十六进制正则
     */
    public static final String RGB_HEX = "^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$";

    /**
     * 验证正则
     *
     * @param str   验证的字符串
     * @param regex 正则
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkRegex(String str, String regex) {
        return str != null && regex != null && regex.startsWith("^") && regex.endsWith("$") && str.matches(regex);
    }

    /**
     * 十六进制格式验证
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkHEX(String str) {
        return checkRegex(str, RGB_HEX);
    }

    /**
     * 电话座机格式验证
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkPhone(String str) {
        return checkRegex(str, PHONE_REG);
    }

    /**
     * 移动手机号验证
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkMobile(String str) {
        return checkRegex(str, MOBILE_REG);
    }

    /**
     * 汉字匹配验证
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkChinese(String str) {
        return checkRegex(str, CHINESE_REG);
    }

    /**
     * 英文和数字匹配(不区分大小写)
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkLetterNumber(String str) {
        return checkRegex(str, LETTER_NUMBER_REG);
    }

    /**
     * 验证用户名
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkUsername(String str) {
        return checkRegex(str, USERNAME);
    }

    /**
     * 验证密码
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkPassword(String str) {
        return checkRegex(str, PASSWORD);
    }


    /**
     * 纯字母
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkOnlyLetter(String str) {
        return checkRegex(str, LETTER_REG);
    }

    /**
     * 邮箱email
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkEmail(String str) {
        return checkRegex(str, EMAIL_REG);
    }

    /**
     * 日期规则
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkDate_Y_M_D(String str) {
        return checkRegex(str, DATE_YMD_REG);
    }

    /**
     * 时间规则
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkDate_Y_M_D_h_m_s(String str) {
        return checkRegex(str, DATE_YMDHMS_REG);
    }

    /**
     * 银行卡卡号位数
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkBankCard(String str) {
        return checkRegex(str, BANK_CARD_NUMBER);
    }

    /**
     * 身份证号码位数限制
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkIdCard(String str) {
        return checkRegex(str, ID_CARD_FORMAT);
    }

    /**
     * 数字校验
     * <p>
     * 包含正负数和小数
     * </p>
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkNumber(String str) {
        return checkRegex(str, NUMBER_REG);
    }

    /**
     * 正数 小数点前 14 位， 后 2位 校验
     *
     * @param str 验证的字符串
     * @return 不为空且符合格式的字符串 返回true
     */
    public static boolean checkNumberFloat_14_2(String str) {
        return checkRegex(str, NUMBER_FLOAT_14_2_REG);
    }

    /**
     * 校验车拍照
     *
     * @param str 牌照
     * @return 正确返回true
     */
    public static boolean checkCarPlate(String str) {
        return checkRegex(str, CAR_PLATE);
    }

    /**
     * 根据列表值生成正则格式
     *
     * @param in 值范围列表
     * @return 不为空且符合格式的字符串 返回true
     */
    public static String getInRegEx(String... in) {
        return getInRegEx(Arrays.asList(in));
    }

    /**
     * 根据列表值生成正则格式
     *
     * @param in 值范围列表
     * @return 不为空且符合格式的字符串 返回true
     */
    public static String getInRegEx(List<String> in) {
        if (in == null || in.isEmpty()) return null;
        StringBuffer reg = new StringBuffer();
        in.forEach(i -> reg.append("|").append(i));
        return String.format("^%s$", reg.substring(1));
    }

    /**
     * 判断包含
     *
     * @param val 值
     * @param in  列表
     * @return 列表为空 或 值为空 或 不包含 返回 false
     */
    public static boolean checkIn(Object val, Object... in) {
        return checkIn(val, Arrays.asList(in));
    }

    /**
     * 判断包含
     *
     * @param val 值
     * @param in  列表
     * @return 列表为空 或 值为空 或 不包含 返回 false
     */
    public static boolean checkIn(Object val, Collection<?> in) {
        if (in == null || in.isEmpty() || val == null) return false;
        return in.contains(val);
    }
    /**
     * 判断是否为移动端
     * @param userAgent request.header中的 user-agent
     * @return true:移动端
     */
    private boolean isMobile(String userAgent) {
        Pattern mobilePattern = Pattern.compile("Mobile|Android|iPhone|iPad|iPod|BlackBerry|Windows Phone", Pattern.CASE_INSENSITIVE);
        return mobilePattern.matcher(userAgent).find();
    }

    /**
     * 判断是否为平板
     * @param userAgent request.header中的 user-agent
     * @return true:平板
     */
    private boolean isTablet(String userAgent) {
        Pattern tabletPattern = Pattern.compile("iPad|Tablet", Pattern.CASE_INSENSITIVE);
        return tabletPattern.matcher(userAgent).find();
    }

    /**
     * 判断是否为PC
     * @param userAgent request.header中的 user-agent
     * @return true:PC
     */
    private boolean isDesktop(String userAgent) {
        Pattern desktopPattern = Pattern.compile("(?!.*\\(X11;\\sCrOS\\s).*\\(X11|Windows|Macintosh\\)", Pattern.CASE_INSENSITIVE);
        return desktopPattern.matcher(userAgent).matches();
    }

    /**
     * 字符串下划线_转驼峰
     *
     * @param str _分割的字符串
     * @return 驼峰字符串
     */
    public static String toHump(String str) {
        return toHump(str, "_");
    }

    /**
     * 字符串转驼峰
     *
     * @param str       需要分割的字符串
     * @param separator 分割字符,默认下划线_
     * @return 驼峰字符串
     */
    public static String toHump(String str, String separator) {
        if (separator == null || separator.trim().isEmpty()) {
            separator = "_";
        }
        if (str.contains(separator)) {
            StringBuilder f = new StringBuilder();
            String[] s = str.split(separator);
            for (String i : s) {
                i = i.toLowerCase();
                if (f.length() < 1) {
                    f.append(i);
                } else {
                    if (i.isEmpty()) {
                        f.append(i.toUpperCase());
                    } else {
                        f.append(i.substring(0, 1).toUpperCase());
                        if (i.length() > 1) {
                            f.append(i.substring(1));
                        }
                    }
                }
            }
            str = f.toString();
        } else if (!str.isEmpty()) {
            if (str.length() > 1) {
                str = str.substring(0, 1).toLowerCase() + str.substring(1);
            } else {
                str = str.substring(0, 1).toLowerCase();
            }
        } else {
            return str.toLowerCase();
        }
        return str;
    }


    public static void main(String[] args) {
        System.out.println("\r---------------------------十六进制格式验证------------------------------");
        System.out.println(checkHEX("f00"));
        System.out.println("\r---------------------------电话座机格式验证------------------------------");
        System.out.println(checkPhone("12345678"));
        System.out.println("\r---------------------------移动手机号验证------------------------------");
        System.out.println(checkMobile("17100237067"));
        System.out.println("\r---------------------------汉字匹配验证------------------------------");
        System.out.println(checkChinese("42度"));
        System.out.println("\r---------------------------英文和数字匹配(不区分大小写)------------------------------");
        System.out.println(checkLetterNumber("42度AGHHmn"));
        System.out.println("\r---------------------------验证用户名------------------------------");
        System.out.println(checkUsername("42度AGHHmn"));
        System.out.println("\r---------------------------验证密码------------------------------");
        System.out.println(checkPassword("42度AGHHmn"));
        System.out.println("\r---------------------------纯字母------------------------------");
        System.out.println(checkOnlyLetter("hAGHHmn"));
        System.out.println("\r---------------------------验证邮箱------------------------------");
        System.out.println(checkEmail("dsfdskfj2585sd@163.com"));
        System.out.println("\r---------------------------日期规则------------------------------");
        System.out.println(checkDate_Y_M_D("2019-01-01"));
        System.out.println("\r---------------------------日期规则------------------------------");
        System.out.println(checkDate_Y_M_D_h_m_s("2019-01-01 12:00:00"));
        System.out.println("\r---------------------------银行卡卡号位数------------------------------");
        System.out.println(checkBankCard("6222020400000000123456789012"));
        System.out.println("\r---------------------------身份证号码验证------------------------------");
        System.out.println(checkIdCard("123456789123456"));
        System.out.println("\r---------------------------数字校验------------------------------");
        System.out.println(checkNumber("123456789123456"));
        System.out.println("\r---------------------------正数 小数点前 14 位， 后 2位 校验------------------------------");
        System.out.println(checkNumberFloat_14_2("123456789123456"));
        System.out.println("\r---------------------------校验车拍照------------------------------");
        System.out.println(checkCarPlate("123456789123456"));
        System.out.println("\r---------------------------根据列表值生成正则格式------------------------------");
        System.out.println(getInRegEx("A", "F", "D", "5", "9"));
        System.out.println("\r---------------------------判断包含------------------------------");
        System.out.println(checkIn("A", "F", "D", "5", "9"));
        System.out.println("\r---------------------------字符串转驼峰------------------------------");
        System.out.println(toHump("to_hump", "_"));
        System.out.println("\r---------------------------密码规则------------------------------");
        System.out.println(checkPassword("Ab1234!"));
    }
}
