package com.ck.tools.check_bean.annotation;

import java.lang.annotation.*;

/**
 * 参数检查注解
 *
 * @author Cyk
 * @since 15:46 2022/8/2
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface CheckValue {
    /**
     * 失败状态代码
     */
    String code() default "";

    /**
     * 检查失败提示信息
     */
    String value() default "";

    /**
     * 属性名称
     */
    String name() default "";

    /**
     * 最大值  仅应用与数值类型
     */
    double max() default -999999999;

    /**
     * 最小值  仅应用与数值类型
     */
    double min() default -999999999;

    /**
     * 为空时的默认值
     */
    String defaultValue() default "DefaultValue";

    /**
     * 参数正则检查
     */
    String regexp() default "";

    /**
     * 是否需要子级检查
     */
    boolean isChild() default false;

    /**
     * 是否非必传参数
     * 使用 required 替代
     */
    @Deprecated
    boolean isOptional() default false;

    /**
     * 是否必传参数
     * 当指定了flag标记时默认为false
     */
    boolean required() default true;

    /**
     * 值不为null 但是可以为空元素 默认为false 比如空数组、空集合、空字符串
     */
    boolean isEmpty() default false;

    /**
     * 自定义是否校验标签列表
     * 校验标记与指定的标记交集则校验（即使required=false非必传也进行验证）
     */
    String[] flag() default {};

    /**
     * 目标枚举类
     */
    Class<?> enumClass() default Class.class;

    /**
     * 用于验证的枚举方法
     */
    String enumMethod() default "ENUM";

    /**
     * 必须是一个过去的日期
     */
    boolean pastDate() default false;

    /**
     * 必须是一个将来的日期
     */
    boolean futureDate() default false;

    /**
     * 必须是一个过去的日期 或当前日期
     */
    boolean pastOrPresentDate() default false;

    /**
     * 必须是一个将来的日期 或当前日期
     */
    boolean futureOrPresentDate() default false;

    /**
     * 最小元素个数
     * 应用于数组、集合、字符串长度、Map长度等
     */
    int sizeMin() default -999999999;

    /**
     * 最大元素个数
     * 应用于数组、集合、字符串长度、Map长度等
     */
    int sizeMax() default -999999999;

}