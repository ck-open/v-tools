package com.ck.tools.check_bean;

import com.ck.tools.reflect.ClassUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 解析 validation-api 校验规则 javax.validation.constraints 包下的注解配置
 */
public class ParseValidationApi {

    private static final Set<Class<? extends Annotation>> annotationsClass = new HashSet<>();
    private static final Class<? extends Annotation> annotationAssertFalse = ClassUtils.getAnnotationClass("javax.validation.constraints.AssertFalse");
    private static final Class<? extends Annotation> annotationAssertTrue = ClassUtils.getAnnotationClass("javax.validation.constraints.AssertTrue");
    private static final Class<? extends Annotation> annotationDecimalMax = ClassUtils.getAnnotationClass("javax.validation.constraints.DecimalMax");
    private static final Class<? extends Annotation> annotationDecimalMin = ClassUtils.getAnnotationClass("javax.validation.constraints.DecimalMin");
    private static final Class<? extends Annotation> annotationDigits = ClassUtils.getAnnotationClass("javax.validation.constraints.Digits");
    private static final Class<? extends Annotation> annotationEmail = ClassUtils.getAnnotationClass("javax.validation.constraints.Email");
    private static final Class<? extends Annotation> annotationFuture = ClassUtils.getAnnotationClass("javax.validation.constraints.Future");
    private static final Class<? extends Annotation> annotationFutureOrPresent = ClassUtils.getAnnotationClass("javax.validation.constraints.FutureOrPresent");
    private static final Class<? extends Annotation> annotationMax = ClassUtils.getAnnotationClass("javax.validation.constraints.Max");
    private static final Class<? extends Annotation> annotationMin = ClassUtils.getAnnotationClass("javax.validation.constraints.Min");
    private static final Class<? extends Annotation> annotationNegative = ClassUtils.getAnnotationClass("javax.validation.constraints.Negative");
    private static final Class<? extends Annotation> annotationNegativeOrZero = ClassUtils.getAnnotationClass("javax.validation.constraints.NegativeOrZero");
    private static final Class<? extends Annotation> annotationNotBlank = ClassUtils.getAnnotationClass("javax.validation.constraints.NotBlank");
    private static final Class<? extends Annotation> annotationNotEmpty = ClassUtils.getAnnotationClass("javax.validation.constraints.NotEmpty");
    private static final Class<? extends Annotation> annotationNotNull = ClassUtils.getAnnotationClass("javax.validation.constraints.NotNull");
    private static final Class<? extends Annotation> annotationNull = ClassUtils.getAnnotationClass("javax.validation.constraints.Null");
    private static final Class<? extends Annotation> annotationPast = ClassUtils.getAnnotationClass("javax.validation.constraints.Past");
    private static final Class<? extends Annotation> annotationPastOrPresent = ClassUtils.getAnnotationClass("javax.validation.constraints.PastOrPresent");
    private static final Class<? extends Annotation> annotationPattern = ClassUtils.getAnnotationClass("javax.validation.constraints.Pattern");
    private static final Class<? extends Annotation> annotationPositive = ClassUtils.getAnnotationClass("javax.validation.constraints.Positive");
    private static final Class<? extends Annotation> annotationPositiveOrZero = ClassUtils.getAnnotationClass("javax.validation.constraints.PositiveOrZero");
    private static final Class<? extends Annotation> annotationSize = ClassUtils.getAnnotationClass("javax.validation.constraints.Size");


    static {

        Stream.of(ParseValidationApi.class.getDeclaredFields())
                .filter(annotationField -> Modifier.isFinal(annotationField.getModifiers())
                        && Modifier.isStatic(annotationField.getModifiers())
                        && annotationField.getName().startsWith("annotation"))
                .forEach(annotationField -> {
                    annotationField.setAccessible(true);
                    try {
                        if (Annotation.class.isAssignableFrom(annotationField.getType())) {
                            Class<? extends Annotation> annotation = (Class<? extends Annotation>) annotationField.get(null);
                            annotationsClass.add(annotation);
                        }
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    public static CheckItem build(Field field) {
        /*
         *  @Validated   @Valid
         *
         * 解析 validation-api 校验规则 javax.validation.constraints 包下的注解配置
         * @AssertFalse
         * @AssertTrue
         * @DecimalMax("1")
         * @DecimalMin("0")
         * @Digits(integer = 1, fraction = 0)
         * @Email
         * @Future
         * @FutureOrPresent
         * @Max(value = 100)
         * @Min(value = 0)
         * @Negative
         * @NegativeOrZero
         * @NotBlank
         * @NotEmpty
         * @NotNull
         * @Null
         * @Past
         * @PastOrPresent
         * @Pattern(regexp = "^[a-zA-Z0-9_]+$")
         * @Positive
         * @PositiveOrZero
         * @Size(min = 0, max = 100)
         *
         */

        Set<Annotation> annotations = annotationsClass.stream()
                .map(field::getAnnotation)
                .filter(Objects::nonNull).collect(Collectors.toSet());

        if (!annotations.isEmpty()) {
            CheckItem checkItem = new CheckItem().setRequired(false);;
            annotations.forEach(annotation -> {
                if (annotationAssertFalse.isAssignableFrom(annotation.getClass())) {
                    /* 必须为 false */
                    checkItem.setRegexp("false|False|FALSE");
                }
                if (annotationAssertTrue.isAssignableFrom(annotation.getClass())) {
                    /* 必须为 true */
                    checkItem.setRegexp("true|True|TRUE");
                }
                if (annotationMax.isAssignableFrom(annotation.getClass()) || annotationDecimalMax.isAssignableFrom(annotation.getClass())) {
                    /* 必须为数字 且值必须小于等于指定的值 */
                    checkItem.setRegexp(RegExUtil.NUMBER_REG);
                    setCheckItem(annotation, "value", value -> {
                        checkItem.setMax(Double.parseDouble(String.valueOf(value)));
                    });
                }
                if (annotationMin.isAssignableFrom(annotation.getClass()) || annotationDecimalMin.isAssignableFrom(annotation.getClass())) {
                    /* 必须为数字 且值必须大于等于指定的值 */
                    checkItem.setRegexp(RegExUtil.NUMBER_REG);
                    setCheckItem(annotation, "value", value -> {
                        checkItem.setMin(Double.parseDouble(String.valueOf(value)));
                    });
                }
                if (annotationDigits.isAssignableFrom(annotation.getClass())) {
                    /* 必须为数字 且值必须在指定的精度范围内   "^-?((\\d{1,})|(0{1}))(\\.\\d{1,})?$" */
                    AtomicReference<String> regexp = new AtomicReference<>("^-?((\\d{%d,})|(0{1}))");
                    setCheckItem(annotation, "integer", value -> {
                        regexp.set(String.format(regexp.get(), Integer.parseInt(String.valueOf(value))));
                    });
                    setCheckItem(annotation, "fraction", value -> {
                        regexp.set(String.format(regexp.get()+"(\\.\\d{%d,})$", Integer.parseInt(String.valueOf(value))));
                    });
                    checkItem.setRegexp(regexp.get());
                }
                if (annotationEmail.isAssignableFrom(annotation.getClass())) {
                    /* 必须是一个邮箱地址 */
                    checkItem.setRegexp(RegExUtil.EMAIL_REG);
                }
                if (annotationNegative.isAssignableFrom(annotation.getClass())) {
                }
                if (annotationNegativeOrZero.isAssignableFrom(annotation.getClass())) {
                }
                if (annotationNotBlank.isAssignableFrom(annotation.getClass())) {
                    /* 只用于String，不能为null，且trim()之后size>0 */
                    checkItem.setRequired(true);
                }
                if (annotationNotEmpty.isAssignableFrom(annotation.getClass())) {
                    /* 用于集合类，不能为null，且size>0 */
                    checkItem.setRequired(true);
                }
                if (annotationNotNull.isAssignableFrom(annotation.getClass())) {
                    /* 不能为null，但可以为empty，没有size的约束 */
                    checkItem.setRequired(true);
                }
                if (annotationNull.isAssignableFrom(annotation.getClass())) {
                    checkItem.setRequired(true);
                }
                if (annotationPast.isAssignableFrom(annotation.getClass())) {
                    /* 必须是一个过去的日期 */
                    checkItem.setPastDate(true);
                    checkItem.setFutureDate(false);
                }
                if (annotationFutureOrPresent.isAssignableFrom(annotation.getClass())) {
                    /* 必须是一个过去的日期 或当前日期 */
                    checkItem.setPastDate(false);
                    checkItem.setFutureDate(false);
                    checkItem.setFutureOrPresentDate(true);
                    checkItem.setPastOrPresentDate(false);
                }
                if (annotationFuture.isAssignableFrom(annotation.getClass())) {
                    /* 必须是一个将来的日期 */
                    checkItem.setPastDate(false);
                    checkItem.setFutureDate(true);
                }
                if (annotationPastOrPresent.isAssignableFrom(annotation.getClass())) {
                    /* 必须是一个将来的日期 或当前日期 */
                    checkItem.setPastDate(false);
                    checkItem.setFutureDate(false);
                    checkItem.setFutureOrPresentDate(false);
                    checkItem.setPastOrPresentDate(true);
                }
                if (annotationPattern.isAssignableFrom(annotation.getClass())) {
                    /* 必须符合指定的正则表达式 */
                    setCheckItem(annotation, "regexp", value -> {
                        checkItem.setRegexp(String.valueOf(value));
                    });
                }
                if (annotationPositive.isAssignableFrom(annotation.getClass())) {
                }
                if (annotationPositiveOrZero.isAssignableFrom(annotation.getClass())) {
                }
                if (annotationSize.isAssignableFrom(annotation.getClass())) {
                    setCheckItem(annotation, "min", value -> {
                        checkItem.setSizeMin(Integer.parseInt(String.valueOf(value)));
                    });
                    setCheckItem(annotation, "max", value -> {
                        checkItem.setSizeMax(Integer.parseInt(String.valueOf(value)));
                    });
                }
            });
        }

        return null;
    }

    private static void setCheckItem(Annotation annotation, String name, Consumer<Object> consumer) {
        if (annotation != null && consumer != null && name != null && !name.trim().isEmpty()) {
            Method method = ClassUtils.getMethod(annotation.getClass(), name);
            if (method != null) {
                try {
                    Object returnValue = method.invoke(annotation);
                    if (Objects.nonNull(returnValue) && !String.valueOf(returnValue).isEmpty()) {
                        consumer.accept(returnValue);
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
