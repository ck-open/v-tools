package com.ck.tools.check_bean;

import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.reflect.Field;

/**
 * 检查校验结果集
 *
 * @author cyk
 * @since 2022/8/5 9:44
 **/
@Data
@Accessors(chain = true)
public class CheckResult {
    /**
     * 是否成功
     */
    private boolean isSucceed = false;
    /**
     * 校验的类
     */
    private Class<?> targetClass;
    /**
     * 校验的字段
     */
    private Field field;
    /**
     * 校验的失败状态代码
     */
    private String code;
    /**
     * 校验的失败信息
     */
    private String message;
    /**
     * 配置的正则
     */
    private String regexp;
    /**
     * 配置的最大值
     */
    private Double max;
    /**
     * 配置的最小值
     */
    private Double min;
    /**
     * 校验的值
     */
    private Object value;

}
