package com.ck.tools.check_bean;

/**
 * 身份证工具类
 */
public class IdCardUtil {

    /**
     * 验证身份证号码是否正确
     *
     * @param idCard 身份证号码 15|18位
     * @return true 正确 false 错误
     */
    public static boolean checkIdCard(String idCard) {
        return checkIdCardFormat(idCard) == null;
    }

    /**
     * 验证身份证号码格式是否正确
     *
     * @param idCard 身份证号码 15|18位
     * @return 格式正确返回null 否则 返回失败原因
     */
    public static String checkIdCardFormat(String idCard) {
        if (idCard == null || "".equalsIgnoreCase(idCard.trim())) {
            return "证件号为空";
        }

        if (idCard.length() == 18) {
            /* 前17位必须都是数字 */
            if (!isNumber(idCard)) {
                return "存在非法字符";
            } else if (!checkVerifyCode(idCard)) {
                return "校验码错误";
            } else {
                return null;
            }
        } else if (idCard.length() == 15) {
            String idCard18 = idCard15to18(idCard);
            if (!idCard18.startsWith(idCard)) {
                return idCard18;
            }
            return null;
        } else {
            return "位数错误";
        }
    }

    /**
     * 15位身份证号码转化为18位的身份证。如果是18位的身份证则直接返回，不作任何变化。
     *
     * @param idCard 15位的有效身份证号码
     * @return idCard18 返回18位的有效身份证
     */
    public static String idCard15to18(String idCard) {
        if (idCard.length() == 15) {
            if (!isNumber(idCard)) {
                return "存在非数字字符";
            } else {
                /* 格式转换 */
                idCard = idCard.substring(0, 6) + "19" + idCard.substring(6);
                idCard += calculateVerifyCode(idCard);
            }
        }
        return idCard;
    }

    /**
     * 身份证校验码验证
     *
     * @param idCard 证件号
     * @return true: 校验码正确 false: 校验码错误
     */
    private static boolean checkVerifyCode(String idCard) {
        /* 最后一位只能是“X”或者数字 */
        String last = idCard.substring(17, 18);
        if (last.equals("X") || isNumber(last)) {
            /* 生成校验码与输入值比较 */
            return calculateVerifyCode(idCard).equals(last);
        }
        return false;
    }

    /**
     * 计算校验码 校验码（第十八位数）
     *
     * @param idCard 证件号
     * @return VerifyCode
     * 十七位数字本体码加权求和公式 S = Sum(Ai * Wi), i = 0...16 ，先对前17位数字的权求和
     * Ai:表示第i位置上的身份证号码数字值
     * Wi:表示第i位置上的加权因子 Wi: 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2
     * 计算模 Y = mod(S, 11)
     * 通过模得到对应的校验码 Y: 0 1 2 3 4 5 6 7 8 9 10 校验码: 1 0 X 9 8 7 6 5 4 3 2
     */
    private static String calculateVerifyCode(String idCard) {
        String[] codes = new String[]{"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
        int sum = 0;
        for (int i = 0; i < 17; i++) {
            /* 加权因子的算法 */
            int ai = idCard.charAt(i) - '0';
            int wi = (int) ((Math.pow(2, 17 - i)) % 11);
            sum += ai * wi;
        }
        sum %= 11;
        return codes[sum];
    }

    /**
     * 判断证件号是否全是数字
     *
     * @param idCard 证件号
     * @return true: 是数字 false: 不是数字
     */
    private static boolean isNumber(String idCard) {
        int size = idCard.length();
        if (size == 18) {
            size--;
        }
        int ch;
        for (int i = 0; i < size; i++) {
            ch = idCard.codePointAt(i);
            if (ch < 48 || ch > 57) {
                return false;
            }
        }
        return true;
    }

    /**
     * 获取证据号中的生日
     *
     * @param idCard 身份证号码 15|18位
     * @return 生日 yyyy-MM-dd  失败返回 -
     */
    public static String getBirthday(String idCard) {
        if (idCard != null && idCard.length() > 14) {
            int year = Integer.parseInt(idCard.substring(6, 10));
            int month = Integer.parseInt(idCard.substring(10, 12));
            int day = Integer.parseInt(idCard.substring(12, 14));
            return year + "-" + month + "-" + day;
        }
        return "-";
    }

    /**
     * 获取证件号中的性别代码
     *
     * @param idCard 身份证号码 18位
     * @return 性别 1:男 2:女 0:未知
     */
    public static Integer getSex(String idCard) {
        if (idCard != null && idCard.length() > 17) {
            int sex = Integer.parseInt(idCard.substring(16, 17));
            if (sex % 2 == 0) {
                return 2;
            } else {
                return 1;
            }
        }
        return 0;
    }

    /**
     * 获取证件号中的性别名称
     *
     * @param idCard 身份证号码 18位
     * @return 性别 1:男 2:女 0:未知
     */
    public static String getSexName(String idCard) {
        if (idCard != null && idCard.length() > 17) {
            int sex = Integer.parseInt(idCard.substring(16, 17));
            if (sex % 2 == 0) {
                return "女";
            } else {
                return "男";
            }
        }
        return "未知";
    }

    /**
     * 获取证件号中的省份代码
     *
     * @param idCard 身份证号码 15|18位
     * @return 代码
     */
    public static String getProvinceCode(String idCard) {
        if (idCard != null && idCard.length() > 2)
            return idCard.substring(0, 2) + "0000";
        return "-";
    }

    /**
     * 获取证件号中的城市代码
     *
     * @param idCard 身份证号码 15|18位
     * @return 代码
     */
    public static String getCityCode(String idCard) {
        if (idCard != null && idCard.length() > 4)
            return idCard.substring(0, 4) + "00";
        return "-";
    }

    /**
     * 获取证件号中的区县代码
     *
     * @param idCard 身份证号码 15|18位
     * @return 代码
     */
    public static String getCountyCode(String idCard) {
        if (idCard != null && idCard.length() > 6)
            return idCard.substring(0, 6);
        return "-";
    }
}
