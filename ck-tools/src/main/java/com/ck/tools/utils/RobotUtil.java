package com.ck.tools.utils;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Logger;

/**
 * 机器人操作工具类
 *
 * @author cyk
 * @since 2021-06-06
 */
public class RobotUtil {
    private static final Logger log = Logger.getLogger(RobotUtil.class.getName());


    /**
     * 获取操作机器人对象
     *
     * @return 操作机器人对象
     */
    public static Robot getRobot() {
        Robot robot = null;
        try {
            robot = new Robot();
            /* 执行完一个事件后再执行下一个*/
            robot.setAutoWaitForIdle(true);
        } catch (AWTException e) {
            log.warning(String.format("Robot Create Error:%s 创建失败", e.getMessage()));
        }
        return robot;
    }

    /**
     * 移动鼠标位置
     *
     * @param x 移动到的横坐标
     * @param y 移动到的纵坐标
     */
    public static void MouseMove(int x, int y) {
        /* 执行鼠标移动*/
        if (x > 0 && y > 0) {
            getRobot().mouseMove(x, y);
        }
    }

    /**
     * 返回鼠标的真正事件<br>
     * 鼠标事件不能直接处理，需要进过转换
     *
     * @return 鼠标事件值
     */
    public static int getMouseKey(int button) {
        if (button == MouseEvent.BUTTON1) {
            /*鼠标左键*/
            return InputEvent.BUTTON1_MASK;
        } else if (button == MouseEvent.BUTTON2) {
            /*鼠标右键*/
            return InputEvent.BUTTON2_MASK;
        } else if (button == MouseEvent.BUTTON3) {
            /*滚轮*/
            return InputEvent.BUTTON3_MASK;
        } else {
            return 0;
        }
    }

    /**
     * 鼠标事件处理<br>
     * 用来判断事件类型，并用robot类执行
     *
     * @param mouse 鼠标事件
     */
    public static void MouseEvent(MouseEvent mouse) {
        /* 拿到事件类型*/
        int type = mouse.getID();
        if (type == Event.MOUSE_DOWN) {
            /* 鼠标按下*/
            getRobot().mousePress(getMouseKey(mouse.getButton()));
        } else if (type == Event.MOUSE_UP) {
            /* 鼠标抬起*/
            getRobot().mouseRelease(getMouseKey(mouse.getButton()));
        } else if (type == Event.MOUSE_DRAG) {
            /* 鼠标拖动*/
            getRobot().mouseMove(mouse.getX(), mouse.getY());
        }
    }


    /**
     * 键盘按下
     *
     * @param keyCode 键盘点击事件code码
     */
    public static void keyPress(int keyCode) {
        getRobot().keyPress(keyCode);
    }

    /**
     * 键盘抬起
     *
     * @param keyCode 键盘点击事件code码
     */
    public static void keyRelease(int keyCode) {
        getRobot().keyRelease(keyCode);
    }

    /**
     * 启动计算器
     *
     * @return 成功返回true 失败或异常返回false
     */
    public static boolean calc() {
        return exec("calc.exe");
    }

    /**
     * 执行系统程序
     *
     * @param command 命令
     * @return 成功返回true 失败或异常返回false
     */
    public static boolean exec(String command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
            if (process.waitFor() == 0) {
                return true;
            }
        } catch (IOException | InterruptedException e) {
            log.warning(String.format("exec Error:%s", e.getMessage()));
        }
        return false;
    }

    /**
     * 判断当前平台是否支持Desktop类
     *
     * @return 支持返回true 不支持返回false
     */
    public static boolean isDesktopSupported() {
        return Desktop.isDesktopSupported();
    }

    /**
     * 判断当前平台是否支持Open操作
     *
     * @return 支持返回true 不支持返回false
     */
    public static boolean isOpen() {
        return getDesktop().isSupported(Desktop.Action.OPEN);
    }

    /**
     * 判断当前平台是否支持Edit操作
     *
     * @return 支持返回true 不支持返回false
     */
    public static boolean isEdit() {
        return getDesktop().isSupported(Desktop.Action.EDIT);
    }

    /**
     * 判断当前平台是否支持Print操作
     *
     * @return 支持返回true 不支持返回false
     */
    public static boolean isPrint() {
        return getDesktop().isSupported(Desktop.Action.PRINT);
    }

    /**
     * 判断当前平台是否支持Mail操作
     *
     * @return 支持返回true 不支持返回false
     */
    public static boolean isMail() {
        return getDesktop().isSupported(Desktop.Action.MAIL);
    }

    /**
     * 判断当前平台是否支持Browse操作
     *
     * @return 支持返回true 不支持返回false
     */
    public static boolean isBrowse() {
        return getDesktop().isSupported(Desktop.Action.BROWSE);
    }

    /**
     * 获取与当前系统平台关联的Desktop对象
     *
     * @return 支持返回true 不支持返回false
     */
    public static Desktop getDesktop() {
        return Desktop.getDesktop();
    }

    /**
     * 启动系统默认浏览器来访问指定URL
     *
     * @param url 支持返回true 不支持返回false
     */
    public static void browse(String url) {
        try {
            getDesktop().browse(URI.create(url));
        } catch (IOException e) {
            log.warning(String.format("Browse boot Error:%s", e.getMessage()));
        }
    }

    /**
     * 启动关联应用程序来打开文件
     *
     * @param file 支持返回true 不支持返回false
     */
    public static void open(File file) {
        try {
            getDesktop().open(file);
        } catch (IOException e) {
            log.warning(String.format("Open File Error:%s", e.getMessage()));
        }
    }

    /**
     * 启动关联编辑器应用程序并打开用于编辑的文件
     *
     * @param file 要打开的文件
     */
    public static void edit(File file) {
        try {
            getDesktop().edit(file);
        } catch (IOException e) {
            log.warning(String.format("Edit File Error:%s", e.getMessage()));
        }
    }

    /**
     * 使用关联应用程序的打印命令，用本机桌面打印设备来打印文件
     *
     * @param file 要打印的文件
     */
    public static void print(File file) {
        try {
            getDesktop().print(file);
        } catch (IOException e) {
            log.warning(String.format("Print File Error:%s", e.getMessage()));
        }
    }

    /**
     * 启动默认邮件客户端
     */
    public static void mail() {
        try {
            getDesktop().mail();
        } catch (IOException e) {
            log.warning(String.format("Open Mail.java Error:%s", e.getMessage()));
        }
    }

    /**
     * 启动默认邮件客户端，填充由mailtoURI指定的消息字段
     *
     * @param mailtoURI 要发送的邮件地址
     */
    public static void mail(String mailtoURI) {
        try {
            getDesktop().mail(URI.create(mailtoURI));
        } catch (IOException e) {
            log.warning(String.format("Open MailToURI Error:%s", e.getMessage()));
        }
    }

    /**
     * 获取系统剪切板
     *
     * @return 剪切板对象
     */
    public static Clipboard getClipboard() {
        return Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    /**
     * 设置文本到系统剪切板
     *
     * @param str 文本数据
     */
    public static void setStrClipboard(String str) {
        /* 构建String数据类型*/
        StringSelection selection = new StringSelection(str);
        /* 添加文本到系统剪切板*/
        getClipboard().setContents(selection, null);
    }

    /**
     * 从剪切板中获取文本数据
     *
     * @return 文本数据
     */
    public static String getStrClipboard() {
        try {
            /* 从系统剪切板中获取数据*/
            Transferable content = getClipboard().getContents(null);
            /* 判断是否为文本类型*/
            if (content.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                /*从数据中获取文本值*/
                return (String) content.getTransferData(DataFlavor.stringFlavor);
            }
        } catch (UnsupportedFlavorException | IOException e) {
            log.warning(String.format("从剪切板获取文本异常：%s", e.getMessage()));
        }
        return null;
    }

    /**
     * 设置图片到系统剪切板
     *
     * @param image 图片数据
     */
    public static void setImageClipboard(Image image) {
        if (image != null) {
            getClipboard().setContents(new Transferable() {
                @Override
                public DataFlavor[] getTransferDataFlavors() {
                    return new DataFlavor[]{DataFlavor.imageFlavor};
                }

                @Override
                public boolean isDataFlavorSupported(DataFlavor flavor) {
                    return DataFlavor.imageFlavor.equals(flavor);
                }

                @Override
                public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
                    return image;
                }
            }, null);
        }
    }

    /**
     * 从剪切板中获取图片数据
     *
     * @return 图片数据
     */
    public static Image getImageClipboard() {
        Transferable transferable = getClipboard().getContents(null);
        try {
            if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                return (Image) transferable.getTransferData(DataFlavor.imageFlavor);
            }
        } catch (UnsupportedFlavorException | IOException e) {
            log.warning(String.format("从剪切板获取图片异常：%s", e.getMessage()));
        }
        return null;
    }

    /**
     * 获取屏幕截屏
     *
     * @return 屏幕截屏
     */
    public static BufferedImage screenShot() {
        /* 截取整个屏幕*/
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        /* 通过截取的屏幕构造矩形对象*/
        Rectangle rec = new Rectangle(dimension);

        /* 缓冲图像  创建包含从屏幕读取的像素的图像。此图像不包括鼠标光标。*/
        return RobotUtil.getRobot().createScreenCapture(rec);
    }
}
