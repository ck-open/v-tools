package com.ck.tools.utils;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * HttpURLConnection 工具类
 *
 * @author cyk
 * @since 2020-01-01
 */
public class WebNetUtil {
    private static final Logger log = Logger.getLogger(WebNetUtil.class.getName());

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String HEAD = "HEAD";


    /**
     * 文件媒介类型
     */
    private static final Map<String, String> CONTENT_TYPES = new HashMap<>();
    static {
        // Excel 文件
        CONTENT_TYPES.put("xls", "application/vnd.ms-excel");
        CONTENT_TYPES.put("xlt", "application/vnd.ms-excel");
        CONTENT_TYPES.put("xla", "application/vnd.ms-excel");
        CONTENT_TYPES.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        CONTENT_TYPES.put("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
        CONTENT_TYPES.put("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12");
        CONTENT_TYPES.put("xltm", "application/vnd.ms-excel.template.macroEnabled.12");
        CONTENT_TYPES.put("xlam", "application/vnd.ms-excel.addin.macroEnabled.12");
        CONTENT_TYPES.put("xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12");

        // Word 文件
        CONTENT_TYPES.put("doc", "application/msword");
        CONTENT_TYPES.put("dot", "application/msword");
        CONTENT_TYPES.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        CONTENT_TYPES.put("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
        CONTENT_TYPES.put("docm", "application/vnd.ms-word.document.macroEnabled.12");
        CONTENT_TYPES.put("dotm", "application/vnd.ms-word.template.macroEnabled.12");

        // PowerPoint 文件
        CONTENT_TYPES.put("ppt", "application/vnd.ms-powerpoint");
        CONTENT_TYPES.put("pot", "application/vnd.ms-powerpoint");
        CONTENT_TYPES.put("pps", "application/vnd.ms-powerpoint");
        CONTENT_TYPES.put("ppa", "application/vnd.ms-powerpoint");
        CONTENT_TYPES.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        CONTENT_TYPES.put("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
        CONTENT_TYPES.put("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
        CONTENT_TYPES.put("ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12");
        CONTENT_TYPES.put("pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12");
        CONTENT_TYPES.put("potm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12");
        CONTENT_TYPES.put("ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12");

        // 媒体文件
        CONTENT_TYPES.put("zip", "application/zip");
        CONTENT_TYPES.put("tar", "application/x-tar");
        CONTENT_TYPES.put("jpeg", "image/jpeg");
        CONTENT_TYPES.put("jpg", "image/jpeg");
        CONTENT_TYPES.put("png", "image/png");
        CONTENT_TYPES.put("gif", "image/gif");
        CONTENT_TYPES.put("svg", "image/svg+xml");
        CONTENT_TYPES.put("webp", "image/webp");
        CONTENT_TYPES.put("pdf", "application/pdf");
        CONTENT_TYPES.put("mp3", "audio/mpeg");
        CONTENT_TYPES.put("wav", "audio/wav");
        CONTENT_TYPES.put("ogg", "audio/ogg");
        CONTENT_TYPES.put("aac", "audio/aac");
        CONTENT_TYPES.put("flac", "audio/flac");
        CONTENT_TYPES.put("m4a", "audio/x-m4a");
        CONTENT_TYPES.put("mp4", "video/mp4");
        CONTENT_TYPES.put("webm", "video/webm");
        CONTENT_TYPES.put("ovg", "video/ogg");
        CONTENT_TYPES.put("avi", "video/x-msvideo");
        CONTENT_TYPES.put("mkv", "video/x-matroska");
        CONTENT_TYPES.put("mov", "video/quicktime");
    }


    /**
     * 发送HttpURLConnection GET 请求
     *
     * @param url     请求地址
     * @param params  请求参数
     * @param convert 接收实体对象泛型转换函数
     * @param <T>     接收实体对象泛型
     * @return 响应结果
     */
    public static <T> T sendGET(String url, String params, Function<String, T> convert) {
        return send(url, GET, params, null, convert);
    }

    /**
     * 发送HttpURLConnection  请求
     *
     * @param url     请求地址
     * @param params  请求参数
     * @param body    请求参数
     * @param convert 接收实体对象泛型转换函数
     * @param <T>     接收实体对象泛型
     * @return 响应结果
     */
    public static <T> T sendPOST(String url, String params, String body, Function<String, T> convert) {
        return send(url, POST, params, body, convert);
    }

    /**
     * 发送HttpURLConnection  请求
     *
     * @param url      请求地址
     * @param method   请求方式
     * @param params   请求参数
     * @param formData 请求参数
     * @param convert  接收实体对象泛型转换函数
     * @param <T>      接收实体对象泛型
     * @return 响应结果
     */
    public static <T> T sendFormData(String url, String method, String params, Consumer<Map<String, String>> formData, Function<String, T> convert) {
        return sendFormData(url, method, params, false, formData, convert);
    }

    /**
     * 发送HttpURLConnection  请求
     *
     * @param url      请求地址
     * @param method   请求方式
     * @param params   请求参数
     * @param isEncode 是否URL编码,true 将对params 逐个参数进行URL编码
     * @param formData 请求参数
     * @param convert  接收实体对象泛型转换函数
     * @param <T>      接收实体对象泛型
     * @return 响应结果
     */
    public static <T> T sendFormData(String url, String method, String params, boolean isEncode, Consumer<Map<String, String>> formData, Function<String, T> convert) {
        if (Objects.isNull(method)) {
            method = POST;
        }
        url = urlWithFormAdd(url, params, StandardCharsets.UTF_8, isEncode);
        HttpURLConnection conn = getHttpURLConnection(url, method, 60000, 60000);

        StringBuilder formDataStr = new StringBuilder();
        if (Objects.nonNull(formData)) {
            Map<String, String> formDataMap = new LinkedHashMap<>();
            formData.accept(formDataMap);
            if (!formDataMap.isEmpty()) {

                // uuid  分割符
                String boundary = "----" + UUID.randomUUID().toString().replace("-", "");
                conn.addRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
//                conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36");


                boundary = "--" + boundary;
                formDataStr.append(boundary).append("\r\n");
                formDataStr.append(formDataMap.keySet().stream().map(k -> {
                    return "Content-Disposition: form-data; name=\"" + k + "\""
                            + "\r\nContent-Type: text/plain"
                            + "\r\n\r\n" + formDataMap.get(k);
                }).collect(Collectors.joining("\r\n" + boundary + "\r\n")));

                formDataStr.append("\r\n").append(boundary).append("--").append("\r\n");
            }
        }
        String result = sendConnect(conn, formDataStr.length() > 0 ? formDataStr.toString() : null);

        return convert.apply(result);
    }


    /**
     * 发送HttpURLConnection  请求
     *
     * @param url     请求地址
     * @param method  请求方式
     * @param params  请求参数
     * @param body    请求参数
     * @param convert 接收实体对象泛型转换函数
     * @param <T>     接收实体对象泛型
     * @return 响应结果
     */
    public static <T> T send(String url, String method, String params, String body, Function<String, T> convert) {
        if (Objects.isNull(method)) {
            method = Objects.isNull(body) ? GET : POST;
        }
        url = urlWithFormAdd(url, params, StandardCharsets.UTF_8);
        HttpURLConnection conn = getHttpURLConnection(url, method, 60000, 60000);
        String result = sendConnect(conn, body);
        return convert.apply(result);
    }

    /**
     * 发送HttpURLConnection请求
     *
     * @param conn    HttpURLConnection
     * @param body    请求体 GET请求、为空 或者 空字符串 则不发送请求体
     * @param convert 接收实体对象泛型转换函数
     * @param <T>     接收实体对象泛型
     * @return 响应结果
     */
    public static <T> T sendConnect(HttpURLConnection conn, String body, Function<String, T> convert) {
        String result = sendConnect(conn, body);
        return convert.apply(result);
    }

    /**
     * 发送HttpURLConnection请求
     *
     * @param conn HttpURLConnection
     * @param body 请求体 GET请求、为空 或者 空字符串 则不发送请求体
     * @return 响应结果
     * @see StandardCharsets
     */
    public static String sendConnect(HttpURLConnection conn, String body) {
        Objects.requireNonNull(conn, "HttpURLConnection对象为空");
        try {
            if (!conn.getRequestProperties().containsKey("content-encoding")) {
                conn.setRequestProperty("content-encoding", StandardCharsets.UTF_8.name());
            }
            if (!conn.getRequestProperties().containsKey("Charset")) {
                conn.setRequestProperty("Charset", StandardCharsets.UTF_8.name());
            }

            Charset charset = Charset.forName(conn.getRequestProperties().get("content-encoding").get(0));

            if (Objects.nonNull(body)) {
                if (!conn.getRequestProperties().containsKey("Content-Type")) {
                    conn.setRequestProperty("Content-Type", "application/json; charset=" + charset.name());
                }
                conn.setRequestProperty("Content-Length", String.valueOf(body.getBytes(charset).length));
            }

            conn.connect();

            if (Objects.nonNull(body)) {
                try (OutputStream os = conn.getOutputStream()) {
                    io(body.getBytes(charset), os);
                    os.flush();
                } catch (UnknownServiceException e) {
                    log.warning(String.format("请求不支持发送报文。请求地址[%s]请求方式[%s]请求体[%s]异常[%s]", conn.getURL().toString(), conn.getRequestMethod(), body, e.getMessage()));
                }
            }

            return getHttpURLConnResource(conn);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取HttpURLConnection对象请求Response结果
     *
     * @param conn HttpURLConnection
     * @return 响应200时返回响应结果，否则返回错误响应结果
     */
    public static String getHttpURLConnResource(HttpURLConnection conn) {
        return getHttpURLConnResource(conn, Objects.isNull(conn.getContentEncoding()) || conn.getContentEncoding().trim().isEmpty()
                ? StandardCharsets.UTF_8 : Charset.forName(conn.getContentEncoding()));
    }

    /**
     * 获取HttpURLConnection对象请求Response结果
     *
     * @param conn    HttpURLConnection
     * @param charset 字符集
     * @return 响应200时返回响应结果，否则返回错误响应结果
     * @see StandardCharsets
     */
    public static String getHttpURLConnResource(HttpURLConnection conn, Charset charset) {
        Objects.requireNonNull(charset, "字符集为空");
        ByteArrayOutputStream outputStream = getHttpURLConnResourceByte(conn);
        try {
            return outputStream.toString(charset.name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取HttpURLConnection对象请求Response结果
     *
     * @param conn HttpURLConnection
     * @return 响应200时返回响应结果，否则返回错误响应结果
     * @see StandardCharsets
     */
    public static ByteArrayOutputStream getHttpURLConnResourceByte(HttpURLConnection conn) {
        Objects.requireNonNull(conn, "HttpURLConnection对象为空");

        int resCode = -1;

        try {
            resCode = conn.getResponseCode();

            /* 只支持 GET 请求的自动重定向 */
            if (GET.equalsIgnoreCase(conn.getRequestMethod())) {
                if (HttpURLConnection.HTTP_MOVED_PERM == resCode || HttpURLConnection.HTTP_MOVED_TEMP == resCode) {
                    String location = conn.getHeaderField("location");
                    if (Objects.isNull(location)) {
                        location = conn.getHeaderField("Location");
                    }
                    log.warning(String.format("请求重定向。请求地址[%s] 请求方式[%s] 响应码[%s] 重定向[%s]", conn.getURL().toString(), conn.getRequestMethod(), resCode, location));

                    conn = getHttpURLConnection(location, conn.getRequestMethod(), conn.getConnectTimeout(), conn.getReadTimeout());

                    return getHttpURLConnResourceByte(conn);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (InputStream is = HttpURLConnection.HTTP_OK == resCode ? conn.getInputStream() : conn.getErrorStream()) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            io(is, outputStream);
            return outputStream;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取HttpURLConnection对象
     *
     * @param url         请求地址
     * @param method      请求方式
     * @param timeout     超时(毫秒)
     * @param readTimeout 读取超时(毫秒)
     * @return HttpURLConnection对象
     */
    public static HttpURLConnection getHttpURLConnection(String url, String method, int timeout, int readTimeout) {
        Objects.requireNonNull(url, "请求地址URL为空");
        Objects.requireNonNull(method, "请求方式为空");
        // 请求转发
        HttpURLConnection conn = null;
        try {
            URL urlConnection = new URL(url);
            conn = (HttpURLConnection) urlConnection.openConnection();

            // 请求方式
            conn.setRequestMethod(method.toUpperCase());

            // 设置一个指定的超时值(以毫秒为单位)，用于打开到此URLConnection引用的资源的通信链接。
            conn.setConnectTimeout(timeout);
            // 将读取超时设置为指定超时(以毫秒为单位)。非零值指定连接到资源时从输入流读取时的超时。如果超时在可用数据读取之前过期。
            conn.setReadTimeout(readTimeout);

            // 设置输入、输出流是否开启  若需要向服务器请求数据，需要设定为true,默认为false
            conn.setDoOutput(Arrays.asList(POST, PUT).contains(conn.getRequestMethod()));
            conn.setDoInput(true);
            // 是否使用缓存数据  若提交为post方式，需要修改为false
            conn.setUseCaches(!Arrays.asList(POST, PUT).contains(conn.getRequestMethod()));

            /* 设置连接 */
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Accept-Language", "zh-CN,zh;q=0.8");

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(String.format("网络连接获取失败！ URL参数编码失败:%s", url), e);
        } catch (MalformedURLException e) {
            throw new RuntimeException(String.format("网络连接获取失败！ URL格式错误:%s", url), e);
        } catch (IOException e) {
            throw new RuntimeException(String.format("网络连接获取失败！I/O异常。URL:%s %s", method, url), e);
        } catch (Exception e) {
            throw new RuntimeException(String.format("网络连接获取失败！ URL:%s %s", method, url), e);
        }
        return conn;
    }

    /**
     * 关闭连接
     *
     * @param conn HttpURLConnection
     */
    public static void closeHttpURLConnection(HttpURLConnection conn) {
        if (conn == null) {
            return;
        }
        /* 断开连接*/
        conn.disconnect();
    }

    /**
     * 追加url 与 参数
     *
     * @param url     原url（如果携带参数拼接时会追加新参数）
     * @param params  参数
     * @param charset 字符集
     * @return 拼接后的完整url
     * @see StandardCharsets
     */
    public static String urlWithFormAdd(String url, String params, Charset charset) {
        return urlWithFormAdd(url, params, charset, false);
    }

    /**
     * 追加url 与 参数
     *
     * @param url      原url（如果携带参数拼接时会追加新参数）
     * @param params   参数
     * @param charset  字符集
     * @param isEncode 是否URL编码,true 将对params 逐个参数进行URL编码
     * @return 拼接后的完整url
     * @see StandardCharsets
     */
    public static String urlWithFormAdd(String url, String params, Charset charset, boolean isEncode) {
        /* 地址不包含API 则拼接上当前请求地址API */
        if (url != null && url.contains("?") && url.length() > url.indexOf("?") + 1) {
            String paramOld = url.substring(url.indexOf("?") + 1);
            url = url.substring(0, url.indexOf("?"));
            params = params == null ? paramOld : paramOld + "&" + params;
        }
        return urlWithForm(url, params, charset, isEncode);
    }

    /**
     * 拼接url 与 参数
     *
     * @param url     原url
     * @param params  参数
     * @param charset 字符集
     * @return 拼接后的完整url
     * @see StandardCharsets
     */
    public static String urlWithForm(String url, String params, Charset charset) {
        return urlWithForm(url, params, charset, false);
    }

    /**
     * 拼接url 与 参数
     *
     * @param url      原url
     * @param params   参数
     * @param charset  字符集
     * @param isEncode 是否URL编码,true 将对params 逐个参数进行URL编码
     * @return 拼接后的完整url
     * @see StandardCharsets
     */
    public static String urlWithForm(String url, String params, Charset charset, boolean isEncode) {
        if (Objects.isNull(url) || url.isEmpty()) {
            return url;
        }
        String oldParams = null;
        if (url.contains("?")) {
            if (url.length() > url.indexOf('?') + 1) {
                oldParams = url.substring(url.indexOf('?') + 1);
            }

            url = url.substring(0, url.indexOf('?'));
        }

//        if (isEncode) {
//            url = urlEncode(url, charset);
//        }

        StringBuilder result = new StringBuilder(url).append("?");

        if (!Objects.isNull(oldParams)) {
            result.append(isEncode ? urlEncode(oldParams, charset) : oldParams);
        }

        if (!Objects.isNull(params)) {
            if (!Objects.isNull(oldParams)) {
                result.append("&");
            }
            result.append(isEncode ? urlEncode(params, charset) : params);
        }
        return result.toString();
    }

    /**
     * 构建请求参数
     *
     * @param params 求参数
     *               个数必须为双数，第一各为key 第二个为值 第三个为key ... 依此类推
     *               数组参数在值中以逗号分割，表示数组
     * @return 拼接后的参数值
     * @see StandardCharsets
     */
    public static String buildParams(Charset charset, String... params) {
        if (params == null || params.length < 1 || params.length % 2 != 0) {
            return "";
        }
        Map<String, String[]> paramsMap = new LinkedHashMap<>();
        for (int i = 0; i < params.length; i += 2) {
            String key = params[i];
            String value = params[i + 1];
            if (Objects.isNull(key) || key.isEmpty()) {
                continue;
            }
            if (Objects.isNull(value) || value.isEmpty()) {
                paramsMap.put(key, null);
            } else {
                String[] values = value.split(",");
                paramsMap.put(key, values);
            }
        }
        return buildParams(paramsMap, charset, null);
    }

    /**
     * 重新构建请求参数
     *
     * @param consumer 自定义参数处理
     * @return 拼接后的参数值
     * @see StandardCharsets
     */
    public static String buildParams(Charset charset, Consumer<Map<String, Object>> consumer) {
        return buildParams(null, charset, consumer);
    }

    /**
     * 重新构建请求参数
     *
     * @param oldParams 原请求参数  HttpServletRequest.getParameterMap()
     * @param consumer  自定义参数处理
     * @return 拼接后的参数值
     * @see StandardCharsets
     */
    public static String buildParams(Map<String, String[]> oldParams, Charset charset, Consumer<Map<String, Object>> consumer) {
        Charset charsetTemp = Objects.isNull(charset) ? StandardCharsets.UTF_8 : charset;

        Map<String, Object> query = new LinkedHashMap<>();
        if (!Objects.isNull(oldParams)) {
            oldParams.forEach((k, v) -> query.put(k, v.length > 0 ? String.join(",", v) : null));
        }
        if (consumer != null) {
            consumer.accept(query);
        }

        StringBuffer params = new StringBuffer();
        AtomicBoolean isFirst = new AtomicBoolean(true);
        query.forEach((k, v) -> {
            if (isFirst.get()) {
                isFirst.set(false);
            } else {
                params.append("&");
            }
            if (!Objects.isNull(k)) {
                params.append(k)
                        .append("=")
                        .append(Objects.isNull(v) ? null : urlEncode(v.toString(), charsetTemp));
            }
        });

        return params.toString();
    }

    /**
     * URLEncoder.encode
     *
     * @param url     地址链接
     * @param charset 字符集
     * @return 编码后的字符串
     * @see StandardCharsets
     */
    public static String urlParamEncode(String url, Charset charset) {
        if (url != null && url.contains("?") && url.length() > url.indexOf("?") + 1) {
            String param = url.substring(url.indexOf("?") + 1);
            url = url.substring(0, url.indexOf("?"));
            return urlWithFormAdd(url, param, charset, true);
        }
        return url;
    }

    /**
     * URLEncoder.encode
     *
     * @param url     地址链接
     * @param charset 字符集
     * @return 编码后的字符串
     * @see StandardCharsets
     */
    public static String urlChineseEncode(String url, Charset charset) {
        try {

            StringBuilder result = new StringBuilder();
            for (char c : url.toCharArray()) {
                if (isChinese(c)) {
                    result.append(URLEncoder.encode(String.valueOf(c), charset.name()));
                } else {
                    result.append(c);
                }
            }

            return result.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * URLEncoder.encode
     *
     * @param url     地址链接
     * @param charset 字符集
     * @return 编码后的字符串
     * @see StandardCharsets
     */
    public static String urlEncode(String url, Charset charset) {
        try {
            return URLEncoder.encode(url, charset.name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * URLDecoder.decode
     *
     * @param url     地址链接
     * @param charset 字符集
     * @return 编码后的字符串
     * @see StandardCharsets
     */
    public static String urlDecoder(String url, Charset charset) {
        try {
            return URLDecoder.decode(url, charset.name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取文件介质类型 ContentType
     *
     * @param fileName 文件名称（带格式后缀）
     */
    public static String getContentTypeByteFileType(String fileName) {
        if (fileName != null && !fileName.isEmpty()) {
            int lastDotIndex = fileName.lastIndexOf('.');
            if (lastDotIndex != -1 && lastDotIndex < fileName.length() - 1) {
                String extension = fileName.substring(lastDotIndex + 1).toLowerCase();
                String mimeType = CONTENT_TYPES.getOrDefault(extension, "text/event-stream");
                return mimeType + ";charset=utf-8";
            }
        }
        return "text/event-stream;charset=utf-8";
    }

    /**
     * 数据传输 速率默认2M
     *
     * @param inputStream  输入流
     * @param outputStream 输出流
     */
    public static boolean io(InputStream inputStream, OutputStream outputStream) {
        return io(inputStream, outputStream, 2);
    }

    /**
     * 数据传输 速率默认2M
     *
     * @param inputStream  输入流
     * @param outputStream 输出流
     * @param speed        指定传输速率 单位M
     */
    public static boolean io(InputStream inputStream, OutputStream outputStream, double speed) {
        try {
            speed = speed > 0 ? speed * 1024 : 2048;
            byte[] buffer = new byte[(int) speed];
            int len = inputStream.read(buffer);
            while (len != -1) {
                outputStream.write(buffer, 0, len);
                outputStream.flush();
                len = inputStream.read(buffer);
            }
            return true;
        } catch (IOException e) {
            throw new RuntimeException("文件数据传输失败！", e);
        }
    }

    /**
     * 数据传输 速率默认2M
     *
     * @param bytes  字节数组
     * @param output 输出流
     */
    public static boolean io(byte[] bytes, OutputStream output) {
        return io(bytes, output, 2);
    }

    /**
     * 数据传输 速率默认2M
     *
     * @param bytes  字节数组
     * @param output 输出流
     * @param speed  指定传输速率 单位M
     */
    public static boolean io(byte[] bytes, OutputStream output, double speed) {
        try {
            if (bytes != null) {
                int len = speed > 0 ? (int) (speed * 1024) : 2048;
                for (int i = 0; i < bytes.length; i = i + len) {
                    if (i + len > bytes.length)
                        len = bytes.length - i;
                    output.write(bytes, i, len);
                    output.flush();
                }
            }
            return true;
        } catch (IOException e) {
            throw new RuntimeException("文件数据传输失败！", e);
        }
    }

    /**
     * 将多个文件压缩下载输出
     *
     * @param files key 文件完整名称,value 文件二进制数据
     * @param os    压缩文件输出
     */
    public static void exportFileZip(Map<String, byte[]> files, OutputStream os) {
        try (ZipOutputStream zos = new ZipOutputStream(os);) {
            files.forEach((k, v) -> {
                try {
                    zos.putNextEntry(new ZipEntry(k));
                    zos.write(v, 0, v.length);
                    zos.closeEntry();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException("文件压缩输出异常", e);
        }
    }

    /**
     * 判断单个字符是否为中文字符
     *
     * @param c 字符
     * @return 如果是中文字符，则返回true；否则返回false
     */
    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.CJK_RADICALS_SUPPLEMENT
                || ub == Character.UnicodeBlock.KANGXI_RADICALS
                || ub == Character.UnicodeBlock.IDEOGRAPHIC_DESCRIPTION_CHARACTERS;
    }

    /**
     * URL 地址/* 或/** 通配符规则匹配
     *
     * @param reg 通配符地址
     * @param url 比较地址
     * @return 匹配成功返回 true
     */
    public static boolean matchingUrl(String reg, String url) {

        if (reg == null || reg.trim().isEmpty() || url == null || url.trim().isEmpty()) {
            return false;
        }

        if (reg.equalsIgnoreCase(url)) {
            return true;
        }

        /* 如果有匹配符** 则转换 */
        if (reg.indexOf("**") > 0) {
            reg = reg.replaceAll("\\*\\*", "*");
        }
        if (reg.indexOf("***") > 0) {
            reg = reg.replaceAll("\\*\\*\\*", "*");
        }

        /* 如果是根级通配符则直接返回 */
        if ("/*".equals(reg))
            return true;

        /* 按 * 切割字符串 */
        String[] regSplit = reg.split("/");
        String[] urlSplit = url.split("/");

        for (int i = 0; i < urlSplit.length; i++) {
            if (regSplit.length <= i) {
                return false;
            }

            /* 不是是通配符 且 字符串不能完全匹配 不区分大小写 */
            if (!"*".equalsIgnoreCase(regSplit[i]) && !regSplit[i].equalsIgnoreCase(urlSplit[i])) {
                return false;
            }

            /* 遍历的是条件的最后一个 */
            if (i == regSplit.length - 1 && regSplit.length != urlSplit.length) {
                /* 匹配条件是通配符 */
                if (regSplit[i].equalsIgnoreCase("*")) {
                    break;
                } else {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * 通过IP地址获取MAC地址
     *
     * @param ip string, 127.0.0.1格式
     * @return mac string
     */
    public static String getMAC(String ip) {
        String line = "";
        String macAddress = "";
        final String MAC_ADDRESS_PREFIX = "MAC Address =";
        final String LOOPBACK_ADDRESS = "127.0.0.1";

        try {
            /* 如果为127.0.0.1,则获取本机MAC地址。*/
            if (LOOPBACK_ADDRESS.equals(ip)) {
                InetAddress inetAddress = InetAddress.getLocalHost();
                /*貌似此方法器 DK1.6*/
                byte[] mac = NetworkInterface.getByInetAddress(inetAddress).getHardwareAddress();
                /*下面代码是把 mac 地址拼接成String*/
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < mac.length; i++) {
                    if (i != 0) {
                        sb.append("-");
                    }
                    /*mac[i] & OxFF 是为了把byte转化为正整数*/
                    String s = Integer.toHexString(mac[i] & 0xFF);
                    sb.append(s.length() == 1 ? 0 + s : s);
                }
                /*把字符串所有小写字母改为大写成为正规的mac地址并返回*/
                macAddress = sb.toString().trim().toUpperCase();
                return macAddress;
            }

            /*获取非本 IP的MAC地址*/
            Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
            InputStreamReader isr = new InputStreamReader(p.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            while ((line = br.readLine()) != null) {
                int index = line.indexOf(MAC_ADDRESS_PREFIX);
                if (index != -1) {
                    macAddress = line.substring(index + MAC_ADDRESS_PREFIX.length()).trim().toUpperCase();
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
        return macAddress;
    }

    /**
     * 获取本机IP
     *
     * @return 本机本地IP地址
     */
    public static String getHostAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (Exception ignored) {
        }
        return null;
    }

    /**
     * 获取本地外网IP
     *
     * @return 外网IP地址
     */
    public static String getOuterIp() {
        String localip = null;/* 本地IP*/
        String netip = null; /* 外网IP*/
        for (InetAddress ip : getLocalIPLike()) {
            if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && !ip.getHostAddress().contains(":")) {/* 外网IP*/
                netip = ip.getHostAddress();
            } else if (ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
                    && !ip.getHostAddress().contains(":")) {/* 内网IP*/
                localip = ip.getHostAddress();
            }
        }
        if (netip != null && !netip.isEmpty()) {
            return netip;
        } else {
            return localip;
        }
    }

    /**
     * 获取IP 到外网链路
     *
     * @return 链路列表
     */
    public static List<InetAddress> getLocalIPLike() {
        List<InetAddress> ipList = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            NetworkInterface networkInterface;
            Enumeration<InetAddress> inetAddresses;
            while (networkInterfaces.hasMoreElements()) {
                networkInterface = networkInterfaces.nextElement();
                inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress instanceof Inet4Address) { /* IPV4*/
                        ipList.add(inetAddress);
                    }
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ipList;
    }


    public static void main(String[] args) {
        System.out.println("\r---------------------------中文字符判断------------------------------");
        System.out.println(isChinese('A'));
        System.out.println(isChinese('都'));
        System.out.println("\r---------------------------WebUtil展示------------------------------");
        System.out.println("拼接url 与 参数 ==> " + WebNetUtil.urlWithForm("http://apis.juhe.cn/idcard", WebNetUtil.buildParams(StandardCharsets.UTF_8, "key", "44d1cbaa1665151ec1514e685b68a764"), StandardCharsets.UTF_8, false));
        System.out.println("构建请求参数 ==> " + WebNetUtil.buildParams(StandardCharsets.UTF_8, "key", "44d1cbaa1665151ec1514e685b68a764"));
        Map<String, String[]> params = new HashMap<>();
        System.out.println("重新构建请求参数 ==> " + WebNetUtil.buildParams(params, StandardCharsets.UTF_8, param -> param.put("key", "44d1cbaa1665151ec1514e685b68a764")));
        System.out.println("URL编码 ==> " + WebNetUtil.urlEncode("http://apis.juhe.cn/idcard/index?key=44d1cba", StandardCharsets.UTF_8));
        System.out.println("通过IP地址获取MAC地址 ==> " + WebNetUtil.getMAC("127.0.0.1"));
        System.out.println("获取本机IP ==> " + WebNetUtil.getHostAddress());
        System.out.println("获取本地外网IP ==> " + WebNetUtil.getOuterIp());
        System.out.println("获取IP 到外网链路 ==> " + WebNetUtil.getLocalIPLike());
        System.out.println("\r---------------------------URL 地址/* 或/** 通配符规则匹配------------------------------");
        System.out.println(WebNetUtil.matchingUrl("A/b/**/okh", "A/b/okh"));
        System.out.println("\r---------------------------访问聚合数据获取身份证信息------------------------------");
//        HttpURLConnection connection = getHttpURLConnection("http://apis.juhe.cn/idcard/index?key=44d1cbaa1665151ec1514e685b68a764&cardno=152327199202153518"
//                , GET, 1000 * 10, 1000 * 10);
//        String result = sendConnect(connection,null);
//        System.out.println(result);

        System.out.println(urlEncode("https://tiger-uat.hinsurance.cn/tiger/cos/downLoad/other_temp_202406-20240823093313.pdf", StandardCharsets.UTF_8));
        System.out.println(urlEncode("https://tiger-uat.hinsurance.cn/tiger/cos/downLoad/other_temp_3D小人-20240824205617.pdf", StandardCharsets.UTF_8));
    }
}
