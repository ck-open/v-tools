package com.ck.tools.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * 时间业务操作工具
 *
 * @author cyk
 * @since 2020-01-01
 */
public class TimeUtil {
    public static final String[] WEEK_DAY_STRING = {"周一", "周二", "周三", "周四", "周五", "周六", "周日"};
    public static final String Format_Y_M_D = "yyyy-MM-dd";
    public static final String Format_Y_M = "yyyy-MM";
    public static final String Format_Y = "yyyy";
    public static final String Format_ISO8601 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String Format_RFC822 = "EEE MMM dd HH:mm:ss zzz yyyy";
    public static final String Format_YMDHmS = "yyyyMMddHHmmss";
    public static final String Format_Y_M_D_H_m_S = "yyyy-MM-dd HH:mm:ss";
    public static final String Format_PRC_D = "yyyy年MM月dd日";
    public static final String Format_PRC_S = Format_PRC_D + " HH时mm分ss秒";
    public static final String Format_Y_M_D_H_m = "yyyy-MM-dd HH:mm";
    public static final String Format_H_m_S = "HH:mm:ss";
    public static final String Format_H_m = "HH:mm";
    public static final String Interval_Y = "y";
    public static final String Interval_YH = "yh";
    public static final String Interval_Q = "q";
    public static final String Interval_M = "M";
    public static final String Interval_D = "d";
    public static final String Interval_H = "h";
    public static final String Interval_m = "m";
    public static final String Interval_S = "s";
    public static final String Interval_W = "w";

    /**
     * 获取当前时间 格式化到日 如：2020-01-01 00:00:00
     *
     * @return 当前时间字符串
     */
    public static Date nowYear() {
        return parseTime(now(Format_Y));
    }

    /**
     * 获取当前时间 格式化到日 如：2020-02-01 00:00:00
     *
     * @return 当前时间字符串
     */
    public static Date nowMonth() {
        return parseTime(now(Format_Y_M));
    }

    /**
     * 获取当前时间 格式化到日 如：2020-02-02 00:00:00
     *
     * @return 当前时间字符串
     */
    public static Date nowDay() {
        return parseTime(now(Format_Y_M_D));
    }

    /**
     * 获取当前时间
     *
     * @return 当前时间字符串
     */
    public static Date now() {
        return new Date();
    }

    /**
     * 获取当前时间字符串
     *
     * @param pattern 格式，默认：yyyy-MM-dd HH:mm:ss
     * @return 当前时间字符串
     */
    public static String now(String pattern) {
        return formatNow("{}", pattern);
    }

    /**
     * 格式化当前时间
     *
     * @param formatStr 字符串信息模板
     * @param pattern   每个{}占位符的格式,从左到右依次替换，默认：yyyy-MM-dd HH:mm:ss
     * @return 格式化后的字符串
     */
    public static String formatNow(String formatStr, String... pattern) {
        Date now = now();
        if (formatStr != null && pattern != null && pattern.length > 0) {
            if (formatStr.contains("{}")) {
                for (String pat : pattern) {
                    formatStr = formatStr.replaceFirst("\\{}", format(now, pat == null ? Format_Y_M_D_H_m_S : pat));
                }
            }
            return formatStr;
        } else {
            return format(now, pattern == null ? Format_Y_M_D_H_m_S : pattern[0]);
        }
    }

    /**
     * 时间字符串格式化为 yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 格式化后的时间字符串
     */
    public static String parseTimeBlind(String time) {
        if (time == null || (!time.contains("-") && !time.contains("/") && !time.contains("\\") && !time.contains("年"))) {
            if (time != null && time.length() == 4) {
                return time + "-01-01 00:00:00";
            }
            return time;
        }
        time = time.trim();
        time = time.replaceAll("/", "-")
                .replaceAll("\\\\", "-")
                .replaceAll("年", "-")
                .replaceAll("月", "-")
                .replaceAll("日", "")
                .replaceAll("时", ":")
                .replaceAll("点", ":")
                .replaceAll("分", ":")
                .replaceAll("秒", "")
                .replaceAll("'", "")
                .replaceAll("’", "")
                .replaceAll("S", "")
                .replaceAll("s", "")
                .replaceAll("Z", "")
                .replaceAll("z", "")
                .replaceAll("T", " ")
                .replaceAll("t", " ");

        if (time.contains("/")) {
            time = time.replaceAll("/", "-");
        }
        if (time.contains("-")) {
            String[] temp = time.contains(" ") ? time.split(" ") : new String[]{time};
            if (temp[0].length() < 10) {
                String[] s = temp[0].split("-");

                for (int i = 0; i < s.length; i++) {
                    if (i == 0 && s[i].length() < 4) {
                        s[i] = formatDate_Y(now()).substring(0, s[i].length()) + s[i];
                    } else if (s[i].length() < 2) {
                        s[i] = 0 + s[i];
                    }
                }
                if (s.length == 0) {
                    temp[0] = formatDate_Y(new Date()) + "-01-01";
                } else if (s.length == 1) {
                    temp[0] = s[0] + "-01-01";
                } else if (s.length == 2) {
                    temp[0] = s[0] + "-" + s[1] + "-01";
                } else {
                    temp[0] = s[0] + "-" + s[1] + "-" + s[2];
                }
            }

            time = temp[0];
            if (temp.length > 1) {
                if (temp[1].contains(":")) {
                    String[] s = temp[1].split(":");

                    for (int i = 0; i < s.length; i++) {
                        if (s[i].length() < 2) {
                            s[i] = 0 + s[i];
                        }
                    }
                    if (s.length == 0) {
                        temp[1] = "00:00:00";
                    } else if (s.length == 1) {
                        temp[1] = s[0] + ":00:00";
                    } else if (s.length == 2) {
                        temp[1] = s[0] + ":" + s[1] + ":00";
                    } else {
                        temp[1] = s[0] + ":" + s[1] + ":" + s[2];
                    }
                }
                time += " " + temp[1];
            } else {
                time += " 00:00:00";
            }
        }
        if (time.length() > 19) {
            time = time.substring(0, 19);
        }
        return time;
    }

    /**
     * 时间字符串解析，支持盲解析
     *
     * @param val 时间对象
     * @return 解析后的时间
     */
    public static Date parseTime(Object val) {
        if (val instanceof Date) {
            return (Date) val;
        } else if (val instanceof Instant) {
            return Date.from((Instant) val);
        } else if (val instanceof LocalDateTime) {
            return Date.from(((LocalDateTime) val).atZone(ZoneId.systemDefault()).toInstant());
        } else if (val instanceof LocalDate) {
            return Date.from(((LocalDate) val).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        } else if (val instanceof String) {
            String time = (String) val;
            if (!"".equalsIgnoreCase(time.trim())) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Format_Y_M_D_H_m_S);
                try {
                    return simpleDateFormat.parse(parseTimeBlind(time));
                } catch (ParseException e) {
                    simpleDateFormat = new SimpleDateFormat(Format_ISO8601);
                    try {
                        return simpleDateFormat.parse(time);
                    } catch (ParseException e1) {
                        simpleDateFormat = new SimpleDateFormat(Format_RFC822, Locale.US);
                        try {
                            return simpleDateFormat.parse(time);
                        } catch (ParseException e2) {
                            simpleDateFormat = new SimpleDateFormat(Format_YMDHmS, Locale.US);
                            try {
                                return simpleDateFormat.parse(time);
                            } catch (ParseException e3) {
                                throw new RuntimeException(String.format("时间盲解析异常！  time: %s", time), e);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * 解析字符串为时间类型
     *
     * @param time    时间戳字符串
     * @param pattern 格式，默认：yyyy-MM-dd HH:mm:ss
     * @return 格式化后的日期
     */
    public static Date parseTime(String time, String pattern) {
        if (pattern == null) {
            pattern = Format_Y_M_D_H_m_S;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            return simpleDateFormat.parse(time);
        } catch (ParseException e) {
            throw new RuntimeException(String.format("时间格式转换异常！  time: %s, pattern: %s", time, pattern), e);
        }
    }

    /**
     * 获取当前季度字符串  2020-1
     *
     * @param time 时间
     * @return 季度字符串
     */
    public static String getQuarter_Y_Q(Date time) {
        Calendar calendar = Calendar.getInstance();
        if (time != null)
            calendar.setTime(time);
        return calendar.get(Calendar.YEAR) + "-" + getQuarter(calendar);
    }

    /**
     * 获取季度数字
     *
     * @param time 时间
     * @return 季度数字 1-4
     */
    public static int getQuarter(Date time) {
        Calendar calendar = Calendar.getInstance();
        if (time != null)
            calendar.setTime(time);
        return getQuarter(calendar);
    }

    /**
     * 获取季度数字
     *
     * @param calendar 时间
     * @return 季度数字 1-4
     */
    public static int getQuarter(Calendar calendar) {
        if (calendar == null) calendar = Calendar.getInstance();

        if (calendar.get(Calendar.MONTH) <= 2) {
            return 1;
        } else if (calendar.get(Calendar.MONTH) <= 5) {
            return 2;
        } else if (calendar.get(Calendar.MONTH) <= 8) {
            return 3;
        } else {
            return 4;
        }
    }

    /**
     * 按周格式化时间字符串
     *
     * @param time 时间
     * @return 格式化后的字符串 2020-52
     */
    public static String formatWeekOfYear(Date time) {
        return formatWeekOfYear(time, null);
    }

    /**
     * 按周格式化时间字符串
     *
     * @param time      时间
     * @param separator 分隔符
     * @return 格式化后的字符串 2020 分隔符(默认-) 52
     */
    public static String formatWeekOfYear(Date time, String separator) {
        if (time != null) {
            Calendar calendar = parseCalendar(time);
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                calendar.add(Calendar.DAY_OF_WEEK, -1);
            }
            return calendar.get(Calendar.YEAR) + (separator == null ? "-" : separator) + calendar.get(Calendar.WEEK_OF_YEAR);
        }
        return null;
    }

    /**
     * 获取周中的天字符串<br>
     * 格式：周一...周日
     *
     * @param date 日期
     * @return 周中的天字符串
     */
    public static String formatWeekDayStr(Date date) {
        if (date != null) {
            return WEEK_DAY_STRING[getDayOfWeek(date) - 1];
        }
        return null;
    }

    /**
     * 获取周中的天<br>
     * 周一为第一天
     *
     * @param date 日期
     * @return 周中的天
     */
    public static Integer getDayOfWeek(Date date) {
        if (date != null) {
            Calendar calendar = parseCalendar(date);
            int day = calendar.get(Calendar.DAY_OF_WEEK);
            return day == 1 ? 7 : day - 1;
        }
        return null;
    }

    /**
     * 将时间格式化成字符串 年
     *
     * @param date 日期
     * @return 格式化后的字符串
     */
    public static String formatDate_Y(Date date) {
        return format(date, Format_Y);
    }

    /**
     * 将时间格式化成字符串 月
     *
     * @param date 日期
     * @return 格式化后的字符串
     */
    public static String formatDate_M(Date date) {
        return format(date, Format_Y_M);
    }

    /**
     * 将时间格式化成字符串 日
     *
     * @param date 日期
     * @return 格式化后的字符串
     */
    public static String formatDate_D(Date date) {
        return format(date, Format_Y_M_D);
    }

    /**
     * 将时间格式化成字符串 时分
     *
     * @param date 日期
     * @return 格式化后的字符串
     */
    public static String formatDate_m(Date date) {
        return format(date, Format_Y_M_D_H_m);
    }

    /**
     * 将时间格式化成字符串 时分秒
     *
     * @param date 日期
     * @return 格式化后的字符串
     */
    public static String formatDate_S(Date date) {
        return format(date, Format_Y_M_D_H_m_S);
    }

    /**
     * 解析时间类型为字符串
     *
     * @param date    日期时间
     * @param pattern 格式，默认：yyyy-MM-dd HH:mm:ss
     * @return 格式化后的日期字符串
     */
    public static String format(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        if (pattern == null) {
            pattern = Format_Y_M_D_H_m_S;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    /**
     * 将秒值格式化为 时间格式
     *
     * @param second 秒数
     * @return 按小时计数的字符串 例如 48:25:36
     */
    public static String formatHour(Number second) {
        if (second == null) {
            return "00:00:00";
        }
        long secondNum = second.longValue();
        String hourStr = format(add(nowDay(), Interval_S, (int) Math.ceil((secondNum % (3600 * 24)))), "HH:mm:ss");

        if (secondNum >= 3600 * 24) {
            long hour = Long.parseLong(hourStr.substring(0, 2));
            hour += secondNum / (3600 * 24) * 24;
            hourStr = String.format("%02d:%s", hour, hourStr.substring(3));
        }
        return hourStr;
    }

    /**
     * 获取当前到指定的时间单位结束时间 相差的毫秒值
     *
     * @param interval 数据间隔粒度调整开始结束时间：y、M、d、h、m、s、w
     * @return 当前时间 到 时间粒度的最后时间 相差的秒值
     */
    public static long lastTimeUntil_S(String interval) {
        return lastTimeUntil(interval) / 1000;
    }

    /**
     * 获取当前到指定的时间单位结束时间 相差的毫秒值
     *
     * @param interval 数据间隔粒度调整开始结束时间：y、M、d、h、m、s、w
     * @return 当前时间 到 时间粒度的最后时间 相差的毫秒值
     */
    public static long lastTimeUntil(String interval) {
        return lastTime(now(), interval).getTime() - now().getTime();
    }

    /**
     * 获取时间粒度的最后时间<br>
     *
     * @param time     时间字符串
     * @param interval 数据间隔粒度调整开始结束时间：y、M、d、h、m、s、w
     * @return 时间粒度的最后时间
     */
    public static Date lastTime(String time, String interval) {
        Date date = parseTime(time);
        return lastTime(date, interval);
    }

    /**
     * 获取时间粒度的最后时间<br>
     *
     * @param date     时间字符串
     * @param interval 数据间隔粒度调整开始结束时间：y、M、d、h、m、s、w
     * @return 时间粒度的最后时间
     */
    public static Date lastTime(Date date, String interval) {
        if (date == null || interval == null) {
            return date;
        }
        Calendar calendar = parseCalendar(date);

        if (!Interval_M.equals(interval)) {
            interval = interval.toLowerCase();
        }
        switch (interval) {
            case Interval_Y:
                calendar.set(calendar.get(Calendar.YEAR), Calendar.DECEMBER, 31, 23, 59, 59);
                break;
            case Interval_M:
                int dayLast = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), dayLast, 23, 59, 59);
                break;
            case Interval_D:
                calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
                break;
            case Interval_H:
                calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), 59, 59);
                break;
            case Interval_m:
                calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 59);
                break;
            case Interval_S:
                break;
            case Interval_Q:  /* 季度 */
                int month = calendar.get(Calendar.MONTH);
                if (month <= 2) {
                    calendar.set(calendar.get(Calendar.YEAR), Calendar.MARCH, 31, 23, 59, 59);
                } else if (month <= 5) {
                    calendar.set(calendar.get(Calendar.YEAR), Calendar.JUNE, 30, 23, 59, 59);
                } else if (month <= 8) {
                    calendar.set(calendar.get(Calendar.YEAR), Calendar.SEPTEMBER, 30, 23, 59, 59);
                } else {
                    calendar.set(calendar.get(Calendar.YEAR), Calendar.DECEMBER, 31, 23, 59, 59);
                }
                break;
            case Interval_YH:  /* 半年 */
                if (calendar.get(Calendar.MONTH) >= 5) {
                    calendar.set(calendar.get(Calendar.YEAR), Calendar.DECEMBER, 31, 23, 59, 59);
                } else {
                    calendar.set(calendar.get(Calendar.YEAR), Calendar.JUNE, 30, 23, 59, 59);
                }
                break;
        }
        return calendar.getTime();
    }

    /**
     * 根据指定的粒度获取给定日期的开始时间<br>
     *
     * @param time     时间字符串
     * @param interval 数据间隔粒度调整开始时间：y、M、d、h、m、s、w
     * @return 给定日期的开始时间
     */
    public static Date startTime(String time, String interval) {
        Date date = parseTime(time);
        return startTime(date, interval);
    }

    /**
     * 调整为开始时间或结束时间<br>
     *
     * @param date     时间字符串
     * @param interval 数据间隔粒度调整开始结束时间：y、M、d、h、m、s、w、q（季度）、yh（半年）
     * @return 时间按照间隔粒度调整后的开始或结束时间
     */
    public static Date startTime(Date date, String interval) {
        if (date != null) {
            Calendar calendar = parseCalendar(date);

            if (interval != null && !"".equalsIgnoreCase(interval.trim())) {
                if (!Interval_M.equals(interval)) {
                    interval = interval.toLowerCase();
                }
                switch (interval) {
                    case Interval_Y:
                        calendar.set(calendar.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
                        break;
                    case Interval_M:
                        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1, 0, 0, 0);
                        break;
                    case Interval_D:
                        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                        break;
                    case Interval_H:
                        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), 0, 0);
                        break;
                    case Interval_m:
                        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), 0);
                        break;
                    case Interval_S:
                        break;
                    case Interval_W:
                        calendar.add(Calendar.DATE, 2 - (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY ? 8 : calendar.get(Calendar.DAY_OF_WEEK)));
                        break;
                    case Interval_Q:  /* 季度 */
                        int month = calendar.get(Calendar.MONTH);
                        if (month <= 2) {
                            calendar.set(calendar.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
                        } else if (month <= 5) {
                            calendar.set(calendar.get(Calendar.YEAR), Calendar.APRIL, 1, 0, 0, 0);
                        } else if (month <= 8) {
                            calendar.set(calendar.get(Calendar.YEAR), Calendar.JULY, 1, 0, 0, 0);
                        } else {
                            calendar.set(calendar.get(Calendar.YEAR), Calendar.OCTOBER, 1, 0, 0, 0);
                        }
                        break;
                    case Interval_YH:  /* 半年 */
                        if (calendar.get(Calendar.MONTH) >= 5) {
                            calendar.set(calendar.get(Calendar.YEAR), Calendar.JUNE, 1, 0, 0, 0);
                        } else {
                            calendar.set(calendar.get(Calendar.YEAR), Calendar.JANUARY, 1, 0, 0, 0);
                        }
                        break;
                }
            }

            date = parseTime(formatDate_S(calendar.getTime()));
        }
        return date;
    }

    /**
     * 时间进行同比或环比 调整
     *
     * @param time     调整的时间
     * @param interval 调整的时间单位：w/y/M/d/h/m/s  周/年/月/日/时
     * @param isQoq    是否调整环比：true/false  同比（年减一）环比
     * @return 调整后的时间
     */
    public static Date getYoYOrQoQ(Date time, String interval, Boolean isQoq) {
        /* 查询的数据 范围时间 按照 同比、环比、当前数据 进行调整 */
        if (time != null) {
            Calendar calendar = parseCalendar(time);

            if (isQoq != null && isQoq) {  /* 环比 */
                if (Interval_Q.equalsIgnoreCase(interval)) { /* 季度 quarter */
                    calendar.add(Calendar.MONTH, -3);
                } else if (Interval_YH.equals(interval)) { /* 半年  Years_Half */
                    calendar.add(Calendar.MONTH, -6);
                } else {
                    add(calendar, interval, -1);
                }
            } else {  /* 同比 */
                add(calendar, interval, -1);
            }
            return calendar.getTime();
        }
        return null;
    }

    /**
     * 按照开始和结束时间 生成 趋势起始和结束范围时间列表
     * 例如（月粒度）：
     * 2023-12-31 00:00:00------2024-01-30 23:59:59
     * 2024-01-31 00:00:00------2024-02-28 23:59:59
     * 2024-02-29 00:00:00------2024-03-30 23:59:59
     * 2024-03-31 00:00:00------2024-04-29 23:59:59
     * 2024-04-30 00:00:00------2024-05-30 23:59:59
     * 2024-05-31 00:00:00------2024-06-29 23:59:59
     * 2024-06-30 00:00:00------2024-07-30 23:59:59
     * 2024-07-31 00:00:00------2024-08-30 23:59:59
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param interval  间隔粒度
     * @return 时间轴数组，指定粒度的时间范围，key 粒度起期  value 粒度止期
     */
    public static Map<Date, Date> getTrendTimesScope(Date startTime, Date endTime, String interval) {
        List<Date> trends = getTrendTimes(startTime, add(endTime, interval, 1), interval);
        Map<Date, Date> results = new LinkedHashMap<>();
        for (int i = 0; i < trends.size() - 1; i++) {
            results.put(trends.get(i), add(trends.get(i + 1), Interval_S, -1));
        }
        return results;
    }

    /**
     * 按照开始和结束时间 生成 趋势时间轴数组
     * 例如（月粒度）：
     * 2023-12-31 00:00:00
     * 2024-01-31 00:00:00
     * 2024-02-29 00:00:00
     * 2024-03-31 00:00:00
     * 2024-04-30 00:00:00
     * 2024-05-31 00:00:00
     * 2024-06-30 00:00:00
     * 2024-07-31 00:00:00
     * 2024-08-31 00:00:00
     * 2024-09-30 00:00:00
     * 2024-10-31 00:00:00
     * 2024-11-30 00:00:00
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param interval  间隔粒度
     * @return 时间轴数组，指定粒度的起期
     */
    public static List<Date> getTrendTimes(Date startTime, Date endTime, String interval) {
        List<Date> result = new ArrayList<>();
        if (startTime != null && endTime != null) {
            Calendar calendar = parseCalendar(startTime);

            int initDay = calendar.get(Calendar.DAY_OF_MONTH);

            while (calendar.getTime().getTime() <= endTime.getTime()) {
                result.add(calendar.getTime());
                add(calendar, interval, 1);
                if (Interval_M.equals(interval)) {
                    /* 如果是月粒度，需要重置日为初始时间的日分量 或 当月最后一天 */
                    calendar.set(Calendar.DAY_OF_MONTH, Math.min(initDay, calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
                }
            }
        }
        return result;
    }

    /**
     * 获取自适应数据间隔粒度<br>
     * 最小间隔为60s(秒)，最大数据个数限制500个
     *
     * @param startTime 数据范围 开始时间
     * @param endTime   数据范围 结束时间
     * @return 返回的间隔值单位为s(秒)
     */
    public static Integer getAdaptInterval(Date startTime, Date endTime) {
        return getAdaptInterval(startTime, endTime, 60, 500);
    }

    /**
     * 获取自适应数据间隔粒度
     *
     * @param startTime   数据范围 开始时间
     * @param endTime     数据范围 结束时间
     * @param minInterval 最小间隔阈值，单位为s(秒)
     * @param maxParticle 最大数据个数限制阈值
     * @return 返回的间隔值单位为s(秒)
     */
    public static Integer getAdaptInterval(Date startTime, Date endTime, int minInterval, double maxParticle) {
        if (startTime != null && endTime != null) {
            /* 自适应  间隔时间粒度m = （（结束时间 - 开始时间（秒值）） / 默认间隔粒度） < 间隔粒度阈值上限 ？ 默认间隔粒度 ： （结束时间 - 开始时间（秒值）） / 间隔粒度阈值上限 */
            long timeInterval = (endTime.getTime() - startTime.getTime()) / 1000;  /* 时间间隔秒值 */
            double totalParticle = (double) timeInterval / minInterval;
            return totalParticle < maxParticle ? minInterval : new BigDecimal(timeInterval).divide(new BigDecimal(maxParticle), 0, RoundingMode.HALF_UP).intValue();
        }
        return null;
    }

    /**
     * 获取两个时间之间的天数
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return 天数
     */
    public static Integer getDaysOfBetween(Date startDate, Date endDate) {
        double between_days = getSecondOfBetween(startDate, endDate) / (3600 * 24.0);
        return (int) Math.ceil((between_days));
    }

    /**
     * 获取两个时间之间的秒数
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return 天数
     */
    public static Integer getSecondOfBetween(Date startDate, Date endDate) {
        if (startDate == null || endDate == null) {
            return 0;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(startDate);
        long time2 = cal.getTimeInMillis();
        double between_num = Math.abs(time1 - time2) / 1000.0;
        return (int) Math.ceil((between_num));
    }

    /**
     * 获取两个时间之间的小时数
     *
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @return 按小时计数的字符串 例如 48:25:36
     */
    public static String getBetweenHour(Date startDate, Date endDate) {
        long secondNum = getSecondOfBetween(startDate, endDate);
        return formatHour(secondNum);
    }

    /**
     * 根据出生日期计算年龄
     *
     * @param birthDay 生日
     * @return 年龄
     */
    public static Integer getAge(Date birthDay) {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new RuntimeException(String.format("出生日期晚于当前时间，无法计算! 出生日期：%s  当前时间：%s", formatDate_S(birthDay), formatDate_S(cal.getTime())));
        }
        int yearNow = cal.get(Calendar.YEAR);  /* 当前年份 */
        int monthNow = cal.get(Calendar.MONTH);  /* 当前月份 */
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH); /* 当前日期 */
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;   /* 计算整岁数 */
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;/* 当前日期在生日之前，年龄减一 */
            } else {
                age--;/* 当前月份在生日之前，年龄减一 */
            }
        }
        return age;
    }

    /**
     * 解析 Date
     *
     * @param localDateTime localDateTime对象
     * @return Date
     */
    public static Date parseDate(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 解析 Date
     *
     * @param localDate localDate对象
     * @return Date
     */
    public static Date parseDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return parseDate(localDate.atStartOfDay());
    }

    /**
     * 解析时间字符串为 Calendar
     *
     * @param date date
     * @return calendar
     */
    public static Calendar parseCalendar(String date) {
        return parseCalendar(parseTime(date));
    }

    /**
     * 解析Date串为 Calendar
     *
     * @param date date
     * @return calendar
     */
    public static Calendar parseCalendar(Date date) {
        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        }
        return null;
    }

    /**
     * 转换LocalDate
     *
     * @param date date
     * @return localDate
     */
    public static LocalDate formatLocalDate(Date date) {
        if (date == null) {
            return null;
        }
        return formatLocalDateTime(date).toLocalDate();
    }

    /**
     * 转换LocalDate
     *
     * @param time time
     * @return localDate
     */
    public static LocalDate formatLocalDate(String time) {
        LocalDateTime localDateTime = formatLocalDateTime(time);
        return localDateTime == null ? null : localDateTime.toLocalDate();
    }

    /**
     * 转换LocalDate
     *
     * @param date date
     * @return localDateTime
     */
    public static LocalDateTime formatLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /**
     * 解析字符串时间
     *
     * @param time time
     * @return LocalDateTime
     */
    public static LocalDateTime formatLocalDateTime(String time) {
        return formatLocalDateTime(parseTime(time));
    }

    /**
     * 增加时间分量
     *
     * @param date     日期
     * @param interval 分量类型：y、M、d、h、m、s、w
     * @param val      增加的分量值
     */
    public static Date add(Date date, String interval, int val) {
        if (date == null) {
            return null;
        }
        Calendar calendar = parseCalendar(date);
        return add(calendar, interval, val).getTime();
    }

    /**
     * 增加时间分量
     *
     * @param calendar 日期
     * @param interval 分量类型：y、M、d、h、m、s、w
     * @param val      增加的分量值
     */
    public static Calendar add(Calendar calendar, String interval, int val) {
        if (calendar != null) {
            switch (interval) {
                case Interval_Y:
                    calendar.add(Calendar.YEAR, val);
                    break;
                case Interval_M:
                    int day = calendar.get(Calendar.DAY_OF_MONTH);
                    calendar.add(Calendar.MONTH, val);
                    calendar.set(Calendar.DAY_OF_MONTH, Math.min(day, calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
                    break;
                case Interval_D:
                    calendar.add(Calendar.DATE, val);
                    break;
                case Interval_H:
                    calendar.add(Calendar.HOUR_OF_DAY, val);
                    break;
                case Interval_m:
                    calendar.add(Calendar.MINUTE, val);
                    break;
                case Interval_S:
                    calendar.add(Calendar.SECOND, val);
                    break;
                case Interval_W:
                    calendar.add(Calendar.WEEK_OF_YEAR, val);
                    break;
                case Interval_Q:  /* 季度 */
                    /* 设置当前月份为当前季度最大月 */
                    add(calendar, Interval_M, val * 3);
                    break;
                case Interval_YH:  /* 半年 */
                    /* 设置当前月份为当前半年度最大月 */
                    add(calendar, Interval_M, val * 6);
                    break;
                default:
                    break;
            }
        }
        return calendar;
    }


    public static void main(String[] args) {
        System.out.println("\r---------------------------日期解析格式化展示------------------------------");
        System.out.println("对日期[2024] 盲解析 ==> " + formatDate_S(parseTime("2024")));
        System.out.println("对日期[2024-2] 盲解析 ==> " + formatDate_S(parseTime("2024-2")));
        System.out.println("对日期[2024-5-05] 盲解析 ==> " + formatDate_S(parseTime("2024-5-05")));
        System.out.println("对日期[2024年5-05日] 盲解析 ==> " + formatDate_S(parseTime("2024年5-05日")));
        System.out.println("对日期[2024/5-05日] 盲解析 ==> " + formatDate_S(parseTime("2024/5-05日")));
        System.out.println("对日期[2024-5-05日 22:] 盲解析 ==> " + formatDate_S(parseTime("2024-5-05日 22:")));
        System.out.println("对日期[2024-05-02 22时25:35] 盲解析 ==> " + formatDate_S(parseTime("2024-05-02 22时25:35")));
        System.out.println("对日期[" + now() + "] 盲解析 ==> " + formatDate_S(parseTime(now().toString())));
        System.out.println("对日期[2024-05-02] 格式化周 ==> " + formatWeekOfYear(parseTime("2024-05-02")));
        System.out.println("对日期[2024-05-02] 格式化周 ==> " + formatWeekOfYear(parseTime("2024-05-02"), "年"));
        System.out.println("对日期[2024-05-02] 格式化周 ==> " + formatWeekDayStr(parseTime("2024-05-02")));

        System.out.println("\r---------------------------日期加减操作展示------------------------------");
        System.out.println(now(Format_Y_M_D_H_m_S));
        System.out.println(formatNow("当前时间格式化==>{} ", Format_Y_M_D_H_m_S));
        String timestamp = now(Format_Y_M_D);
        Date time = parseTime(timestamp);
        System.out.println("对日期[" + timestamp + "] 加一季度 ==> " + formatDate_S(add(time, Interval_Q, 1)));
        System.out.println("对日期[" + timestamp + "] 加一月 ==> " + formatDate_S(add(time, Interval_M, 1)));
        System.out.println("对日期[" + timestamp + "] 减一周 ==> " + formatDate_S(add(time, Interval_W, -1)));
        System.out.println("对日期[" + timestamp + "] 减一天 ==> " + formatDate_S(add(time, Interval_D, -1)));
        System.out.println("获取日期[" + timestamp + "] 最后时间-年 ==> " + formatDate_S(lastTime(time, Interval_Y)));
        System.out.println("获取日期[" + timestamp + "] 最后时间-半年 ==> " + formatDate_S(lastTime(time, Interval_YH)));
        System.out.println("获取日期[" + timestamp + "] 最后时间-季度 ==> " + formatDate_S(lastTime(time, Interval_Q)));
        System.out.println("获取日期[" + timestamp + "] 最后时间-月 ==> " + formatDate_S(lastTime(time, Interval_M)));
        System.out.println("获取日期[" + timestamp + "] 最后时间-日 ==> " + formatDate_S(lastTime(time, Interval_D)));

        System.out.println("\r---------------------------趋势日期展示------------------------------");
        Objects.requireNonNull(getTrendTimes(parseTime("2023-12-31"), parseTime("2024-12-30 23:35:45"), Interval_M)).forEach(i -> {
            System.out.println(formatDate_S(i));
        });

        System.out.println("\r---------------------------趋势日期范围展示------------------------------");
        Map<Date, Date> map = getTrendTimesScope(parseTime("2023-12-31"), parseTime("2024-12-30"), Interval_M);
        map.forEach((k, v) -> System.out.println(formatDate_S(k) + "------" + formatDate_S(v)));

        System.out.println("\r---------------------------自定义测试展示------------------------------");
        System.out.println("获取到指定的时间单位结束时间 相差的秒值 ==> " + lastTimeUntil_S(Interval_D));
        System.out.println("获取到指定的时间区间 相差的秒值 ==> " + getSecondOfBetween(parseTime("2024-12-30 00:00:00"),parseTime("2024-12-30 06:08:50")));
        System.out.println("获取到指定的时间区间 相差的小时值字符串 ==> " + getBetweenHour(parseTime("2024-12-30 00:00:00"),parseTime("2024-12-31 01:00:00")));
        System.out.println("获取到指定的秒值 格式化为小时字符串 ==> " + formatHour(90005));

    }
}
