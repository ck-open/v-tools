package com.ck.tools.utils;

import com.ck.tools.exception.CalculatorException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Stream;

/**
 * 函数表达式计算器
 *
 * @author Cyk
 * @since 18:09 2022/11/18
 **/
public class CalcUtil {
    /**
     * 基础运算符列表
     */
    protected static final Set<String> BaseTags = new LinkedHashSet<>(Arrays.asList("+", "-", "*", "/", "(", ")", ",", "<", ">", "<>", "><", "==", "!="));

    /**
     * 默认计算精度
     */
    private static Integer scale = 8;
    /**
     * 默认计算精度
     */
    private static RoundingMode roundingMode = RoundingMode.HALF_UP;

    /**
     * 自定义的函数列表
     */
    private static final Map<String, Method> customFun = new HashMap<>();

    static {
        setCustomFun(CustomFunc.class);
    }

    /**
     * 获取支持的计算符和函数名列表
     *
     * @return tag
     */
    public static Set<String> getTags() {

        Set<String> tag = new LinkedHashSet<>(BaseTags);
        // 自定义函数
        customFun.keySet().forEach(m -> tag.add(m.toUpperCase()));

        return tag;
    }

    /**
     * 设置默认保留精度
     *
     * @param scale 计算结果保留值精度
     */
    public static void setScale(int scale) {
        CalcUtil.scale = scale;
    }

    /**
     * 设置精度取整方式， 默认 四舍五入 RoundingMode.HALF_UP
     *
     * @param roundingMode 取整方式
     */
    public static void setRoundingMode(RoundingMode roundingMode) {
        CalcUtil.roundingMode = roundingMode;
    }

    /**
     * 设置自定义函数类
     * 自定义函数类 要求函数名与方法名相同，并且方法为 public static 修饰，返回值为 BigDecimal 类型。
     * 方法只能有一个参数，参数类型为 String （是传递过去的函数表达式）
     *
     * @param customFunctions 自定义函数类
     */
    public static void setCustomFun(Class<?>... customFunctions) {
        /* 自定义函数 */
        if (customFunctions != null) {
            Stream.of(customFunctions)
                    .filter(Objects::nonNull)
                    .forEach(aClass ->
                            Stream.of(aClass.getDeclaredMethods())
                                    .filter(i -> Modifier.isStatic(i.getModifiers())
                                            && Modifier.isPublic(i.getModifiers())
                                            && i.getReturnType() == BigDecimal.class
                                            && i.getParameterCount() == 1
                                            && i.getParameterTypes()[0] == String.class
                                    ).forEach(m -> customFun.put(m.getName().toLowerCase(), m)));
        }
    }

    /**
     * 执行计算
     *
     * @param equation 函数表达式字符串
     * @return 计算结果
     */
    public static BigDecimal calculate(String equation) {
        return calculate(equation, scale, roundingMode);
    }

    /**
     * 执行计算
     *
     * @param equation     函数表达式字符串
     * @param scale        计算结果精度
     * @param roundingMode 取整方式
     * @return 计算结果
     */
    public static BigDecimal calculate(String equation, int scale, RoundingMode roundingMode) {

        if (equation == null || equation.trim().isEmpty()) {
            throw new CalculatorException("请输入正确的计算表达式");
        }
        String temp = equation;
        while (temp.contains("(")) {
            int leftIndex = temp.lastIndexOf("(");
            String item = temp.substring(leftIndex);
            int rightIndex = item.indexOf(")");
            item = item.substring(0, rightIndex + 1);

            /* 获取括号左侧的函数名称，不是函数则返回空字符串 */
            String functionName = getLeft(temp, leftIndex);
            /* 更新左侧下标 和子表达式函数名称 */
            leftIndex -= functionName.length();
            /* 运算表达式 */
            item = operation(item, functionName);

            String leftStr = temp.substring(0, leftIndex);
            String rightStr = temp.substring(leftIndex + functionName.length() + rightIndex + 1);
            temp = leftStr + item + rightStr;
        }

        if (temp.contains("+") || temp.contains("-") || temp.contains("*") || temp.contains("/")) {
            temp = operation(temp, null);
        }

        return new BigDecimal(temp).setScale(scale, roundingMode);
    }

    /**
     * 运算
     *
     * @param equation     运算表达式
     * @param functionName 函数运算方法
     * @return 计算结果
     */
    public static String operation(String equation, String functionName) {
        if (functionName == null || functionName.isEmpty()) {
            return operation(equation);
        }

        if (!customFun.containsKey(functionName.toLowerCase())) {
            throw new CalculatorException(String.format("未找到函数：%s  表达式：%s%s", functionName, functionName, equation));
        }
        try {
            Method method = customFun.get(functionName.toLowerCase());
            Object result = method.invoke(null, equation);
            return result.toString();
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new CalculatorException(String.format("执行[%s]函数失败, 表达式：%s%s", functionName, functionName, equation), e);
        }
    }

    /**
     * 基础运算
     *
     * @param equation 运算表达式
     * @return 计算结果
     */
    public static String operation(String equation) {
        if (equation != null) {
            equation = splitBrackets(equation);

            /* 运算符 */
            while ((equation.contains("*") || equation.contains("/") || equation.contains("+")
                    || (equation.contains("-") && !equation.matches("^-?((\\d{1,})|(0{1}))(\\.\\d{1,})?$")))
                    && !equation.contains(",") && !equation.contains("<") && !equation.contains(">") && !equation.contains("=") && !equation.contains("!")
            ) {
                /* 获取运算符 */
                int index1 = equation.indexOf("+");
                int index2 = equation.indexOf("-");
                int index3 = equation.indexOf("*");
                int index4 = equation.indexOf("/");

                if (index3 != -1 && (index4 == -1 || index3 < index4)) {
                    /* 乘法运算 */
                    String leftVal = getLeft(equation, index3);
                    String rightVal = getRight(equation, index3);
                    BigDecimal number = new BigDecimal(leftVal.isEmpty() ? "0" : leftVal).multiply(new BigDecimal(rightVal));
                    equation = equation.substring(0, index3 - leftVal.length()) + number + equation.substring(index3 + rightVal.length() + 1);
                } else if (index4 != -1 && (index3 == -1 || index4 < index3)) {
                    /* 除法运算 */
                    String leftVal = getLeft(equation, index4);
                    String rightVal = getRight(equation, index4);
                    BigDecimal number = new BigDecimal(leftVal.isEmpty() ? "0" : leftVal).divide(new BigDecimal(rightVal), scale, roundingMode);
                    equation = equation.substring(0, index4 - leftVal.length()) + number + equation.substring(index4 + rightVal.length() + 1);
                } else if (index1 != -1 && (index2 < 1 || index1 < index2)) {
                    /*加法运算*/
                    String leftVal = getLeft(equation, index1);
                    String rightVal = getRight(equation, index1);
                    BigDecimal number = new BigDecimal(leftVal.isEmpty() ? "0" : leftVal).add(new BigDecimal(rightVal));
                    equation = equation.substring(0, index1 - leftVal.length()) + number + equation.substring(index1 + rightVal.length() + 1);
                } else if (index2 != -1 && (index1 == -1 || index2 < index1)) {
                    /*减法运算*/
                    String leftVal = getLeft(equation, index2);
                    String rightVal = getRight(equation, index2);
                    BigDecimal number = new BigDecimal(leftVal.isEmpty() ? "0" : leftVal).subtract(new BigDecimal(rightVal));
                    equation = equation.substring(0, index2 - leftVal.length()) + number + equation.substring(index2 + rightVal.length() + 1);
                }
            }
        }
        return equation;
    }

    /**
     * 获取指定下标左侧的函数名称 或者 值
     *
     * @param equation  函数表达式字符串
     * @param leftIndex 指定的下标
     * @return 函数名称 或者 值，不是函数或者没有值则返回空字符串
     */
    public static String getLeft(String equation, int leftIndex) {
        StringBuilder tag = new StringBuilder();
        while (leftIndex > 0) {
            String left = String.valueOf(equation.charAt(--leftIndex));
            if (BaseTags.contains(left) && leftIndex != 0) {
                if (!"-".equals(left) || !BaseTags.contains(String.valueOf(equation.charAt(leftIndex + 1)))) {
                    break;
                }
            }
            tag.append(left);
        }
        return tag.reverse().toString();
    }

    /**
     * 获取指定下标右侧的 值
     *
     * @param equation   函数表达式字符串
     * @param rightIndex 指定的下标
     * @return 值，没有值则返回空字符串
     */
    public static String getRight(String equation, int rightIndex) {
        StringBuilder tag = new StringBuilder();
        while (rightIndex < equation.length() - 1) {
            String right = String.valueOf(equation.charAt(++rightIndex));
            if (BaseTags.contains(right)) {
                if (!"-".equals(right) || (rightIndex < equation.length() - 1
                        && !BaseTags.contains(String.valueOf(equation.charAt(rightIndex - 1))))) {
                    break;
                }
            }
            tag.append(right);
        }
        return tag.toString();
    }

    /**
     * 切割字符串前后 ( )
     *
     * @param equation 函数表达式
     * @return 计算结果
     */
    public static String splitBrackets(String equation) {
        if (equation != null) {
            if (equation.startsWith("(")) {
                equation = equation.substring(1);
            }
            if (equation.endsWith(")")) {
                equation = equation.substring(0, equation.length() - 1);
            }
        }
        return equation;
    }

    /**
     * 判断boolean 表达式结果
     *
     * @param logical 逻辑表达式
     * @return 结果值
     */
    public static boolean isTrue(String logical) {
        if (logical.contains("==")) {
            String[] test = logical.split("==");
            return test[0].equals(test[1]);
        } else if (logical.contains("true") || logical.contains("false")) {
            return Boolean.parseBoolean(logical);
        } else if (logical.contains("<>") || logical.contains("><") || logical.contains("!=")) {
            String[] test = logical.split(logical.contains("<>") ? "<>" : logical.contains("><") ? "><" : "!=");
            return !test[0].equals(test[1]);
        } else if (logical.contains("<")) {
            String[] test = logical.split("<");
            return new BigDecimal(test[0]).compareTo(new BigDecimal(test[1])) < 0;
        } else if (logical.contains(">")) {
            String[] test = logical.split(">");
            return new BigDecimal(test[0]).compareTo(new BigDecimal(test[1])) > 0;

        } else if (logical.equals("1")) {
            return true;
        } else if (logical.equals("0")) {
            return false;
        }
        return false;
    }


    /**
     * 默认函数实现函数
     */
    private final static class CustomFunc {

        /**
         * if函数支持  三元表达式  Excel使用方式
         *
         * @param equation 函数表达式
         * @return true 返回1  false返回0
         */
        public static BigDecimal IF(String equation) {
            if (equation == null) {
                throw new CalculatorException.FunctionRun("参数错误：null");
            }

            if (equation.startsWith("IF")) {
                equation = equation.substring(2);
            }
            equation = splitBrackets(equation);

            String[] equationArr = equation.split(",");
            String val = equationArr[0];
            String isTrue = equationArr.length > 1 ? equationArr[1] : "0";
            String isFalse = equationArr.length > 2 ? equationArr[2] : "0";

            return isTrue(val) ? new BigDecimal(CalcUtil.operation(isTrue)) : new BigDecimal(CalcUtil.operation(isFalse));
        }

        /**
         * 条件或
         *
         * @param equation 函数表达式
         * @return true 返回1  false返回0
         */
        public static BigDecimal or(String equation) {
            if (equation == null) {
                throw new CalculatorException.FunctionRun("参数错误：null");
            }
            if (equation.startsWith("or")) {
                equation = equation.substring(2);
            }
            equation = splitBrackets(equation);

            String[] equationArr = equation.split(",");

            return Stream.of(equationArr).anyMatch(CalcUtil::isTrue) ? BigDecimal.ONE : BigDecimal.ZERO;
        }

        /**
         * 条件且
         *
         * @param equation 函数表达式
         * @return true 返回1  false返回0
         */
        public static BigDecimal and(String equation) {
            if (equation == null) {
                throw new CalculatorException.FunctionRun("参数错误：null");
            }
            if (equation.startsWith("and")) {
                equation = equation.substring(4);
            }
            equation = splitBrackets(equation);

            String[] equationArr = equation.split(",");

            return Stream.of(equationArr).allMatch(CalcUtil::isTrue) ? BigDecimal.ONE : BigDecimal.ZERO;
        }

        /**
         * 取整  四舍五入
         *
         * @param equation     函数表达式
         * @param roundingMode 取整方式
         * @return 计算结果
         */
        public static BigDecimal round(String equation, RoundingMode roundingMode) {
            if (equation == null) {
                throw new CalculatorException.FunctionRun("参数错误：null");
            }

            String[] equationArr = equation.split(",");
            String val = equationArr[0];
            val = CalcUtil.operation(val);
            String scale = equationArr.length > 1 ? equationArr[1] : CalcUtil.scale.toString();
            scale = CalcUtil.operation(scale);
            if (!val.matches("^-?((\\d{1,})|(0{1}))(\\.\\d{1,})?$") || !scale.matches("^(\\d{1})$")) {
                throw new CalculatorException.FunctionRun("参数错误：" + equation);
            }

            return new BigDecimal(val).setScale(Integer.parseInt(scale), roundingMode);
        }

        /**
         * 取整  四舍五入
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal round(String equation) {
            if (equation != null && equation.startsWith("round")) {
                equation = equation.substring(5);
            }
            equation = splitBrackets(equation);

            return round(equation, RoundingMode.HALF_UP);
        }

        /**
         * 取整  向上
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal roundup(String equation) {
            if (equation != null && equation.startsWith("roundup")) {
                equation = equation.substring(7);
            }
            equation = splitBrackets(equation);

            return round(equation, RoundingMode.UP);
        }

        /**
         * 取整  向下
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal roundDown(String equation) {
            if (equation != null && equation.startsWith("roundDown")) {
                equation = equation.substring(9);
            }
            equation = splitBrackets(equation);

            return round(equation, RoundingMode.DOWN);
        }

        /**
         * 求和
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal sum(String equation) {
            if (equation != null) {
                if (equation.startsWith("sum")) {
                    equation = equation.substring(4);
                }
                equation = splitBrackets(equation);

                return Stream.of(equation.split(",")).map(i -> new BigDecimal(CalcUtil.operation(i))).reduce(BigDecimal.ZERO, BigDecimal::add);
            }
            return BigDecimal.ZERO;
        }

        /**
         * 求平均值
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal avg(String equation) {
            if (equation != null) {
                if (equation.startsWith("max")) {
                    equation = equation.substring(4);
                }
                equation = splitBrackets(equation);
                return BigDecimal.valueOf(Stream.of(equation.split(",")).mapToDouble(i -> new BigDecimal(CalcUtil.operation(i)).doubleValue()).average().orElse(0.0));
            }
            return BigDecimal.ZERO;
        }

        /**
         * 最大值
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal max(String equation) {
            if (equation != null) {
                if (equation.startsWith("max")) {
                    equation = equation.substring(4);
                }
                equation = splitBrackets(equation);

                return Stream.of(equation.split(",")).map(i -> new BigDecimal(CalcUtil.operation(i))).max(Comparator.comparing(BigDecimal::doubleValue)).orElse(BigDecimal.ZERO);
            }
            return BigDecimal.ZERO;
        }

        /**
         * min
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal min(String equation) {
            if (equation != null) {
                if (equation.startsWith("min")) {
                    equation = equation.substring(4);
                }
                equation = splitBrackets(equation);

                return Stream.of(equation.split(",")).map(i -> new BigDecimal(CalcUtil.operation(i))).min(Comparator.comparing(BigDecimal::doubleValue)).orElse(BigDecimal.ZERO);
            }
            return BigDecimal.ZERO;
        }

        /**
         * count
         *
         * @param equation 函数表达式
         * @return 计算结果
         */
        public static BigDecimal count(String equation) {
            if (equation != null) {
                if (equation.startsWith("count")) {
                    equation = equation.substring(5);
                }
                equation = CalcUtil.operation(equation);
                return new BigDecimal(equation.split(",").length);
            }
            return BigDecimal.ZERO;
        }
    }

    public static void main(String[] args) {
        String equation = "if(or(24==24,89>100),roundDown(9+avg(3,1,sum(1,3+8,9*8)-sum(2,4),7*3)+8/2,2),round(-265/23,2))+sum(1,3+8,9*8)";

        System.out.println("计算表达式的结果值：" + equation);
        BigDecimal result = CalcUtil.calculate(equation);
        System.out.println("计算结果：" + result);

        List<String> equations = new ArrayList<>();
        equation = "if(or(%s==%s,%s>%s),roundDown(%s+avg(%s,1,sum(1,3+%s,9*%s)-sum(2,%s),7*%s)+%s/2,2),round(-%s/%s,2))+sum(1,3+8,%s*8)";
        for (int i = 0; i < 100000; i++) {
            //计算值
            equation = String.format(equation, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6, Math.random() * 125.6);
            equations.add(equation);
            result = calculate(equation);
        }
        long millis = System.currentTimeMillis();
        equations.forEach(equationItem -> {
            BigDecimal resultItem = calculate(equationItem);
        });

        System.out.printf("计算%s个表达式，总耗时:%s ms\n", equations.size(), (System.currentTimeMillis() - millis));
    }
}
