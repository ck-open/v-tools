package com.ck.tools.utils;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * 文件操作类
 *
 * @author cyk
 * @since 2020-01-01
 */
public class FileUtil {
    private static final Logger log = Logger.getLogger(FileUtil.class.getName());

    public static void main(String[] args) {
        String path = "E:\\test\\2699.zip";
        System.out.println(zip(Paths.get("E:\\test\\2699")));
        System.out.println(zipUnpack(Paths.get(path), Paths.get("E:\\test\\zipUnpack")));
        ;

        copy("D:\\Drivers", "D:\\Driver", 10);
    }

    /**
     * 重置路径分隔符 为 File.separator
     *
     * @param path 原路径
     * @return 重置后的路径
     */
    public static String resetSeparator(String path) {
        if (path == null) {
            return null;
        }
        return path.replace("/", File.separator).replace("\\", File.separator);
    }

    /**
     * 将文件二进制数据转为目录结构
     *
     * @param sourceData key:文件地址+文件名及类型后缀(未指定后缀将按照目录处理)  value：文件二进制数据
     * @return 解析后的目录结构树  key:目录名或文件名 value:子集目录结构 或 文件二进制数据
     */
    public static Map<String, Object> parsePathToDirTree(Map<String, byte[]> sourceData) {
        Map<String, Object> sourcePaths = new LinkedHashMap<>();
        if (sourceData == null) {
            return sourcePaths;
        }
        sourceData.keySet().forEach(path -> {
            // 构建路径目录结构
            path = resetSeparator(path);
            Iterator<String> iterator = Stream.of(path.split(path.contains("\\") ? "\\\\" : "/")).iterator();
            Map<String, Object> parentDir = sourcePaths;
            while (iterator.hasNext()) {
                String pathTemp = iterator.next();
                if (!parentDir.containsKey(pathTemp) && (iterator.hasNext() || !pathTemp.contains("."))) {
                    Map<String, Object> dir = new LinkedHashMap<>();
                    parentDir.put(pathTemp, dir);
                    parentDir = dir;
                } else if (!pathTemp.contains(".")) {
                    parentDir = (Map<String, Object>) parentDir.get(pathTemp);
                }

                // 存储文件数据到对应节点
                if (!iterator.hasNext() && pathTemp.contains(".")) {
                    parentDir.put(pathTemp, sourceData.get(path));
                }
            }
        });
        return sourcePaths;
    }

    /**
     * 数据传输
     *
     * @param inputStream  输入流
     * @param outputStream 输出流
     * @param mbps         传输速度
     * @return boolean 传输完成返回true 失败或异常返回 false
     */
    public static boolean io(InputStream inputStream, OutputStream outputStream, int mbps) {
        try {
            mbps = mbps <= 0 ? 1 : Math.min(mbps, 100);

            byte[] buffer = new byte[1024 * mbps];
            int len = inputStream.read(buffer);
            while (len != -1) {
                outputStream.write(buffer, 0, len);
                outputStream.flush();
                len = inputStream.read(buffer);
            }
            return true;
        } catch (IOException e) {
            log.log(Level.WARNING, "文件数据传输失败！", e);
        }
        return false;
    }

    /**
     * 数据传输
     *
     * @param bytes  二进制数据
     * @param output 输出流
     * @param mbps   传输速度
     * @return boolean 传输完成返回true 失败或异常返回 false
     */
    public static boolean io(byte[] bytes, OutputStream output, int mbps) {
        try {
            mbps = mbps <= 0 ? 1 : Math.min(mbps, 100);
            if (bytes != null) {
                int len = 1024 * mbps;
                for (int i = 0; i < bytes.length; i = i + len) {
                    if (i + len > bytes.length)
                        len = bytes.length - i;
                    output.write(bytes, i, len);
                    output.flush();
                }
            }
            output.flush();
            return true;
        } catch (IOException e) {
            log.log(Level.WARNING, "文件数据传输失败！", e);
        }
        return false;
    }

    /**
     * 新建目录
     *
     * @param folderPath String 如 c:/fqf
     * @return boolean 已存在或成功返回true
     */
    public static boolean newFolder(String folderPath) {
        try {
            File myFilePath = new File(folderPath);
            if (!myFilePath.exists()) {
                return myFilePath.mkdir();
            }
            return true;
        } catch (Exception e) {
            log.warning(String.format("新建目录操作出错:%s  %s", folderPath, e.getMessage()));
        }
        return false;
    }

    /**
     * 创建文件
     *
     * @param file File 文件对象
     * @return 已存在或成功返回true
     */
    public static boolean newFile(File file) {
        try {
            if (file == null) {
                return false;
            }
            if (!file.getParentFile().exists()) {
                if (!file.getParentFile().mkdirs()) {
                    return false;
                }
            }
            if (!file.exists()) {
                return file.createNewFile();
            }
            return true;
        } catch (IOException e) {
            log.warning(String.format("新建文件操作出错:%s  %s", file.getName(), e.getMessage()));
        }
        return false;
    }

    /**
     * 删除文件
     *
     * @return boolean 文件已不存在或成功返回true
     */
    public static boolean delFile(String fileName) {
        return fileName != null && delFile(new File(fileName));
    }

    /**
     * 删除文件
     *
     * @return boolean 文件已不存在或成功返回true
     */
    public static boolean delFile(File file) {
        if (file != null) {
            if (!file.exists()) {
                return true;
            }
            if (file.isDirectory()) {
                return delFolder(file);
            } else if (file.isFile()) {
                return file.delete();
            }
        }
        return false;
    }

    /**
     * 删除文件夹及其子目录文件夹
     *
     * @return boolean 文件目录树全部删除返回true  否则可能剩余无法删除部分文件或目录
     */
    public static boolean delFolder(String folderPath) {
        return folderPath != null && delFolder(new File(folderPath));
    }

    /**
     * 删除文件夹及其子目录文件夹
     *
     * @return boolean 文件目录树全部删除返回true  否则可能剩余无法删除部分文件或目录
     */
    public static boolean delFolder(File file) {
        if (file == null || !file.isDirectory()) {
            return false;
        }
        Path path = Paths.get(file.toURI());
        AtomicBoolean result = new AtomicBoolean(true);
        try (Stream<Path> walk = Files.walk(path)) {
            walk.sorted(Comparator.reverseOrder()).forEach(i -> {
                try {
                    Files.deleteIfExists(i);
                } catch (IOException e) {
                    if (result.get()) {
                        result.set(false);
                    }
                }
            });
            return file.delete() && result.get(); /* 删除空文件夹*/
        } catch (Exception e) {
            log.warning(String.format("删除文件夹操作出错:%s  %s", file.getName(), e.getMessage()));
        }
        return false;
    }

    /**
     * 遍历文件夹目录及文件
     *
     * @param source   Path 文件路径 如：c:/fqf
     * @param consumer Consumer<Stream<Path>> 内容遍历方法
     */
    public static void forFolder(Path source, Consumer<Stream<Path>> consumer) {
        if (source == null || !Files.exists(source) || !Files.isDirectory(source)) {
            return;
        }
        consumer.accept(forFolder(source));
    }

    /**
     * 遍历文件夹目录及文件
     *
     * @param source Path 文件路径 如：c:/fqf
     * @return Stream<Path> 文件路径列表流
     */
    public static Stream<Path> forFolder(Path source) {
        if (source == null || !Files.exists(source) || !Files.isDirectory(source)) {
            return Stream.empty();
        }

        try (Stream<Path> walk = Files.walk(source)) {
            return walk.collect(Collectors.toList()).stream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 复制单个文件
     *
     * @param source String 原文件路径 如：c:/fqf.txt
     * @param target String 复制后路径 如：f:/fqf.txt
     * @param mbps   传输速率，单位M 默认1M 最大100M
     * @return 复制成功返回true  失败或异常 返回false
     */
    public static boolean copyFile(Path source, Path target, int mbps) {
        if (source == null || target == null || !Files.exists(source) || Files.isDirectory(source)) {
            return false;
        }

        if (!newFile(new File(target.toUri()))) {
            log.warning(String.format("复制单个文件操作,在创建新文件时出错:%s 到 %s", source, target));
            return false;
        }

        try (
                InputStream inStream = Files.newInputStream(source); /* 读入原文件*/
                OutputStream os = Files.newOutputStream(target);
        ) {
            return io(inStream, os, mbps);
        } catch (Exception e) {
            log.warning(String.format("复制单个文件操作出错:%s 到 %s  %s", source, target, e.getMessage()));
        }
        return false;
    }

    /**
     * 复制整个文件夹内容
     *
     * @param source String 原文件路径 如：c:/fqf
     * @param target String 复制后路径 如：f:/fqf/ff
     * @param mbps   传输速率，单位M 默认1M 最大100M
     * @return boolean 复制完成返回true  失败或异常 返回false
     */
    public static boolean copyFolder(Path source, Path target, int mbps) {
        if (source == null || target == null || !Files.exists(source)) {
            return false;
        }
        /* 原文件目录前缀 */
        String sourcePrefixPath = source.toString();

        try (Stream<Path> walk = Files.walk(source)) {

            if (!Files.exists(target)) {
                Files.createDirectories(target);
            }

            /* walk 流不能被多此消费，因此需要先将流转换为集合 */
            Set<Path> pathSet = walk.collect(Collectors.toSet());

            /* 构建所有文件夹目录结构树 */
            pathSet.stream().sorted(Comparator.reverseOrder()).filter(Files::isDirectory).forEach(path -> {
                String targetPath = path.toString().replace(sourcePrefixPath, target.toString());
                if (!newFolder(targetPath)) {
                    throw new RuntimeException("文件目录创建失败，path:" + targetPath);
                }
            });

            /* 将所有文件复制到目标目录 多线程分批次处理以提升速度 */
            Set<Path> filePaths = pathSet.stream().sorted(Comparator.reverseOrder())
                    .filter(i -> Files.isRegularFile(i) || !Files.isDirectory(i))  /* 是常规文件  或者 不是目录 */
                    .collect(Collectors.toCollection(LinkedHashSet::new));

            if (!filePaths.isEmpty()) {
                CompletableFuture<Void> completableFuture = ThreadPoolUtil.runBatchTask("ThreadPool-FileCopy", 8, filePaths,
                        (integer, path) -> {
                            Path targetPath = Paths.get(path.toString().replace(sourcePrefixPath, target.toString()));

                            if (!newFile(targetPath.toFile())) {
                                throw new RuntimeException("文件创建失败，path:" + targetPath);
                            }

                            try (InputStream inStream = Files.newInputStream(path);
                                 OutputStream os = Files.newOutputStream(targetPath)
                            ) {
                                if (!io(inStream, os, mbps)) {
                                    throw new IOException("文件复制失败，path:" + targetPath);
                                }
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        });

                if (completableFuture != null) {
                    completableFuture.get();
                    return true;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            log.warning(String.format("复制文件操作出错，源文件:%s 目标:%s Error:%s", source, target, e.getMessage()));
        }

        return false;
    }

    /**
     * 复制整个文件夹内容
     *
     * @param source String 原文件路径 如：c:/fqf
     * @param target String 复制后路径 如：f:/fqf/ff
     * @param mbps   传输速率，单位M 默认1M 最大100M
     * @return boolean 复制完成返回true  失败或异常 返回false
     */
    public static boolean copy(String source, String target, int mbps) {
        return copy(Paths.get(source), Paths.get(target), mbps);
    }

    /**
     * 复制整个文件夹内容
     *
     * @param source String 原文件路径 如：c:/fqf
     * @param target String 复制后路径 如：f:/fqf/ff
     * @param mbps   传输速率，单位M 默认1M 最大100M
     * @return boolean 复制完成返回true  失败或异常 返回false
     */
    public static boolean copy(Path source, Path target, int mbps) {
        if (Files.isDirectory(source)) {
            return copyFolder(source, target, mbps);
        } else {
            return copyFile(source, target, mbps);
        }
    }

    /**
     * 将文件保存到本地    保存位置在项目路径+自定义路径
     *
     * @param data        接收的文件数据 MultipartFile.getBytes()
     * @param contentType 文件类型 MultipartFile.getContentType()
     * @param fileName    文件名 MultipartFile.getOriginalFilename()
     * @param filePath    自定义保存位置
     * @return "uuid+文件名";  返回null 则表示操作失败
     */
    public static String saveMultipartFile(byte[] data, String contentType, String fileName, String filePath) {
        /*保存的文件名  uuid-fileName*/
        String name = UUID.randomUUID().toString().replace("-", "") + fileName;
        if (!saveFile(data, filePath, name)) {
            log.info("fileName:" + fileName + "  getContentType:" + contentType + "  filePath:" + filePath);
            return null;
        }
        return name;
    }

    /**
     * 将文件保存到本地
     *
     * @param file     要保存的文件
     * @param filePath 保存的地址(不存在则创建)
     * @param fileName 文件名
     */
    public static boolean saveFile(byte[] file, String filePath, String fileName) {
        File targetFile = new File(filePath + File.separator + fileName);
        /*判断文件是否存在，不存在则创建*/
        if (!newFile(targetFile)) {
            throw new RuntimeException("文件保存到本地操作时创建文件出错:" + targetFile.getName());
        }
        try (FileOutputStream out = new FileOutputStream(targetFile);) {
            io(file, out, 2);
            return true;
        } catch (IOException e) {
            log.warning(String.format("文件保存到本地操作出错:%s   %s   %s", filePath, fileName, e.getMessage()));
        }
        return false;
    }

    /**
     * 判断文件是否占用
     *
     * @param file 文件
     * @return 占用时返回 true
     */
    public static boolean isFileLocked(File file) {
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
             FileChannel fileChannel = randomAccessFile.getChannel()) {
            FileLock fileLock = fileChannel.tryLock();
            if (fileLock != null) {
                fileLock.release();
                return false;
            }
        } catch (IOException ignored) {
        }
        return true;
    }

    /**
     * 读取文件到字符串
     *
     * @param file 文件
     * @return 文件内容字符串
     */
    public static String readTextFile(File file) {
        try {
            return readTextFile(new FileInputStream(file), StandardCharsets.UTF_8, 2);
        } catch (FileNotFoundException e) {
            log.log(Level.WARNING, "文件读取异常", e);
        }
        return null;
    }

    /**
     * 读取文件到字符串
     *
     * @param inputStream 目标文件
     * @param charset     字符集
     * @param mbps        传输速率，单位M 默认1M 最大100M
     * @return 文件内容字符串
     * @see StandardCharsets
     */
    public static String readTextFile(InputStream inputStream, Charset charset, int mbps) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {
            if (!io(inputStream, outputStream, mbps)) {
                throw new RuntimeException("文件读取异常");
            }
            if (charset == null) {
                charset = StandardCharsets.UTF_8;
            }
            return outputStream.toString(charset.name());
        } catch (IOException e) {
            log.log(Level.WARNING, String.format("读取文件到字符串操作出错:%s   %s   %s", inputStream, charset, e.getMessage()), e);
        }
        return null;
    }

    /**
     * 读取Text文件文本 按行读取
     *
     * @param filePath 文件路径
     * @param dispose  处理函数
     */
    public static void readTextFileLines(String filePath, BiConsumer<String, String> dispose) {
        if (filePath == null || filePath.trim().isEmpty()) {
            return;
        }
        readTextFileLines(new File(filePath), null, dispose);
    }

    /**
     * 读取Text文件文本  按行读取
     *
     * @param file    文件路径
     * @param charset 文件字符串字符集
     * @param dispose 处理函数
     */
    public static void readTextFileLines(File file, Charset charset, BiConsumer<String, String> dispose) {
        if (file == null || !file.isFile() || dispose == null) {
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(file));) {
            String lastRow = null;
            String readRow;
            while ((readRow = reader.readLine()) != null) {

                if (charset != null)
                    readRow = new String(readRow.getBytes(), charset);

                dispose.accept(lastRow, readRow);
                lastRow = readRow;
            }
        } catch (IOException e) {
            log.log(Level.WARNING, "读取文件内容出错", e);
        }
    }


    /**
     * 将文本写出到文件，文件不存在则创建
     * 写入文件的字符集 UTF-8
     * 写入时将覆盖文件
     *
     * @param fileName 文件储存路径及文件名
     * @param date     需要写入到文件的内容
     * @return 写入成功时返回true，否则返回false
     */
    public static boolean writerTextFile(String fileName, String date) {
        return writerTextFile(new File(fileName), date, false, StandardCharsets.UTF_8);
    }

    /**
     * 将文本写出到文件，文件不存在则创建
     * 写入文件的字符集 UTF-8
     *
     * @param filePathAndName 文件储存路径及文件名
     * @param date            需要写入到文件的内容
     * @param append          是否写入到文件末尾，false将覆盖文件
     * @return 写入成功时返回true，否则返回false
     */
    public static boolean writerTextFile(String filePathAndName, String date, boolean append) {
        return writerTextFile(new File(filePathAndName), date, append, StandardCharsets.UTF_8);
    }

    /**
     * 将文本写出到文件，文件不存在则创建
     *
     * @param file    文件储存路径及文件名
     * @param date    需要写入到文件的内容
     * @param append  是否写入到文件末尾，false将覆盖文件
     * @param charset 写入文件的字符集，null则为系统默认
     * @return 写入成功时返回true，否则返回false
     * @see StandardCharsets
     */
    public static boolean writerTextFile(File file, String date, boolean append, Charset charset) {

        if (!newFile(file)) {
            throw new RuntimeException("文件写入操作出错:" + file.getName());
        }

        if (charset == null) {
            charset = StandardCharsets.UTF_8;
        }

        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, append), charset)))) {
            printWriter.println(date);
            printWriter.flush();
            return true;
        } catch (Exception e) {
            log.log(Level.WARNING, "文件写入操作出错:" + file.getName(), e);
        }
        return false;
    }

    /**
     * 文档导出到本地文件
     *
     * @param data     文档二进制
     * @param filePath 本地路径
     * @param fileName 文件名称
     */
    public static void download(ByteArrayOutputStream data, String filePath, String fileName) {

        if (fileName == null || fileName.trim().isEmpty()) {
            fileName = "download";
        }

        File file = getFile(filePath, fileName);
        if (file != null) {
            try (FileOutputStream out = new FileOutputStream(file);) {
                if (!io(data.toByteArray(), out, 2)) {
                    throw new RemoteException("文档导出失败:" + file.getName());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 获取路径下指定文件，不存在则创建
     *
     * @param filePath 文件路径
     * @param fileName 文件名称
     * @return 文件对象 不存在且无法创建则返回null
     */
    public static File getFile(String filePath, String fileName) {
        File file = new File(filePath + File.separator + fileName);
        if (!newFile(file)) {
            return null;
        }
        return file;
    }

    /**
     * 获取路径文件夹,不存在则创建
     *
     * @param filePath 文件路径
     * @return 文件夹对象 不存在且无法创建则返回null
     */
    public static File getFolder(String filePath, String... pathItems) {
        if (filePath == null) {
            return null;
        }
        if (filePath.endsWith(File.separator) || filePath.endsWith("/")) {
            filePath = filePath.substring(0, filePath.length() - 1);
        }

        if (pathItems != null) {
            StringBuilder filePathBuilder = new StringBuilder(filePath);
            for (String pathItem : pathItems) {
                filePathBuilder.append(File.separator).append(pathItem);
            }
            filePath = filePathBuilder.toString();
        }

        if (filePath.trim().isEmpty() || !newFolder(filePath)) {
            return null;
        }
        return new File(filePath);
    }

    /**
     * 压缩文件夹.zip
     *
     * @param source 需要压缩的文件夹
     */
    public static boolean zip(Path source) {
        if (source == null) {
            return false;
        }
        String target = source.getParent() + File.separator + source.getFileName() + ".zip";
        return zip(source, Paths.get(target));
    }

    /**
     * 压缩文件夹.zip
     *
     * @param source 需要压缩的文件夹
     * @param target 压缩文件路径
     * @return 成功返回true，失败或异常返回false
     */
    public static boolean zip(Path source, Path target) {
        if (source == null || target == null) {
            return false;
        }

        try (OutputStream outputStream = Files.newOutputStream(target);) {
            return zip(source, outputStream, true, null);
        } catch (IOException e) {
            log.log(Level.WARNING, "压缩文件出错", e);
            return false;
        }
    }

    /**
     * 压缩文件夹.zip
     *
     * @param source           需要压缩的文件夹
     * @param outputStream     压缩文件输出流
     * @param includeParentDir 压缩文件中是否包含父级目录 true则压缩后文件中包含父级目录
     * @param zipEntryConsumer 压缩文件中内容自定义操作函数，所有内容文件或文件夹都将被此函数处理
     * @return 成功返回true，失败或异常返回false
     */
    public static boolean zip(Path source, OutputStream outputStream, boolean includeParentDir, Consumer<ZipEntry> zipEntryConsumer) {
        if (source == null || outputStream == null) {
            return false;
        }

        try {

            List<Path> sourcePaths = null;
            if (!Files.isDirectory(source)) {
                /* 不是目录则只压缩当前文件 */
                sourcePaths = Collections.singletonList(source);
            } else {
                /*获取目录结构*/
                sourcePaths = forFolder(source).collect(Collectors.toList());
            }

            try (ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);) {

                sourcePaths.stream().sorted(Comparator.reverseOrder())
                        .forEach(path -> {
                            try {
                                /* 去掉路径中的原始父级路径 */
                                String zipEntryName = path.toString().replace(includeParentDir ? source.getParent().toString() : source.toString(), "");

                                if (zipEntryName.startsWith(File.separator)) {
                                    zipEntryName = zipEntryName.substring(1);
                                }

                                /* 截取后名称为空  则跳过当前目录 */
                                if (zipEntryName.trim().isEmpty()) {
                                    return;
                                }

                                boolean isDirectory = Files.isDirectory(path);
                                if (isDirectory) {
                                    zipEntryName += File.separator;
                                }
                                ZipEntry zipEntry = new ZipEntry(zipEntryName);

                                /* 执行自定义操作 */
                                if (zipEntryConsumer != null) {
                                    zipEntryConsumer.accept(zipEntry);
                                }

                                zipOutputStream.putNextEntry(zipEntry);

                                /* 是常规文件  或者 不是目录 写入文件 */
                                if (!isDirectory) {
                                    /* 写文件 */
                                    try (InputStream inputStream = Files.newInputStream(path);) {
                                        if (!io(inputStream, zipOutputStream, 1)) {
                                            throw new RuntimeException(String.format("压缩文件[%s]时失败", path));
                                        }
                                    }
                                }

                                // 关闭当前的 ZipEntry
                                zipOutputStream.closeEntry();
                            } catch (IOException e) {
                                throw new RuntimeException(String.format("压缩文件[%s]时失败", path), e);
                            }
                        });
            }
            return true;
        } catch (Exception e) {
            log.log(Level.WARNING, "压缩文件失败:" + source, e);
        }
        return false;
    }


    /**
     * 压缩文件夹.zip
     *
     * @param sourceData       需要压缩的文件数据
     * @param outputStream     压缩文件输出流
     * @param zipEntryConsumer 压缩文件中内容自定义操作函数，所有内容文件或文件夹都将被此函数处理
     * @return 成功返回true，失败或异常返回false
     */
    public static boolean zip(Map<String, byte[]> sourceData, OutputStream outputStream, Consumer<ZipEntry> zipEntryConsumer) {
        if (sourceData == null || sourceData.isEmpty() || outputStream == null) {
            return false;
        }

        try {
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);) {
                sourceData.forEach((path, data) -> {
                    try {
                        if (path.startsWith(File.separator)) {
                            path = path.substring(1);
                        }

                        /* 截取后名称为空  则跳过当前目录 */
                        if (path.trim().isEmpty()) {
                            return;
                        }

                        ZipEntry zipEntry = new ZipEntry(path);

                        /* 执行自定义操作 */
                        if (zipEntryConsumer != null) {
                            zipEntryConsumer.accept(zipEntry);
                        }

                        zipOutputStream.putNextEntry(zipEntry);

                        /* 是常规文件  或者 不是目录 写入文件 */
                        if (data != null && data.length > 0) {
                            /* 写文件 */
                            if (!io(data, zipOutputStream, 1)) {
                                throw new RuntimeException(String.format("压缩文件[%s]时失败", path));
                            }
                        }

                        // 关闭当前的 ZipEntry
                        zipOutputStream.closeEntry();
                    } catch (IOException e) {
                        throw new RuntimeException(String.format("压缩文件[%s]时失败", path), e);
                    }
                });
            }
            return true;
        } catch (Exception e) {
            log.log(Level.WARNING, "压缩文件失败:", e);
        }
        return false;
    }

    /**
     * 解压缩.zip
     * 字符集  默认 GBK
     *
     * @param zipPath 压缩文件地址
     * @return 解压成功返回true，解压失败或异常返回false
     */
    public static boolean zipUnpack(Path zipPath) {
        return zipUnpack(zipPath, zipPath.getParent(), null, false, null);
    }

    /**
     * 解压缩.zip
     * 字符集  默认 GBK
     *
     * @param zipPath    压缩文件地址
     * @param unpackPath 解压文件储存地址
     * @return 解压成功返回true，解压失败或异常返回false
     */
    public static boolean zipUnpack(Path zipPath, Path unpackPath) {
        return zipUnpack(zipPath, unpackPath, null, false, null);
    }

    /**
     * 解压缩.zip
     *
     * @param zipPath          压缩文件地址
     * @param unpackPath       解压文件储存地址
     * @param charset          字符集  默认 GBK
     * @param isCurrentDir     解压到当前目录，false 时压缩文件名将作为内容的根目录
     * @param zipEntryConsumer 压缩文件中内容自定义操作函数，所有内容文件或文件夹都将被此函数处理
     * @return 解压成功返回true，解压失败或异常返回false
     */
    public static boolean zipUnpack(Path zipPath, Path unpackPath, Charset charset, boolean isCurrentDir, Consumer<ZipEntry> zipEntryConsumer) {
        if (zipPath == null || unpackPath == null) {
            return false;
        }
        if (!Files.exists(zipPath)) {
            log.log(Level.WARNING, "压缩文件不存在:" + zipPath);
            return false;
        }

        if (newFolder(unpackPath.toString()) && !Files.isDirectory(unpackPath)) {
            log.log(Level.WARNING, String.format("解压地址不是目录，压缩文件:%s 解压地址:%s", zipPath, unpackPath));
            return false;
        }

        if (charset == null) {
            charset = Charset.forName("GBK");
        }

        if (!isCurrentDir) {
            unpackPath = Paths.get(unpackPath + File.separator + zipPath.getFileName().toString().substring(0, zipPath.getFileName().toString().lastIndexOf('.')));
        }

        try (ZipFile zipFile = new ZipFile(zipPath.toFile(), charset);) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry zipEntry = entries.nextElement();
                if (zipEntryConsumer != null) {
                    zipEntryConsumer.accept(zipEntry);
                }

                log.info("解压缩文件==> " + zipEntry.getName());

                if (zipEntry.isDirectory() || zipEntry.getName().endsWith(File.separator)) {
                    if (!newFile(getFolder(unpackPath.toString(), zipEntry.getName()))) {
                        throw new RuntimeException(String.format("解压文件夹[%s]失败", zipEntry.getName()));
                    }
                } else {
                    File file = getFile(unpackPath.toString(), zipEntry.getName());
                    if (!newFile(file)) {
                        throw new RuntimeException(String.format("解压文件[%s]失败", zipEntry.getName()));
                    } else if (file != null) {
                        /* 写文件 */
                        try (OutputStream outputStream = Files.newOutputStream(file.toPath());) {
                            if (!io(zipFile.getInputStream(zipEntry), outputStream, 2)) {
                                throw new RuntimeException(String.format("解压文件[%s]失败", zipEntry.getName()));
                            }
                        }
                    } else {
                        throw new RuntimeException(String.format("解压文件[%s]失败", zipEntry.getName()));
                    }
                }
            }

            return true;
        } catch (IOException e) {
            log.log(Level.WARNING, "解压文件失败:" + zipPath, e);
        }

        return false;
    }
}
