package com.ck.tools.utils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * WebUtil 工具类
 * 依赖于 servlet-api 2.5
 *
 * @author cyk
 * @since 2020-01-01
 */
public class WebUtil extends WebNetUtil {
    private static final Logger log = Logger.getLogger(WebUtil.class.getName());

    /**
     * 重置请求体
     *
     * @param request     请求
     * @param charset     报文编码
     * @param readBodyFun 读取函数
     * @return 重置后的请求
     */
    public static HttpServletRequest resetBody(HttpServletRequest request, Charset charset, Function<String, String> readBodyFun) {
        return resetBody(request, (body) -> {
            String bodyStr = new String(body, charset);
            bodyStr = readBodyFun.apply(bodyStr);
            if (bodyStr != null) {
                return bodyStr.getBytes(charset);
            }
            return null;
        });
    }

    /**
     * 重置请求体
     *
     * @param request     请求
     * @param readBodyFun 读取函数
     * @return 重置后的请求
     */
    public static HttpServletRequest resetBody(HttpServletRequest request, Function<byte[], byte[]> readBodyFun) {
        return new HttpServletRequestWrapper(request) {
            /**
             * 用于存储原始请求体的字节数组
             */
            private final byte[] requestBody;
            /**
             * 标记请求体是否已被读取过
             */
            private boolean isRequestBodyRead;

            {
                /* 在实例初始化时读取并缓存原始请求体 */
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                try {
                    WebNetUtil.io(request.getInputStream(), os);
                } catch (IOException e) {
                    throw new RuntimeException("请求数据读取异常", e);
                }
                byte[] body = os.toByteArray();
                if (readBodyFun != null) {
                    body = readBodyFun.apply(body);
                }
                this.requestBody = body;
                this.isRequestBodyRead = false;
            }

            @Override
            public ServletInputStream getInputStream() throws IOException {
                if (!isRequestBodyRead) {
                    isRequestBodyRead = true;
                    if (this.requestBody == null) {
                        throw new NullPointerException("Request body is null");
                    }
                    ByteArrayInputStream is = new ByteArrayInputStream(requestBody);
                    return new ServletInputStream() {
                        @Override
                        public int read() throws IOException {
                            return is.read();
                        }
                    };
                } else {
                    throw new IllegalStateException("Request body has already been read.");
                }
            }

            /**
             * 如果需要支持getReader()方法，还需要覆盖此方法以提供BufferedReader的支持
             */
            @Override
            public BufferedReader getReader() throws IOException {
                String enc = getCharacterEncoding();
                if (enc == null) {
                    enc = StandardCharsets.UTF_8.name();
                }
                return new BufferedReader(new InputStreamReader(getInputStream(), enc));
            }
        };
    }

    /**
     * 转发请求
     *
     * @param request           客户端请求对象
     * @param response          客户端响应对象
     * @param url               路由目标地址
     * @param method            请求方式
     * @param reqHeaderConsumer 自定义请求头
     * @param resHeaderConsumer 自定义响应头
     * @param reqBodyFunc       自定义请求体
     * @param resBodyFunc       自定义响应体
     */
    public static void forward(HttpServletRequest request, HttpServletResponse response, String url, String method
            , Consumer<Map<String, String>> reqHeaderConsumer, Consumer<Map<String, List<String>>> resHeaderConsumer
            , Function<String, String> reqBodyFunc, Function<String, String> resBodyFunc) {
        int timeOut = 1000 * 60 * 6;
        forward(request, response, url, method, timeOut, timeOut, reqHeaderConsumer, resHeaderConsumer, reqBodyFunc, resBodyFunc);
    }

    /**
     * 转发请求
     *
     * @param request           客户端请求对象
     * @param response          客户端响应对象
     * @param url               路由目标地址
     * @param method            请求方式
     * @param timeout           超时时间
     * @param readTimeout       读取超时时间
     * @param reqHeaderConsumer 自定义请求头
     * @param resHeaderConsumer 自定义响应头
     * @param reqBodyFunc       自定义请求体
     * @param resBodyFunc       自定义响应体
     */
    public static void forward(HttpServletRequest request, HttpServletResponse response, String url, String method, int timeout, int readTimeout
            , Consumer<Map<String, String>> reqHeaderConsumer, Consumer<Map<String, List<String>>> resHeaderConsumer
            , Function<String, String> reqBodyFunc
            , Function<String, String> resBodyFunc) {
        Objects.requireNonNull(request, "客户端请求对象 HttpServletRequest 为空");
        Objects.requireNonNull(response, "客户端响应对象 HttpServletResponse 为空");
        Objects.requireNonNull(url, "请求地址URL为空");

        /* 默认请求方式与客户端相同 */
        if (Objects.isNull(method)) {
            method = request.getMethod();
        }

        /* 地址不包含API 则拼接上当前请求地址API */
        if (!url.contains("/")) {
            if (url.contains("?") && url.length() > url.indexOf("?") + 1) {
                String param = url.substring(url.indexOf("?") + 1);
                url = url.substring(0, url.indexOf("?"));
                url += request.getRequestURI() + "?" + param;
            } else {
                url += request.getRequestURI();
            }
        }

        /* 地址不携带参数 则拼接携带当前请求参数 */
        if (!url.contains("?")) {
            url = WebNetUtil.urlWithForm(url, WebNetUtil.buildParams(request.getParameterMap(), StandardCharsets.UTF_8, null), StandardCharsets.UTF_8, false);
        }

        /* 创建 HttpURLConnection */
        HttpURLConnection conn = WebNetUtil.getHttpURLConnection(url, method, timeout, readTimeout);

        /* 发送请求 */
        try {
            conn.connect();

            /* 设置请求头 */
            Enumeration<String> headers = request.getHeaderNames();
            Map<String, String> reqHeader = new LinkedHashMap<String, String>();
            while (headers.hasMoreElements()) {
                String key = headers.nextElement();
                reqHeader.put(key, request.getHeader(key));
            }
            if (Objects.nonNull(reqHeaderConsumer)) {
                reqHeaderConsumer.accept(reqHeader);
            }
            reqHeader.forEach(conn::setRequestProperty);

            /* 转发客户端请求数据流 */
            if (Arrays.asList(WebNetUtil.POST, WebNetUtil.PUT).contains(conn.getRequestMethod())) {
                try (OutputStream os = conn.getOutputStream();
                     InputStream is = request.getInputStream();) {

                    /* 执行自定义请求体处理 */
                    if (Objects.nonNull(reqBodyFunc)) {
                        ByteArrayOutputStream bodyOS = new ByteArrayOutputStream();
                        WebNetUtil.io(is, bodyOS);
                        if (Objects.isNull(request.getCharacterEncoding()) || request.getCharacterEncoding().trim().isEmpty()) {
                            request.setCharacterEncoding(StandardCharsets.UTF_8.name());
                        }
                        String body = reqBodyFunc.apply(bodyOS.toString(request.getCharacterEncoding()));
                        if (Objects.nonNull(body)) {
                            WebNetUtil.io(body.getBytes(request.getCharacterEncoding()), os);
                        } else {
                            WebNetUtil.io(bodyOS.toByteArray(), os);
                        }
                    } else {
                        WebNetUtil.io(is, os);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            /* 响应结果转发 */
            try (OutputStream os = response.getOutputStream();
                 InputStream is = HttpURLConnection.HTTP_OK == conn.getResponseCode() ? conn.getInputStream() : conn.getErrorStream();) {

                response.setStatus(conn.getResponseCode());

                /* 转发响应头信息 */
                Map<String, List<String>> resHeader = conn.getHeaderFields();
                if (Objects.nonNull(resHeaderConsumer)) {
                    resHeaderConsumer.accept(resHeader);
                }
                resHeader.forEach((k, v) -> {
                    if (Objects.nonNull(k) && Objects.nonNull(v)) {
                        response.setHeader(k, String.join(",", v));
                    }
                });

                response.setContentType(conn.getContentType());
                response.setContentLength(conn.getContentLength());
                response.setCharacterEncoding(Objects.isNull(response.getCharacterEncoding()) ? StandardCharsets.UTF_8.name() : response.getCharacterEncoding());

                /* 执行自定义响应体处理 */
                if (Objects.nonNull(resBodyFunc)) {
                    ByteArrayOutputStream bodyOS = new ByteArrayOutputStream();
                    WebNetUtil.io(is, bodyOS);
                    String body = resBodyFunc.apply(bodyOS.toString(response.getCharacterEncoding()));
                    if (Objects.nonNull(body)) {
                        WebNetUtil.io(body.getBytes(response.getCharacterEncoding()), os);
                    } else {
                        WebNetUtil.io(bodyOS.toByteArray(), os);
                    }
                } else {
                    WebNetUtil.io(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("转发请求异常", e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (Objects.nonNull(conn)) {
                conn.disconnect();
            }
        }
    }

    /**
     * 通过HttpServletRequest返回IP地址
     *
     * @param request HttpServletRequest
     * @return ip string
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-client-Ip");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP CLIENT IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP X FORWARDED FOR");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     * 设置 Cookie（生成时间为1天）
     *
     * @param response 响应对象
     * @param name     名称
     * @param value    值
     */
    public static void setCookie(HttpServletResponse response, String name, String value) {
        setCookie(response, name, value, 60 * 60 * 24);
    }

    /**
     * 设置 Cookie
     *
     * @param response 响应对象
     * @param name     名称
     * @param value    值
     */
    public static void setCookie(HttpServletResponse response, String name, String value, String path) {
        setCookie(response, name, value, path, 60 * 60 * 24);
    }

    /**
     * 设置 Cookie
     *
     * @param response 响应对象
     * @param name     名称
     * @param value    值
     * @param maxAge   生存时间（单位秒）
     */
    public static void setCookie(HttpServletResponse response, String name, String value, int maxAge) {
        setCookie(response, name, value, "/", maxAge);
    }

    /**
     * 设置 Cookie
     *
     * @param response 响应对象
     * @param name     名称
     * @param value    值
     * @param maxAge   生存时间（单位秒）
     * @param path     路径
     */
    public static void setCookie(HttpServletResponse response, String name, String value, String path, int maxAge) {
        Cookie cookie = new Cookie(name, null);
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);
        try {
            cookie.setValue(URLEncoder.encode(value, StandardCharsets.UTF_8.name()));
        } catch (UnsupportedEncodingException e) {
            log.warning(String.format("设置Cookie失败：%s", e.getMessage()));
        }
        response.addCookie(cookie);
    }

    /**
     * 移除cookie
     *
     * @param response 响应对象
     * @param name     名称
     */
    public static void removeCookie(HttpServletResponse response, String name) {
        Cookie cookie = new Cookie(name, null);
        cookie.setPath("/");
        cookie.setMaxAge(0);
        cookie.setValue(null);
        response.addCookie(cookie);
    }

    /**
     * 获得指定Cookie的值
     *
     * @param name 名称
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, String name) {
        return getCookie(request, null, name, false);
    }

    /**
     * 获得指定Cookie的值，并删除。
     *
     * @param name 名称
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name) {
        return getCookie(request, response, name, true);
    }

    /**
     * 获得指定Cookie的值
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param name     名字
     * @param isRemove 是否移除
     * @return 值
     */
    public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name, boolean isRemove) {
        String value = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    try {
                        value = URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8.name());
                    } catch (UnsupportedEncodingException e) {
                        log.warning(String.format("获取Cookie失败：%s", e.getMessage()));
                    }
                    if (isRemove) {
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                    }
                }
            }
        }
        return value;
    }

    /**
     * 响应json 数据
     *
     * @param response   响应对象
     * @param jsonString 需要响应的JSON格式字符串
     */
    public static void resJson(HttpServletResponse response, String jsonString) {
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType("application/json");
        try (OutputStream os = response.getOutputStream()) {
            WebNetUtil.io(jsonString.getBytes(StandardCharsets.UTF_8), os);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 响应浏览器重定向
     *
     * @param response 响应对象
     * @param url      重定向地址
     */
    public static void resRedirect(HttpServletResponse response, String url) {
        Objects.requireNonNull(response, "响应对象不能为空");
        Objects.requireNonNull(url, "重定向地址不能为空");
        try {
            response.sendRedirect(url);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 设置跨域访问
     *
     * @param response response
     */
    public static void setResponseAccessControlAllow(HttpServletResponse response) {
        // 启用 服务器端的 cors 跨域
        if (!response.containsHeader("Access-Control-Allow-Origin"))
            response.setHeader("Access-Control-Allow-Origin", "*");
        if (!response.containsHeader("Access-Control-Allow-Headers"))
            response.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
        if (!response.containsHeader("Access-Control-Allow-Methods"))
            response.setHeader("Access-Control-Allow-Methods", "GET, POST");
    }

    /**
     * 设置文件响应头
     *
     * @param fileName 文件名
     * @param length   文件长度
     * @param request  请求对象
     * @param response 响应对象
     */
    public static void setResponseHeaders(String fileName, Long length, HttpServletRequest request, HttpServletResponse response) {

        String fileNameTemp = restFileNameByBrowser(fileName, request);
//        response.reset();
//        response.setHeader("Content-Disposition", "attachment;filename=" + fileNameTemp);
        if (!response.containsHeader("Content-Disposition"))
            response.setHeader("Content-Disposition", "filename=" + fileNameTemp);

        if (response.getContentType() == null)
            setContentTypeByteFileType(fileName, response);

        if (length != null) {
            if (!response.containsHeader("Content-Length"))
                response.setHeader("Content-Length", "" + length);
        } else {
            response.setHeader("Accept-Ranges", "bytes");
        }
    }

    /**
     * 根据不同浏览器编码文件名
     *
     * @param request  请求对象
     * @param fileName 文件名称（带格式后缀）
     * @return 编码后的文件名称
     */
    public static String restFileNameByBrowser(String fileName, HttpServletRequest request) {
        try {
            fileName = fileName == null || fileName.trim().isEmpty() ? "download" : fileName;
            String userAgent = request.getHeader("User-Agent");

            if (userAgent.toLowerCase().indexOf("firefox") > 0) {
                fileName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1); // firefox浏览器
            } else if (userAgent.toUpperCase().indexOf("MSIE") > 0) {
                fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());// IE浏览器
            } else if (userAgent.toUpperCase().indexOf("CHROME") > 0) {
                fileName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);// 谷歌
            } else {
                fileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8.name());// IE浏览器
            }
            return fileName;
        } catch (Exception e) {
            throw new RuntimeException("文档导出文件名称生成失败！" + e.getMessage());
        }
    }

    /**
     * 设置文件介质类型 ContentType
     *
     * @param fileName 文件名称（带格式后缀）
     * @param response 响应对象
     */
    public static void setContentTypeByteFileType(String fileName, HttpServletResponse response) {
        response.setContentType(WebNetUtil.getContentTypeByteFileType(fileName));
    }

    /**
     * 将文件数组数据下载输出<br>
     * ByteArrayOutputStream.toByteArray();
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param data     文件数据
     */
    public static void exportFile(HttpServletRequest request, HttpServletResponse response, byte[] data, String fileName) {
        exportFile(request, response, new ByteArrayInputStream(data), fileName, (long) data.length);
    }

    /**
     * 将文件数组数据下载输出<br>
     * ByteArrayOutputStream.toByteArray();
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param is       文件数据
     */
    public static void exportFile(HttpServletRequest request, HttpServletResponse response, InputStream is, String fileName, Long fileSize) {
        exportFile(request, response, fileName, fileSize, os -> WebNetUtil.io(is, os));
    }

    /**
     * 将文件数组数据下载输出<br>
     * ByteArrayOutputStream.toByteArray();
     *
     * @param request    请求对象
     * @param response   响应对象
     * @param osConsumer 请求响应输出流输出数据函数
     */
    public static void exportFile(HttpServletRequest request, HttpServletResponse response, String fileName, Long fileSize, Consumer<OutputStream> osConsumer) {
        try (OutputStream os = response.getOutputStream()) {
            setResponseHeaders(fileName, fileSize, request, response);
            osConsumer.accept(os);
        } catch (Exception e) {
            log.log(Level.WARNING, "文件输出异常", e);
        }
    }

    /**
     * 将文件下载输出
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param file     文件对象
     * @return 执行结果状态 true 成功
     */
    public static boolean exportFile(HttpServletRequest request, HttpServletResponse response, File file) {
        if (file.exists()) {
            setResponseHeaders(file.getName(), file.length(), request, response);

            try (FileInputStream fis = new FileInputStream(file);
                 BufferedInputStream bis = new BufferedInputStream(fis);
                 OutputStream os = response.getOutputStream()) {
                return WebNetUtil.io(bis, os);
            } catch (IOException e) {
                log.log(Level.WARNING, "文件输出异常", e);
            }
        }
        return false;
    }

    /**
     * 将多个文件压缩下载输出
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param fileName 压缩后的文件名称
     * @param files    文件列表
     * @return 执行结果状态 true 成功
     */
    public static boolean exportFileZip(HttpServletRequest request, HttpServletResponse response, String fileName, File... files) {
        Objects.requireNonNull(request, "文件压缩输出失败，HttpServletRequest is null");
        Objects.requireNonNull(response, "文件压缩输出失败，HttpServletResponse is null");
        Objects.requireNonNull(files, "文件压缩输出失败，Files is null");

        if (fileName == null || fileName.trim().isEmpty()) {
            fileName = files.length < 2 ? fileName = files[0].getName() : "downZip";
            if (!fileName.contains(".")) {
                fileName += ".zip";
            }
        }

        long length = 0;
        for (File file : files) length += file.length();
        setResponseHeaders(fileName, length, request, response);

        try (ZipOutputStream zipOutputStream = new ZipOutputStream(response.getOutputStream())) {
            FileInputStream inputStream = null;
            for (File file : files) {
                // 文件不存在直接跳过
                if (!file.exists()) continue;

                ZipEntry entry = new ZipEntry(file.getPath());
                zipOutputStream.putNextEntry(entry);
                inputStream = new FileInputStream(file);
                WebNetUtil.io(inputStream, zipOutputStream);
                inputStream.close();
                zipOutputStream.closeEntry();
                zipOutputStream.finish();
            }
            zipOutputStream.finish();
        } catch (IOException e) {
            throw new RuntimeException("文件压缩输出异常", e);
        }
        return true;
    }

    /**
     * 将多个文件压缩下载输出
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param fileName 压缩后的文件名称
     * @param files    key 文件完整名称,value 文件二进制数据
     */
    public static void exportFileZip(HttpServletRequest request, HttpServletResponse response, String fileName, Map<String, byte[]> files) {
        try (OutputStream os = response.getOutputStream()) {
            setResponseHeaders(fileName, files.values().stream().map(i -> Long.valueOf(i.length)).reduce(0L, Long::sum), request, response);
            WebNetUtil.exportFileZip(files, os);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
