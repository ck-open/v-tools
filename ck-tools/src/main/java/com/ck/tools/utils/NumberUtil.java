package com.ck.tools.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author cyk
 * @since 2020-01-01
 */
public class NumberUtil {

    /**
     * 阿拉伯数字
     */
    private static final char[] ARABIC_NUMS = "0123456789".toCharArray();
    /**
     * 中文简写数字
     */
    private static final char[] CHN_NUMS_SIMPLE = "零一二三四五六七八九".toCharArray();
    /**
     * 中文繁写数字
     */
    private static final char[] CHN_NUMS = "零壹贰叁肆伍陆柒捌玖".toCharArray();
    /**
     * 中文整数单位
     */
    private static final String[] UNITS = {"整", "拾", "佰", "仟", "万", "亿", "兆", "京", "垓", "秭", "穰", "沟", "涧", "正", "载", "极", "恒河沙", "阿僧祗", "那由他", "不可思议", "无量", "大数", "古戈尔", "频波罗"};
    /**
     * 中文小数单位
     */
    private static final String[] UNITS_FLOAT = {"分", "厘", "毛", "糸", "忽", "微", "纤", "沙", "尘", "埃", "渺", "漠", "模糊", "逡巡", "须臾", "瞬息", "弹指", "刹那", "六德", "虚空", "清净", "阿赖耶", "阿摩罗", "涅槃寂静"};
    /**
     * 人民币数字单位
     */
    private static final String[] UNITS_FLOAT_MONEY = {"角", "分", "厘", "毫"};

    /**
     * 将字符串转换为BigDecimal对象，如果为空，则返回0
     *
     * @param number 数字字符串
     * @return BigDecimal对象
     */
    public static BigDecimal parseDecimalDefZERO(String number) {
        if (number == null || number.trim().isEmpty() || "null".equalsIgnoreCase(number.trim())) {
            return BigDecimal.ZERO;
        }
        return new BigDecimal(number);
    }

    /**
     * 数字格式化为千分位分隔符
     *
     * @param number 数字
     * @return 格式化后的字符串
     */
    public static String formatGrouping(Number number) {
        return formatGrouping(number, 2, null);
    }

    /**
     * 数字格式化为千分位分隔符
     *
     * @param number 数字
     * @param devVal 默认值 当 number 为 null 时，使用默认值
     * @return 格式化后的字符串
     */
    public static String formatGrouping(Number number, String devVal) {
        return formatGrouping(number, 2, devVal);
    }

    /**
     * 数字格式化为千分位分隔符
     *
     * @param number 数字
     * @param scale  保留小数位数
     * @return 格式化后的字符串
     */
    public static String formatGrouping(Number number, int scale) {
        return formatGrouping(number, scale, null);
    }

    /**
     * 数字格式化为千分位分隔符
     *
     * @param number 数字
     * @param scale  保留小数位数
     * @param devVal 默认值 当 number 为 null 时，使用默认值
     * @return 格式化后的字符串
     */
    public static String formatGrouping(Number number, int scale, String devVal) {
        if (number == null) {
            return devVal;
        }

        if (scale == 2) {
            return format(number, "#,##0.00");
        }

        return format(number, "#,##" + format(null, scale));
    }

    /**
     * 数字格式化为中文简写 使用计量单位并非货币单位
     *
     * @param number 数字
     * @param unit   整数单位 例如：元
     * @return 格式化后的字符串
     */
    public static String formatCHNSimple(Number number, Character unit) {
        return formatCHN(number, CHN_NUMS_SIMPLE, unit, false);
    }

    /**
     * 数字格式化为中文大写 使用计量单位并非货币单位
     *
     * @param number 数字
     * @param unit   整数单位 例如：元
     * @return 格式化后的字符串
     */
    public static String formatCHN(Number number, Character unit) {
        return formatCHN(number, CHN_NUMS, unit, false);
    }

    /**
     * 数字格式化为元单位符
     *
     * @param number 数字
     * @return 格式化后的字符串
     */
    public static String formatCurrency_ARABIC(Number number) {
        return formatCHN(number, ARABIC_NUMS, '元', true);
    }

    /**
     * 数字格式化为中文简写
     *
     * @param number 数字
     * @return 格式化后的字符串
     */
    public static String formatCurrencySimple(Number number) {
        return formatCHN(number, CHN_NUMS_SIMPLE, '元', true);
    }

    /**
     * 数字格式化为中文简写
     *
     * @param number 数字
     * @param unit   整数单位 例如：元
     * @return 格式化后的字符串
     */
    public static String formatCurrencySimple(Number number, Character unit) {
        return formatCHN(number, CHN_NUMS_SIMPLE, unit, true);
    }

    /**
     * 数字格式化为中文大写
     *
     * @param number 数字
     * @return 格式化后的字符串
     */
    public static String formatCurrency(Number number) {
        return formatCHN(number, CHN_NUMS, '元', true);
    }

    /**
     * 数字格式化为中文大写
     *
     * @param number 数字
     * @param unit   整数单位 例如：元
     * @return 格式化后的字符串
     */
    public static String formatCurrency(Number number, Character unit) {
        return formatCHN(number, CHN_NUMS, unit, true);
    }

    /**
     * 小数浮点部分中文格式化
     *
     * @param number 数字
     * @param nums   0-9中文格式字符
     * @return 格式化后的字符串
     */
    private static String formatCHNFloat(Number number, char[] nums, String[] unit) {
        String numberStr = format(number, "#.###########################################################");
        StringBuilder result = new StringBuilder();

        if (numberStr.contains(".")) {
            numberStr = numberStr.substring(numberStr.indexOf(".") + 1);

            for (int i = 0; i < numberStr.length(); i++) {
                char n = numberStr.charAt(i);
                if (n != '0') {
                    result.append(nums[n - '0']); // 插入数字
                    if (unit.length > i) {
                        result.append(unit[i]);
                    }
                }
            }
        }
        return result.reverse().toString();
    }

    /**
     * 数字格式化为中文字符
     *
     * @param number  数字
     * @param nums    0-9中文格式字符
     * @param unit    整数单位 例如：元
     * @param isMoney 是否使用货币单位
     * @return 格式化后的字符串
     */
    private static String formatCHN(Number number, char[] nums, Character unit, boolean isMoney) {
        StringBuilder result = new StringBuilder();

        /* 转换小数 */
        String floatStr = formatCHNFloat(number, nums, isMoney ? UNITS_FLOAT_MONEY : UNITS_FLOAT);
        /* 转换整数 */
        String numberStr = format(number, "0.00");
        numberStr = numberStr.substring(0, numberStr.indexOf("."));

        if (floatStr.trim().isEmpty()) {
            /* 没有小数追加末尾单位符  如 整 */
            result.append(UNITS[0]);
        } else {
            result.append(floatStr);
        }

        int index = 0;
        for (int i = numberStr.length() - 1; i >= 0; i--, index++) {

            char n = numberStr.charAt(i);
            /* 个位单位符 */
            if (index == 0 && unit != null) {
                if (!"0".equals(numberStr) || floatStr.trim().isEmpty()) {
                    result.append(unit);
                }
                /* 整数部分为0 */
                if ("0".equals(numberStr) && floatStr.trim().isEmpty()) {
                    result.append(nums[0]);
                }
            }

            /* 插入[万]或者[亿] */
            if (index > 0 && index % 4 == 0) {
                result.append(UNITS[index / 4 + 3]);
            }

            if (n != '0') {
                /* 插入[拾],[佰]或[仟] */
                if (index % 4 != 0) {
                    result.append(UNITS[index % 4]);
                }
                /* 插入数字 */
                result.append(nums[n - '0']);
            } else if (numberStr.length() != i + 1    /* 不是最后一个字符 */
                    && numberStr.charAt(i - 1) != '0'  /* 左侧字符不是0 */
                    && numberStr.charAt(i + 1) != '0'  /* 右侧字符不是0 */
                    && index % 4 != 0   /* 数字位数取余4 不为0  表示不是整万或亿等单位进位 */
                    && nums[0] != '0') {
                result.append(nums[0]);
            }
        }
        return result.reverse().toString();
    }


    /**
     * 格式化数字，保留两位小数
     *
     * @param number  原数字
     * @param pattern <p>
     *                0：表示数字的位数，如果数字不足指定的位数，则在前面填充0。
     *                #：表示数字的位数，如果数字不足指定的位数，则忽略该位。
     *                .：表示小数点的位置，用于指定小数部分的位数。
     *                ,：表示千位分隔符的位置，用于指定整数部分的位数。
     *                ? 表示乘以 1000 和作为千进制货币符显示。
     *                </p>
     * @return 格式化后
     */
    public static String format(Number number, String pattern) {
        return format(number, pattern, RoundingMode.HALF_UP);
    }

    /**
     * 格式化数字，保留两位小数
     *
     * @param number       原数字
     * @param pattern      <p>
     *                     0：表示数字的位数，如果数字不足指定的位数，则在前面填充0。
     *                     #：表示数字的位数，如果数字不足指定的位数，则忽略该位。
     *                     .：表示小数点的位置，用于指定小数部分的位数。
     *                     ,：表示千位分隔符的位置，用于指定整数部分的位数。
     *                     ? 表示乘以 1000 和作为千进制货币符显示。
     *                     </p>
     * @param roundingMode 进位舍入方式
     * @return 格式化后
     */
    public static String format(Number number, String pattern, RoundingMode roundingMode) {
        if (pattern == null || pattern.trim().isEmpty()) {
            pattern = "0.00";
        }
        DecimalFormat df = new DecimalFormat(pattern);
        if (roundingMode != null) {
            df.setRoundingMode(roundingMode);
        }
        if (number == null) {
            return df.format(0.0);
        }
        return df.format(number);
    }

    /**
     * 保留指定位数的小数
     *
     * @param number 原数字
     * @param scale  保留小数位数
     * @return 格式化后
     */
    public static String format(Number number, int scale) {
        return format(number, scale, RoundingMode.HALF_UP);
    }

    /**
     * 保留指定位数的小数
     *
     * @param number       原数字
     * @param scale        保留小数位数
     * @param roundingMode 进位舍入方式
     * @return 格式化后
     */
    public static String format(Number number, int scale, RoundingMode roundingMode) {
        if (number == null) {
            StringBuilder reString = new StringBuilder("0.");
            for (int i = 1; i <= scale; i++) {
                reString.append("0");
            }
            return reString.toString();
        }
        NumberFormat nf = NumberFormat.getNumberInstance();
        if (roundingMode != null) {
            nf.setRoundingMode(roundingMode);
        }
        nf.setMaximumFractionDigits(scale);
        nf.setGroupingUsed(false);
        return nf.format(number);
    }

    /**
     * 按指定位数将数字转换为字符串
     *
     * @param number 目标数字
     * @param length 字符长度 0或null 表示不限制长度
     * @return 数字大于长度10进制表述最大值后自动转换为36进制返回
     */
    public static String setNumberLength(Number number, int length) {
        if (number == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        // 设置序号长度
        String noTemp = format(number, 0);
        if (length > 0) {
            if (noTemp.length() > length) {
                noTemp = Long.toString(Long.parseLong(noTemp), 36).toUpperCase();
            }

            if (!noTemp.isEmpty() && noTemp.length() < length) {
                for (int i = 0; i < length - noTemp.length(); i++)
                    result.append("0");
            }
        }
        return result.append(noTemp).toString();
    }

    /**
     * 将36进制数字转为10进制数字
     *
     * @param number 36进制数字
     * @return 10进制数字
     */
    public static Long parseLong(String number) {
        if (number == null || number.trim().isEmpty()) {
            return null;
        }
        while (number.startsWith("0"))
            number = number.substring(1);

        if (number.matches("^(0|[1-9][0-9]*|-[1-9][0-9]*)$"))
            return Long.parseLong(number);

        return Long.parseLong(number, 36);
    }


    /**
     * 计算税费 净值
     * 保留2位小数进行计算
     *
     * @param val     原值
     * @param taxRate 税率
     * @return 不含税净值
     */
    public static BigDecimal taxNet(BigDecimal val, BigDecimal taxRate) {
        return taxNet(val, taxRate, 2, RoundingMode.HALF_UP);
    }

    /**
     * 计算税费 净值
     *
     * @param val          原值
     * @param taxRate      税率
     * @param scale        保留小数位数
     * @param roundingMode 进位舍入方式
     * @return 不含税净值
     */
    public static BigDecimal taxNet(BigDecimal val, BigDecimal taxRate, int scale, RoundingMode roundingMode) {
        if (val == null || taxRate == null) {
            return BigDecimal.ZERO;
        }

        return val.divide(BigDecimal.ONE.add(taxRate), scale, roundingMode);
    }

    /**
     * 计算税费
     * 保留2位小数进行计算
     *
     * @param val     原值
     * @param taxRate 税率
     * @return 税值
     */
    public static BigDecimal taxFee(BigDecimal val, BigDecimal taxRate) {
        return taxFee(val, taxRate, 2, RoundingMode.HALF_UP);
    }

    /**
     * 计算税费
     *
     * @param val          原值
     * @param taxRate      税率
     * @param scale        保留小数位数
     * @param roundingMode 进位舍入方式
     * @return 税值
     */
    public static BigDecimal taxFee(BigDecimal val, BigDecimal taxRate, int scale, RoundingMode roundingMode) {
        return val.subtract(taxNet(val, taxRate, scale, roundingMode));
    }


    /**
     * 尾差值计算并调整
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @param mapper   字段取值映射器
     * @param sorted   调整尾差前进行排序
     * @param dispose  尾差值处理
     * @param <T>      列表对象
     */
    public static <T> void adjustment(BigDecimal totalVal, List<T> values, Function<? super T, BigDecimal> mapper, Function<? super T, BigDecimal> sorted, BiConsumer<T, BigDecimal> dispose) {
        if (values == null || values.isEmpty()) {
            return;
        }
        if (sorted != null)
            values.sort(Comparator.comparing(sorted));

        adjustment(totalVal, values, mapper, dispose);
    }


    /**
     * 尾差值计算并调整
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @param mapper   字段取值映射器
     * @param dispose  尾差值处理
     * @param <T>      列表对象
     */
    public static <T> void adjustment(BigDecimal totalVal, List<T> values, Function<? super T, BigDecimal> mapper, BiConsumer<T, BigDecimal> dispose) {
        if (mapper == null) {
            throw new RuntimeException("尾差值计算 mapper不能为空");
        }
        if (values == null || values.isEmpty()) {
            return;
        }
        BigDecimal adjustment = adjustment(totalVal, values.stream().map(mapper).collect(Collectors.toList()));
        if (dispose != null) {
            dispose.accept(values.get(values.size() - 1), adjustment);
        }
    }

    /**
     * 尾差值计算
     * 通过列表值计算尾差值，得到结果后与列表最后一个元素值累加
     *
     * @param totalVal 总值
     * @param values   列表值
     * @return 尾差值
     */
    public static BigDecimal adjustment(BigDecimal totalVal, Collection<BigDecimal> values) {
        if (totalVal == null) {
            throw new RuntimeException("尾差值计算 totalVal不能为空");
        }
        if (values == null || values.isEmpty()) {
            return BigDecimal.ZERO;
        }
        return totalVal.subtract(values.stream().reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    /**
     * 设置值精度
     *
     * @param val   值
     * @param scale 保留小数位数
     * @return 舍入结果
     */
    public static BigDecimal scale(BigDecimal val, int scale) {
        return scale(val, scale, RoundingMode.HALF_UP);
    }

    /**
     * 设置值精度
     *
     * @param val   值
     * @param scale 保留小数位数
     * @return 舍入结果
     */
    public static BigDecimal scale(BigDecimal val, int scale, RoundingMode roundingMode) {
        if (val == null || scale < 0) {
            return val;
        }
        if (roundingMode == null) {
            roundingMode = RoundingMode.HALF_UP;
        }
        return val.setScale(scale, roundingMode);
    }

    /**
     * 计算 加法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    static BigDecimal add(BigDecimal val1, BigDecimal val2) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("加法值不能为空: " + val1 + " * " + val2);
        }
        return val1.add(val2);
    }

    /**
     * 计算 减法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    static BigDecimal subtract(BigDecimal val1, BigDecimal val2) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("减法值不能为空: " + val1 + " * " + val2);
        }
        return val1.subtract(val2);
    }

    /**
     * 计算 乘法运算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    public static BigDecimal multiply(BigDecimal val1, BigDecimal val2) {
        return multiply(val1, val2, 2, RoundingMode.HALF_UP);
    }

    /**
     * 计算 乘法运算
     *
     * @param val1         值1
     * @param val2         值2
     * @param scale        保留小数位数
     * @param roundingMode 进位舍入方式
     * @return 运算结果
     */
    public static BigDecimal multiply(BigDecimal val1, BigDecimal val2, int scale, RoundingMode roundingMode) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("乘数不能为空: " + val1 + " * " + val2);
        }
        return scale(val1.multiply(val2), scale, roundingMode);
    }

    /**
     * 计算 除法运算
     * 保留2位小数进行计算
     *
     * @param val1 值1
     * @param val2 值2
     * @return 运算结果
     */
    public static BigDecimal divide(BigDecimal val1, BigDecimal val2) {
        return divide(val1, val2, 2, RoundingMode.HALF_UP);
    }

    /**
     * 计算 除法运算
     *
     * @param val1         值1
     * @param val2         值2
     * @param scale        保留小数位数
     * @param roundingMode 进位舍入方式
     * @return 运算结果
     */
    public static BigDecimal divide(BigDecimal val1, BigDecimal val2, int scale, RoundingMode roundingMode) {
        if (val1 == null || val2 == null) {
            throw new RuntimeException("除数不能为空: " + val1 + " / " + val2);
        }
        if (val2.compareTo(BigDecimal.ZERO) == 0) {
            throw new RuntimeException("除数错误: " + val1 + " / " + val2);
        }
        return scale(val1.divide(val2, scale, roundingMode), scale);
    }


    public static void main(String[] args) {

        String str = "0.68";
        System.out.println("\r---------------------------数字格式化展示------------------------------");
        System.out.println("原格式 ==> " + str);
        System.out.println("解析字符串数值默认0 ==> " + parseDecimalDefZERO("null"));
        System.out.println("解析字符串数值默认0 ==> " + parseDecimalDefZERO(" "));
        System.out.println("数字格式化为千分位逗号分割 ==> " + formatGrouping(new BigDecimal("253468497536.25")));
        System.out.println("数字格式化为千分位逗号分割 ==> " + formatGrouping(253468497536.25564, 3));
        System.out.println("数字格式化为中文简写计量单位 ==> " + formatCHNSimple(new BigDecimal(str), '元'));
        System.out.println("数字格式化为中文大写计量单位 ==> " + formatCHN(new BigDecimal(str), '元'));
        System.out.println("货币数字格式化为元单位符 ==> " + formatCurrency_ARABIC(new BigDecimal(str)));
        System.out.println("货币数字格式化为中文简写 ==> " + formatCurrencySimple(new BigDecimal(str)));
        System.out.println("货币数字格式化为中文大写 ==> " + formatCurrency(new BigDecimal(str)));
        System.out.println("格式化数字，并四舍五入 ==> " + format(56.548975215, "#.###"));
        System.out.println("格式化数字，指定进位方式 ==> " + format(56.548975215, "#.###", RoundingMode.HALF_UP));
        System.out.println("保留指定位数的小数 ==> " + format(56.548975215, 4));
        System.out.println("保留指定位数的小数 ==> " + format(56.548975215, 0));
        System.out.println("保留指定位数，超长则进位36进制 ==> " + setNumberLength(55448435416545456L, 4));
        System.out.println("保留指定位数，超长则进位36进制 ==> " + setNumberLength(55448435416545456.25415, 5));
        System.out.println("保留指定位数，解析36进制为10进制 ==> " + parseLong(setNumberLength(55448435416545456L, 5)));
    }
}
