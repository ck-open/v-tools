package com.ck.tools.utils;

import java.util.Properties;
import java.util.logging.Logger;

/**
 * 设备操作工具类
 *
 * @author cyk
 * @since 2021-06-06
 */
public class SystemUtil {
    private static final Logger log = Logger.getLogger(SystemUtil.class.getName());
    public static final Properties PROPS = System.getProperties();

    /**
     * Java的运行环境版本
     */
    public static String getJavaVersion() {
        return PROPS.getProperty("java.version");
    }

    /**
     * Java的运行环境供应商
     */
    public static String getJavaVendor() {
        return PROPS.getProperty("java.vendor");
    }

    /**
     * Java供应商的URL
     */
    public static String getJavaVendorUrl() {
        return PROPS.getProperty("java.vendor.url");
    }

    /**
     * Java的安装路径
     */
    public static String getJavaHome() {
        return PROPS.getProperty("java.home");
    }

    /**
     * Java的虚拟机规范版本
     */
    public static String getJavaVmVSpecificationVersion() {
        return PROPS.getProperty("java.vm.specification.version");
    }

    /**
     * Java的虚拟机规范供应商
     */
    public static String getJavaVmVSpecificationVendor() {
        return PROPS.getProperty("java.vm.specification.vendor");
    }

    /**
     * Java的虚拟机规范名称
     */
    public static String getJavaVmVSpecificationName() {
        return PROPS.getProperty("java.vm.specification.name");
    }

    /**
     * Java的虚拟机实现版本
     */
    public static String getJavaVmVersion() {
        return PROPS.getProperty("java.vm.version");
    }

    /**
     * Java的虚拟机实现供应商
     */
    public static String getJavaVmVendor() {
        return PROPS.getProperty("java.vm.vendor");
    }

    /**
     * Java的虚拟机实现名称
     */
    public static String getJavaVmName() {
        return PROPS.getProperty("java.vm.name");
    }

    /**
     * Java运行时环境规范版本
     */
    public static String getJavaSpecificationVersion() {
        return PROPS.getProperty("java.specification.version");
    }

    /**
     * Java运行时环境规范供应商
     */
    public static String getJavaSpecificationVendor() {
        return PROPS.getProperty("java.specification.vender");
    }

    /**
     * Java运行时环境规范名称
     */
    public static String getJavaSpecificationName() {
        return PROPS.getProperty("java.specification.name");
    }

    /**
     * Java的类格式版本号
     */
    public static String getJavaClassVersion() {
        return PROPS.getProperty("java.class.version");
    }

    /**
     * Java的类路径
     */
    public static String getJavaClassPath() {
        return PROPS.getProperty("java.class.path");
    }

    /**
     * 加载库时搜索的路径列表
     */
    public static String getJavaLibraryPath() {
        return PROPS.getProperty("java.library.path");
    }

    /**
     * 默认的临时文件路径
     */
    public static String getJavaIoTmpdir() {
        return PROPS.getProperty("java.io.tmpdir");
    }

    /**
     * 一个或多个扩展目录的路径
     */
    public static String getJavaExtDirs() {
        return PROPS.getProperty("java.ext.dirs");
    }

    /**
     * 操作系统的名称
     */
    public static String getOsName() {
        return PROPS.getProperty("os.name");
    }

    /**
     * 操作系统的构架
     */
    public static String getOsArch() {
        return PROPS.getProperty("os.arch");
    }

    /**
     * 操作系统的版本
     */
    public static String getOsVersion() {
        return PROPS.getProperty("os.version");
    }

    /**
     * 用户的主目录
     */
    public static String getUserHome() {
        return PROPS.getProperty("user.home");
    }

    /**
     * 用户的当前工作目录
     */
    public static String getUserDir() {
        return PROPS.getProperty("user.dir");
    }

    /**
     * 文件分隔符
     */
    public static String getFileSeparator() {
        return PROPS.getProperty("file.separator");
    }

    /**
     * 路径分隔符
     */
    public static String getPathSeparator() {
        return PROPS.getProperty("path.separator");
    }

    /**
     * 行分隔符
     */
    public static String getLineSeparator() {
        return PROPS.getProperty("line.separator");
    }

    /**
     * 用户的账户名称
     */
    public static String getUserName() {
        return PROPS.getProperty("user.name");
    }

    /**
     * 是否为windows
     *
     * @return boolean
     */
    public static boolean isWindows() {
        return getOsName().toLowerCase().contains("windows");
    }

    /**
     * 是否为mac
     *
     * @return boolean
     */
    public static boolean isMac() {
        return getOsName().toLowerCase().contains("mac");
    }

    /**
     * 是否为linux
     *
     * @return boolean
     */
    public static boolean isLinux() {
        return getOsName().toLowerCase().contains("linux");
    }

    /**
     * 是否为unix
     *
     * @return boolean
     */
    public static boolean isUnix() {
        return getOsName().toLowerCase().contains("unix");
    }

    /**
     * 当前环境信息
     *
     * @return String
     */
    public static String print() {
        return "Java的运行环境版本：" + getJavaVersion() + getLineSeparator() + "\r" +
                "Java的运行环境供应商：" + getJavaVendor() + getLineSeparator() + "\r" +
                "Java供应商的URL：" + getJavaVendorUrl() + getLineSeparator() + "\r" +
                "Java的安装路径：" + getJavaHome() + getLineSeparator() + "\r" +
                "Java的虚拟机规范版本：" + getJavaVmVSpecificationVersion() + getLineSeparator() + "\r" +
                "Java的虚拟机规范供应商：" + getJavaVmVSpecificationVendor() + getLineSeparator() + "\r" +
                "Java的虚拟机规范名称：" + getJavaVmVSpecificationName() + getLineSeparator() + "\r" +
                "Java的虚拟机实现版本：" + getJavaVmVersion() + getLineSeparator() + "\r" +
                "Java的虚拟机实现供应商：" + getJavaVmVendor() + getLineSeparator() + "\r" +
                "Java的虚拟机实现名称：" + getJavaVmName() + getLineSeparator() + "\r" +
                "Java运行时环境规范版本：" + getJavaSpecificationVersion() + getLineSeparator() + "\r" +
                "Java运行时环境规范供应商：" + getJavaSpecificationVendor() + getLineSeparator() + "\r" +
                "Java运行时环境规范名称：" + getJavaSpecificationName() + getLineSeparator() + "\r" +
                "Java的类格式版本号：" + getJavaClassVersion() + getLineSeparator() + "\r" +
                "Java的类路径：" + getJavaClassPath() + getLineSeparator() + "\r" +
                "加载库时搜索的路径列表：" + getJavaLibraryPath() + getLineSeparator() + "\r" +
                "默认的临时文件路径：" + getJavaIoTmpdir() + getLineSeparator() + "\r" +
                "一个或多个扩展目录的路径：" + getJavaExtDirs() + getLineSeparator() + "\r" +
                "操作系统的名称：" + getOsName() + getLineSeparator() + "\r" +
                "操作系统的构架：" + getOsArch() + getLineSeparator() + "\r" +
                "操作系统的版本：" + getOsVersion() + getLineSeparator() + "\r" +
                "用户的主目录：" + getUserHome() + getLineSeparator() + "\r" +
                "用户的当前工作目录：" + getUserDir() + getLineSeparator() + "\r" +
                "文件分隔符：" + getFileSeparator() + getLineSeparator() + "\r" +
                "路径分隔符：" + getPathSeparator() + getLineSeparator() + "\r" +
                "用户的账户名称：" + getUserName() + getLineSeparator() + "\r";
    }

    public static void main(String[] args) {
        System.out.println(print());
    }
}
