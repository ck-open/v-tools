package com.ck.tools.utils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * 图片操作类
 *
 * @author cyk
 * @since 2020-01-01
 */
public class ImageUtil {
    private static final Logger log = Logger.getLogger(ImageUtil.class.getName());

    public static final String PrintType_IsTop = "PrintType_IsTop"; /* 像素点位置信息 上 */
    public static final String PrintType_IsBottom = "PrintType_IsBottom"; /* 像素点位置信息 下 */
    public static final String PrintType_IsLeft = "PrintType_IsLeft"; /* 像素点位置信息 左 */
    public static final String PrintType_IsRight = "PrintType_IsRight"; /* 像素点位置信息 右 */
    public static final String PrintType_IsCenterTop = "PrintType_IsCenterTop"; /* 像素点位置信息 正上 */
    public static final String PrintType_IsCenterBottom = "PrintType_IsCenterBottom"; /* 像素点位置信息 正下 */
    public static final String PrintType_IsLeftTop = "PrintType_IsLeftTop"; /* 像素点位置信息 左上 */
    public static final String PrintType_IsLeftCenter = "PrintType_IsLeftCenter"; /* 像素点位置信息 左 */
    public static final String PrintType_IsLeftBottom = "PrintType_IsLeftBottom"; /* 像素点位置信息 左下 */
    public static final String PrintType_IsRightTop = "PrintType_IsRightTop"; /* 像素点位置信息 右上 */
    public static final String PrintType_IsRightCenter = "PrintType_IsRightCenter"; /* 像素点位置信息 右 */
    public static final String PrintType_IsRightBottom = "PrintType_IsRightBottom"; /* 像素点位置信息 右下 */

    /**
     * 默认格式
     */
    private static final String Format_Name = "JPEG";

    /**
     * 获取Image数据对象
     *
     * @param imagePath 图片路径
     * @return Image数据对象
     */
    public static BufferedImage readImage(String imagePath) {
        if (imagePath != null)
            return readImage(Paths.get(imagePath));
        return null;
    }

    /**
     * 获取Image数据对象
     *
     * @param imagePath 图片路径
     * @return Image数据对象
     */
    public static BufferedImage readImage(Path imagePath) {
        if (imagePath != null && Files.exists(imagePath)) {
            try {
                return ImageIO.read(imagePath.toFile());
            } catch (IOException e) {
                log.warning(String.format("getImage Reader Error:%s", e.getMessage()));
            }
        }
        return null;
    }

    /**
     * 写出图片到文件
     *
     * @param image       图像对象
     * @param imageTarget 目标文件
     * @param formatName  图片格式 如：jpg
     */
    public static void writeImage(BufferedImage image, File imageTarget, String formatName) {
        try {
            ImageIO.write(image, formatName, imageTarget);
        } catch (IOException e) {
            throw new RuntimeException(String.format("writeImage Error：%s", e.getMessage()), e);
        }
    }

    /**
     * 将图像数组读取成BufferedImage
     *
     * @param imageBytes 图像byte[]
     * @return BufferedImage
     */
    public static BufferedImage getBytesToBufferedImage(byte[] imageBytes) {
        try {
            /* 将图像数组读取成BufferedImage*/
            ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes);
            return ImageIO.read(bis);
        } catch (IOException e) {
            log.warning(String.format("getBytesToBufferedImage Error：%s", e.getMessage()));
        }
        return null;
    }

    /**
     * 将图像数组读取成ImageIcon
     *
     * @param imageBytes 图像byte[]
     * @return ImageIcon
     */
    public static ImageIcon getBytesToImageIcon(byte[] imageBytes) {
        return new ImageIcon(imageBytes);
    }

    /**
     * 将BufferedImage图像转换成ImageIcon
     *
     * @param image 图像BufferedImage
     * @return ImageIcon
     */
    public static ImageIcon getBufferedImageToImageIcon(BufferedImage image) {
        return new ImageIcon(image);
    }

    /**
     * 将ImageIcon图像转换成BufferedImage
     *
     * @param imageIcon 图像ImageIcon
     * @return BufferedImage
     */
    public static BufferedImage getImageIconToBufferedImage(ImageIcon imageIcon) {
        /* 把imageIcon转为image格式、然后设置图片宽高为当前窗口宽高*/
        return (BufferedImage) imageIcon.getImage().getScaledInstance(imageIcon.getIconWidth(),
                imageIcon.getIconHeight(), Image.SCALE_SMOOTH);
    }

    /**
     * 将BufferedImage图像转换成JPEGImage二进制文件
     *
     * @param images 图像BufferedImage
     * @return byte[]
     */
    public static byte[] getBufferedImageToByte(BufferedImage images) {
        try {
            /* 将BufferedImage压缩为二进制图片文件*/
            ByteArrayOutputStream bas = new ByteArrayOutputStream();
            /* 将image按照指定的图片格式 转换成byte[] 并传输给bas */
            ImageIO.write(images, Format_Name, bas);
            /* 转换成byte数组*/
            return bas.toByteArray();
        } catch (IOException e) {
            log.warning(String.format("getBufferedImageToByte Error：%s", e.getMessage()));
        }
        return null;
    }

    /**
     * 按比例缩放重绘图片像素
     *
     * @param image    要重绘的图片
     * @param width    指定重绘后的图形宽度
     * @param height   指定重绘后的图像高度
     * @param hints    图像缩放算法:
     *                 Image.SCALE_SMOOTH  平滑优先
     *                 Image.SCALE_FAST    不能速度优先
     *                 Image.SCALE_AREA_AVERAGING  区域均值
     *                 Image.SCALE_REPLICATE   像素复制型缩放
     *                 Image.SCALE_DEFAULT   默认缩放模式
     * @param observer 异步时成功后的回调
     * @return 重绘后的BufferedImage
     */
    public static BufferedImage setImageRedraw(BufferedImage image, int width, int height, int hints, ImageObserver observer) {
        /* 将BufferedImage向上造型Image*/

        /* 构造一个类型为预定义图像类型之一的 BufferedImage*/
        BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        /*
         * 绘制图像 getScaledInstance表示创建此图像的缩放版本，返回一个新的缩放版本Image,按指定的width,height呈现图像。
         */
        tag.getGraphics().drawImage(image.getScaledInstance(width, height, hints), 0, 0, observer);
        return tag;
    }

    /**
     * 使用 Graphics2D 进行图片按比例重绘
     *
     * @param image  要重绘的图片
     * @param width  指定重绘后的图形宽度
     * @param height 指定重绘后的图像高度
     * @return 重绘后的BufferedImage
     */
    public static BufferedImage setImageRedraw2D(BufferedImage image, int width, int height) {
        int imageType = image.getTransparency() == 1 ? 1 : 2;
        BufferedImage bufferedImage = new BufferedImage(width, height, imageType);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }

    /**
     * 提升图像亮度
     *
     * @param image  图像
     * @param amount 值
     * @return 提升后的图像
     */
    public static BufferedImage adjustBrightness(BufferedImage image, int amount) {
        return adjustBrightness(image, amount, 20);
    }

    /**
     * 提升图像亮度
     *
     * @param image  图像
     * @param amount 值
     * @param offset 补偿值  大于平均值的像素点补偿值
     * @return 提升后的图像
     */
    public static BufferedImage adjustBrightness(BufferedImage image, int amount, int offset) {
        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage enhancedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        int avg = getRGBColorAvg(image);
        int[] avgRGB = ImageUtil.rgbToColor(avg);

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int rgb = image.getRGB(x, y);
                int r = (rgb & 0xFF0000) >> 16;
                int g = (rgb & 0xFF00) >> 8;
                int b = rgb & 0xFF;
                int adjustedR = r + amount;
                int adjustedG = g + amount;
                int adjustedB = b + amount;

                if (avgRGB[0] - offset < adjustedR && avgRGB[1] - offset < adjustedG && avgRGB[2] - offset < adjustedB) {
                    adjustedR -= (int) (offset * 1.6);
                    adjustedG -= (int) (offset * 1.6);
                    adjustedB -= (int) (offset * 1.6);
                }

                adjustedR = Math.max(0, Math.min(255, adjustedR));
                adjustedG = Math.max(0, Math.min(255, adjustedG));
                adjustedB = Math.max(0, Math.min(255, adjustedB));

                int enhancedRGB = (adjustedR << 16) | (adjustedG << 8) | adjustedB;
                enhancedImage.setRGB(x, y, enhancedRGB);
            }
        }

        return enhancedImage;
    }

    /**
     * 图像旋转矫正
     *
     * @param image 要旋转的图片
     * @param angle 设置旋转角度，单位为度  如： 45.0f
     * @return 旋转后的图片
     */
    public static BufferedImage rotate(BufferedImage image, double angle) {
        int width = image.getWidth();
        int height = image.getHeight();
        /* 创建一个与原图像大小相同的矩形 */
        AffineTransform at = AffineTransform.getRotateInstance(angle, (double) width / 2, (double) height / 2);
        /* 创建一个通过AffineTransform对图像进行缩放、平移、旋转等操作的图像处理器 */
        AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        /* 对原图像进行旋转操作，返回旋转后的图像 */
        return op.filter(image, null);
    }

    /**
     * 按照固定宽高原图压缩
     *
     * @param img    图片文件
     * @param width  目标 宽度
     * @param height 目标 高度
     * @param out    目标图片输出流
     */
    public static void thumbnail(File img, int width, int height, OutputStream out) {
        try {
            BufferedImage BI = ImageIO.read(img);
            Image image = BI.getScaledInstance(width, height, Image.SCALE_SMOOTH);

            imageIoWriter(image, width, height, out);
        } catch (IOException e) {
            log.warning(String.format("thumbnail Error：%s", e.getMessage()));
        }
    }

    /**
     * 设置图像尺寸并输出到流
     *
     * @param image  图片
     * @param width  宽
     * @param height 高
     * @param out    输出流
     */
    public static void imageIoWriter(Image image, int width, int height, OutputStream out) {
        try {
            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.setColor(Color.RED);
            g.drawImage(image, 0, 0, null); /* 绘制处理后的图*/
            g.dispose();
            ImageIO.write(tag, Format_Name, out);
        } catch (IOException e) {
            log.warning(String.format("imageIoWriter Error：%s", e.getMessage()));
        }
    }

    /**
     * 按照宽高裁剪
     *
     * @param srcImageFile 图片文件
     * @param destWidth    宽
     * @param destHeight   高
     * @param out          结果输出流
     */
    public static void cut_w_h(File srcImageFile, int destWidth, int destHeight, OutputStream out) {
        cut_w_h(srcImageFile, 0, 0, destWidth, destHeight, out);
    }

    /**
     * 按照宽高裁剪
     *
     * @param imageFile  图片文件
     * @param destWidth  宽
     * @param destHeight 高
     * @param out        结果输出流
     * @param x          截取起始横向坐标
     * @param y          截取纵向坐标
     */
    public static void cut_w_h(File imageFile, int x, int y, int destWidth, int destHeight, OutputStream out) {
        try {
            Image img;
            ImageFilter cropFilter;
            /* 读取源图像*/
            BufferedImage bi = ImageIO.read(imageFile);
            int srcWidth = bi.getWidth(); /* 源图宽度*/
            int srcHeight = bi.getHeight(); /* 源图高度*/

            if (srcWidth >= destWidth && srcHeight >= destHeight) {
                Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);

                cropFilter = new CropImageFilter(x, y, destWidth, destHeight);
                img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), cropFilter));

                imageIoWriter(img, destWidth, destHeight, out);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 合成图片并添加文字
     *
     * @param tempImage   模版图片
     * @param mergedImage 叠加图片
     * @param left        叠加图片左边距
     * @param top         叠加图片上边距
     * @param width       叠加图片宽带
     * @param height      叠加图片高度
     * @param str         文字
     * @param font        文字字体
     * @param fontLeft    文字左边距
     * @param fonTop      文字上边距
     * @param outputFile  最终合成文件
     */
    public static void merged(File tempImage, File mergedImage, int left,
                              int top, int width, int height, String str, Font font,
                              int fontLeft, int fonTop, File outputFile) {
        try {

            /* 加载模版图片*/
            BufferedImage imageLocal = ImageIO.read(tempImage);
            /* 加载叠加图片*/
            BufferedImage imageCode = ImageIO.read(mergedImage);
            Graphics2D g = imageLocal.createGraphics();
            /* 在模板上添加叠加图片(地址,左边距,上边距,图片宽度,图片高度,未知)*/
            g.drawImage(imageCode, left, top, width, height, null);
            /* 添加文本说明*/
            if (str != null) {
                /* 设置文本样式*/
                g.setFont(font);
                g.setColor(Color.RED);
                g.drawString(str, fontLeft, fonTop);
            }
            /* 完成模板修改*/
            g.dispose();
            ImageIO.write(imageLocal, "png", outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 比对两个Image是否相同<br>
     * 根据图片的像素RGB颜色值比较，存在一定误差。
     *
     * @param oldImage 旧图
     * @param newImage 新图
     * @return 是否相同 相同返回true
     */
    public static boolean contrastPixelRGB(BufferedImage oldImage, BufferedImage newImage) {
        if (oldImage == null || newImage == null)
            return false;
        /* 截取色号编码前三位数字用*/
        String sb = null;
        /* 记录不同色号连续出现次数*/
        int conut = 0;

        for (int i = 0; i < oldImage.getWidth(); i += 10) {
            for (int j = 0; j < oldImage.getHeight(); j += 3) {
                /* 比较两张图同一位置色号,并取出前三个数字相比较，不够三个则全部取出*/
                int oldPixel = oldImage.getRGB(i, j);
                sb = String.valueOf(Math.abs(oldPixel));
                oldPixel = Integer.parseInt(sb.substring(0, Math.min(sb.length(), 3)));
                int newPixel = newImage.getRGB(i, j);
                sb = String.valueOf(Math.abs(newPixel));
                newPixel = Integer.parseInt(sb.substring(0, Math.min(sb.length(), 3)));

                /* 比较差大于限定值，则认为该像素点不相同，不行同计数器+1，否则计数器-1直至为0。计数器大于限定值则认为两张图片不同*/
                int pixel = Math.abs(oldPixel - newPixel);
                if (pixel > 300)
                    conut++;
                else if (conut >= 0)
                    conut--;
                if (conut >= 5)
                    return false;
            }
        }
        return true;
    }


    /**
     * RGB值转换颜色分量
     *
     * @param rgbColor rgb颜色值
     * @return {r,g,b}
     */
    public static int[] rgbToColor(int rgbColor) {
        final int r = (rgbColor >> 16) & 0xff;
        final int g = (rgbColor >> 8) & 0xff;
        final int b = rgbColor & 0xff;
        return new int[]{r, g, b};
    }

    /**
     * 颜色分量转换RGB值
     *
     * @param alpha 透明度
     * @param red   红色
     * @param green 绿色
     * @param blue  蓝色
     * @return rgb颜色值
     */
    public static int colorToRGB(int alpha, int red, int green, int blue) {
        int newPixel = 0;
        newPixel += alpha;
        newPixel = newPixel << 8;
        newPixel += red;
        newPixel = newPixel << 8;
        newPixel += green;
        newPixel = newPixel << 8;
        newPixel += blue;
        return newPixel;
    }


    /**
     * 灰度化
     *
     * @param pattern 灰度化方式：1最大值法、2最小值法、3均值法、4加权法
     * @param image   原文件
     * @return 灰度化图片
     */
    public static BufferedImage grayImage(int pattern, BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        BufferedImage grayImage = new BufferedImage(width, height, image.getType());
        /*        BufferedImage grayImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY); */

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = image.getRGB(i, j);
                int gray = getGray(pattern, color);
                grayImage.setRGB(i, j, colorToRGB(0, gray, gray, gray));
            }
        }
        return grayImage;
    }

    /**
     * 获取灰度值
     *
     * @param pattern 灰度化方式：1最大值法、2最小值法、3均值法、4加权法
     * @param color   RGB 颜色值
     * @return 灰度值
     */
    private static int getGray(int pattern, int color) {
        final int r = (color >> 16) & 0xff;
        final int g = (color >> 8) & 0xff;
        final int b = color & 0xff;
        int gray = 0;
        if (pattern == 1) {
            gray = r >= g && r >= b ? r : Math.max(g, b);  /* 最大值法灰度化*/
        } else if (pattern == 2) {
            gray = r <= g && r <= b ? r : Math.min(g, b); /* 最小值法灰度化*/
        } else if (pattern == 3) {
            gray = (r + g + b) / 3;  /* 均值法灰度化*/
        } else if (pattern == 4) {
            gray = (int) (0.3 * r + 0.59 * g + 0.11 * b); /* 加权法灰度化*/
        }
        return gray;
    }

    /***
     * 重置背景色
     * @param rbc 重置后的背景色：0-255
     * @param image 原文件
     * @return 重置背景色后的图像
     */
    public static BufferedImage resetBottomColour(int rbc, BufferedImage image) {
        return resetBottomColour(30, rbc, image);
    }

    /**
     * 重置背景色
     *
     * @param offset 允许误差值偏移量
     * @param rbc    重置后的背景色：0-255
     * @param image  原文件
     * @return 重置背景色后的图像
     */
    public static BufferedImage resetBottomColour(int offset, int rbc, BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();

        int bcAuto = getRGBColorMax(image, colorToRGB(0, rbc, rbc, rbc));

        BufferedImage grayImage = new BufferedImage(width, height, image.getType());
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = image.getRGB(i, j);

                if (equalsRGBPrint(offset, color, bcAuto)) {
                    grayImage.setRGB(i, j, colorToRGB(0, rbc, rbc, rbc));
                } else {
                    grayImage.setRGB(i, j, color);
                }
            }
        }
        return grayImage;
    }

    /**
     * 获取图像平均像素点
     *
     * @param image     图像文件
     * @param skipColor 跳过，不参与统计的颜色
     * @return 图像平均像素点
     */
    public static int getRGBColorAvg(BufferedImage image, Integer... skipColor) {
        image = setImageRedraw2D(image, 64, 64);
        int width = image.getWidth();
        int height = image.getHeight();
        List<Integer> skipRgbColor = Arrays.asList(skipColor);
        int r = 0;
        int g = 0;
        int b = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = image.getRGB(i, j);
                if (!skipRgbColor.contains(color)) {
                    r = (color >> 16) & 0xff;
                    g = (color >> 8) & 0xff;
                    b = color & 0xff;
                    if (j != 0 || i != 0) {
                        r /= 2;
                        g /= 2;
                        b /= 2;
                    }
                }
            }
        }
        return colorToRGB(0, r, g, b);
    }

    /**
     * 获取图像最多的像素点
     *
     * @param image     图像文件
     * @param skipColor 跳过，不参与统计的颜色
     * @return 图像最多的像素点 RGB
     */
    public static int getRGBColorMax(BufferedImage image, Integer... skipColor) {
        image = setImageRedraw2D(image, 64, 64);
        int width = image.getWidth();
        int height = image.getHeight();
        List<Integer> skipRgbColor = Arrays.asList(skipColor);
        Map<Integer, Integer> count = new HashMap<>();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int color = image.getRGB(i, j);
                if (!skipRgbColor.contains(color)) {
                    count.put(color, count.containsKey(color) ? count.get(color) + 1 : 0);
                }
            }
        }

        AtomicInteger max = new AtomicInteger(0);
        AtomicInteger bcAuto = new AtomicInteger(0);
        count.forEach((k, v) -> {
            if (v > max.get()) {
                max.set(v);
                bcAuto.set(k);
            }
        });
        return bcAuto.get();
    }

    /**
     * 判断RGB像素点是否相同
     *
     * @param offset 允许误差值偏移量
     * @param color1 像素点1
     * @param color2 像素点2
     * @return 在偏移量内返回true，超出则返回false
     */
    public static boolean equalsRGBPrint(int offset, int color1, int color2) {

        int[] rgb1 = rgbToColor(color1);
        int[] rgb2 = rgbToColor(color2);

        for (int i = 0; i < rgb1.length; i++) {
            if (Math.abs(Math.abs(rgb1[i]) - Math.abs(rgb2[i])) >= offset) {
                return false;
            }
        }
        return true;
    }

    /**
     * 图像二值化
     *
     * @param image 原图像
     * @param sw    设置二值化的阈值
     * @return 二值化后的图像
     */
    public static BufferedImage binaryImage(BufferedImage image, double sw) {
        BufferedImage binaryImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);

        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {

                /* 计算像素的灰度值 */
                int gray = (image.getRGB(x, y) >> 8) & 0xFF;

                /* 设置二值化后的图像的像素 */
                binaryImage.setRGB(x, y, gray > sw ? colorToRGB(0, 255, 255, 255) : colorToRGB(0, 0, 0, 0));
            }
        }
        return binaryImage;
    }

    /**
     * 判断像素点是否为指定颜色
     *
     * @param image     图像文件
     * @param offset    色差补偿值
     * @param printType 指定像素点位置
     * @param rgb       比较的颜色
     * @param x         当前像素横坐标
     * @param y         当前像素纵坐标
     * @return true：是指定颜色，false：不是指定颜色
     */
    public static boolean isRGB(BufferedImage image, int offset, String printType, int rgb, int x, int y) {
        if (image == null || x > image.getWidth() - 1 || y > image.getHeight() - 1) {
            return false;
        }

        int xLast = x - 1;
        int xNext = x + 1;
        int yLast = y - 1;
        int yNext = y + 1;

        int center = image.getRGB(x, y);  /* 中间像素点 */
        int centerTop = xLast < 0 || xLast >= image.getWidth() ? -1 : Math.abs(image.getRGB(xLast, y));  // 上侧像素点
        int centerBottom = xNext < 0 || xNext >= image.getWidth() ? -1 : Math.abs(image.getRGB(xNext, y)); // 下侧像素点
        int leftCenter = yLast < 0 || yLast >= image.getHeight() ? 0 : Math.abs(image.getRGB(x, yLast)); // 左侧像素点
        int rightCenter = yNext < 0 || yNext >= image.getHeight() ? 0 : Math.abs(image.getRGB(x, yNext)); // 右侧像素点
        int leftTop = xLast < 0 || xLast >= image.getWidth() || yLast < 0 || yLast >= image.getHeight() ? -1 : Math.abs(image.getRGB(xLast, yLast)); // 左上侧像素点
        int leftBottom = xNext < 0 || xNext >= image.getWidth() || yLast < 0 || yLast >= image.getHeight() ? -1 : Math.abs(image.getRGB(xNext, yLast)); // 左下侧像素点
        int rightTop = xLast < 0 || xLast >= image.getWidth() || yNext < 0 || yNext >= image.getHeight() ? -1 : Math.abs(image.getRGB(xLast, yNext)); // 右上侧像素点
        int rightBottom = xNext < 0 || xNext >= image.getWidth() || yNext < 0 || yNext >= image.getHeight() ? -1 : Math.abs(image.getRGB(xNext, yNext)); // 右下侧像素点


        switch (printType) {
            case PrintType_IsTop:  /* 顶部三个像素都是白色 */
                return isRGB(offset, rgb, leftTop, centerTop, rightTop);
            case PrintType_IsBottom:  /* 底侧三个像素都是白色 */
                return isRGB(offset, rgb, leftBottom, centerBottom, rightBottom);
            case PrintType_IsLeft:  /* 左侧三个都是白色 */
                return isRGB(offset, rgb, leftTop, leftCenter, leftBottom);
            case PrintType_IsRight:  /* 右侧三个都是白色 */
                return isRGB(offset, rgb, rightTop, rightCenter, rightBottom);
            case PrintType_IsCenterTop:  /* 中间顶部像素是白色 */
                return isRGB(offset, rgb, centerTop);
            case PrintType_IsCenterBottom:  /* 中间底部像素是白色 */
                return isRGB(offset, rgb, centerBottom);
            case PrintType_IsLeftTop:  /* 左上角像素是白色 */
                return isRGB(offset, rgb, leftTop);
            case PrintType_IsLeftCenter:  /* 左侧像素是白 */
                return isRGB(offset, rgb, leftCenter);
            case PrintType_IsLeftBottom:  /* 左下角像素是白色 */
                return isRGB(offset, rgb, leftBottom);
            case PrintType_IsRightTop:  /* 右上角像素是白色 */
                return isRGB(offset, rgb, rightTop);
            case PrintType_IsRightCenter:  /* 右侧像素是白 */
                return isRGB(offset, rgb, rightCenter);
            case PrintType_IsRightBottom:  /* 右下角像素是白色 */
                return isRGB(offset, rgb, rightBottom);
            default:
                return false;
        }
    }

    private static boolean isRGB(int offset, int rgb, int... printRGB) {
        rgb = Math.abs(rgb);
        for (int item : printRGB) {
            if (Math.abs(item - rgb) > offset) {
                return false;
            }
        }
        return true;
    }

    public static boolean isBody(BufferedImage image, int x, int y) {
        if (x < 1 || y < 1 || x > image.getWidth() - 2 || y > image.getHeight() - 2) {
            return false;
        }

        int xLast = x - 1;
        int xNext = x + 1;
        int yLast = y - 1;
        int yNext = y + 1;

        /* 计算像素的灰度值 */
        int gray = (image.getRGB(x, y) >> 8) & 0xFF;
        int content = 0;
        /* 左侧像素点比较 */
        if (gray == ((image.getRGB(xLast, y) >> 8) & 0xFF)) {
            content++;
        }
        /* 右侧像素点比较 */
        if (gray == ((image.getRGB(xNext, y) >> 8) & 0xFF)) {
            content++;
        }
        /* 上侧像素点比较 */
        if (gray == ((image.getRGB(x, yLast) >> 8) & 0xFF)) {
            content++;
        }
        /* 下侧像素点比较 */
        if (gray == ((image.getRGB(x, yNext) >> 8) & 0xFF)) {
            content++;
        }
        /* 左上侧像素点比较 */
        if (gray == ((image.getRGB(xLast, yLast) >> 8) & 0xFF)) {
            content++;
        }
        /* 左下侧像素点比较 */
        if (gray == ((image.getRGB(xLast, yNext) >> 8) & 0xFF)) {
            content++;
        }
        /* 右上侧像素点比较 */
        if (gray == ((image.getRGB(xNext, yLast) >> 8) & 0xFF)) {
            content++;
        }
        /* 右下侧像素点比较 */
        if (gray == ((image.getRGB(xNext, yNext) >> 8) & 0xFF)) {
            content++;
        }
        return content > 3;
    }


    public static void main(String[] args) {

        BufferedImage image = ImageUtil.readImage(Paths.get("E:\\data\\身份证1.jpg"));
//        BufferedImage image = ImageUtil.readImage(new File("E:\\data\\4224818.jpg"));


        // 转换为灰度图像
//        image = ImageUtil.grayImage(3,image);
        // 高斯滤波
        // 计算图像梯度和方向

        // 非最大抑制
        // 双边滤波
        // 计算边缘置信度
        // 最小二乘法

        /*
        1. 首先，需要将图像进行二值化处理，将图像转换为只有黑白两色的图像。这可以通过使用图像处理库（如OpenCV）中的阈值函数来实现。
        2. 接下来，需要进行图像的边缘检测。边缘是图像中亮度变化比较明显的部分，可以通过使用图像处理库中的边缘检测算法（如Canny算法）来实现。
        3. 然后，需要计算图像的倾斜角度。可以使用Hough变换来检测直线，并计算出直线的倾斜角度。具体实现时，可以先检测出图像中的四条边，然后计算其中两条边的倾斜角度，取其平均值作为图像的倾斜角度。
        4. 最后，可以对图像进行旋转校正，将其调整为水平或垂直方向。可以使用图像处理库中的旋转函数，将图像根据计算得到的倾斜角度进行旋转。
         */


        image = ImageUtil.adjustBrightness(image, 50);
//        image = ImageUtil.sharpen(image);
        image = ImageUtil.binaryImage(image, 180);

//        double angle = ImageUtil.getTiltAngle(image);
//        System.out.println(angle);


        image = setImageRedraw2D(image, image.getWidth() * 2, image.getHeight() * 2);
        ImageUtil.writeImage(image, new File("E:\\data\\sharpen001.jpg"), "jpg");

    }
}
