package com.ck.tools.utils;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 线程池工具类
 *
 * @author cyk
 * @since 2020-01-01
 */
public class ThreadPoolUtil {
    private static final Logger log = Logger.getLogger(ThreadPoolUtil.class.getName());

    /**
     * 执行Runnable任务
     *
     * @param task 任务
     */
    public static void run(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }

    /**
     * 执行Runnable任务
     *
     * @param executor 线程池
     * @param tasks    任务列表
     */
    public static void run(Executor executor, Runnable... tasks) {
        Objects.requireNonNull(executor, "executor is null");
        Objects.requireNonNull(tasks, "tasks is null");

        Stream.of(tasks).forEach(executor::execute);
    }

    /**
     * 线程休眠
     *
     * @param millis 毫秒值
     */
    public static void sleep(int millis) {
        try {
            if (millis > 0)
                Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException("Thread sleep Error ", e);
        }
    }

    /**
     * 获取线程循环执行,并使用外部线程控制循环终止
     *
     * @param task   任务
     * @param millis 每次循环线程休眠时长/毫秒值
     * @param onOff  循环终止开关
     */
    public static void runWhileSync(Runnable task, int millis, AtomicBoolean onOff) {
        new Thread(() -> {
            while (onOff != null && onOff.get()) {
                task.run();
                sleep(millis);
            }
        });
    }

    /**
     * 启动定时器，循环执行任务
     *
     * @param task   任务
     * @param first  首次执行延迟/毫秒值
     * @param period 每次运行间隔/毫秒值
     * @return 返回定时器对象
     */
    public static Timer schedule(Runnable task, int first, int period) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                task.run();
            }
        }, first, period);

        return timer;
    }

    /**
     * 启动定时器,执行一次任务
     *
     * @param task  任务
     * @param first 首次执行延迟/毫秒值
     * @return 返回定时器对象
     */
    public static Timer schedule(Runnable task, int first) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                task.run();
            }
        }, first);
        return timer;
    }

    /**
     * 创建 ThreadPoolExecutor 线程池
     *
     * @param threadName    创建的线程名称 默认 ThreadPool-Class.getSimpleName-线程序号
     * @param corePoolSize  最小(初始)线程数 默认2
     * @param maxPoolSize   最大线程数 默认6
     * @param keepAliveTime 大于最小线程数之外的闲置线程保持存活时间，单位毫秒 默认最小1000L
     * @param queueCapacity 任务阻塞队列长度 默认最小50
     * @param handler       最大线程数量及最大阻塞队列全部被占用是的处理逻辑，true 继续阻塞添加到队列，false 抛弃任务。默认true
     * @return ThreadPoolExecutor 线程池
     */
    public static ThreadPoolExecutorAutoCloseable createThreadPoolExecutor(String threadName, int corePoolSize, int maxPoolSize, long keepAliveTime, int queueCapacity, boolean handler) {
        AtomicInteger number = new AtomicInteger(0);
        if (threadName == null || threadName.trim().isEmpty())
            threadName = "ThreadPool-" + ThreadPoolUtil.class.getSimpleName();
        String finalThreadName = ThreadPoolUtil.class.getSimpleName() + "-" + threadName;
        return new ThreadPoolExecutorAutoCloseable(Math.min(corePoolSize, 2), Math.max(maxPoolSize, 6), Math.max(keepAliveTime, 1000L),
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(Math.max(queueCapacity, 50)), (Runnable runnable) -> new Thread(runnable, finalThreadName + "-" + number.getAndIncrement())
                , (Runnable runnable, ThreadPoolExecutor executor) -> {
            if (!handler) {
                log.info(String.format("Thread pool [%s] is exhausted, executor=%s", finalThreadName, executor.toString()));
            } else if (!executor.isShutdown()) {
                try {
                    /* 添加一个元素， 如果队列满，则阻塞 */
                    executor.getQueue().put(runnable);
                } catch (InterruptedException e) {
                    throw new RuntimeException(String.format("线程池[%s],队列中添加任务异常", finalThreadName), e);
                }
            }
        });
    }

    /**
     * 等待获取执行状态,并消费抛出的异常
     *
     * @param future 线程执行状态收集器
     * @param err    线程异常消费
     */
    public static void getFuture(CompletableFuture<Void> future, Consumer<Throwable> err) {
        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            err.accept(e);
        }
    }

    /**
     * 等待获取执行状态,抛出线程异常
     *
     * @param future 线程执行状态收集器
     */
    public static void getFuture(CompletableFuture<Void> future) {
        getFuture(future, e -> {
            throw new RuntimeException(e);
        });
    }

    /**
     * 批量任务执行，异步执行
     *
     * @param tasks 任务函数
     */
    public static CompletableFuture<Void> runBatchTask(Runnable... tasks) {
        try (ThreadPoolExecutorAutoCloseable threadPool =
                     createThreadPoolExecutor("RunBatchTask", 2, 8, 1000 * 30, 500, true)) {
            return runBatchTask(threadPool, tasks);
        }
    }

    /**
     * 批量任务执行
     *
     * @param tasks 任务函数
     */
    public static CompletableFuture<Void> runBatchTask(Executor executor, Runnable... tasks) {
        return CompletableFuture.allOf(Stream.of(tasks)
                .map(r -> CompletableFuture.runAsync(r, executor)).toArray(CompletableFuture[]::new));
    }

    /**
     * 批量任务执行
     *
     * @param collection 任务数据集合
     * @param task       任务函数
     * @param <T>        任务函数参数泛型
     */
    public static <T> CompletableFuture<Void> runBatchTask(Collection<T> collection, Consumer<? super T> task) {
        return runBatchTask(8, collection, (index, t) -> task.accept(t));
    }

    /**
     * 批量任务执行
     *
     * @param collection 任务数据集合
     * @param task       任务函数
     * @param <T>        任务函数参数泛型
     */
    public static <T> CompletableFuture<Void> runBatchTask(Collection<T> collection, BiConsumer<Integer, ? super T> task) {
        return runBatchTask(8, collection, task);
    }

    /**
     * 批量任务执行
     *
     * @param maxPoolSize 线程池最大线程数
     * @param collection  任务数据集合
     * @param task        任务函数
     * @param <T>         任务函数参数泛型
     */
    public static <T> CompletableFuture<Void> runBatchTask(int maxPoolSize, Collection<T> collection, Consumer<? super T> task) {
        return runBatchTask(maxPoolSize, collection, (index, t) -> task.accept(t));
    }

    /**
     * 批量任务执行
     *
     * @param maxPoolSize 线程池最大线程数
     * @param collection  任务数据集合
     * @param task        任务函数
     * @param <T>         任务函数参数泛型
     */
    public static <T> CompletableFuture<Void> runBatchTask(int maxPoolSize, Collection<T> collection, BiConsumer<Integer, ? super T> task) {
        return runBatchTask(null, maxPoolSize, collection, task);
    }

    /**
     * 批量任务执行
     *
     * @param threadName  创建的线程名称 默认 BatchTask-RunBatchTask-线程序号
     * @param maxPoolSize 线程池最大线程数
     * @param collection  任务数据集合
     * @param task        任务函数
     * @param <T>         任务函数参数泛型
     */
    public static <T> CompletableFuture<Void> runBatchTask(String threadName, int maxPoolSize, Collection<T> collection, BiConsumer<Integer, ? super T> task) {

        threadName = threadName == null || threadName.trim().isEmpty() ? "BatchTask-RunBatchTask" : threadName;

        if (collection == null || collection.isEmpty()) return null;
        int queueCapacity = Math.max(100, collection.size());

        try (ThreadPoolExecutorAutoCloseable executor = createThreadPoolExecutor(threadName, 2, maxPoolSize, 1000 * 30, queueCapacity, true)) {
            return runBatchTask(executor, collection, task);
        }
    }

    /**
     * 批量任务执行
     *
     * @param executor   线程池
     * @param collection 任务数据集合
     * @param task       任务函数
     * @param <T>        任务函数参数泛型
     */
    public static <T> CompletableFuture<Void> runBatchTask(Executor executor, Collection<T> collection, BiConsumer<Integer, ? super T> task) {

        if (collection == null || collection.isEmpty() || executor == null) return null;
        AtomicInteger index = new AtomicInteger(0);
        return CompletableFuture.allOf(
                collection.stream().map(i -> CompletableFuture.runAsync(() ->
                        task.accept(index.getAndIncrement(), i), executor)).toArray(CompletableFuture[]::new));
    }

    /**
     * 批量任务执行
     *
     * @param executor   线程池
     * @param collection 任务数据集合
     * @param task       任务函数
     * @param <K>        Map Key 类型
     * @param <V>        Map Value 类型
     */
    public static <K, V> CompletableFuture<Void> runBatchTask(Executor executor, Map<K, V> collection, BiConsumer<K, V> task) {

        if (collection == null || collection.isEmpty() || executor == null) return null;

        List<CompletableFuture<Void>> futures = new ArrayList<>();
        collection.forEach((k, v) -> {
            CompletableFuture<Void> future = CompletableFuture.runAsync(() ->
                    task.accept(k, v), executor
            );
            futures.add(future);
        });

        return CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
    }

    /**
     * 按照指定长度裁切List为多份，最后不满足长度的剩余数据为一份
     *
     * @param size   每份长度
     * @param source 原集合
     * @param <T>    集合对象泛型
     * @return 分割后的集合列表
     */
    public static <T> List<List<T>> splitListBySize(int size, Collection<T> source) {
        if (source == null) return null;
        if (size < 1) size = source.size();

        int finalSize = size;
        long limit = (long) Math.ceil(source.size() / (double) size);
        return Stream.iterate(0, i -> i + 1).limit(limit)
                .map(i -> source.stream().skip((long) i * finalSize).limit(finalSize).collect(Collectors.toList())).collect(Collectors.toList());
    }

    /**
     * 将集合均分为指定的份数
     *
     * @param copies 份数
     * @param source 原集合
     * @param <T>    集合对象泛型
     * @return 分割后的集合列表
     */
    public static <T> List<List<T>> splitListByAverage(int copies, Collection<T> source) {
        if (source == null) return null;
        if (copies < 1 || copies > source.size()) copies = source.size();
        long limit = (source.size() + copies - 1) / copies;
        return Stream.iterate(0, n -> n + 1).limit(copies)
                .map(a -> source.stream().skip(a * limit).limit(limit).collect(Collectors.toList())).collect(Collectors.toList());
    }

    /**
     * 集合长度大于1多线程执行
     *
     * @param collection 集合
     * @param task       任务
     * @param <T>        集合对象泛型
     */
    public static <T> void forEach(Collection<T> collection, BiConsumer<Integer, ? super T> task) {
        forEach(2, collection, task);
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param asyncSize  大于等于指定的长度进行多线程执行
     * @param collection 集合
     * @param task       任务
     * @param <T>        集合对象泛型
     */
    public static <T> void forEach(int asyncSize, Collection<T> collection, BiConsumer<Integer, ? super T> task) {
        forEach(8, asyncSize, collection, task);
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param maxPoolSize 最大线程数 默认6
     * @param asyncSize   大于等于指定的长度进行多线程执行
     * @param collection  集合
     * @param task        任务
     * @param <T>         集合对象泛型
     */
    public static <T> void forEach(int maxPoolSize, int asyncSize, Collection<T> collection, BiConsumer<Integer, ? super T> task) {
        forEach(2, maxPoolSize, asyncSize, collection, task);
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param corePoolSize 最小(初始)线程数 默认2
     * @param maxPoolSize  最大线程数 默认6
     * @param asyncSize    大于等于指定的长度进行多线程执行
     * @param collection   集合
     * @param task         任务
     * @param <T>          集合对象泛型
     */
    public static <T> void forEach(int corePoolSize, int maxPoolSize, int asyncSize, Collection<T> collection, BiConsumer<Integer, ? super T> task) {
        if (collection == null) return;
        if (collection.size() >= asyncSize) {
            try (ThreadPoolExecutorAutoCloseable executor = createThreadPoolExecutor("ForEachOrDoWhile", corePoolSize, maxPoolSize, 1000 * 30, Math.max((int)(collection.size() * 0.75), 1), true)) {
                CompletableFuture<Void> future = runBatchTask(executor, collection, task);
                getFuture(future);
            }
        } else {
            AtomicInteger index = new AtomicInteger();
            collection.forEach(i -> task.accept(index.getAndIncrement(), i));
        }
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param maxPoolSize 最大线程数 默认6
     * @param asyncSize   大于等于指定的长度进行多线程执行
     * @param array       数组
     * @param task        任务
     * @param <T>         集合对象泛型
     */
    public static <T> void forEach(int maxPoolSize, int asyncSize, T[] array, BiConsumer<Integer, ? super T> task) {
        forEach(2, maxPoolSize, asyncSize, array, task);
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param corePoolSize 最小(初始)线程数 默认2
     * @param maxPoolSize  最大线程数 默认6
     * @param asyncSize    大于等于指定的长度进行多线程执行
     * @param array        数组
     * @param task         任务
     * @param <T>          集合对象泛型
     */
    public static <T> void forEach(int corePoolSize, int maxPoolSize, int asyncSize, T[] array, BiConsumer<Integer, ? super T> task) {
        forEach(corePoolSize, maxPoolSize, asyncSize, Arrays.asList(array), task);
    }

    /**
     * 集合长度大于1多线程执行
     *
     * @param map  集合
     * @param task 任务
     * @param <K>  Map Key 类型
     * @param <V>  Map Value 类型
     */
    public static <K, V> void forEach(Map<K, V> map, BiConsumer<K, V> task) {
        forEach(2, map, task);
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param asyncSize 大于等于指定的长度进行多线程执行
     * @param map       集合
     * @param task      任务
     * @param <K>       Map Key 类型
     * @param <V>       Map Value 类型
     */
    public static <K, V> void forEach(int asyncSize, Map<K, V> map, BiConsumer<K, V> task) {
        forEach(8, asyncSize, map, task);
    }

    /**
     * 按照指定的集合长度来判断是否需要多线程执行
     *
     * @param maxPoolSize 最大线程数 默认6
     * @param asyncSize   大于等于指定的长度进行多线程执行
     * @param map         集合
     * @param task        任务
     * @param <K>         Map Key 类型
     * @param <V>         Map Value 类型
     */
    public static <K, V> void forEach(int maxPoolSize, int asyncSize, Map<K, V> map, BiConsumer<K, V> task) {
        if (map == null || map.isEmpty()) return;

        if (map.size() >= asyncSize) {
            try (ThreadPoolExecutorAutoCloseable executor = createThreadPoolExecutor("ForEachOfMap", 2, maxPoolSize, 1000 * 30, Math.max((int)(map.size() * 0.75), 1), true)) {
                CompletableFuture<Void> future = runBatchTask(executor, map, task);
                getFuture(future);
            }
        } else {
            map.forEach(task);
        }
    }

    /**
     * 多线程执行任务 模仿 do-while 操作
     *
     * @param maxPoolSize 最大线程数
     * @param asyncSize   大于等于指定的长度进行多线程执行
     * @param task        任务，第一次任务返回的数值作为总任务执行次数; 任务参数为 当前执行次数 第一次为1 第二次为2 以此类推
     */
    public static void doWhile(int maxPoolSize, int asyncSize, Function<Long, Long> task) {
        doWhile(2, maxPoolSize, asyncSize, task);
    }

    /**
     * 多线程执行任务 模仿 do-while 操作
     *
     * @param corePoolSize 最小(初始)线程数 默认2
     * @param maxPoolSize  最大线程数
     * @param asyncSize    大于等于指定的长度进行多线程执行
     * @param task         任务，第一次任务返回的数值作为总任务执行次数; 任务参数为 当前执行次数 第一次为1 第二次为2 以此类推
     */
    public static void doWhile(int corePoolSize, int maxPoolSize, int asyncSize, Function<Long, Long> task) {
        long indexInit = 1L;
        Long total = task.apply(indexInit);
        if (total == null || total <= indexInit) return;

        List<Long> indexes = new ArrayList<>();
        while (indexInit < total) {
            indexes.add(++indexInit);
        }
        forEach(corePoolSize, maxPoolSize, asyncSize, indexes, (i, index) -> task.apply(index));
    }

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("字符串1", "字符串2", "字符串3", "字符串4", "字符串5", "字符串6", "字符串7", "字符串8", "字符串9");

        System.out.println("\r---------------------------执行异步任务------------------------------");
        run(() -> System.out.println("执行异步任务"));

        sleep(2000);
        System.out.println("\r---------------------------线程休眠------------------------------");

        System.out.println("\r---------------------------获取线程循环执行,并使用外部线程控制循环终止------------------------------");
        AtomicBoolean off = new AtomicBoolean(true);
        runWhileSync(() -> System.out.println("外部控制异步循环"), 1000, off);
        off.set(false);

        System.out.println("\r---------------------------启动定时器，循环执行任务------------------------------");
        Timer timer = schedule(() -> System.out.println("启动定时器，循环执行任务"), 1000, 1000);
        timer.cancel();

        System.out.println("\r---------------------------启动定时器,执行一次任务------------------------------");
        timer = schedule(() -> System.out.println("启动定时器，循环执行任务"), 1000);
        timer.cancel();

        System.out.println("\r---------------------------创建 ThreadPoolExecutor 线程池------------------------------");
        long time = System.currentTimeMillis();
        ThreadPoolExecutor executor = createThreadPoolExecutor("BatchTask-RunBatchTask", 2, 10, 1000 * 30, 1000, true);
        System.out.println("创建 ThreadPoolExecutor 线程池耗时：" + (System.currentTimeMillis() - time) + " ms");

        System.out.println("\r---------------------------批量任务执行,不阻断主流程------------------------------");
        CompletableFuture<Void> future = runBatchTask(executor, strings, (index, s) -> System.out.println(s + "   " + index));
        System.out.println("批量任务执行中......");
        executor.shutdown(); /* 手动创建的线程池务必要关闭 */

        System.out.println("\r---------------------------等待获取执行状态,并消费抛出的异常------------------------------");
        getFuture(future, e -> System.out.println(e.getMessage()));

        System.out.println("\r---------------------------按照指定长度裁切List为多份------------------------------");
        splitListBySize(2, strings).forEach(i -> System.out.println(i.size()));

        System.out.println("\r---------------------------将集合均分为指定的份数------------------------------");
        splitListByAverage(3, strings).forEach(i -> System.out.println(i.size()));

        System.out.println("\r---------------------------多线程遍历集合------------------------------");
        forEach(2, strings, (index, s) -> System.out.println(s + "   " + index));

        System.out.println("\r---------------------------多线程模拟DoWhile遍历------------------------------");
        doWhile(2, 2, index -> {
            System.out.println(index);
            return 5L;
        });
    }

    /**
     * 线程池实现 AutoCloseable 接口
     */
    public static class ThreadPoolExecutorAutoCloseable extends ThreadPoolExecutor implements AutoCloseable {

        public ThreadPoolExecutorAutoCloseable(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        }

        public ThreadPoolExecutorAutoCloseable(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
        }

        public ThreadPoolExecutorAutoCloseable(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
        }

        public ThreadPoolExecutorAutoCloseable(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        }

        @Override
        public void close() {
            shutdown();
        }
    }
}
