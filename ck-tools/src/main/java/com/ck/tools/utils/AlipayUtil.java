package com.ck.tools.utils;

import com.ck.tools.encrypt_decrypt.RSAUtil;

import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * 支付宝相关应用工具
 *
 * @author cyk
 * @since 2024-01-01
 */
public class AlipayUtil extends WebNetUtil {
    private static final Logger log = Logger.getLogger(WebNetUtil.class.getName());


    /**
     * 获取支付宝外部二维码跳转授权地址
     *
     * @param redirectUri 支付宝授权后回调地址
     * @param appId       应用ID
     * @param scope       授权方式：auth_base（静默客户无感）、auth_user（用户授权，默认）
     * @return 跳转地址
     */
    public static String getAlipayAuthUrlQRCode(String appId, String scope, String redirectUri) {
        return getAlipayQRCode(getAlipayAuthUrl(appId, scope, redirectUri));
    }

    /**
     * 获取支付宝授权地址
     *
     * @param redirectUri 支付宝授权后回调地址
     * @param appId       应用ID
     * @param scope       授权方式：auth_base（静默客户无感）、auth_user（用户授权，默认）
     * @return 跳转地址
     */
    public static String getAlipayAuthUrl(String appId, String scope, String redirectUri) {
        if (!"auth_base".equals(scope) && !"auth_user".equals(scope)) {
            scope = "auth_user";
        }
        if (redirectUri == null || redirectUri.trim().isEmpty() || appId == null || appId.trim().isEmpty()) {
            throw new IllegalArgumentException("参数错误, appId:" + appId + ", scope:" + scope + ", redirectUri:" + redirectUri);
        }

        // 阿里授权地址
        return "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?"
                + "app_id=" + appId
                + "&scope=" + scope
                + "&redirect_uri=" + WebNetUtil.urlEncode(redirectUri, StandardCharsets.UTF_8);
    }

    /**
     * 获取支付宝二维码地址
     *
     * @param qrcodeUrl 二维码内容
     * @return 跳转地址
     */
    public static String getAlipayQRCode(String qrcodeUrl) {
        // 二维码跳转地址    唤起支付宝扫一扫 appId=10000007
        String qrcode = "alipays://platformapi/startapp?appId=10000007&qrcode=" + WebNetUtil.urlEncode(qrcodeUrl, StandardCharsets.UTF_8);

        // scheme 跳转地址
        return "https://ds.alipay.com/?scheme=" + WebNetUtil.urlEncode(qrcode, StandardCharsets.UTF_8);
    }


    /**
     * 换取授权Token
     *
     * @param authCode        授权码
     * @param appId           应用ID
     * @param algorithm       签名算法
     * @param appPrivateKey   应用私钥
     * @param alipayPublicKey 支付宝公钥
     * @param callback        回调
     * @return 授权Token内容 验签失败返回null
     */
    public static String authToken_V1(String authCode, String appId, String algorithm, String appPrivateKey, String alipayPublicKey, BiConsumer<Boolean, String> callback) {
        String msg = String.format("authCode:%s,appId:%s"
                , authCode, appId);

        // 构建请求全部参数
        Map<String, String> param = new LinkedHashMap<>();
        param.put("app_id", appId);
        param.put("charset", "UTF-8");
        param.put("format", "json");
        param.put("method", "alipay.system.oauth.token");
        param.put("sign_type", RSAUtil.Algorithm_SSHA256WithRSA.equals(algorithm) ? "RSA2" : "RSA");
        param.put("timestamp", TimeUtil.formatDate_S(TimeUtil.now()));
        param.put("version", "1.0");
        param.put("grant_type", "authorization_code");
        param.put("code", authCode);

        // 参数签名
        String paramStr = param.keySet().stream().sorted().map(k -> k + "=" + param.get(k)).collect(Collectors.joining("&"));
        String signStr = RSAUtil.sign(paramStr, RSAUtil.getPrivateKey(appPrivateKey), algorithm);
        param.put("sign", signStr);

        // 剔除 form-data参数后进行参数拼接
        param.remove("grant_type");
        param.remove("code");
        paramStr = param.keySet().stream().sorted()
                .map(k -> k + "=" + WebNetUtil.urlEncode(param.get(k), StandardCharsets.UTF_8))
                .collect(Collectors.joining("&"));

        // 调用支付宝获取Token 并验证结果 并返回Token
        return WebNetUtil.sendFormData("https://openapi.alipay.com/gateway.do", WebNetUtil.POST, paramStr, form -> {
            form.put("grant_type", "authorization_code");
            form.put("code", authCode);
        }, res -> {
            // 验签状态
            boolean isVerify = false;
            String signParam = null;

            String signKey = "sign";
            String paramKey = "alipay_system_oauth_token_response";

            if (res != null && !res.trim().isEmpty() && res.contains(paramKey) && res.contains(signKey)) {
                signParam = res.substring(res.indexOf(paramKey) + paramKey.length());
                signParam = signParam.substring(signParam.indexOf("{"));
                signParam = signParam.substring(0, signParam.indexOf("}") + 1);

                String signVal = res.substring(res.indexOf(signKey) + signKey.length());
                signVal = signVal.substring(0, signVal.indexOf("}"));
                signVal = signVal.replace("\"", "").replace(":", "").replace(" ", "");

                try {
                    isVerify = RSAUtil.verify(signParam, RSAUtil.getPublicKey(alipayPublicKey), signVal, algorithm, StandardCharsets.UTF_8);
                    if (!isVerify) {
                        log.info("调用支付宝获取Token验签失败," + msg + ", signParam: " + signParam + ", sign: " + signVal);
                    }
                } catch (Exception e) {
                    log.info("调用支付宝获取Token验签异常," + msg + ", algorithm: " + algorithm + ", signParam: " + signParam + ", sign: " + signVal);
                }
            }
            if (callback != null) {
                callback.accept(isVerify, signParam == null ? res : signParam);
            }
            return isVerify ? signParam : null;
        });
    }


    public static void main(String[] args) {
        System.out.println("\r---------------------------支付宝跳转地址------------------------------");
        System.out.println(getAlipayAuthUrlQRCode("2021004165691692", "auth_base", "https://tiger-uat.hinsurance.cn/public/tiger-api/aliPay/callBack?service_code=ALI_PAY_LETTER_CES&business_no=TEST123456789"));

        System.out.println("\r---------------------------支付宝换取Token------------------------------");
        authToken_V1("411de59a14c44e5a9a700bff8123VX94", "2021004165691692", RSAUtil.Algorithm_SSHA256WithRSA
                , "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCzsHT3zWrY9hh4cQ4sY/+CQ" +
                        "4rvIxL+6PkrZ5uAbRX3xZ0c7O4J8R1kFJIB7QQ6HwoLPrQavwT/zZGmwPcQNzVv0VoamE1EgqmQe/M6jF7hpyPUO" +
                        "tNQqNAcnW27RrdTCQ0N5YCbhpxUU2zptyg4xemN4kqGH0HlrY7VoLdFoEOfsbXab8GsNrHr+e/bwZWWs/nP3Y+up" +
                        "LsEv1xLsX/dM4sfBWwPr9kbNLPtE8CNDunmIDbGWHJMEBR2EDFQRuvEvNEb2PYpTUJhyqt3bLwPgndaXOiybA5gO" +
                        "CJLxINiZ0FrAq3jFzzXx4sojDnsagBukqBs9QT+yAXVO7u1wkTKQkK9AgMBAAECggEAMBSHN8c/T86JvbCCml4ng" +
                        "DOSeWppKswr3/FZKoQF3MaDw7mh+AcMVjsq1lkVSHgFt/yPNJjoPYpD7tqzQSOEqObbtUILasmLlZ2U95WXzclrT" +
                        "N5wrKR/CykUs4e4hf9l/tHqF7NtISMxgVgA6zAJzm0+g6TZfRtznLXkVD0N2yy+q/a60yyz3p2hyGwKsEA4sWV4K" +
                        "5TdGOAVvpBTapZVGVrFtvFH4PO49T3FERxUPeErGeFkumuqM0Fldi1IcfuwqjaUYcq9uu8q8m3yR4KogFNM9aiTk" +
                        "lrS0n2cyqWo00mJn07jw7A4Fdq3OzcPQiOlj1jtZPMrLYzzfseIlJ1QAQKBgQDsM5aehasKqFbbmRdTZwnVaAMwX" +
                        "m0/wAkMBrATsUNiBQ+Vqwy2nbpQpkoLV+CgvAPRoDMhtRMRHgHI2E4LTDFS0to04+xVIOeeZu2dUgWvtlglKEKiW" +
                        "JbOzEmv+kXnx8ykaZ6n6vWDZuRsklUPqG05VduKmNQyzt6X6vb+iLtP1QKBgQDCwDqsWVr3cwf6eoWKpimLY6zrN" +
                        "d2pUOY3h7hIvgPJGyFL22z2T8LirbJu6uIPyggtKbNZht2ldkB/8wbpW4Vhs94TIlrcWmRd28/ebUxt51mXfxpWn" +
                        "WBQ98PyHkjGHr0W5I710l487FvuqrDRG1vyfeU0WzMfKQxOmVkufMUDSQKBgQCT4EBeAoVw6K9oUFIgLxQi/Tlx4" +
                        "Mw46zs+wjkeOw5aoZx+MVaGRf1NXK5ip6PEPbGOXeNlKP5E6ypm2bUSsaL3nXZrQn7FVRJtQzdkFOPmfBx3Nttw1" +
                        "FGmKpO/jsqRL69ZB58CtcD7gUFlD8AdS28GRFBsVv4SyRzms33rISNT+QKBgDAGOTHBAyacDi6dQgsTuJDv7nc+H" +
                        "EAhe/Pbmv0NJl/UOdy76l2/A37keGjTsRVVq7h/XQPJtySTSYqDwoOPFxAly7CKOz+1ZqSijNr7tuovL6+lHziWr" +
                        "GarAn3VK/r3H4fiqsJBsw1w96aeHuYhygW9N+U2TB/1/lvvgsds6yRJAoGABfcVhnRXyLHt6tBD4A46/cO/yrsOt" +
                        "2uHy47iypF+VZqDurPCiNkxMo8k9BHrdV+Fn9TdnMooOGHhQFpJgT/kxXtr5XBrsYwOL9tfs6LhjkpggPdfW6aH4" +
                        "3HYZ/10bNpVRaKawAlmVwDlmsPtxM76BBs2CVmyaoC0j0qgz3ZRX6o="
                , "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlRqqxQThHQK526TwApSc1C7Z9nsifuAFX2LQfL" +
                        "zATRGkYt8ArPuROONlui5fCaqWbtTF5Up7c9W4j0ILsCtfPVOwXUYtVuNOBGZL4FhUJgSomg4404C6vywtbAkO5q" +
                        "XWjJ6HU0WlSwL/YTQ2sTZ0a/JDNuoC2upX1NA0u7EOzK2KlKjJM++/xXM6itwJ/cJgEab+R9QYB5EbD7Kfp6mN6R" +
                        "jTuHaPpLKcYRJWp/KhxeTN6g6YvZtYMLWUZWaJwUdjazWKa8wCo3vCk+jg66sZeRY9pSQpxBoAPV5PRYgMEAFxR4" +
                        "vGjZ10GXdVuSqv83dWL3UGfdUX19H5VaSk90xl/QIDAQAB"
                , (isVerify, res) -> {

                });

    }
}
