package com.ck.tools.utils;

/**
 * 基础页面信息渲染展示 工具类
 *
 * @author cyk
 * @since 2024-01-01
 */
public class WebPageUtil {

    /**
     * 消息提醒展示标签文本
     *
     * @param msg 提示文本
     * @return HTML文本
     */
    public static String msgViewHtmlTag(String msg) {
        return msgViewHtmlTag("消息提醒", msg);
    }

    /**
     * 消息提醒展示标签文本
     *
     * @param msg 提示文本
     * @return HTML文本
     */
    public static String msgViewHtmlTag(String title, String msg) {
        if (title == null || title.isEmpty())
            throw new IllegalArgumentException("消息提醒标题 title 不能为空");
        if (msg == null || msg.isEmpty()) {
            throw new IllegalArgumentException("消息提醒展示标签文本 msg 不能为空");
        }
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <head>\n" +
                "        <meta charset=\"UTF-8\">\n" +
                "        <title>" + title + "</title>\n" +
                "    </head>\n" +
                "    <body>\n" +
                "        <div style=\"display: grid; place-items: center; height: 100vh;\">\n" +
                "            <span style=\"color: darkgray;font-size: 2em;\">" + msg + "</span>\n" +
                "        </div>\n" +
                "    </body>\n" +
                "</html>";
    }

    /**
     * PDF 在线预览
     *
     * @param title 标签标题
     * @param url   文件地址
     * @return HTML文本
     */
    public static String pdfViewHtmlTag(String title, String url) {

        if (title == null || title.isEmpty())
            title = "PDF 在线预览";

        if (url == null || url.isEmpty()) {
            throw new IllegalArgumentException("PDF文件 url 不能为空");
        }

               /*
                   PDFObject v2.1.1
                   https://github.com/pipwerks/PDFObject
                   Copyright (c) 2008-2018 Philip Hutchison
                   MIT-style license: http://pipwerks.mit-license.org/
                   UMD module pattern from https://github.com/umdjs/umd/blob/master/templates/returnExports.js
                */
        return "<!DOCTYPE html>\n" +
                "<html lang=\"zh-CN\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>" + title + "</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<!-- 定义一个 div 作为 PDF 文件的容器 -->\n" +
                "<div id=\"pdf-container\" style=\"width: 100%; height: 98vh;\"></div>\n" +
                "\n" +
                "<script>\n" +
                "    (function(root,factory){if(typeof define==='function'&&define.amd){define([],factory);}else if(typeof module==='object'&&module.exports){module.exports=factory();}else{root.PDFObject=factory();}}(this,function(){\"use strict\";if(typeof window===\"undefined\"||typeof navigator===\"undefined\"){return false;}\n" +
                "        var pdfobjectversion=\"2.1.1\",ua=window.navigator.userAgent,supportsPDFs,isIE,supportsPdfMimeType=(typeof navigator.mimeTypes['application/pdf']!==\"undefined\"),supportsPdfActiveX,isModernBrowser=(function(){return(typeof window.Promise!==\"undefined\");})(),isFirefox=(function(){return(ua.indexOf(\"irefox\")!==-1);})(),isFirefoxWithPDFJS=(function(){if(!isFirefox){return false;}\n" +
                "            return(parseInt(ua.split(\"rv:\")[1].split(\".\")[0],10)>18);})(),isIOS=(function(){return(/iphone|ipad|ipod/i.test(ua.toLowerCase()));})(),createAXO,buildFragmentString,log,embedError,embed,getTargetElement,generatePDFJSiframe,generateEmbedElement;createAXO=function(type){var ax;try{ax=new ActiveXObject(type);}catch(e){ax=null;}\n" +
                "            return ax;};isIE=function(){return!!(window.ActiveXObject||\"ActiveXObject\"in window);};supportsPdfActiveX=function(){return!!(createAXO(\"AcroPDF.PDF\")||createAXO(\"PDF.PdfCtrl\"));};supportsPDFs=(!isIOS&&(isFirefoxWithPDFJS||supportsPdfMimeType||(isIE()&&supportsPdfActiveX())));buildFragmentString=function(pdfParams){var string=\"\",prop;if(pdfParams){for(prop in pdfParams){if(pdfParams.hasOwnProperty(prop)){string+=encodeURIComponent(prop)+\"=\"+encodeURIComponent(pdfParams[prop])+\"&\";}}\n" +
                "            if(string){string=\"#\"+string;string=string.slice(0,string.length-1);}}\n" +
                "            return string;};log=function(msg){if(typeof console!==\"undefined\"&&console.log){console.log(\"[PDFObject] \"+msg);}};embedError=function(msg){log(msg);return false;};getTargetElement=function(targetSelector){var targetNode=document.body;if(typeof targetSelector===\"string\"){targetNode=document.querySelector(targetSelector);}else if(typeof jQuery!==\"undefined\"&&targetSelector instanceof jQuery&&targetSelector.length){targetNode=targetSelector.get(0);}else if(typeof targetSelector.nodeType!==\"undefined\"&&targetSelector.nodeType===1){targetNode=targetSelector;}\n" +
                "            return targetNode;};generatePDFJSiframe=function(targetNode,url,pdfOpenFragment,PDFJS_URL,id){var fullURL=PDFJS_URL+\"?file=\"+encodeURIComponent(url)+pdfOpenFragment;var scrollfix=(isIOS)?\"-webkit-overflow-scrolling: touch; overflow-y: scroll; \":\"overflow: hidden; \";var iframe=\"<div style='\"+scrollfix+\"position: absolute; top: 0; right: 0; bottom: 0; left: 0;'><iframe  \"+id+\" src='\"+fullURL+\"' style='border: none; width: 100%; height: 100%;' frameborder='0'></iframe></div>\";targetNode.className+=\" pdfobject-container\";targetNode.style.position=\"relative\";targetNode.style.overflow=\"auto\";targetNode.innerHTML=iframe;return targetNode.getElementsByTagName(\"iframe\")[0];};generateEmbedElement=function(targetNode,targetSelector,url,pdfOpenFragment,width,height,id){var style=\"\";if(targetSelector&&targetSelector!==document.body){style=\"width: \"+width+\"; height: \"+height+\";\";}else{style=\"position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%;\";}\n" +
                "            targetNode.className+=\" pdfobject-container\";targetNode.innerHTML=\"<embed \"+id+\" class='pdfobject' src='\"+url+pdfOpenFragment+\"' type='application/pdf' style='overflow: auto; \"+style+\"'/>\";return targetNode.getElementsByTagName(\"embed\")[0];};embed=function(url,targetSelector,options){if(typeof url!==\"string\"){return embedError(\"URL is not valid\");}\n" +
                "            targetSelector=(typeof targetSelector!==\"undefined\")?targetSelector:false;options=(typeof options!==\"undefined\")?options:{};var id=(options.id&&typeof options.id===\"string\")?\"id='\"+options.id+\"'\":\"\",page=(options.page)?options.page:false,pdfOpenParams=(options.pdfOpenParams)?options.pdfOpenParams:{},fallbackLink=(typeof options.fallbackLink!==\"undefined\")?options.fallbackLink:true,width=(options.width)?options.width:\"100%\",height=(options.height)?options.height:\"100%\",assumptionMode=(typeof options.assumptionMode===\"boolean\")?options.assumptionMode:true,forcePDFJS=(typeof options.forcePDFJS===\"boolean\")?options.forcePDFJS:false,PDFJS_URL=(options.PDFJS_URL)?options.PDFJS_URL:false,targetNode=getTargetElement(targetSelector),fallbackHTML=\"\",pdfOpenFragment=\"\",fallbackHTML_default=\"<p>This browser does not support inline PDFs. Please download the PDF to view it: <a href='[url]'>Download PDF</a></p>\";if(!targetNode){return embedError(\"Target element cannot be determined\");}\n" +
                "            if(page){pdfOpenParams.page=page;}\n" +
                "            pdfOpenFragment=buildFragmentString(pdfOpenParams);if(forcePDFJS&&PDFJS_URL){return generatePDFJSiframe(targetNode,url,pdfOpenFragment,PDFJS_URL,id);}else if(supportsPDFs||(assumptionMode&&isModernBrowser&&!isIOS)){return generateEmbedElement(targetNode,targetSelector,url,pdfOpenFragment,width,height,id);}else if(PDFJS_URL){return generatePDFJSiframe(targetNode,url,pdfOpenFragment,PDFJS_URL,id);}else{if(fallbackLink){fallbackHTML=(typeof fallbackLink===\"string\")?fallbackLink:fallbackHTML_default;targetNode.innerHTML=fallbackHTML.replace(/\\[url\\]/g,url);}\n" +
                "                return embedError(\"This browser does not support embedded PDFs\");}};return{embed:function(a,b,c){return embed(a,b,c);},pdfobjectversion:(function(){return pdfobjectversion;})(),supportsPDFs:(function(){return supportsPDFs;})()};}));\n" +
                "</script>\n" +
                "<script>\n" +
                "    // 当文档加载完成后执行\n" +
                "    window.onload = function() {\n" +
                "        // 使用 PDFObject 嵌入 PDF 文件\n" +
                "        PDFObject.embed(\"" + url + "\", \"#pdf-container\", {\n" +
                "            // 可选参数\n" +
                "            width: \"100%\",\n" +
                "            height: \"100%\",\n" +
                "            // 更多配置选项...\n" +
                "        });\n" +
                "    };\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>\n";
    }


}
