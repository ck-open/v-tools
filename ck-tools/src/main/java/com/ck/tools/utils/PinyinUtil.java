package com.ck.tools.utils;

import com.ck.tools.check_bean.RegExUtil;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.logging.Logger;

/**
 * 文字转拼音
 * 依赖于开源项目 com.belerweb pinyin4j 2.5.0
 *
 * @author cyk
 * @since 2020-01-01
 */
public class PinyinUtil {
    private static final Logger log = Logger.getLogger(PinyinUtil.class.getName());

    public static void main(String[] args) {
        System.out.println(toPinyin("我不信"));
        System.out.println(toPinyin("我不信", "_", false));

    }


    /**
     * 将文字转为汉语拼音 驼峰
     *
     * @param chineseLanguage 要转成拼音的中文
     * @return 拼音
     */
    public static String toPinyin(String chineseLanguage) {
        return toPinyin(chineseLanguage, "_", true, HanyuPinyinCaseType.LOWERCASE, HanyuPinyinToneType.WITHOUT_TONE, HanyuPinyinVCharType.WITH_V);
    }

    /**
     * 将文字转为汉语拼音
     *
     * @param chineseLanguage 要转成拼音的中文
     * @param interval        拼音间隔符
     * @param isHump          是否驼峰
     * @return 拼音
     */
    public static String toPinyin(String chineseLanguage, String interval, boolean isHump) {
        return toPinyin(chineseLanguage, interval, isHump, HanyuPinyinCaseType.LOWERCASE, HanyuPinyinToneType.WITHOUT_TONE, HanyuPinyinVCharType.WITH_V);
    }

    /**
     * 将文字转为汉语拼音
     *
     * @param chineseLanguage 要转成拼音的中文
     * @param interval        拼音间隔符
     * @param isHump          是否驼峰
     * @param caseType        输出拼音全部大|小写
     * @param toneType        带|不声调。用声调表示，如：liú 设置特殊拼音ü的显示格式
     * @param charType        HanyuPinyinVCharType.WITH_U_AND_COLON 以U和一个冒号表示该拼音，例如：lu: HanyuPinyinVCharType.WITH_V
     *                        以V表示该字符，例如：lv HanyuPinyinVCharType.WITH_U_UNICODE 以ü表示
     * @return 拼音
     */
    public static String toPinyin(String chineseLanguage, String interval, boolean isHump, HanyuPinyinCaseType caseType, HanyuPinyinToneType toneType, HanyuPinyinVCharType charType) {
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(caseType);
        defaultFormat.setToneType(toneType);
        defaultFormat.setVCharType(charType);
        return toPinyin(chineseLanguage, interval, isHump, defaultFormat);
    }

    /**
     * 将文字转为汉语拼音
     *
     * @param chineseLanguage 要转成拼音的中文
     * @param interval        拼音间隔符
     * @param isHump          是否驼峰
     * @param defaultFormat   转换格式
     * @return 拼音
     */
    public static String toPinyin(String chineseLanguage, String interval, boolean isHump, HanyuPinyinOutputFormat defaultFormat) {
        if (chineseLanguage == null) {
            return null;
        }

        char[] cl_chars = chineseLanguage.trim().toCharArray();

        StringBuilder pinyin = new StringBuilder();

        try {
            for (int i = 0; i < cl_chars.length; i++) {
                String pin;
                if (String.valueOf(cl_chars[i]).matches("[\u4e00-\u9fa5]+")) {// 如果字符是中文,则将中文转为汉语拼音
                    String[] p;
                    if (defaultFormat == null) {
                        p = PinyinHelper.toHanyuPinyinStringArray(cl_chars[i]);
                    } else {
                        p = PinyinHelper.toHanyuPinyinStringArray(cl_chars[i], defaultFormat);
                    }
                    pin = p[0];
                } else {// 如果字符不是中文,则不转换
                    pin = String.valueOf(cl_chars[i]);
                }

                if (i != 0) {
                    if (interval != null) {
                        pinyin.append(interval);
                    }
                }
                pinyin.append(pin);
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            log.info(String.format("字符[%s]不能转成汉语拼音", chineseLanguage));
        }

        if (isHump) {
            return RegExUtil.toHump(pinyin.toString());
        }
        return pinyin.toString();
    }
}
