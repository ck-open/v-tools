package com.ck.tools.qw;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ck.tools.exception.BusinessException;
import com.ck.tools.utils.WebUtil;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

import static com.alibaba.fastjson.JSON.parseObject;

public class WXAppUtil {
    private static final Logger log = Logger.getLogger(WXAppUtil.class.getName());
    /**
     * 企微Api 域名地址
     */
    public static final String qwHost = "https://qyapi.weixin.qq.com";

    /**
     * Api 根据 应用corpId 和 应用corpSecret 获取企业微信Token
     */
    public static final String getToken = qwHost + "/cgi-bin/gettoken";

    /**
     * Api 根据 应用token 和 用户授权码Code 查询用户信息
     */
    public static final String getUserInfo = qwHost + "/cgi-bin/auth/getuserinfo";

    /**
     * Api 根据 应用token 和 用户userTicket 查询用户详细信息
     */
    public static final String getUserDetail = qwHost + "/cgi-bin/auth/getuserdetail";

    /**
     * Api 根据 应用token 发送应用消息
     */
    public static final String sendMsg = qwHost + "/cgi-bin/message/send";


    /**
     * 获取应用授权地址
     *
     * @param config  应用消息配置
     * @param request 请求信息，用于获取重定向需要携带的参数信息
     * @return 应用授权地址
     */
    public static String qwAppAuthUrl(SendQWAppMsgConfig config, HttpServletRequest request) {
        if (Objects.isNull(config) || Objects.isNull(config.getCorpId()) || Objects.isNull(config.getAgentId()) || config.getCorpId().isEmpty()) {
            throw new BusinessException("未配置 CorpId 或 AgentId");
        }
        if (Objects.isNull(config.getAuthorizeUrl()) || Objects.isNull(config.getSecret()) || config.getAuthorizeUrl().isEmpty() || config.getSecret().isEmpty()) {
            throw new BusinessException("未配置 AuthorizeUrl 或 RedirectUri");
        }
        if (Objects.isNull(config.getResponseType()) || Objects.isNull(config.getScope()) || config.getResponseType().isEmpty() || config.getScope().isEmpty()) {
            throw new BusinessException("未配置 ResponseType 或 Scope");
        }

        String redirect_uri = WebUtil.urlWithForm(config.getRedirectUri(), WebUtil.buildParams(request.getParameterMap(), StandardCharsets.UTF_8, null), StandardCharsets.UTF_8, true);

        return String.format("%s?appid=%s&redirect_uri=%s&response_type=%s&scope=%s&state=%s&agentid=%s#wechat_redirect"
                , config.getAuthorizeUrl(), config.getCorpId()
                , WebUtil.urlEncode(redirect_uri, StandardCharsets.UTF_8)
                , config.getResponseType(), config.getScope(), config.getState(), config.getAgentId());
    }

    /**
     * 获取应用token
     *
     * @return accessToken
     */
    public static String getAppToken(SendQWAppMsgConfig config) {
        log.info(String.format("获取企业微信Token weChatCorpId：%s  secret：%s", config.getCorpId(), config.getSecret()));

        String url = WebUtil.urlWithForm(qwHost + getToken, WebUtil.buildParams(null, StandardCharsets.UTF_8, param -> {
            param.put("corpid", config.getCorpId());
            param.put("corpsecret", config.getSecret());
        }), StandardCharsets.UTF_8, true);
        log.info(String.format("获取企业微信Token URL：%s", url));

        String result = WebUtil.sendConnect(WebUtil.getHttpURLConnection(url, "GET", 10000, 10000), null);

        log.info(String.format("获取企业微信Token 响应结果：%s", result));

        QWAppVo.Token token = JSON.parseObject(result, QWAppVo.Token.class);

        if (Objects.nonNull(token) && token.isOk()) {
            log.info(String.format("获取企业微信 weChatCorpId：%s  secret：%s  成功", config.getCorpId(), config.getSecret()));
            return token.getAccessToken();
        }
        log.info(String.format("获取企业微信 weChatCorpId：%s  secret：%s  失败", config.getCorpId(), config.getSecret()));
        return null;
    }

    /**
     * 通过用户授权码获取用户信息
     *
     * @param accessToken 企微应用授权Token
     * @param code        用户授权码
     * @return 获取到的用户信息
     */
    public static QWAppVo.UserDetail getUserDetail(String accessToken, String code) {
        log.info(String.format("通过用户授权码获取企业微信用户信息 accessToken：%s  code：%s", accessToken, code));

        if (Objects.isNull(accessToken) || Objects.isNull(code) || accessToken.isEmpty() || code.isEmpty()) {
            throw new BusinessException("2001", "获取企业微信用户信息失败，accessToken：{}  code：{}", (Object) accessToken, code);
        }

        String url = WebUtil.urlWithForm(qwHost + getUserInfo, WebUtil.buildParams(null, StandardCharsets.UTF_8, param -> {
            param.put("access_token", accessToken);
            param.put("code", code);
        }), StandardCharsets.UTF_8, true);
        log.info(String.format("获取企业微信用户信息 URL：%s", url));

        QWAppVo.UserInfo userInfo = WebUtil.sendConnect(WebUtil.getHttpURLConnection(url, "GET", 10000, 10000), null, res->JSONObject.parseObject(res,QWAppVo.UserInfo.class));
        log.info(String.format("获取企业微信用户信息 响应结果：%s", JSONObject.toJSONString(userInfo)));


        if (Objects.isNull(userInfo) || !userInfo.isOk()) {
            throw new BusinessException("2001", "获取企业微信用户信息失败，accessToken：{}  code：{}", (Object) accessToken, code);
        }

        url = WebUtil.urlWithForm(qwHost + getUserDetail, WebUtil.buildParams(null, StandardCharsets.UTF_8, param -> {
            param.put("access_token", accessToken);
        }), StandardCharsets.UTF_8, true);
        log.info(String.format("获取企业微信用户详细信息 URL：%s", url));

        HashMap<String, String> map = new HashMap<>();
        map.put("user_ticket", userInfo.getUserTicket());
        QWAppVo.UserDetail userDetail = WebUtil.sendConnect(WebUtil.getHttpURLConnection(url, "POST", 10000, 10000), JSONObject.toJSONString(map), res->JSONObject.parseObject(res,QWAppVo.UserDetail.class));
        log.info(String.format("获取企业微信用户详细信息 响应结果：%s", JSONObject.toJSONString(userDetail)));

        if (Objects.isNull(userDetail) || !userDetail.isOk()) {
            throw new BusinessException("2001", "获取企业微信用户详细信息失败，accessToken：{}  code：{}", (Object) accessToken, code);
        }

        return userDetail;
    }


    /**
     * 发送应用消息
     */
    public static boolean sendMsg(SendQWAppMsgConfig config, BiConsumer<Map<String, Object>, Map<String, Object>> consumer) {
        log.info("推送企业微信应用消息：" + JSONObject.toJSONString(config));
        String accessToken = getAppToken(config);
        if (Objects.nonNull(accessToken) && accessToken.isEmpty()) {

            if ((Objects.isNull(config.getToUser()) || config.getToUser().trim().isEmpty()) && (Objects.isNull(config.getToParty())) || config.getToParty().trim().isEmpty()) {
                throw new BusinessException("2001", "推送企业微信应用消息失败，未提供推送消息接收人");
            }

            if (Objects.isNull(config.getTitle()) || config.getTitle().trim().isEmpty()) {
                throw new BusinessException("2001", "推送企业微信应用消息失败，未提供推送消息标题");
            }

            if (Objects.isNull(config.getDescription()) || config.getDescription().trim().isEmpty()) {
                throw new BusinessException("2001", "推送企业微信应用消息失败，未提供推送消息内容");
            }

            Map<String, Object> params = new LinkedHashMap<>();
            Map<String, Object> textCard = new LinkedHashMap<>();

            params.put("touser", config.getToUser()); /* 成员ID列表（消息接收者，最多支持1000个） */
            params.put("toparty", config.getToParty()); /* 部门ID列表，最多支持100个 */
            params.put("totag", ""); /* 本企业的标签ID列表，最多支持100个 */
            params.put("msgtype", "textcard"); /* 消息类型，此时固定为：textcard */
            params.put("agentid", config.getAgentId()); /* 企业应用的id，整型 */
            params.put("textcard", textCard); /* 消息信息 */

            textCard.put("title", config.getTitle()); /* 标题，不超过128个字节，超过会自动截断 */
            textCard.put("description", config.getDescription()); /* 描述，不超过512个字节，超过会自动截断 */
            textCard.put("url", config.getUrl());  /* 点击后跳转的链接 */
            textCard.put("btntxt", config.getBtnTxt()); /* 按钮文字。 默认为“详情”， 不超过4个文字，超过自动截断 */

            /* 执行自定义参数调整 */
            if (Objects.nonNull(consumer)) {
                consumer.accept(params, textCard);
            }

            log.info("推送企业微信应用消息内容：" + JSONObject.toJSONString(params));

            String url = WebUtil.urlWithForm(qwHost + sendMsg, WebUtil.buildParams(null, StandardCharsets.UTF_8, param -> param.put("access_token", accessToken)), StandardCharsets.UTF_8, true);
            log.info(String.format("推送企业微信应用消息 URL：%s", url));

            QWAppVo.MessageSend messageSend = WebUtil.sendConnect(WebUtil.getHttpURLConnection(url, "POST", 10000, 10000), JSONObject.toJSONString(params), res->JSONObject.parseObject(res,QWAppVo.MessageSend.class));
            log.info(String.format("推送企业微信应用消息 响应结果：%s", JSONObject.toJSONString(messageSend)));

            if (Objects.nonNull(messageSend) && messageSend.isOk()) {
                log.info("推送企业微信应用消息成功");
                return true;
            }
        }
        log.info("推送企业微信应用消息失败");
        return false;
    }
}
