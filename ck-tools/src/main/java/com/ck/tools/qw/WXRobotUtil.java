package com.ck.tools.qw;

import com.ck.tools.exception.ExceptionUtil;
import com.ck.tools.utils.WebNetUtil;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * 腾讯企业微信群消息机器人
 * 官方API文档：<a href="https://developer.work.weixin.qq.com/document/path/91770">...</a>
 *
 * @author cyk
 * @since 2022-01-11
 */
public class WXRobotUtil {
    private static final Logger log = Logger.getLogger(WXRobotUtil.class.getName());
    private static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(10);
    private static final String robot_url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send";
    private static final String robot_url_file = "https://qyapi.weixin.qq.com/cgi-bin/webhook/upload_media";

    /**
     * 消息链路追踪ID提供
     */
    public static Supplier<String> traceIdSupplier = null;

    /**
     * 发送消息 - 异步
     *
     * @param key                 群机器人创建时的key
     * @param content             消息文本内容
     * @param mentionedMobileList 需要 @ 那些手机号的人
     */
    public static void sendTextAsync(String key, String content, String... mentionedMobileList) {
        THREAD_POOL.execute(() -> sendText(key, content, mentionedMobileList));
    }


    /**
     * 发送文件 - 异步
     *
     * @param key      群机器人创建时的key
     * @param fileName 文件名称 test.txt
     * @param file     文件
     */
    public static void sendFileAsync(String key, String fileName, File file) {
        THREAD_POOL.execute(() -> sendFile(key, fileName, file));
    }

    /**
     * 发送文件 - 异步
     *
     * @param key      群机器人创建时的key
     * @param fileName 文件名称 test.txt
     * @param file     文件二进制内容
     */
    public static void sendFileAsync(String key, String fileName, byte[] file) {
        THREAD_POOL.execute(() -> sendFile(key, fileName, file));
    }

    /**
     * 发送图片 - 异步
     *
     * @param key       群机器人创建时的key
     * @param imageFile 文件
     */
    public static void sendImageAsync(String key, File imageFile) {
        THREAD_POOL.execute(() -> sendImage(key, imageFile));
    }

    /**
     * 发送图片 - 异步
     *
     * @param key       群机器人创建时的key
     * @param imageFile 文件二进制内容
     */
    public static void sendImageAsync(String key, byte[] imageFile) {
        THREAD_POOL.execute(() -> sendImage(key, imageFile));
    }

    /**
     * 发送消息
     *
     * @param key                 群机器人创建时的key
     * @param content             消息文本内容
     * @param mentionedMobileList 需要 @ 那些手机号的人
     * @return 返回消息中的uuid
     */
    public static String sendText(String key, String content, String... mentionedMobileList) {
        return sendMessage(key, "text", content, null, null, mentionedMobileList);
    }


    /**
     * 发送文件
     *
     * @param key      群机器人创建时的key
     * @param fileName 文件名称 test.txt
     * @param file     文件
     * @return 腾讯响应结果
     */
    public static String sendFile(String key, String fileName, File file) {
        byte[] data = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            WebNetUtil.io(Files.newInputStream(file.toPath()), byteArrayOutputStream);
            data = byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            log.warning("发送WX文件解析异常：" + e.getMessage());
            return "文件解析异常";
        }
        return sendFile(key, fileName, data);
    }

    /**
     * 发送文件
     *
     * @param key      群机器人创建时的key
     * @param fileName 文件名称 test.txt
     * @param file     文件二进制内容
     * @return 腾讯响应结果
     */
    public static String sendFile(String key, String fileName, byte[] file) {
        return sendMessage(key, "file", uploadingFile(key, fileName, file), null, null);
    }

    /**
     * 发送图片
     *
     * @param key       群机器人创建时的key
     * @param imageFile 文件
     * @return 腾讯响应结果
     */
    public static String sendImage(String key, File imageFile) {
        // 图片内容（base64编码前）的md5值
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            WebNetUtil.io(Files.newInputStream(imageFile.toPath()), byteArrayOutputStream);
        } catch (IOException e) {
            log.warning("发送WX图片 MD5 和 Base64 转换异常：" + e.getMessage());
        }
        return sendImage(key, byteArrayOutputStream.toByteArray());
    }

    /**
     * 发送图片
     *
     * @param key       群机器人创建时的key
     * @param imageFile 文件二进制内容
     * @return 腾讯响应结果
     */
    public static String sendImage(String key, byte[] imageFile) {
        String md5 = null;
        String body = null;
        try {
            body = Base64.getEncoder().encodeToString(imageFile);

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(imageFile);
            md5 = new BigInteger(1, md.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            log.warning("发送WX图片 MD5 和 Base64 转换异常：" + e.getMessage());
        }

        String finalBody = body;
        String finalMd = md5;
        return sendMessage(key, "image", finalBody, finalMd, null);
    }

    /**
     * 发送消息
     *
     * @param key                 群机器人创建时的key
     * @param msgType             消息类型：markdown、image、file、text（默认）
     * @param content             消息文本内容  (发送文件时上传后返回的 media_id)
     * @param md5                 发图片时 图片内容（base64编码前）的md5值 （content 图片文件 base64 加密后字符串）
     * @param mentionedList       需要 @ 那些账号
     * @param mentionedMobileList 需要 @ 那些手机号的人
     * @return 腾讯响应结果
     */
    public static String sendMessage(String key, String msgType, String content, String md5, List<String> mentionedList, String... mentionedMobileList) {


        String body = "";
        if ("markdown".equals(msgType)) {
            /* markdown类型 消息    报文如下
                    msgtype	是	消息类型，此时固定为image
                    base64	是	图片内容的base64编码
                    md5	是	图片内容（base64编码前）的md5值
                    注：图片（base64编码前）最大不能超过2M，支持JPG,PNG格式
                    {
                        "msgtype": "markdown",
                        "markdown": {
                            "content": "实时新增用户反馈<font color=\"warning\">132例</font>，请相关同事注意。\n
                             >类型:<font color=\"comment\">用户反馈</font>
                             >普通用户反馈:<font color=\"comment\">117例</font>
                             >VIP用户反馈:<font color=\"comment\">15例</font>"
                        }
                    }
             */
            body = String.format("{\"msgtype\":\"markdown\",\"markdown\":{\"content\":\"%s\"}}", content);
        } else if ("image".equals(msgType)) {
            /* image类型 消息   报文如下
                msgtype	是	消息类型，此时固定为image
                base64	是	图片内容的base64编码
                md5	是	图片内容（base64编码前）的md5值
                注：图片（base64编码前）最大不能超过2M，支持JPG,PNG格式
                    {
                        "msgtype": "image",
                        "image": {
                            "base64": "DATA",
                            "md5": "MD5"
                        }
                    }
             */
            body = String.format("{\"msgtype\":\"image\",\"image\":{\"base64\":\"%s\",\"md5\":\"%s\"}}", content, md5);
        } else if ("file".equals(msgType)) {
            /* file类型 消息  报文如下
                    {
                        "msgtype": "file",
                        "file": {
                            "media_id": "3a8asd892asd8asd"
                        }
                    }
             */
            String template = "{\"msgtype\":\"file\",\"file\":{\"media_id\":\"%s\"}}";
            body = String.format(template, content);
        } else {
            /* 文本消息   报文如下
                    {
                        "msgtype": "text",
                        "text": {
                            "content": "广州今日天气：29度，大部分多云，降雨概率：60%",
                            "mentioned_list":["wangqing","@all"],
                            "mentioned_mobile_list":["13800001111","@all"]
                        }
                    }
             */
            String template = "{\"msgtype\":\"text\",\"text\":{\"content\":\"%s\",\"mentioned_list\":[%s],\"mentioned_mobile_list\":[%s]}}\n";
            String mentionedStr = "";
            if (mentionedList != null) {
                StringBuilder msb = new StringBuilder();
                for (String m : mentionedList) {
                    msb.append(",\"").append(m).append("\"");
                }
                if (msb.toString().startsWith(",")) {
                    mentionedStr = msb.substring(1);
                }
            }

            String mentionedMobileStr = "";
            if (mentionedMobileList != null) {
                StringBuilder msb = new StringBuilder();
                for (String m : mentionedMobileList) {
                    msb.append(",\"").append(m).append("\"");
                }
                if (msb.toString().startsWith(",")) {
                    mentionedMobileStr = msb.substring(1);
                }
            }
            body = String.format(template, content, mentionedStr, mentionedMobileStr);
        }

        String url = WebNetUtil.urlWithForm(robot_url
                , WebNetUtil.buildParams(StandardCharsets.UTF_8, "key", key), StandardCharsets.UTF_8);

        HttpURLConnection conn = WebNetUtil.getHttpURLConnection(url, WebNetUtil.POST, 60000, 60000);

        try {
            log.info("发送WX消息 " + url + " Body：" + body);

            String result = WebNetUtil.sendConnect(conn, body);

            if (200 == conn.getResponseCode() && Objects.nonNull(result) && result.contains("\"errcode\":0")) {
                return "200";
            } else {
                log.warning("发送WX消息失败：" + result);
                return result;
            }

        } catch (Exception e) {
            log.warning("发送WX消息异常：" + e);
        } finally {
            WebNetUtil.closeHttpURLConnection(conn);
        }
        return null;
    }


    /**
     * 上传文件
     *
     * @param key      群机器人创建时的key
     * @param fileName 文件名称 test.txt
     * @param file     文件二进制内容
     * @return media_id
     */
    public static String uploadingFile(String key, String fileName, byte[] file) {
        /*
            Content-Disposition: form-data; name="media";filename="wework.txt"; filelength=6
            Content-Type: application/octet-stream

            https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=8e281ea3-37fc-46f8-b351-8e4e391c3487
         */


        if (file != null) {

            // form-data 请求报文体前缀
            byte[] formDataPrefix = String.format("---------------------------acebdf13572468\r\n" +
                    "Content-Disposition: form-data; name=\"media\";filename=\"%s\"; filelength=%s\r\n" +
                    "Content-Type: application/octet-stream\r\n\r\n", fileName, file.length).getBytes(StandardCharsets.UTF_8);

            // form-data 请求报文体后缀（结束标记）
            byte[] formDataSuffix = "\r\n---------------------------acebdf13572468--\r\n".getBytes(StandardCharsets.UTF_8);

            String url = WebNetUtil.urlWithForm(robot_url_file,
                    WebNetUtil.buildParams(StandardCharsets.UTF_8, "type", "file", "key", key), StandardCharsets.UTF_8, false);

            HttpURLConnection conn = WebNetUtil.getHttpURLConnection(url, WebNetUtil.POST, 60000 * 30, 60000 * 30);

            conn.addRequestProperty("Content-Type", " multipart/form-data; boundary=-------------------------acebdf13572468");
            conn.addRequestProperty("Content-Length", String.valueOf(file.length + formDataPrefix.length + formDataSuffix.length));

            try {
                log.info("发送WX文件 " + url + " fileName：" + fileName + " 大小：" + (new BigDecimal(file.length / 1024d / 1024d).setScale(4, RoundingMode.HALF_UP)) + "MB");
                conn.connect();

                try (OutputStream out = conn.getOutputStream()) {
                    WebNetUtil.io(new ByteArrayInputStream(formDataPrefix), out, 2);
                    WebNetUtil.io(new ByteArrayInputStream(file), out, 2);
                    WebNetUtil.io(new ByteArrayInputStream(formDataSuffix), out, 2);
                }

                if (200 == conn.getResponseCode()) {
                    /* 正确响应报文：
                            {
                               "errcode": 0,
                               "errmsg": "ok",
                               "type": "file",
                               "media_id": "1G6nrLmr5EC3MMb_-zK1dDdzmd0p7cNliYu9V5w7o8K0",
                               "created_at": "1380000000"
                            }
                     */
                    String result = WebNetUtil.getHttpURLConnResource(conn);
                    if (200 == conn.getResponseCode() && Objects.nonNull(result) && result.contains("\"errcode\":0")) {

                        result = result.substring(result.indexOf("\"media_id\":\""));
                        result = result.substring(0, result.indexOf("\","));

                        return result.replace("\"media_id\":\"", "");
                    } else {
                        log.warning("发送WX消息失败：" + result);
                        return result;
                    }
                }
            } catch (IOException e) {
                log.warning("发送WX文件上传异常：" + e.getMessage());
            } finally {
                WebNetUtil.closeHttpURLConnection(conn);
            }
        }

        return null;
    }

    /**
     * 异常数据保存到文件 并 发送企业微信通知
     *
     * @param sendKey       密钥
     * @param sendContent   企业微信消息文本内容
     * @param e             异常对象
     * @param packagePrefix 包前缀，异常堆栈信息中符合前缀的信息将打印到消息面板中，null则不打印
     */
    public static void sendError(String sendKey, String sendContent, Exception e, String debugData, String... packagePrefix) {
        sendErrorAndExport(sendKey, sendContent, e, debugData, null, packagePrefix);
    }

    /**
     * 异常数据保存到文件 并 发送企业微信通知
     *
     * @param sendKey       密钥
     * @param sendContent   企业微信消息文本内容
     * @param e             异常对象
     * @param debugData     发生异常的数据
     * @param exportPath    异常文件储存地址  null则不储存
     * @param packagePrefix 包前缀，异常堆栈信息中符合前缀的信息将打印到消息面板中，null则不打印
     */
    public static void sendErrorAndExport(String sendKey, String sendContent, Exception e, String debugData, String exportPath, String... packagePrefix) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");

        // 异常日志发送企业微信
//        sendContent = sendContent + "\r" + " Time：" + simpleDateFormat.format(new Date()) + "\r异常文件：" + errorExportPath + "\r堆栈信息：\r" + ExceptionUtil.getExceptionStackTraceInfo(e,3,"cn.com.hyundai") + "\r";
        sendContent = "Msg：" + sendContent + "\r" + "Time：" + simpleDateFormat.format(new Date()) + "\rUUID：" + uuid + "\r";

        // 链路追踪ID
        if (traceIdSupplier != null) {
            sendContent += "TraceId：" + traceIdSupplier.get() + "\r";
        }

        if (e != null) {
            if (exportPath != null && !exportPath.trim().isEmpty()) {
                // 异常日志保存到本地
                exportPath = ExceptionUtil.exceptionStackTraceExportToFile(e, debugData, uuid, exportPath);
                sendContent += "异常文件：" + exportPath + "\r";
            }
            if (packagePrefix != null) {
                sendContent += "StackTraceInfo：\r" + ExceptionUtil.getExceptionStackTraceInfo(e, 5, packagePrefix) + "\r";
            }
            debugData += "\r\n\r\n" + ExceptionUtil.getExceptionStackTraceInfo(e);
        }
        sendText(sendKey, sendContent, uuid);
        if (debugData != null && !debugData.trim().isEmpty())
            sendFile(sendKey, uuid + ".txt", debugData.getBytes(StandardCharsets.UTF_8));
    }


    public static void main(String[] args) {
        sendErrorTest();
//        sendRobotTest();
    }

    private static void sendRobotTest() {
        String robot_key = "06157a4b-adc9-497c-a899-661361cb4617";
//        sendFile(robot_key, "123.txt", "测试文档内容fghfghxdzxfghkfhiyuhuo;jijhiugyufgxzawdsARZefgdtfgjhgigytufhgfchjgjhgchfcgnhgfxf".getBytes(StandardCharsets.UTF_8));

//        sendImage(robot_key, new File("D:\\图片\\0017031094904213_b.jpg"));
//        sendImage(robot_key, new File("D:\\WBS2021-12-01.png"));

        sendText(robot_key, "测试信息", "545454");
    }

    private static void sendErrorTest() {
        String sendKey = "06157a4b-adc9-497c-a899-661361cb4617";
        String errorExportPath = "/home/tomcat/DataLogs/";

        try {
            String str = null;
            System.out.println(str.toString());
        } catch (Exception e) {
            System.out.println(ExceptionUtil.getExceptionStackTraceInfo(e));
            sendErrorAndExport(sendKey, "本地测试微信异常日志(" + WebNetUtil.getHostAddress() + ")\r", e, "异常附带文件信息", errorExportPath);
        }
    }
}
