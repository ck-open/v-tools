package com.ck.tools.qw;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

/**
 * 发送企微应用消息配置信息
 */
@Data
@Accessors(chain = true)
public class SendQWAppMsgConfig {

    /**
     * 应用标记
     * 使用prpDCode 表配置参数时 对应 codeType 字段，例如：QWApp_AuthDiDi
     */
    private String appFlag;

    /**
     * 应用商户ID 获取Token使用
     */
    private String corpId;
    /**
     * 应用商户密钥 获取Token使用
     */
    private String secret;

    /**
     * 用户授权地址
     */
    private String authorizeUrl;

    /**
     * 用户授权后重定向地址
     */
    private String redirectUri;

    /**
     * 响应类型 如：code
     */
    private String responseType;

    /**
     * 授权范围 如：snsapi_privateinfo
     */
    private String scope;

    /**
     * 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     */
    private String state;

    /**
     * 成员ID列表（消息接收者，最多支持1000个）
     * 多个用 | 分割
     */
    private String toUser;
    /**
     * 用户ID关联，与toUser一一对应
     */
    private Map<String, String> toUserCode;
    /**
     * 部门ID列表，最多支持100个
     * 多个用 | 分割
     */
    private String toParty;
    /**
     * 本企业的标签ID列表，最多支持100个
     * 多个用 | 分割
     */
    private String toTag;
    /**
     * 企业应用的id，整型
     */
    private Integer agentId;

    /**
     * 标题，不超过128个字节，超过会自动截断
     */
    private String title;
    /**
     * 描述，不超过512个字节，超过会自动截断
     */
    private String description;
    /**
     * 点击后跳转的链接
     */
    private String url;
    /**
     * 按钮文字。 默认为“详情”， 不超过4个文字，超过自动截断
     */
    private String btnTxt;

}
