package com.ck.tools.qw;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Objects;


public class QWAppVo {

    @Data
    @Accessors
    public static class Res {
        //        @JsonProperty("errcode")
        private String errCode;

        //        @JsonProperty("errmsg")
        private String errMsg;

        public Boolean isOk() {
            return Objects.equals("0", this.errCode);
        }
    }

    /**
     * 获取Token 响应结果
     */
    @EqualsAndHashCode(callSuper = true)
    @Data
    @Accessors
    public static class Token extends Res {
        //        @JsonProperty("access_token")
        private String accessToken;

        //        @JsonProperty("expires_in")
        private String expiresIn;
    }

    /**
     * 用户信息响应结果
     */
    @EqualsAndHashCode(callSuper = true)
    @Data
    @Accessors
    public static class UserInfo extends Res {
        //        @JsonProperty("userid")
        private String userId;

        //        @JsonProperty("user_ticket")
        private String userTicket;
    }

    /**
     * 用户详情信息 响应结果
     */
    @EqualsAndHashCode(callSuper = true)
    @Data
    @Accessors
    public static class UserDetail extends Res {
        /**
         * 用户ID
         */
//        @JsonProperty("userid")
        private String userId;
        /**
         * 性别
         */
        private String gender;
        /**
         * 头像链接
         */
        private String avatar;
        /**
         * 二维码链接
         */
//        @JsonProperty("qr_code")
        private String qrCode;
        /**
         * 手机号码
         */
        private String mobile;
        /**
         * 邮箱地址
         */
        private String email;
        /**
         * 企业邮箱地址
         */
//        @JsonProperty("biz_mail")
        private String bizMail;
        /**
         * 地址
         */
        private String address;
    }

    /**
     * 发送消息响应结果
     */
    @EqualsAndHashCode(callSuper = true)
    @Data
    @Accessors
    public static class MessageSend extends Res {
        //        @JsonProperty("invalidparty")
        private String invalidparty;
        //        @JsonProperty("msgid")
        private String msgid;
    }
}
