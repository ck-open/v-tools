package com.ck.tools.tree;

import com.ck.tools.check_bean.CheckItem;
import com.ck.tools.check_bean.annotation.CheckValue;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 接口json报文配置结构树
 * </p>
 *
 * @author cyk
 * @since 2023-06-14
 */
@Data
@Accessors(chain = true)
public abstract class ApiJsonConfVo {
    /**
     * 是 Y
     */
    public static final String YES = "Y";
    /**
     * 是 N
     */
    public static final String NO = "N";

    /**
     * 参数类型 - 对象，Map 结构
     */
    public static final String TYPE_MAP = "Map";
    /**
     * 参数类型 - 列表
     */
    public static final String TYPE_LIST = "List";
    /**
     * 参数类型 - 字符串
     */
    public static final String TYPE_STRING = "String";
    /**
     * 参数类型 - 整数
     */
    public static final String TYPE_INTEGER = "Integer";
    /**
     * 参数类型 - 浮点，小数
     */
    public static final String TYPE_DOUBLE = "Double";
    /**
     * 参数类型 - 下拉选
     */
    public static final String TYPE_OPTION = "Option";
    /**
     * 参数类型 - 日期
     */
    public static final String TYPE_DATE = "Date";
    /**
     * 参数类型 - 时间
     */
    public static final String TYPE_TIME = "Time";
    /**
     * 参数类型 - 日期时间
     */
    public static final String TYPE_DATE_TIME = "DateTime";



    @CheckValue(value = "权限标记", isOptional = true)
    private String authorityFlag;

    @CheckValue(value = "接口", isOptional = true)
    private String api;

    @CheckValue(value = "父节点Id")
    private Integer parentId;

    @CheckValue(value = "节点Id", isOptional = true)
    private Integer nodeId;

    @CheckValue(value = "参数名称")
    private String name;

    @CheckValue(value = "参数代码")
    private String code;

    @CheckValue(value = "参数类型：Map、List、String、Integer、Double、Date、Time、DateTime")
    private String type;

    @CheckValue(value = "参数示例", isOptional = true)
    private String examples;

    @CheckValue(value = "是否必传项（Y：是  N：否）", isOptional = true)
    private String isRequired;

    @CheckValue(value = "参数值是否可编辑（Y：是  N：否）", isOptional = true)
    private String isEditable;

    @CheckValue(value = "此配置是否是模板 N表示不同示例的展示数据（Y：是  N：否）", isOptional = true)
    private String isTemplate;

    @CheckValue(value = "参数正则规则", isOptional = true)
    private String regex;

    @CheckValue(value = "参数描述", isOptional = true)
    private String paramDesc;

    @CheckValue(value = "子数据", isOptional = true, isChild = true)
    private List<ApiJsonConfVo> children;


    /**
     * 构建结构树
     *
     * @param data 树结构对象列表
     * @return 构建树结构后的根对象列表
     */
    public static List<ApiJsonConfVo> build(List<ApiJsonConfVo> data, Consumer<ApiJsonConfVo> setValue) {
        List<ApiJsonConfVo> result = new ArrayList<>();
        if (data != null) {
            data.stream().filter(Objects::nonNull).collect(Collectors.groupingBy(i -> i.getAuthorityFlag() + i.getApi())).forEach((k, v) -> {
                v.sort(Comparator.comparing(ApiJsonConfVo::getNodeId));
                Map<Integer, ApiJsonConfVo> treeMap = new HashMap<>();
                v.forEach(i -> {
                    if (treeMap.containsKey(i.getParentId())) {
                        if (setValue != null) {
                            setValue.accept(i);
                        }
                        treeMap.get(i.getParentId()).getChildren().add(i);
                    }
                    treeMap.put(i.getNodeId(), i.setChildren(new ArrayList<>()));
                });
                result.add(v.get(0));
            });
        }
        return result;
    }

    /**
     * 将树结构平铺数据
     *
     * @return 树结构对象列表
     */
    public List<ApiJsonConfVo> tiledData(Integer maxNodeId) {
        return tiledData(maxNodeId == null ? null : new AtomicInteger(maxNodeId), new HashSet<>(), this.nodeId);
    }

    /**
     * 将树结构平铺数据
     *
     * @return 树结构对象列表
     */
    private List<ApiJsonConfVo> tiledData(AtomicInteger maxNodeId, Set<Integer> nodeIds, Integer parentId) {
        List<ApiJsonConfVo> result = new ArrayList<>();
        if (this.nodeId == null || !nodeIds.add(this.nodeId)) {  // 节点ID为空 或者 重复则重新分配ID
            if (maxNodeId != null) {
                this.nodeId = maxNodeId.incrementAndGet();
                this.parentId = parentId;
            }
        }
        if (this.children != null) {
            this.children.forEach(i -> result.addAll(i.tiledData(maxNodeId, nodeIds, this.nodeId)));
        }
        result.add(this.setChildren(null));
        return result;
    }


    /**
     * 递归获取指定层级节点集合
     *
     * @param parentId 父节点
     * @return
     */
    public ApiJsonConfVo getNode(long parentId) {
        if (parentId == this.nodeId) {
            return this;
        }

        if (this.children != null) {
            for (ApiJsonConfVo tree : this.children) {
                ApiJsonConfVo treeTemp = tree.getNode(parentId);
                if (treeTemp != null) {
                    return treeTemp;
                }
            }
        }
        return null;
    }


    /**
     * 深层树copy
     *
     * @return 复制出的新对象
     */
    abstract ApiJsonConfVo copy();

    /**
     * 深层树copy
     *
     * @param condition   设置节点复制的条件  不设置则全部复制
     * @param setNewValue 对复制出的新对象进行值设置
     * @param setOldValue 对复制后的旧对象进行值设置
     * @return 复制出的新对象
     */
    public ApiJsonConfVo copy(Function<ApiJsonConfVo, Boolean> condition, Function<ApiJsonConfVo, ApiJsonConfVo> setNewValue, Function<ApiJsonConfVo, ApiJsonConfVo> setOldValue) {
        if (condition != null && !condition.apply(this)) {
            return null;
        }
        ApiJsonConfVo temp = this.copy();
        if (temp != null) {
            if (Objects.nonNull(setNewValue)) {
                setNewValue.apply(temp);
            }
            if (Objects.nonNull(setOldValue)) {
                setOldValue.apply(this);
            }
            if (temp.getChildren() != null) {
                temp.setChildren(temp.getChildren().stream()
                        .map(i -> i.copy(condition, setNewValue, setOldValue))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList()));
            }
        }
        return temp;
    }

    /**
     * 排除指定节点
     *
     * @param function 节点排除的规则
     * @return 排除后的节点
     */
    public ApiJsonConfVo exclude(Function<ApiJsonConfVo, Boolean> function) {
        if (function.apply(this)) {
            if (this.children != null) {
                this.children = this.children.stream().map(i -> i.exclude(function)).filter(Objects::nonNull).collect(Collectors.toList());
            }
            return this;
        }
        return null;
    }

    /**
     * 根据请求报文设置配置示例默认值
     *
     * @param body 请求报文
     * @return 设置默认值后的配置对象
     */
    public ApiJsonConfVo setApiJsonExample(Object body) {
        if (body != null) {
            if (Collection.class.isAssignableFrom(body.getClass())) {
                boolean offOn = true;
                for (Object item : ((Collection<Object>) body)) {
                    if (offOn) {
                        this.setApiJsonExample(item);
                        offOn = false;
                    } else {
                        ApiJsonConfVo apiJsonTemp = this.copy(i -> YES.equalsIgnoreCase(i.getIsTemplate()), i -> i.setNodeId(null).setParentId(null).setIsTemplate(NO), null);
                        if (Objects.nonNull(apiJsonTemp)) {
                            this.getChildren().addAll(apiJsonTemp.getChildren());
                            apiJsonTemp.setApiJsonExample(item);
                        }
                    }
                }
            } else if (Map.class.isAssignableFrom(body.getClass()) && this.getChildren() != null) {
                Map<String, ApiJsonConfVo> apiJsonMap = this.getChildren().stream()
                        .collect(Collectors.toMap(ApiJsonConfVo::getCode, Function.identity(), (oldValue, newValue) -> newValue));
                ((Map) body).forEach((k, v) -> {
                    if (apiJsonMap.containsKey(String.valueOf(k))) {
                        apiJsonMap.get(String.valueOf(k)).setApiJsonExample(v);
                    }
                });
            } else {
                this.setExamples(String.valueOf(body));
            }
        }
        return this;
    }

    /**
     * 构建报文体校验规则对象
     */
    public CheckItem buildCheckItem() {
        return buildCheckItem(new CheckItem());
    }

    /**
     * 构建报文体校验规则对象
     *
     * @param checkItem 校验规则对象
     */
    private CheckItem buildCheckItem(CheckItem checkItem) {
        if (this.getChildren() != null) {
            checkItem.setCheckItemChild(new TreeMap<>());
            for (ApiJsonConfVo apiJsonItem : this.getChildren()) {
                CheckItem checkItemTemp = new CheckItem()
                        .setName(apiJsonItem.getCode())
                        .setValue(apiJsonItem.getName())
                        .setCode(checkItem.getCode())
                        .setRegexp(apiJsonItem.getRegex())
                        .setRequired(!NO.equalsIgnoreCase(apiJsonItem.getIsRequired()))
                        .setChild(true);
                checkItem.getCheckItemChild().put(checkItemTemp.getName(), checkItemTemp);
                apiJsonItem.buildCheckItem(checkItemTemp);
            }
        }
        return checkItem;
    }
}
