package com.ck.ocr;

import com.ck.tools.utils.ImageUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) {

//        BufferedImage image = ImageUtil.readImage("E:\\data\\00015.jpeg");
        BufferedImage image = ImageUtil.readImage("E:\\data\\666.jpg");
//        BufferedImage image = ImageUtil.readImage("E:\\data\\test1.jpeg");
//        BufferedImage image = ImageUtil.readImage("E:\\data\\001.jpg");
//        BufferedImage image = ImageUtil.readImage("E:\\data\\002.png");


        imageProcessor(image);


    }


    /**
     * 图像处理
     *
     * @param image
     */
    public static void imageProcessor(BufferedImage image) {
        // 图像提升亮度
        image = ImageUtil.adjustBrightness(image, 86, 24);
        ImageUtil.writeImage(image, new File("E:\\data\\sharpen001.jpg"), "jpg");

        // 图像灰度
        image = ImageUtil.grayImage(3, image);

        // 图像二值化
        image = ImageUtil.binaryImage(image, 90);

        // 图像放大
//        image = ImageUtil.setImageRedraw2D(image, image.getWidth() * 4, image.getHeight() * 4);

        ImageUtil.writeImage(image, new File("E:\\data\\sharpen001.jpg"), "jpg");

        int white = ImageUtil.colorToRGB(0, 255, 255, 255);

        List<List<PrintRGB>> printRGBList = new ArrayList<>();
        for (int x = 0; x < image.getWidth(); x++) {
            printRGBList.add(new ArrayList<>());
            for (int y = 0; y < image.getHeight(); y++) {
                PrintRGB printRGB = PrintRGB.build(image, x, y);
                printRGBList.get(printRGBList.size() - 1).add(printRGB);
            }
        }

        BufferedImage lineImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        // 找到横边
        List<List<PrintRGB>> lines = new ArrayList<>();
        List<PrintRGB> lineList = new ArrayList<>();

        for (int y = 0; y < printRGBList.get(0).size(); y++) {
            for (int x = 0; x < printRGBList.size(); x++) {
                PrintRGB item = printRGBList.get(x).get(y);

                if (item.isRGB(image, ImageUtil.PrintType_IsCenterBottom, white)) {
                    boolean off = true;
                    for (int i = 0; i < 4; i++) {
                        if (printRGBList.size() > x + i) {
                            if (!printRGBList.get(x + i).get(y).isRGB(image, ImageUtil.PrintType_IsCenterBottom, white)
                            ) {
                                off = false;
                            }
                        }
                    }
                    if (off) {
                        if (lineList.isEmpty() || Math.abs(item.getX() - lineList.get(lineList.size() - 1).getX()) < 20) {
                            lineList.add(item);
                            break;
                        } else {
                            if (lineList.size()<10){
                                lineList.clear();
                            }else{
                                lines.add(lineList);
                                lineList = new ArrayList<>();
                            }
                        }
                    }
                }
            }
        }

        lines.add(lineList);


//        lines.stream().max(Comparator.comparing(List::size)).ifPresent(item -> {
//            item.forEach(i -> {
//                lineImage.setRGB(i.getX(), i.getY(), ImageUtil.colorToRGB(0, 255, 153, 0));
//            });
//        });

        lines.forEach(printRGBS -> printRGBS.forEach(printRGB -> lineImage.setRGB(printRGB.getX(), printRGB.getY(), ImageUtil.colorToRGB(0, 255, 153, 0))));


        ImageUtil.writeImage(lineImage, new File("E:\\data\\sharpen001.jpg"), "jpg");





        for (int x = 0; x < image.getWidth(); x++) {
            printRGBList.add(new ArrayList<>());
            for (int y = 0; y < image.getHeight(); y++) {
               PrintRGB printRGB = PrintRGB.build(image,x,y);
                printRGBList.get(printRGBList.size() - 1).add(printRGB);


                if (printRGB.isRGB(image,ImageUtil.PrintType_IsBottom,white)) {
                    /* 底侧三个像素都是白色 */
                    if (printRGB.isRGB(image,ImageUtil.PrintType_IsLeftCenter,white) && printRGB.isRGB(image,ImageUtil.PrintType_IsRightCenter,white)) {
                        image.setRGB(x, y, white);
                    }
                } else if (printRGB.isRGB(image,ImageUtil.PrintType_IsTop,white)) {
                    /* 顶部三个像素都是白色 */
                    if (printRGB.isRGB(image,ImageUtil.PrintType_IsLeftCenter,white) && printRGB.isRGB(image,ImageUtil.PrintType_IsRightCenter,white)) {
                        image.setRGB(x, y, white);
                    }
                } else if (printRGB.isRGB(image,ImageUtil.PrintType_IsLeft,white)) {
                    /* 左侧三个都是白色 */
                    if (printRGB.isRGB(image,ImageUtil.PrintType_IsCenterTop,white) && printRGB.isRGB(image,ImageUtil.PrintType_IsCenterBottom,white)) {
                        image.setRGB(x, y, white);
                    }
                } else if (printRGB.isRGB(image,ImageUtil.PrintType_IsRight,white)) {
                    /* 右侧三个都是白色 */
                    if (printRGB.isRGB(image,ImageUtil.PrintType_IsCenterTop,white) && printRGB.isRGB(image,ImageUtil.PrintType_IsCenterBottom,white)) {
                        image.setRGB(x, y, white);
                    }
                }

            }
        }

        ImageUtil.writeImage(image, new File("E:\\data\\sharpen001.jpg"), "jpg");


    }

    @Data
    @Accessors(chain = true)
    public static class PrintRGB {

        /**
         * 构建 PrintRGB 对象
         */
        public static PrintRGB build(BufferedImage image, int x, int y) {
            return new PrintRGB().setX(x).setY(y);
        }

        private Integer x;  // 中间像素点 横坐标
        private Integer y;  // 中间像素点 纵坐标


        /**
         * 判断位置像素点的颜色
         *
         * @param printType 验证的类型 参考上面常量 PrintType_
         * @param rgb       判断的颜色
         * @return true 符合要求 false 不符合要求
         */
        public boolean isRGB(BufferedImage image,String printType, int rgb) {
            return ImageUtil.isRGB(image,60,printType, rgb,x,y);
        }

    }


    /**
     * 图像文字识别
     *
     * @param image
     */
    public static void tesseract(BufferedImage image) {

        // 图像提升亮度
        image = ImageUtil.adjustBrightness(image, 86, 24);
        ImageUtil.writeImage(image, new File("E:\\data\\sharpen001.jpg"), "jpg");

        // 图像灰度
        image = ImageUtil.grayImage(3, image);

        // 图像二值化
        image = ImageUtil.binaryImage(image, 190);

        // 图像放大
//        image = ImageUtil.setImageRedraw2D(image, image.getWidth() * 4, image.getHeight() * 4);


        // 设置语言库位置
//        String projectPath = Main.class.getClassLoader().getResource("").getPath()+"tessdata";
        String projectPath = Main.class.getClassLoader().getResource("").getPath() + "tessdata_best";
        projectPath = projectPath.substring(1);


        ITesseract tesseract = new Tesseract();
        // 设置语言库地址
        tesseract.setDatapath(projectPath);
        // 设置语言  对应语言库文件名
        tesseract.setLanguage("chi_sim");

        // 设置引擎模式是神经网络LSTM引擎
        tesseract.setOcrEngineMode(1);

        // 使用 OSD 进行自动页面分割进行图像处理
        tesseract.setPageSegMode(1);

        try {
            String result = tesseract.doOCR(image);
//            List<Word> words = instance.getWords(image,2);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
