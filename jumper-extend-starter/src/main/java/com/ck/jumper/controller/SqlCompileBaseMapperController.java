package com.ck.jumper.controller;

import com.ck.tools.com.TResult;
import com.ck.jumper.mybatis.SqlCompileBaseMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 动态加载BaseMapper
 */
@RequestMapping("/jumperSqlCompile")
public class SqlCompileBaseMapperController {

    @Resource
    private SqlCompileBaseMapper sqlCompileBaseMapper;

    /**
     * Sql动态构建BaseMapper
     * @param className 指定BaseMapper类名
     * @param sql 构建的sql
     * @return true 成功 false 失败
     */
    @GetMapping("/registry")
    @ResponseBody
    public TResult<Boolean> registryMapper(@RequestParam(value = "className") String className, @RequestParam(value = "sql") String sql) {
        try {
            return TResult.success(sqlCompileBaseMapper.registryBaseMapper(className, sql));
        } catch (Exception e) {
            return TResult.build(TResult.Code.OPERATE_ERROR, e.getMessage());
        }
    }

    /**
     * 重置Sql动态构建BaseMapper
     * @param className 指定BaseMapper类名
     * @param sql 构建的sql
     * @return true 成功 false 失败
     */
    @GetMapping("/reset")
    @ResponseBody
    public TResult<Boolean> resetMapper(@RequestParam(value = "className") String className, @RequestParam(value = "sql") String sql) {
        try {
            return TResult.success(sqlCompileBaseMapper.resetBaseMapper(className, sql));
        } catch (Exception e) {
            return TResult.build(TResult.Code.OPERATE_ERROR, e.getMessage());
        }
    }

    /**
     * 卸载BaseMapper
     * @param className 指定Base
     * @return true 成功 false 失败
     */
    @GetMapping("/destroy")
    @ResponseBody
    public TResult<Boolean> destroyMapper(@RequestParam(value = "className") String className) {
        try {
            return TResult.success(sqlCompileBaseMapper.destroyBaseMapper(className));
        } catch (Exception e) {
            return TResult.build(TResult.Code.OPERATE_ERROR, e.getMessage());
        }
    }
}
