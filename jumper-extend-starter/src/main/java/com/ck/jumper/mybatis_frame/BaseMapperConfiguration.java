package com.ck.jumper.mybatis_frame;

import org.apache.ibatis.session.Configuration;

/**
 * 基础 Mapper类型配置
 * @author Cyk
 * @version 1.0
 * @since 2023/4/11 17:32
 **/
public interface BaseMapperConfiguration {

    Configuration getConfig();

    Class<?> getBaseMapperClass();

    /**
     * 删除 MapperRegistry 中记录的Mapper对象Class<T>
     * 此方法违背纲常天理不容外部谨慎使用
     *
     * @param mapper Mapper ClassName
     * @return 是否删除成功
     */
   boolean removeBaseMapper(Object mapper);
}
