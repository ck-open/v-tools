package com.ck.jumper.mybatis_frame;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisMapperRegistry;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.override.MybatisMapperProxy;
import com.baomidou.mybatisplus.core.toolkit.GlobalConfigUtils;
import com.ck.jumper.utils.QueryUtil;
import com.ck.spring.tools.spring.SpringContextUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.binding.MapperProxy;
import org.apache.ibatis.binding.MapperRegistry;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.session.Configuration;
import org.mybatis.spring.SqlSessionTemplate;

import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * 基础 Mapper类型配置
 * @author Cyk
 * @version 1.0
 * @since 2023/4/11 17:23
 **/
@Getter
@Slf4j
public class BaseMapperConfigurationMyBatisPlus implements BaseMapperConfiguration {

    private final Configuration config;

    /**
     * 基础 Mapper类型
     */
    private final Class<?> baseMapperClass;

    public BaseMapperConfigurationMyBatisPlus(Class<?> baseMapperClass) {
        this.baseMapperClass = baseMapperClass;
        this.config = Objects.requireNonNull(SpringContextUtil.getBean(SqlSessionTemplate.class)).getConfiguration();
    }


    /**
     * 删除 MapperRegistry 中记录的Mapper对象Class<T>
     * 此方法违背纲常天理不容禁止外部使用
     *
     * @param mapper Mapper ClassName
     * @return 是否删除成功
     */
    public boolean removeBaseMapper(Object mapper) {

        if (!BaseMapper.class.isAssignableFrom(mapper.getClass())) {
            log.error("非 MyBatisPlus BaseMapper 类型对象，无法卸载BaseMapper实例");
        }

        Field knownMappersField = null;
        Field mapperInterfaceField = null;
        Field loadedResourcesField = null;
        Field reflectorFactoryField = null;
        Field mappedStatementsField = null;

        try {

            Class<?> mapperRegistryClass;
            Class<?> mapperProxyClass;
            if (MybatisMapperRegistry.class.isAssignableFrom(this.config.getMapperRegistry().getClass())) {
                mapperRegistryClass = MybatisMapperRegistry.class;
                mapperProxyClass = MybatisMapperProxy.class;
            } else {
                mapperRegistryClass = MapperRegistry.class;
                mapperProxyClass = MapperProxy.class;
            }
            knownMappersField = mapperRegistryClass.getDeclaredField("knownMappers");
            mapperInterfaceField = mapperProxyClass.getDeclaredField("mapperInterface");
            loadedResourcesField = Configuration.class.getDeclaredField("loadedResources");
            reflectorFactoryField = Configuration.class.getDeclaredField("reflectorFactory");
            mappedStatementsField = MybatisConfiguration.class.getDeclaredField("mappedStatements");

            knownMappersField.setAccessible(true);
            mapperInterfaceField.setAccessible(true);
            loadedResourcesField.setAccessible(true);
            reflectorFactoryField.setAccessible(true);
            mappedStatementsField.setAccessible(true);

            Object knownMappersValue = knownMappersField.get(this.config.getMapperRegistry());
            Object mapperInterface = mapperInterfaceField.get(Proxy.getInvocationHandler(mapper));
            Object loadedResources = loadedResourcesField.get(this.config);
            Object reflectorFactory = reflectorFactoryField.get(this.config);
            Object mappedStatementsValue = mappedStatementsField.get(this.config);
            Set<String> mapperRegistryCache = GlobalConfigUtils.getMapperRegistryCache(this.config);


            if (loadedResources != null && Set.class.isAssignableFrom(loadedResources.getClass())) {
                ((Set) loadedResources).remove(mapperInterface.toString());
            }

            if (mappedStatementsValue != null && Map.class.isAssignableFrom(mappedStatementsValue.getClass())) {
                String mapperClassName = mapperInterface.toString();
                if (mapperClassName.contains("interface")) {
                    mapperClassName = mapperClassName.replaceAll("interface", "").trim();
                }
                String finalMapperClassName = mapperClassName;

                mapperRegistryCache.remove(mapperInterface.toString());
                Map<String, MappedStatement> map = (Map<String, MappedStatement>) mappedStatementsValue;
                new HashSet<String>(map.keySet()).stream().filter(i -> map.get(i).getId().startsWith(finalMapperClassName)).forEach(map::remove);
            }


            if (reflectorFactory != null && ReflectorFactory.class.isAssignableFrom(reflectorFactory.getClass())) {
                Field reflectorMapField = reflectorFactory.getClass().getDeclaredField("reflectorMap");
                reflectorMapField.setAccessible(true);
                Object reflectorMap = reflectorMapField.get(reflectorFactory);
                if (reflectorMap != null && Map.class.isAssignableFrom(reflectorMap.getClass())) {
                    ((Map) reflectorMap).remove(QueryUtil.getMapperEntityClass((BaseMapper) mapper));
                }
                reflectorMapField.setAccessible(false);
            }

            if (knownMappersValue != null && Map.class.isAssignableFrom(knownMappersValue.getClass())) {
                Map knownMappers = (Map) knownMappersValue;
                return !knownMappers.containsKey(mapperInterface) || knownMappers.remove(mapperInterface) != null;
            }
            return false;
        } catch (Exception e) {
            log.error("无法删除 MapperRegistry 中的Mapper Class<?> 缓存。 error:" + e.getMessage(),e);
        } finally {
            if (knownMappersField != null) {
                knownMappersField.setAccessible(false);
            }
            if (mapperInterfaceField != null) {
                mapperInterfaceField.setAccessible(false);
            }
            if (loadedResourcesField != null) {
                loadedResourcesField.setAccessible(false);
            }
            if (reflectorFactoryField != null) {
                reflectorFactoryField.setAccessible(false);
            }
            if (mappedStatementsField != null) {
                mappedStatementsField.setAccessible(false);
            }
        }
        return false;
    }
}
