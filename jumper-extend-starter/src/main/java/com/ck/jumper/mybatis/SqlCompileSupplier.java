package com.ck.jumper.mybatis;

import java.util.Map;

/**
 * 提供需要编译成 BaseMapper 对象的Sql 列表 将在服务启动后执行
 * @author Cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
public interface SqlCompileSupplier {

    /**
     * 需要编译的 Sql 列表
     * key: beanName  value: Sql语句
     *
     * @return Sql列表
     */
    Map<String, String> getSql();

}
