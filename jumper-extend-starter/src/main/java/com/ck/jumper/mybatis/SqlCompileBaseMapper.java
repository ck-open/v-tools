package com.ck.jumper.mybatis;

import com.ck.jumper.event.EventHandler;
import com.ck.jumper.mybatis_frame.BaseMapperConfiguration;
import com.ck.tools.exception.JavaCompilerException;
import com.ck.tools.reflect.JavaCompilerManager;
import com.ck.tools.reflect.JavaCompilerUtils;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * 依据自定义 javaCode 动态生成 mybatis plus BaseMapper  并注入到 Spring 容器
 * @author Cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
@Slf4j
public class SqlCompileBaseMapper {
    private static final Set<String> baseMapperBeanNames = Collections.synchronizedSet(new HashSet<>());

    private final DefaultListableBeanFactory listableBeanFactory;
    private final SqlSessionTemplate sqlSessionTemplate;
    private final BaseMapperConfiguration baseMapperConfiguration;
    private String packagePath = "jumper.db.mapper";
    private SqlCompileTemplate sqlCompileTemplate;
    private JavaCompilerHandler JavaCompilerHandler;

    public SqlCompileBaseMapper(DefaultListableBeanFactory listableBeanFactory) {
        this.listableBeanFactory = listableBeanFactory;

        try {
            this.sqlSessionTemplate = this.listableBeanFactory.getBean(SqlSessionTemplate.class);
        } catch (Exception e) {
            throw new RuntimeException("构建 SqlCompileBaseMapper 失败，未获取到 SqlSessionTemplate 实例");
        }

        try {
            this.sqlCompileTemplate = this.listableBeanFactory.getBean(SqlCompileTemplate.class);
        } catch (Exception e) {
            this.sqlCompileTemplate = new SqlCompileTemplate();
        }

        try {
            this.JavaCompilerHandler = this.listableBeanFactory.getBean(JavaCompilerHandler.class);
        } catch (Exception e) {
            this.JavaCompilerHandler = (javaCode)->{
                JavaCompilerManager javaCompilerManager = JavaCompilerUtils.compilerString(javaCode);
                if (javaCompilerManager.isResult()){
                    return javaCompilerManager.classLoader().getClassMap();
                }else{
                    throw new JavaCompilerException(String.join(",", javaCompilerManager.getDiagnosticMsg()));
                }
            };
        }

        try {
            this.baseMapperConfiguration = this.listableBeanFactory.getBean(BaseMapperConfiguration.class);
        } catch (Exception e) {
            throw new RuntimeException("获取 BaseMapperConfiguration 实例失败 Jumper 无法启动");
        }
    }

    /**
     * 编译自定义 sql 为 BaseMapper 接口并注入到 Spring 容器
     * <p>
     * 生成的 Mapper.class 存储的包路径  优先 配置文件中的 jumper.package_mapper 进行配置
     * 默认 保存在项目路径 jumper.db.mapper 路径下
     *
     * @param className 类名
     * @param sql 自定义 sql
     * @return true 成功
     */
    public boolean registryBaseMapper(String className, String sql) {
        String beanName = this.sqlCompileTemplate.toHump(this.sqlCompileTemplate.getMapperName(className.trim()));
        try {
            if (this.listableBeanFactory.containsBean(beanName)) {
                log.info(String.format("动态 Sql 编译BaseMapper失败，ClassName: %s 已存在容器中", beanName));
                return false;
            }
            if (!baseMapperBeanNames.contains(beanName)) {
                String javaCode = this.sqlCompileTemplate.getBaseMapperJavaCode(packagePath, className, sql);
                Map<String, Class<?>> mapperClassMap = this.JavaCompilerHandler.compiler(javaCode);

                if (ObjectUtils.isEmpty(mapperClassMap)) {
                    EventHandler.publish(new EventHandler.SqlCompileError(className));
                    throw new RuntimeException(String.format("动态 Sql 编译BaseMapper失败，ClassName: %s", className));
                }
                mapperClassMap.forEach((k, v) -> {
                    this.sqlSessionTemplate.getConfiguration().addMapper(v);
                    this.listableBeanFactory.registerSingleton(this.sqlCompileTemplate.toHump(beanName), this.sqlSessionTemplate.getMapper(v));
                });
                baseMapperBeanNames.add(beanName);
                return true;
            }
        } catch (Exception e) {
            baseMapperBeanNames.remove(beanName);
            log.error(" 向Spring 注入自定义BaseMapper接口失败", e);
            if (JavaCompilerException.class.isAssignableFrom(e.getClass())){
                throw e;
            }
        }
        return false;
    }

    /**
     * 编译自定义 sql 为 BaseMapper 接口并注入到 Spring 容器
     * <p>
     * 生成的 Mapper.class 存储的包路径  优先 配置文件中的 jumper.package_mapper 进行配置
     * 默认 保存在项目路径 jumper.db.mapper 路径下
     *
     * @param className 类名
     * @param sql 自定义 sql
     * @return true 成功
     */
    public boolean resetBaseMapper(String className, String sql) {
        try {
            if (destroyBaseMapper(className)) {
                return registryBaseMapper(className, sql);
            }
        } catch (Exception e) {
            log.error(" 重置 Spring 注入自定义BaseMapper接口失败", e);
            if (JavaCompilerException.class.isAssignableFrom(e.getClass())){
                throw e;
            }
        }
        return false;
    }

    /**
     * 卸载容器中的 BaseMapper 对象
     *
     * @param beanName 自定义的 BaseMapper 对象 name名称
     * @return true 成功
     */
    public boolean destroyBaseMapper(String beanName) {
        try {
            beanName = this.sqlCompileTemplate.toHump(this.sqlCompileTemplate.getMapperName(beanName.trim()));
            if (baseMapperBeanNames.contains(beanName)) {
                Object o = this.listableBeanFactory.getBean(beanName, this.baseMapperConfiguration.getBaseMapperClass());

                this.listableBeanFactory.destroySingleton(beanName);
                baseMapperBeanNames.remove(beanName);
                this.baseMapperConfiguration.removeBaseMapper(o);
                return true;
            } else {
                log.info(String.format("禁止破坏容器中非动态编译的BaseMapperBean对象 BeanName:%s", beanName));
            }
        } catch (Exception e) {
            log.error("卸载 BaseMapper 失败 Error: " + e.getMessage(), e);
            if (JavaCompilerException.class.isAssignableFrom(e.getClass())){
                throw e;
            }
        }
        return false;
    }


    /**
     * 启动事件监听  自动注入自定义SQL 生成 BaseMapper
     *
     * @author Cyk
     * @since 14:37 2022/7/15
     **/
    @EventListener({ApplicationReadyEvent.class})
    public void applicationReadyEvent() {
        if (this.listableBeanFactory != null) {
            Map<String, SqlCompileSupplier> sqlCompileSuppliers = this.listableBeanFactory.getBeansOfType(SqlCompileSupplier.class);
            if (ObjectUtils.isEmpty(sqlCompileSuppliers)) {
                log.info("无需要自动生成 BaseMapper 的 SqlCompileSupplier");
                return;
            }
            sqlCompileSuppliers.forEach((name, sqlCompileSupplier) -> {
                Map<String, String> sqlMap = sqlCompileSupplier.getSql();
                if (!ObjectUtils.isEmpty(sqlMap)) {
                    sqlMap.forEach((k, v) -> {
                        try {
                            registryBaseMapper(k, v);
                        } catch (Exception e) {
                            log.error("初始化自定义Sql 编译BaseMapper失败", e);
                        }
                    });
                }
            });
        }
    }
}
