package com.ck.jumper.event;

import com.ck.spring.tools.spring.SpringContextUtil;
import org.springframework.context.ApplicationEvent;

/**
 * 事件处理器
 *
 * @author Cyk
 * @version 1.0
 * @since 2022/4/24 10:46
 **/
public class EventHandler {

    public static <T extends ApplicationEvent> void publish(T event) {
        SpringContextUtil.getApplicationContext().publishEvent(event);
    }


    /**
     * Sql 编译BaseMapper 失败事件通知
     */
    public static class SqlCompileError extends ApplicationEvent {
        public SqlCompileError(Object source) {
            super(source);
        }
    }
}
