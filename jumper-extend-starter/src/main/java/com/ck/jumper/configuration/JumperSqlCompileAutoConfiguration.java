package com.ck.jumper.configuration;


import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.jumper.controller.SqlCompileBaseMapperController;
import com.ck.jumper.mybatis.SqlCompileBaseMapper;
import com.ck.jumper.mybatis_frame.BaseMapperConfiguration;
import com.ck.jumper.mybatis_frame.BaseMapperConfigurationMyBatisPlus;
import com.ck.spring.tools.configuration.SpringToolsAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * JumperSqlCompileAutoConfiguration
 * @author cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
@Slf4j
@ConditionalOnProperty(prefix = "jumper", name = "enabled", havingValue = "true")
@Configuration
@AutoConfigureAfter({SpringToolsAutoConfiguration.class, MybatisPlusAutoConfiguration.class})
public class JumperSqlCompileAutoConfiguration {

    /**
     * 注入动态生成编译 BaseMapper源码 操作接口
     */
    @Bean
    @ConditionalOnProperty(prefix = "jumper.SqlCompile", name = "controller", havingValue = "true")
    @ConditionalOnMissingBean(SqlCompileBaseMapperController.class)
    public SqlCompileBaseMapperController sqlCompileBaseMapperController() {
        SqlCompileBaseMapperController exceptionHandler = new SqlCompileBaseMapperController();
        log.info("SqlCompileBaseMapperController [{}]", exceptionHandler);
        return exceptionHandler;
    }

    /**
     * 指定MyBatisPlus BaseMapper
     */
    @Bean
    @ConditionalOnMissingBean(BaseMapperConfiguration.class)
    public BaseMapperConfiguration baseMapperConfiguration() {
        BaseMapperConfiguration baseMapperConfiguration = new BaseMapperConfigurationMyBatisPlus(BaseMapper.class);
        log.info("BaseMapperConfiguration [{}]", baseMapperConfiguration);
        return baseMapperConfiguration;
    }

    /**
     * 注入动态生成编译 BaseMapper源码 功能
     */
    @Bean("sqlCompileBaseMapper")
    @ConditionalOnMissingBean(SqlCompileBaseMapper.class)
    public SqlCompileBaseMapper sqlCompileBaseMapper(DefaultListableBeanFactory beanFactory) {
        SqlCompileBaseMapper sqlCompileBaseMapper = new SqlCompileBaseMapper(beanFactory);
        log.info("SqlCompileBaseMapper [{}]", sqlCompileBaseMapper);
        return sqlCompileBaseMapper;
    }
}
