package com.ck.method.log.aspect;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;

/**
 * 切面 @MethodLog 注解
 *
 * @author cyk
 * @since 2023-09-11
 */
@Slf4j
@Aspect
public class MethodLogAspect implements MethodInterceptor {

    private final MethodLogHandler methodLogHandler;

    public MethodLogAspect(MethodLogHandler methodLogHandler) {
        if (methodLogHandler == null) {
            throw new RuntimeException("methodLogHandler is null");
        }
        this.methodLogHandler = methodLogHandler;
    }

    /**
     * 静态注解切点
     *
     * @param joinPoint 切面参数
     * @return 方法返回值
     */
    @Around("@annotation(com.ck.method.log.aspect.MethodLog)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        return around(joinPoint
                , joinPoint.getTarget().getClass()
                , ((MethodSignature) joinPoint.getSignature()).getMethod()
                , joinPoint.getArgs());
    }

    /**
     * 动态切点 dynamic
     *
     * @param invocation 方法调用
     * @return 方法返回值
     */
    @Override
    public Object invoke(@NonNull MethodInvocation invocation) throws Throwable {
        return around(invocation, invocation.getMethod().getDeclaringClass(), invocation.getMethod(), invocation.getArguments());
    }

    /**
     * 切点 功能扩展
     *
     * @return 方法返回值
     */
    private Object around(Object joinPoint, Class<?> entityClass, Method entityMethod, Object[] values) throws Throwable {
        log.info("Method log starts to execute ...");
        StopWatch stopWatch = new StopWatch("Method log");
        stopWatch.start("parseParameters");

        MethodLogVo methodLogVo = MethodLogVo.build(entityClass, entityMethod, values);
        stopWatch.stop();
        stopWatch.start("parseReturnValue");
        try {
            if (ProceedingJoinPoint.class.isAssignableFrom(joinPoint.getClass())) {
                methodLogVo.setReturnVal(((ProceedingJoinPoint) joinPoint).proceed());
            } else if (MethodInvocation.class.isAssignableFrom(joinPoint.getClass())) {
                methodLogVo.setReturnVal(((MethodInvocation) joinPoint).proceed());
            }
            return methodLogVo.getReturnVal();
        } catch (Exception e) {
            methodLogVo.setReturnException(e);
            throw e;
        } finally {
            if (stopWatch.isRunning()) {
                stopWatch.stop();
            }
            stopWatch.start("consumer");
            this.methodLogHandler.push(methodLogVo);
            stopWatch.stop();
            log.info("Method log end to execute, run time {} seconds .", stopWatch.getTotalTimeSeconds());
        }
    }
}