package com.ck.method.log.aspect;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;


/**
 * 配置方法映射列表
 */
public interface MethodLogConfig {

    String PrintName = LogHelper.PrintName;
    String Expressions = LogHelper.Expressions;

    default Map<String, String> buildExpressions(Method method, String... params) {
        return LogHelper.buildExpressions(method, params);
    }

    default Method getMethod(Class<?> entityClass, String methodName, Class<?>... parameterTypes) {
        return LogHelper.getMethod(entityClass, methodName, parameterTypes);
    }

    default String parameterTypeNames(Class<?>... target) {
        return LogHelper.parameterTypeNames(target);
    }

    /**
     * 切面扫描配置的pointcut表达式
     * 默认支持 @MethodLog 注解方式
     * <p>
     * 例如：
     * execution(public * *(..))、execution(* set*(..))
     * execution(* com.xyz.service.AccountService.*(..))
     * execution(* com.xyz.service..*.*(..))
     * execution(public * com.ck.test.service.TestServiceImpl.get(..))
     * </p>
     *
     * @return 切点表达式列表
     */
    List<String> getExpressions();

    /**
     * 获取切面方法参数转换配置信息
     *
     * @param entityClass  目标类
     * @param entityMethod 目标方法
     * @return Map<T.FieldName, valConf>
     * key：为MethodLogConsumer 指定的泛型T（logVo）的字段名称。
     * value：从方法切面信息中的取值方式配置，例如：
     * name:{常量值}
     * name:方法形参名称
     * name:方法形参名称.形参对象字段名
     * name:方法形参名称.[1]数组下标1对应的对象字段名
     * name:$  ($表示返回值)
     * name:$.返回值对象某个字段名  ($表示返回值)
     */
    Map<String, String> getMethodLogConfig(Class<?> entityClass, Method entityMethod);
}
