package com.ck.method.log.test.service;

import com.ck.method.log.aspect.MethodLogConsumer;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Slf4j
@Service
public class MethodLogConsumerImpl implements MethodLogConsumer<MethodLogConsumerImpl.TestVo, Object> {

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private TestService testService;

    /**
     * 切面截取的方法出入数据消费
     *
     * @param entityClass   切面目标类
     * @param methodName    切面目标方法
     * @param logVo         根据 @MethodLog 注解配置的格式转换的Log日志对象
     * @param parametersAll 方法全部参数信息
     * @param returnVo      方法返回值
     */
    @Override
    public void consumer(Class<?> entityClass, String methodName, TestVo logVo, Map<String, Object> parametersAll, Object returnVo, Throwable returnException) {
        try {
            log.info("Consumer Class:{},Method:{}", entityClass.getName(), methodName);
            log.info("LogVo:{}", objectMapper.writeValueAsString(logVo));
            log.info("ReturnVo:{}", objectMapper.writeValueAsString(returnVo));
            log.info("ParametersAll:{}", objectMapper.writeValueAsString(parametersAll));
            log.info(testService.toString());
        } catch (Exception ignored) {
        }
    }


    @Data
    @Accessors(chain = true)
    public static class TestVo {
        private String orderNo;
        private String name;
        private String address;
        private String idNumber;
    }
}
