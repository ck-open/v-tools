package com.ck.method.log.aspect;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface MethodLog {
    /**
     * 配置需要提取的参数列表 为空则不提取
     */
    Param[] value() default {};

    /**
     * 是否提取全部参数  为true 则 value值失效
     */
    boolean isAll() default false;
}