package com.ck.method.log.aspect;

import com.ck.tools.reflect.ClassUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 切面方法 参数信息与相应值 对象
 *
 * @author cyk
 * @since 2023-09-11
 */
@Slf4j
@Data
@Accessors(chain = true)
public class MethodLogVo {
    /**
     * 对象解析
     */
    private ObjectMapper objectMapper;

    /**
     * 切面目标类
     */
    private Class<?> entityClass;

    /**
     * 切面目标类方法
     */
    private Method entityMethod;

    /**
     * 类名
     */
    private String className;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 方法日志注解
     */
    private MethodLog methodLog;

    /**
     * 配置的需要提取的接口参数
     */
    private Map<String, String> methodLogConfig = new LinkedHashMap<>();

    /**
     * 全部参数
     */
    private Map<String, Object> parametersAll;

    /**
     * 方法请求参数  如果非基本类型参数会转换为Map类型
     */
    private Map<String, Object> parameters;

    /**
     * 响应值
     */
    private Object returnVal;

    /**
     * 响应异常信息
     */
    private Throwable returnException;

    /**
     * 构建MethodLogVo 对象
     *
     * @param entityClass  切面目标类
     * @param entityMethod 切面方法
     * @param values       切面参数列表
     * @return 切面数据
     */
    public static MethodLogVo build(Class<?> entityClass, Method entityMethod, Object[] values) {
        return new MethodLogVo()
                .setEntityClass(entityClass)
                .setClassName(entityClass.getName())
                .setEntityMethod(entityMethod)
                .setMethodName(entityMethod.getName())
                .parseMethodLog()
                .parseParameterAll(values);
    }

    /**
     * 将返回值格式化为指定对象
     *
     * @param beanClass 对象类型
     * @param <T>       对象类型
     * @return 返回值
     */
    public <T> T formatReturnBean(Class<T> beanClass) {
        if (beanClass == null || this.returnVal == null) {
            return null;
        } else if (beanClass.isAssignableFrom(this.returnVal.getClass())) {
            return beanClass.cast(this.returnVal);
        }

        return copyBean(this.returnVal, beanClass);
    }

    /**
     * 将请求参数格式化为指定对象
     *
     * @param beanClass 对象类型
     * @param <T>       对象类型
     * @return 返回值
     */
    public <T> T formatParametersBean(Class<T> beanClass) {
        if (beanClass == null) {
            return null;
        }

        return copyBean(this.parameters, beanClass);
    }

    /**
     * 根据配置获取过滤匹配后的参数列表  未配置则返回全部参数
     */
    public void parseParameters() {
        this.parameters = new LinkedHashMap<>();
        if (ObjectUtils.isEmpty(this.methodLogConfig)) {
            this.parameters = this.parametersAll;
        } else {
            for (String k : this.methodLogConfig.keySet()) {
                String[] ls = this.methodLogConfig.get(k).split("\\|");
                for (String v : ls) {
                    if (!ObjectUtils.isEmpty(this.parameters.get(k)) || v == null || v.trim().isEmpty()) {
                        break;
                    }
                    v = v.trim();

                    if (v.startsWith("{") && v.endsWith("}")) {
                        this.parameters.put(k, v.substring(1, v.lastIndexOf("}")));
                    } else if (this.parametersAll.containsKey(v)) {
                        this.parameters.put(k, this.parametersAll.get(v));
                    } else if ("$".equals(v)) {
                        this.parameters.put(k, this.returnVal);
                    } else if (v.contains(".")) {
                        AtomicReference<Object> tempVal = new AtomicReference<>();
                        if (v.startsWith("$")) {
                            tempVal.set(this.returnVal);
                            v = v.substring(2);
                        } else {
                            tempVal.set(this.parametersAll);
                        }
                        ClassUtils.forNames(v, (itemName, isLast) -> {
                            if (tempVal.get() != null) {
                                if (ClassUtils.isBasicType(tempVal.get().getClass())) {
                                    this.parameters.put(k, tempVal.get());
                                    tempVal.set(null);
                                } else {
                                    tempVal.set(parseParameters(itemName, tempVal.get()));
                                    if (isLast) {
                                        this.parameters.put(k, tempVal.get());
                                    }
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    /**
     * 深度解析 Collection 和 Map 或 对象类型
     *
     * @param name 值名称  数组时可以用[0] 来指定下标，例如 list[1]  表示取用户列表中第三条
     * @param val  值对象
     * @return 解析后的值
     */
    private Object parseParameters(String name, Object val) {
        if (Collection.class.isAssignableFrom(val.getClass())) {

            Collection collection = (Collection) val;
            // 兼容多层数组
            while (!ObjectUtils.isEmpty(collection) && Collection.class.isAssignableFrom(collection.iterator().next().getClass())) {
                collection = (Collection) collection.iterator().next();
            }

            int index = 0;

            /* 指定数组下标 */
            if (name.contains("[") && name.contains("]")) {
                String indexStr = name.substring(name.indexOf("["), name.indexOf("]")).replaceAll("\\[", "");
                try {
                    index = indexStr.trim().isEmpty() ? 0 : Integer.parseInt(indexStr);
                } catch (Exception e) {
                    throw new RuntimeException("数组下标设置格式错误：" + name);
                }
                if (name.startsWith("[")) {
                    name = name.substring(name.indexOf("]") + 1);
                } else {
                    name = name.substring(0, name.indexOf("["));
                }
            }

            Object valTemp = null;
            Iterator<Object> iterator = ((Collection) val).iterator();
            while (index-- >= 0 && iterator.hasNext()) {
                valTemp = iterator.next();
            }
            if (valTemp == null || ClassUtils.isBasicType(valTemp.getClass())) {
                return valTemp;
            }
            return parseParameters(name, valTemp);

        } else {
            if (Map.class.isAssignableFrom(val.getClass())) {
                return ((Map) val).get(name);
            } else {
                Field field = ClassUtils.getField(name, val.getClass());
                return ClassUtils.getValueOrNull(field, val);
            }
        }
    }

    /**
     * 获取方法形参列表
     *
     * @param values 参数列表
     * @return 返回值
     */
    private MethodLogVo parseParameterAll(Object[] values) {

        if (this.parametersAll == null) {
            this.parametersAll = new LinkedHashMap<>();
        }

        Parameter[] parameters = this.entityMethod.getParameters();
        if (!ObjectUtils.isEmpty(parameters)) {
            for (int i = 0; i < parameters.length; i++) {
                if (values.length > i) {
                    this.parametersAll.put(parameters[i].getName(), values[i]);
                }
            }
        }
        return this;
    }

    /**
     * 获取注解
     *
     * @return 返回值
     */
    private MethodLogVo parseMethodLog() {
        /* 获取方法签名 */
        this.methodLog = this.entityMethod.getAnnotation(MethodLog.class);
        if (this.methodLog != null) {
            for (Param param : this.methodLog.value()) {
                if (!ObjectUtils.isEmpty(param.name()) && !ObjectUtils.isEmpty(param.value())) {
                    this.methodLogConfig.put(param.name(), param.value());
                }
            }
        }
        return this;
    }

    /**
     * 深度复制 对象
     *
     * @param source 源对象
     * @param beanClass 目标对象类型
     * @param <T> 返回值
     * @return 返回值
     */
    protected  <T> T copyBean(Object source, Class<T> beanClass) {
        if (this.objectMapper == null) {
            this.objectMapper = new ObjectMapper();
        }
        try {
            return this.objectMapper.convertValue(source, beanClass);
        } catch (Exception e) {
            String dataSource = "";
            try {
                dataSource = this.objectMapper.writeValueAsString(source);
            } catch (JsonProcessingException ignored) {
            }
            log.error(String.format("Cannot deserialize dataSource:%s", dataSource), e);
        }
        return ClassUtils.newInstance(beanClass);
    }
}
