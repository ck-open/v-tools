package com.ck.method.log.aspect;

import com.ck.tools.reflect.ClassUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 切面方法 参数信息 处理器
 *
 * @author cyk
 * @since 2023-09-11
 */
@Slf4j
@Data
public class MethodLogHandler {
    private DefaultListableBeanFactory beanFactory;
    /**
     * 容器注入的消费实例列表
     */
    private List<MethodLogConsumer> consumers;

    /**
     * 获取自定义配置
     */
    private MethodLogConfig methodLogConfig;

    /**
     * 接口参数
     */
    private MethodLogVo methodLogVo;

    public MethodLogHandler(DefaultListableBeanFactory beanFactory) {
        try {
            this.beanFactory = beanFactory;
            this.methodLogConfig = this.beanFactory.getBean(MethodLogConfig.class);
        } catch (Exception ignored) {
        }
    }

    /**
     * 方法数据发送到各个实现类进行消费
     *
     * @param methodLogVo 方法数据
     */
    public void push(MethodLogVo methodLogVo) {
        this.methodLogVo = methodLogVo;

        /* 加载自定义接口配置 */
        if (!ObjectUtils.isEmpty(this.getMethodLogConfig())) {
            Map<String, String> config = this.getMethodLogConfig().getMethodLogConfig(this.methodLogVo.getEntityClass(), this.methodLogVo.getEntityMethod());
            if (!ObjectUtils.isEmpty(config)) {
                this.methodLogVo.getMethodLogConfig().putAll(config);
            }
        }
        try {
            consumer();
        } catch (ClassCastException e) {
            log.error("logVo ClassCastException {}", e.getMessage());
        }
    }

    /**
     * 消费切到的数据
     */
    private void consumer() {
        log.info("Method log consumer starts to execute ...");

        /* 解析配置的返回值 */
        this.methodLogVo.parseParameters();

        if (ObjectUtils.isEmpty(this.consumers)) {
            initConsumers();
        }

        this.consumers.forEach(methodLogConsumer -> {
            Class<?> parameterVoClass = Map.class;
            Class<?> returnVoClass = Map.class;
            Map<String, Type[]> interfacesTypes = ClassUtils.getGenericInterfaces(methodLogConsumer, MethodLogConsumer.class);
            if (!ObjectUtils.isEmpty(interfacesTypes) && !ObjectUtils.isEmpty(interfacesTypes.get(MethodLogConsumer.class.getName()))) {
                parameterVoClass = (Class<?>) interfacesTypes.get(MethodLogConsumer.class.getName())[0];
                returnVoClass = (Class<?>) interfacesTypes.get(MethodLogConsumer.class.getName())[1];
            }
            Object logVo = this.methodLogVo.formatParametersBean(parameterVoClass);
            Object returnVo = this.methodLogVo.formatReturnBean(returnVoClass);
            methodLogConsumer.consumer(this.methodLogVo.getEntityClass(), this.methodLogVo.getMethodName(), logVo, this.methodLogVo.getParametersAll(), returnVo, this.methodLogVo.getReturnException());
        });

        log.info("Method log consumer end to execute .");
    }

    private void initConsumers() {
        if (ObjectUtils.isEmpty(this.consumers)) {
            log.info("Method log consumers initialization start ...");
            if (!ObjectUtils.isEmpty(this.getBeanFactory())) {
                Map<String, MethodLogConsumer> methodLogConsumerMap = this.getBeanFactory().getBeansOfType(MethodLogConsumer.class);
                if (!ObjectUtils.isEmpty(methodLogConsumerMap)) {
                    methodLogConsumerMap.keySet().forEach(i -> log.info(String.format("Method log consumers initialization -> %s", i)));
                    this.consumers = methodLogConsumerMap.values().stream().map(i -> (MethodLogConsumer) ClassUtils.getProxyTarget(i)).collect(Collectors.toList());
                }
            }
            if (ObjectUtils.isEmpty(this.consumers)) {
                log.info(String.format("Method log consumers initialization -> %s", "MethodLogConsumer.MethodLogConsumerDefault"));
                this.consumers = Collections.singletonList(new MethodLogConsumer.MethodLogConsumerDefault());
            }
            log.info("Method log consumers initialization end .");
        }
    }
}
