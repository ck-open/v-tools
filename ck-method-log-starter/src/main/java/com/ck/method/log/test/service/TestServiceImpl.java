package com.ck.method.log.test.service;

import com.ck.method.log.test.dto.Person;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

//    @MethodLog({
//            @Param(name = "orderNo",value = "orderNo"),
//            @Param(name = "name",value = "name"),
//            @Param(name = "address",value = "person.familyMembers.[1]address")
//    })
    @Override
    public Person paramReturn(String orderNo, String name, Integer age, Double money, boolean isTime, Person person) {
        return person;
    }

    @Override
    public String paramIsNull() {
        return "paramIsNull";
    }
    @Override
    public void returnVoid() {
    }

    public static void paramReturn() {
    }
}
