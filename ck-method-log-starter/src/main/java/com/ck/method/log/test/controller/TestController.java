package com.ck.method.log.test.controller;


import com.ck.method.log.test.dto.Person;
import com.ck.method.log.test.service.TestService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;

@RequestMapping("/test")
@RestController
public class TestController {

    @Resource
    private TestService testService;

    @Resource
    ObjectMapper objectMapper;

    @GetMapping("/get")
    public String checkCorePolicy(@RequestParam(value = "orderNo", required = false) String orderNo) {
        testService.paramReturn(orderNo, "张三", 25, 86.5, false, new Person()
                .setName("李四").setIdNumber("12345678912354895").setBirthDate(new Date())
                .setAddress("不知道是什么地方").setPhoneNumber("12345678901").setNotes(Arrays.asList("备注1", "备注2"))
                .setFamilyMembers(Arrays.asList(
                        new Person().setName("王五").setIdNumber("1548578912354895").setBirthDate(new Date())
                                .setAddress("不知道是什么地方").setPhoneNumber("45845568").setNotes(Arrays.asList("备注4", "备注5"))
                        , new Person().setName("赵六").setIdNumber("12345678912354895").setBirthDate(new Date())
                                .setAddress("谁知道是什么地方").setPhoneNumber("58756498498").setNotes(Arrays.asList("备注7", "备注8")))));
        testService.paramIsNull();
        testService.returnVoid();
        return "ok";
    }

}
