package com.ck.method.log.test.service;

import com.ck.method.log.test.dto.Person;

public interface TestService {

    Person paramReturn(String orderNo, String name, Integer age, Double money, boolean isTime, Person person);

    String paramIsNull();

    void returnVoid();
}
