package com.ck.method.log.test.service;

import com.ck.method.log.aspect.LogHelper;
import com.ck.method.log.aspect.MethodLogConfigSimple;
import com.ck.method.log.test.dto.Person;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MethodLogConfigImpl extends MethodLogConfigSimple {

    @Override
    public void setting(LogHelper logHelper) {
        logHelper.method(TestServiceImpl.class, "paramReturn", String.class, String.class, Integer.class, Double.class, boolean.class, Person.class)
                .build("idNumber:person.familyMembers.[1]idNumber");
    }
}
