package com.ck.method.log.test.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class Person {
    private String name; // 姓名  
    private String idNumber; // 身份证号码  
    private String gender; // 性别  
    private Date birthDate; // 出生日期
    private String address; // 住址  
    private String phoneNumber; // 电话号码  
    private String email; // 电子邮件地址  
    private String education; // 教育背景  
    private String career; // 职业  
    private String familyStatus; // 家庭状况（已婚、未婚等）  
    private String nationality; // 国籍  
    private Date joinDate; // 入职日期  
    private Date leaveDate; // 离职日期  
    private List<String> notes; // 备注  
    private List<Person> familyMembers; // 家庭成员信息
    private List<String> educationalBackgrounds; // 教育背景信息  
    private List<String> workExperiences; // 工作经历信息
    private List<String> hobbies; // 爱好  
    private List<String> specialties; // 专长  
    private List<String> achievements; // 成就  
    private List<String> awards; // 获奖情况  

    // 省略 getter 和 setter 方法  
}