package com.ck.method.log.configuration;

import com.ck.method.log.aspect.MethodLogAspect;
import com.ck.method.log.aspect.MethodLogHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.Pointcut;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @author Cyk
 * @version 1.0
 * @since 2023/1/30 13:37
 **/
@Slf4j
@Configuration
@AutoConfigureOrder(Integer.MIN_VALUE)
public class MethodLogAutoConfiguration {

    /**
     * 注入方法日志处理器
     */
    @Bean
    @ConditionalOnMissingBean(MethodLogHandler.class)
    public MethodLogHandler methodLogHandler(DefaultListableBeanFactory beanFactory) {
        MethodLogHandler methodLogHandler = new MethodLogHandler(beanFactory);
        log.info("MethodLogHandler [{}]", methodLogHandler);
        return methodLogHandler;
    }

    /**
     * 注入注解方法切面
     */
    @Bean
    @ConditionalOnMissingBean(MethodLogAspect.class)
    public MethodLogAspect methodLogAspect(MethodLogHandler methodLogHandler) {
        MethodLogAspect methodLogAspect = new MethodLogAspect(methodLogHandler);
        log.info("MethodLogAspect [{}]", methodLogAspect);
        return methodLogAspect;
    }


    @Bean
    @Conditional({CheckPointcutConfigCondition.class})
    public Pointcut customExpressionPointcut(MethodLogHandler methodLogHandler) {
        /* AspectJExpressionPointcut提供表达式匹配类和方法。 */
        AspectJExpressionPointcut aspectJExpressionPointcut = new AspectJExpressionPointcut();

        String expressions = null;
        if (!ObjectUtils.isEmpty(methodLogHandler) && !ObjectUtils.isEmpty(methodLogHandler.getMethodLogConfig())) {
            List<String> expressionsConfig = methodLogHandler.getMethodLogConfig().getExpressions();
            if (!ObjectUtils.isEmpty(expressionsConfig)) {
                expressions = String.join(" || ", expressionsConfig);
            }
        }
        log.info("customExpressionPointcut [{}] expressionsConfig :{}", aspectJExpressionPointcut, expressions);
        aspectJExpressionPointcut.setExpression(expressions);
        return aspectJExpressionPointcut;
    }

    @Bean
    @Conditional({CheckPointcutConfigCondition.class})
    public DefaultPointcutAdvisor defaultPointcutAdvisor(Pointcut pointcut, MethodLogAspect methodLogAspect) {
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor();
        log.info("DefaultPointcutAdvisor [{}]", defaultPointcutAdvisor);
        defaultPointcutAdvisor.setPointcut(pointcut);
        defaultPointcutAdvisor.setAdvice(methodLogAspect);
        return defaultPointcutAdvisor;
    }
}
