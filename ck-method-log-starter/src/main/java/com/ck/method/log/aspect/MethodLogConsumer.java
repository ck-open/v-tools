package com.ck.method.log.aspect;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 方法切面 参数 和 返回值 消费
 *
 * @author cyk
 * @since 2023-09-11
 */
public interface MethodLogConsumer<T, R> {

    /**
     * 切面截取的方法出入数据消费
     *
     * @param entityClass     切面目标类
     * @param methodName      切面目标方法
     * @param logVo           根据 @MethodLog 注解配置的格式转换的Log日志对象
     * @param parametersAll   方法全部参数信息
     * @param returnVo        方法返回值
     * @param returnException 相应异常时的信息
     */
    void consumer(Class<?> entityClass, String methodName, T logVo, Map<String, Object> parametersAll, R returnVo, Throwable returnException);


    /**
     * MethodLogConsumer 的默认实现 打印日志
     */
    @Slf4j
    class MethodLogConsumerDefault implements MethodLogConsumer<Object, Object> {
        @Override
        public void consumer(Class<?> entityClass, String methodName, Object logVo, Map<String, Object> parametersAll, Object returnVo, Throwable returnException) {
            log.info("MethodLogConsumer The interface has no implementation class");
        }
    }
}
