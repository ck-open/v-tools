package com.ck.method.log.aspect;

import com.ck.tools.reflect.ClassUtils;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 方法切面日志帮助工具
 */
public class LogHelper {
    public static final String PrintName = "PrintName";  /* 切点方法值为  类名.方法名-方法参数类型名 */
    public static final String Expressions = "Expressions";  /* 切点 Expression 表达式 */

    /**
     * 切点方法配置容器
     */
    private final Map<String, LogHelperVo> paramConf = new LinkedHashMap<>();

    /**
     * 切点 Expression 表达式列表
     */
    @Getter
    private final List<String> expressions = new ArrayList<>();

    /**
     * 临时内部切点对象
     */
    private LogHelperVo logHelperVo;

    /**
     * 构建助手工具
     *
     * @return LogHelper 日志帮助对象
     */
    public static LogHelper helper() {
        return new LogHelper();
    }

    /**
     * 构建配置表达式字符串
     *
     * @param method 目标方法
     * @param params method.getName()  方法名
     * @return Map<String, String> key：属性字段  value：属性对应的值位置，
     * 如：$.name 表示从返回值取值，返回值对象的name属性值
     * param.name 表示参数列表中取值，param参数的name属性值
     */
    public static Map<String, String> buildExpressions(Method method, String... params) {
        Objects.requireNonNull(method, "MethodLogConfig build expressions class Method is null");
        Class<?> entityClass = method.getDeclaringClass();

        Map<String, String> paramMap = new LinkedHashMap<>();
        String printName = String.format("%s.%s-%s", entityClass.getName(), method.getName(), parameterTypeNames(method.getParameterTypes()));
        paramMap.put(MethodLogConfig.PrintName, printName);

        String parameterTypes = "..";
        if (!ObjectUtils.isEmpty(method.getParameterTypes())) {
            parameterTypes = Stream.of(method.getParameterTypes()).map(Class::getName).collect(Collectors.joining(","));
        }
        paramMap.put(MethodLogConfig.Expressions, String.format("execution(* %s.%s(%s))", entityClass.getName(), method.getName(), parameterTypes));
        if (params != null && params.length > 0) {
            Objects.requireNonNull(method.getParameters(), String.format("MethodLogConfig build expressions class Method Parameters does not exist -> %s", printName));
            Map<String, Parameter> parameters = Stream.of(method.getParameters()).collect(Collectors.toMap(Parameter::getName, Function.identity()));
            for (String param : params) {
                if (!param.contains(":")) {
                    Objects.requireNonNull(parameters.get(param), String.format("MethodLogConfig build expressions class Method Parameters [%s] format error -> %s", param, printName));
                }
                String fieldName = param.substring(0, param.indexOf(":"));
                param = param.substring(param.indexOf(":") + 1);
                paramMap.put(fieldName, param);

                if (param.startsWith("{") && param.endsWith("}")) {
                    continue;
                } else if (param.startsWith("$")) {
                    continue;
                }

                if (param.contains(".")) {
                    param = param.substring(0, param.indexOf("."));
                }
                Objects.requireNonNull(parameters.get(param), String.format("MethodLogConfig build expressions class Method Parameters [%s] does not exist -> %s", param, printName));
            }
        }

        return paramMap;
    }

    /**
     * 获取类方法
     *
     * @param entityClass    目标类
     * @param methodName     方法名
     * @param parameterTypes 方法参数类型列表
     * @return Method
     */
    public static Method getMethod(Class<?> entityClass, String methodName, Class<?>... parameterTypes) {
        Objects.requireNonNull(entityClass, "MethodLogConfig build expressions entityClass is null");
        if (ObjectUtils.isEmpty(parameterTypes)) {
            return ClassUtils.getMethod(entityClass, methodName);
        }
        return ClassUtils.getMethod(entityClass, methodName, parameterTypes);
    }

    /**
     * 参数列表类型拼接字符串
     *
     * @param target 参数类型列表
     * @return 多个class.name的字符串已 - 拼接
     */
    public static String parameterTypeNames(Class<?>... target) {
        if (ObjectUtils.isEmpty(target)) {
            return "";
        }
        return Stream.of(target).map(Class::getName).collect(Collectors.joining("-"));
    }

    /**
     * 切点方法配置
     *
     * @param entityClass    切点目标类
     * @param methodName     切点目标类方法
     * @param parameterTypes 切点目标类方法参数列表
     * @return 日志帮助对象
     */
    public LogHelper method(Class<?> entityClass, String methodName, Class<?>... parameterTypes) {
        this.logHelperVo = new LogHelperVo(entityClass, getMethod(entityClass, methodName, parameterTypes));
        return this;
    }

    /**
     * 构建切点配置到容器
     *
     * @param params 切点方法参数转LogVo对象配置
     * @return 日志帮助对象
     */
    public LogHelper build(String... params) {
        this.logHelperVo.setConf(buildExpressions(this.logHelperVo.getMethod(), params));
        this.expressions.add(this.logHelperVo.getConf().get(MethodLogConfig.Expressions));
        this.logHelperVo.getConf().remove(MethodLogConfig.Expressions);
        String key = this.logHelperVo.getConf().get(MethodLogConfig.PrintName);
        this.logHelperVo.getConf().remove(MethodLogConfig.PrintName);
        this.paramConf.put(key, this.logHelperVo);
        this.logHelperVo = null;
        return this;
    }

    /**
     * 获取指定方法配置
     *
     * @param entityMethod 目标方法
     * @return Map<String, String> key：属性字段  value：属性对应的值位置，
     * * 如：$.name 表示从返回值取值，返回值对象的name属性值
     * * param.name 表示参数列表中取值，param参数的name属性值
     */
    public Map<String, String> getMethodLogConf(Method entityMethod) {
        String key = String.format("%s.%s-%s", entityMethod.getDeclaringClass().getName(), entityMethod.getName(), parameterTypeNames(entityMethod.getParameterTypes()));
        if (this.paramConf.containsKey(key)) {
            return this.paramConf.get(key).getConf();
        }
        return null;
    }

    @Data
    @Accessors(chain = true)
    static class LogHelperVo {
        private Class<?> entityClass;
        private Method method;
        private Map<String, String> conf;

        public LogHelperVo(Class<?> entityClass, Method method) {
            this.method = method;
            this.entityClass = entityClass;
        }
    }
}
