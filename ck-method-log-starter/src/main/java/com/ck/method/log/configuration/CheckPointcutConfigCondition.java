package com.ck.method.log.configuration;

import com.ck.method.log.aspect.MethodLogHandler;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.ObjectUtils;

public class CheckPointcutConfigCondition implements Condition {

    /**
     * @param conditionContext:判断条件能使用的上下文环境
     * @param annotatedTypeMetadata:注解所在位置的注释信息
     */
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        ConfigurableListableBeanFactory beanFactory = conditionContext.getBeanFactory();

        if (!ObjectUtils.isEmpty(beanFactory)) {
            MethodLogHandler methodLogHandler = beanFactory.getBean(MethodLogHandler.class);
            return !ObjectUtils.isEmpty(methodLogHandler)
                    && !ObjectUtils.isEmpty(methodLogHandler.getMethodLogConfig())
                    && !ObjectUtils.isEmpty(methodLogHandler.getMethodLogConfig().getExpressions());
        }
        return false;
    }
}