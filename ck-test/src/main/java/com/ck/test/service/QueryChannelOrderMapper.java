package com.ck.test.service;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

public interface QueryChannelOrderMapper extends BaseMapper<QueryChannelOrderMapper.QueryChannelOrder> {
    @Data
    @TableName(value = " op_channel_order oco left join op_channel_policy ocp on ocp.channel_order_no=oco.channel_order_no  ")
    class QueryChannelOrder implements Serializable {
        @TableField("oco.channel_order_no")
        private String channelOrderNo;
        @TableField("oco.order_type")
        private String orderType;
        @TableField("oco.channel_code")
        private String channelCode;
        @TableField("oco.out_channel_code")
        private String outChannelCode;
        @TableField("oco.contract_no")
        private String contractNo;
        @TableField("ocp.application_no")
        private String applicationNo;
        @TableField("ocp.policy_no")
        private String policyNo;
        @TableField("ocp.former_policy_no")
        private String formerPolicyNo;
        @TableField("ocp.goods_code")
        private String goodsCode;
        @TableField("ocp.plan_code")
        private String planCode;
        @TableField("oco.gmt_created")
        private Date gmtCreated;

    }
}