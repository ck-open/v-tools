package com.ck.test.kuai_lv;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Data
@Accessors(chain = true)
public class PolicyDto {
    /**
     * 渠道业务号，渠道唯一单号(幂等使用)     必须-Y
     */
    private String policyId;
    /**
     * 产品码（现代提供）     必须-Y
     */
    private String productCode;
    /**
     * 投保⽇期 yyyy-MM-dd HH:mm:ss     必须-Y
     */
    private String applyTime;
    /**
     * 保额(单位：元，保留2位小数)     必须-Y
     */
    private BigDecimal amount;
    /**
     * 保费费率     必须-Y
     */
    private BigDecimal premiumRate;
    /**
     * 保费     必须-Y
     */
    private BigDecimal premium;
    /**
     * 生效时间 yyyy-MM-dd HH:mm:ss     必须-Y
     */
    private String effectTime;
    /**
     * 保险结束日期 yyyy-MM-dd HH:mm:ss     必须-Y
     */
    private String endTime;
    /**
     * 保期-天     必须-Y
     */
    private Integer policyPeriod;
    /**
     * 投保校验编码（签约时由保司返回）     必须-Y
     */
    private String signingCode;
    /**
     * 账单日期，送达月的次月1号  yyyy-MM-dd     必须-Y
     */
    private String billDate;
    /**
     * 订单最晚下单时间  yyyy-MM-dd HH:mm:ss     必须-Y
     */
    private String lastOrderTime;
    /**
     * 投保人（个人类型）     必须-N
     */
    private PersonDto applyPersonal;
    /**
     * 被保险人 （个人类型）     必须-N
     */
    private PersonDto insuredPersonal;
    /**
     * 投保人（公司类型）     必须-N
     */
    private ComInfoDto applyCom;
    /**
     * 被保人(公司类型)     必须-N
     */
    private ComInfoDto insuredCom;
    /**
     * 标的物信息     必须-Y
     */
    private List<TargetDto> targetList;


    @Data
    @Accessors(chain = true)
    public static class PersonDto {
        /**
         * 姓名     必须-Y
         */
        private String name;
        /**
         * 证件类型 见数据字典     必须-Y
         */
        private String identifyType;
        /**
         * 证件号码     必须-Y
         */
        private String identifyNumber;
        /**
         * 地址     必须-Y
         */
        private String address;
        /**
         * 电话     必须-Y
         */
        private String phone;
        /**
         * 邮箱     必须-N
         */
        private String email;

        /**
         * 投保人id     必须-N
         */
        private String userId;
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @Accessors(chain = true)
    public static class ComInfoDto extends PersonDto {
        /**
         * 法人姓名     必须-Y
         */
        private String legalPersonName;
        /**
         * 证件类型 见数据字典     必须-Y
         */
        private String legalPersonIdentifyType;
        /**
         * 法人证件号     必须-Y
         */
        private String legalPersonIdentifyNumber;
        /**
         * 法人手机号     必须-Y
         */
        private String legalPersonPhone;
        /**
         * 法人邮箱     必须-Y
         */
        private String legalPersonEmail;
    }

    @Data
    @Accessors(chain = true)
    public static class TargetDto {
        /**
         * 订单号     必须-Y
         */
        private String orderNo;
        /**
         * 售后单号，有售后单时必传     必须-N
         */
        private String afterSaleNo;
        /**
         * 单据类型；订单1   售后2     必须-Y
         */
        private String orderType;
        /**
         * 单据金额     必须-Y
         */
        private String orderAmount;
        /**
         * 下单时间     必须-N
         */
        private String orderTime;
        /**
         * 送达时间     必须-N
         */
        private String deliveryTime;
        /**
         * 售后完成时间  yyyy-MM-dd HH:mm:ss     必须-N
         */
        private String afterSaleEndTime;
    }


    public static void main(String[] args) {
        PolicyDto policyDto = new PolicyDto()
                .setPolicyId("123456789")
                .setProductCode("123456789")
                .setApplyTime("2021-01-01 00:00:00")
                .setAmount(new BigDecimal("100.00"))
                .setPremiumRate(new BigDecimal("0.01"))
                .setPremium(new BigDecimal("1.00"))
                .setEffectTime("2021-01-01 00:00:00")
                .setEndTime("2021-01-01 00:00:00")
                .setSigningCode("123456789")
                .setBillDate("2021-01-01")
                .setLastOrderTime("2021-01-01 00:00:00")
                .setPolicyPeriod(1)
//                .setApplyPersonal(new PolicyDto.PersonDto().setName("张三").setIdentifyNumber("123456789").setIdentifyType("1").setAddress("北京").setPhone("123456789").setEmail("123456789@163.com"))
                .setApplyCom(new ComInfoDto().setLegalPersonName("张三").setLegalPersonIdentifyNumber("123456789").setLegalPersonIdentifyType("1").setLegalPersonPhone("123456789").setLegalPersonEmail("123456789@163.com"))
//                .setInsuredPersonal(new PolicyDto.PersonDto().setName("李四").setIdentifyNumber("123456789").setIdentifyType("1").setAddress("北京").setPhone("123456789").setEmail("123456789@163.com"))
                .setInsuredCom(new ComInfoDto().setLegalPersonName("李四").setLegalPersonIdentifyNumber("123456789").setLegalPersonIdentifyType("1").setLegalPersonPhone("123456789").setLegalPersonEmail("123456789@163.com"))
                .setTargetList(Collections.singletonList(new TargetDto().setOrderNo("123456789").setOrderType("1").setOrderAmount("123456789").setOrderTime("2020-01-01 00:00:00").setDeliveryTime("2020-01-01 00:00:00").setAfterSaleNo("123456789").setAfterSaleEndTime("2020-01-01 00:00:00")));
        policyDto.getApplyCom().setName("xxx有限公司").setIdentifyType("10").setIdentifyNumber("91310000MA1FL1FA73").setUserId("123456789").setAddress("北京").setPhone("123456789").setEmail("123456789@163.com");
        policyDto.getInsuredCom().setName("yyy有限公司").setIdentifyType("10").setIdentifyNumber("91310000MA1FL1FA73").setUserId("123456789").setAddress("北京").setPhone("123456789").setEmail("123456789@163.com");

        System.out.println(JSONObject.toJSONString(policyDto));

    }
}
