package com.ck.test.kuai_lv;


import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 风控模型对象
 */
@Data
@Accessors(chain = true)
public class RiskManager {
    /**
     * 唯一识别id，幂等字段     必须-Y
     */
    private String reqId;
    /**
     * 申请时间，yyyy-MM-dd HH:mm:ss     必须-Y
     */
    private String reqTime;
    /**
     * 客户名称     必须-Y
     */
    private String compName;
    /**
     * 客户证件类型     必须-Y
     */
    private String compCertType;
    /**
     * 客户证件号码     必须-Y
     */
    private String compCertNum;
    /**
     * 快驴客户名称     必须-Y
     */
    private String kuailvCustomerName;
    /**
     * 法人姓名，客户类型为企业必传     必须-N
     */
    private String legalPersonName;
    /**
     * 法人证件类型，客户类型为企业必传     必须-N
     */
    private String legalPersonCertType;
    /**
     * 法人证件号码，客户类型为企业必传     必须-N
     */
    private String legalPersonCertNum;
    /**
     * 法人手机号，客户类型为企业必传     必须-N
     */
    private String legalPersonMobile;
    /**
     * 快驴评估额度     必须-Y
     */
    private String kuailvEvalAmount;
    /**
     * 平台流水区间     必须-Y
     */
    private String platFlowRange;
    /**
     * 倒闭风险系数     必须-Y
     */
    private String failRiskCoeff;
    /**
     * 采购占比系数     必须-Y
     */
    private String purchaseRatioCoeff;
    /**
     * 入驻时长     必须-Y
     */
    private String merchantEntryDuration;
    /**
     * 餐饮业态     必须-Y
     */
    private String cateringBusinessType;
    /**
     * 行业排名     必须-Y
     */
    private String industryRank;


    public static void main(String[] args) {
        RiskManager riskManager = new RiskManager().setReqId("123")
                .setReqTime("2020-01-01 00:00:00")
                .setCompName("快驴")
                .setCompCertType("身份证")
                .setCompCertNum("123456789012345678")
                .setKuailvCustomerName("快驴")
                .setLegalPersonName("张三")
                .setLegalPersonCertType("身份证")
                .setLegalPersonCertNum("123456789012345678")
                .setLegalPersonMobile("12345678901")
                .setKuailvEvalAmount("100000")
                .setPlatFlowRange("100000-200000")
                .setFailRiskCoeff("0.1")
                .setPurchaseRatioCoeff("0.1")
                .setMerchantEntryDuration("1")
                .setCateringBusinessType("美食")
                .setIndustryRank("1");

        System.out.println(JSONObject.toJSONString(riskManager));
    }
}
