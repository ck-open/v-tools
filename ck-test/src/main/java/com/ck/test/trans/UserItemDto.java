package com.ck.test.trans;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class UserItemDto extends UserDto {

    /**
     * 获取分布式锁的key
     *
     * @return 锁的key
     */
    @Override
    public String getLockKey() {
        return getUserName();
    }
}
