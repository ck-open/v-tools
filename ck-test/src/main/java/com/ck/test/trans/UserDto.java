package com.ck.test.trans;

import com.ck.business.trans.entity.dto.IInDto;
import com.ck.business.trans.entity.dto.IOutDto;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDto implements IInDto, IOutDto {

    private String userName;
    private String password;
    private String phone;
    private String channelCode;
    private String channelOrderNo;
    private String channelOrderStatus;
    private String channelOrderType;
    private String channelOrderStatusDesc;

    /**
     * 获取分布式锁的key
     *
     * @return 锁的key
     */
    @Override
    public String getLockKey() {
        return this.userName;
    }
}
