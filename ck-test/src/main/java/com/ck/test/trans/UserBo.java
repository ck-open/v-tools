package com.ck.test.trans;

import com.ck.business.trans.entity.vo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class UserBo extends BaseBo {

    private String userName;
    private String password;
    private String phone;
    private String channelCode;
    private String channelOrderNo;
    private String channelOrderStatus;
    private String channelOrderType;
    private String channelOrderStatusDesc;

}
