package com.ck.test.trans;

import com.ck.tools.com.TResult;
import com.ck.business.trans.ITransConvert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserTransConvert implements ITransConvert<UserItemDto, TResult<UserDto>, UserItemBo> {
    /**
     * 是否支持
     *
     * @param userDto 输入数据对象
     * @return 是否支持
     */
    @Override
    public boolean isSupport(UserItemDto userDto) {
        return true;
    }

    /**
     * 转换业务对象
     *
     * @param userDto 输入数据对象
     * @return 转换后的业务对象
     */
    @Override
    public UserItemBo convertBo(UserItemDto userDto) {
        log.info("转换Bo完成");
        return new UserItemBo();
    }

    /**
     * 业务对象转换输出数据对象
     *
     * @param bo 业务对象
     * @return 转换后的业务对象
     */
    @Override
    public TResult<UserDto> convertOutDto(UserItemBo bo) {
        log.info("转换OutDto完成");
        return TResult.success(new UserItemDto());
    }

}
