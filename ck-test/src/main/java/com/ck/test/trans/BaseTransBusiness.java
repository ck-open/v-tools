package com.ck.test.trans;

import com.ck.tools.com.TResult;
import com.ck.business.trans.service.TransBusiness;
import org.springframework.stereotype.Service;

@Service
public class BaseTransBusiness extends TransBusiness<UserDto, TResult<UserDto>, UserBo> {

    /**
     * 输入数据转换业务对象
     *
     * @param userDto 输入数据Dto
     * @return 业务对象
     */
//    @Override
//    public UserItemBo convertBo(UserDto userDto) {
//        return SpringContextUtil.copyBean(userDto, UserItemBo.class);
//    }

    /**
     * 业务对象转换输出数据Dto
     *
     * @param bo    业务对象
     * @param inDto
     * @return 输出数据Dto
     */
//    @Override
//    public TResult<UserDto> convertOutDto(UserBo bo, UserDto inDto) {
//        return SpringContextUtil.copyBean(bo, new TypeReference<TResult<UserDto>>() {
//        });
//    }
}
