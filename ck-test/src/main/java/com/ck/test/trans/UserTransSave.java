package com.ck.test.trans;

import com.ck.tools.com.TResult;
import com.ck.business.trans.ITransSave;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserTransSave implements ITransSave<UserDto, TResult<UserDto>, UserBo> {
    /**
     * 是否支持
     *
     * @param userDto 输入数据对象
     * @return 是否支持
     */
    @Override
    public boolean isSupport(UserDto userDto) {
        return true;
    }

    /**
     * 输入数据对象保存
     *
     * @param userDto 输入数据对象
     * @return 保存成功返回true，否则返回false
     */
    @Override
    public boolean saveInDto(UserDto userDto) {
        log.info("保存输入数据对象成功");
        return true;
    }

    /**
     * 业务数据对象保存
     *
     * @param bo 业务对象
     * @return 保存成功返回true，否则返回false
     */
    @Override
    public boolean saveBo(UserBo bo) {
        log.info("保存业务数据对象成功");
        return true;
    }

    /**
     * 输出数据对象保存
     *
     * @param userDtoTResult 输出数据对象
     * @return 保存成功返回true，否则返回false
     */
    @Override
    public boolean saveOutDto(TResult<UserDto> userDtoTResult) {
        log.info("保存输出数据对象成功");
        return true;
    }
}
