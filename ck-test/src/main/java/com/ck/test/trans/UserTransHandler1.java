package com.ck.test.trans;

import com.ck.business.trans.ITransHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserTransHandler1 implements ITransHandler<UserBo> {
    /**
     * 是否支持此操作
     *
     * @param bo 业务对象
     * @return 支持则返回true，否则返回false
     */
    @Override
    public boolean isSupport(UserBo bo) {
        return true;
    }

    /**
     * 执行业务处理器
     *
     * @param bo 业务对象
     */
    @Override
    public void doTrans(UserBo bo) {
        log.info("执行业务处理器-{}", this.getClass().getSimpleName());
    }
}
