package com.ck.test.controller;

import com.ck.tools.com.TResult;
import com.ck.test.trans.BaseTransBusiness;
import com.ck.test.trans.UserDto;
import com.ck.test.trans.UserItemDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestBusinessApi {

    @Resource
    BaseTransBusiness baseTransBusiness;

    @PostMapping("testBusinessApi")
    public TResult<UserDto> test(UserItemDto body) {
        return baseTransBusiness.run(body);
    }

    @PostMapping("testFormData")
    public TResult<String> testFormData(@RequestParam(value = "test1",required = false) String test1,@RequestParam(value = "test2",required = false) String test2) {
        return TResult.build("0",test1+"----"+test2);
    }
}
