package com.ck.test;

import com.ck.excel.ExcelUtil;
import com.ck.excel.annotation.ExcelCell;
import com.ck.excel.annotation.ExcelTable;
import com.ck.excel.enums.ExcelTypeEnum;
import com.ck.tools.utils.WebUtil;
import lombok.Data;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        PolicyLock.redisDelBatchKey(Paths.get("C:\\Users\\cyk\\Desktop\\E20241108134753.csv"));


    }


    @ExcelTable
    @Data
    public static class PolicyLock {

        @ExcelCell(value = "key")
        private String key;

        /**
         * 批量删除Redis key
         * 查询sql
         * select operate_batch_no,third_policy_no,policy_no,concat('cn:com:hyundai:tiger:common:service:BatchTrans:lock:',policy_no)
         * from tiger_core_bill.tg_bill_batch_commissions where operate_batch_no='E20241104110830';
         *
         * @param path 文件路径
         */
        public static void redisDelBatchKey(Path path) {
            List<PolicyLock> list = ExcelUtil.read(ExcelTypeEnum.CSV, path,null,null, PolicyLock.class);
            list.forEach(policyLock -> System.out.println(policyLock.redisDelKey()));
        }

        public String redisDelKey() {
            if (this.key != null)
                return WebUtil.sendGET("https://tiger.hinsurance.cn/inner/tiger-api/redis/del", String.format("key=%s", this.key), res -> this.key + "  res:" + res);
            return null;
        }
    }
}


