package com.ck.test.jdPolicyEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 京东保险承保 API 数据模型
 */
public class JdPolicyDto {

    /**
     * 承保请求报文模型
     */
    @Data
    @Accessors(chain = true)
    public static class Req {
        /**
         * 公共头信息   是否必传：Y  公共请求参数
         */
        private Header header;
        /**
         * 业务数据   是否必传：Y  业务数据详见各接口明细
         */
        private ReqBody body;
    }

    /**
     * 承保请求报文模型
     */
    @Data
    @Accessors(chain = true)
    public static class Header {
        /**
         * 版本   是否必传：Y  版本号，文档版本
         * 响应时 与请求报文值一致
         */
        private String version;
        /**
         * 交易类型   是否必传：Y  见接口列表
         * 响应时 与请求报文值一致
         */
        private String requestType;
        /**
         * 请求标识   是否必传：Y  京东订单号，若其他接口有orderId字段，这俩值相同
         * 响应时 与请求报文值一致
         */
        private String uuid;
        /**
         * 保险公司编码   是否必传：Y  京东提供
         * 响应时 与请求报文值一致
         */
        private String comId;
        /**
         * 发送时间   是否必传：Y  时间戳
         * 仅请求时使用
         */
        private Long sendTime;

        /**
         * 返回时间   是否必传：Y  时间戳
         * 仅返回响应时使用
         */
        private Long responseTime;
        /**
         * 应答返回码   是否必传：Y  即接口连通性，不代表业务返回结果，公共头返回码
         * 仅返回响应时使用
         */
        private String responseCode;
        /**
         * 应答返回信息   是否必传：Y  网络失败、加解密失败描述（系统处理超时、加解密失败等场景），禁止返回纯业务逻辑错误描述，纯业务逻辑描述（例如：身份证、日期错误，投保人信息错误等场景）请在Body中返回。
         * 仅返回响应时使用
         */
        private String responseInfo;

    }

    /**
     * 承保请求报文模型
     */
    @Data
    @Accessors(chain = true)
    public static class ReqBody {
        /**
         * 订单总金额   是否必传：Y  以分为单位，所有保单保费的总金额。人民币
         */
        private Long orderTotalPrice;
        /**
         * 订单实收金额   是否必传：Y  以分为单位，所有保单保费的实收总金额。人民币
         */
        private Long orderTotalPayPrice;
        /**
         * 订单实收金额美元   是否必传：N  以元为单位，所有保单保费美元的实收总金额。美元
         */
        private String orderTotalPayPriceUSD;
        /**
         * 投保人信息   是否必传：Y  投保人信息
         */
        private HolderInfo holderInfo;
        /**
         * 保单列表   是否必传：Y  List<Policy>
         */
        private List<Policy> policyList;
        /**
         * 订单扩展信息   是否必传：N  订单扩展信息
         */
        private ExpandInfo expandInfo;
    }

    /**
     * 投保人信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class HolderInfo {
        /**
         * 投保人姓名/企业名称   是否必传：Y  投保人姓名/企业名称
         */
        private String holderName;
        /**
         * 投保人证件类型   是否必传：Y  投保人证件类型
         */
        private Integer holderCardType;
        /**
         * 投保人证件号   是否必传：Y  投保人证件号
         */
        private String holderCardNo;
        /**
         * 账户名称   是否必传：N  账户名称
         */
        private String accountName;
        /**
         * 投保人类型   是否必传：Y  1-个人 2-企业
         */
        private Integer holderType;
    }

    /**
     * 保单信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class Policy {
        /**
         * 产品方案   是否必传：Y  产品方案
         */
        private ProductPlan productScheme;
        /**
         * 被保人信息   是否必传：Y  List<InsuredInfo>
         */
        private List<InsuredInfo> insuredInfoList;
        /**
         * 标的列表   是否必传：Y  List<Target>
         */
        private List<Target> targetList;
        /**
         * 保单基本信息   是否必传：Y  保单基本信息
         */
        private PolicyInfo policyInfo;

    }

    /**
     * 产品方案信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class ProductPlan {
        /**
         * 责任列表   是否必传：Y  责任列表
         */
        private List<Risk> riskList;
        /**
         * 产品方案信息   是否必传：Y  产品方案信息
         */
        private ProductPlanInfo productSchemeInfo;
    }

    /**
     * 责任信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class Risk {
        /**
         * 责任编号   是否必传：Y  责任编号
         */
        private String riskCode;
        /**
         * 责任名称   是否必传：Y  String
         */
        private String riskName;
    }

    /**
     * 产品方案信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class ProductPlanInfo {
        /**
         * 方案保费   是否必传：Y  以分为单位
         */
        private Long schemePremium;
        /**
         * 保障期间类型   是否必传：Y  单位D:天；M:月；Y:年；A:岁; H:小时
         */
        private String insurancePeriodType;
        /**
         * 保障期间值   是否必传：Y  终身传值999保障期限类型为A
         */
        private String insurancePeriod;
        /**
         * 保单起期   是否必传：Y  yyyy-MM-dd HH:mm:ss 保单生效起始日期。
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyBeginDate;
        /**
         * 保单止期   是否必传：Y  yyyy-MM-dd HH:mm:ss 保单生效截止日期。
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyEndDate;
        /**
         * sku编码   是否必传：N  京品保必填
         */
        private String skuCode;
        /**
         * 三级险种编码   是否必传：N  京品保必填
         */
        private String classicThird;
        /**
         * 保司方案编号   是否必传：Y  保司方案编号
         */
        private String schemeCode;
        /**
         * 保司方案名称   是否必传：Y  保司方案名称
         */
        private String schemeName;
        /**
         * 产品编码   是否必传：Y  产品编码
         */
        private String productCode;
        /**
         * 产品名称   是否必传：Y  产品名称
         */
        private String productName;

    }

    /**
     * 被保险人信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class InsuredInfo {
        /**
         * 被保人姓名/企业名称   是否必传：Y  投保份数
         */
        private String insuredName;
        /**
         * 投保份数   是否必传：N  投保份数
         */
        private Integer insuredNum;
        /**
         * 被保人证件类型   是否必传：Y  证件类型
         */
        private Integer insuredCardType;
        /**
         * 被保人证件号   是否必传：Y  被保人证件号
         */
        private String insuredCardNo;
        /**
         * 账户名称   是否必传：Y  账户名称
         */
        private String accountName;
        /**
         * 受益方式   是否必传：Y  1法定、2投保人自己、3指定
         */
        private Integer benefitType;
        /**
         * 受益人列表   是否必传：N  benefitType 为1时List<benefitInfo>为空
         */
        private List<BenefitInfo> benefitInfoList;
        /**
         * 被保人类型   是否必传：Y  1-个人、2-企业
         */
        private Integer insuredType;
    }

    /**
     * 受益人信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class BenefitInfo {
        /**
         * 受益人名称   是否必传：N  京品保必填，值为JDpin
         */
        private String benefitName;
        /**
         * 会员等级   是否必传：N  0-普通用户，1-L1、2-L2、3-L3、4-L4、5-L5、6-L6
         */
        private Integer memberLevel;
    }

    /**
     * 标的信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class Target {
        /**
         * 标的物编码   是否必传：Y  标的物编码
         */
        private String targetCode;
        /**
         * 标的物名称   是否必传：Y  标的物名称
         */
        private String targetName;
        /**
         * 标的物数量   是否必传：Y  标的物数量
         */
        private Integer targetNum;
        /**
         * 标的物单价   是否必传：Y  以分为单位
         */
        private Long targetPrice;
        /**
         * 标的物类型   是否必传：Y  标的物类型
         */
        private String targetType;
        /**
         * 标的扩展信息   是否必传：N  Map
         */
        private ExpandInfo ext;
    }

    /**
     * 保单基本信息模型
     */
    @Data
    @Accessors(chain = true)
    public static class PolicyInfo {
        /**
         * 应收保费   是否必传：Y  以分为单位，人民币。 商家实付保费
         */
        private Long policyPayPrice;
        /**
         * 应收保费美元   是否必传：N  以元为单位，美元。商家实付保费
         */
        private String policyPayPriceUSD;
        /**
         * 被保人数   是否必传：Y  被保人人数，默认1
         */
        private Integer insuredNum;
        /**
         * 保额   是否必传：Y  以分为单位
         */
        private Long policyCoverage;
        /**
         * 保额美元   是否必传：N  以元为单位。美元
         */
        private BigDecimal policyCoverageUSD;
        /**
         * 保单生效时间   是否必传：Y  yyyy-MM-dd HH:mm:ss
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyEffectDate;
        /**
         * 保单创建时间   是否必传：Y  yyyy-MM-dd HH:mm:ss
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyCreateDate;
        /**
         * 投保时间   是否必传：Y  yyyy-MM-dd HH:mm:ss
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date insuredDate;
        /**
         * 赠险标识   是否必传：N  0-否1-是，默认0
         */
        private String giftInsuranceType;
        /**
         * 佣金率   是否必传：N  京品保必填
         */
        private String commissionRate;
        /**
         * 保单id   是否必传：Y  京东保单号，京东唯一标识
         */
        private String policyId;
        /**
         * 总保费   是否必传：Y  以分为单位，人民币。 活动优惠金额 + 商家实付保费
         */
        private Long policyPrice;
        /**
         * 保单基本扩展信息   是否必传：N  保单基本扩展信息
         */
        private ExpandInfo policyExtInfo;


    }

    /**
     * 扩展信息模型
     * 注意 京东方传递的是Map<String,Object>   为方便代码书写，这里使用对象接收
     */
    @Data
    @Accessors(chain = true)
    public static class ExpandInfo {
        /*------------------------以下是Body 请求报文扩展信息--------------------------*/
        /**
         * 重推类型   是否必传：N  1：保司数据丢失重推，2：承保失败重推
         */
        private String pushAgainType;

        /*------------------------以下是Target 标的扩展信息--------------------------*/
        /**
         * 商城订单号   是否必传：N  标的扩展信息ext中
         */
        private String shopOrderId;
        /**
         * 一级类目ID   是否必传：N  标的扩展信息ext中
         */
        private String firstCategoryId;
        /**
         * 二级类目ID   是否必传：N  标的扩展信息ext中
         */
        private String secondCategoryId;
        /**
         * 门店id   是否必传：N  门店id
         */
        private String storeId;
        /**
         * 门店名称   是否必传：N  门店名称
         */
        private String storeName;
        /**
         * 发货省ID   是否必传：N  标的扩展信息ext中
         */
        private String shipperProvince;
        /**
         * 收货省ID   是否必传：N  标的扩展信息ext中
         */
        private String consigneeProvince;
        /**
         * 标的物类目ID   是否必传：N  标的扩展信息ext中，三级类目ID
         */
        private String targetCategoryId;
        /**
         * 商家ID   是否必传：N  标的扩展信息ext中
         */
        private String venderId;
        /**
         * 销售费率   是否必传：N  京品保必填
         */
        private String saleRate;
        /**
         * 是否活动   是否必传：N  0-否1-是，默认0
         */
        private String isActivity;
        /**
         * 活动类型   是否必传：N  1-黄埔 是否活动选择“是”时必传
         */
        private String activityType;
        /**
         * 商品生产日期   是否必传：N  格式为yyyy-MM
         */
        private String produceDate;
        /**
         * 商品质保期   是否必传：N  格式为数字+(天/月/年)
         */
        private String wServe;


        /*------------------------以下是PolicyInfo 保单基本信息扩展信息--------------------------*/
        /**
         * 商家总实付金额   是否必传：Y  商家总实付金额 单位：分
         */
        private Long businessTotalPayAmount;
        /**
         * 关联保单号   是否必传：N  关联保单号
         */
        private String relatedPolicyNo;
        /**
         * 关联保单保费   是否必传：N  关联保单保费，单位：分
         */
        private Long relatedPolicyPrice;

    }


    /**
     * 承保请求响应报文模型
     */
    @Data
    @Accessors(chain = true)
    public static class Res {
        /**
         * 公共头信息   是否必传：Y  公共请求参数
         */
        private Header header;
        /**
         * 业务数据   是否必传：Y  业务数据详见各接口明细
         */
        private ResBody body;
    }

    /**
     * 承保请求响应报文模型
     */
    @Data
    @Accessors(chain = true)
    public static class ResBody {
        /**
         * orderTotalPrice   是否必传：Y  以分为单位
         */
        private Long orderTotalPrice;
        /**
         * 订单实收金额   是否必传：Y  以分为单位
         */
        private Long orderTotalPayPrice;
        /**
         * 保单列表   是否必传：Y  List<policyInfo>
         */
        private List<ResPolicyInfo> policyInfoList;
    }

    /**
     * 承保请求响应报文模型
     */
    @Data
    @Accessors(chain = true)
    public static class ResPolicyInfo {
        /**
         * 保单id   是否必传：Y  保单id
         */
        private String policyId;
        /**
         * 投保单号   是否必传：Y  投保单号。无核保承保时值为保司生成的保单号。
         */
        private String proposalNo;
        /**
         * 保费   是否必传：Y  以分为单位 活动优惠金额 + 商家实付保费
         */
        private Long policyPrice;
        /**
         * 保费实收   是否必传：Y  以分为单位 商家实付保费
         */
        private Long policyPayPrice;
        /**
         * 保单号   是否必传：Y  保险公司生成保险合同单号
         */
        private String policyNo;
        /**
         * 承保是否成功   是否必传：Y  true或false
         */
        private Boolean isSuccess;
        /**
         * 承保时间   是否必传：Y  保险公司承保时间 yyyy-MM-dd HH:mm:ss
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyDate;
        /**
         * 保单起期   是否必传：Y  yyyy-MM-dd HH:mm:ss 保单生效起始日期
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyBeginDate;
        /**
         * 保单止期   是否必传：Y  yyyy-MM-dd HH:mm:ss 保单生效截止日期
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date policyEndDate;
        /**
         * 承保失败原因   是否必传：N  当isSuccess为false时必返
         */
        private String failReason;
        /**
         * 承保失败返回码   是否必传：N  当isSuccess为false时必返
         */
        private String failCode;

    }
}
