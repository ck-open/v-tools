package com.ck.demo.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * JWT   依赖：java-jwt
 * Token 验证工具类
 *
 * @author cyk
 * @since 2020-01-01
 *
 */
public abstract class JwtUtil {

	/**
	 * 过期时间15分钟
	 */
	private static final long EXPIRE_TIME = 15 * 60 * 1000;
	/**
	 * 私钥
	 */
	private static final String TOKEN_SECRET_KEY = "f26e587c28064d0e85572c0a6a0e618";

	/**
	 * 生成签名，15min后过期
	 * 
	 * @param username
	 *            用户名
	 * @return 加密的token
	 */
	public static String getToken(String username) {
		// try {
		// 过期时间
		Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
		// 私钥及加密算法
		Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET_KEY);

		// 设置头部信息
		Map<String, Object> header = new HashMap<>(2); // initialCapacity 设置Hash Map 初始容量
		header.put("typ", "JWT"); // 设置token 令牌类型
		header.put("alq", "HS256"); // 设置散列算法
		// 附带username, userId信息,生成签名
		return JWT.create().withHeader(header) // 设置token头 jwt第一部分 头
				.withClaim("LoqinName", username) // 设置用户名 jwt第二部分 有效载荷
				// .withClaim("userId", userId) //设置用户Id jwt第二部分 有效载荷
				.withExpiresAt(date) // 设置过期时间 jwt第二部分 有效载荷
				.sign(algorithm); // 设置私钥
		// }catch (UnsupportedEncodingException e) {
		// return null;
		// }
	}

	/**
	 * 校验token是否正确
	 * 
	 * @param token
	 *            密钥
	 * @return 是否正确
	 **/
	public static boolean verifyToken(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET_KEY);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);

			// 获得token 中的头
			System.out.println(jwt.getHeader());
			// 获得token中绑定的用户名
			System.out.println(
					"用户名：" + jwt.getClaim("LoqinName").asString() + " \n用户ID：" + jwt.getClaim("userId").asString());

			return true;
		} catch (Exception exception) {
			return false;
		}
	}

	public static void main(String[] args) {
		// System.out.println(sign("abc", "1"));

//		System.out.println(verifyToken(
//				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImFscSI6IkhTMjU2In0.eyJleHAiOjE1NTAxMTEwNTcsIkxvcWluTmFtZSI6ImFiYyIsInVzZXJJZCI6IjEifQ.Otr6jFM3jD36SfQH8XPILiomGnMN2_o-HTAfn88vGIY"));
		
		System.out.println(UUID.randomUUID());
	}

}
