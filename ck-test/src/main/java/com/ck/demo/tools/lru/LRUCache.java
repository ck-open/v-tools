package com.ck.demo.tools.lru;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 一个LRU缓存，基于LinkedHashMap
 * <p>
 * 他的缓存具有固定的最大元素数量(cacheSize).
 * 如果缓存已满，并且添加了另一个条目，则删除LRU(最近最少的sed)条目。
 * <p>
 * 这个类是线程安全的。该类的所有方法都是同步的。
 *
 * @author cyk
 * @since 2020-01-01
 */
public class LRUCache<K, V> {

	private static final float hashTableLoadFactor = 0.75f;

	private LinkedHashMap<K, V> map;
	private int cacheSize;

	/**
	 * 创建一个新的LRU缓存。
	 * @param cacheSize  将保存在此缓存中的最大条目数。
	 */
	public LRUCache(int cacheSize) {
		//初始化最大缓存条目数
		this.cacheSize = cacheSize;
		//hashTable容量，缓存条目数/0.75+1取整
		int hashTableCapacity = (int) Math.ceil(cacheSize / hashTableLoadFactor) + 1;
		//实例化map，指定初始容量、加载因子、插入顺序（accessOrder）
		//accessOrder是一个boolean变量，它在初始化的时候为false，这时，就是默认的 【插入顺序】；如果为true，表示启用 【访问顺序】。而启用访问顺序只能使用一个特殊的构造方法：
		map = new LinkedHashMap<K, V>(hashTableCapacity, hashTableLoadFactor, true) {
			// 匿名内部类
			private static final long serialVersionUID = 1;
			/**
			 * 重写删除旧条目规则
			 * 如果当前对象的size大于类设定的最大条目数则返回true
			 */
			@Override
			protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
				return size() > LRUCache.this.cacheSize;
			}
		};
	}

	/**
	 * 从缓存中检索条目。
	 * 检索到的条目成为MRU(最近使用的)条目。
	 * 
	 * @param key  根据key获取对应的值
	 * @return 返回key对应的value，没有则返回null
	 */
	public synchronized V get(K key) {
		return map.get(key);
	}

	/**
	 * 向此缓存添加条目。新的条目成为MRU(最近使用的)条目。
	 * 如果具有指定键的项已经存在于缓存中，则由新项替换。
	 * 如果缓存已满，则从缓存中删除LRU(最近最少使用的)条目。
	 * 
	 * @param key  key值
	 * @param value value值
	 */
	public synchronized void put(K key, V value) {
		map.put(key, value);
	}

	/**
	 * 清空缓存
	 */
	public synchronized void clear() {
		map.clear();
	}

	/**
	 *返回缓存中使用的条目数。
	 * @return 当前缓存中的条目数。
	 */
	public synchronized int usedEntries() {
		return map.size();
	}

	/**
	 * 返回一个集合，包含所有缓存项的副本。
	 * @return 具有缓存内容副本的一个集合。
	 */
	public synchronized Collection<Map.Entry<K, V>> getAll() {
		return new ArrayList<Map.Entry<K, V>>(map.entrySet());

	} 

	// 测试 LRUCache 类
	public static void main(String[] args) {
		LRUCache<String, String> c = new LRUCache<String, String>(3);
		c.put("1", "one"); // 1
		c.put("2", "two"); // 2 1
		c.put("3", "three"); // 3 2 1
		c.put("4", "four"); // 4 3 2
		if (c.get("2") == null)
			throw new Error(); // 2 4 3
		c.put("5", "five"); // 5 2 4
		c.put("4", "second four"); // 4 5 2
		// Verify cache content.
		if (c.usedEntries() != 3)
			throw new Error();
		if (!c.get("4").equals("second four"))
			throw new Error();
		if (!c.get("5").equals("five"))
			throw new Error();
		if (!c.get("2").equals("two"))
			throw new Error();
		// List cache content.
		for (Map.Entry<String, String> e : c.getAll())
			System.out.println(e.getKey() + " : " + e.getValue());
	}
}
