//package com.ck.demo.tools.jdbc;
//
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//
///**
// * 数据库操作工具类
// *
// * @author cyk
// * @since 2020-11-06
// */
//public abstract class SQLExecute {
//
//    /**
//     * 数据新增
//     *
//     * @param tableName 表名
//     * @param po        数据对象实例
//     * @param dbManager 数据连接管理器
//     * @throws Exception
//     */
//    public static Integer insert(String tableName, Object po, Connection dbManager) {
//        int result = 0;
//        try {
//            result = insertBatch(tableName, Arrays.asList(po), dbManager);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                dbManager.rollback();
//                dbManager.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return result;
//
//    }
//
//    /**
//     * 数据新增
//     * <p>
//     * 支持事务的
//     * </p>
//     *
//     * @param tableName 表名
//     * @param pos       数据对象实例
//     * @param dbManager 数据连接管理器
//     * @throws Exception
//     */
//    public static Integer insertBatchTransaction(String tableName, List pos, Connection dbManager) {
//        int result = 0;
//        try {
//            result = insertBatch(tableName, pos, dbManager);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                dbManager.rollback();
//                dbManager.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return result;
//    }
//
//    /**
//     * 数据新增  批量
//     *
//     * @param tableName 表名
//     * @param pos       数据对象实例列表
//     * @param dbManager 数据连接管理器
//     */
//    public static Integer insertBatch(String tableName, List pos, Connection dbManager) {
//        int result = 0;
//        if (pos != null) {
//            List<String> insertSqlList = SQLUtils.getInsertBatchSql(tableName, pos);
//            for (int i = 0; i < insertSqlList.size(); i++) {
//                result = DBUtils.executeUpdate(dbManager, insertSqlList.get(i));
//            }
//        }
//
//        return result;
//    }
//
//
//    /**
//     * 数据更改
//     *
//     * @param tableName 表名
//     * @param po        数据对象实例
//     * @param where     条件
//     * @param dbManager 数据连接管理器
//     * @return
//     * @throws Exception
//     */
//    public static Integer updateTransaction(String tableName, Object po, String where, Connection dbManager) {
//        int result = 0;
//        try {
//            result = update(tableName, po, where, dbManager);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                DBUtils.rollBackTransaction(dbManager);
//                dbManager.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return result;
//    }
//
//    /**
//     * 数据更改
//     *
//     * @param tableName 表名
//     * @param po        数据对象实例
//     * @param where     条件
//     * @param dbManager 数据连接管理器
//     * @return
//     * @throws Exception
//     */
//    public static Integer update(String tableName, Object po, String where, Connection dbManager) throws Exception {
//        int result = 0;
//        if (tableName == null || po == null || where == null || dbManager == null || "".equalsIgnoreCase(where.trim())) {
//            return result;
//        }
//        String updateStr = SQLUtils.getUpdateSql(tableName, po, where);
//        result = DBUtils.executeUpdate(dbManager, updateStr);
//
//        return result;
//    }
//
//    /**
//     * 数据删除
//     *
//     * @param tableName 表名
//     * @param po        数据对象实例
//     * @param dbManager 数据连接管理器
//     * @return
//     */
//    public static Integer deleteTransaction(String tableName, Object po, Connection dbManager) {
//        int result = 0;
//        try {
//            result = delete(tableName, po, dbManager);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                DBUtils.rollBackTransaction(dbManager);
//                dbManager.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return result;
//    }
//
//    /**
//     * 数据删除
//     *
//     * @param tableName 表名
//     * @param where     条件
//     * @param dbManager 数据连接管理器
//     * @return
//     */
//    public static Integer deleteTransaction(String tableName, String where, Connection dbManager) {
//        int result = 0;
//        try {
//            result = delete(tableName, where, dbManager);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                DBUtils.rollBackTransaction(dbManager);
//                dbManager.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return result;
//    }
//
//
//    /**
//     * 数据删除
//     *
//     * @param tableName 表名
//     * @param po        数据对象实例
//     * @param dbManager 数据连接管理器
//     * @return
//     */
//    public static Integer delete(String tableName, Object po, Connection dbManager) throws Exception {
//        int result = 0;
//        if (tableName == null || po == null || dbManager == null) {
//            return result;
//        }
//        result = DBUtils.executeUpdate(dbManager, SQLUtils.getDeleteSql(tableName, po));
//
//        return result;
//    }
//
//    /**
//     * 数据更改
//     *
//     * @param tableName 表名
//     * @param where     条件
//     * @param dbManager 数据连接管理器
//     * @return
//     */
//    public static Integer delete(String tableName, String where, Connection dbManager) throws Exception {
//        int result = 0;
//        if (tableName == null || where == null || dbManager == null || "".equalsIgnoreCase(where.trim())) {
//            return result;
//        }
//
//        String updateStr = SQLUtils.getDelete(tableName,where);
//        result = DBUtils.executeUpdate(dbManager, updateStr);
//        return result;
//    }
//
//
//    /**
//     * select 查询
//     * 支持事务
//     *
//     * @param sql
//     * @param dbManager 数据源
//     * @param cla
//     * @param <T>
//     * @return
//     */
//    public static <T> List<T> selectTransaction(String sql, Connection dbManager, Class<T> cla) {
//        List<T> result = null;
//        try {
//            result = select(sql, dbManager, cla);
//        } catch (Exception e) {
//            e.printStackTrace();
//            try {
//                DBUtils.rollBackTransaction(dbManager);
//                dbManager.close();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        return result;
//    }
//
//    /**
//     * select 查询
//     * 支持事务
//     *
//     * @param sql
//     * @param dbManager 数据源
//     * @param cla
//     * @param <T>
//     * @return
//     */
//    public static <T> List<T> select(String sql, Connection dbManager, Class<T> cla) throws Exception {
//        List<T> result = null;
//
//        ResultSet resultSet = DBUtils.executeQuery(dbManager, sql);
//        result = SQLUtils.parseResultSet(resultSet, cla);
//
//        return result;
//    }
//
//    /**
//     * 分页查询
//     *
//     * @param sql
//     * @param pageNo
//     * @param pageSize
//     * @param dbManager
//     * @param cla
//     * @param <T>
//     * @return
//     * @throws Exception
//     */
//    public static <T> SQLUtils.PageResult<T> selectPage(String sql, Integer pageNo, Integer pageSize, Connection dbManager, Class<T> cla) throws Exception {
//        SQLUtils.PageResult<T> result = new SQLUtils.PageResult();
//        result.setPageNo(pageNo);
//        result.setPageSize(pageSize);
//        result.setPages(0);
//        result.setTotal(0);
//
//        String pageSql = null;
//        if (pageNo != null && pageSize != null) {
//            Integer startRow = (pageNo - 1) * pageSize;
//            Integer endRow = startRow + pageSize;
//            pageSql = "SELECT * from (SELECT a.*, ROWNUM rn from (" + sql + ") a where rownum <=" + endRow + ") where rn >" + startRow;
//        }
//        if (pageSql != null) {
//            result.setData(select(pageSql, dbManager, cla));
//        } else {
//            result.setData(select(sql, dbManager, cla));
//        }
//        if (result.getData() != null && pageSql != null) {
//            sql = "SELECT COUNT(*) AS total " + sql.substring(sql.toLowerCase().indexOf("from"));
//            List<Map> totalMaps = select(sql, dbManager, Map.class);
//            if (totalMaps != null && !totalMaps.isEmpty()) {
//                Map totalMap = totalMaps.get(0);
//                String fieldName = "total";
//                if (totalMap.get(fieldName) != null) {
//                    Integer total = Integer.valueOf(totalMap.get(fieldName).toString());
//                    if (total != null) {
//                        result.setTotal(total);
//                        int pages = total / pageSize;
//                        int page = total % pageSize == 0 ? 0 : 1;
//                        pages = pages + page;
//                        result.setPages(pages);
//                    }
//                }
//            }
//        }
//        return result;
//    }
//
//    public static void main(String[] args) {
//        double val = 5 / 2;
//        System.out.println(Math.ceil(val));
//    }
//}
