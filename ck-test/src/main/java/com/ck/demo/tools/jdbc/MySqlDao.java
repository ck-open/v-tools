//package com.ck.demo.tools.jdbc;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//public class MySqlDao {
//
//    private Connection conn=null;
//    private PreparedStatement ps=null;
//    private ResultSet rs=null;
//    /**
//     * 数据库查询数据
//     * @return
//     */
//    public List<Map<String,Object>> findAll(){
//        List<Map<String,Object>> result=new ArrayList<Map<String,Object>>();
//        try {
//            conn=DBUtils.getConn();
//            String sql="select * from user";
//            ps=conn.prepareStatement(sql);
//            rs=ps.executeQuery();
//            while(rs.next()) {
//                int id=rs.getInt("id");
//                String name=rs.getString("username");
//                String pwd=rs.getString("password");
//                // TODO 数据封装到结果
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            //将异常抛出，交给调用者来处理
//            throw new RuntimeException(e);
//        }finally {
//            DBUtils.close(conn, ps, rs);
//        }
//        return result;
//    }
//    /**
//     * 数据库插入数据
//     */
//    public void save() {
//        try {
//            conn=DBUtils.getConn();
//            String sql="insert into t_user values(null,?,?,?)";
//            ps=conn.prepareStatement(sql);
////            ps.setString(1, user.getUsername());
////            ps.setString(2, user.getPsw());
////            ps.setString(3, user.getEmail());
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            // 记日志
//            throw new RuntimeException(e);
//            //将异常抛出
//        }finally {
//            DBUtils.close(conn, ps, null);
//        }
//    }
//    /**
//     * 数据库删除数据
//     */
//    public void delete(int id) {
//        try {
//            conn=DBUtils.getConn();
//            ps=conn.prepareStatement("delete from t_user where id=?");
//            ps.setInt(1, id);
//            ps.executeUpdate();
//        } catch (SQLException e) {
//            //记录日志
//            throw new RuntimeException(e);
//        }finally {
//            DBUtils.close(conn, ps, null);
//        }
//    }
//    /**
//     * 登陆查询
//     */
//    public int Login(String name,String pwd) {
//        int x=0;
//        try {
//            conn=DBUtils.getConn();
//            ps=conn.prepareStatement("select count(*) from t_user where username=? and password=?");
//            ps.setString(1, name);
//            ps.setString(2, pwd);
//            rs=ps.executeQuery();
//            while(rs.next()) {x=rs.getInt(1);}
//        } catch (SQLException e) {
//            // 记日志
//            throw new RuntimeException(e);
//        }finally {
//            DBUtils.close(conn, ps, rs);
//        }
//        return x;
//    }
//}
