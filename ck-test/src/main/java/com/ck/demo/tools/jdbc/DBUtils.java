//package com.ck.demo.tools.jdbc;
//
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.sql.*;
//import java.util.Properties;
//
///**
// * 数据库连接池工具类<br>
// *     需要导包：
// *         <!-- 数据库连接池 Apache-Commons-DBCP -->
// *         <dependency>
// *             <groupId>commons-dbcp</groupId>
// *             <artifactId>commons-dbcp</artifactId>
// *             <version>1.4</version>
// *         </dependency>
// *         <!-- 对象池 Apache-Commons-Pool -->
// *         <dependency>
// *             <groupId>commons-pool</groupId>
// *             <artifactId>commons-pool</artifactId>
// *             <version>1.6</version>
// *         </dependency>
// */
//public class DBUtils {
//	public static BasicDataSource dataSource;
//	static {
//		dataSource=new BasicDataSource();
//		//设置数据里连接参数：驱动、数据库连接、用户名、密码
//		dataSource.setDriverClassName(prop("Driver")); // com.mysql.jdbc.Driver
//		dataSource.setUrl(prop("Url"));   // jdbc:mysql://127.0.0.1:3306/system-admin?serverTimezone=GMT%2B8&autoReconnect=true&socketTimeout=30000&useUnicode=true&characterEncoding=UTF-8&useSSL=false
//		dataSource.setUsername(prop("Username"));  // root
//		dataSource.setPassword(prop("Password"));  // ？
//		//设置初始连接数量和最大连接数量
//		dataSource.setInitialSize(Integer.parseInt(prop("InitialSize")));
//		dataSource.setMaxActive(Integer.parseInt(prop("MaxActive")));
//	}
//
//	/**
//	 * 读取数据库属性文件
//	 * @param key
//	 * @return
//	 */
//	public static String prop(String key) {
//				Properties prop=new Properties(); //创建属性对象
//				//得到文件的输入流,通过反射的方式获得  通过类加载器获得文件的输入流
//				InputStream ips=DBUtils.class.getClassLoader().getResourceAsStream("jdbc.properties");
//				try {
//					prop.load(ips); //把文件加载到属性对象中
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//				return prop.getProperty(key); //获取属性配置文件中的数据
//	}
//
//	/**
//	 * 获取数据库连接
//	 * @return
//	 * @throws SQLException
//	 */
//	public static Connection getConn() throws SQLException {
//		//获取数据库连接并返回
//		return dataSource.getConnection();
//	}
//
//	/**
//	 * 关闭连接
//	 * @param conn
//	 * @param stat
//	 * @param rs
//	 */
//	public static void close(Connection conn, Statement stat, ResultSet rs) {
//
//		try {
//			if (rs != null) {
//				rs.close();
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		try {
//			if (stat != null) {
//				stat.close();
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		try {
//			if (conn != null) {
//				conn.setAutoCommit(true); //连接归还前将自动提交打开，保证每个连接都是自动提交的
//				conn.close();
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//
//	/**
//	 * 开始事务
//	 * @param cnn
//	 */
//	public static void beginTransaction(Connection cnn){
//		if(cnn!=null){
//			try {
//				if(cnn.getAutoCommit()){
//					cnn.setAutoCommit(false);
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	/**
//	 * 提交事务
//	 * @param cnn
//	 */
//	public static void commitTransaction(Connection cnn){
//		if(cnn!=null){
//			try {
//				if(!cnn.getAutoCommit()){
//					cnn.commit();
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	/**
//	 * 回滚事务
//	 * @param cnn
//	 */
//	public static void rollBackTransaction(Connection cnn){
//		if(cnn!=null){
//			try {
//				if(!cnn.getAutoCommit()){
//					cnn.rollback();
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//
//	/**
//	 * 执行Update 语句并返回影响条数
//	 *
//	 * @param con 链接对象
//	 * @param sql 需要执行的SQL
//	 * @return
//	 */
//	public static int executeUpdate(Connection con, String sql) {
//		Integer result = null;
//		try {
//			//用于增删改操作  返回执行影响数据行数
//			result = execute(con, sql).executeUpdate();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	/**
//	 * 执行SQL
//	 *
//	 * @param con 链接对象
//	 * @param sql 需要执行的SQL
//	 * @return
//	 */
//	public static ResultSet executeQuery(Connection con, String sql) {
//		ResultSet result = null;
//		try {
//			//获取查询结果集
//			result = execute(con, sql).executeQuery();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
//
//	/**
//	 * 获取SQL 执行对象
//	 *
//	 * @param con 链接对象
//	 * @param sql 需要执行的SQL
//	 * @return
//	 */
//	public static PreparedStatement execute(Connection con, String sql) {
//		PreparedStatement ps = null;
//		try {
//			//创建prepareStatement对象，用于执行SQL
//			ps = con.prepareStatement(sql);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return ps;
//	}
//}