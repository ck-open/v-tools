package com.ck.demo.tools.util;

import com.alibaba.fastjson.annotation.JSONField;
import com.ck.tools.reflect.ClassUtils;
import com.ck.tools.reflect.JsonUtils;
import com.ck.tools.reflect.LambdaUtils;
import com.ck.tools.reflect.serializable.SFunction;
import com.ck.tools.utils.TimeUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BeanUtils {
    private static final Logger log = Logger.getLogger(BeanUtils.class.getName());


    /**
     * 对象深度Copy
     *
     * @param source 原数据对象
     * @param clazz  结果类型
     * @param <T>
     * @return
     */
    @SafeVarargs
    public static <T> T copyBean(Object source, Class<T> clazz, SFunction<T, ?>... passField) {
        return copyBean(source, clazz, false, passField);
    }

    /**
     * 对象深度Copy
     *
     * @param source 原数据对象
     * @param clazz  结果类型
     * @param setIc  是否忽略字段大小写
     * @param <T>
     * @return
     */
    @SafeVarargs
    public static <T> T copyBean(Object source, Class<T> clazz, boolean setIc, SFunction<T, ?>... passField) {
        Set<String> passFieldNames = passField == null ? new HashSet<>() :
                Stream.of(passField).map(LambdaUtils::fieldName).collect(Collectors.toSet());
        return copyBean(source, clazz, setIc, passFieldNames);
    }

    /**
     * 对象深度Copy
     *
     * @param source 原数据对象
     * @param clazz  结果类型
     * @param setIc  是否忽略字段大小写
     * @param <T> 返回值类型
     * @return 新对象
     */
    public static <T> T copyBean(Object source, Class<T> clazz, boolean setIc, Set<String> passField) {
        if (source == null || clazz == null || (Collection.class.isAssignableFrom(clazz) && !Collection.class.isAssignableFrom(source.getClass()))) {
            return null;
        }

        // 基本类型
        if (ClassUtils.isBasicType(clazz)) {
            if (!ClassUtils.isBasicType(source.getClass())) {
                log.warning("原对象：" + source.getClass().getName() + "非基本类型，无法转换为：" + clazz.getName());
                return null;
            }
            return (T) ClassUtils.parseValueType(clazz, source);
        }

        if (clazz.getName().contains(Object.class.getName())) {
            clazz = (Class<T>) Map.class;
        }

        T result = ClassUtils.newInstance(clazz);

        if (Collection.class.isAssignableFrom(clazz)) {
            // 数组
            for (Object c : (Collection) source) {
                Class<?> clazzTemp = ClassUtils.getGenerics(clazz, 0);
                clazzTemp = clazzTemp == null ? Map.class : clazzTemp;
                ((Collection<Object>) result).add(copyBean(c, clazzTemp, setIc, passField));
            }
        } else if (Map.class.isAssignableFrom(source.getClass()) && Map.class.isAssignableFrom(clazz)) {
            // 原数据与目标对象都是Map
            Map<Object, Object> mapResult = (Map) result;
            Map<Object, Object> mapSource = (Map) source;
            mapSource.forEach((k, v) -> {
                if (!passField.contains(k.toString())) {
                    mapResult.put(k, copyBean(v, v == null ? null : v.getClass(), setIc, passField));
                }
            });
        } else if (!Map.class.isAssignableFrom(clazz)) {
            // 目标对象类型不是Map
            Map<String, Field> fieldMap = new HashMap<>();
            ClassUtils.getFields(clazz).forEach(i -> fieldMap.put(setIc ? i.getName().toLowerCase() : i.getName(), i));

            if (Map.class.isAssignableFrom(source.getClass())) {
                // Map转对象
                ((Map<Object, Object>) source).forEach((k, v) -> {
                    if (!passField.contains(k.toString())) {
                        Field field = fieldMap.get(setIc ? k.toString().toLowerCase() : k.toString());
                        if (field != null) {
                            ClassUtils.setValue(result, field, copyBeanCollection(setIc, field, v, passField));
                        }
                    }
                });
            } else {
                // 对象转对象
                Set<Field> fields = ClassUtils.getFields(source.getClass());
                for (Field fieldSource : fields) {
                    String fieldName = setIc ? fieldSource.getName().toLowerCase() : fieldSource.getName();
                    if (fieldMap.containsKey(fieldName)) {
                        Field field = fieldMap.get(fieldName);
                        if (field != null && !passField.contains(field.getName())) {
                            Object val = ClassUtils.getValue(fieldSource, source);
                            ClassUtils.setValue(result, field, copyBeanCollection(setIc, field, val, passField));
                        }
                    }
                }
            }
        } else if (Map.class.isAssignableFrom(clazz)) {
            // 对象转Map
            Map<Object, Object> mapResult = (Map) result;
            Set<Field> fields = ClassUtils.getFields(source.getClass());
            for (Field field : fields) {
                if (!passField.contains(field.getName())) {
                    if (ClassUtils.isBasicType(field.getType())) {
                        mapResult.put(field.getName(), ClassUtils.getValue(field, source));
                    } else {
                        mapResult.put(field.getName(), copyBeanCollection(setIc, field, ClassUtils.getValue(field, source), passField));
                    }
                }
            }
        }
        return result;
    }

    /**
     * copyBean是集合支持
     *
     * @param setIc
     * @param field
     * @param val
     */
    private static Object copyBeanCollection(boolean setIc, Field field, Object val, Set<String> passField) {
        if (val != null && Collection.class.isAssignableFrom(field.getType())) {
            Collection<Object> collection = (Collection) ClassUtils.newInstance(field.getType());
            if (Collection.class.isAssignableFrom(val.getClass())) {
                Collection<Object> valCollection = (Collection<Object>) val;

                Class<?> c = ClassUtils.getGenerics(field, 0);

                Type[] types = ClassUtils.getGenerics(field);
                Type type = types[0];
                String typeClassName = type.getTypeName();
                int listLayerNum = 0;
                Class<?> typeClass = null;
                if (typeClassName.contains("<")) {
                    typeClass = ClassUtils.getClassForName(typeClassName.substring(0, typeClassName.indexOf("<")));
                    if (Collection.class.isAssignableFrom(typeClass)) {
                        c = ClassUtils.getClassForName(typeClassName.substring(typeClassName.lastIndexOf("<") + 1, typeClassName.indexOf(">")));
                    }
                    // 多层嵌套集合
                    for (int i = 0; i < typeClassName.length(); i++) {
                        if ('<' == typeClassName.charAt(i)) {
                            listLayerNum++;
                        }
                    }
                }

                Class<?> finalC = c == null ? Map.class : c;
                collection = copyBeanCollectionLayer(valCollection, finalC, listLayerNum, setIc, passField);
            }
            return collection;
        } else {
            return copyBean(val, field.getType(), setIc, passField);
        }
    }

    /**
     * copyBean时集合支持
     *
     * @param val
     * @param entity
     * @param listLayerNum
     * @param setIc
     * @param passField
     * @return
     */
    private static Collection<Object> copyBeanCollectionLayer(Collection<Object> val, Class<?> entity, Integer listLayerNum, boolean setIc, Set<String> passField) {
        Collection<Object> result = ClassUtils.newInstance(Collection.class);
        for (Object ov : val) {
            if (Collection.class.isAssignableFrom(ov.getClass()) && listLayerNum > 0) {
                Collection<?> temp = copyBeanCollectionLayer((Collection<Object>) ov, entity, listLayerNum, setIc, passField);
                result.add(temp);
            } else {
                listLayerNum--;
                Object t = copyBean(ov, entity, setIc, passField);
                result.add(t);
            }
        }
        return result;
    }


    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        Class cla = list.getClass();
        Type type = cla.getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] types = parameterizedType.getActualTypeArguments();
            for (Type i : types) {
                System.out.println(i.getTypeName());
            }
        }


        TestVo vo = new TestVo().setName("Test001").setAge(25).setSex(1).setMoney(56.2)
                .setItemVo(new TestItemVo().setP1("001").setP2("010").setP3(BigDecimal.ZERO).setP4(TimeUtil.parseTime("2023-08-23"))
                ).setItemVos(new ArrayList<>());
        vo.getItemVos().add(new TestItemVo().setP1("201").setP2("210").setP3(BigDecimal.ONE).setP4(TimeUtil.parseTime("2023-08-28")));
        vo.getItemVos().add(new TestItemVo().setP1("301").setP2("310").setP3(BigDecimal.ONE).setP4(TimeUtil.parseTime("2023-08-23")));
        vo.getItemVos().add(new TestItemVo().setP1("401").setP2("410").setP3(BigDecimal.ONE).setP4(TimeUtil.parseTime("2023-08-24")));
        vo.setItemVoList(new ArrayList<>());
        vo.getItemVoList().add(vo.getItemVos());
        vo.getItemVoList().add(vo.getItemVos());
        vo.getItemVoList().add(vo.getItemVos());

        vo.setItemVoArr(vo.itemVos.toArray(new TestItemVo[vo.getItemVos().size()]));
        System.out.println(JsonUtils.toJsonStr(vo));
        Map map = copyBean(vo, HashMap.class, false);

        TestVo vo1 = copyBean(map, TestVo.class, true);

        TestVo vo2 = copyBean(vo1, TestVo.class, false);

        vo2.setAge(69).setMoney(85.2).getItemVo().setP3(BigDecimal.ONE);

        System.out.println(JsonUtils.toJsonStr(map));
        System.out.println(JsonUtils.toJsonStr(vo1, null, true));
        System.out.println(JsonUtils.toJsonStr(map, "HH:mm:ss", true));
        System.out.println(JsonUtils.toJsonStr(new ArrayList<>(), true));
        System.out.println(JsonUtils.toJsonStr(new HashMap<>(), true));
    }

    @Data
    @Accessors(chain = true)
    public static class TestVo {
        private String name;
        private Integer age;
        private int sex;
        private Double money;
        private TestItemVo itemVo;
        private List<TestItemVo> itemVos;
        private TestItemVo[] itemVoArr;
        private List<List<TestItemVo>> itemVoList;
    }

    @Data
    @Accessors(chain = true)
    public static class TestItemVo {
        private String p1;
        private String p2;
        private BigDecimal p3;
        @JSONField(name = "time",format = "yyyy-MM-dd HH:mm")
        private Date p4;
    }
}
