package com.ck.demo.tools.trend;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        LinearRegression.test();
    }


    /**
     * Linear Regression线性回归
     * <p>
     * 　　1）预测结果是介于0和1之间的概率
     * <p>
     * 　　2）可以适用于连续性和类别性自变量
     * <p>
     * 线性回归公式：y = b + w*x，w表示权重b表示偏置。
     * 在实际实现中可以将公式写作：y = w[0] * x[0] + w[1] * x[1]，x[0]=1，这样就可以很方便的进行参数求解，同样稍作修改将公式写成：y = w[0] * x[0] + w[1] * x[1] + ... + w[n]*x[n]，就变成了多元回归。
     * 采用梯度下降和多次迭代不断优化参数，梯度下降计算参数的梯度，计算流程分为以下几步：
     * <p>
     * 1、根据当前参数和训练计算数据预测值
     * preY = sum(w[n] + x[n])
     * <p>
     * 2、计算梯度
     * wright_gradient[n] = sum(2 * (preY - y) * x[n] / N)，N为训练数据总行数
     * <p>
     * 3、更新参数：
     * wright[n] = wright[n] - a * wright_gradient[n]，a为学习率，学习率取值范围[0,1]，根据训练数据和训练情况来定。
     * <p>
     * 4、迭代
     * 每迭代一次就多整个训练数据计算一次梯度和更新一次参数，通过迭代使函数不断逼近最小误差
     */
    public static class LinearRegression {

        public static void test() {
            //            double[] weights = train(Arrays.asList(new double[]{4,4,4,4}), 0.001, 1000);

            List<double[]> trainData = new ArrayList<>(Arrays.asList(
                    new double[]{1}
                    , new double[]{31}
                    , new double[]{6}
                    , new double[]{26}
                    , new double[]{25}
                    , new double[]{8}
                    , new double[]{15}
                    , new double[]{14}
                    , new double[]{19}
                    , new double[]{5}
                    , new double[]{10}
            ));

            double total = 0;
            for (double[] v : trainData){
                total+=v[0];
            }
            System.out.println(String.format("总值：%s  个数：%s  均值：%s",total,trainData.size(),total/trainData.size()));

            for (int x = 0; x < 50; x++) {
                double[] weights = train(trainData, 0.001, 10000);
                trainData.add(weights);
                for (double i : weights) {
                    System.out.print(i + ",");
                }
                System.out.println();
            }
        }


        /**
         * 线性回归 训练
         *
         * @param trainData    数据集 [[x,x...],[x,x...]...]
         * @param learningRate 机器学习率  取值范围[0,1]
         * @param iterationNum 迭代次数  通过迭代使函数不断逼近最小误差
         * @return [y, y...]
         */
        public static double[] train(List<double[]> trainData, double learningRate, int iterationNum) {
            return updateWeights(trainData, learningRate, iterationNum, trainData.iterator().next().length);
        }

        /**
         * 更新权重
         *
         * @param trainData    数据集 [[x,x...],[x,x...]...]
         * @param learningRate 机器学习率   取值范围[0,1]
         * @param iterationNum 迭代次数  通过迭代使函数不断逼近最小误差
         * @param weightsNum   权重数量
         * @return [y, y...]
         */
        private static double[] updateWeights(List<double[]> trainData, double learningRate, int iterationNum, int weightsNum) {
            double[] weights = new double[weightsNum];
            for (int i = 0; i < iterationNum; i++) {
                double[] weights_gradient = new double[weights.length];
                for (int j = 0; j < trainData.size(); j++) {
                    double[] line = trainData.get(j);
                    double[] x = new double[line.length];
                    x[0] = 1;

                    double y = line[line.length - 1];

                    for (int n = 1; n < x.length; n++) {
                        x[n] = line[n - 1];
                    }

                    // 根据当前参数和数据 预测 preY
                    double preY = 0.0;
                    for (int n = 0; n < weights.length; n++) {
                        preY += x[n] * weights[n];
                    }
                    for (int n = 0; n < weights.length; n++) {
                        weights_gradient[n] += 2 * (preY - y) * x[n] / trainData.size();
                    }
                }
                // 更新参数
                for (int j = 0; j < weights.length; j++) {
                    weights[j] = weights[j] - learningRate * weights_gradient[j];
                }

                // 每迭代100次 输出Loss
                if (i % 100 == 0) {
                    double loss = computeError(trainData, weights);
//                    System.out.println(loss);
                }
            }
            return weights;
        }

        /**
         * 计算 Error
         *
         * @param trainData 数据集
         * @param weights   权重
         * @return
         */
        private static double computeError(List<double[]> trainData, double[] weights) {
            double error = 0.0;
            for (int i = 0; i < trainData.size(); i++) {
                double[] line = trainData.get(i);

                double preY = 0.0;
                double[] x = new double[line.length];
                x[0] = 1;
                double y = line[line.length - 1];
                for (int n = 1; n < x.length; n++) {
                    x[n] = line[n - 1];
                }
                for (int j = 0; j < line.length; j++) {
                    preY += weights[j] * x[j];
                }
                error += (y - preY) * (y - preY);
            }
            return error / trainData.size();
        }
    }
}
