package com.ck.demo.tools.obj;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author cyk
 * @since 2020-01-01
 */
public class ObjUtil {

	/**
	 * 根据对象类型创建对象
	 * @return
	 * @throws RuntimeException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static <T> T createBean() throws RuntimeException {
		try {
			Object o = new Object();
			T t =  (T)o.getClass().getConstructor().newInstance();
			return t;
		}catch (Exception e){
			throw new RuntimeException("对象创建失败！");
		}
	}

	/**
	 * 根据对象类型创建对象
	 * @return
	 * @throws RuntimeException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public static Object createBean(Class cla) throws RuntimeException {
		try {
			return cla.getClass().newInstance();
		}catch (Exception e){
			throw new RuntimeException("对象创建失败！");
		}
	}

	/**
	 * 将json解析成给定的对象集合<br>
	 * key为sheet工作表的第一行<br>
	 * 如果第一行不是表头或有合并单元格，则该方法不适用。
	 *
	 * @param obj
	 *            要封装数据的对象
	 * @param propertyName
	 *            对象属性名和数据Key映射
	 * @param val
	 *            数据json对象
	 * @throws RuntimeException
	 * @throws Exception
	 */
	public static void setObjPropertyVal(Object obj, Map propertyName, Map val) throws RuntimeException {
		List<String> objPropertyNames = getField(obj);

		// 遍历对象的所有属性并进行对应值的封装
		for (String key : objPropertyNames) {
			setFieldValueByName(key, obj, val.get(propertyName.get(key)==null?null:propertyName.get(key).toString()));
		}
	}

	/**
	 * 获得对象属性名称
	 * 
	 * @param object
	 */
	public static List<String> getField(Object object) {
		List<String> result = new ArrayList<>();
		// 获得类
		@SuppressWarnings("rawtypes")
		Class clazz = object.getClass();
		// 获取实体类的所有属性信息，返回Field数组
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			// 获得属性名称
			Integer index = field.toString().lastIndexOf(".");
			String name = field.toString().substring(index + 1);
			result.add(name);
		}
		return result;
	}

	/**
	 * 
	 * 根据属性名获取属性值
	 * 
	 * @param fieldName
	 *            属性名称
	 * @param o
	 *            对象
	 * @return
	 * @throws RuntimeException
	 */
	public static Object getFieldValueByName(String fieldName, Object o) throws RuntimeException {

		List<String> list = getField(o);
		if (!list.contains(fieldName))
			return null;

		try {
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "get" + firstLetter + fieldName.substring(1);
			Method method = o.getClass().getMethod(getter, new Class[] {});
			Object value = method.invoke(o, new Object[] {});
			return value;
		} catch (Exception e) {
			throw new RuntimeException("对象属性值获取失败！ ==>  属性名：" + fieldName + "  错误描述：" + e.getMessage());
		}
	}

	/**
	 * 
	 * 根据属性名设置属性值
	 * 
	 * @param fieldName
	 *            属性名称
	 * @param obj
	 *            要封装的对象
	 * @param value
	 *            要封装的值
	 * @return
	 * @throws RuntimeException
	 * @throws Exception
	 *             数据值类型不匹配时将抛出异常
	 */
	public static Object setFieldValueByName(String fieldName, Object obj, Object value) throws RuntimeException {

		Object val = getObjType(obj, fieldName, value);
		if (val == null)
			return obj;

		try {
			// 得到该属性的set方法名
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "set" + firstLetter + fieldName.substring(1);
			// 得到该属性的set方法
			Method method = obj.getClass().getMethod(getter, new Class[] { val.getClass() });
			method.invoke(obj, new Object[] { val });
			return obj;
		} catch (Exception e) {
			throw new RuntimeException("对象属性值注入失败！ ==> 属性名：" + fieldName + "  属性值：" + value + "  错误描述：" + e.getMessage());
		}
	}

	/**
	 * 判断对象属性与值类型是否一致
	 * 
	 * @param obj
	 * @param propertyName
	 *            属性名
	 * @param val
	 * @return
	 * @throws RuntimeException
	 *             数据值类型不匹配时将抛出异常
	 */
	public static Object getObjType(Object obj, String propertyName, Object val) throws RuntimeException {
		if (obj == null || val == null)
			return null;
		String objType = null;
		String valType = null;
		try {
			// 根据属性名获得属性类型名
			objType = obj.getClass().getDeclaredField(propertyName).getType().toString();
			if (objType.startsWith("class ")) {
				objType = objType.replaceAll("class ", "");
			}
			// 获得值对象类型名
			valType = val.getClass().getTypeName();

			if ("java.lang.Integer".equals(objType) && "java.lang.Double".equals(valType)) {
				return ((Number) val).intValue();
			} else if ("java.lang.Integer".equals(valType) && "java.lang.Double".equals(objType)) {
				return ((Number) val).doubleValue();
			} else if ("java.lang.Integer".equals(objType) && "java.lang.String".equals(valType)) {
				String v = String.valueOf(val).replaceAll("\\D*", "");
				return "".equals(v) ? null : Integer.parseInt(v);
			} else if ("java.lang.Double".equals(objType) && "java.math.BigDecimal".equals(valType)) {
				return ((BigDecimal) val).doubleValue();
			} else if ("java.lang.Double".equals(valType) && "java.math.BigDecimal".equals(objType)) {
				return new BigDecimal((Double) val);
			} else if ("java.lang.Double".equals(objType) && "java.lang.String".equals(valType)) {
				String v = String.valueOf(val);
				if (v.indexOf(".") != -1) {
					String v1 = v.substring(0, v.lastIndexOf("."));
					String v2 = v.substring(v.lastIndexOf("."));
					v1 = v1.replaceAll("\\D*", "");
					v2 = v2.replaceAll("\\D*", "");
					v = v1 + "." + v2;
				} else {
					v = v.replaceAll("\\D*", "");
				}
				return "".equals(v) ? null : Double.parseDouble(v);
			} else if ("java.util.Date".equals(objType) && "java.lang.Long".equals(valType)) {
				return new Date((long) val);
			} else if ("java.util.Date".equals(objType) && "java.lang.String".equals(valType)) {
				return parseStrToDate(String.valueOf(val));
			} else if (objType.equals(valType)) {
				return val;
			}

			throw new RuntimeException(
					"对象属性与值类型匹配失败！ --> 对象属性类型：" + objType + "   与值类型：" + valType + "  参数值：" + val + "  不匹配且无法转换！");
		} catch (Exception e) {
			throw new RuntimeException("对象属性与值类型匹配失败！ --> 对象属性类型：" + objType + "   与值类型：" + valType + "  参数值：" + val
					+ "  错误描述：" + e.getMessage());
		}
	}

	/**
	 * 尝试解析字符串为Date
	 * @param str
	 * @return
	 */
	private static Object parseStrToDate(String str) {
		if (str == null || "".equals(str))
			return str;

		if (str.indexOf("/") != -1) {
			str.replaceAll("/", "-");
		}
		String fromat = "";
		if (str.indexOf("-") != -1) {
			fromat = "YYYY-MM-DD";
			if (str.indexOf(":") != -1)
				fromat += " HH:mm:ss";
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromat);
		try {
			return simpleDateFormat.parse(str);
		} catch (Exception e) {
			return str;
		}

	}
}
