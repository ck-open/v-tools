package com.ck.demo.tools.jdbc;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 数据库操作工具类
 *
 * @author cyk
 * @since 2020-11-06
 */
public abstract class SQLUtils {


    /**
     * @param tableName 表名
     * @param pos       数据对象实例列表
     * @return List<String>
     * @description 数据新增  批量
     * @author Cyk
     * @since 17:19 2020-11-06
     **/
    public static List<String> getInsertBatchSql(String tableName, List pos) {
        List<String> insertSqlList = new ArrayList<String>();
        if (pos != null) {
            for (int p = 0; p < pos.size(); p++) {
                Object po = pos.get(p);
                StringBuffer parameter = new StringBuffer();
                StringBuffer values = new StringBuffer();
                if (po instanceof Map) {
                    for (Object key : ((Map) po).keySet()) {
                        Object val = parseSqlValue(((Map) po).get(key));
                        parameter.append("," + key.toString());
                        values.append("," + val);
                    }
                } else {
                    Field[] fields = po.getClass().getDeclaredFields();
                    if (fields != null) {
                        for (int i = 0; i < fields.length; i++) {
                            Field fieldItem = fields[i];
                            if (Modifier.isStatic(fieldItem.getModifiers())) {
                                continue;
                            }
                            fieldItem.setAccessible(true);
                            Object val = null;
                            try {
                                val = parseSqlValue(fieldItem.get(po));
                            } catch (IllegalAccessException e) {
                                throw new RuntimeException(e);
                            }
                            if (val != null) {
                                parameter.append("," + fieldItem.getName());
                                values.append("," + val);
                            }
                        }
                    }
                }
                insertSqlList.add("INSERT INTO " + tableName + " (" + parameter.toString().substring(1) + ")  VALUES (" + values.toString().substring(1) + ")");
            }
        }

        return insertSqlList;
    }


    /**
     * @param tableName 表名
     * @param po        数据对象实例
     * @param where     条件
     * @return String
     * @description 数据更改
     * @author Cyk
     * @since 17:18 2020-11-06
     **/
    public static String getUpdateSql(String tableName, Object po, String where) {
        String updateStr = null;
        if (tableName == null || po == null || where == null || "".equalsIgnoreCase(where.trim())) {
            return updateStr;
        }

        updateStr = "UPDATE " + tableName + " SET ";
        Field[] fields = po.getClass().getDeclaredFields();
        if (fields != null) {
            StringBuffer valTemp = new StringBuffer(" ");
            for (int i = 0; i < fields.length; i++) {
                Field fieldItem = fields[i];
                fieldItem.setAccessible(true);
                Object val = null;
                try {
                    val = fieldItem.get(po);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                if (val != null) {
                    if (i > 0) {
                        valTemp.append(",");
                    }
                    if (val instanceof String) {
                        val = "'" + val.toString() + "'";
                    }
                    valTemp.append(fieldItem.getName() + "=" + val);
                }
            }

            updateStr = updateStr + valTemp.toString() + " WHERE " + where;
        }

        return updateStr;
    }


    /**
     * @param tableName 表名
     * @param po        数据对象实例
     * @return String
     * @description 数据删除
     * @author Cyk
     * @since 17:17 2020-11-06
     **/
    public static String getDeleteSql(String tableName, Object po) {
        if (tableName == null || po == null) {
            return null;
        }

        Field[] fields = po.getClass().getDeclaredFields();
        if (fields != null) {
            StringBuffer valTemp = new StringBuffer(" ");
            for (int i = 0; i < fields.length; i++) {
                Field fieldItem = fields[i];
                fieldItem.setAccessible(true);
                Object val = null;
                try {
                    val = fieldItem.get(po);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                if (val != null) {
                    if (i > 0) {
                        valTemp.append(" AND ");
                    }
                    if (val instanceof String) {
                        val = "'" + val.toString() + "'";
                    }
                    valTemp.append(fieldItem.getName() + "=" + val);
                }
            }
            return getDelete(tableName, valTemp.toString());
        }

        return null;
    }


    /**
     * @param tableName 表名
     * @param where     条件
     * @return String
     * @description 数据更改
     * @author Cyk
     * @since 17:16 2020-11-06
     **/
    public static String getDelete(String tableName, String where) {
        String updateStr = null;
        if (tableName == null || where == null || "".equalsIgnoreCase(where.trim())) {
            return updateStr;
        }
        return "DELETE FROM  " + tableName + "  WHERE  " + where;
    }


    /**
     * @param sql
     * @param pageNo
     * @param pageSize
     * @return String
     * @description 分页查询
     * @author Cyk
     * @since 17:16 2020-11-06
     **/
    public static String getSelectSql(String sql, Integer pageNo, Integer pageSize) {
        String pageSql = null;
        if (pageNo != null && pageSize != null) {
            Integer startRow = (pageNo - 1) * pageSize;
            Integer endRow = startRow + pageSize;
            pageSql = "SELECT * from (SELECT a.*, ROWNUM rn from (" + sql + ") a where rownum <=" + endRow + ") where rn >" + startRow;
        }
        return pageSql;
    }

    /**
     * @param sql
     * @return String
     * @description 获取统计Cunt 函数Sql
     * @author Cyk
     * @since 17:11 2020-11-06
     **/
    public static String getSelectCountSql(String sql) {
        return "SELECT COUNT(1) AS total " + sql.substring(sql.toLowerCase().indexOf("from"));
    }


    public static void main(String[] args) {
        double val = 5 / 2;
        System.out.println(Math.ceil(val));
    }


    /**
     * @param val
     * @return Object
     * @description 解析值对象为sql语句
     * @author Cyk
     * @since 17:16 2020-11-06
     **/
    public static Object parseSqlValue(Object val) {
        if (val != null) {
            if (val instanceof String) {
                val = "'" + val.toString() + "'";
            }
            if (val instanceof Date) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                val = "TO_DATE('" + simpleDateFormat.format(val) + "', 'YYYY-MM-DD HH24:MI:SS')";
            }
        }
        return val;
    }


    /**
     * @param resultSet 查询结果集
     * @param cla       结果泛型
     * @return List<T>
     * @description 解析数据库查询结果集
     * @author Cyk
     * @since 17:15 2020-11-06
     **/
    public static <T> List<T> parseResultSet(ResultSet resultSet, Class<T> cla) throws SQLException {
        if (resultSet != null) {
            List<T> result = new ArrayList();
            ResultSetMetaData rmd = resultSet.getMetaData();
            if (rmd != null) {
                while (resultSet.next()) {
                    Map bean = new HashMap();
                    for (int i = 1; i <= rmd.getColumnCount(); i++) {
                        if (rmd.getColumnName(i) != null && resultSet.getString(i) != null) {
                            bean.put(rmd.getColumnName(i).toLowerCase(), resultSet.getString(i));
                        }
                    }
                    result.add((T) parseMapToBean(bean, true, cla));
                }
            }
            return result;
        }
        return null;
    }


    /**
     * Map 转 Bean 对象
     *
     * @param map   map
     * @param setIc 转换中是否忽略key值大小写
     * @param clazz 需要转换的对象类型
     * @param <T>   转换后的对象
     * @return
     * @author cyk
     * @since 2020-01-01
     */
    public static <T> T parseMapToBean(Map map, boolean setIc, Class<?> clazz) {
        T obj = null;
        try {

            if (map != null && !map.isEmpty()) {
                if (clazz.equals(Map.class)) {
                    return (T) map;
                }
                obj = (T) clazz.newInstance();

                // 不区分大小写时 将所有key转小写
                if (setIc) {
                    Map mapTemp = new HashMap();
                    for (Object key : map.keySet()) {
                        if (key instanceof String) {
                            mapTemp.put(key.toString().toLowerCase(), map.get(key));
                        } else {
                            mapTemp.put(key, map.get(key));
                        }
                    }
                    map = mapTemp;
                }

                // 获取类型及继承父类的所有参数
                Class clazzTemp = clazz;
                List<Field> fields = new ArrayList();
                while (clazzTemp != null) {
                    fields.addAll(Arrays.asList(clazzTemp.getDeclaredFields()));
                    clazzTemp = clazzTemp.getSuperclass();
                }

                for (int i = 0; i < fields.size(); i++) {
                    Field field = fields.get(i);
                    Object value = map.get(setIc ? field.getName().toLowerCase() : field.getName());
                    if (value != null) {
                        value = parseDataType(field.getType().getName(), value);
                        if (value == null) {
                            continue;
                        }
                        field.setAccessible(true);
                        field.set(obj, value);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * @description 数据类型解析转换
     * @author Cyk
     * @since 17:28 2022/4/27
     * @param field 实体字段
     * @param value 需要转换的值
     * @return Object
     **/
    public static Object parseDataType(Field field, Object value) {
        if (field == null) return value;
        return parseDataType(field.getType().getName(), value);
    }

    /**
     * @param className 例如：String.class.getName() || Field.getType().getName()
     * @param value     new Object()
     * @return Object
     * @description 数据类型解析转换
     * @author Cyk
     * @since 17:15 2020-11-06
     **/
    public static Object parseDataType(String className, Object value) {
        if (Date.class.getName().equals(className)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                value = simpleDateFormat.parse(value.toString());
            } catch (Exception e) {
                System.out.println("==========SQLUtils  Error  数据类型解析失败，数据：" + value.toString() + "   解析成 Date（yyyy-MM-dd HH:mm:ss）类型是异常：" + e.getMessage());
            }
        } else if (String.class.getName().equals(className)) {
            value = value.toString();
        } else if (Long.class.getName().equals(className) || long.class.getName().equals(className)) {
            value = Long.parseLong(value.toString());
        } else if (Integer.class.getName().equals(className) || int.class.getName().equals(className)) {
            value = Integer.parseInt(value.toString());
        } else if (Float.class.getName().equals(className)
                || float.class.getName().equals(className)) {
            value = Float.parseFloat(value.toString());
        } else if (Double.class.getName().equals(className)
                || double.class.getName().equals(className)) {
            value = Double.parseDouble(value.toString());
        } else if (BigDecimal.class.getName().equals(className)) {
            value = new BigDecimal(value.toString());
        } else {
            return null;
        }

        return value;
    }

    /**
     * List<List<String>> 转 List<Bean> 对象
     *
     * @param list  数据参数二维数组，第一行必须为对象属性名称
     * @param setIc 转换中是否忽略key值大小写
     * @param cls   需要转换的对象类型
     * @param <T>   转换后的对象
     * @return
     * @author cyk
     * @since 2020-01-01
     */
    public static <T> List<T> parseListToBeans(List<List<String>> list, boolean setIc, Class<T> cls) {
        List<T> result = new ArrayList<>();

        // 获取类型及继承父类的所有参数
        Class clazzTemp = cls;
        List<Field> fields = new ArrayList();
        while (clazzTemp != null) {
            fields.addAll(Arrays.asList(clazzTemp.getDeclaredFields()));
            clazzTemp = clazzTemp.getSuperclass();
        }
        Map<String, Field> fieldMap = new HashMap<>();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (setIc) {
                fieldName = fieldName.toUpperCase();
            }
            fieldMap.put(fieldName, field);
        }

        List<String> title = null;
        for (int r = 0; r < list.size(); r++) {
            List<String> row = list.get(r);

            if (r == 0) {
                title = row;
                continue;
            }
            if (row.get(0) == null || "".equals(row.get(0))) {
                continue;
            }

            Field f = null;
            Object v = null;
            try {
                T temp = cls.newInstance();
                result.add(temp);
                for (int c = 1; c < row.size(); c++) {
                    String var = row.get(c);

                    f = fieldMap.get(title.get(c));
                    if (f != null) {
                        v = var;
                        if ("".equals(var)) {
                            v = null;
                        }
                        v = parseDataType(f, v);
                        f.setAccessible(true);
                        f.set(temp, v);
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException("二维数组第" + (r + 1) + "行 转换实体对象（" + cls.getName() + "）失败：" + row + "\r\n"
                        + cls.getSimpleName() + "." + f.getName() + "=" + (v == null ? v : v.toString()) + "  需要的参数类型：" + f.getType().getName());
            }
        }
        return result;
    }


    /**
     * 分页数据实体
     */
    public static class PageResult<E> {
        List<E> data;
        int pageNo;
        int pageSize;
        int pages;
        int total;

        public List<E> getData() {
            return data;
        }

        public void setData(List<E> data) {
            this.data = data;
        }

        public int getPageNo() {
            return pageNo;
        }

        public void setPageNo(int pageNo) {
            this.pageNo = pageNo;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
            int pages = total / pageSize;
            this.pageNo = total % pageSize == 0 ? 0 : 1;
            this.pages = pages + this.pageNo;
        }
    }
}
