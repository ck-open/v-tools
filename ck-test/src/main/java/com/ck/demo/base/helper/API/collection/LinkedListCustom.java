package com.ck.base.helper.API.collection;

/**
 * 模拟实现双向循环链表
 * 
 * @author cyk
 *
 * @param <E>
 */
class LinkedListCustom<E> {
	private Node head;
	private int size;

	class Node {
		E data;
		Node next; // 下一个
		Node prev; // 上一个
		// Node构造器

		Node(E e) {
			data = e;
		}
	}

	public void add(E e) {
		if (head == null) {
			// 第一次添加元素的情况
			head = new Node(e);
			head.next = head;
			head.prev = head;
		} else {
			// 第二次以后,添加元素
			Node last = head.prev; // 记录第一个元素的prev上一个元素
			Node node = new Node(e); // 创建添加的元素
			node.next = head; // 新添加的元素的next下一个指向第一个元素
			node.prev = last; // 新添加元素的prev指向第一个元素原来指向的上一个
			head.prev = node; // 将第一个元素的previous上一个指向新添加的元素
			last.next = node; // 将原来的最后一个元素指向新添加的元素

		}
		size++;
	}

	public void add(int index, E e) {
		// index超出size返回，抛出异常
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		Node node = find(index);
		// 创建新元素
		Node newNode = new Node(e);
		Node prevNode = node.prev;
		// 连接节点
		prevNode.next = newNode; // 插入位置的上一个元素连接新元素
		newNode.next = node; // 新元素连接原位置之前的元素为下一个元素
		newNode.prev = prevNode;
		node.prev = newNode;

		size++;
	}

	public E delete(int index) {
		// index超出size返回，抛出异常
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		Node node = find(index);
		Node nextNode = node.next; // 记录删除元素的下一个元素
		Node prevNode = node.prev; // 记录删除元素的上一个元素
		nextNode.prev = prevNode; // 删除元素的下一个元素连接删除元素的上一个元素
		prevNode.next = nextNode; // 删除元素的上一个元素连接删除元素的下一个元素
		return node.data;
	}

	/**
	 * 查找元素
	 * 
	 * @param index
	 * @return
	 */
	private Node find(int index) {
		Node node;
		if (index <= size / 2) {
			// 正方向插入元素
			// 查找插入元素的当前位置的节点
			int n = 0;
			node = head;
			while (n != index) {
				node = node.next;
				n++;
			}
		} else {
			// 负方向插入元素
			// 查找插入元素的当前位置的节点
			int n = size - 1;
			node = head.prev;
			while (n != index) {
				node = node.prev;
				n--;
			}
		}
		return node;
	}

	public int size() {
		return size;
	}

	public String toString() {
		StringBuilder str = new StringBuilder("[");
		str.append(head.data);
		// node代表当前正在遍历的节点
		Node node = head.next;
		// 如果当前元素的下一个不是头节点，则继续循环
		while (node != head) {
			str.append(",").append(node.data);
			node = node.next;
		}
		return str.append("]").toString();
	}
}