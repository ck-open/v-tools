package com.ck.base.helper.binary;

import java.math.BigInteger;

/**
 * 二进制基础
 */
public class BinaryBase {
    /*
     * Integer.toBinaryString(i)  //将变量i转换为二进制以字符串类型返回
     * Integer.toHexString(i)     //将变量i转换为十六进制以字符串类型返回
     * Integer.parseInt(hex, 16); // 将十六进制反编成十进制数字
     * 十六进制：0x6e2ad5   十六进制用于简写二进制，每1位十六进制代表从右到左4位二进制数字
     *
     * 对照
     *       十进制       0     1      2     3     4    5     6      7    8      9    10    11    12    13    14    15
     *       二进制      0000  0001  0010  0011  0100  0101  0110  0111  1000  1001  1010  1011  1100  1101  1110  1111
     *       十六进制0x   0     1      2     3     4    5     6      7    8      9     a     b     c     d     e     f
     *
     * 补码
     *      经一个固定位数的2进制数分一半作为负数的编码规则。解决核心问题：解决负数问题！
     *      计算时，多出(溢出)的位数自动舍弃，二进制首位数字为1表示负数，为0表示整数。
     *      补码互补对称公式：int m=~n+1;二进制负数转整数，所有位数取反+1，最小数除外。为二进制正数。 1110 转换正数为0001+1
     * 运算符
     *     1. ～ ：非运算 ，int i=8;  int b= ~i;表示将i取反（-9）赋值给b
     *     2. &  ：与运算 ,有0则0。用于mask(掩码)计算，示例 0x76a943fa & 0xff ；表示截取0x76a943fa二进制后八位数字。
     *     3. |   ：或运算 ，有1则1。用于二进制数字的拼接。示例： 00001011 | 01010000 =01011011；
     *     4. >>> : 逻辑右移位计算， 0x76a943fa >>>8;表将 0x76a943fa二进制数字向右移动8位，前补0，后舍弃。
     *     5. >>   : 数学右移计算，右移位计算,规则：将数字整体向右移动，底位自动溢出舍弃，高位补0。
     *     6. <<   : 左移位计算,规则：将数字整体向左移动，高位自动溢出舍弃，低位补0。
     * >>/>>>的区别：
     *      >>  : 数学右移位，正数高位补0，负数高位补1，相当于数学除法向小方向取整数(即小数取舍时取最小的数)的结果。
     *      >>>: 逻辑右移位，无论正负高位都补0，不符合数学运算规律。
     */
    public static void main(String[] args) {
        Integer i = -255;
        System.out.println("十进制："+i+"  二进制："+Integer.toBinaryString(i)+"  十六进制："+Integer.toHexString(i));
        System.out.println("将i："+i+"  补码取反 ~（非运算符） ："+(~i+1));
        System.out.println("将i："+i+"  掩码&（与运算符）："+(i&0xff));
        System.out.println("将i："+i+"  二进制拼接|（或运算符）："+(i|0xff));
        System.out.println("将i："+i+"  逻辑右移>>>："+(i>>>8));
        System.out.println("将i："+i+"  数学右移>>："+(i>>8));
        System.out.println("将i："+i+"  逻辑左移<<："+(i<<8));
    }

    /**
     * 十进制转十六进制<br>
     *     十进制除16取余，将所有余数倒序排列
     * @param number
     * @return
     */
    private static String denaryToHex(int number) {
        number = number<0?~number+1:number;
        StringBuffer result = new StringBuffer();
        char[] hex = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        while(number != 0){
            int index = number%16;
            result = result.append(hex[index]);
            number = number/16;
        }
        return result.reverse().toString();  // 倒序并返回字符串
    }

// ************** Integer 二进制转换源码 **********************************
    /**
     * Convert the integer to an unsigned number.
     * 将整数转换为无符号数。
     */
    private static String toUnsignedString0(int val, int shift) {
        // assert shift > 0 && shift <=5 : "Illegal shift value";
        int mag = Integer.SIZE - Integer.numberOfLeadingZeros(val);
        int chars = Math.max(((mag + (shift - 1)) / shift), 1);
        char[] buf = new char[chars];

        formatUnsignedInt(val, shift, buf, 0, chars);

        // Use special constructor which takes over "buf".
        return new String(buf);
    }

    /**
     * 将Long(视为无符号)格式化为字符缓冲区。
     * Format a long (treated as unsigned) into a character buffer.
     * @param val the unsigned int to format
     *            要格式化的无符号整型数
     * @param shift the log2 of the base to format in (4 for hex, 3 for octal, 1 for binary)
     *              要格式化的基的Log2(4表示十六进制，3表示八进制，1表示二进制)
     * @param buf the character buffer to write to
     *            要写入的字符缓冲区
     * @param offset the offset in the destination buffer to start at
     *               目标缓冲区中要开始的偏移量
     * @param len the number of characters to write
     *            要写的字符数
     * @return the lowest character  location used  使用的最低字符位置
     */
    static int formatUnsignedInt(int val, int shift, char[] buf, int offset, int len) {
        final char[] digits = {
                '0' , '1' , '2' , '3' , '4' , '5' ,
                '6' , '7' , '8' , '9' , 'a' , 'b' ,
                'c' , 'd' , 'e' , 'f' , 'g' , 'h' ,
                'i' , 'j' , 'k' , 'l' , 'm' , 'n' ,
                'o' , 'p' , 'q' , 'r' , 's' , 't' ,
                'u' , 'v' , 'w' , 'x' , 'y' , 'z'
        };

        int charPos = len;
        int radix = 1 << shift;
        int mask = radix - 1;
        do {
            buf[offset + --charPos] = digits[val & mask];
            val >>>= shift;
        } while (val != 0 && charPos > 0);

        return charPos;
    }

}
