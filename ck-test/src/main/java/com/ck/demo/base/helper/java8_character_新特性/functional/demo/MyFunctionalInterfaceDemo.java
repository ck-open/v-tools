package com.ck.base.helper.java8_character_新特性.functional.demo;

/**
 * 函数式接口的使用：一般可作为方法的参数或返回值类型
 */
public class MyFunctionalInterfaceDemo {
    // 定义发一个方法，参数使用函数式接口MyFunctionalInterface
    public static void show(MyFunctionalInterface myFunctionalInterface){
        myFunctionalInterface.method();
    }

    public static void main(String[] args) {
        // 调用show方法,方法参数是一个接口，所以可以传递参数的实现类对象
        show(new MyFunctionalInterfaceImpl());

        // 调用show方法,方法参数是一个接口，可以传递接口的匿名内部类
        show(new MyFunctionalInterface() {
            @Override
            public void method() {
                System.out.println("函数式接口使用匿名内部类传递参数！");
            }
        });

        // 调用show方法,方法参数是一个接口，可以使用lambda传递接口的匿名内部类
        // 内部类中只有一个方法是可以省略()->{} 中的大括号。lambda看似是简化内部类的语法糖，但编译后lambda没有独立的.class文件
        show(()->System.out.println("函数式接口使用lambda传递参数！"));
    }
}
