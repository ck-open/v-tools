package com.ck.base.helper.java8_character_新特性.functional.vutils;

import java.util.Objects;

/**
 * 表示接受单个输入参数且不返回结果的操作，与大多数其他功能接口不同。Consumer预计通过副本进行操作。
 * 这是一个功能接口，其功能方法是accept(Object)。
 * 自 java 1.8
 *
 * @param <T> 参数<T> 操作的输入类型
 */
@FunctionalInterface
public interface MyConsumer<T> {

    /**
     * 对给定参数执行此操作
     *
     * @param t 输入的参数
     */
    void accept(T t);

    /**
     * 返回一个组合的Consumer，它依次执行此操作和accept操作。
     * 如果执行任一操作引发异常，则将其转发给组合操作的调用者。
     * 如果执行此操作抛出异常，则不会执行accept操作。
     *
     * @param after 再次操作之后执行的操作
     * @return 一个组合的Consumer，它按顺序执行此操作，然后执行accept操作。
     * 如果after为空则抛出：NullPointerException
     */
    default MyConsumer<T> andThen(MyConsumer<? super T> after) {
        Objects.requireNonNull(after);
        return (T t) -> {
            accept(t);
            after.accept(t);
        };
    }
}
