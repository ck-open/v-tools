package com.ck.demo.base.test;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class RAF {

	/**
	 * 写入用户信息到文件
	 * @param name
	 * @param password
	 * @param email
	 * @param age
	 */
	public static void writeUser(String name,String password,String email,int age) {
		RandomAccessFile raf = null; //创建文件流对象
		try {
			//初始化对象，获取文件流连接
			raf = new RandomAccessFile("./raf.dat","rw");
			//将文件指针移动到文件末尾，方便追加
			raf.seek(raf.length());
			//写入用户名
			byte[] data = name.getBytes("utf-8"); //编码
			data = Arrays.copyOf(data, 32); //扩容
			raf.write(data); //写出
			//写入密码
			data = password.getBytes("utf-8");
			data = Arrays.copyOf(data, 32);
			raf.write(data);
			//写入邮箱
			data = email.getBytes("utf-8");
			data = Arrays.copyOf(data, 32);
			raf.write(data);
			//写入年龄
			raf.writeInt(age); //需要使用writeInt  否则不会占用4字节
			System.out.println("数据写入完成！");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				raf.close();//关闭文件流连接
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * 读取文件中的用户信息
	 */
	public static void readUser() {
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile("./raf.dat","r");
			byte[] data = new byte[32];
			for (int i = 0; i < raf.length()/100; i++) {
				raf.seek(i*100);
				//读取用户名
				raf.read(data);
				String name = new String(data,"utf-8").trim();
				//读取密码
				raf.read(data);
				String password = new String(data,"utf-8").trim();
				//读取邮箱
				raf.read(data);
				String email = new String(data,"utf-8").trim();
				//读取年龄
				int age = raf.read();
				System.out.println("name-->"+name+"   password-->"+password+"   email-->"+email+"   age-->"+age);
			}
			System.out.println("读取完毕！");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				raf.close();//关闭文件流连接
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		//写入测试
		writeUser("捷克反对人", "root123", "root@root.cn", 25);
		
		//读取测试
		readUser();
	}
}
