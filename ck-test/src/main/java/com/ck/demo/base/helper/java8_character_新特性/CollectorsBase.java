package com.ck.base.helper.java8_character_新特性;


import jdk.nashorn.internal.AssertsEnabled;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 集合工具新特性
 * Java8函数式编程（三）：Collectors.groupingBy
 */
public class CollectorsBase {
    public static void main(String[] args) {
//        count();
//        streamNumber();



        String[] arr = {"1","2","3","4","5"};
        Spliterator spliterator = Arrays.spliterator(arr);
        Spliterator spliterator2 = spliterator.trySplit();
        Spliterator spliterator3 = spliterator.trySplit();

        while (spliterator.tryAdvance(i-> System.out.println(i)));
        while (spliterator2.tryAdvance(i-> System.out.println(i)));
        while (spliterator3.tryAdvance(i-> System.out.println(i)));

        fruitList.sort(Comparator.comparing(Fruit::getPrice).reversed());

//        int i=0;
//        while (spliterator.tryAdvance(System.out::println)){
//            System.out.println("i的值："+ ++i);
//        }
//
//
//
//        Iterator<String> iterator = Arrays.asList("a","b","c","d").iterator();
//        // 传统遍历
//        while(iterator.hasNext()){
//            System.out.println(iterator.next());
//        }
//        // 使用forEachRemaining 遍历
//        iterator.forEachRemaining(a-> System.out.println(a));
//
//
//
//
//
//
//
//
//        spliterator.trySplit();
//        spliterator.estimateSize();
//        spliterator.forEachRemaining(null);

    }


   private static List<Fruit> fruitList = Arrays.asList(
           new Fruit("apple", 6.0),
            new Fruit("apple", 6.0),
            new Fruit("apple", 11.0),
            new Fruit("banana", 7.0), new Fruit("banana", 7.0),
            new Fruit("banana", 7.0), new Fruit("grape",8.0));

    /**
     * 计数
     */
    public static void count(){

        /*
         * 计数  输出结果是：{banana=3, apple=2, grape=1}
         */
        Map<String, Long> map = fruitList.stream().
                collect(Collectors.groupingBy(Fruit::getName,Collectors.counting()));
        System.out.println( "计数 ==>"+map.toString());

        /*
         * 排序  现在要按照水果map中value的数量逆序打印每个entry
         */
        map.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .forEachOrdered(System.out::println);
        System.out.println( "排序 ==>"+map.toString());

        // 排序  List里的Map 进行排序
        List<Map<String,String>> table = new ArrayList<>();
        Map<String, String> row = new TreeMap<>();
        row.put("team", "一班");  // 班组名称
        row.put("value", "1.56 Kwh");  // 泵站名称
        row.put("pumpStation", "一号泵站");  // 泵站名称
        row.put("department", "黄河泵站");  // 部门名称
        table.add(row);
        // 按照数据排序
        table.sort(Comparator.comparing(o -> o.get("value")));

        /*
         * 累加求和  输出结果是：{banana=21, apple=12, grape=8}
         */
        Map<String, Double> sumMap = fruitList.stream().
                collect(Collectors.groupingBy(Fruit::getName, Collectors.summingDouble(Fruit::getPrice)));
        System.out.println( "累加 ==>"+sumMap.toString());

    }


    public static void group(){
        /*
         * 分组   代码根据name将list分组，如果name是唯一的，那么上述代码就会显得啰嗦。我们需要知道，Guava补JDK之不足，现在该Guava一显身手了。
         */
        Map<String, List<Fruit>> group = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName));
        System.out.println( "分组 ==>"+group.toString());



        // 返回个不可变长度集合
        fruitList.stream().collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));

        Optional.of(fruitList.stream().collect(Collectors.counting())).ifPresent(System.out::println);

        Map<String,Double> fruitAvgPrice = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName,TreeMap::new,Collectors.averagingDouble(Fruit::getPrice)));

        /*
         * 根据不同的名字分为若干组
         * 生成的Map是ImmutableMap(不可变Map)，不可更改里面的值。比如map.remove("apple")会抛出异常：java.lang.UnsupportedOperationException
         */
        // group by price, uses 'mapping' to convert List<Fruit> to List<String>
        Map<String, List<Double>> groupMap =fruitList.stream().collect(Collectors.groupingBy(Fruit::getName,
                Collectors.mapping(Fruit::getPrice, Collectors.toList())));
        System.out.println( "分为多组1 ==>"+groupMap.toString());
        // 上面一段代码可以用Guava代替
        Map<String, Double> multiMap = new HashMap<>();
        fruitList.forEach(fruit -> multiMap.put(fruit.getName(), fruit.getPrice()));
        System.out.println( "分为多组2 ==>"+multiMap.toString());



        // 取list中对象的某个属性作为唯一key，对象作为value形成一个map集合，能够便于在后续业务中取值而不用遍历list集合，使代码更优雅。
        Map<String,Fruit> fruitMap = fruitList.stream().collect(Collectors.toMap(Fruit::getName, Function.identity()));

        // 通过分组取每组第一条数据的操作可以在很多场景适用，比如取多条数据中最新的一条数据等场景。
        fruitMap = fruitList.stream().collect(Collectors.toMap(Fruit::getName, Function.identity(), (i1, i2) -> i1.getNumber() > i2.getNumber() ? i1 : i2));

        fruitMap = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName
                , Collectors.collectingAndThen(
                        Collectors.reducing((i1, i2) -> i1.getNumber() > i2.getNumber() ? i1 : i2)
                        , Optional::get)));


        Optional fruit = Optional.ofNullable(fruitList.get(0)).filter(i->i.getNumber()!=null);
        Objects.requireNonNull(fruit,()->{
            System.out.println(fruit);
            return "参数为NUll";
        });




        // 单条件分组
        Map<String,List<Fruit>> mapByOne = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName));

        // 分组后，统计每组中的数据量
        Map<String,Long> count = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName,Collectors.counting()));

        // 分组后，求出每组某属性的平均值
        Map<String,Double> avg = fruitList.stream().filter(i->i.getPrice()!=null).collect(Collectors.groupingBy(Fruit::getName, Collectors.averagingDouble(Fruit::getPrice)));

        // 分组，某属性求和
        Map<String,Double> sum = fruitList.stream().filter(i->i.getPrice()!=null).collect(Collectors.groupingBy(Fruit::getName,Collectors.summingDouble(Fruit::getPrice)));

        // 对求和的数据进行从小到大排序
        Map<String,Double> finalMap = new LinkedHashMap<>();
        sum.entrySet().stream().sorted(Map.Entry.<String,Double>comparingByValue().reversed()).forEachOrdered(e->finalMap.put(e.getKey(),e.getValue()));

        // 分组后通过join 组成新的map
        Map<String,String> joinNewMap = fruitList.stream().filter(i->i.getPrice()!=null)
                .collect(Collectors.groupingBy(Fruit::getName,Collectors.mapping(i->i.getPrice().toString(),Collectors.joining(",","Prices:[","]"))));

        // 转换分组结果
        Map<String,Set<Double>> priceByName = fruitList.stream().filter(i->i.getPrice()!=null)
                .collect(Collectors.groupingBy(Fruit::getName,Collectors.mapping(Fruit::getPrice,Collectors.toSet())));

        // 多条件分组
        Map<String,Map<String,List<Fruit>>> groupByMore = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName,Collectors.groupingBy(Fruit::getShelfLife)));

    }

    public static void streamNumber(){
        int[] ints = {1,5,9,2,5,63,4,8};
        System.out.println("____使用rangeClosed____");
        // 返回子序列[a,b]左闭右开,意味着包括b元素,增长步值为1
        IntStream.rangeClosed(0,5).forEach(System.out::println);

        System.out.println("____使用range____");
        // 返回子序列[a,b),右闭右开,意味着不包括b
        IntStream.range(0,5).forEach(System.out::println);

        System.out.println("____使用sum____");
        // 计算所有元素总和
        System.out.println(IntStream.rangeClosed(0,5).sum());

        System.out.println("____使用sorted____");
        // 排序元素(从小到大)
        IntStream.of(ints).sorted().forEach(System.out::println);

        // 循环一个list集合并获取循环的索引
        Stream.iterate(0,i->i+1).limit(ints.length).forEach(i->{
            System.out.println(i+" ==> "+ints[i]);
        });


    }


    public static void sumBigDecimal(){
        List<BigDecimal> list = new ArrayList<>();
        list.add(BigDecimal.valueOf(1.1));
        list.add(BigDecimal.valueOf(1.2));
        list.add(BigDecimal.valueOf(1.3));
        list.add(BigDecimal.valueOf(1.4));

        BigDecimal decimal = list.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(decimal);
    }

}


// 水果
class Fruit {
    // 水果名称
    private String name;
    // 是否热带水果
    private Boolean isTropic;
    // 保质期
    private String shelfLife;
    // 价格
    private Double price;
    // 数量
    private Integer number;

    public Fruit(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fruit fruit = (Fruit) o;
        return java.util.Objects.equals(name, fruit.name) &&
                java.util.Objects.equals(price, fruit.price);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(name, price);
    }

    public  String getName() {
        return this.name;
    }

    public Double getPrice() {
        return this.price;
    }

    public String getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }

    public Boolean getTropic() {
        return isTropic;
    }

    public void setTropic(Boolean tropic) {
        isTropic = tropic;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}