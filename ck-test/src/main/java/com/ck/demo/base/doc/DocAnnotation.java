
/*

java.lang.annotation.Annotation  注解类型接口
    Annotation接口有4个抽象方法,equals(Object obj), hashCode(),toString(),annotationType()
    Annotation接口关联1个RetentionPolicy类和1~n个ElementType类。

注解框架的关联类
    RetentionPolicy类
        RetentionPolicy是一个枚举类型,位于java.lang.annotation包下
        RetentionPolicy有三种类型,SOURCE,CLASS,RUNTIME
        1个Annotation接口关联1个RetentionPolicy的类型
        RetentionPolicy可理解为给Annotation指定策略/作用域,这三种策略分别SOURCE,CLASS,RUNTIME
            这三种类型的解释如下表所示:
                SOURCE	表示Annotation信息仅存在于编译器处理期间，编译器处理完之后就没有该Annotation信息了
                CLASS	表示编译器将Annotation存储于类对应的.class文件中,不会被加载进JVM。(默认行为)
                RUNTIME	表示编译器将Annotation存储于class文件中，并且可由JVM读入

    ElementType类
        ElementType是一个枚举类型,位于java.lang.annotation包下
        1个Annotation接口关联1~n个ElementType的类型
        ElementType可理解为指定Annotation的元素类型
        ElementType包含10种类型,分别为: TYPE ,FIELD ,METHOD ,PARAMETER ,CONSTRUCTOR ,LOCAL_VARIABLE ,ANNOTATION_TYPE ,PACKAGE, TYPE_PARAMETER, TYPE_USE
            TYPE	类、接口（包括注释类型）或枚举声明
            FIELD	字段声明（包括枚举常量）
            METHOD	方法声明
            PARAMETER	参数声明
            CONSTRUCTOR	构造方法声明
            LOCAL_VARIABLE	局部变量声明
            ANNOTATION_TYPE	注释类型声明
            PACKAGE	包声明

Annotation的实现类
        @Documented
        @Target(ElementType.TYPE)
        @Retention(RetentionPolicy.RUNTIME)
        public @interface myAnnotation{}
    上面的作用是定义一个Annotation，它的名字是MyAnnotation。定义了MyAnnotation之后，我们可以在代码中通过“@MyAnnotation”来使用它。
    其它的，@Documented, @Target, @Retention, @interface注解都是来修饰MyAnnotation的。
        @interface
            使用@interface定义注解时，意味着它实现了java.lang.annotation.Annotation接口，即该注解就是一个Annotation。定义Annotation时，@interface是必须的。
            注意：它和我们通常的implemented实现接口的方法不同。Annotation接口的实现细节都由编译器完成。
            通过@interface定义注解后，该注解不能继承其他的注解或接口。

        @Documented
            类和方法的Annotation在缺省情况下是不出现在javadoc中的。如果使用@Documented修饰该Annotation，则表示它可以出现在javadoc中。
            定义Annotation时，@Documented可有可无；若没有定义，则Annotation不会出现在javadoc中。

        @Target(ElementType.TYPE)
            ElementType是Annotation的类型属性。而@Target的作用，就是来指定Annotation的类型属性。
            @Target(ElementType.TYPE)的意思就是指定该Annotation的类型是ElementType.TYPE。这就意味着，MyAnnotation1是来修饰“类、接口（包括注释类型）或枚举声明”的注解。
            定义Annotation时，@Target可有可无。若有@Target，则该Annotation只能用于它所指定的地方；若没有@Target，则该Annotation可以用于任何地方。

        @Retention(RetentionPolicy.RUNTIME)
            RetentionPolicy是Annotation的策略属性，而@Retention的作用，就是指定Annotation的策略属性。
            @Retention(RetentionPolicy.RUNTIME)的意思就是指定该Annotation的策略是RetentionPolicy.RUNTIME。这就意味着，编译器会将该Annotation信息保留在.class文件中，并且能被虚拟机读取。
            定义Annotation时，@Retention可有可无。若没有@Retention，则默认是RetentionPolicy.CLASS。

        备注:修饰其他注解的注解称为元注解。
            Retention里的参数只有一个,因为前面我们提到RetentionPolicy和Annotation的关联是1对1Target可以有多个这是因为ElementType和Annotation的关联是1~n对1,
            接下来就不在做任何注解解释了,你可以直接跳转到对应注解的源码,然后通过上面给出的相应表格查询相应说明。


    注解定义的时候常常还定义一些参数,这些参数一般的数据类型如下:
                    所有基本类型；
                    String；
                    枚举类型；
                    基本类型、String、Class以及枚举的数组。
        定义参数如下：
            @Documented
            @Target(ElementType.TYPE)
            @Retention(RetentionPolicy.RUNTIME)
            @Inherited
            public @interface Inheritable{
                int type() default 0;
                String level() default "info";
                String value() default "";
            }
        使用的时候就可以给注解相应参数赋值,这里赋的值都是常量,如下: @Inheritable(type = 10,level = "debug",value = "10")
        备注:注解常常都有一个value参数,该参数表示最常用的参数。

    Annotation的实现类
        java 常用Annotation的实现类如下表所示：
            @Deprecated	所标注内容，不再被建议使用。
            @Override	只能标注方法，表示该方法覆盖父类中的方法。
            @Documented	所标注内容，可以出现在javadoc中。
            @Inherited	只能被用来标注“Annotation类型”，它所标注的Annotation具有继承性。
            @Retention	只能被用来标注“Annotation类型”，而且它被用来指定Annotation的RetentionPolicy属性。
            @Target	    只能被用来标注“Annotation类型”，而且它被用来指定Annotation的ElementType属性。
            @SuppressWarnings	所标注内容产生的警告，编译器会对这些警告保持静默。

        被@Deprecated注解标注表示不在被建议使用,使用的时候会给出相应的提示信息,方法或类名称出现删除线。


 */


package com.ck.demo.base.doc;

import java.lang.annotation.*;

public class DocAnnotation {




    public static void main(String[] args) {
    }

    @Documented
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface myAnnotation{}

    @Documented
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Inherited
    public @interface Inheritable{
        int type() default 0;
        String level() default "info";
        String value() default "";
    }
}
