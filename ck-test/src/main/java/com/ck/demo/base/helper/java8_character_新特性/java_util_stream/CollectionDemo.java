package com.ck.base.helper.java8_character_新特性.java_util_stream;


import lombok.Data;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class CollectionDemo {

    /*
        collectors 工具
        工厂方法        返回类型                    作用
        toList          List<T>                 把流中所有项目收集到一个 List
        toSet	        Set<T>	                把流中所有项目收集到一个 Set，删除重复项
        toCollection	Collection<T>	        把流中所有项目收集到给定的供应源创建的集合menuStream.collect(toCollection(), ArrayList::new)
        counting	    Long	                计算流中元素的个数
        sumInt	        Integer	                对流中项目的一个整数属性求和
        averagingInt	Double	                计算流中项目 Integer 属性的平均值
        summarizingInt	IntSummaryStatistics	收集关于流中项目 Integer 属性的统计值，例如最大、最小、 总和与平均值
        joining	        String	                连接对流中每个项目调用 toString 方法所生成的字符串collect(joining(", "))
        maxBy	        Optional<T>	            一个包裹了流中按照给定比较器选出的最大元素的 Optional， 或如果流为空则为 Optional.empty()
        minBy	        Optional<T>	            一个包裹了流中按照给定比较器选出的最小元素的 Optional， 或如果流为空则为 Optional.empty()
        reducing	                            归约操作产生的类型	从一个作为累加器的初始值开始，利用 BinaryOperator 与流 中的元素逐个结合，从而将流归约为单个值累加int totalCalories = menuStream.collect(reducing(0, Dish::getCalories, Integer::sum));
        collectingAndThen	                    转换函数返回的类型	包裹另一个收集器，对其结果应用转换函数int howManyDishes = menuStream.collect(collectingAndThen(toList(), List::size))
        groupingBy	    Map<K, List<T>>	        根据项目的一个属性的值对流中的项目作问组，并将属性值作 为结果 Map 的键
        partitioningBy	Map<Boolean,List<T>>	根据对流中每个项目应用谓词的结果来对项目进行分区

     */


    /**
     * 分组
     *
     * @param list
     * @return
     */
    public Map<Integer, List<User>> group(List<User> list) {
        return list.stream().collect(Collectors.groupingBy(User::getAge));
    }

    /**
     * 转Map
     *
     * @param list
     * @return
     */
    public Map<String, User> toMap(List<User> list) {
        return list.stream().collect(Collectors.toMap(User::getUserName, i -> i, (i1, i2) -> i1.age > i2.age ? i1 : i2));
    }

    /**
     * 过滤器
     *
     * @param list
     * @return
     */
    public List<User> filter(List<User> list) {
        return list.stream().filter(i -> i.age >= 18).collect(Collectors.toList());
    }

    /**
     * 求和
     *
     * @param list
     * @return
     */
    public BigDecimal sum(List<User> list) {
        return list.stream().map(User::getMoney).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * 获取最大或最小
     *
     * @param list
     * @return
     */
    public User maxOrMin(List<User> list) {
//        Optional<User> user = list.stream().min(Comparator.comparing(User::getMoney));
        Optional<User> user = list.stream().max(Comparator.comparing(User::getMoney));

        return user.get();
    }

    /**
     * 统计
     *
     * @param list
     * @return
     */
    public long count(List<User> list) {
        return list.stream().filter(i -> "张三".equals(i.getUserName())).count();
    }


    /**
     * 去重
     *
     * @param list
     * @return
     */
    public List<User> distinct(List<User> list) {
        // https://blog.csdn.net/love3765/article/details/69385765
        return list;
    }

    @Data
    public static class User {
        private String userName;
        private Integer age;
        private Integer sex;
        private BigDecimal money;
    }
}
