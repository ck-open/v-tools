package com.ck.base.helper.API.io;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

/**
 * java.io.File
 * File的每一个实例用于表示文件系统（硬盘中）上的一个文件或目录。
 * 使用File可以：
 * 1：访问属性信息（名字，长度，修改时间等）
 * 2：操作文件或目录（创建，删除）
 * 3：获取一个目录的子项但是不能访问文件数据。
 * @author cyk
 *
 */
public class FileBase {

    /**
     * File没有无参构造方法，需要传入一个字符串，该字符串内容为路径。注意，路径通常不会使用
     * 绝对路径，因为不同系统的绝对路径定义不同，会出现跨平台问题。
     * 相对路径的好处在于不存在跨平台问题，但是相对的路径通常有运行环境而定。实际开发中比较常使
     * 用的是类加载路径。
     * 在eclipse中，通常相对“当前目录”，当前目录指的是当前程序所在项目的项目目录。
     */
    public void fileBase() throws IOException {

        File file = new File("./fileBase.base");  // 当前路径下的fileBase文件，“./”不写，默认也是在当前目录中

        String fileName = file.getName();   // 获取名字
        long length = file.length();        // 获取长度 byte
        boolean cr = file.canRead();        // 是否可读
        boolean cw = file.canWrite();       // 是否可写
        boolean isFile = file.isFile();     // 是否是文件
        boolean isDir = file.isDirectory(); // 是否是目录
        boolean ih = file.isHidden();       // 是否隐藏
        boolean exists = file.exists();     // 文件是否存在
        boolean isDone = file.delete();     // 删除文件或目录，删除目录的前提条件是，该目录必须是一个空目录。
        isDone = file.createNewFile();      //创建一个文件
        isDone = new File("./baseDir").mkdir();              // 创建一个目录
        isDone = new File("./Dir/baseDir").mkdirs();             // 创建多级目录
        File[] files = file.listFiles();    // 获得当前目录下的所有子file

        /*
         * File提供了一个重载的listFiles方法，可以传入一个文件过滤器，然后将其表现的目录中满足过滤器要求的子项返回。
         * File[] listFiles(FileFilter filter)
         */
        FileFilter filter = new FileFilter() {
            public boolean accept(File file) {
                return file.isFile()&&file.getName().startsWith(".");
            }
        };
        File[] subs = file.listFiles(filter);



    }
    /**
     * 递归删除多级目录文件夹
     * 在当前方法内部再次调用当前方法的现象称为递归调用
     */
    public static void deleteFiles(File file) {
        if(file.isDirectory()) {
            File[] subs = file.listFiles();//3
            for(int i=0;i<subs.length;i++) {
                File sub = subs[i];
                deleteFiles(sub);
            }
        }
        file.delete();
    }
}
