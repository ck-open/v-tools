package com.ck.base.helper.API.io;

import java.io.*;
import java.util.Scanner;

/**
 * java标准IO
 * IO指的是输入与输出（Input,Output)
 * java将读写操作按照方向划分为输入流与输出流，其中输入流用来读，输出流用来写。
 * java.io.InputStream：所有字节输入流的超类，定义了若干抽象方法，其中规定了读取文字的相关操作。
 *
 * java.io.OutputStream：所有字节输出流的超类，定义了写出字节的相关方法。
 *
 * 流有两类：
 * 节点流和处理流
 *
 * 节点流：又称为低级流。是实际链接程序与数据源的“管道”。
 *            负责实际数据的搬运工作。读写数据一定是建立在节点流的基础上进行的。特点：数据源明确。
 *
 * 处理流：又称为高级流。不能独立存在，必须链接在其他流
 *            上，作用是当数据流经该流时，其可以对数据进行某些加工处理。这样可以简化我们再对数据加工的操作。
 * 流的链接：将需要的一组处理流链接在一起，最终链接在节点
 *            流上，可对数据进行“流水线”式的加工并进行读写这些是IO读写的重点。
 *
 * 文件流与RAF都是用来读写文件数据的，但是他们也有各自的优点与缺点。
 * 文件流：文件流是一组节点流，作用是读写文件。
 *            由于流的读写模式是顺序读写，所以文件流也是以该方式读写文件数据的。所以做不到编辑文件数据（只覆盖文件
 *            特定位置的字节）。读写不能回退操作。但是基于流链接操作可以轻松读写复杂数据。
 *
 * RAF：基于指针进行读写，所以RAF可以操作指针读写特定位置的字节，能够编辑文件数据。虽然有提供方便的读写基本类型
 *            数据与字符串等方法，但是读写复杂数据需要自行完成。
 * @author cyk
 *
 */
public class IOBases {

    /**
     * 文件输出流有两种创建方式：
     * FileOutputStream(File file)
     * FileOutputStream(String path)
     * 以上构造方法创建的文件流是覆盖写操作，即：若该文件已经存在，会先将该文件数据清除，然后通过
     * 这个流写出的内容作为文件数据保存。
     *
     * FileOutputStream(File file，boolean append)
     * FileOutputStream(String path,boolean append)
     * 若第二个参数为true,则是追加写模式，即：若该文件存在，保留该文件所有数据，将通过当前流写出的数据追加到文件末尾。
     *
     * 做不到像RAF那样操作指针，覆盖部分内容的操作。
     * @author cyk
     */
    public  void fileStream() throws IOException {

        // 文件数据写出
        FileOutputStream fileOutputStream = new FileOutputStream("./fileStream.stream",true);
        byte[] data = "通过文件输出流写出成功！".getBytes("UTF-8");  // 指定编码格式,转换成Byte
        fileOutputStream.write(data);    // 写出数据到文件
        fileOutputStream.close();        // 关闭输出流

        // 文件数据读取
        FileInputStream fileInputStream = new FileInputStream("./fileStream.stream");
        data = new byte[100];     // 设置100子节数组
        int len = fileInputStream.read(data);  // 读取data长度的数据，len实际读取到的数据长度
        String dataStr = new String(data,0,len,"UTF-8");  // 将读取到的数据按指定位置指定字符集转换为String
        fileInputStream.close();  // 关闭输入流

    }

    /**
     * 使用文件流复制文件
     * @param source
     * @param target
     * @author cyk
     */
    public void copyFileByFileStream(File source,File target){
        try (FileInputStream fileInputStream = new FileInputStream(source);
             FileOutputStream fileOutputStream = new FileOutputStream(target,true);
             ){
            byte[] date = new byte[1024*10];  // 数据复制速度，即读取写入byte数组长度
            int len = -1;
            while((len = fileInputStream.read(date))!=-1){
                fileOutputStream.write(date,0,len); // 指定数据长度，并写出到文件
            }
        }catch(IOException id){
            System.out.println("文件 《 "+source.getName()+" 》 复制失败！");
        }
        System.out.println("文件 《 "+source.getName()+" 》 复制成功！");
    }

    /**
     * 缓冲流
     * java.io.BufferedOutputStream
     * java.io.BufferedInputStream
     *
     * 缓冲流是一对高级流，可以加快读写数据的效率。无论我们通过缓冲流进行的是随机读写还是块读写，
     * 最终都会被缓冲流转换为块读写操作。
     * @author cyk
     */
    public void bufferedStream() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("bufferedStream.stream");
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        byte[] data = "缓冲输出流数据输出！".getBytes("UTF-8");
        bufferedOutputStream.write(data);

        /*
         * void flush()
         * 缓冲流提供了强制清空缓冲区的操作，调用flush
         * 会将当前缓冲区已经缓冲的字节一次性写出。
         */
        bufferedOutputStream.flush();
        bufferedOutputStream.close(); // 关闭缓冲输出流即可

        FileInputStream fileInputStream = new FileInputStream("bufferedStream.stream");
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        int d = bufferedInputStream.read();  // 缓冲流读取数据
        bufferedInputStream.close();
    }

    /**
     * 对象流
     * java.io.ObjectOutputStream
     * java.io.ObjectInputStream
     * 对象流是一对高级流，可以方便的读写java中任何对象。
     *
     * 当一个类的实例希望被对象流读写，那么该类必须实现：java.io.Serializable接口,否则在序列化时会抛出异常
     * 当我们实现Serializable接口后，该类被编译器编译时会多出一个方法，作用是将当前实例转换为一组字节。
     * 但是源码中不体现，由于不实现该接口，编译器不会给类添加这个方法，这就是为什么对象流在序列化时要求必须
     * 实现该接口的原因。否则无法将对象转换为一组字节。
     *
     * @author cyk
     */
    public void objectStream() throws IOException, ClassNotFoundException {
        /*
         * 当一个类实现了Serializable接口后，应当定义一个常量：serialVersionUID   序列化版本号
         * 序列化版本号直接影响反序列化对象的结果是否成功
         *
         * 当对象输入流反序列化一个对象时，会检测该对象的版本号是否与当前版本号一致，若不一致则直接抛出
         * 反序列化失败异常。若版本号一致，则采用兼容模式，
         * 即：若反序列化的对象与当前类结构不完全一致时，所有可以还原的属性都还原。若结构一致则直接还原。
         *
         * 若我们没有定义序列化版本号，则编译器在编译当前类时会根据结构生成一个版本号，弊端是只要当前类的
         * 结构发生改变，版本号一定改变。
         *
         * 当一个属性被transient修饰后，那么当前类实例在序列化时，该属性值会被忽略。
         * 忽略不需要保存的属性可以达到对象瘦身的功能。
         */
        String objectStr= "对象流操作！";
        FileOutputStream fileOutputStream = new FileOutputStream("Object.stream");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        /*
         * 对象输出流提供了写出对象的方法： void writeObject(Object obj)
         * 将给定的对象转换为一组字节后写出
         *
         * 这里通过oos将给定对象写入文件，实际经历了
         * 两个操作：
         * 1.objectOutputStream将给定的对象转换为了一组字节
         *      这个过程称为：对象序列化
         * 2.fileOutputStream将这组字节写入到文件中（硬盘上）
         *      这个过程称为：数据持久化
         */
        objectOutputStream.writeObject(objectStr);
        objectOutputStream.close();

        FileInputStream fileInputStream = new FileInputStream("Object.stream");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        /*
         * 对象输入流提供了对象反序列化方法： Object readObject()
         * 需要注意，该方法在读取字节时，必须保证读取到的字节是对象输出流将一个对象序列化的字节，否
         * 则会抛出ClassNotFoundException
         */
        Object obj = objectInputStream.readObject();
        objectInputStream.close();
    }

    /**
     * 字符流
     * java按照读写单位将流划分为：字节流与字符流
     *
     * 字符流的读写单位是以字符为单位的。但底层实际还是要转换为字节进行写操作，
     * 或者将读取到的字节转换为字符。本质还是读写字节。
     *
     * 转换流：
     * java.io.OutputStreamWriter
     * java.io.InputStreamReader
     * 它们是字符流的一对常用实现类，将来实际开发中很少直接使用它们，但它们在流链接中是重要的一环。
     * @author cyk
     */
    public void characterStream() throws IOException {
        // 创建字符输出流
        FileOutputStream fileOutputStream = new FileOutputStream("Character.stream");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream,"UTF-8");
        outputStreamWriter.write("摩羯一生，只有两件事，关你屁事和关我屁事。");
        outputStreamWriter.close();

        // 创建字符输入流
        FileInputStream fileInputStream = new FileInputStream("Character.stream");
        InputStreamReader  inputStreamReader = new InputStreamReader(fileInputStream,"UtF-8");
        char character = (char) inputStreamReader.read();
        inputStreamReader.close();
    }

    /**
     * 缓冲字符流
     * java.io.BufferedWriter
     * java.io.BufferedReader
     * 缓冲字符流可以块读写，并且特点是可以按行读写字符串
     *
     * java.io.PrintWriter
     * 具有自动行刷新的缓冲字符输出流。内部连接BufferedWriter作为缓冲功能。
     * @author cyk
     */
    public void bufferedCharacterStream() throws IOException {
        /*
         * PrintWriter 流连接过程
         * 在创建PrintWriter时，若构造方法第一个参数为流（字节流字符流均可），那么该构造方法就支持一个重载，可以再传入
         * 第二个参数，该参数为boolean型，若值为true时，则具有了自动行刷新功能。即：每当调用println方法（注意，不是调
         * 用print方法！！！）后就会自动刷新，将该字符串实际写出。但是该功能会降低写效率（因为提高了写次数），但是若考虑
         * 到即时性，该方法很易用。
         */
        FileOutputStream fileOutputStream = new FileOutputStream("Character.stream");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream,"UTF-8");
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        PrintWriter printWriter = new PrintWriter(bufferedWriter);
        printWriter.println("使用字符输出流直接输出字符了！");
        printWriter.close();

        // 直接使用PrintWriter构建字符输出流
        printWriter = new PrintWriter("PrintWriter.stream","UTF-8");
        printWriter.println("直接使用PrintWriter构建字符输出流对象！");
        printWriter.close();

        /*
         * BufferedReader提供方法： String readLine（）
         * 顺序读取若干字符，直到读取了换行符为止，然后 将换行符之前的所有字符以一个字符串形式返回。
         * 注意，返回的字符串中是不含有最后的换行符的。 若返回值为null，表示末尾。
         */
        FileInputStream fileInputStream = new FileInputStream("Character.stream");
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = null;
        while((line = bufferedReader.readLine())!=null){
            System.out.println(line);
        }
        bufferedReader.close();
    }

    /**
     * 控制台记事本小工具，利用字符流实现
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void notepadByCharacterStream() throws FileNotFoundException, UnsupportedEncodingException {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入文件名：");
        String name = sc.nextLine();
        FileOutputStream fos = new FileOutputStream(name);
        OutputStreamWriter osw = new OutputStreamWriter(fos,"utf-8");
        BufferedWriter bw = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bw,true);
        System.out.println("请输入内容：");
        while(true) {
            String wz = sc.nextLine().toLowerCase().trim();
            if("exit".equals(wz)) {
                System.out.println("退出成功。");
                break;
            }
            pw.println(wz);
        }
        pw.close();
    }
}
