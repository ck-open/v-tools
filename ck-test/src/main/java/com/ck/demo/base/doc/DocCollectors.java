
/*java.util.stream.Collectors  实现各种有用的缩减操作的Collector的实现，例如将元素累积到集合中，根据各种标准汇总元素等。

方法目录
    1. averagingDouble
        averagingDouble方法返回一个Collector收集器，它生成应用于输入元素的double值函数的算术平均值。如果没有元素，则结果为0。
        返回的平均值可能会因记录值的顺序而变化，这是由于除了不同大小的值之外，还存在累积舍入误差。
        通过增加绝对量排序的值(即总量，样本越大，结果越准确)往往会产生更准确的结果。如果任何记录的值是NaN或者总和在任何点NaN，那么平均值将是NaN。
        注意： double格式可以表示-253到253范围内的所有连续整数。如果管道有超过253的值，则平均计算中的除数将在253处饱和，从而导致额外的数值误差。
            // 所有水果单价平均值
            Double avgPrice = fruitList.stream().collect(Collectors.averagingDouble(Fruit::getPrice));

    2. collectingAndThen
        collectingAndThen方法调整Collector收集器以执行其它的结束转换。
        例如，可以调整toList()收集器，以始终生成一个不可变的列表：
            // 生成一个不可变的列表
            List<Fruit> unmodifiableList = fruitList.stream().collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
            // 以指定字符串 ==>25 输出所有水果数量
            String strNumber = fruitList.stream().collect(Collectors.collectingAndThen(Collectors.summingInt(Fruit::getNumber), i->"==>"+i));

    3. counting
        counting方法返回一个Collector收集器接受T类型的元素，用于计算输入元素的数量。如果没有元素，则结果为0。
            // 打印水果总数
            Optional.of(fruitList.stream().collect(Collectors.counting())).ifPresent(System.out::println);

    4. groupingBy
        4.1 groupingBy(Function)
            groupingBy(Function)方法返回一个Collector收集器对T类型的输入元素执行"group by"操作，根据分类函数对元素进行分组，并将结果返回到Map。
            分类函数将元素映射到某些键类型K。收集器生成一个Map，其键是将分类函数应用于输入元素得到的值，其对应值为List，其中包含映射到分类函数下关联键的输入元素。
            无法保证返回的Map或List对象的类型，可变性，可序列化或线程安全性。

            注意： 返回的Collector收集器不是并发的。对于并行流管道，combiner函数通过将键从一个映射合并到另一个映射来操作，这可能是一个昂贵的操作。
                    如果不需要保留元素出现在生成的Map收集器中的顺序，则使用groupingByConcurrent(Function)可以提供更好的并行性能。
                // 根据水果名称分组
                Map<String,List<Fruit>> mapByOne = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName));


        4.2 groupingBy(Function, Collector)
            groupingBy(Function, Collector)方法返回一个Collector收集器，对T类型的输入元素执行级联"group by"操作，
            根据分类函数对元素进行分组，然后使用指定的下游Collector收集器对与给定键关联的值执行缩减操作。
            分类函数将元素映射到某些键类型K。下游收集器对T类型的元素进行操作，并生成D类型的结果。产生收集器生成Map<K, D>。
            返回的Map的类型，可变性，可序列化或线程安全性无法保证。

            注意： 返回的Collector收集器不是并发的。对于并行流管道，combiner函数通过将键从一个映射合并到另一个映射来操作，这可能是一个昂贵的操作。
                    如果不需要保留向下游收集器提供元素的顺序，则使用groupingByConcurrent(Function, Collector)可以提供更好的并行性能。
                // 每种水果价格列表
                Map<String, List<Double>> groupMap =fruitList.stream().collect(Collectors.groupingBy(Fruit::getName, Collectors.mapping(Fruit::getPrice, Collectors.toList())));

        4.3 groupingBy(Function, Supplier, Collector)
            groupingBy(Function, Supplier, Collector)方法返回一个Collector收集器，对T类型的输入元素执行级联"group by"操作，根据分类函数对元素进行分组，
            然后使用指定的下游Collector收集器对与给定键关联的值执行缩减操作。收集器生成的Map是使用提供的工厂函数创建的。
            分类函数将元素映射到某些键类型K。下游收集器对T类型的元素进行操作，并生成D类型的结果。产生收集器生成Map<K, D>。

            注意： 返回的Collector收集器不是并发的。对于并行流管道，combiner函数通过将键从一个映射合并到另一个映射来操作，这可能是一个昂贵的操作。
            如果不需要保留向下游收集器提供元素的顺序，则使用groupingByConcurrent(Function, Supplier, Collector)可以提供更好的并行性能。
                // 分组并计算水果均价 返回TreeMap类型结果
                Map<String,Double> fruitAvgPrice = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName,TreeMap::new,Collectors.averagingDouble(Fruit::getPrice)));

    5. groupingByConcurrent
        5.1 groupingByConcurrent(Function)
            groupingByConcurrent(Function)方法返回一个并发Collector收集器对T类型的输入元素执行"group by"操作，根据分类函数对元素进行分组。
            这是一个Collector.Characteristics#CONCURRENT并发和Collector.Characteristics#UNORDERED无序收集器。
            分类函数将元素映射到某些键类型K。收集器生成一个ConcurrentMap<K, List>，其键是将分类函数应用于输入元素得到的值，其对应值为List，其中包含映射到分类函数下关联键的输入元素。
            无法保证返回的Map或List对象的类型，可变性或可序列化，或者返回的List对象的线程安全性。

        5.2 groupingByConcurrent(Function, Collector)
            groupingByConcurrent(Function, Collector)方法返回一个并发Collector收集器，对T类型的输入元素执行级联"group by"操作，
            根据分类函数对元素进行分组，然后使用指定的下游Collector收集器对与给定键关联的值执行缩减操作。
            这是一个Collector.Characteristics#CONCURRENT并发和Collector.Characteristics#UNORDERED无序收集器。
            分类函数将元素映射到某些键类型K。下游收集器对T类型的元素进行操作，并生成D类型的结果。产生收集器生成Map<K, D>。

        5.3 groupingByConcurrent(Function, Supplier, Collector)
            groupingByConcurrent(Function, Supplier, Collector)方法返回一个并行Collector收集器，对T类型的输入元素执行级联"group by"操作，
            根据分类函数对元素进行分组，然后使用指定的下游Collector收集器对与给定键关联的值执行缩减操作。收集器生成的ConcurrentMap是使用提供的工厂函数创建的。
            这是一个Collector.Characteristics#CONCURRENT并发和Collector.Characteristics#UNORDERED无序收集器。
            分类函数将元素映射到某些键类型K。下游收集器对T类型的元素进行操作，并生成D类型的结果。产生收集器生成Map<K, D>。

    6. joining
        6.1 joining()
            joining()方法返回一个Collector收集器，它按遇见顺序将输入元素连接成String。
                // 拼接起来的水果名称
                String fruitNames = fruitList.stream().map(Fruit::getName).collect(Collectors.joining());

        6.2 joining(delimiter)
            joining(delimiter)方法返回一个Collector收集器，它以遇见顺序连接由指定分隔符分隔的输入元素。
                // 拼接起来的水果名称 用逗号分隔的
                String fruitNames = fruitList.stream().map(Fruit::getName).collect(Collectors.joining(","));

        6.3 joining(delimiter, prefix, suffix)
            joining(delimiter, prefix, suffix)方法返回一个Collector收集器，它以遇见顺序将由指定分隔符分隔的输入元素与指定的前缀和后缀连接起来。
                // 拼接起来的水果名称 用逗号分隔的 增加前后缀
                String fruitNames = fruitList.stream().map(Fruit::getName).collect(Collectors.joining(",","FruitName[","]"));

    7. mapping
        mapping方法通过在累积之前将映射函数应用于每个输入元素，将Collector收集器接受U类型的元素调整为一个接受T类型的元素。
            Map<String,String> fruitPrices = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName
                            ,Collectors.mapping(i->i.getPrice().toString(),Collectors.joining(",","Prices:[","]"))));

    8. maxBy
        maxBy方法返回一个Collector收集器，它根据给定的Comparator比较器生成最大元素，描述为Optional。
            // 查询最贵的水果
            Fruit maxPrice = fruitList.stream().collect(Collectors.maxBy(Comparator.comparingDouble(Fruit::getPrice))).orElse(null);
            // 可直接使用max
            Fruit maxPrice = fruitList.stream().max(Comparator.comparingDouble(Fruit::getPrice)).orElse(null);

    9. minBy
        minBy方法返回一个Collector收集器，它根据给定的Comparator比较器生成最小元素，描述为Optional。
            // 查询便宜的水果
            Fruit minPrice = fruitList.stream().collect(Collectors.minBy(Comparator.comparingDouble(Fruit::getPrice))).orElse(null);
            // 可直接使用min
            Fruit minPrice = fruitList.stream().min(Comparator.comparingDouble(Fruit::getPrice)).orElse(null);

    10. partitioningBy
        10.1 partitioningBy(Predicate)
            partitioningBy(Predicate)方法返回一个Collector收集器，它根据Predicate对输入元素进行分区，并将它们组织成Map。
            返回的Map的类型，可变性，可序列化或线程安全性无法保证。
                // 按热带水果分区
                Map<Boolean,List<Fruit>> fruitTropic = fruitList.stream().collect(Collectors.partitioningBy(Fruit::getTropic));

        10.2 partitioningBy(Predicate, Collector)
            partitioningBy(Predicate, Collector)方法返回一个Collector收集器，它根据Predicate对输入元素进行分区，根据另一个Collector收集器减少每个分区中的值，并将它们组织成Map，其值是下游减少的结果。
            返回的Map的类型，可变性，可序列化或线程安全性无法保证。
                // 热带水果分区并计算水果均价
                Map<Boolean,Double> fruitTropicPrice = fruitList.stream().collect(Collectors.partitioningBy(Fruit::getTropic,Collectors.averagingDouble(Fruit::getPrice)));

    11. reducing
        11.1 reducing(BinaryOperator)
            返回一个Collector收集器，它在指定的BinaryOperator下执行其输入元素的缩减。结果被描述为Optional。
            reducing()相关收集器在groupingBy或partitioningBy下游的多级缩减中使用时非常有用。要对流执行简单缩减，请使用Stream#reduce(BinaryOperator)。
                // 获取最贵的水果
                Fruit maxPrice = fruitList.stream().collect(Collectors.reducing(BinaryOperator.maxBy(Comparator.comparingDouble(Fruit::getPrice)))).orElseGet(null);
                // 可直接使用reduce
                Fruit maxPrice = fruitList.stream().reduce(BinaryOperator.maxBy(Comparator.comparingDouble(Fruit::getPrice))).orElseGet(null);

        11.2 reducing(Object, BinaryOperator)
            返回一个Collector收集器，它使用提供的标识在指定的BinaryOperator下执行其输入元素的缩减。
            reducing()相关收集器在groupingBy或partitioningBy下游的多级缩减中使用时非常有用。要对流执行简单缩减，请使用Stream#reduce(Object, BinaryOperator)。
                // 统计水果总数量
                Integer sumPrice = fruitList.stream().map(Fruit::getNumber).collect(Collectors.reducing(0,(i, x)->i+x));
                // 可直接使用reduce
                Integer sumPrice = fruitList.stream().map(Fruit::getNumber).reduce(0, (i, x) -> i + x);
                Integer sumPrice = fruitList.stream().map(Fruit::getNumber).reduce(0, Integer::sum);

        11.3 reducing(Object, Function, BinaryOperator)
            返回一个Collector收集器，它在指定的映射函数和BinaryOperator下执行其输入元素的缩减。这是对reducing(Object, BinaryOperator)的概括，它允许在缩减之前转换元素。
            reducing()相关收集器在groupingBy或partitioningBy下游的多级缩减中使用时非常有用。要对流执行简单缩减，请使用Stream#map(Function)和Stream#reduce(Object, BinaryOperator)。
                // 统计水果总数量
                Integer sumPrice = fruitList.stream().collect(Collectors.reducing(0,Fruit::getNumber,(i, x)->i+x));

    12. summarizingDouble
        summarizingDouble方法返回一个Collector收集器，它将double生成映射函数应用于每个输入元素，并返回结果值的摘要统计信息。
            // 统计水果总量，单价总和，最高、最低
            DoubleSummaryStatistics result = fruitList.stream().collect(Collectors.summarizingDouble(Fruit::getPrice));

    13. summingDouble
        返回一个Collector收集器，它生成应用于输入元素的double值函数的总和。如果没有元素，则结果为0。
        返回的总和可能会因记录值的顺序而变化，这是由于除了不同大小的值之外，还存在累积舍入误差。
        通过增加绝对量排序的值(即总量，样本越大，结果越准确)往往会产生更准确的结果。如果任何记录的值是NaN或者总和在任何点NaN，那么总和将是NaN。
            // 统计单价总和
            Double sumPrice = fruitList.stream().collect(Collectors.summingDouble(Fruit::getPrice));
            // 可直接使用mapToDouble
            Double sumPrice = fruitList.stream().mapToDouble(Fruit::getPrice).sum();

    14. toCollection
        返回一个Collector收集器，它按遇见顺序将输入元素累积到一个新的Collection收集器中。Collection收集器由提供的工厂创建。
            // 获取所有热带水果集合
            List<Fruit> fruitTropic = fruitList.stream().filter(Fruit::getTropic).collect(Collectors.toCollection(LinkedList::new));

    15. toConcurrentMap
        15.1 toConcurrentMap(Function, Function)
            返回一个并发的Collector收集器，它将元素累积到ConcurrentMap中，其键和值是将提供的映射函数应用于输入元素的结果。
            如果映射的键包含重复项(根据Object#equals(Object))，则在执行收集操作时会抛出IllegalStateException。如果映射的键可能有重复，请使用toConcurrentMap(Function, Function, BinaryOperator)。
            注意： 键或值作为输入元素是常见的。在这种情况下，实用方法java.util.function.Function#identity()可能会有所帮助。
            这是一个Collector.Characteristics#CONCURRENT并发和Collector.Characteristics#UNORDERED无序收集器。
                // 统计每种水果单价
                Map<String,Double> fruitPrice = fruitList.stream().collect(Collectors.toConcurrentMap(Fruit::getName,Fruit::getPrice));
                // 根据水果名称分组，每个元素返回一个对象
                Map<String,Fruit> mapByOne = fruitList.stream().collect(Collectors.toMap(Fruit::getName, Function.identity()));

        15.2 toConcurrentMap(Function, Function, BinaryOperator)
            返回一个并发的Collector收集器，它将元素累积到ConcurrentMap中，其键和值是将提供的映射函数应用于输入元素的结果。
            如果映射的键包含重复项(根据Object#equals(Object))，则将值映射函数应用于每个相等的元素，并使用提供的合并函数合并结果。
            注意： 有多种方法可以处理映射到同一个键的多个元素之间的冲突。toConcurrentMap的其它形式只是使用无条件抛出的合并函数，但你可以轻松编写更灵活的合并策略。
            例如，如果你有一个Person流，并且你希望生成一个“电话簿”映射名称到地址，但可能有两个人具有相同的名称，你可以按照以下方式进行优雅的处理这些冲突，并生成一个Map将名称映射到连接的地址列表中：
                    Map<String, String> phoneBook = people.stream().collect(toConcurrentMap(Person::getName, Person::getAddress,(s, a) -> s + ", " + a));
            这是一个Collector.Characteristics#CONCURRENT并发和Collector.Characteristics#UNORDERED无序收集器。
                // 统计每种水果单价  当两种水果重名时 将价格相加
                Map<String,Double> fruitPrice = fruitList.stream().collect(Collectors.toConcurrentMap(Fruit::getName,Fruit::getPrice,(i,x)->i+x));

        15.3 toConcurrentMap(Function, Function, BinaryOperator, Supplier)
            返回一个并发的Collector收集器，它将元素累积到ConcurrentMap中，其键和值是将提供的映射函数应用于输入元素的结果。
            如果映射的键包含重复项(根据Object#equals(Object))，则将值映射函数应用于每个相等的元素，并使用提供的合并函数合并结果。ConcurrentMap由提供的供应商函数创建。
            这是一个Collector.Characteristics#CONCURRENT并发和Collector.Characteristics#UNORDERED无序收集器。
                // 统计每种水果单价  当两种水果重名时 将价格相加  并指定返回的Map类型
                Map<String,Double> fruitPrice = fruitList.stream().collect(Collectors.toConcurrentMap(Fruit::getName,Fruit::getPrice,(i,x)->i+x, ConcurrentSkipListMap::new));

    16. toList
        返回一个Collector收集器，它将输入元素累积到一个新的List中。返回的List的类型，可变性，可序列化或线程安全性无法保证;如果需要更多地控制返回的List，请使用toCollection(Supplier)。
            // 获取所有热带水果集合
            List<Fruit> fruitTropic = fruitList.stream().filter(Fruit::getTropic).collect(Collectors.toList());

    17. toMap
        17.1toMap(Function, Function)
            返回一个Collector收集器，它将元素累积到Map中，其键和值是将提供的映射函数应用于输入元素的结果。
            如果映射的键包含重复项(根据Object#equals(Object))，则在执行收集操作时会抛出IllegalStateException。如果映射的键可能有重复，请使用toMap(Function, Function, BinaryOperator)。
            注意： 键或值作为输入元素是常见的。在这种情况下，实用方法java.util.function.Function#identity()可能会有所帮助。
            返回的Collector收集器不是并发的。对于并行流管道，combiner函数通过将键从一个映射合并到另一个映射来操作，这可能是一个昂贵的操作。
            如果不需要将结果以遇见的顺序插入Map，则使用toConcurrentMap(Function, Function)可以提供更好的并行性能。

        17.2 toMap(Function, Function, BinaryOperator)
            返回一个并发的Collector收集器，它将元素累积到Map中，其键和值是将提供的映射函数应用于输入元素的结果。
            如果映射的键包含重复项(根据Object#equals(Object))，则将值映射函数应用于每个相等的元素，并使用提供的合并函数合并结果。
            注意： 有多种方法可以处理映射到同一个键的多个元素之间的冲突。toMap的其它形式只是使用无条件抛出的合并函数，但你可以轻松编写更灵活的合并策略。
            例如，如果你有一个Person流，并且你希望生成一个“电话簿”映射名称到地址，但可能有两个人具有相同的名称，你可以按照以下方式进行优雅的处理这些冲突，并生成一个Map将名称映射到连接的地址列表中：
                    Map<String, String> phoneBook = people.stream().collect(toConcurrentMap(Person::getName, Person::getAddress, (s, a) -> s + ", " + a));
            返回的Collector收集器不是并发的。对于并行流管道，combiner函数通过将键从一个映射合并到另一个映射来操作，这可能是一个昂贵的操作。
            如果不需要将结果以遇见的顺序插入Map，则使用toConcurrentMap(Function, Function, BinaryOperator)可以提供更好的并行性能。

        17.3 toMap(Function, Function, BinaryOperator, Supplier)
            返回一个并发的Collector收集器，它将元素累积到Map中，其键和值是将提供的映射函数应用于输入元素的结果。
            如果映射的键包含重复项(根据Object#equals(Object))，则将值映射函数应用于每个相等的元素，并使用提供的合并函数合并结果。Map由提供的供应商函数创建。
            注意： 返回的Collector收集器不是并发的。对于并行流管道，combiner函数通过将键从一个映射合并到另一个映射来操作，这可能是一个昂贵的操作。
            如果不需要将结果以遇见的顺序插入Map，则使用toConcurrentMap(Function, Function, BinaryOperator, Supplier)可以提供更好的并行性能。

    18. toSet
        返回一个Collector收集器，它将输入元素累积到一个新的Set中。返回的Set的类型，可变性，可序列化或线程安全性无法保证;如果需要更多地控制返回的Set，请使用toCollection(Supplier)。
        这是一个Collector.Characteristics#UNORDERED无序收集器。
            // 获取所有热带水果集合
            List<Fruit> fruitTropic = fruitList.stream().filter(Fruit::getTropic).collect(Collectors.toSet());
*/


package com.ck.demo.base.doc;
public class DocCollectors{
    public static void main(String[] args) {

    }
}

