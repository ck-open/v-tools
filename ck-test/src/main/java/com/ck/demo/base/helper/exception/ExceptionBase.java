package com.ck.base.helper.exception;


import java.io.FileOutputStream;

public class ExceptionBase {

    /**
     * java异常处理机制中的try-catch
     * 语法：
     * try{ 代码片段 }catch(XXException e){ 处理XXException的代码片段 }
     * 或:
     * try{  ... }finally{ ... }
     *
     * @author cyk
     */
    public void tryCatch() {
        System.out.println("程序开始了");
        try {
            String str = "a";
            System.out.println(Integer.parseInt(str));  // 在try语句块中出错的代码以下的内容都不会被执行
        } catch (NullPointerException e) {
            // 如果try语句块中的代码没有出现异常，那么catch语句块是不执行的
            System.out.println("出现空指针！");
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("字符串下标越界了！");
        } catch (Exception e) {
            // 应当养成一个好习惯，在最后一个catch上捕获Exception，避免因为try代码块中一个未捕获的异常导致程序中断。
            System.out.println("未知错误！");
        }
        System.out.println("程序结束了");
    }

    /**
     * finally块
     * finally块只能定义在异常处理机制的最后。即：直接跟在try之后或者最后一个catch之后。
     * finally块可以保证只要程序执行到try语句块中，那么finally块中的代码必定执行。
     * 所以通常将无关乎程序是否异常都要执行的代码放在这里，例如IO操作完毕后的流关闭。
     *
     * @author cyk
     */
    public void finallyDemo() {
        /*
         * finally常见面试题
         *
         * 请简述：final,finally,finalize
         * final:类、方法、变量修饰符，表示唯一，类不能被继承、方法不能被重写、变量不能修改
         * finally:异常处理机制的一部分，可以保证只要程序执行到try语句块中，那么finally块中的代码必定执行。
         * finalize:是Object定义的方法。当GC要释放一个对象时会调用该方法。该方法执行完毕意味着其即将被释放。该方法不应当有耗时操作。
         */
        System.out.println("程序开始了");
        try {
            String str = "";
            System.out.println(str.length());
            return;
        } catch (Exception e) {
            System.out.println("未知错误！");
        } finally {
            System.out.println("finally中的代码运行了！");
        }
        System.out.println("程序结束了");
    }

    /**
     * 在JDK7之后，推出了一个新的特性：自动关闭资源。
     *
     * @author cyk
     */
    public void autoClose() {
        /*
         * 实现了AutoCloseable接口的定义在这里，
         * 最终可以被自动关闭
         * 实际上下面的代码会被编译器在编译时修改为在Finally中释放资源
         */
        try (
                FileOutputStream fos = new FileOutputStream("fos.dat");
        ) {
            fos.write(1);
        } catch (Exception e) {
            System.out.println("出错了！");
        }
    }

    /**
     * 异常的抛出
     * 当调用一个含有throws声明异常抛出的方法时必须处理该异常，否则编译不通过。
     * 处理异常的方法有两种：
     * 1、使用try-catch捕获并处理该异常
     * 2、在当前方法上继续使用throws声明该异常的抛出。
     *
     * 子类在重写父类含有throws声明异常抛出的方法时对throws的重写规则如下:
     * 1、仅抛出部分异常
     * 2、不再抛出任何异常
     * 3、可抛出父类方法抛出异常的子类型异常
     * 4、不允许抛出额外异常
     * 5、不允许抛出父类方法抛出异常的父类型异常
     *
     * 使用throw在方法中抛出一个异常逻辑通常在以下情况发生：
     * 1、符合语法，但是不符合业务逻辑时，可以当成异常抛出给调用者。
     * 2、确实发生了一个异常，但是该异常不应当被当前方法解决，这时可以抛出给调用者。
     * @author cyk
     */
    public void ThrowDemo() throws customException {
        /*
         * 通常方法中使用throw什么异常，就要在方法声明的同时使用throws声明该异常的抛出。
         * 只有RuntimeException及其子类异常不需要。
         */
        throw new customException("自定义异常抛出！");
    }

}

/**
 * 自定义异常
 * 自定义异常通常用来说明如业务逻辑错误的情况。
 *
 * @author cyk
 *
 */
class customException extends Exception{
    private static final long serialVersionUID = 1L;

    public customException() {
        super();
    }

    public customException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public customException(String message, Throwable cause) {
        super(message, cause);
    }

    public customException(String message) {
        super(message);
    }

    public customException(Throwable cause) {
        super(cause);
    }
}