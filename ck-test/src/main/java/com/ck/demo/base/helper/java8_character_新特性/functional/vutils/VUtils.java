package com.ck.base.helper.java8_character_新特性.functional.vutils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Consumer;

public class VUtils {
    public static void main(String[] args) {
//        VUtils.isTrue(true).throwMessage("函数式IF异常抛出");

        VUtils.isTrueOrFalse(true).trueOrFalseHandle(() -> System.out.println("True, 开始秀了"), () -> System.out.println("False, 秀不动了，快跑"));

        VUtils.isBlankOrNoBlank("Hello").presentOrElseHandle(System.out::println, () -> System.out.println("字符串为空"));

        VUtils.isCustomException(true,CustomException.class).throwMessage("就是想抛异常，自己定义的");
    }

    /**
     * 如果参数为true抛出异常
     *
     * @param b
     * @return
     */
    public static ThrowRuntimeExceptionFun isRuntimeException(boolean b) {
        return (errorMessage) -> {
            if (b) {
                throw new RuntimeException(errorMessage);
            }
        };
    }

    /**
     * 如果参数为true抛出异常
     *
     * @param b
     * @return
     */
    public static ThrowRuntimeExceptionFun isCustomException(boolean b, Class<? extends RuntimeException> customException) {
        if (customException == null) isRuntimeException(true).throwMessage("未指定将要抛出的自定义异常类型");
        return (errorMessage) -> {
            if (b) {
                try {
                    Constructor cs = customException.getConstructor(String.class);
                    RuntimeException exception = (RuntimeException) cs.newInstance(errorMessage);
                    throw exception;
                } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    isRuntimeException(true).throwMessage("自定义异常抛出失败  异常：" + e.getMessage() + "  抛出的异常信息：" + errorMessage);
                }
            }
        };
    }

    /**
     * 参数为true或false时，分别进行不同的操作
     *
     * @param b
     * @return
     */
    public static BranchHandle isTrueOrFalse(boolean b) {
        return (trueHandle, falseHandle) -> {
            if (b) {
                trueHandle.run();
            } else {
                falseHandle.run();
            }
        };
    }

    /**
     * 参数为空或不为空分别执行不同的操作
     *
     * @param str
     * @return
     */
    public static PresentOrElseHandler<?> isBlankOrNoBlank(String str) {
        return (action, emptyAction) -> {
            if (str == null || str.length() == 0) emptyAction.run();
            else action.accept(str);
        };
    }


    /**
     * 抛出异常接口
     */
    @FunctionalInterface
    public interface ThrowRuntimeExceptionFun {
        /**
         * 抛出异常信息
         *
         * @param message 异常信息
         */
        void throwMessage(String message);
    }

    /**
     * 分支处理接口
     */
    @FunctionalInterface
    public interface BranchHandle {
        /**
         * 分支操作
         *
         * @param trueHandle  为true时要进行的操作
         * @param falseHandle 为false时要进行的操作
         */
        void trueOrFalseHandle(Runnable trueHandle, Runnable falseHandle);
    }

    /**
     * 空值与非空值分支处理
     *
     * @param <T>
     */
    @FunctionalInterface
    public interface PresentOrElseHandler<T extends Object> {
        /**
         * 值不为空时执行消费操作
         * 值为空时执行其他的操作
         *
         * @param action
         * @param emptyAction
         */
        void presentOrElseHandle(Consumer<? super T> action, Runnable emptyAction);
    }


    public static class CustomException extends RuntimeException{
        public CustomException(String message){
            super(message);
        }
    }
}
