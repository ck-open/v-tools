package com.ck.base.helper.java8_character_新特性.lambda;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 使用Lambda日志案例
 * Lambda的特点：延迟加载
 * Lambda的使用必须存在函数式接口
 */
public class LambdaDemo {

    // 定义一个显示日志的方法，方法的参数传递日志的等级和MessageBuilder接口
    public static void showLog(int level, MessageBuilder messageBuilder){
        // 对日志的等级进行判断，如果是1级，则调用MessageBuilder中的BuilderMassage方法
        if (level == 1){
            System.out.println(messageBuilder.builderMessage());
        }
    }

    // 启动多线程
    public static void startThread(Runnable runnable){
        new Thread(runnable).start();
    }

    /**
     * 如果一个方法返回值类型是一个函数式接口，那么可以直接返回一个Lambda表达式。
     * 当需要通过一个方法获取一个java.util.Comparator接口类型的对象作为排序器时，就可以调用该方法获取。
     * @return
     */
    public static Comparator<String> getComparator(){
        // 方法的返回值类型是一个接口，那么可以返回这个接口的匿名内部类
//        return new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                // 按照字符串的降序
//                return o2.length()-o1.length();
//            }
//        };

        // 方法的返回值类型是一个函数式接口，可以返回一个Lambda表达式
        return ((o1, o2) -> o2.length()-o1.length());
    }

    public static void main(String[] args) {
        // 定义三个日志信息
        String msg1 = "Hello";
        String msg2 = "World";
        String msg3 = "Java";

        /*
         * 调用showLog方法，参数MessageBuilder是个函数式接口，可以传递Lambda表达式
         *
         * 使用Lambda表达式作为参数传递，仅仅是把参数传递到showLog方法中去。
         *      只有满足条件，调用MassageBuilder中的BuilderMassage方法时才进行字符串拼接
         *      从而降低性能浪费。
         */
        showLog(1, ()->{
            System.out.println("不满足条件不执行！");
            // 返回一个拼接好的字符串
            return msg1+msg2+msg3;
        });


        // 使用匿名内部类创建线程
        startThread(new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名内部类启动线程！");
            }
        });

        startThread(()->System.out.println("使用Lambda表达式优化匿名内部类启动线程！"));


        // 创建一个字符串数组，用排序器进行排序
        String[] strings = {"sdfdsf","dsfdsf","dsfasdrew"};
        Arrays.sort(strings,getComparator());
        System.out.println(Arrays.toString(strings));

    }
}
