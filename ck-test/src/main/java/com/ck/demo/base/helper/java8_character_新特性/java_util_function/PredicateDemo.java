package com.ck.base.helper.java8_character_新特性.java_util_function;

import java.util.function.Predicate;

/**
 * java.util.function.Predicate<T>接口
 *     作用:对某种数据类型的数据进行判断,结果返回一个boolean值
 *
 *     Predicate接口中包含的抽象方法：
 *         boolean test(T t):用来对指定数据类型数据进行判断的方法
 *             结果:
 *                 符合条件,返回true
 *                 不符合条件,返回false
 *
 *         Predicate<T> and(Predicate<? super T> other)：表示两个Predicate的 与 关系,也可以用于连接两个判断条件
 *                 方法内部的两个判断条件,也是使用&&运算符连接起来的
 *                      default Predicate<T> and(Predicate<? super T> other) {
 *                          Objects.requireNonNull(other);
 *                          return (t) -> this.test(t) && other.test(t);
 *                      }
 *
 *         Predicate<T> or(Predicate<? super T> other)：表示两个Predicate的 或 关系,也可以用于连接两个判断条件
 *                 方法内部的两个判断条件,也是使用||运算符连接起来的
 *                      default Predicate<T> or(Predicate<? super T> other) {
 *                          Objects.requireNonNull(other);
 *                          return (t) -> test(t) || other.test(t);
 *                      }
 */
public class PredicateDemo {

    /**
     * 定义一个方法
     *         参数传递一个String类型的字符串
     *         传递一个Predicate接口,泛型使用String
     *         使用Predicate中的方法test对字符串进行判断,并把判断的结果返回
     * @param s
     * @param predicate
     * @return
     */
    public static boolean checkString(String s, Predicate<String> predicate){
        return predicate.test(s);
    }

    /**
     * 逻辑非
     * @param s
     * @param predicate
     * @return
     */
    public static boolean checkStringNegate(String s, Predicate<String> predicate){
        //return pre1.test(s) || pre2.test(s);
        return  predicate.negate().test(s);//等价于return pre1.test(s) || pre2.test(s);
    }

    /**
     * 逻辑与
     * @param s
     * @param pre1
     * @param pre2
     * @return
     */
    public static boolean checkStringAnd(String s, Predicate<String> pre1,Predicate<String> pre2){
        //return pre1.test(s) && pre2.test(s);
        return pre1.and(pre2).test(s);//等价于return pre1.test(s) && pre2.test(s);
    }

    /**
     * 逻辑或
     * @param s
     * @param pre1
     * @param pre2
     * @return
     */
    public static boolean checkStringOr(String s, Predicate<String> pre1, Predicate<String> pre2){
        //return pre1.test(s) || pre2.test(s);
        return  pre1.or(pre2).test(s);//等价于return pre1.test(s) || pre2.test(s);
    }

    public static void main(String[] args) {
        // 测试predicate函数的test方法
        // 调用checkString方法对字符串进行校验,参数传递字符串和Lambda表达式
        String s = "asgfds";
        boolean b = checkString(s, str->str.length()>5);
        System.out.println(b);

        // 测试predicate函数的negate方法
        //调用checkStringNegate方法,参数传递字符串和两个Lambda表达式, 判断字符串的长度是否大于5 和 字符串中是否包含a
        b = checkStringNegate(s, str->str.length()>5);
        System.out.println(b);

        // 测试predicate函数的and方法
        //调用checkStringAnd方法,参数传递字符串和两个Lambda表达式, 判断字符串的长度是否大于5 和 字符串中是否包含a
        b = checkStringAnd(s, str->str.length()>5, str->str.contains("x"));
        System.out.println(b);

        // 测试predicate函数的or方法
        //调用checkStringOr方法,参数传递字符串和两个Lambda表达式, 判断字符串的长度是否大于5 和 字符串中是否包含a
        b = checkStringOr(s, str->str.length()>5, str->str.contains("x"));
        System.out.println(b);

    }
}
