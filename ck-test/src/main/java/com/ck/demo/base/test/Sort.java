package com.ck.demo.base.test;

/**
 * 排序案例
 * @author cyk
 *
 */
public class Sort {
	/**
	 * 快速排序
	 * 
	 * @param arr
	 * @param left
	 * @param right
	 */
	public static void QuickSort(int[] arr, int left, int right) {
		int l = left;
		int r = right;
		int p = arr[left];
		while (l < r) {
			while (l < r && arr[r] >= p)
				r--;
			if (l < r) {
				int temp = arr[r];
				arr[r] = arr[l];
				arr[l] = temp;
				l++;
			}
			while (l < r && arr[l] <= p)
				l++;
			if (l < r) {
				int temp = arr[l];
				arr[l] = arr[r];
				arr[r] = temp;
				r--;
			}
		}
		if (l > left)
			QuickSort(arr, left, l);
		if (r < right)
			QuickSort(arr, r + 1, right);
	}

	/**
	 * 冒泡排序
	 * 
	 * @param arr
	 */
	public static void BubbleSort(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = 0; j < arr.length - i - 1; j++) {
				if (arr[j] > arr[j + 1]) {
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}

	}

}
