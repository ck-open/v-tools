/*
java.util.Optional<T>  这是一个包含有可选值的包装类，这意味着 Optional 类既可以含有对象也可以为空。
    是 Java 实现函数式编程的强劲一步，并且帮助在范式中实现。但是 Optional 的意义显然不止于此。

方法目录
    empty:
        public static<T> Optional<T> empty()
        创建空值的Optional对象

    of:
        public static <T> Optional<T> of(T value)
        创建执行值的Optional对象。但是不能传递null 否则将抛出NoSuchElementException。

    ofNullable:
        public static <T> Optional<T> ofNullable(T value)
        创建执行值的Optional对象。传递null则返回 empty()。

    get:
        public T get()
        从Optional实例中取回实际值对象。这个方法会在值为null的时候抛出异常 NoSuchElementException("No value present")。

    isPresent:
        public boolean isPresent()
        检查是否存在实际值对象，为null则返回false
        public void ifPresent(Consumer<? super T> consumer)
        该方法除了执行检查，还接受一个Consumer(消费者) 参数，如果对象不是空的，就对执行传入的 Lambda 表达式。
        例如：optional.ifPresent(System.out::println);

    filter:
        public Optional<T> filter(Predicate<? super T> predicate)
        按条件“过滤”值，返回测试结果为 true 的值。如果测试结果为 false，会返回一个空的Optional。
        例如：Optional fruit = optional.filter(i->i.getNumber()!=null);

    map:
        public<U> Optional<U> map(Function<? super T, ? extends U> mapper)
        对值应用(调用)作为参数的函数，然后将返回的值包装在Optional中。
        这就使对返回值进行链试调用的操作成为可能 —— 这里的下一环就是orElse()。
        例如：optional.map(Fruit::getName).orElse("无名称");

    flatMap:
        public<U> Optional<U> flatMap(Function<? super T, Optional<U>> mapper)
        对值应用(调用)作为参数的函数，返回的值不会包装在Optional中。
        如果函数获取的值为null 则抛出 NullPointerException。

    orElse:
        public T orElse(T other)
        如果optional对象有实际值则返回该值，否则返回传递给它的参数值。

    orElseGet:
        public T orElseGet(Supplier<? extends T> other)
        这个方法会在有值的时候返回值，如果没有值，它会执行作为参数传入的Supplier(供应者)函数式接口，并将返回其执行结果。
        例如：User result = Optional.ofNullable(user).orElseGet( () -> user2);

    orElseThrow:
        public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier)
        如果optional对象有实际值则返回该值，否则返回传递给它的异常子类实例。
        例如：optional.orElseThrow(NullPointerException::new);

 */

package com.ck.demo.base.doc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class DocOptional{
        public static void main(String[] args) {
                List list = Arrays.asList("a",'b','c','d');

                Optional<List> optional = Optional.empty();// 创建空值的Optional对象
                optional = Optional.of(list); // 创建执行值的Optional对象。但是不能传递null 否则将抛出NoSuchElementException。
                optional = Optional.ofNullable(list); // 创建执行值的Optional对象。传递null则返回 empty()。

                list = optional.get(); // 从Optional实例中取回实际值对象。这个方法会在值为null的时候抛出异常 NoSuchElementException("No value present")。

                System.out.println(optional.isPresent()); // 检查是否存在实际值对象，为null则返回false

                optional.ifPresent(System.out::println);  // 该方法除了执行检查，还接受一个Consumer(消费者) 参数，如果对象不是空的，就对执行传入的 Lambda 表达式。
                optional = optional.filter(i->i.get(0)!=null); // 按条件“过滤”值，返回测试结果为 true 的值。如果测试结果为 false，会返回一个空的Optional。
                Object val = optional.map(i->i.get(0)).orElse("元素1为空"); // 对值应用(调用)作为参数的函数，然后将返回的值包装在Optional中。
                val = optional.flatMap(i->Optional.ofNullable(i.get(0))); // 对值应用(调用)作为参数的函数，返回的值不会包装在Optional中。如果函数获取的值为null 则抛出 NullPointerException。
                list = optional.orElse(list); // 如果optional对象有实际值则返回该值，否则返回传递给它的参数值。
                list = optional.orElseGet(Arrays::asList); // 这个方法会在有值的时候返回值，如果没有值，它会执行作为参数传入的Supplier(供应者)函数式接口，并将返回其执行结果。
                list = optional.orElseThrow(NumberFormatException::new); // 如果optional对象有实际值则返回该值，否则返回传递给它的异常子类实例。
        }
}

