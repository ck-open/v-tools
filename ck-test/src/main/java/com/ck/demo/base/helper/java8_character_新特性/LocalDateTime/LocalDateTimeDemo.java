package com.ck.base.helper.java8_character_新特性.LocalDateTime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.Locale;

/**
 * @ClassName LocalDateTimeDemo
 * @Description LocalDateTime
 * @Author Cyk
 * @Version 1.0
 * @since 2022/7/5 15:41
 **/
public class LocalDateTimeDemo {
    public static void main(String[] args) {
        // 本地(北京)时间
        LocalDateTime time = LocalDateTime.now();
        System.out.println(time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        // 美国洛杉矶时间
        LocalDateTime time2 = LocalDateTime.now(ZoneId.of("America/Los_Angeles"));
        // 指定年月日时分
        LocalDateTime time3 = LocalDateTime.of(2020,7,5,13,50);
        // 指定年月日时分秒
        LocalDateTime time4 = LocalDateTime.of(2020,7,5,13,50,30);
        // 解析时间字符串
        LocalDateTime time5 = LocalDateTime.parse("2022-07-05T13:50:30");
        // 解析指定格式时间字符串
        LocalDateTime time6 = LocalDateTime.parse("2022-07-05 13:50:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime time7 = LocalDateTime.parse("2022-07-05 13:50:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.US));
        // 日期加5天
        LocalDateTime time8 = time.plus(5, ChronoUnit.DAYS);
        // 日期加5天
        LocalDateTime time9 = time.plusDays(7);
        // 日期减少5天
        LocalDateTime time10 = time.minus(5,ChronoUnit.DAYS);
        // 修改日期为当前月第一天
        LocalDateTime time11 = time.with(TemporalAdjusters.firstDayOfMonth());
        // 修改年份
        LocalDateTime time12 = time.withYear(2020);
        // 修改时间
        LocalDateTime time13 = time.withHour(1);
        // 修改日期为当前月第一天 0时0分0秒
        LocalDateTime time14 = time.with(TemporalAdjusters.firstDayOfMonth()).withHour(0).withMinute(0).withSecond(0);
        // 日期差值
        long years = time.until(time3,ChronoUnit.YEARS);
        // LocalDateTime 转 Date
        Date date = Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
        // Date 转 LocalDateTime
        LocalDateTime time15 = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
