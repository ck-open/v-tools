package com.ck.base.helper.java8_character_新特性.functional.demo;

/**
 * 函数式接口：有且只有一个抽象方法，称之为函数时接口。
 * 当然接口中可以包含其他的方法（默认、静态、私有）
 *
 * @FunctionalInterface  检测接口是否为函数式接口
 *                          是：编译成功
 *                          否：编译失败（接口中没有抽象方法或抽象方法个数大于1个）
 */
@FunctionalInterface
public interface MyFunctionalInterface {

    // 定义一个抽象方法
    public abstract void method();
}
