package com.ck.base.helper.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池基础
 *使用ExecutorService实现线程池，ExecutorService是java提供的用于管理线程池的类。
 * 	1、 线程池有两个主要作用:控制线程数量、重用线程
 * 	    线程数量过多会导致内存消耗大，并且会产生CPU过度切换导致整体并发性能降低的问题。并且频繁的
 * 	    创建销毁线程也会给系统带来负担。为此在实际开发中出现了上述情况时我们应当使用线程池来管理线程。
 * 	2、 当一个程序中若创建大量线程，并在任务结束后销毁，会给系统带来过度消耗资源，以及过度切换线程的危险，
 * 	    从而可能导致系统崩溃。为此我们应使用线程池来解决这个问题。
 * 	3、 线程池的概念：首先创建一些线程，它们的集合称为线程池，当服务器接受到一个客户请求后，就从线程池中
 * 	    取出一个空闲的线程为之服务，服务完后不关闭该线程，而是将该线程还回到线程池中。
 * 	4、 在线程池的编程模式下，任务是提交给整个线程池，而不是直接交给某个线程，线程池在拿到任务后，它就在
 * 	    内部找有无空闲的线程，再把任务交给内部某个空闲的线程，一个线程同时只能执行一个任务，但可以同时向
 * 	    一个线程池提交多个任务
 */
public class ThreadPoolBase {
    public void createThreadPool(){
        // 创建一个可根据需要创建新线程的线程池，但是在以前构造的线程可用时将重用它们。
        ExecutorService threadPoolCached = Executors.newCachedThreadPool(); // 弹性，随需求增长，延迟缩减

        // 创建一个可重用固定线程集合的线程池，以共享的无界队列方式来运行这些线程。
        ExecutorService threadPoolFixed = Executors.newFixedThreadPool(20); // 固定线程数量，集合排队 （懒汉模式，被动工作线程池和饿汉模）

        // 创建一个线程池，它可安排在给定延迟后运行命令或者定期地执行。
        ExecutorService threadPoolScheduled = Executors.newScheduledThreadPool(100);

        // 创建一个使用单个 worker 线程的 Executor，以无界队列方式来运行该线程。
        ExecutorService threadPoolSingle = Executors.newSingleThreadExecutor();

        /*
         * 停止线程池
         * shutdown():调用后，线程池不会马上停止，这时的线程池不再接收新任务，并且会将所有已经在线程池中的任
	     *              务全部执行完毕后停止。
	     * shutdownNow():线程池会强制中断所有正在运行的线程，并立即停止线程池。
         */
        threadPoolCached.shutdown();
        threadPoolFixed.shutdownNow();
    }

    /**
     * 线程池使用方式
     */
    public void threadPoolDemo(){
        ExecutorService threadPool=Executors.newFixedThreadPool(2);  //创建固定大小的线程池（线程数量为2）
        for(int i=0;i<5;i++) {
            Runnable runn=new Runnable() {
                @Override
                public void run() {
                    Thread t=Thread.currentThread();
                    try {
                        System.out.println(t.getName()+"正在运行...");
                        Thread.sleep(5000);
                        System.out.println(t.getName()+"运行任务完成！");
                    } catch (Exception e) {}}};
            //将任务交给线程池
            threadPool.execute(runn);
            System.out.println("指派了一个任务给线程池");
        }
        threadPool.shutdownNow();
        System.out.println("线程池关闭了");
    }
}
