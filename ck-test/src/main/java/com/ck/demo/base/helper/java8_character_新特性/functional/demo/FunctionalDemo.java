package com.ck.base.helper.java8_character_新特性.functional.demo;


/**
 * 函数式变成
 */
public class FunctionalDemo {

    public static void main(String[] args) {
        // static methods
        IConvert<String, String> convert = Something::startsWith;
        String converted = convert.convert("123");


        // object methods
        Something something = new Something();
        IConvert<String, String> converter = something::endWith;
        converted = converter.convert("Java");

        // constructor methods
        IConvert<String, Something> convertObject = Something::new;
        something = convertObject.convert("constructors");

    }
}



@FunctionalInterface
interface IConvert<F, T> {
    T convert(F form);
}

class Something {
    public Integer getI(char[] iConvert){
        return 0;
    }

    // constructor methods
    Something() {
    }

    Something(String something) {
        System.out.println(something);
    }

    // static methods
    static String startsWith(String s) {
        return String.valueOf(s.charAt(0));
    }

    // object methods
    String endWith(String s) {
        return String.valueOf(s.charAt(s.length() - 1));
    }

    void endWith() {
    }
}
