package com.ck.base.helper.base;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * java反射机制
 * 1、反射机制允许我们的程序在运行时动态加载一个类，并实例化，或者获取其定义的相关信息(属性、方法等)，
 * 	    甚至可以动态调用这些方法。而非传统的在编码期间以编码形式实例化或调用方法等。
 * 2、这样做大大的提高了代码的灵活度。但是反射也是一把双刃剑，过度的使用会降低系统性能。
 */
public class Reflect {
    /*
    * Class类
	*   1、Class类的每一个实例是用来表示一个被JVM加载的类。通过它可以理解到其表示的类
	*        的相关信息，比如有那些属性、构造方法、方法等等。所以我们也称Class的实例叫做类的类对象。
	*   2、在JVM内部，每个被加载的类都有且只有唯一的一个Class的实例表示它。那么这个Class的实例是
    *        在JVM加载一个类的时候创建的。
	*   3、想获取一个类的类对象可以通过下面几种方式得到：
	*       1：每个类都有一个静态属性calss，可以得到这个类的类对象。例如Class cls=Person.class;
	*               通过这个cls可以获取所有有关Person的信息。比如Person有那些方法、那些属性、构造方法等信息。
	*       2：通过Class的静态方法forName()加载指定的类并得到其类对象。
	*                例如：Class cls=Class.forName("object.person");加载object包下的Person类，需要注意，这里的
	*                参数必须写类的完全限定名（包名.类名）。
	*       3：可以通过类加载器加载一个类
    */
    public void classBase() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class cls = this.getClass();  // 获得当前类Class对象

        /*
         * 通过Class实例化其表示的类的一个实例
         * 1.加载Person的类对象。例：Class cls=Class.forName("reflect.Person");
         * 2.通过类对象实例化其表示的类的实例，Class提供了一个newInstance()方法，该方法调用其表示的类的无参构造方法实例化它。
         */
        Class loadCls = Class.forName("package.className"); // 根据完整类名称加载类对象。加载className类时会抛出找不到该类的异常。
        Object object = loadCls.newInstance();    // 根据类对象构建实例，需要处理地址非法和实例化异常

        /*
         * 获取该类定义的要调用的方法 例：Method method=cls.getDeclaredMethod("sayHello", null); ，无参方法设置null，否则传值该方法参数类型
	     * Method的每一个实例用于表示一个类中定义的一个方法，通过Method可以获取与表示的方法相关的信息，比如：方法的返回值类型、
         * 方法名、参数、访问修饰符等等，甚至可以调用该方法。下面的示例：method实例表示的是Person类的成员方法sayHello()方法。
         *
         * JDK1.5以前没有无参方法调用的重载，调用无参方法是需要传null
         */
        Method method = loadCls.getMethod("MethodName"); // 创建方法对象，实例化类成员方法时会抛出没有该方法的异常。
        method.invoke(object);  // 执行方法，传参调用那个对象的方法。调用对象的方法时会抛出引用异常，即找不到该方法
        method = loadCls.getDeclaredMethod("MethodName",new Class[]{String.class});// 有参数方法,指定参数类型
        method.invoke(object,new Object[]{"参数值"}); // 执行有参方法。参数对象要和指定的参数类型对应上。
    }

    /**
     * 反射调用私有方法
     * 私有方法在设置强制方法后，是可以被执行的,即将私有方法实例强制设置为可执行。
     */
    public void invocationPrivateMethod() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class cls = Class.forName("package.ClassName"); // 加载类
        Object object = cls.newInstance();  // 构建类实例化
        Method method = cls.getDeclaredMethod("MethodName"); // 构建方法对象

        // 私有方法在设置强制方法后，是可以被执行的
        method.setAccessible(true);

        method.invoke(object);  // 执行方法
    }

}
