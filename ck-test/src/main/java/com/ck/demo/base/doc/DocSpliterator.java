/*java.util.Spliterator（splitable iterator可分割迭代器）
    接口是Java为了并行遍历数据源中的元素而设计的迭代器，这个可以类比最早Java提供的顺序遍历迭代器Iterator，但一个是顺序遍历，一个是并行遍历。
    从最早Java提供顺序遍历迭代器Iterator时，那个时候还是单核时代，但现在多核时代下，顺序遍历已经不能满足需求了，如何把多个任务分配到不同核上并行执行，才是能最大发挥多核的能力，所以Spliterator应运而生。

    Spliterator 是一个对源（数组，集合，io流等等）中元素进行遍历和分区的类。
    可以通过 tryAdvance() 方法逐个遍历，也可以按照 forEachRemaining() 方法进行按 bulk 逐块的遍历。（内部调用的还是tryAdvance）
    Spliterator 有类似 Collector 中的 characteristics , 但都是由十六进制来表示的。


常用函数
    int characteristics()
        函数定义为int characteristics()，此方法返回此分隔符及其元素的一组特征。它可以是以下8个值中的任何一个。
        以下所有值都是静态最终整数值：
            ORDERED：
                源的元素有序，tryAdvance ，forEachRemaining和 trySplit 都会保证有序的进行元素的处理。
                需要注意 hashSet 这类 Collection 是不保证有序的。
                有ORDERED 特性的数据，在并发计算的时候客户端也要做顺序限制的保证。
            DISTINCT：
                唯一性。 类似 Set 这样的传入集合会拥有这样的特性。
            SORTED：
                有这种特性的 Spliterator ，有一个特定的顺序。或者是所有元素都是可比较的，或者是有特定的比较器。
                有 SORTED 一定会有 ORDERED。
            SIZED：
                有这种属性的 Spliterator 在遍历和分割之前，estimateSize() 返回的大小是固定的，并且是准确的。
            NONNULL：
                不为 NULL, 大部分并发的集合，队列，Map 都可能会有这样的特性。
            IMMUTABLE：
                不可变的。元素遍历期间不可以被 添加，替换，删除（cannot be added, replaced, or removed），否则，应该抛出异常。
            CONCURRENT：
                支持并发操作的。
                顶层的 Spliterator 不可以 CONCURRENT 与 SIZED。 这两者是相互冲突的。
                但是分割之后的 Spliterator ， 可能是 SIZED， 顶层不能决定底层。
            SUBSIZED：
                该 Spliterator 和所有从它拆分出来的分割迭代器都是 SIZED 以及 SUBSIZED 的。
                如果分割后，没有按照要求返回SIZED 以及 SUBSIZED 属性，那么操作是不被保证的，也就是结果不可预测。
                这个属性和 SIZED 的区别就是， SIZED 不保证 SUBSIZED。而 SUBSIZED 会要求保证 SIZED。

    boolean hasCharacteristics(int characteristics)
        characteristics()上面已经解释过的方法用于找出分离器的特性。
        hasCharacteristics用于检查分隔符是否包含所有特征。
        它返回一个布尔值。'true’如果分隔符中存在所有特征，否则返回false。

    boolean tryAdvance(Consumer<? super T> action)
        同时做了 hasNext() 以及 next() 的工作。
        类似于普通的 Iterator ,它会按顺序一个一个使用 Spliterator 中的元素执行action,并且如果还有其他元素要遍历就返回 true，否则返回 false。
        例如：
            String[] arr = {"1","2","3","4","5"};
            Spliterator spliterator = Arrays.spliterator(arr);
            while (spliterator.tryAdvance(System.out::println));
        因此，我们已使用该spliterator()方法从中创建一个Spliterator。我们使用while循环来检查的当前返回值是否tryAdvance为true。如果为true，则在while条件内打印当前值。
        while循环的主体为空,我们可以将其用于其他任何操作。

    Spliterator<T> trySplit()
        在可能并行的操作中使用。
        尝试切分源来的 Spliterator， 返回的是（注意！！！）分割出来的那一部分 数据，原有的数据集将不再包含这部分数据集合。两者 没有交集。剩下的可以继续分割，也许不可以继续分割了。
                举个例子，我原来有 100个元素，我通过 trySplit 切分出 30 个，作为一个新的 Spliterator分割迭代器 返回，原有的，就还剩下 70 个。
        如果是原有数据集合是 ORDERD 的，分出来的也是有序的。
        除非元素数量是无穷的，否则，最后一定会出现不能再分割的情况，这种情况下，返回的结果是 null。
        例如：
            String[] arr = {"1","2","3","4","5"};
            Spliterator spliterator = Arrays.spliterator(arr);
            Spliterator spliterator2 = spliterator.trySplit();
            Spliterator spliterator3 = spliterator.trySplit();
            while (spliterator.tryAdvance(i-> System.out.println(i)));
            while (spliterator2.tryAdvance(i-> System.out.println(i)));
            while (spliterator3.tryAdvance(i-> System.out.println(i)));
            运行结果：4、5、1、2、3

    long estimateSize()
        估算还剩下多少个元素需要遍历，不一定精确。
        但是如果这个 Spliterator 是 SIZED，没有被遍历或者 split， 或是 SUBSIZED的，没有被遍历，那么他这个值一定是准确的。

    void forEachRemaining(Consumer<? super T> action)
        能够将Iterator中迭代剩余的元素传递给一个函数。
        是一个默认方法，对余下的元素进行操作，直到元素全部被遍历完。
        一般情况下会直接调用上面的tryAdvance() 方法，但是也可以根据需要进行重写。
        例如：
            Iterator<String> iterator = Arrays.asList("a","b","c","d").iterator();
            // 传统遍历
            while(iterator.hasNext()){
                System.out.println(iterator.next());
            }
            // 使用forEachRemaining 遍历
            iterator.forEachRemaining(a-> System.out.println(a));


*/

package com.ck.demo.base.doc;
public class DocSpliterator{


    public static void main(String[] args) {

    }
}