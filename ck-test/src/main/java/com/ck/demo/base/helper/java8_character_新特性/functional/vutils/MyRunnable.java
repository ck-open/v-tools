package com.ck.base.helper.java8_character_新特性.functional.vutils;

/**
 * Runnable接口应该由任何类实现，其实例应该由线程执行。
 * 类必须定义一个不带参数的方法run。
 * <p>
 * 此接口旨在为希望在活动时执行代码的对象提供公共协议。
 * 例如，Runnable由类Thread实现。处于活动状态仅意味着线程已经启动且尚未停止。
 * <p>
 * 此外，Runnable提供了使类处于活动状态而不是子类化Thread。
 * 通过实例化Thread实例并将自身作为目标传入，实现Runnable的类可以在不继承Thread的情况下运行，
 * 在大多数情况下，如果你只打算覆盖run()方法，而不覆盖其他Thread方法，那么应该使用Runnable接口。
 * 这很重要，因为除非程序员打算修改或增强类的基本行为，否则类不应该被子类化。
 */
@FunctionalInterface
public interface MyRunnable {
    /**
     * 当一个实现接口Runnable的对象被用来创建一个线程时，启动这个线程会导致对象的run方法在单独执行的线程中被调用。
     * <p>
     * run方法的一般约定是，它可以执行任何操作。
     */
    public abstract void run();
}
