package com.ck.base.helper.java8_character_新特性;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *lambda表达式  JDK8推出的一个特性，可用来快速便捷的创建匿名内部类
 * 语法：（[arg1,arg2...])->{方法体}
 * 	1、 lambda表达式在创建一个匿名内部类时有一个要求，就是其对应接口必须有且只能有一个
 * 	    抽象方法时才可以使用。如果存在多个方法时使用编译是不通过的。示例如下：
 * 	    普通内部类：Runnable runn=new Runnable(){public void run() {
 * 			                System.out.println("hello");}};
 * 	               Lambda表达式： Runnable run=()->{System.out.println("hello");};
 * 	2、 若重写的方法中只有一句代码，那么该方法的“{}"是可以省略的。
 * 	    Lambda表达式只有一句代码时：Runnable ru=()-> System.out.println();
 *
 * 	3、 重写方法若含有参数，参数类型可以忽略。编译器会结合上下文判定参数类型。
 * 		    FileFilter filter=(f)->{return f.getName().endsWith(".txt");};
 * 	4、 若可以忽略方法的"{}",那么当这个方法要求返回值时，return关键字也必须忽略。
 * 		    FileFilter filter=(f)->f.getName().endsWith(".txt");
 * 	    或直接套用：File[] subs=file.listFiles((f)->f.getName().endsWith(".txt"));
 *
 * JDK1.8之后，集合和Map推出了为lambda表达式实现的遍历操作，并且对于一个线程安全的集合或Map而言，
 * 	使用这种遍历方式是可以保证并发安全的。而以前使用迭代器则不能保证，需要 自行维护并发安全。
 * 	1、lambda形式遍历集合：list.forEach((str)->System.out.println(str));
 * 	2、lambda形式遍历Map查询表：map.forEach((k,v)->System.out.println(k+":"+v));
 */
public class LambdaBase {


    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        map.put("key1","sdfdsagf");
        map.put("key2","fgfdsg");
        map.put("key3","fdgfdgfd");
        map.put("key4","dfhreret");

        new HashSet<>(map.keySet()).forEach(k->{
            map.put(k+"dsfd","dsewrfsdf");
        });
        System.out.println(JSONObject.toJSONString(map));
    }
}
