package com.ck.base.helper.java8_character_新特性.functional.vutils;

import java.util.Objects;
import java.util.function.Function;

/**
 * 表示接受一个参数并产生结果的函数。
 * <p>
 * 这是一个函数接口，其函数方法是apply(Object)。
 *
 * @param <T> 函数输入的类型
 * @param <R> 函数结果的类型
 * @since 1.8
 */
@FunctionalInterface
public interface MyFunction<T, R> {
    /**
     * 将此函数应用于给定的参数。
     *
     * @param t 函数参数
     * @return 函数的结果
     */
    R apply(T t);

    /**
     * 返回一个复合函数，该函数首先将[@code before)函数应用于其输入，然后将此函数应用于结果。
     * 如果任意一个函数的求值引发异常，则将其传递给组合函数的调用方。
     *
     * @param <V>    函数和组合函数的输入类型
     * @param before 在应用此函数之前应用的函数
     * @return 一个组合函数，首先应用{@code before}函数，然后应用这个函数
     * @throws NullPointerException 如果before为空
     * @see #andThen(Function)
     */
    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }

    /**
     * 返回一个复合函数，该函数首先将此函数应用于其输入，然后将f@code)函数应用于结果。如果任意一个函数的求值引发异常，则将其传递给组合函数的调用方。
     *
     * @param <V>   函数和复合函数的输出类型
     * @param after 应用此函数后要应用的函数
     * @return 一个组合函数，首先应用这个函数，然后应用{@code after}函数
     * @throws NullPointerException 如果 after为null
     * @see #compose(Function)
     */
    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }

    /**
     * 返回一个总是返回其输入参数的函数。
     *
     * @param <T> 函数的输入和输出对象的类型
     * @return 一个总是返回输入参数的函数
     */
    static <T> Function<T, T> identity() {
        return t -> t;
    }
}
