package com.ck.base.helper.thread;


/**
 * 多线程
 * 多线程可以实现多段代码“同时运行”的效果。实际上是并发运行的。
 *
 * 创建线程有两种方式
 * 方式1：继承Thread并重写run方法。run方法的作用是定义该线程要执行的任务。
 * 方式2：实现Runnable接口，单独定义线程任务
 * @author cyk
 *
 */
public class ThreadBase {

    public void createThread(){
        /*
         * 第一种创建线程的方式比较简单，适合使用匿名内部类快速创建临时线程跑任务。
         * 但是它也存在两种设计不足
         * 1：由于需要继承Thread，而java又是单继承的，这就导致若
         *      继承了Thread就无法再继承其它类去复用方法。这在实际开发中是出现比较多的矛盾。
         * 2：继承Thread后需要重写run方法来定义任务，这就导致了
         *      线程与线程要执行的任务之间存在一个必然的耦合关系，导致线程重用性变差。
         */
        Thread thread1 = new Thread(){
            public void run(){
                System.out.println("线程业务逻辑执行了！");
            }
        };
        /*
         * 启动线程要调用start方法，而不是直接调用run方法。
         * 当一个线程的start方法调用完毕后，该线程会纳入到线程调度中，一旦被分配CPU时间片，该线程会自动的
         * 执行其run方法开始运行任务。所以一个线程的start方法执行后run方法会很快的被运行起来。
         */
        thread1.start();


        // 第二种方式
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("第二种方式，实现任务接口。");
            }
        };
        Thread thread2 = new Thread(runnable);
        thread2.start();
    }

    public void threadUtil() throws InterruptedException {
        /*
         * Thread提供了一个静态方法： static Thread currentThread()
         * 该方法可以获取到运行该方法的线程并将其返回
         *
         * 后面会有一个很重要的API：ThreadLocal，就是利用这个方法解决问题的。包括后期spring中使用aop管理事物时
         * 就用到了这个操作。
         */
        Thread nowThread = Thread.currentThread();  // 获取运行方法的线程

        String threadName = nowThread.getName();    // 获取线程名字
        long threadId = nowThread.getId();          // 获取唯一标示
        int priority = nowThread.getPriority();     // 获取线程优先级
        boolean isAlive = nowThread.isAlive();      // 是否处于活动状态
        boolean isDaemon = nowThread.isDaemon();    // 判断线程是否为守护线程
        boolean isInterrupted = nowThread.isInterrupted(); // 判断线程是否被中断

        /*
         * 线程的优先级
         * 线程在并发运行时对于线程调度的工作是不可控的，即：
         * 不能主动获取CPU时间片，只能被动的被分配。但是线程调度会尽可能均匀的将时间片分配给所有并发运行的线程，但不能
         * 保证每个线程“一人一次”。
         * 通过调整线程的优先级可以最大程度的改善获取CPU时间片的次数。理论上，线程优先级越高的线程获取CPU时间片的次数越多。
         * 线程优先级有10个等级，分别用整数1-10表示。其中1为最低，10为最高，5为默认
         */
        nowThread.setPriority(Thread.MAX_PRIORITY);

        /*
         * 守护线程
         * 守护线程又称为后台线程。默认创建出来的线程都是前台线程，后台线程是需要在线程启动前单独进行设置的。
         * 设置的方法为：void setDaemon(boolean on) 当参数为true时，该线程被设置为守护线程（后台线程）
         *
         * 在使用上前台线程与后台线程没有什么区别，但是在结束时机上有一个不同，即：当进程结束时，所有还在运行的守护线程
         * 都会被强制中断。
         * 进程的结束：当一个进程中的所有前台线程都结束时，进程结束。
         */
        nowThread.setDaemon(false);

        /*
         * 多个线程之间的异步运行代码的（各自代码运行之间不存在先后关系，各干各的）
         * 同步运行：执行有先后顺序，通常单线程运行代码是同步的
         * 异步运行：各干各的，多线程执行就是异步执行
         *
         * 线程提供了：void join()
         * 该方法允许一个线程在join方法所属线程后面等待，直到该线程执行完毕后再继续运行。所以使用join可以协调线程
         * 之间同步运行的代码。
         */
        nowThread.join();  // 当前线程会等待nowTread线程执行完毕后在执行

        /*
         * static void sleep(long ms)
         * 线程提供的sleep方法可以让运行该方法的线程处于阻塞状态指定毫妙，当超时后，线程自动回到RUNNABLE状态
         * 等待再次分配时间片并发运行。
         * 当一个线程处于BLOCK（阻塞）状态时，CPU会立刻释放其时间片，去执行其他线程。
         *
         * sleep方法要求必须处理中断异常：InterruptedException
         * 当我们调用一个线程的interrupt方法时，可以中断一个正在运行的线程，但是若该线程正处于阻塞时，那么调用该方法时并不是中断
         * 开线程，而仅是中断其阻塞状态，这时会抛出中断异常，通知程序该线程的阻塞状态被中断。
         */
        Thread.sleep(1000);


    }

    /**
     * 多线程并发安全问题
     * 当多个线程并发操作同一资源时，由于线程切换实际不确定导致程序未按照设计要求的顺序执行而出现了逻辑混乱。
     * 严重时可能导致系统瘫痪。
     * 解决多线程并发安全问题的方式就是让多个线程排队访问该资源。将异步操作变为同步操作。
     *
     * 当一个方法被synchronized修饰后，那么该方法称为同步方法，即：多个线程不能同时进入方法内部执行，
     * 只能一个线程执行完，其他线程再进来执行。将异步操作修改为同步操作就解决了并发安全问题。
     * 成员方法上使用synchronized，那么同步监视器对象就是当前方法所属对象，即方法中看到的this。
     * 语法：public synchronized void methodName(){方法体}
     *
     * 静态方法若使用synchronized修饰后，那么该方法一定具有同步效果。
     * 实际上静态方法的同步监视器对象为当前类的类对象。类对象后面在学习反射的知识时介绍。
     * 语法：public static synchronized void methodName(){方法体}
     *
     * 有效的缩小同步范围可以在保证并发安全的前提下提高并发的效率。
     * 同步块可以更准确的控制需要同步运行的代码片段。
     * 语法：synchronized(同步监视器对象）{需要同步运行的代码片段}
     *
     * 互斥锁
     * 当使用synchronized控制多段代码，而这些同步块使用的同步监视器对象是同一个时，那么控制的这些代码片段之间
     * 就是互斥关系，多个线程不能同时执行它们。
     * @author cyk
     */
    public void threadSynchronized(){
        Thread t = Thread.currentThread();
        try {
            System.out.println(t.getName()+"正在挑衣服...");
            Thread.sleep(5000);
            /*
             * 使用同步块精确的控制需要同步的代码片段。
             * 需要注意，若想让多个线程同步运行控制的代码片段必须保证同步监视器对象多个线程看到的是同一个。
             */
            synchronized(this) {
//			synchronized(new Object) {//这样做就没有同步效果了
                System.out.println(t.getName()+"正在试衣服...");
                Thread.sleep(5000);
            }
            System.out.println(t.getName()+"结帐离开。");
        } catch (InterruptedException e) {
        }

    }

}
