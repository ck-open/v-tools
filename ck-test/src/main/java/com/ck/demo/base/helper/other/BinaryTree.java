package com.ck.base.helper.other;

/**
 * 二叉树
 */
public class BinaryTree {

	public static void main(String[] args) {

		Tree<Integer> t = new Tree<Integer>();
		t.add(60,56,7,46,8,56,46,2,56,9,64,46,131,66,4);
		System.out.println(t.toString());

	}

}

class Tree<E extends Comparable<E>> {
	private Node root; // 二叉树状结构根节点
	public int size;

	class Node {
		E data;
		Node parent;
		Node left;
		Node right;

		Node(E e) {
			this.data = e;
		}

		/**
		 * 插入子元素
		 * 
		 * @param e
		 */
		public void addChild(E e) {
			// 比较e当前数据data
			int val = e.compareTo(data);
			if (val == 0) {
				return;
			}
			if (val < 0) { // e小于data
				if (left == null) {
					left = new Node(e);
					left.parent = this;
				} else {
					left.addChild(e);
				}

			} else {
				if (right == null) {
					right = new Node(e);
					right.parent = this;
				} else {
					right.addChild(e);
				}
			}
			size++;
		}
		
		public String toString() {
			String l="",r="";
			if(left!=null) {
				l=left.toString();
			}
			if(right!=null) {
				r = right.toString();
			}
			//称为中序遍历 左+中+右，输出结果是排序的
			return l+data+" "+r;
		}
	}

	/**
	 * 将元素添加到二叉树
	 * 
	 * @param e
	 */
	public void add(E e) {
		if (root == null) {
			// 第一个元素，直接存入根节点
			root = new Node(e);
		} else {
			// 给root添加子元素(child)元素
			root.addChild(e);
		}
	}
	/**
	 * 可变长参数类型... 变量名
	 * 1. 调用方法时，可以给任意长度的数据。也可以直接传入数组
	 * 2. 方法中可变参数就是一个数组
	 * 3. 一个方法参数只能使用一个变长参数
	 * 4. 只能在最后一个参数使用变长参数
	 * @param e
	 */
	public void add(E... e) {
		int n=0;
		for (E i : e) {
			add(i);
		}

	}
	
	public String toString() {
		return root.toString();
	}

}