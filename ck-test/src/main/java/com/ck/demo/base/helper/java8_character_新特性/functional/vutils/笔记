在开发过程中经常会使用 if...else...进行判断抛出异常、分支处理等操作。
可以用java 8 中的Function 接口来取缔繁琐的if...else...来美化代码。

# Function函数接口
    使用注解@FunctionalInterface标识，并且只包含一个抽象方法的接口是函数式接口。
    函数式接口主要分为Supplier供给型函数、Consumer消费型函数、Runnable无参返回型函数和Function有参返回型函数。
    Function可以看作转换型函数。

# Supplier供给型函数
    Supplier的表现形式为不接受参数、只返回数据
        /**
         * 代表结果的提供者
         * 没有要求每次调用供应商时都返回一个新的或不同的结果
         * 这是一个函数式接口，其函数方法是get()
         * 自 java 1.8
         *
         * @param <T> 参数<T> 此供应商提供的结果类型
         */
        @FunctionalInterface
        public interface Supplier<T> {

            /**
             * Gets a result
             * 返回值: a result
             *
             * @return
             */
            T get();
        }


# Consumer消费型函数
    Consumer消费型函数和Supplier刚好相反。
    Consumer接收一个参数，没有返回值
        /**
         * 表示接受单个输入参数且不返回结果的操作，与大多数其他功能接口不同。Consumer预计通过副本进行操作。
         * 这是一个功能接口，其功能方法是accept(Object)。
         * 自 java 1.8
         *
         * @param <T> 参数<T> 操作的输入类型
         */
        @FunctionalInterface
        public interface Consumer<T> {

            /**
             * 对给定参数执行此操作
             *
             * @param t 输入的参数
             */
            void accept(T t);

            /**
             * 返回一个组合的Consumer，它依次执行此操作和accept操作。
             * 如果执行任一操作引发异常，则将其转发给组合操作的调用者。
             * 如果执行此操作抛出异常，则不会执行accept操作。
             *
             * @param after 再次操作之后执行的操作
             * @return 一个组合的Consumer，它按顺序执行此操作，然后执行accept操作。
             * 如果after为空则抛出：NullPointerException
             */
            default Consumer<T> andThen(Consumer<? super T> after) {
                Objects.requireNonNull(after);
                return (T t) -> {
                    accept(t);
                    after.accept(t);
                };
            }
        }

# Runnable无参返回型函数
    Runnable的表现形式为既没有参数也没有返回值
        /**
         * Runnable接口应该由任何类实现，其实例应该由线程执行。
         * 类必须定义一个不带参数的方法run。
         * <p>
         * 此接口旨在为希望在活动时执行代码的对象提供公共协议。
         * 例如，Runnable由类Thread实现。处于活动状态仅意味着线程已经启动且尚未停止。
         * <p>
         * 此外，Runnable提供了使类处于活动状态而不是子类化Thread。
         * 通过实例化Thread实例并将自身作为目标传入，实现Runnable的类可以在不继承Thread的情况下运行，
         * 在大多数情况下，如果你只打算覆盖run()方法，而不覆盖其他Thread方法，那么应该使用Runnable接口。
         * 这很重要，因为除非程序员打算修改或增强类的基本行为，否则类不应该被子类化。
         */
        @FunctionalInterface
        public interface Runnable {
            /**
             * 当一个实现接口Runnable的对象被用来创建一个线程时，启动这个线程会导致对象的run方法在单独执行的线程中被调用。
             * <p>
             * run方法的一般约定是，它可以执行任何操作。
             */
            public abstract void run();
        }

# Function函数
    Function函数的表现形式为接收一个参数，并返回一个值。Supplier、Consumer和Runnable可以看作Function的一种特殊形式
        /**
         * 表示接受一个参数并产生结果的函数。
         * <p>
         * 这是一个函数接口，其函数方法是apply(Object)。
         *
         * @param <T> 函数输入的类型
         * @param <R> 函数结果的类型
         * @since 1.8
         */
        @FunctionalInterface
        public interface Function<T, R> {
            /**
             * 将此函数应用于给定的参数。
             *
             * @param t 函数参数
             * @return 函数的结果
             */
            R apply(T t);

            /**
             * 返回一个复合函数，该函数首先将[@code before)函数应用于其输入，然后将此函数应用于结果。
             * 如果任意一个函数的求值引发异常，则将其传递给组合函数的调用方。
             *
             * @param <V>    函数和组合函数的输入类型
             * @param before 在应用此函数之前应用的函数
             * @return 一个组合函数，首先应用{@code before}函数，然后应用这个函数
             * @throws NullPointerException 如果before为空
             * @see #andThen(Function)
             */
            default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
                Objects.requireNonNull(before);
                return (V v) -> apply(before.apply(v));
            }

            /**
             * 返回一个复合函数，该函数首先将此函数应用于其输入，然后将f@code)函数应用于结果。如果任意一个函数的求值引发异常，则将其传递给组合函数的调用方。
             *
             * @param <V>   函数和复合函数的输出类型
             * @param after 应用此函数后要应用的函数
             * @return 一个组合函数，首先应用这个函数，然后应用{@code after}函数
             * @throws NullPointerException 如果 after为null
             * @see #compose(Function)
             */
            default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
                Objects.requireNonNull(after);
                return (T t) -> after.apply(apply(t));
            }

            /**
             * 返回一个总是返回其输入参数的函数。
             *
             * @param <T> 函数的输入和输出对象的类型
             * @return 一个总是返回输入参数的函数
             */
            static <T> Function<T, T> identity() {
                return t -> t;
            }
        }

# 处理抛出异常的if
    1、定义函数
        定义一个抛出异常的形式的函数式接口，这个接口只有参数没有返回值是个消费型接口
            /**
             * 抛出异常接口
             */
            @FunctionalInterface
            public interface ThrowExceptionFunction {
                /**
                 * 抛出异常信息
                 * @param message 异常信息
                 */
                void throwMessage(String message);
            }

    2、编写判断方法
        创建工具类VUtils并创建一个isTrue方法，方法的返回值为刚才定义的函数式接口ThrowExceptionFunction。
        ThrowExceptionFunction的接口实现逻辑为当参数b为true时抛出异常
            /**
             * 如果参数为true抛出异常
             * @param b
             * @return
             */
            public static ExceptionIFFunction.ThrowExceptionFunction isTrue(boolean b){
                return (errorMessage)->{
                  if (b){
                      throw new RuntimeException(errorMessage);
                  }
                };
            }

    3、使用方式
        调用工具类参数后，调用函数接口的throwMessage方法传入异常信息。当传入的参数为false时正常执行，true时抛出异常。
            public static void main(String[] args) {
                VUtils.isTrue(true).throwMessage("函数式IF异常抛出");
            }

# 处理if分支操作
    1、定义函数式接口
        创建BranchHandle的函数式接口，接口的参数为两个Runnable接口。
        这两个Runnable接口分别代表True或False时要进行的操作。
            /**
             * 分支处理接口
             */
            @FunctionalInterface
            public interface BranchHandle{
                /**
                 * 分支操作
                 * @param trueHandle 为true时要进行的操作
                 * @param falseHandle 为false时要进行的操作
                 */
                void trueOrFalseHandle(Runnable trueHandle,Runnable falseHandle);
            }
    2、编写判断方法
        创建isTrueOrFalse的方法，方法返回值为BranchHandle。
            /**
             * 参数为true或false时，分别进行不同的操作
             * @param b
             * @return
             */
            public static BranchHandle isTrueOrFalse(boolean b){
                return (trueHandle, falseHandle) -> {
                    if (b){
                        trueHandle.run();
                    }else{
                        falseHandle.run();
                    }
                };
            }
    3、使用方式
        参数为true时执行trueHandle
            public static void main(String[] args) {
                  VUtils.isTrue(true).throwMessage("函数式IF异常抛出");
                VUtils.isTrueOrFalse(true).trueOrFalseHandle(()-> System.out.println("True, 开始秀了"),()-> System.out.println("False, 秀不动了，快跑"));
            }
# 值存在与否的执行消费操作，否则执行基于空的操作
    1、定义函数
        创建PresentOrElseHandle的函数式接口，接口的参数一个为Consumer接口，另一个为Runnable。
        分别代表值不为空时执行的消费操作和值为空时执行的其他操作。
            /**
             * 空值与非空值分支处理
             * @param <T>
             */
            @FunctionalInterface
            public interface PresentOrElseHandler<T extends Object> {
                /**
                 * 值不为空时执行消费操作
                 * 值为空时执行其他的操作
                 * @param action
                 * @param emptyAction
                 */
                void presentOrElseHandle(Consumer<? super T> action, Runnable emptyAction);
            }
    2、编写判断方法
        创建isBlankOrNoBlank的方法，方法的返回值为PresentOrElseHandle。
            /**
             * 参数为空或不为空分别执行不同的操作
             * @param str
             * @return
             */
            public static PresentOrElseHandler<?> isBlankOrNoBlank(String str) {
                return (action, emptyAction) -> {
                    if (str == null || str.length() == 0) emptyAction.run();
                    else action.accept(str);
                };
            }
    3、使用方式
        public static void main(String[] args) {
            VUtils.isBlankOrNoBlank("Hello").presentOrElseHandle(System.out::println,()-> System.out.println("字符串为空"));
        }



