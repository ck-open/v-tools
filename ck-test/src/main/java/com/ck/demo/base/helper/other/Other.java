package com.ck.base.helper.other;

import java.util.Arrays;

/**
 *
 */
public class Other {

    public static void main(String[] args) {
        Other o = new Other();
        System.out.println("回文检查："+o.plalindrome("abcdefecba"));
        System.out.println("同分异构检查："+o.isomerism("abcde","adcbe"));

        System.out.println("主函数输出x="+test());
    }

    public strictfp static int test(){
        int x = 1;
        try {
            return x;
        }finally{
            ++x;
            System.out.println("finally打印输出x="+x);
            return x;
        }
    }

    /**
     * 字符串回文判断
     * @param str
     */
    public boolean plalindrome(String str){
        if (str==null || "".equals(str)){
            return false;
        }
        String temp = str.trim();
        int len = temp.length()-1;

        for (int i=0;i<temp.length()/2;i++){
            if (temp.charAt(i)!=temp.charAt(len--))
                return false;
        }
        return true;
    }

    /**
     * 字符串同分异构判断
     * @param s1
     * @param s2
     */
    public boolean isomerism(String s1,String s2){
        if (s1==null || s2==null || s1.length()!=s2.length()){
            return false;
        }

        for (int i=0;i<s1.length();i++){
            if (s2.indexOf(s1.charAt(i))==-1 || s1.indexOf(s2.charAt(i))==-1){
                return false;
            }
        }
        return true;
    }

    /**
     * 冒泡排序
     */
    public void BubbleSort(){
        // 生成随机数 数组
        int[] arr=new int[200000];
        for(int i=0;i<arr.length;i++) {
            arr[i]=(int)(Math.random()*100);
        }
        System.out.println("...........分割线................");
        //冒泡排序法
        for(int i=0;i<arr.length;i++) {  //所有数比较
            for(int y=0;y<arr.length-1-i;y++) {  //每个数比较，第一个不需要比较所以最大长度-1，比过的数字在最后面也不需要再次比较所有-i
                if(arr[y]>arr[y+1]) {
                    int a;
                    a=arr[y];
                    arr[y]=arr[y+1];
                    arr[y+1]=a;
                }
            }
        }
    }

    /**
     * 快速排序
     * @param arr
     * @param left
     * @param right
     */
    public void quickSort(int[] arr, int left, int right){
        int p = arr[left]; //记录基数
        int l = left; //创建left指针
        int r = right; //创建right指针
        while (l<r) { //指针不重叠循环
            while (l<r && arr[r] >= p) r--;	 //r指针>=基数p 指针左移
            if (l<r) { //指针不相重就交换r/l指针数据
                int temp = arr[l];
                arr[l] = arr[r];
                arr[r] = temp;
                l++;
            }
            while (l<r && arr[l] <= p) l++; //l指针<=基数p 指针右移
            if (l<r) { //指针不相重就交换r/l指针数据
                int temp = arr[l];
                arr[l] = arr[r];
                arr[r] = temp;
                r--;
            }
        }
        if(l>left) quickSort(arr, left,l);//l指针大于数组上标进行递归
        if(r<right) quickSort(arr,r+1,right); //r指针小于数组下标进行递归
    }

    /**
     * 二分查找
     */
    public int binarySearch(Integer[] srcArray, int des){
        //定义初始最小、最大索引
        int start = 0;
        int end = srcArray.length - 1;
        //确保不会出现重复查找，越界
        while (start <= end) {
            //计算出中间索引值
            int middle = (end + start)>>>1 ;//防止溢出
            if (des == srcArray[middle]) {
                return middle;
                //判断下限
            } else if (des < srcArray[middle]) {
                end = middle - 1;
                //判断上限
            } else {
                start = middle + 1;
            }
        }
        //若没有，则返回-1
        return -1;
    }

    public void breakNested(){
        ok:
        for (int i=0;i<10;i++){
            for (int j=0;j<10;j++){
                System.out.println("i="+i+"   j="+j);
                if(j==5){
                    break ok;
                }
            }
        }
    }
    void fund(int a, float f){

    }
    void fund(int a,int b){

    }


}

class outer{
    static int i = 0;
    static class innerP{
        public void run(){
            System.out.println(i);
            this.getClass();
        }
    }
}


