package com.ck.demo.base.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Remove_Copy {

	public static void main(String[] args) {
		// 设置要删除的文件路径
		File file = new File("D:\\129ee078c7f514baaad83dfd3def7f5f");
		// 调用方法递归删除该路径下的所有文件
		deleteFile(file);

		// 复制的文件
		File fileCopy = new File("D:\\1234.txt");
		// 要复制到的位置和文件名
		File filePath = new File("D:\\avd\\dfsg\\sdg\\fhgfd" );
		//调用方法进行复制
		 copyFile(fileCopy,filePath);
	}

	/**
	 * 复制文件
	 * 
	 * @param file
	 * @param targetPath
	 * @throws FileNotFoundException
	 */
	public static void copyFile(File file, File targetPath) {
		if(!file.exists()) {
			System.out.println("复制的文件不存在！");
			return;
		}
		if(!targetPath.exists()) {
			targetPath.mkdirs();
		}
		
		RandomAccessFile raf = null;
		RandomAccessFile target = null;
		try {
			// 获取源文件
			raf = new RandomAccessFile(file, "r");
			// 获取目标文件
			target = new RandomAccessFile(targetPath+"\\Copy_"+file.getName(), "rw");
			System.out.println("读取到文件准备复制...");
			// 创建读取器
			byte[] data = new byte[1024 * 10];
			// 记录读取的长度
			int len = -1;
			// 循环读取
			while ((len = raf.read(data)) != -1) {
				// 将读取到的数据写出到目标文件
				target.write(data, (int) target.length(), len);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// 关闭资源
			try {
				raf.close();
				target.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("复制完毕！");
	}

	/**
	 * 删除多级目录
	 */
	public static void deleteFile(File file) {
		// 判断文件是否有下级目录
		if (file.isDirectory()) {
			// 获取子集目录
			File[] files = file.listFiles();
			// 遍历每个子集文件
			for (File f : files) {
				// 每个子集文件递归调用该方法
				deleteFile(f);
			}
		}
		// 删除文件
		file.delete();
		System.out.println(file.getPath()+"  文件以删除！");
	}
}
