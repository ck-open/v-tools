package com.ck.base.helper.class_loader;

import java.io.*;

public class MyClassLoader extends ClassLoader {
    @Override
    public Class<?> loadClass(String arg0) throws ClassNotFoundException {
        return super.loadClass(arg0);
    }

    @SuppressWarnings("deprecation")
    @Override
    public Class<?> findClass(String name){
        byte[] b = new byte[0];
        try {
            b = loadClassData(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return defineClass("textarc.hh", b, 0, b.length);
    }

    /**
     * 读文件
     * @param name
     * @return
     * @throws IOException
     */
    private byte[] loadClassData(String name) throws IOException {
        // load the class data from the connection
        int n=0;
        BufferedInputStream bufferedInputStream =new BufferedInputStream(new FileInputStream(name));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while ((n=bufferedInputStream.read())!=-1){
            byteArrayOutputStream.write(n);
        }
        bufferedInputStream.close();
        return byteArrayOutputStream.toByteArray();
    }
}
