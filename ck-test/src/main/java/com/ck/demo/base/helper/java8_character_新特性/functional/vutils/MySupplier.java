package com.ck.base.helper.java8_character_新特性.functional.vutils;

/**
 * 代表结果的提供者
 * 没有要求每次调用供应商时都返回一个新的或不同的结果
 * 这是一个函数式接口，其函数方法是get()
 * 自 java 1.8
 *
 * @param <T> 参数<T> 此供应商提供的结果类型
 */
@FunctionalInterface
public interface MySupplier<T> {

    /**
     * Gets a result
     * 返回值: a result
     *
     * @return
     */
    T get();
}
