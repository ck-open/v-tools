package com.ck.base.helper.java8_character_新特性.java_util_function;

import java.util.function.Consumer;

/**
 * java.util.function.Supplier<T> 接口正好与Supplier接口相反。
 *      它不是产生一个数据，而是消费一个数据，其数据类型由泛型决定。
 *
 * Consumer接口中包含抽象方法void accept(); 意为消费一个指定泛型的数据
 *
 * Consumer接口中包含抽象方法void andThen();
 *      如果一个方法的参数和返回值全都是 Consumer 类型，
 *      那么就可以实现效果：消费数据的时候，首先做一个操作，然后再做一个操作，实现组合
 *      作用：将两个Consumer接口组合到一起，然后再进行消费。
 *      例如
 *          Consumer c1;
 *          Consumer c2;
 *          String s = "Hello!"
 *          c1.accept(s);
 *          c2.accept(s);
 *          使用andThen进行连接组合后,c1调用andThen传递c2，然后再accept方法进行消费。
 *          组合的消费顺序为，谁再前边谁先消费。
 *          c1.andThen(c2).accept(s);
 *
 */
public class ConsumerDemo {

    /**
     * 定义一个方法
     * 方法的参数传递一个姓名和一个Consumer函数式接口，泛型使用String来消费姓名
     * @param name
     * @param consumer
     */
    public static void method(String name, Consumer<String> consumer){
        consumer.accept(name);
    }

    // 测试andThen()方法。
    public static void method(String name, Consumer<String> consumer1, Consumer<String> consumer2){
//        consumer1.accept(name);
//        consumer2.accept(name);
        // 等同于下面
        consumer1.andThen(consumer2).accept(name);
    }


    public static void main(String[] args) {
        // 调用method方法传递姓名和 Consumer接口的Lambda表达式实现
        method("Consumer", name->{
            System.out.println("消费方式1，直接输出："+name);

            System.out.println("消费方式2，将字符串进行反转输出："+new StringBuilder(name).reverse().toString());
        });

        // 测试andThen方法
        method("测试andThen方法！"
                , name->System.out.println("andThen方法进行了简单输出！"+name)
                , name->System.out.println("andThen方法将字符串反转后进行输出！"+ new StringBuilder(name).reverse().toString()));
    }

}
