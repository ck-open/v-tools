package com.ck.demo.base.test;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 网络连接控制类
 * 
 * @author cyk
 *
 */

public class NetControl {

	/**
	 * 创建网络连接
	 * 
	 * @param urlPort
	 *            请求地址http://localhost:port/user/...
	 * @param requestMethod
	 *            请求方式：GET/POST 参数为null则默认GET
	 * @return HttpURLConnection连接对象
	 */
	public static HttpURLConnection getConnection(String urlPort, String requestMethod) {
		if (!urlPort.startsWith("http://") || !urlPort.startsWith("https://"))
			urlPort = "http://" + urlPort;
		// 创建URL obiect
		URL url;
		try {
			url = new URL(urlPort);
			// 根据请求地址创建Connection连接
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			// 请求方式
			conn.setRequestMethod(requestMethod == null ? "GET" : requestMethod);
			// 设置一个指定的超时值(以毫秒为单位)，用于打开到此URLConnection引用的资源的通信链接。
			conn.setConnectTimeout(5000);
			// 将读取超时设置为指定超时(以毫秒为单位)。非零值指定连接到资源时从输入流读取时的超时。如果超时在可用数据读取之前过期。
			conn.setReadTimeout(5000);
			// 设置输入、输出流是否开启
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 是否使用缓存数据
			conn.setUseCaches(false);
			// 设置连接和字符集
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Charset", "UTF-8");
			// 返回此连接
			return conn;
		} catch (MalformedURLException e) {
			System.out.println("网络连接获取失败！ URL格式错误:" + urlPort + "   ==> " + e.getMessage());
		} catch (IOException e) {
			System.out.println("网络连接获取失败！ I/O异常:" + e.getMessage());
		}
		return null;
	}

	/**
	 * POST请求时向接口发送参数
	 * 
	 * @param conn
	 *            HttpURLConnection连接对象
	 * @param json
	 *            要发送给的JSON数据对象
	 */
	public static boolean sendJSON(HttpURLConnection conn, JSONObject json) {
		try {
			// 设置文件类型:
			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			// 设置接收类型否则返回415错误
			// conn.setRequestProperty("accept","*/*")此处为暴力方法设置接受所有类型，以此来防范返回415;
			conn.setRequestProperty("accept", "application/json");
			// 指定正文内容长度
			conn.setRequestProperty("Content-Length", String.valueOf(json.toJSONString().getBytes().length));

			// 传输请求
			conn.connect();
			OutputStream out = conn.getOutputStream();
			// 写入请求的字符串
			out.write(json.toJSONString().getBytes());
			// 刷新缓存输出流
			out.flush();
			// 关闭释放此输出流
			out.close();
			// 断开请求
			conn.disconnect();
			return true;
		} catch (IOException e) {
			System.out.println("网络数据发送失败！ I/O异常:" + e.getMessage());
		}
		return false;
	}

	/**
	 * 解读服务端响应的JSON数据<br>
	 * 解码字符集：UTF-8
	 * 
	 * @param conn
	 *            HttpURLConnection连接对象
	 * @return JSONObject
	 */
	public static JSONObject getResponse(HttpURLConnection conn) {
		try {
			// 获得响应状态码
			int code = conn.getResponseCode();
			// 获得输入流
			InputStream sendStatus = conn.getInputStream();
			// 获取数据长度
			int size = sendStatus.available();
			// 根据数据长度创建块读数组
			byte[] jsonBytes = new byte[size];
			// 读取数据
			sendStatus.read(jsonBytes);
			// 根据指定字符集解读为字符串
			String retMsg = new String(jsonBytes, "UTF-8");
			// 关闭输入流
			sendStatus.close();
			// 将数据字符串解析为JSON对象
			JSONObject result = JSONObject.parseObject(retMsg);
			result.put("ResponseCode", code);
			return result;
		} catch (IOException e) {
			System.out.println("响应数据解读失败！I/O异常：" + e.getMessage());
		}
		return null;
	}

	/**
	 * 关闭连接
	 * 
	 * @param conn
	 */
	public static void closeHttpURLConnection(HttpURLConnection conn) {
		// 断开连接
		conn.disconnect();
		conn = null;
	}

}
