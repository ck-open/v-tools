package com.ck.base.helper.base;

import java.io.UnsupportedEncodingException;

/**
 *
 */
public class StringBase {

    /**
     * String是不变对象，即：字符串对象一旦创建，内容不可改变想改变内容一定创建对象。
     * @author cyk
     *
     */
    public void stringDemo() throws UnsupportedEncodingException {
        /*
         * java建议创建字符串对象时使用字面量形式直接赋值。因为这样创建时java会重用已经创建过的字符串对象。
         * 使用字面量创建字符串对象时，JVM会首先检查常量池是否已经缓存过该内容的字符串对象，若有
         * 则直接重用该对象，若没有才创建该字符串对象并缓存它。
         */
        String s1 = "123abc";  // s1和s2 指向了常量池中同一对象，即地址相同
        String s2 = "123abc";
        String s3 = new String("123abc"); // 拷贝常量池中对象并重新在堆中创建一个对象，引用地址与s1、s2不同
        s1 = s1+"update";  // 将会在堆中创建新对象，s1将引用新对象地址。

        /*
         * 编译器有一个优化措施：当编译器在编译代码时发现一个计算表达式的参数都是字面量时，那么
         * 会直接计算，并将结果编译到class文件中。所以下面的代码会被编译器改为：String s5 = "123abc";
         */
        String s4 = "123"+"abc";
        String s = "123";
        String s5 = s + "abc";   // s4 和 s5 引用常量池同一对象

        int len = "我爱Java".length();  // 即返回String底层char[] 长度。
        int index = "thinking in java".indexOf("in"); // 查找给定字符串在当前字符串中的位置。若当前字符串中不含有该内容则返回值为-1.
        index = "thinking in java".indexOf("in",3); // 重载方法，从指定位置开始查找第一次出现的位置

        /*
         * String substring(int start ,int end)  截取当前字符串指定范围内的字符串。
         * 需要注意，java API有个特点，通常使用两个数字表示范围时，都是“含头不含尾”的。
         */
        String str = "www.thinking.com".substring(4,12);
        str = "  thinking ".trim();  // 去除当前字符串两边的空白字符
        char charTemp = str.charAt(0);     // 返回当前字符串中给定位置对应的char字符
        boolean isStart = str.startsWith("th"); // 判断字符串是否是以给定的字符串开始
        boolean isEnd = str.endsWith("king");  // 判断字符串是否是以给定的字符串结束
        String upperStr = str.toUpperCase();   // 将当前字符串中的英文部分转换为全大写
        String lowerStr = str.toLowerCase();   // 将当前字符串中的英文部分转换为全小写
        String valueOfStr = String.valueOf(123); // String提供了一组静态的valueOf方法，作用是将其他类型数据转换为字符串，比较常用的是将基本类型转换为字符串

        /*
         * 字符串支持正则表达式方法一： boolean matches(String regex)
         * 使用给定的正则表达式验证当前字符串是否满足格式要求
         * 注意：Java中无论正则表达式是否加了边界匹配符：“^”,"$"都是做全匹配验证。
         */
        String regex = "\\w+@\\w+(\\.[a-zA-Z]+)+"; // 邮箱正则
        boolean match = "yunke125@163.com".matches(regex);
        /*
         * 字符串支持正则表达式方法二： String[] split(String regex)
         * 按照满足给定的正则表达式的部分进行拆分当前字符串，将拆分后的每部分以数组形式返回
         */
        String[] array = "sd5d1f6df1sd2e".split("\\d+");
        /*
         * 字符串支持正则表达式方法三： String replaceAll(String regex,String str)
         * 将当前字符串中满足正则表达式的部分替换为给定字符串
         */
        regex = "(wqnmlgb|djb|mdzz|cnm|nc|mmp|sb)";
        String message = "wqnmlgb!你这个sb!你怎么这么nc!mmp!你个djb!".replaceAll(regex, "***");

        /*
         * 字符串提供了转换为字节的方法：
         * byte[] getBytes()将当前字符串按照系统默认字符集转换为一组字节
         * byte[] getBytes(String csn)使用指定的字符集将字符串转换为对应的一组字节
         *
         * 常见字符集：
         * GBK：国标编码，英文1字节，中文2字节
         * UTF-8：unicode的一种字符集，也成为万国码，包含世界上流行的语言对应的字符。英文1字节，中文3字节。
         * ISO8859-1:欧洲的一种字符集，不支持中文。
         */
        byte[] binary = "binary".getBytes("UTF-8");

        //将给定的字节数组中所有字节按照指定字符集还原为字符串
        String strBinary = new String(binary,"UTF-8");
    }

    /**
     * 由于字符串的设计及优化是针对重要性，所以字符串不适合频繁修改。
     * java设计了一个专门针对字符串修改操作的类：
     * StringBuilder，内部维护一个可变的字符数组，使用它修改字符串方便，并且开销小。
     * 不是线程安全的，执行速度快。
     * @author cyk
     */
    public void stringBuilder(){
        StringBuilder builder = new StringBuilder("努力学习java");
        builder.append(",为了找个好工作!");                            // 追加内容
        builder.replace(9, 16, "就是为了改变世界");    // 修改内容
        builder.delete(0, 8);                                        // 删除内容
        builder.insert(0, "活着");                        // 插入内容
        builder.reverse();                                           // 字符串倒序
    }

    /**
     * 与StringBuilder相同只是在底层方法中添加了 synchronized 锁
     */
    public void stringBuffer(){
        StringBuffer buffer = new StringBuffer();
    }
}
