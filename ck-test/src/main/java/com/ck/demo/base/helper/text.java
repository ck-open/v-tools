package com.ck.base.helper;

import java.io.*;

class Jad2Java {
    public static void main(String[] args) throws Exception {
        File srcDir = new File("java");
        if (!(srcDir.exists() && srcDir.isDirectory())) throw new Exception("目录不存在");
        File[] files = srcDir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".java");
            }
        });
        System.out.println(files.length);
        File destDir = new File("jad");
        if (!destDir.exists())
            destDir.mkdir();
        for (File f : files) {
            FileInputStream fis = new FileInputStream(f);
            String destFileName = f.getName().replaceAll(".javaS", "jad");
            FileOutputStream fos = new FileOutputStream(new File(destDir.getName()));
            copy(fis, fos);
            fis.close();
            fos.close();
        }
    }

    private static void copy(InputStream ips, OutputStream ops) throws Exception {
        int len = 0;
        byte[] buf = new byte[1024];
        while ((len = ips.read(buf)) != -1) {
            ops.write(buf, 0, len);
        }
    }
}