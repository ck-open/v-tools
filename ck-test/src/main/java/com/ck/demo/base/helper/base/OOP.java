package com.ck.base.helper.base;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 面向对象相关<br>
 *     兽棋案例<br>
 *         讲述了Java面向对象继承、封装、多态
 */
public class OOP {

    public static void main(String[] args) {
        Random random = new Random();

        // java 多态  同一引用指向了不同对象
        BeastChess beastChess1 = new BeastChess();
        BeastChess beastChess2 = new BeastChess();
        while (beastChess1.beast.size()!=0&&beastChess2.beast.size()!=0){
            int index1 = beastChess1.beast.size();
            int index2 = beastChess2.beast.size();
            if (random.nextBoolean()){
                beastChess1.attackBase(beastChess2.beast.get(random.nextInt(index2)),beastChess1.beast.get(random.nextInt(index1)));
            }else{
                beastChess2.attackBase(beastChess1.beast.get(random.nextInt(index1)),beastChess2.beast.get(random.nextInt(index2)));
            }
        }

        if (beastChess1.beast.size()>0){
            System.out.println("红方获胜！ 胜利者：");
            for (ZooClass z:beastChess1.beast){
                System.out.println(z.getName());
            }
        }else{
            System.out.println("蓝方获胜！ 胜利者：");
            for (ZooClass z:beastChess2.beast){
                System.out.println(z.getName());
            }
        }
    }
}

/**
 * 兽棋<br>
 *     封装了斗兽棋游戏规则和成员
 */
class BeastChess{
    /**
     * 兽棋成员
     */
    List<ZooClass> beast = new ArrayList<ZooClass>();

    /**
     * 已死亡的兽棋
     */
    List<ZooClass> die = new ArrayList<>();

    public BeastChess(){
        beast.add(new Elephant(46,1));
        beast.add(new Lion(50,1));
        beast.add(new Tiger(84,1));
        beast.add(new Leopard(62,1));
        beast.add(new Wolf(65,1));
        beast.add(new Dog(60,1));
        beast.add(new Cat(58,1));
        beast.add(new Mouse(100,1));
    }

    /**
     * 基本攻击规则
     * @param zoo1 攻击者
     * @param zoo2 被攻击者
     */
    public void attackBase(ZooClass zoo1, ZooClass zoo2){
        zoo1.attack(zoo2);
        this.deleteDie();
    }

    /**
     * 兽棋攻击规则
     * @param zoo1 攻击者
     * @param zoo2 被攻击者
     */
    public void attack(ZooClass zoo1, ZooClass zoo2){
        if (zoo1.getClass().equals(zoo2.getClass())){
            // 如果类型一样则互相攻击
            zoo1.attack(zoo2);
            zoo2.attack(zoo1);
        }
        if (Elephant.class.equals(zoo1.getClass())){
            // 攻击者是大象
            if (!Mouse.class.equals(zoo2.getClass())){ // 被攻击者不是老鼠则攻击
                zoo1.attack(zoo2);
            }
        }else if (Lion.class.equals(zoo1.getClass())){
            // 攻击者是狮子
            if (!Elephant.class.equals(zoo2.getClass())){ // 被攻击者不是大象则攻击
                zoo1.attack(zoo2);
            }
        }else if (Tiger.class.equals(zoo1.getClass())){
            // 攻击者是老虎
            if (!Elephant.class.equals(zoo2.getClass())&&!Lion.class.equals(zoo2.getClass())){ // 被攻击者不是大象和狮子则攻击
                zoo1.attack(zoo2);
            }
        }else if (Leopard.class.equals(zoo1.getClass())){
            // 攻击者是豹子
            if (!Elephant.class.equals(zoo2.getClass())&&!Lion.class.equals(zoo2.getClass())
                    &&!Tiger.class.equals(zoo2.getClass())){ // 被攻击者不是大象、狮子、老虎则攻击
                zoo1.attack(zoo2);
            }
        }else if (Wolf.class.equals(zoo1.getClass())){
            // 攻击者是狼
            if (Dog.class.equals(zoo2.getClass())||
                    Cat.class.equals(zoo2.getClass())||Mouse.class.equals(zoo2.getClass())){ // 是狗、猫或者老鼠则攻击
                zoo1.attack(zoo2);
            }
            zoo1.attack(zoo2);
        }else if (Dog.class.equals(zoo1.getClass())){
            // 攻击者是狗
            if (Cat.class.equals(zoo2.getClass())||Mouse.class.equals(zoo2.getClass())){ // 是猫或者老鼠则攻击
                zoo1.attack(zoo2);
            }
        }else if (Cat.class.equals(zoo1.getClass())){
            // 攻击者是猫
            if (Mouse.class.equals(zoo2.getClass())){ // 是老鼠则攻击
                zoo1.attack(zoo2);
            }
        }else if (Mouse.class.equals(zoo1.getClass())){
            // 攻击者是老鼠
            if (Elephant.class.equals(zoo2.getClass())){ // 被攻击者是大象则攻击
                zoo1.attack(zoo2);
            }
        }
        this.deleteDie();
    }

    private void deleteDie(){
        // 清除已死亡的兽棋
        for (ZooClass zc : beast){
            if (zc.getHealthPoint()==0){
                die.add(zc);
            }
        }
        beast.removeAll(die);
    }

}


/**
 * 动物接口<br>
 * 约定动物的共有属性
 */
interface Zoo {
    /**
     * 奔跑，行动
     *
     * @param x 目标x坐标
     * @param y 目标y坐标
     */
    void run(int x, int y);

    /**
     * 奔跑，行动<br>
     * 方法重载（Overloaded）
     *
     * @param x 目标x坐标
     * @param y 目标y坐标
     * @param z 目标高度
     */
    void run(int x, int y, int z);

    /**
     * 喂养
     *
     * @param feedValue 喂养值
     */
    void feed(int feedValue);

    /**
     * 睡觉
     *
     * @param sleepTime 睡觉时长
     */
    void sleep(int sleepTime);

    /**
     * 承受伤害
     *
     * @param zoo 攻击的对象
     */
    void takeDamage(ZooClass zoo);

    /**
     * 发器攻击
     *
     * @param zoo 攻击的对象
     */
    void attack(ZooClass zoo);

}

/**
 * 抽象动物超类<br>
 * 实现动物共有行为和属性
 */
abstract class ZooClass implements Zoo {
    /**
     * 动物名称
     */
    private String name;

    /**
     * 生命
     */
    private int life = 1;

    /**
     * 健康值
     */
    private int healthPoint = 100;

    /**
     * 休眠剩余时间
     */
    private int sleepValue = 0;

    /**
     * 饥饿值
     */
    private int hungerValue = 0;

    /**
     * 伤害值
     */
    private int harm = 0;

    @Override
    public void run(int x, int y) {
        System.out.println("动物：" + this.name + " 奔跑了起来！");
    }

    @Override
    public void run(int x, int y, int z) {
        System.out.println("动物：" + this.name + "飞行了起来！");
    }

    @Override
    public void feed(int feedValue) {
        this.hungerValue = this.hungerValue - feedValue;
        if (this.hungerValue < 0) {
            this.hungerValue = 0;
        }
        System.out.println("动物：" + this.name + " 进行了喂养！");
    }

    @Override
    public void sleep(int sleepTime) {
        this.sleepValue += sleepTime;
        System.out.println("动物：" + this.name + " 进行睡觉了！");
    }

    @Override
    public void takeDamage(ZooClass zoo) {
        this.healthPoint -= zoo.getHarm();
        if (this.healthPoint <= 0) {
            if (this.life > 1) {
                this.life--;
                this.healthPoint += 100;
            } else {
                this.healthPoint = 0;
            }
        }
        System.out.println("动物：" + this.name + "承受了来自：" + zoo.getName() + " 的攻击！ 命：" + this.life + "  健康值：" + this.healthPoint);
    }

    @Override
    public void attack(ZooClass zoo) {
        zoo.takeDamage(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    public int getHarm() {
        return harm;
    }

    public void setHarm(int harm) {
        this.harm = harm;
    }

    public int getSleepValue() {
        return sleepValue;
    }

    public void setSleepValue(int sleepValue) {
        this.sleepValue = sleepValue;
    }

    public int getHungerValue() {
        return hungerValue;
    }

    public void setHungerValue(int hungerValue) {
        this.hungerValue = hungerValue;
    }
}


/**
 * 大象
 */
class Elephant extends ZooClass {
    public Elephant(int Harm, int Life) {
        this.setName("大象");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}

/**
 * 狮子
 */
class Lion extends ZooClass {
    public Lion(int Harm, int Life) {
        this.setName("狮子");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}

/**
 * 老虎
 */
class Tiger extends ZooClass {
    public Tiger(int Harm, int Life) {
        this.setName("老虎");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}

/**
 * 豹子
 */
class Leopard extends ZooClass {
    public Leopard(int Harm, int Life) {
        this.setName("猎豹");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}
/**
 * 狼
 */
class Wolf extends ZooClass {
    public Wolf(int Harm, int Life) {
        this.setName("豺狼");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}

/**
 * 动物狗，继承动物类
 */
class Dog extends ZooClass {
    public Dog(int Harm, int Life) {
        this.setName("狗狗");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}

/**
 * 猫咪
 */
class Cat extends ZooClass {
    public Cat(int Harm, int Life) {
        this.setName("猫咪");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}

/**
 * 老鼠
 */
class Mouse extends ZooClass {
    public Mouse(int Harm, int Life) {
        this.setName("老鼠");
        this.setLife(Life);
        this.setHarm(Harm);
    }
}
