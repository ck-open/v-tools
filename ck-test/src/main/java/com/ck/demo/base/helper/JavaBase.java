package com.ck.base.helper;

import java.lang.annotation.Retention;
import java.util.ArrayList;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public class JavaBase {

	public static void main(String[] args) {
		System.err.println("Java 基础");

		final Value< String > value = new Value<>();

		System.out.println(value.getOrDefault( null, Value.defaultValue() ));

		Optional< String > firstName = Optional.of( "Tom" );
		System.out.println( "First Name is set? " + firstName.isPresent() );
		System.out.println( "First Name: " + firstName.orElseGet( () -> "[none]" ) );
		System.out.println( firstName.map( s -> "Hey " + s + "!" ).orElse( "Hey Stranger!" ) );
		System.out.println();
	}
}


 class Value< T > {
	public static< T > T defaultValue() {
		return null;
	}

	public T getOrDefault( T value, T defaultValue ) {
		return ( value != null ) ? value : defaultValue;
	}
}


class Annotations {
	@Retention( RetentionPolicy.RUNTIME )
	@Target( { ElementType.TYPE_USE, ElementType.TYPE_PARAMETER } )
	public @interface NonEmpty {
	}

	public static class Holder< @NonEmpty T > extends @NonEmpty Object {
		public void method() throws @NonEmpty Exception {
		}
	}

	@SuppressWarnings( "unused" )
	public static void main(String[] args) {
		final Holder< String > holder = new @NonEmpty Holder< String >();
		@NonEmpty Collection< @NonEmpty String > strings = new ArrayList<>();
	}
}