package com.ck.base.helper.API.io;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * java.io.RandomAccessFile
 * RAF是专门用来读写文件数据的类，它基于指针进行读写。可对文件内容进行编辑操作。
 *
 * @author cyk
 */
public class RandomAccessFileBase {

    /**
     * RandomAccessFile常用构造方法：
     * RandomAccessFile(File file,String mode)
     * RandomAccessFile(String path,String mode)
     * 第一个参数用于指定要进行读写操作的文件。
     * 第二个参数为操作模式，常用：
     * r：只读模式，仅对文件进行读取操作
     * rw：读写模式，对文件数据可读也可写
     */
    public void RAFBase() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("./RAFBase.base", "rw");
        /*
         * void write(int d)
         * 向文件中写入一个字节，将给定的int值所对应的2进制的低八位写入文件
         *
         * 块写：
         * void write(byte[] data)
         * 通过RAF将给定字节数组所有字节一次性写出
         *
         * void write(byte[] data,int offset,int len)
         * 将给定的字节数组从下标offset处的连续len个字节一次性写出
         */
        randomAccessFile.write(1);
        /*
         * int read()
         * 从文件当前指针位置读取1个字节，读取后以int形式返回。若读取到文件末尾，则返回值为-1.
         *
         * 块读：
         * int read(byte[] data)
         * 通过当前RAF一次性读取给定的字节数组总长度的字节量，并装入到该数组中，返回值为实际读取到
         * 的字节量。若返回值为-1，则表示本次没有读取到任何数据（已经是文件末尾了）
         */
        int d = randomAccessFile.read();

        randomAccessFile.close();  // 关闭文件
    }

    /**
     * RAF提供了读写基本类型数据的相关方法。以及操作指针的
     * 相关方法
     * RAF是基于指针进行读写数据的，即：RAF总是在指针指向的文件字节位置进行读或写，
     * 并且读写后指针会自动向后移动到下一个字节准备读写。
     *
     * @author cyk
     */
    public void RAFPointer() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("./RAFBase.base", "rw");

        long pointer = randomAccessFile.getFilePointer();  // 获取当前RAF的指针位置
        randomAccessFile.seek(0);   // 将指针移动到指定位置 void seek(long pos)
        /*
         * int readInt()
         * 连续读取4个字节，还原该int值
         * 若读取过程中发现到了文件末尾，则抛出：EOFException   "EOF  end of file"
         */
        int data = randomAccessFile.readInt();
        randomAccessFile.close();
    }

    /**
     * 使用RAF实现用户注册功能
     * 程序启动后，要求用户输入以下信息：
     * 用户名，密码，昵称，年龄。其中年龄是int值，其他都是字符串。
     * 然后将该用户信息写入user.dat文件中保存。
     * <p>
     * 每条记录占用固定的100字节。
     * 其中用户名，密码，昵称各占32字节。年龄是int值固定4字节。
     * 字符串留白可以方便的编辑字符串内容，而且每条记录的格式也统一。
     *
     * @author cyk
     */
    public void userWriteFileByRAF() {
        System.out.println("欢迎注册！");
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入用户名：");
        String name = sc.nextLine();
        System.out.println("请输入密码：");
        String password = sc.nextLine();
        System.out.println("请输入昵称：");
        String nickname = sc.nextLine();
        System.out.println("请输入年龄：");
        int age = Integer.parseInt(sc.nextLine());

        try {
            RandomAccessFile raf = new RandomAccessFile("user.dat", "rw");
            //将指针移动到文件末尾，以便追加操作
            raf.seek(raf.length());

            //写用户名
            byte[] data = name.getBytes("UTF-8");
            data = Arrays.copyOf(data, 32);
            raf.write(data);

            //写密码
            data = password.getBytes("UTF-8");
            data = Arrays.copyOf(data, 32);
            raf.write(data);

            //写昵称
            data = nickname.getBytes("UTF-8");
            data = Arrays.copyOf(data, 32);
            raf.write(data);

            //写年龄
            raf.writeInt(age);
            System.out.println("pos:" + raf.getFilePointer());
            raf.close();
            System.out.println("注册成功！");
        } catch (IOException io) {
            System.out.println("注册失败！" + io.getMessage());
        }
    }

    /**
     * 显示所有注册用户信息
     *
     * @author cyk
     */
    public void showAllUserInfo() {
        try {
            RandomAccessFile raf = new RandomAccessFile("user.dat", "r");
            for (int i = 0; i < raf.length() / 100; i++) {
                //读用户名
                byte[] data = new byte[32];
                raf.read(data);
                String name = new String(data, "UTF-8").trim();
                //读密码
                raf.read(data);
                String password = new String(data, "UTF-8").trim();
                //读昵称
                raf.read(data);
                String nickname = new String(data, "UTF-8").trim();
                //读年龄
                int age = raf.readInt();
                System.out.println(name + "," + password + "," + nickname + "," + age);
            }
            raf.close();
        } catch (IOException io) {
            System.out.println("用户信息显示失败！" + io.getMessage());
        }
    }

    /**
     * 修改昵称操作
     * <p>
     * 要求输入要修改昵称的用户名，以及新昵称。然后将其修改。
     * 若输入的用户名无效，则提示没有此用户。
     *
     * @author cyk
     */
    public void updateUser() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("请输入要修改昵称的用户名：");
            String name = sc.nextLine();
            System.out.println("请输入新昵称：");
            String nickname = sc.nextLine();
            RandomAccessFile raf = new RandomAccessFile("user.dat", "rw");
            boolean check = false;
            for (int i = 0; i < raf.length() / 100; i++) {
                raf.seek(i * 100);
                byte[] data = new byte[32];
                raf.read(data);
                String username = new String(data, "UTF-8").trim();
                if (username.equals(name)) {
                    raf.seek(i * 100 + 64);
                    data = nickname.getBytes("UTF-8");
                    data = Arrays.copyOf(data, 32);
                    raf.write(data);
                    System.out.println("修改成功！");
                    check = true;
                    break;
                }
            }
            if (!check) {
                System.out.println("此用户不存在！");
            }
        } catch (IOException io) {
            System.out.println("用户信息更新失败！" + io.getMessage());
        }
    }
}
