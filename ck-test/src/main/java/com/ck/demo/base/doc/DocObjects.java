
/*java.util.Objects 是jdk1.7添加的一个工具类，进行一些常用的检查操作。
此类包含static实用程序方法，用于操作对象或在操作前检查某些条件。
这些实用程序包括null或null方法，用于计算对象的哈希代码，返回对象的字符串，比较两个对象，以及检查索引或子范围值是否超出范围。

方法目录
    equals:
        public static boolean equals(Object a, Object b)
        相等判断 都为null或a.equals(b) 返回true。

    deepEquals:
        public static boolean deepEquals(Object a, Object b)
        相等判断 都为null或Arrays.deepEquals0(a, b)数组深度判断为true 返回true。

    hashCode:
        public static int hashCode(Object o)
        返回对象Hash值，参数为null则返回0。

    hash:
        public static int hash(Object... values)
        返回多个对象Hash值，Arrays.hashCode(values)。

    toString:
        public static String toString(Object o)
        返回String.valueOf(o)方法值。

    toString:
        public static String toString(Object o, String nullDefault)
        返回对象的toString()值，如果参数对象为null则返回指定的默认值。

    compare:
        public static <T> int compare(T a, T b, Comparator<? super T> c)
        比较器，两个值相等则返回0，否则返回传入的比较器执行结果

    isNull:
        public static boolean isNull(Object obj)
        传入的参数对象==null则返回true

    nonNull:
        public static boolean nonNull(Object obj)
        传入的参数对象!=null则返回true

    requireNonNull:
        public static <T> T requireNonNull(T obj)
        传入的参数对象为null则抛出NullPointerException异常，
        public static <T> T requireNonNull(T obj, String message)
        传入的参数对象为null则抛出NullPointerException异常，并执行异常的message值，

    requireNonNull:
        public static <T> T requireNonNull(T obj, Supplier<String> messageSupplier)
        传入的参数对象为null则抛出NullPointerException异常
        例如：
                Objects.requireNonNull(fruit,()->{
                    System.out.println(fruit);
                    return "参数为NUll";
                });
*/

package com.ck.demo.base.doc;
import java.util.Comparator;
import java.util.Objects;

public class DocObjects {
    public static void main(String[] args) {

        System.out.println(Objects.equals("", null));
        System.out.println(Objects.deepEquals("", null));
        System.out.println(Objects.hashCode("123456"));
        System.out.println(Objects.hash("123456"));
        System.out.println(Objects.toString(null));
        System.out.println(Objects.toString(null, "空指针"));
        System.out.println(Objects.compare(1, 2, Comparator.comparing(i -> i)));
        System.out.println(Objects.isNull(null));
        System.out.println(Objects.nonNull(null));
        try {
            Objects.requireNonNull(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Objects.requireNonNull(null, "空值");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
