package com.ck.base.helper.API.collection;

import java.util.*;
import java.util.HashSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * java.util.Collection
 * 集合
 * 集合是一组数据结构，与数组一样，用来保存一组元素。
 * 集合提供了一系列操作元素的相关方法，使用更方便。
 * <p>
 * Collection是所有集合的顶级接口，规定了所有集合都应当
 * 具备的方法。
 * 有两个常用的派生接口：
 * java.util.List:可重复集且有序
 * java.util.Set:不可重复集
 * 元素是否重复是依靠元素自身的equals比较的结果。
 *
 * @author cyk
 */
public class CollectionBase {

    /**
     * 集合基础
     */
    public void collectionBase() {
        // Collection 作为集合的超类可以应用任何集合对象(map属于散列表)
        Collection collArray = new ArrayList();  // 创建有序集合 底层由可变数组实现、元素可重复、可为null
        Collection collSet = new HashSet();      // 创建无序集合 底层由Hash表实现(实际上就是封装了一个Map)、不可重复、可为null
        boolean result = collArray.add("向集合添加元素"); // 集合添加元素会返回Boolean结果表示添加是否成功

        boolean empty = collSet.isEmpty(); // 判断当前集合是否为空集（不含有任何元素）
        collSet.clear();  //清空集合

        /*
         * contains方法会用参数对象与集合现有元素顺序
         * 进行equals比较（元素自身的equals方法），若有
         * 返回值为true的则集合认为包含该元素。
         * 所以元素自身equals方法直接决定集合判断包含
         * 该元素的结果。
         */
        boolean contains = collSet.contains("判断集合是否包含此元素");

        /*
         * boolean containsAll(Collection c)
         * 判断当前集合是否包含给定集合中的所有元素
         */
        contains = collSet.containsAll(collArray);

        /*
         * 删除集合元素
         * boolean remove(E e)
         * 将给定的元素从集合中删除，需要注意，该方法依然是依靠
         * 元素equals比较。
         */
        collSet.remove("删除本元素");

        /*
         * boolean removeAll(Collection c)
         * 删除当前集合中与给定集合的共有元素（删除交集部分）
         */
        collSet.removeAll(collArray);

        /*
         * boolean addAll(Collection c)
         * 将给定集合中的所有元素添加到当前集合中。添加后
         * 当前集合元素发生改变则返回true。
         */
        boolean addAll = collSet.addAll(collArray);
    }

    /**
     * 集合迭代器遍历集合<br>
     * 对于Collection这个层面而言，下面的集合实现类不都是有序的。所有无法通过如数组那样操作下标来遍历玩家。
     * Collection提供了统一的遍历集合元素的方式：迭代器模式
     * <p>
     * Iterator iterator()
     * 该方法可以获取1个用于遍历当前集合的迭代器实现类。
     * <p>
     * java.util.Iterator
     * 迭代器接口，规定了迭代器遍历集合的相关操作方法，而不同的
     * 集合都实现了一个用于遍历自身元素的迭代器实现类。我们无需
     * 记住这些实现类的名字，用多态的角度当他们是Iterator看待
     * 即可。
     * 迭代器遍历集合的规则为：问，取，删
     * 其中删除元素不是必须操作。
     */
    public void iterator() {
        Collection<String> c = new ArrayList<String>();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");

        Iterator it = c.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            System.out.println(str);
            if ("#".equals(str)) {
                // 迭代器要求在遍历的过程中不要通过集合的方法增删元素，否则会抛出异常。c.remove(str);
                it.remove(); // 迭代器的remove方法删除的是通过next
            }
        }

        /*
         * JDK1.5之后推出了一个新的特性：增强for循环，也称为：新循环，for each。
         * 不取代传统for循环的工作，新循环只用来遍历集合或数组使用。
         * 新循环并非新的语法，编译器认可而不是JVM认可。
         * 编译器在编译源程序时若发现时用新循环遍历数组时会将代码改为使用普通for循环遍历。
         * 新循环遍历集合，会被编译器改为使用迭代器遍历集合。所以在使用新循环遍历集合过程中，不要通过集合的方法增删元素。否则新循环遍历集合会抛出异常。
         */
        for (String str : c) {
            System.out.println(str);
        }
    }

    /**
     * java.util.List
     * List集合：可重复集合，并且有序，特点是可以通过下标操作元素常用实现类：
     * java.util.ArrayList:内部由数组实现，查询性能更好
     * java.util.LinkedList:内部由链表实现，增删元素性能更好，尤其首尾增删元素。
     */
    public void listCollection() {
        List<String> list = new ArrayList<String>();
        list.add(1, "element");          // 指定下标位置添加元素
        String element = list.set(0, "elementUpdate");  // 将给定元素设置到指定位置，返回值为原位置对应的元素（替换元素操作）
        element = list.get(0);                         // 返回指定下标位置的元素
        element = list.remove(0);               // 删除指定下标位置元素并将其返回

        List<String> sonList = list.subList(0, 1);      // 获取当前集合中指定范围内的子集。(只是得到了对应位置的引用，对象还是同一个)
        list.subList(0, 1).clear();                     // 删除集合中的范围元素

        List<String> listArray = Arrays.asList(new String[]{"a", "b"});   // 数组转集合，由于数组是定长的，所有该集合不允许增删元素。否则会抛出异常
        String[] array = list.toArray(new String[listArray.size()]);      // 集合转数组

    }

    /**
     * 集合工具类：Collections
     * <p>
     * java.util.Collections是集合的工具类，提供了一系列操作集合的静态方法。
     * 其中sort方法可以对List集合进行自然排序 即：从小到大排序集合元素。
     */
    public void collectionUtil() {
        List<Integer> list = new ArrayList<Integer>();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            list.add(rand.nextInt(100));
        }

        /*
        * Collections的sort(List list)方法是按照集合元素自身的
        * 比较规则比较后由小到大进行排序的。而有时集合元素已经实现了比较规则，但是该规则不满足我们排序需求。
        */
        Collections.sort(list);    // 排序
        Collections.shuffle(list); // 乱序

        /*
         * Collection提供了一个重载的sort方法：static void sort(List list,Comparator com)
         *   该排序方法要求再传入一个参数，该参数是一个比较器，该方法会使用该比较器的比较规则对集合元素进行比较并按照比较的结果进行排序。
         *
         * 当排序含有自定义类型元素的集合时，不建议使用上述的sort方法。而应当使用重载的需要传入额外比较器的sort方法：
         * static void sort(List list,Comparator com)
         * 因为该方法不要求集合元素必须实现Comparable接口，就必须重写抽象方法compareTo，那么该方法对我们集合元素Point就没有侵入性。
         *
         * 侵入性：当我们需要调用某个功能方法时，该方法要求我们的程序为其修改其他的额外代码，修改的越多，侵入性越强。
         * 侵入性对后期系统维护不利，在开发中应当尽量避免。
         */
        Comparator<Integer> com = new Comparator<Integer>() {
            /*
            * 该方法的返回值为int值，该值不关心具体取值，关心的是取值范围。
            *   当返回值>0:当前对象大于参数对象  this>0
            *   当返回值<0:当前对象小于参数对象
            *   当返回值=0:两个对象相等
             */
            public int compare(Integer o1, Integer o2) {
                return o1-o2;
            }};
        Collections.sort(list,com);
    }

    /**
     * 队列：java.util.Queue
     *
     * 队列是经典的数据结构，可以保存一组元素，但是存取元素
     * 必须遵循先进先出原则。
     * @author cyk
     *
     */
    public void queueCollection(){
        /*
         * LinkedList实现了队列接口，因为链表本身可以保存一组元素，并且特点是首尾增删元素效率高，
         * 这正好满足队列特点。
         */
        Queue<String> queue = new LinkedList<String>();

        boolean result = queue.offer("element");   // 入队操作,并返回入队是否成功
        String element = queue.poll();      //  出队操作，获取队首元素，获取后该元素即从队列中被删除
        element = queue.peek();       // 引用队首元素，即获取队首元素引用，并不删除

        /*
         *由于队列接口继承自Collection，所以可以使用迭代器遍历队列，并且不会影响队列中的元素
         */
        for(String s : queue) {
            System.out.println(s);
        }
    }

    /**
     * 双端队列 java.util.Deque
     *
     * Deque接口继承自Queue，双端队列是两端都可以做进出队操作的队列。并且提供了明确方向的进出队方法。
     *   常见实现类：java.util.LinkedList
     * @author cyk
     */
    public void dequeCollection(){
        Deque<String> deque = new LinkedList<String>();

        deque.offer("elementFirst");    // 从队首入队
        deque.offerLast("elementLast"); // 从队尾入队

        String element = deque.pollFirst(); // 从队首获取元素
        element = deque.pollLast();  // 从队尾获取元素
    }

    /**
     * 栈
     * 栈也是一个经典的数据结构，可以保存一组元素，但是存取必须遵循先进后出原则。
     * 通常使用栈是为了实现“后退”这样的功能。
     *
     * Deque双端队列可以实现栈操作，并且为栈也提供了对应的入栈与出栈方法（push,pop)
     * @author cyk
     *
     */
    public void stackCollection(){
        Deque<String> stack = new LinkedList<>();

        stack.push("element");  // 元素入栈
        stack.pop();  // 元素出栈
    }

    /**
     * 线程安全的集合
     * 集合常用的实现类有：ArrayList,LinkedList,HashSet
     * 它们都不是线程安全的，在多线程并发操作时会出现并发安全问题。
     * Collections提供了一组静态方法，可以将现有的集合转换为一个线程安全的。
     * @author cyk
     */
    public void synchronizedCollection(){
        /*
         * API手册上也有说明，对于一个线程安全的集合而言也不与迭代器遍历该集合的操作做互斥。所以若存在
         * 多个线程遍历以及增删元素同时操作的情况，要自行维护遍历与增删元素的互斥。
         */
        List<String> list = new ArrayList<>();
        list = Collections.synchronizedList(list);  // 将给定的list集合转换为一个线程安全的List

        Set<String> set = new HashSet<>();
        set = Collections.synchronizedSet(set);     // 将给定的set集合转换为一个线程安全的Set

        /*
         * 阻塞队列，双缓冲队列
         * 阻塞队列是并发安全的队列，在多线程情况使用。
         * 并且由于内部由双缓冲实现，并发效率比较好。
         * BlockingQueue
         * 常用实现类：
         * ArrayBlockingQueue
         * LinkedBlockingQueue
         */
        LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>();
        /*
         * 阻塞式入队
         */
        try {
            /*
             * 对于有界队列，有可能会存在队列满了存不进去的情
             * 况，这时可以使用阻塞入队操作，指定超时时间以及
             * 时间单位，在规定时间内若可以将元素入队则方法执
             * 行完毕，否则会抛出超时异常。在等待的过程中会发
             * 生该线程阻塞现象。
             */
            queue.offer("three", 500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}


/**
 * 泛型
 * JDK1.5之后推出的一个新的特性，泛型也称为参数化类型，意图
 * 在于将一个类中属性，方法参数，返回值等类型的定义权交给了
 * 使用者，这样大大提高了代码的灵活性。
 * 泛型的原型是Object，即：在使用时若不明确指定泛型的实际类型时，
 * 则默认按照原型Object使用。
 * 泛型是编译器认可，而非虚拟机，面试常见问题。
 *
 * @author cyk
 */
class Pointers<X, Y> {
    private X x;
    private Y y;

    public Pointers(X x, Y y) {
        super();
        this.x = x;
        this.y = y;
    }

    public X getX() {
        return x;
    }

    public void setX(X x) {
        this.x = x;
    }

    public Y getY() {
        return y;
    }

    public void setY(Y y) {
        this.y = y;
    }

    public String toString() {
        return "(" + x + "," + y + ")";
    }
}