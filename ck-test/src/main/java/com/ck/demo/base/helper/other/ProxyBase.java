package com.ck.base.helper.other;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 动态代理示范
 */
public class ProxyBase {
	public static void main(String[] args) {
		InvocationHandler h = new InvocationHandler() {
			Foo target=new F();
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				synchronized (proxy) {
					System.out.println("动态代理！调用方法" + method);
					Object obj = method.invoke(target, args);
					return null;
				}
			}
		};

		Object obj = Proxy.newProxyInstance(
				ProxyBase.class.getClassLoader(),
				  new Class[]{Foo.class,Aoo.class},
				h);
		
		if(obj instanceof Foo) {
			Foo o = (Foo)obj;
			o.f();
		}
		if(obj instanceof Aoo) {
			Aoo a = (Aoo)obj;
			a.a();
		}
		
	}
}

interface Foo{
	void f();
}
interface Aoo{
	void a();
}

class F implements Foo{
	public void f() {
		System.out.println("foo的f方法執行了");
	}
}

class A implements Aoo{
	public void a() {
		System.out.println("aoo的a方法執行了");
	}
	
}