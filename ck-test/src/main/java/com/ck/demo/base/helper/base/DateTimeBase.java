package com.ck.base.helper.base;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * 日期时间相关操作基础
 */
public class DateTimeBase {

    /**
     * java.util.Date   date的每一个实例用于表示一个具体的时间。Date date = new Date();
     * 	 由于Date存在时区以及千年虫问题，所以大部分方法在1.1版本就被声明为过时的。 现在使用Date通常仅用于表示时间。
     * 	 long getTime()该方法返回一个long值，该值就是当前date内部维护的long值，表示自1970年1月1日 00：00：00到
     * 		当前Date表示的时间之间所经过的毫秒。例如：long time=date.getTime();
     * 		也可以表示明天这一刻：time=time+1000*60*60*24;
     * 		也可以设置一个long值，让Date表示该时间：date.setTime(time); 例：date1.setTime(l+10000l*24*60*60*1000);
     */
    public void dateBase(){
        Date date = new Date();
        long millisecond = date.getTime();  // 表示自1970年1月1日 00：00：00到当前Date表示的时间之间所经过的毫秒。
    }

    /**
     * java.text.SimpleDateFormat  SDF是用来在String与Date之间相互转换的API。
     * 	1、SDF转换基于一个给定的日期格式。 "yyyy-MM-dd HH:mm:ss E a"
     * 	   例如：SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E a");
     * 	2、String format(Date date) 该方法会将给定的Date按照当前SDF指定的日期格式转换为对应的字符串 Date-->String
     * 	   例如：String line=sdf.format(date);
     * 	3、Date parse(String str) 将一个字符串解析为Date对象，将给定的字符串按照当前SDF指定的日期格式解析为Date对象。
     * 	   例：SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     * 		   Date date=sdf.parse(“2008-08-08 20：08：08”);
     */
    public void simpleDateFormatBase(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss  E a");
        String timeStr = simpleDateFormat.format(new Date());
        System.out.println(timeStr);
        try {
            Date time = simpleDateFormat.parse("2018-08-08 08:08:08 ");
        } catch (ParseException e) {
            //字符串转换日期格式，需要处理解析异常
            e.printStackTrace();
        }
    }

    /**
     * 1、使用日历类可以对时间进行一系列的操作。Dade大部分操作时间的方法都被声明为过时的，取而代之的是使用Calendar的相关操作。
     * 2、Calendar是一个抽象类，定义了日历类操作时间的功能，常用实现类为：java.util.GregorianCalendar, 即：阳历。
     * 3、Calendar提供了一个静态方法：getInstance,该方法可以获取一个当前系统所在地区适用的实现类，大部分 地区返回的也是阳历实现类。
     * 4、Calendar默认创建也表示当前系统时间。但是toString返回的字符串不能直观的体现。
     * 5、Calendar提供了与Date之间互转的方法：Date getTime() 将当前Calendat表示的日期以date实例形式返回。
     * 6、void setTime(Date date) 是当前Calendar表示给定的Date所表示的日期。
     *       例如：Date date=calendar.getTime(); //转date
     *           calendar.setTime(date);  //date转Calendar
     * 7、Calendar提供了一个方法：int get(int field)该方法可以获取当前Calendar给定的时间分量所对应的值，不同的时间分量对应的
     *               数字不同，但是无需记住这些数字，因为Calendar为这些数字量提供了对应的常量。
     *               1.获取年: int year=calendar.get(Calendar.YEAR);
     * 		         2.需要注意，月从0开始，即0表示1月，1表示2月...  int month=calendar.get(Calendar.MONTH)+1;
     * 		         3.与“天”相关的时间分量有：DATE：月中的天、DAY_OF_MONTH:月中的天，与DATE一致、DAY_OF_WEEK:周中的天、DAY_OF_YEAR:年中的天
     * 			        int day=calendar.get(Calendar.DATE);
     * 		         4.获取当前周：int dow=calendar.get(Calendar.DAY_OF_WEEK);  返回值1对应的是周日！
     * 8、void set(int fild,int value)设置指定的时间分量。   例如：calendar.set(year, month, date, hourOfDay, minute);
     * 		1.设置年：calendar.set(Calendar.YEAR, 2008);
     * 		2.设置月：calendar.set(Calendar.MONTH, 7);
     * 		3.设置天：calendar.set(Calendar.DATE, 8);
     * 		4.设置时间：calendar.set(Calendar.HOUR_OF_DAY, 20); calendar.set(Calendar.MINUTE, 8); calendar.set(Calendar.SECOND, 8);
     * 		5.设置周：calendar.set(Calendar.DAY_OF_WEEK, 4); 自动以该日期最近的周
     *
     * 9、Calendar计算时间的操作：void add(int field,int amount) 对指定的时间分量加上给定的值，若给定的值为负数，则是减去给定的时间。
     * 		1. calendar.add(Calendar.YEAR, 3);//加3年
     * 		2. calendar.add(Calendar.MONTH, 2); //加2个月
     * 		3. calendar.add(Calendar.DAY_OF_YEAR, 25); //加25天
     * 		4. calendar.add(Calendar.DATE, -7);//减7天
     * 		6. calendar.add(Calendar.WEEK_OF_YEAR, -2);  //减2周
     */
    public void calendarBase(){
        /*
         * 计算商品促销日期 输入一个商品的生产日期，格式：yyyy-MM-dd 在输入该商品的保质期，
         * 天数经过程序计算，输出该商品的促销日期，格式：yyyy-MM-dd 促销日期规则：该商品过期日期前两周的周三
         */
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入商品生产日期：");
        String birthdays = sc.nextLine(); // 生产日期
        System.out.println("请输入保质期天数：");
        int bestday = Integer.parseInt(sc.nextLine()); // 保质期天数
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 设定日期格式
        Calendar calendar = Calendar.getInstance();  // 促销日期
        try {
            Date birthday = sdf.parse(birthdays);    // 生产日期
            calendar.setTime(birthday);              // 设置成Date时间
            calendar.add(Calendar.DATE, bestday);    // 过期日
            calendar.add(Calendar.WEEK_OF_YEAR, -2); // 减2周
            calendar.set(Calendar.DAY_OF_WEEK, 3);   // 设置成周3
            System.out.println("促销日期为：" + sdf.format(calendar.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
