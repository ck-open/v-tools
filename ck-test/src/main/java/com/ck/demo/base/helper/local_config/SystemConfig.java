package com.ck.base.helper.local_config;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class  SystemConfig {
	/**
	 * 配置信息
	 */
	private static Map<String, String> map;
	
	/**
	 * 数字格式
	 */
	private static final String REG_NUMBER = "-?\\d+(\\.\\d+)?";
	
	/**
	 * 单例对象
	 */
	private static SystemConfig systemConfig;

	private  SystemConfig() {
		super();

		// 读取properties内容并转换成map形式
		Properties props = new Properties();
		map = new HashMap<String, String>();
		try {
			//用calss加载器加载配置文件
			InputStream in = getClass().getResourceAsStream("SystemConfig.properties");
			props.load(in);
			//获得所有参数名并遍历
			@SuppressWarnings("rawtypes")
			Enumeration en = props.propertyNames();
			while (en.hasMoreElements()) {
				//将下个遍历的参数名存为key
				String key = (String) en.nextElement();
				//根据此key获得value
				String property = props.getProperty(key);
				map.put(key, property);
				//System.out.println(key + "." + property);
			}
		} catch (Exception e) {
			System.out.println("配置文件错误！");
		}
	}
	
	
	/**
	 * 根据给定的key 获得整数value
	 * @param key
	 * @return
	 */
	public Integer getInteger(String key) {
		if(key==null||"".equals(key))
			return null;
		String str = map.get(key);
		return str.matches(REG_NUMBER)?Integer.parseInt(str):null;
	}
	
	/**
	 * 根据给定的key获得小数value
	 * @param key
	 * @return
	 */
	public Double getDouble(String key) {
		if(key==null||"".equals(key))
			return null;
		String str = map.get(key);
		return str.matches(REG_NUMBER)?Double.parseDouble(str):null;
	}
	
	/**
	 * 根据给定的key 获得字符串value
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		if(key==null||"".equals(key))
			return null;
		String str = map.get(key);
		return str;
	}
	
	/**
	 * 获得系统配置信息对象
	 * @return
	 */
	public static SystemConfig getSystemConfig() {
		if(systemConfig==null)
			systemConfig = new SystemConfig();
		return systemConfig;
	}
	
	
	/**
	 * 测试Main函数
	 * @param args
	 */
	public static void main(String[] args) {
		SystemConfig get = getSystemConfig();
		System.out.println("启动了"+get.getString("DBurl"));
	}

}
