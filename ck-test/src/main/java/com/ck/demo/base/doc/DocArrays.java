/*java.util.Arrays
    Arrays位于java.util包下(Arrays是一个操作数组的工具类)
    Arrays包含各种操作数组的方法(例如排序和搜索)。该类还包含一个静态工厂，允许将数组视为列表。
    Arrays类里的方法都是静态方法可以通过Arrays.方法名()直接调用

    sort：对数组进行升序排序，排序后 ，数组中存放的是排序后的结果。对称快速排序
        public static void sort(int[] a)
            将指定的数组按升序进行排序。
        public static void sort(int[] a, int fromIndex, int toIndex)
            将数组的指定范围按升序排序。要排序的范围从索引 fromIndex(包含)扩展到索引 toIndex(不包含)。
            如果 fromIndex = toIndex，则要排序的范围为空。
        public static void sort(Object[] a)
            将指定的数组按升序进行排序。
        public static void sort(Object[] a, int fromIndex, int toIndex)
            将数组的指定范围按升序排序。要排序的范围从索引 fromIndex(包含)扩展到索引 toIndex(不包含)。
            如果 fromIndex = toIndex，则要排序的范围为空。
        public static <T> void sort(T[] a, Comparator<? super T> c)
            将指定的数组按指定的比较器进行排序。
        public static <T> void sort(T[] a, int fromIndex, int toIndex, Comparator<? super T> c)

    parallelSort：
        此方法也可对对象或基本类型数组进行排序。与sort()类似，它也有两个变体排序方法，以实现对全数组和部分数组排序。多线程 合并排序
        方法parallelSort()在功能上与前者是不同的。也就是说，与使用单个线程对数据进行顺序排序的sort()不同，它使用并行的sort-merge(合并排序)算法。它将数组拆分成子数组，子数组本身进行排序，然后合并。
        对于执行的并行任务，它使用ForkJoin池。
        但是我们要知道，它只在满足特定条件时才使用并行特性。如果数组大小小于或等于8192，或者处理器只有一个核心，那么它使用顺序Dual-Pivot快速排序算法。否则，它使用并行排序。

        public static void parallelSort(int[] a)
        public static void parallelSort(int[] a, int fromIndex, int toIndex)
        public static <T extends Comparable<? super T>> void parallelSort(T[] a)
        public static <T extends Comparable<? super T>> void parallelSort(T[] a, int fromIndex, int toIndex)
        public static <T> void parallelSort(T[] a, Comparator<? super T> cmp)
        public static <T> void parallelSort(T[] a, int fromIndex, int toIndex, Comparator<? super T> cmp)

    parallelPrefix：
        public static <T> void parallelPrefix(T[] array, BinaryOperator<T> op)
            使用提供的函数，并行地将给定数组的每个元素累积到相应的位置。
            例如，如果数组最初包含代码[2,1,0, 3]，并且操作执行加法，则返回时数组包含@code[2,3,3,6]}。对于大型数组，并行前缀计算通常比顺序循环更有效。
        public static <T> void parallelPrefix(T[] array, int fromIndex, int toIndex, BinaryOperator<T> op)
        public static void parallelPrefix(int[] array, IntBinaryOperator op)
        public static void parallelPrefix(int[] array, int fromIndex, int toIndex, IntBinaryOperator op)

    binarySearch：对排序好的数组，采用二分查找的方式查找某个元素，可以在整个数组中查找，也可以在某个范围内查找。
        public static int binarySearch(int[] a, int key)
        public static int binarySearch(int[] a, int fromIndex, int toIndex, int key)
        public static int binarySearch(Object[] a, Object key)
        public static int binarySearch(Object[] a, int fromIndex, int toIndex, Object key)
        public static <T> int binarySearch(T[] a, T key, Comparator<? super T> c)
        public static <T> int binarySearch(T[] a, int fromIndex, int toIndex, T key, Comparator<? super T> c)

    equals：判断两个数组中的元素是否一一对应相等。
        public static boolean equals(int[] a, int[] a2)

    fill：给定特定值val，使整个数组中或者某下标范围内的元素值为val。
        public static void fill(int[] a, int val)
        public static void fill(int[] a, int fromIndex, int toIndex, int val)
        public static void fill(Object[] a, Object val)
        public static void fill(Object[] a, int fromIndex, int toIndex, Object val)

    copyOf：将原始数组的元素，复制到新的数组中，可以设置复制的长度（即需要被复制的元素个数）。
        public static <T> T[] copyOf(T[] original, int newLength)
        public static <T,U> T[] copyOf(U[] original, int newLength, Class<? extends T[]> newType)

    copyOfRange：将某个范围内的元素复制到新的数组中。
        public static <T> T[] copyOfRange(T[] original, int from, int to)
        public static <T,U> T[] copyOfRange(U[] original, int from, int to, Class<? extends T[]> newType)

    asList：
        public static <T> List<T> asList(T... a)
            该方法适用于对象型数据的数组（String、Integer…）
            该方法不建议使用于基本数据类型的数组（byte,short,int,long,float,double,boolean）
            该方法将数组与List列表链接起来：当更新其一个时，另一个自动更新
            不支持add()、remove()、clear()等方法

    hashCode：
        public static int hashCode(int a[])
        public static int hashCode(Object a[])

    deepHashCode：
        public static int deepHashCode(Object a[])

    deepEquals：
        public static boolean deepEquals(Object[] a1, Object[] a2)

    toString：
        public static String toString(int[] a)
        public static String toString(Object[] a)

    deepToString：
        public static String deepToString(Object[] a)

    setAll：这个方法相当于stream.map会挨个元素遍历执行方法
        public static <T> void setAll(T[] array, IntFunction<? extends T> generator)
        public static void setAll(int[] array, IntUnaryOperator generator)
        public static void setAll(long[] array, IntToLongFunction generator)

    parallelSetAll：这个方法相当于stream.map会挨个元素遍历执行方法。 多线程并发
        public static <T> void parallelSetAll(T[] array, IntFunction<? extends T> generator)
        public static void parallelSetAll(int[] array, IntUnaryOperator generator)

    spliterator：
        public static <T> Spliterator<T> spliterator(T[] array)
        public static <T> Spliterator<T> spliterator(T[] array, int startInclusive, int endExclusive)
        public static Spliterator.OfInt spliterator(int[] array)
        public static Spliterator.OfInt spliterator(int[] array, int startInclusive, int endExclusive)

    stream：把数组转换成stream,然后可以使用java8的stream特性
        public static <T> Stream<T> stream(T[] array)
        public static <T> Stream<T> stream(T[] array, int startInclusive, int endExclusive)
        public static IntStream stream(int[] array)
        public static IntStream stream(int[] array, int startInclusive, int endExclusive)
*/

package com.ck.demo.base.doc;
public class DocArrays{
    public static void main(String[] args) {

    }
}

