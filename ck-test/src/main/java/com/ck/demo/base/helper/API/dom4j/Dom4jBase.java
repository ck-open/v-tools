//package com.ck.base.helper.API.dom4j;
//
//import org.dom4j.*;
//import org.dom4j.io.SAXReader;
//import org.dom4j.io.XMLWriter;
//
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.List;
//
///**
// * dom4j-full.jar  == dom for java
// * 使用该类，需要导入jar包，在pom.xml文件中末尾添加以下内容：
// * 		<dependency>
// * 			<groupId>dom4j</groupId>
// * 			<artifactId>dom4j</artifactId>
// * 			<version>1.6.1</version>
// * 		</dependency>
// */
//public class Dom4jBase {
//
//    /**
//     * SAX  解析方式Simple API for XML  文件流读入过程即解析过程。优点读取速度快，内存消耗低。缺点不能对节点修改。
//     * DOM  解析方式Document Object Model 把xml文件在内存中构造树形结构对象，可以遍历和修改每个节点。如果文件较大时内存有压力，解析时间长。
//     * 1、使用DOM解析XML的大致步骤：
//     *      1：创建SAXReader     例： SAXReader reader=new SAXReader();
//     *      2：使用SAXReader读取指定的xml文档并生成Document对象。这一步就是XML文档全部读取完毕并以Document对象形式保存在內存中。
//     *         有两个方法： 1.  Document doc=reader.read(new File("文件地址/emplist.xml"));
//     *                     2. Document doc=reader.read(new FileInputStream("文件地址/emplist.xml"));
//     *      3：通过Document获取根元素   例：Element root=doc.getRootElement();
//     * 	    4：根据XML文档的结构逐级获取子元素以达到遍历XML文档的目的。 例：List<Element> empList=root.elements("emp");
//     * 2、提供的方法：
//     * 		 * Document提供了获取根元素的方法：Element getRootElement()
//     * 		 * Element的每一个实例用于表示xml文档中的一个元素(一对标签)。
//     * 		 * Element提供了获取其表示的元素的相关方法常用的有：
//     * 		 * String getName()   获取当前标签的名字
//     * 		 * String getText()   获取当前标签中间的文本，例如：<a>获取的文本</a>
//     * 		 * String elementText(String name) 直接获取指定标签下的文本
//     * 		 * Element element(String name) 获取当前标签指定名字的子标签。
//     * 		 * List elements()  获取当前标签中所有子标签
//     * 		 * List elements(String name)  获取当前标签中所有同名(指定的名字)子标签
//     * 		 * 获取<emp id="xx">标签的属性id对应的值，Element提供了获取属性的方法：Attribute的每个实例用于表示某个标签中的一个属性，
//     * 		    其有两个常用方法：
//     * 		        String getName()  获取属性名字
//     * 		        String getValue() 获取属性的值
//     * 		    Element也提供了快捷获取属性值的方法： String attributeValue(String name)  获取当前标签中指定名字对应属性的值
//     */
//    public void parseXML() throws DocumentException {
//        // 1、创建SAXReader流对象
//        SAXReader reader = new SAXReader();
//
//        // 2、读取XML文件到Document对象，需要处理文档异常
//        Document document = reader.read("./XML.xml");
//
//        // 3、获取文档对象中根元素标签对象
//        Element element = document.getRootElement();
//
//        // 4、获取element标签下的所有子标签列表，也可以指定子标签名字：List elements(String name)
//        List<Element> elements = element.elements();
//
//        String elementName = element.getName();           // 获取当前标签的名字
//        String text = element.getText();                  // 获取当前标签中间的文本，例如：<a>获取的文本</a>
//        text = element.elementText("elementName");     // 直接获取指定标签下的文本
//        Element sonElement = element.element("");         // 获取当前标签指定名字的子标签。
//        Object parameterValue = element.attributeValue("paramName");    // 根据属性名获取属性值
//
//        // 获取标签中的属性对象,根据属性名
//        Attribute attribute = element.attribute("paramName");
//    }
//
//    /**
//     * DOM自动生成XML文档
//     * 	生成一个XML文档的大致步骤：
//     * 		1：创建一个Document实例，表示一个空白文档。
//     * 		2：向Document实例中添加根元素。
//     * 		3：按照预定生成的XML文档结构向根元素中逐级添加子元素以及对应数据。
//     * 		4：创建XmlWriter。
//     * 		5：通过XmlWriter将Document写出以生成XML文档。
//     */
//    public void createDocument() throws IOException {
//        // 1、创建文档对象
//        Document document = DocumentHelper.createDocument();
//
//        /*
//         * 2.创建文档的根标签 Document提供了添加根元素的方法：Element addElement(String name)
//         *   向当前文档中添加给定名字的元素，由于一个文档中只能有一个根元素，所以该 方法只能调用一次。
//         *   该方法会返回一个Element实例。该实例就表示添加的元素，以便对其继续追加子元素等操作。
//         */
//        Element root = document.addElement("root");
//        root.addAttribute("paramName","paramValue"); // 向标签中添加属性
//
//        // 3.追加子标签 将子标签添加到<root>中。
//        Element sonElement = root.addElement("son");
//        sonElement.addText("这是子标签的文本内容！");  // 添加标签内容
//
//        // 4、创建XmlWriter 文件输出流 ,OutputFormat:输出文件格式。
//        XMLWriter xmlWriter = new XMLWriter(new FileOutputStream("./XML.xml"));
//
//        // 5、调用文件输出流将doc写入到文件
//        xmlWriter.write(document);
//        xmlWriter.close();
//        System.out.println("XML.xml文档写出完毕！");
//    }
//
//}
