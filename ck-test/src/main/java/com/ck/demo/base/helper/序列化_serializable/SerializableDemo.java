package com.ck.base.helper.序列化_serializable;

import java.io.*;
import java.util.Date;

/**
 * Java中要实现将对象保存起来持久化，需要让对象实现Serializable接口，这样就能将java对象用二进制流保存并恢复。下面我将以保存到文件和保存到mysql来进行解析。
 */
public class SerializableDemo {
}


/**
 * 实现可序列化接口
 */
class Person implements Serializable {
    private String name;  //名字
    private int year;     //年龄
    private String city;  //城市
    private Date birth;   //生日
}


/**
 * 文件流操作类
 */
class FileHelper {


    /**
     * 将Java对象序列化为byte[]
     * @param obj
     * @return
     * @throws Exception
     */
    public static byte[] obj2byte(Object obj) throws Exception {
        byte[] ret = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(baos);
        out.writeObject(obj);
        out.close();
        ret = baos.toByteArray();
        baos.close();
        return ret;
    }

    /**
     * 将byte[]反序列化为Java对象
     * @param bytes
     * @return
     * @throws Exception
     */
    public static Object byte2obj(byte[] bytes) throws Exception {
        Object ret = null;
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ObjectInputStream in = new ObjectInputStream(bais);
        ret = in.readObject();
        in.close();
        return ret;
    }
}