package com.ck.base.helper.java8_character_新特性.java_util_function;

import java.util.Arrays;
import java.util.function.Supplier;

/**
 * java.util.function.Supplier<T> 接口仅包含一个无参方法：T get()。
 *      用于获取一个泛型数据指定类型的对象数据。由于这个是一个函数式接口，
 *      这也就意味着对应的Lambda表达式需要“对外提供”一个符合泛型类型的数据对象。
 *
 * Supplier<T> 接口被称之为生产型接口，指定泛型的接口是什么类型，那么get()方法就会产生什么类型的数据
 */
public class SupplierDemo {

    // 定义一个方法，方法的参数传递Supplier<T> 接口，泛型指定String类型，get()方法就返回一个String
    public static String getSting(Supplier<String> function){
        return function.get();
    }

    /**
     * 使用Supplier函数式接口作为参数类型，通过Lambda表达式求出int数组的最大值
     * @param supplier
     * @return
     */
    public static int getMaxInt(Supplier<Integer> supplier){
        return supplier.get();
    }

    public static void main(String[] args) {
        // 调用getString方法，参数是一个Supplier函数式接口，所以可以传递一个Lambda表达式
       String  supplier = getSting(()->"Supplier函数式接口生产一个字符串测试！");
       System.out.println(supplier);

       // Supplier求取int数组最大值
        int[] ints = {1,4,8,96,5,23,4,82,};
        // 调用getMaxInt() ， 传递Lambda表达式
        int max = getMaxInt(()->{
            // 获取数组最大值并返回
            // 定义一个变量记录Max值
            int maxInt = ints[0];
            for (int i : ints){
                maxInt = maxInt<i?i:maxInt;
            }
            return maxInt;
        });
        System.out.println("数组："+ Arrays.toString(ints)+"\n最大值为："+max);

    }
}
