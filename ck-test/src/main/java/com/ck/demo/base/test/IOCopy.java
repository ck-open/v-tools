package com.ck.demo.base.test;

import java.io.*;

public class IOCopy {

	public static void main(String[] args) throws IOException {
		
		File file = new File("E:\\华硕-灵耀S2-原装系统Ghost备份\\Ghost安装器.exe");
		copy(file,"E:\\华硕-灵耀S2-原装系统Ghost备份\\");
	}
	
	public static void copy(File file,String path) throws IOException {
		InputStream is = new FileInputStream(file);
		OutputStream os = new FileOutputStream(path+"Copy_"+file.getName());
		
		byte[] data = new byte[1024*10];
		int len = -1;
		while((len=is.read(data))!=-1) {
			os.write(data, 0, len);
		}
		is.close();
		os.close();
		System.out.println("复制完毕！");
	}
	
}
