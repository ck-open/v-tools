package com.ck.base.helper.other;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

/**
 * 动态代理 List
 * @param <E>
 */
public class ListHandler<E> implements InvocationHandler {
	private List<E> target; //代理的目标对象

	public ListHandler() {
		target = new ArrayList<E>();
	}

	public ListHandler(List<E> list) {
		target = list;
	}

	@Override
	public Object invoke(Object proxy, // 引用Proxy Api创建的代理对象
						 Method method, // 引用了正在执行的代理的方法
						 Object[] args // 引用了传递给代理方法的参数
	) throws Throwable {
		// 假设，调用了add方法，则method=“add()”
		//args 是调用add是传递的参数
		synchronized (proxy) {
			//在目标对象上执行add方法，传递target参数
			System.out.println("调用："+method);
			Object value = method.invoke(target, args);
			return value;
		}
	}
}

class ProxyDemo {
	public static void main(String[] args) {
		ListHandler<String> handler = new ListHandler<String>();
		//利用动态代理Proxy创建List接口的对象
		//loader：当前的类加载器，利用当前类获得即可
		//interfaces：数组，储存需要实现的系列接口
		//handler：用于处理代理方法执行时候的逻辑
		List<String> list  = (List<String>) Proxy.newProxyInstance(ProxyDemo.class.getClassLoader(), new Class[]{List.class}, handler);
		list.add("Tom");
		list.add("Jerry");
		System.out.println(list.size());
		System.out.println(list);

	}
}
