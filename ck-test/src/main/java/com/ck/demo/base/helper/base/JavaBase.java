package com.ck.base.helper.base;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Java基础
 * @author soft01 作者
 * @version 1.0 版本号
 * @see String 参考类
 * @since JDK1.5 起始版本号
 */
public class JavaBase {

    public static void main(String[] args) {
        JavaBase javaBase = new JavaBase();
        javaBase.detail();
    }

    /**
     * Java基础细节<br>
     * <li>递增：A++ 和 ++A</li>
     */
    public void detail() {
        // 递增 A++ 和 ++A 的故事
        int increase = 0;
        System.out.println("A++ 对变量A的增加在变量使用之后：" + (increase++) + "\n++A 对变量A的递增在变量使用之前：" + (++increase));

        //&& 和 & 的故事
        int index = 0;
        boolean flag = false && index++!=0;
        System.out.println("&& 称为 断路与 即左侧表达式为 false 则右侧不再进行判断。例如 false && i++!=0 则i并没有递增："+index);

        // 数组创建方式
        int[] arr1=new int[10];   // 声明数组长度为10
        int[] arr2= {2,5,8};      // 直接赋值创建数组
        int[] arr3=new int[] {2,5,8}; // 创建数组并赋值
        int[][] table = new int[10][10]; // 创建二维数组

        Arrays.sort(arr1);  //调用sort方法排序
        arr1=Arrays.copyOf(arr1, arr1.length+1); //数组扩容

    }

    /**
     * 获取控制台输入的字符串
     * @return
     */
    public String consoleInput() {
        Scanner scan = new Scanner(System.in); // 获取控制台数据输入扫描
//        int scanInt = scan.nextInt();
        String scanStr = scan.nextLine();

        return scanStr;
    }

    /**
     * 数字取余数
     *
     * @param number
     * @param divisor
     * @return
     */
    public int remainder(int number, int divisor) {
        // 判断闰年
        // boolean flag=(year%4==0&&year%100!=0)||year%400==0;
        // 可以判断数字奇偶
        if (number % 2 == 0) {
            System.out.println("数字：" + number + "  为 偶数！");
        } else {
            System.out.println("数字：" + number + "  为 奇数！");
        }
        return number % divisor;
    }

    /**
     * Java 数据类型
     */
    public void DataType() {
        // 基本类型 - 正数类型
        byte b = 1;  // byte 1子节     范围：2^7-1 ~ -2^7     位数：8bit
        short s = 2; // short 2子节    范围：2^15-1 ~ -2^15   位数：16bit
        int i = 4;   // int 4子节      范围：2^31-1 ~ -2^31   位数：32bit
        long l = 8L;  // long 8子节    范围：2^63-1 ~ -2^63   位数：64bit

        // 基本类型 - 浮点数
        float f = 0.4f; // float 4子节   范围：3.4028235E38 ~ 1.4E-45              位数：32bit
        double d = 0.8; // double 8子节  范围：1.7976931348623157E308 ~ 4.9E-324   位数：64bit

        // 基本类型 - 字符/逻辑
        char c = '2';       // char 2子节     范围：0~65535          位数：16bit  char在Java中是16位的，因为Java用的是Unicode
        boolean boo = true; // boolean 1子节  范围：true / false     位数：16bit

        // 包装类型   JDK 1.7 以后实现自动拆装箱   变量不需要valueOf() 即可自动转换成包装类型
        Byte byteImpl = Byte.valueOf(b);
        Short shortImpl = Short.valueOf(s);
        Integer integer = Integer.valueOf(i);
        Long longImpl = Long.valueOf(l);
        Float floatImpl = Float.valueOf(f);
        Double doubleImpl = Double.valueOf(d);
        Character character = Character.valueOf(c);
        Boolean booleanImpl = Boolean.valueOf(boo);
        // 自动拆箱
        i = integer.intValue();
        d = doubleImpl.doubleValue();

    }


}
