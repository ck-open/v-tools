package com.ck.demo.ssh;

import com.alibaba.fastjson.JSONObject;
import com.jcraft.jsch.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * SSH 远程操作类
 *
 * 需要依赖包：
 * <p>
 *     <dependency>
 *             <groupId>com.jcraft</groupId>
 *             <artifactId>jsch</artifactId>
 *             <version>0.1.54</version>
 *      </dependency>
 * </p>
 *
 * @author cyk
 * @since 2020-11-03
 */
public class SSHUtil {

    private static final Logger logger = Logger.getLogger("FTPUtil");

    private static final Map<String, Integer> timesMap = new ConcurrentHashMap();


    public static void main(String[] args) throws SftpException {
        String host = "10.69.10.237";
        int port = 22;
        String username = "weblogic";
        String password = "weblogic123";
        String directory = "/home/weblogic/com.ck//jk";
        String urla = "E:\\jk\\";// 存在本地的路径
//        String fileName = "06b3f677-a4b4-4174-968f-4b3cbae7c29d.idg_sign.pdf";

//        Session session = SSHUtil.connect(host, port, username, password);
        Session session = SSHUtil.connect(host, port, "root", null,"e:\\ssh\\id_rsa","qazwsx123");

        ChannelSftp sftp = SSHUtil.getSFTP(session);
        Vector vector = sftp.ls(directory);

        if (vector != null) {
            vector.forEach(item -> {
                JSONObject object = JSONObject.parseObject(JSONObject.toJSONString(item));
                String fileName = object.get("filename").toString();
                if (fileName.endsWith(".zip")) {
                    System.out.println(fileName);
                }
            });
        }

        SSHUtil.sftpClose(sftp);

        System.out.println("测试");
    }

    /**
     * 创建SSH连接 session
     * @param host  服务器地址
     * @param port  端口号
     * @param username  用户名
     * @param password  密码
     * @return
     */
    public static Session connect(String host, int port, String username, String password) {
        return SSHUtil.connect(host,port,username,password,null,null);
    }


    /**
     * 创建SSH连接 session
     * @param host  服务器地址
     * @param port  端口号
     * @param username  用户名
     * @param password  密码
     * @param keyFilePath 密钥路径
     * @param passphrase 密钥的密码
     * @return
     */
    public static Session connect(String host, int port, String username, String password,String keyFilePath, String passphrase) {
        ChannelSftp sftp = null;
        Session sshSession = null;
        try {
            JSch jsch = new JSch();

            // 如果私钥不为空则设置私钥
            if (keyFilePath != null) {
                if (passphrase != null) {
                    jsch.addIdentity(keyFilePath, passphrase);// 设置私钥
                } else {
                    jsch.addIdentity(keyFilePath);// 设置私钥
                }
                logger.info("连接sftp，私钥文件路径：" + keyFilePath);
            }

            //获取sshSession  账号-ip-端口
            sshSession = jsch.getSession(username, host, port);
            logger.info("SSH Session created......");

            //添加密码
            if (password != null) {
                sshSession.setPassword(password);
            }
            Properties sshConfig = new Properties();
            //严格主机密钥检查,设置host-key验证，可选值：(ask | yes | no)
            sshConfig.put("StrictHostKeyChecking", "no");
            sshSession.setConfig(sshConfig);
            sshSession.setConfig("kex", "diffie-hellman-group1-sha1");

            // 设置超时时间为
            sshSession.setTimeout(300000);
            //开启sshSession链接
            sshSession.connect();
            logger.info("SSH Session Connected to " + host + "  OK");
//            logger.info("Opening Channel SFTP......");
//            //获取sftp通道
//            Channel channel = sshSession.openChannel("sftp");
//            //开启
//            channel.connect();
//            sftp = (ChannelSftp) channel;
//            logger.info("Opening Channel SFTP OK");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sshSession;
    }

    /**
     *  创建SFTP通道
     * @param host  服务器地址
     * @param port  端口号
     * @param username  用户名
     * @param password  密码
     * @param keyFilePath 密钥路径
     * @param passphrase 密钥的密码
     * @return
     */
    public static ChannelSftp getSFTP(String host, int port, String username, String password,String keyFilePath, String passphrase) {
        Session sshSession =  SSHUtil.connect(host,port,username,password,keyFilePath,passphrase);
        return SSHUtil.getSFTP(sshSession);
    }
    /**
     * 创建SFTP通道
     * @param sshSession
     * @return
     */
    public static ChannelSftp getSFTP(Session sshSession) {
        //获取sftp通道
        Channel channel = null;
        ChannelSftp sftp = null;
        try {
            logger.info("Opening Channel SFTP......");
            channel = sshSession.openChannel("sftp");
            //开启
            channel.connect();
            sftp = (ChannelSftp) channel;
            logger.info("Opening Channel SFTP OK");

        } catch (JSchException e) {
            logger.log(Level.SEVERE, "Opening Channel SFTP Error  " + e.getMessage());
            e.printStackTrace();
        }
        return sftp;
    }

    /**
     * 实现SFTP数据上传
     *
     * @return void
     * @参数：@param directory 需要下载文件的路径,必须是客户端设置默认直接读取到的文件夹("/Download")
     * @参数：@param uploadFile 下载地址("D:\\SFTP")
     * @参数：@param sftp
     */
    public static boolean upload(String directory, String uploadFile, ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            logger.info("uploadFile：------>" + uploadFile + "<-----");
            File file = new File(uploadFile);
            logger.info("file.getPath：------>" + file.getPath() + "<-----");
            sftp.put(new FileInputStream(file), file.getName());
            logger.info("文件上传成功");
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "文件上传失败" + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 函数功能说明:实现sftp下载
     *
     * @return void
     * @参数：@param fileName  文件名(aa.zip)
     * @参数：@param directory    需要下载文件的路径,必须是客户端设置默认直接读取到的文件夹("/Download")
     * @参数：@param downloadFile 下载到本地的地址("D:\\SFTP")
     * @参数：@param sftp
     */
    public static boolean download(String fileName, String directory, String downloadFile,
                                   ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            SftpATTRS attr = sftp.stat(fileName);
            long fileSize = attr.getSize();
            sftp.get(fileName, downloadFile);
            logger.info("下载成功<" + fileName + ">");
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "下载失败<" + fileName + ">");
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 删除文件
     *
     * @param directory  要删除文件所在目录
     * @param deleteFile 要删除的文件
     * @param sftp
     */
    public static boolean delete(String directory, String deleteFile, ChannelSftp sftp) {
        try {
            sftp.cd(directory);
            sftp.rm(deleteFile);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 校验目录是否存在
     *
     * @param path
     * @param sftp
     * @return
     */
    public static boolean isExistDir(String path, ChannelSftp sftp) {
        boolean isExist = false;
        try {
            SftpATTRS sftpATTRS = sftp.lstat(path);
            isExist = true;
            return sftpATTRS.isDir();
        } catch (Exception e) {
            if (e.getMessage().toLowerCase().equals("no such file")) {
                isExist = false;
            }
        }
        return isExist;
    }


    /**
     * @return
     * @description 退出Sftp服务器登录
     **/
    public static void sftpClose(ChannelSftp channelSftp) {
        if (channelSftp != null) {
            if (channelSftp.isConnected()) {
                channelSftp.disconnect();
            }
        }
    }

    /**
     * 关闭session
     */
    public static void sessionClose(Session sshSession) {
        if (sshSession != null) {
            if (sshSession.isConnected()) {
                sshSession.disconnect();
                sshSession = null;
            }
        }
    }



}
