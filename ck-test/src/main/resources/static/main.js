// http://layui.xhcen.com/demo/grid.html
// 菜单
import {menus, globalData} from "./Config.js";

window.$ = layui.jquery;

window.onload = function () {
    login.check();
    setLoginUserInfo(globalData.user)
    renderMenu(menus);
}

/**
 * 函数用于递归生成菜单
 * @returns {*|jQuery|HTMLElement} 菜单DOM对象
 * @param menus
 */
function renderMenu(menus) {
    /**
     * 函数用于递归生成菜单
     * @param menu  菜单数据
     * @param parent 父级菜单
     * @returns {*|jQuery|HTMLElement} 菜单DOM对象
     */
    function renderMenu(menu, parent) {
        const ul = parent || $('<ul class="layui-nav layui-nav-tree"  style="text-align: justify;"></ul>');
        $.each(menu, function (i, item) {
            if (item.isShow === false) {
                return;
            }
            const li = !parent ? $('<li class="layui-nav-item"></li>') : $('<dd></dd>');
            const a = $(`<a href="javascript:" class="home-menu-active" data-id="${item.id}" data-type="tabAdd" style="font-size: 1.1em;">
                        <i class="layui-icon" style="font-size: ${!parent ? 1.5 : 1.2}em; color: #1E9FFF;">${item.icon ? item.icon : "&emsp;"}</i>&emsp;${item.name}</a>`).appendTo(li);

            if (item.children && item.children.length > 0) {
                const dl = $(`<dl class="layui-nav-child"></dl>`).appendTo(li);
                renderMenu(item.children, dl);
            }

            li.appendTo(ul);
        });
        return ul;
    }

    // 使用上面的函数生成菜单
    const menuUl = renderMenu(menus, null);

    // 将生成的菜单添加到页面
    $('.layui-side-scroll .layui-nav').empty().append(menuUl.html());
    // 渲染菜单
    layui.element.init();

    /**
     * 菜单点击事件
     * 当点击有 homeMenuActive 属性的标签时，触发点击事件
     */
    $('.home-menu-active').on('click', function () {
        const clickTag = $(this); // 获取当前点击的a标签
        const dataId = clickTag.attr("data-id");
        tabs.open(dataId);
    });
}


/**
 * 导航页面根 触发事件
 */
window.tabs = {
    // 指定面包屑容器 lay-filter 的值  class="layui-tab" lay-filter="tabs"
    tabBoxLayFilter: 'tabs',
    tabOpens: new Set(), // 已打开的tab页
    menus: menus,

    /**
     * 新增一个Tab项
     * @param id 规定的id 标签中data-id的属性值
     * @param name 标题
     * @param path 路由地址
     * @param url tab页面的地址
     */
    tabAdd: function (id, name, path, url) {
        if (!url.endsWith('.html')) {
            console.error(`${url} 不是html文件`);
        }

        layui.element.tabAdd(this.tabBoxLayFilter, {
            id: id,
            title: name,
            content: `
                    <i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 8em; color: #1E9FFF;position: absolute;top: 50%;left: 46%;">&#xe63d;</i>
                    <iframe id="${id}" data-frameid="${id}" src="${url}" style="width:100%;height:100%;border: 0;position: relative;z-index: 2;display: none;">
                    </iframe>`
        })
        // 最后将状态更改推到地址栏
        window.history.pushState({id}, "");
        this.tabOpens.add(id);
        // 触发window上的resize事件
        window.dispatchEvent(new Event('resize'));
    },

    /**
     * 切换到指定Tab项
     * 根据传入的id传入到指定的tab项
     * @param id 规定的id 标签中data-id的属性值
     */
    tabChange: function (id) {
        layui.element.tabChange(this.tabBoxLayFilter, id);
    },

    /**
     * 删除指定Tab项
     * @param ids 规定的id 标签中data-id的属性值 ids是一个数组，里面存放了多个id，调用tabDelete方法分别删除
     */
    tabDelete: function (...ids) {
        if (ids.length > 0) {
            for (let id of ids) {
                layui.element.tabDelete(this.tabBoxLayFilter, id);
                this.tabOpens.delete(id);
            }
        }
    },

    /**
     * 打开指定Tab项
     * 未打开则打开，已打开则切换到该tab项
     * @param id  规定的id 标签中data-id的属性值
     * @param tabData 新打开的tab 需要传递过去的数据
     */
    open: function (id, tabData) {
        let menu = this.findMenu(id);
        if (menu) {
            if (menu.url && menu.name && menu.url.length > 0 && menu.name.length > 0) {
                let name = menu.name;
                if (tabData && tabData.tabName && tabData.tabName.length > 0) {
                    name = tabData.tabName;
                    id = id + '_' + name;
                }
                if (this.tabOpens.size === 0 || !this.tabOpens.has(id)) {
                    this.tabAdd(id, name, menu.path, menu.url);
                    tabOnload(id, tabData);
                }
                this.tabChange(id);
            }
        } else {
            console.error(`菜单配置错误，未找到对应 id:${id}`);
        }
    },
    /**
     * 重新打开指定Tab项
     * @param id  规定的id 标签中data-id的属性值
     * @param tabData 新打开的tab 需要传递过去的数据
     */
    reopen: function (id, tabData) {
        if (tabData && tabData.tabName && tabData.tabName.length > 0) {
            this.tabDelete((id + '_' + tabData.tabName));
        } else {
            this.tabDelete(id);
        }
        open(id, tabData);
    },

    /**
     * 获取菜单
     * @param id id
     * @returns {null} 菜单
     */
    findMenu(id) {
        let menu = null;
        this.menus.forEach(item => {
            if (item.id === id) {
                menu = item;
                return;
            }
            if (item.children) {
                item.children.forEach(child => {
                    if (child.id === id) {
                        menu = child;
                    }
                })
            }
        })
        return menu;
    },
};

/**
 * tab的onload事件
 * @param id
 * @param tabData 新打开的tab 需要传递过去的数据
 */
function tabOnload(id, tabData) {
    tabRightMenu(id);
    const loadingTag = $('.layui-tab-content > div:last-child > i');   // 加载中 动画标签
    const iframeTag = $('.layui-tab-content > div:last-child > iframe'); // iframe标签
    // 子页面继承父页面的样式
    JSUtils.iframeExtent(iframeTag.get(0), (iframeWindow, iframeDocument) => {
        // 子页面继承父页面的样式
        JSUtils.iframeExtentLink(iframeWindow, iframeDocument);
        JSUtils.iframeExtentScript(iframeWindow, iframeDocument, () => {
            // 动态继承的js 加载完成后 调用子页面初始化 并向子页面传递公共信息
            iframeWindow.globalData = globalData;
            iframeWindow.$ = $;
            iframeWindow.tabs = tabs;
            if (iframeWindow.init) {
                iframeWindow.init({tabData: tabData});
            }
            loadingTag.hide();
            iframeTag.show();
        });
    });
}

/**
 * 设置登陆人头像
 */
function setLoginUserInfo({avatar, name}) {
    const userTag = $('.home-login-active[data-type="userLogo"]');
    userTag.empty();
    userTag.wrapInner(`<img src="${avatar}" class="layui-nav-img" alt="">${name}`);
}

/**
 * 登陆人点击事件
 * 当点击有 homeMenuActive 属性的标签时，触发点击事件
 */
$('.home-login-active').on('click', function () {
    const clickTag = $(this); // 获取当前点击的a标签
    const dataType = clickTag.attr("data-type");
    switch (dataType) {
        case "userInfo":
        // case "userLogo":
            tabs.open("2-1");
            break;
        case "password":
            tabs.open("2-2");
            break;
        case "logout":
            login.logOut();
            break;
    }
});


/**
 * 监听Tab切换
 */
layui.element.on('tab(filter)', function (data) {
    login.check();
});

/**
 * tab右键菜单点击事件
 * 当点击有 home-tab-right-menu-active 属性的标签时，触发点击事件
 */
$('.home-tab-right-menu-active').on('click', function () {
    const clickTag = $(this); // 获取当前点击的a标签
    const dataType = clickTag.attr("data-type");
    const dataId = clickTag.attr("data-id");
    switch (dataType) {
        case "closeThis":
            tabs.tabDelete(dataId);
            break;
        case "closeOther":
            tabs.tabDelete(...Array.from(tabs.tabOpens).filter(item => item !== dataId));
            break;
        case "closeAll":
            tabs.tabDelete(...tabs.tabOpens);
            break;
    }
});

/**
 * 面包屑右键菜单 隐藏控制
 * tabRightMenu属性开始是隐藏的 ，当右击的时候显示，左击的时候隐藏
 */
$('.layui-layout-body,.layui-tab-title li').click(function () {
    const tabRightMenu = $('.tab-right-menu');
    tabRightMenu.hide();
    tabRightMenu.find("li").attr("data-id", null);
});
$('.tab-right-menu').mouseleave(function () {
    const tabRightMenu = $('.tab-right-menu');
    tabRightMenu.hide();
    tabRightMenu.find("li").attr("data-id", null);
});

/**
 * 绑定 tab右键菜单 显示
 */
function tabRightMenu() {
    // 点击tab标签关闭按钮时 关闭tab标签并删除tab标签对应的id
    $('.layui-tab-title li i').click(e => {
        const clickTag = $(e.target.parentElement);
        const id = clickTag.attr("lay-id");
        tabs.tabDelete(id);
    });
    $('.layui-tab-title li').on('contextmenu', function (e) {
        e.preventDefault();
        e.stopPropagation();

        const popupmenu = $(".tab-right-menu");
        // 解决事件冒泡
        if (popupmenu.is(':visible')) {
            return false;
        }

        const clickTag = $(this); // 获取当前点击的a标签
        const id = clickTag.attr("lay-id");
        tabs.open(id);

        popupmenu.find("li").attr("data-id", id); //在右键菜单中的标签绑定id属性
        //判断右侧菜单的位置
        let l = ($(document).width() - e.clientX) < popupmenu.width() ? (e.clientX - popupmenu.width()) : e.clientX;
        let t = ($(document).height() - e.clientY) < popupmenu.height() ? (e.clientY - popupmenu.height()) : e.clientY;
        l -= 200 - 26; // 减去左侧导航的宽度
        t -= 60 + 6; // 减去顶部导航的高度
        popupmenu.css({left: l, top: t}).show(); //进行绝对定位
        //alert("右键菜单")
        return false;
    });
}

/**
 * 监听popstate事件  浏览器后退按键
 */
window.addEventListener("popstate", event => {
    if (event && event.state && event.state.id) {
        tabs.open(event.state.id);
    }
});

/**
 * 当窗口大小发生变化时，从新计算frame高度
 */
window.addEventListener('resize', (e) => {
    const h = $(window).height() - 188;
    $("iframe").css("height", h + "px");
});








