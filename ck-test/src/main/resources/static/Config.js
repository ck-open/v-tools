/**
 * 页面配置信息
 * id：页面id
 * name：页面名称
 * icon：页面图标
 * isShow：是否显示
 * path：页面路径
 * url：页面地址
 * children：子菜单
 */
export const menus = [
    {
        id: "1",
        name: '功能示例',
        icon: "&#xe857;",
        children: [
            {
                id: "1-1",
                name: '按钮',
                icon: "&#xe617;",
                path: 'Button',
                url: '../static/pages/demo/Button.html',
            },
            {
                id: "1-2",
                name: '表单',
                icon: "&#xe63c;",
                path: 'Form',
                url: '../static/pages/demo/Form.html',
            },
            {
                id: "1-3",
                name: '数据表格',
                icon: "&#xe62d;",
                path: 'TableList',
                url: '../static/pages/demo/TableList.html',
            },
            {
                id: "1-4",
                name: '静态表格',
                icon: "&#xe62d;",
                path: 'TableStatic',
                url: '../static/pages/demo/TableStatic.html',
            },
            {
                id: "1-5",
                name: '日期时间',
                icon: "&#xe637;",
                path: 'LayDate',
                url: '../static/pages/demo/LayDate.html',
            },
            {
                id: "1-6",
                name: '选项卡',
                icon: "&#xe62a;",
                path: 'Tab',
                url: '../static/pages/demo/Tab.html',
            },
            {
                id: "1-7",
                name: '进度条',
                icon: "&#xe649;",
                path: 'Progress',
                url: '../static/pages/demo/Progress.html',
            },
            {
                id: "1-8",
                name: '文件上传',
                icon: "&#xe681;",
                path: 'Upload',
                url: '../static/pages/demo/Upload.html',
            },
            {
                id: "1-9",
                name: '其他',
                icon: "&#xe658;",
                path: 'Other',
                url: '../static/pages/demo/Other.html',
            },
            {
                id: "1-10",
                name: '文字穿透',
                icon: "&#xe658;",
                path: 'Other-01',
                url: '../static/pages/demo/文字穿透.html',
            },
            {
                id: "1-11",
                name: '旋转中的视差效果',
                icon: "&#xe658;",
                path: 'Other-02',
                url: '../static/pages/demo/旋转中的视差效果.html',
            },
            {
                id: "1-12",
                name: '弹出层',
                icon: "&#xe604;",
                path: 'Layer',
                url: '../static/pages/demo/Layer.html',
            },
            {
                id: "1-13",
                name: '图标',
                icon: "&#xe628;",
                path: 'Icon',
                url: '../static/pages/demo/Icon.html',
            },
            {
                id: "1-14",
                name: '登录模板',
                icon: "&#xe628;",
                path: 'Icon',
                url: '../static/pages/demo/LoginTemplate.html',
            },
            {
                id: "1-15",
                name: '注册模板',
                icon: "&#xe628;",
                path: 'Icon',
                url: '../static/pages/demo/RegisterTemplate.html',
            },
            {
                id: "1-16",
                name: '格栅布局',
                icon: "&#xe628;",
                path: 'Icon',
                url: '../static/pages/demo/Grid.html',
            },
            {
                id: "1-17",
                name: '赋值取值',
                icon: "&#xe628;",
                path: 'Icon',
                url: `../static/pages/demo/SetGet.html`,
            },
        ]
    },
    {
        id: "2",
        name: '当前登录用户',
        isShow: false,
        children: [
            {
                id: "2-1",
                name: '基本资料',
                path: 'UserInfo',
                url: '../static/pages/login/UserInfo.html',
                // isShow: false,
            },
            {
                id: "2-2",
                name: '密码修改',
                path: 'Password',
                url: '../static/pages/login/Password.html',
                // isShow: false,
            },
        ]
    },
    {
        id: "3",
        name: '验证测试',
        icon: "&#xe64e;",
        children: [
            {
                id: "3-1",
                name: '测试',
                path: 'Test',
                url: '../static/pages/Test.html',
                // isShow: false,
            },
            {
                id: "3-2",
                name: '测试JS',
                path: 'Test',
                url: '../static/pages/Test.js',
                // isShow: false,
            },
        ]
    },
    {
        id: "4",
        name: '设置',
        icon: "&#xe614;",
        path: 'Test',
        url: '../static/pages/Test55.html',
    },
]



/**
 * 全局数据对象
 * apiPrefix 请求API 地址前缀 用于 nginx 接口代理 无需代理默认为空
 */
export const globalData = {apiPrefix: ""};