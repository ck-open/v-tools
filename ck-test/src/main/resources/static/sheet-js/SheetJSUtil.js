/*

//编码行号
XLSX.utils.encode_row(2);  //"3"
//解码行号
XLSX.utils.decode_row("2"); //1

//编码列标
XLSX.utils.encode_col(2);  //"C"
//解码列标
XLSX.utils.decode_col("A"); //0

//编码单元格
XLSX.utils.encode_cell({ c: 1, r: 1 });  //"B2"
//解码单元格
XLSX.utils.decode_cell("B1"); //{c: 1, r: 0}

//编码单元格范围
XLSX.utils.encode_range({ s: { c: 1, r: 0 }, e: { c: 2, r: 8 } });  //"B1:C9"
//解码单元格范围
XLSX.utils.decode_range("B1:C9"); //{s:{c: 1, r: 0},e: {c: 2, r: 8}}



Worksheet Object
    Worksheet Object 指工作表对象，这个对象中每个不以!开头的属性，都代表一个单元格。
    例如 worksheet["A1"] 返回A1单元格对象。

    worksheet['!ref']：表示工作表范围的字符串。
        worksheet['!ref'] = "A1:B5"

    worksheet['!cols']：存储列对象的数组，可以在这里设置列宽。
        //wpx 字段表示以像素为单位，wch 字段表示以字符为单位
        worksheet['!cols'] = [
            { wpx: 200 }, //设置第1列列宽为200像素
            { wch: 50 },  //设置第2列列宽为50字符
        ];

    worksheet['!merges']：存储合并单元格范围的数组。
        //合并B2到D4范围内的单元格
        worksheet['!merges'] = [
            {
                s: { c: 1, r: 1 }, //B2
                e: { c: 3, r: 3 }, //D4
            }
        ]

    worksheet['!freeze']：冻结单元格。
        //冻结第一行和第一列：
        worksheet['!freeze'] = {
            xSplit: "1",  //冻结列
            ySplit: "1",  //冻结行
            topLeftCell: "B2",  //在未冻结区域的左上角显示的单元格，默认为第一个未冻结的单元格
            state: "frozen"
        }

 打印相关的设置
    worksheet['!rowBreaks']：行分页数组。
        //第一行为一页，第二行和第三行为一页，第三行之后为一页
        worksheet['!rowBreaks'] = [1,3]

    worksheet['!colBreaks']：列分页数组。
        //第一列为一页，第二列和第三列为一页，第三列之后为一页
        worksheet['!colBreaks'] = [1,3]

    worksheet['!pageSetup']：设置缩放大小和打印方向的对象。
        //缩放100%，打印方向为纵向
        worksheet['!pageSetup'] = {
            scale: '100',
            orientation: 'portrait'
        }
        // orientation 取值如下：
        // 'portrait'  - 纵向
        // 'landscape' - 横向

    worksheet['!printHeader']：需要重复的第一行和最后一行索引的数组，用于分页时重复打印表头。
        //分页时重复打印第一行
        worksheet['!printHeader'] = [1,1]

Workbook Object
    workbook.SheetNames：存储工作表名称的数组。
    workbook.Sheets：存储工作表对象的对象。
    workbook.Sheets[sheetname]：返回对应名称的工作表对象。

单元格样式
    设置单元格的样式，就是设置工作表对象中的单元格对象的 s 属性。
    这个属性的值也是一个对象，它有五个属性：fill、font、numFmt、alignment和border。


*/
