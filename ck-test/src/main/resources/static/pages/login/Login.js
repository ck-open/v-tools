import {globalData} from "../../Config.js";

;(function (window) {
    import('../../utils/PunctureBox.js')

    document.getElementById('loginBox').innerHTML = `
<div class="puncture-text-box" img-url="../../../img/big-01.jpg" img-text="后台管理中心">
    <div class="puncture-text-box-item">
        <div class="layui-container" style="width: 40vw;top: 10vw;right: 3vw;z-index: 2;">
          <div class="layui-row">
            <div class="layui-col-md6 layui-col-md-offset3">
              <form class="layui-form" action="">
                <div class="layui-form-item"><img class="layui-nav-img" src="../static/img/YDFR.png" alt="" style="padding-left: 11em;width: 100px;height: 100px;"></div>
                <div class="layui-form-item">
                  <label class="layui-form-label" style="font-family: 宋体;font-size: 1.5em;font-weight: bold;color: #FBFBFB;text-align: center;">用户名</label>
                  <div class="layui-input-block" style="width: 14em;">
                    <input type="text" name="username" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
                  </div>
                </div>
                <div class="layui-form-item">
                  <label class="layui-form-label" style="font-family: 宋体;font-size: 1.5em;font-weight: bold;color: #FBFBFB;text-align: center;">密码</label>
                  <div class="layui-input-block" style="width: 14em;">
                    <input type="password" name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
                  </div>
                </div>
                <div class="layui-form-item">
                  <div class="layui-input-block" style="width: 14em;padding-left: 3em;">
                    <button class="layui-btn" lay-submit lay-filter="login" style="font-size: 1.5em;">登&nbsp;&nbsp;&nbsp;录</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
    `;


    window.login = {
        check: function () {
            globalData.token = localStorage.getItem("token");
            if (globalData.token) {
                $('#loginBox').hide();
                $('.layui-layout-admin').show();
            } else {
                $('.layui-layout-admin').hide();
                $('#loginBox').show();
            }
            if (!globalData.user){
                // 待后面调整 TODO
                globalData.user = {
                    username: "admin",
                    name: "管理员",
                    avatar: "../static/img/lian_hua.png"
                }
            }
        },
        logOut: function () {
            globalData.token = null;
            localStorage.removeItem("token");
            this.check();
        },
        login: function (token) {
            globalData.token = token;
            localStorage.setItem("token", token);
            this.check();
        }
    }

    layui.use('form', function () {
        const form = layui.form;
        //监听提交
        form.on('submit(login)', function (data) {
            console.log("用户名：" + data.field.username + "  密码：" + data.field.password)
            //弹出登录成功提示框
            layer.msg('登录成功', {icon: 1,time: 500,offset:'t',area:['1em','4em']},()=>login.login(data.field.username));
            return false;
        });
    });

})(window)