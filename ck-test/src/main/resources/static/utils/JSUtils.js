/**
 * js工具类
 */
;(function (window, undefined) {
    window.JSUtils = {
        /**
         * 获取兄弟元素
         * @param element 当前元素
         * @param func 兄弟元素的回调函数
         * @returns {*[]}
         */
        getSiblings: function (element, func) {
            // 获取当前元素的父节点
            const parent = element.parentNode;

            // 获取父节点下的所有子节点
            const siblings = parent.childNodes;

            // 过滤出元素节点并创建一个数组来存储它们
            // 因为childNodes会包含文本节点和注释节点等，所以需要过滤
            const siblingElements = [];
            for (let i = 0; i < siblings.length; i++) {
                if (siblings[i].nodeType === Node.ELEMENT_NODE && siblings[i] !== element) {
                    siblingElements.push(siblings[i]);
                    func(siblings[i])
                }
            }
            return siblingElements;
        },
        /**
         *  清除对象空值
         *
         * @param data 对象
         * @param defaultVal 将空值或指定值转为 此默认值
         * @param regex 指定要转换为默认值的控制或特定值
         * @returns {*}
         */
        removeBlank: function (data, defaultVal, ...regex) {
            if (defaultVal === undefined) {
                return this.removeBlank(data, null, ...regex);
            }
            if (regex.length < 1) {
                return this.removeBlank(data, defaultVal, '', undefined);
            }
            //  普通数据类型
            if (data === undefined || typeof data !== 'object' || regex.includes(data)) {
                if (data === undefined || regex.includes(data)) {
                    return defaultVal;
                } else {
                    return data;
                }
            }
            // 引用数据类型
            if (data !== null) {
                let off = false;
                for (const v of Object.keys(data)) {
                    data[v] = this.removeBlank(data[v], defaultVal, ...regex);
                    if (data[v] !== defaultVal) {
                        off = true;
                    }
                }
                if (!off) {
                    return defaultVal;
                }
            }
            return data;
        },

        /**
         * 分块执行某些任务
         * @param datas  数据数组  或者执行次数
         * @param task  需要执行的任务函数
         * @param scheduler
         */
        performChunk: function (datas, task, scheduler) {
            if (typeof datas === 'number') {
                datas = {length: datas};
            }
            if (datas === 0) return;

            let i = 0;

            /* 执行下一块任务 */
            function _run() {
                if (i >= datas.length) return;
                scheduler((isGoOn) => {
                    while (isGoOn() && i < datas.length) {
                        /* 执行下一个任务 */
                        task(datas[i], i++);
                    }
                    _run();
                });
            }

            _run();
        },

        /**
         * 利用浏览器页面渲染时间分块执行某些任务
         * @param datas
         * @param task
         */
        performChunkOfBrowser: function (datas, task) {
            const scheduler = (isGoOn) => {
                requestIdleCallback((idle) => {
                    isGoOn(() => idle.timeRemaining() > 0);
                });
            };
            performChunk(datas, task, scheduler);
        },

        /**
         * 下载文件 - 带进度监控
         * @param url: 文件请求路径
         * @param method 请求方式:GET|POST
         * @param filename 保存的文件名
         * @param body 请求报文体
         * @param progress 进度处理回调函数
         * @param success 下载完成回调函数
         *
         * XMLHttpRequest的监听事件：
         *      onabort  当请求中止时触发
         *      onload  当请求成功时触发
         *      onloadend  在请求成功或者失败时触发；load、abort、error、timeout事件发生之后
         *      onloadstart  当请求开始时触发
         *      onreadystatechange  当readyStateChange属性改变时触发
         *      ontimeout  当timeout属性指定的时间已经过去但响应依旧没有完成时触发
         *      onerror  当请求因错误失败时触发。注意404等状态码不是error，因为此时响应仍然是成功完成的。
         *      onprogress  当响应主体正在下载重复触发（约每隔50ms一次）
         *
         **/
        progressDownLoad: function ({url, method, filename, body, progress, success}) {
            const xhr = new XMLHttpRequest();
            xhr.open(method ? method : body ? 'POST' : 'GET', url, true);
            //监听进度事件
            xhr.addEventListener("progress", function (evt) {
                if (progress) try {
                    progress.call(evt);
                } catch (e) {
                }
            }, false);

            xhr.responseType = "blob";
            xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    if (typeof window.chrome !== 'undefined') {
                        // Chrome version
                        const link = document.createElement('a');
                        link.href = window.URL.createObjectURL(xhr.response);
                        link.download = filename;
                        link.click();
                    } else if (typeof window.navigator.msSaveBlob !== 'undefined') {
                        // IE version
                        const blob = new Blob([xhr.response], {type: 'application/force-download'});
                        window.navigator.msSaveBlob(blob, filename);
                    } else {
                        // Firefox version
                        const file = new File([xhr.response], filename, {type: 'application/force-download'});
                        window.open(URL.createObjectURL(file));
                    }
                    if (success) try {
                        success.call(xhr);
                    } catch (e) {
                    }
                }
            };
            xhr.send(JSON.stringify($scope.exportData));
        },

        /**
         * iframe onload 事件处理
         * 注意：页面中只能有一个调用 否则将覆盖上面的调用
         * @param iframe iframe 标签对象
         * @param fun iframe 加载后处理函数
         */
        iframeExtent: function (iframe, fun) {
            if (iframe && fun) {
                iframe.onload = function () {
                    const iframeWindow = iframe.contentWindow;
                    const iframeDocument = iframe.contentDocument;
                    if (!iframeWindow) {
                        console.error("iframeWindow is null");
                        return;
                    }
                    if (!iframeDocument) {
                        console.error("iframeDocument is null");
                        return;
                    }
                    fun(iframeWindow, iframeDocument);
                }
            }
        },

        /**
         * iframe 子页面继承父页面 head 中的 link 标签
         * 注意：如果需要向下传递，需要在对应标签的class 中添加 ：iframe-extent
         * @param iframeWindow iframe 子页面的 window 对象
         * @param iframeDocument iframe 子页面的 document 对象
         */
        iframeExtentLink: function (iframeWindow, iframeDocument) {
            const linkList = iframeWindow.parent.document.getElementsByTagName("link");
            for (let i = 0; i < linkList.length; i++) {
                const link = linkList[i];
                if (!link.classList.contains("iframe-extent")) {
                    continue;
                }
                const iframeLink = iframeDocument.createElement("link");
                iframeLink.href = link.href;
                if (link.rel)
                    iframeLink.rel = link.rel;
                if (link.type)
                    iframeLink.type = link.type;
                if (link.media)
                    iframeLink.media = link.media;
                iframeDocument.head.appendChild(iframeLink);
            }
        },

        /**
         * iframe 子页面继承父页面 中的 script 引用 标签
         * 注意：如果需要向下传递，需要在对应标签的class 中添加 ：iframe-extent
         * @param iframeWindow iframe 子页面的 window 对象
         * @param iframeDocument iframe 子页面的 document 对象
         * @param fun 所有脚本加载完成后执行
         */
        iframeExtentScript: function (iframeWindow, iframeDocument, fun) {
            const scriptList = iframeWindow.parent.document.getElementsByTagName("script");

            let scriptNewCont = 0;
            for (let i = 0; i < scriptList.length; i++) {
                const script = scriptList[i];
                if (script.children.length <= 0 && script.classList.contains("iframe-extent")) {
                    const scriptNew = iframeDocument.createElement('script');
                    scriptNew.src = script.src;
                    if (script.type)
                        scriptNew.type = script.type;
                    scriptNew.onload = function () {
                        scriptNewCont -= 1;
                        if (fun && scriptNewCont <= 0) fun();
                    };

                    scriptNewCont += 1;
                    iframeDocument.head.appendChild(scriptNew);
                }
            }
            if (scriptList.length === 0 && fun) fun();
        },
        /**
         * 将字符 标签结构 转换为 DOM 对象
         * @param parent 父节点DOM 对象
         * @param str 字符 标签结构
         * @returns {*} DOM 对象
         */
        strToDom: function (parent, str) {
            const div = document.createElement('div');
            div.innerHTML = str;
            for (let i = 0; i < div.children.length; i++) {
                parent.appendChild(div.children[i]);
            }
            return parent;
        },
    }
})(window, undefined)