;(function () {

    // 获取或创建样式表
    let styleSheet = document.styleSheets[0] || (function () {
        let style = document.createElement('style');
        document.head.appendChild(style);
        return style.sheet;
    })();

    // 加入样式表
    styleSheet.insertRule(`.puncture-text-box,.puncture-text-box .puncture-text-box-item,.puncture-text-box .puncture-text-box-item span {
            margin: 0;padding: 0;
            --bg: url(#) no-repeat center/cover;
            --tm: 1.5s;  /* 动画时间 */
            --fs: 6vw; /* 字体大小 */
            --fm: 隶书; /* 字体 */
    }`, styleSheet.cssRules.length);
    styleSheet.insertRule(`.puncture-text-box {
            background: var(--bg);
            height: 100vh;
            font-family: 'Open Sans', sans-serif;
    }`, styleSheet.cssRules.length);
    styleSheet.insertRule(`.puncture-text-box .puncture-text-box-item {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            animation: show-up-box 3ms forwards;
    }`, styleSheet.cssRules.length);
    styleSheet.insertRule(`
        .puncture-text-box .puncture-text-box-item span {
            width: 100%;
            margin-top: 20vw;
            display: flex; /* 将对象作为弹性伸缩盒显示 */
            justify-content: center; /* 弹性盒子元素将向行中间位置对齐 */
            align-items: center; /* 弹性盒子元素在该行的侧轴（纵轴）上居中放置 */
            font-size: var(--fs);
            font-family: var(--fm);
            -webkit-text-stroke: 1px #fff; /* 设置边框阴影 */
            background: var(--bg);
            -webkit-background-clip: text; /* 从前景内容的形状（比如文字）作为裁剪区域向外裁剪 */
            color: transparent; /* 文字颜色设置透明 */
            animation: show-up var(--tm) forwards; /* 文字折叠展开动画 */
        }`, styleSheet.cssRules.length);

    styleSheet.insertRule(`
        @keyframes show-up {
            from {
                letter-spacing: -10vw;
                filter: blur(14px);
            }
            to {
                letter-spacing: 0;
                filter: blur(0px);
            }
        }`, styleSheet.cssRules.length);

    styleSheet.insertRule(`
        @keyframes show-up-box {
            from {
                filter: contrast(100); /* 对比度 */
            }
            to {
                filter: contrast(1.2); /* 对比度 */
            }
        }`, styleSheet.cssRules.length);

    const punctureBoxList = document.getElementsByClassName('puncture-text-box');

    if (punctureBoxList && punctureBoxList.length > 0) {
        for (let punctureBox of punctureBoxList) {
            let imgUrl = punctureBox.getAttribute('img-url');
            let text = punctureBox.getAttribute('img-text');
            if (imgUrl) {
                imgUrl = `url(${imgUrl}) no-repeat center/cover`;

                // 父容器样式
                punctureBox.style.setProperty('--bg', imgUrl);

                // 内部容器样式
                const punctureItemBoxList = punctureBox.getElementsByClassName('puncture-text-box-item');
                let punctureItemBox = null;
                if (punctureItemBoxList.length === 0){
                    punctureItemBox = document.createElement('div');
                    punctureItemBox.className="puncture-text-box-item"
                    punctureBox.appendChild(punctureItemBox);
                }else{
                    punctureItemBox = punctureItemBoxList[0];
                }
                const spanText = document.createElement('span');
                spanText.innerHTML = text;
                punctureItemBox.appendChild(spanText);
            }
        }
    }
})()