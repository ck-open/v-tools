package com.ck.qr;

import com.ck.qr.swetake.jp.sourceforge.qrcode.QRCodeDecoder;
import com.ck.qr.swetake.jp.sourceforge.qrcode.exception.DecodingFailedException;
import com.ck.qr.swetake.util.Qrcode;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

/**
 * 二维码工具类
 * QR码属于矩阵式二维码中的一个种类，由DENSO(日本电装)公司开发，由JIS和ISO将其标准化。
 */
public class QRCodeUtils {
    private static final Logger log = Logger.getLogger(QRCodeUtils.class.getName());

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param imgPath 图片路径
     */
    public static void encoderQRCode(String content, String imgPath) {
        encoderQRCode(content, imgPath, "png", 7);
    }

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param output  输出流
     */
    public static void encoderQRCode(String content, OutputStream output) {
        encoderQRCode(content, output, "png", 7);
    }

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param imgPath 图片路径
     * @param imgType 图片类型
     */
    public static void encoderQRCode(String content, String imgPath, String imgType) {
        encoderQRCode(content, imgPath, imgType, 7);
    }

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param output  输出流
     * @param imgType 图片类型
     */
    public static void encoderQRCode(String content, OutputStream output, String imgType) {
        encoderQRCode(content, output, imgType, 7);
    }

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param imgPath 图片路径
     * @param imgType 图片类型
     * @param size    二维码尺寸
     */
    public static void encoderQRCode(String content, String imgPath, String imgType, int size) {
        try {
            BufferedImage bufImg = encoderQRCode(content.getBytes(StandardCharsets.UTF_8), size);

            File imgFile = new File(imgPath);
            /* 生成二维码QRCode图片 */
            ImageIO.write(bufImg, imgType, imgFile);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 生成二维码(QRCode)图片
     *
     * @param content 存储内容
     * @param output  输出流
     * @param imgType 图片类型
     * @param size    二维码尺寸
     */
    public static void encoderQRCode(String content, OutputStream output, String imgType, int size) {
        try {
            BufferedImage bufImg = encoderQRCode(content.getBytes(StandardCharsets.UTF_8), size);
            /* 生成二维码QRCode图片 */
            ImageIO.write(bufImg, imgType, output);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 生成二维码(QRCode)图片的公共方法
     *
     * @param content 存储内容
     * @param charset 文本内容编码格式
     * @param imgType 图片类型名称，例如：png
     * @param size    二维码尺寸
     * @return 二进制图片文件
     * @see StandardCharsets
     */
    public static byte[] encoderQRCode(String content, Charset charset, String imgType, int size) {
        /* 获得内容的字节数组，设置编码格式 */
        byte[] contentBytes = content.getBytes(charset);
        BufferedImage bi = encoderQRCode(contentBytes, size);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        /* 生成二维码QRCode图片 */
        try {
            ImageIO.write(bi, imgType, bos);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bos.toByteArray();
    }

    /**
     * 生成二维码(QRCode)图片的公共方法
     *
     * @param contentBytes 存储内容的字节数组
     * @param size         二维码尺寸
     * @return 二进制图片文件
     */
    public static BufferedImage encoderQRCode(byte[] contentBytes, int size) {
        BufferedImage bufImg = null;
        try {
            Qrcode qrcodeHandler = new Qrcode();
            /* 设置二维码排错率，可选L(7%)、M(15%)、Q(25%)、H(30%)，排错率越高可存储的信息越少，但对二维码清晰度的要求越小 */
            qrcodeHandler.setQrCodeErrorCorrect('M');
            qrcodeHandler.setQrCodeEncodeMode('B');
            /* 设置设置二维码尺寸，取值范围1-40，值越大尺寸越大，可存储的信息越大 */
            qrcodeHandler.setQrCodeVersion(size);
            /* 图片尺寸 */
            int imgSize = 67 + 12 * (size - 1);
            bufImg = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
            Graphics2D gs = bufImg.createGraphics();
            /* 设置背景颜色 */
            gs.setBackground(Color.WHITE);
            gs.clearRect(0, 0, imgSize, imgSize);

            /* 设定图像颜色> BLACK */
            gs.setColor(Color.BLACK);
            /* 设置偏移量，不设置可能导致解析出错 */
            int pixOff = 2;
            /* 输出内容> 二维码 */
            if (contentBytes.length > 0 && contentBytes.length < 800) {
                boolean[][] codeOut = qrcodeHandler.calQrcode(contentBytes);
                for (int i = 0; i < codeOut.length; i++) {
                    for (int j = 0; j < codeOut.length; j++) {
                        if (codeOut[j][i]) {
                            gs.fillRect(j * 3 + pixOff, i * 3 + pixOff, 3, 3);
                        }
                    }
                }
            } else {
                throw new RuntimeException("QRCode content bytes length = " + contentBytes.length + " not in [0, 800].");
            }
            gs.dispose();
            bufImg.flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return bufImg;
    }

    /**
     * 解析二维码（QRCode）
     *
     * @param imgFile 图片路径
     * @param charset 结果字符集
     * @return 二维码内容信息
     * @see StandardCharsets
     */
    public static String decoderQRCode(File imgFile, Charset charset) {
        /* QRCode 二维码图片的文件 */
        try {
            BufferedImage bufImg = ImageIO.read(imgFile);
            QRCodeDecoder decoder = new QRCodeDecoder();
            return new String(decoder.decode(new TwoDimensionCodeImage(bufImg)), charset);
        } catch (IOException | DecodingFailedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 解析二维码（QRCode）
     *
     * @param input   输入流
     * @param charset 结果字符集
     * @return 二维码内容信息
     * @see StandardCharsets
     */
    public static String decoderQRCode(InputStream input, Charset charset) {
        try {
            BufferedImage bufImg = ImageIO.read(input);
            QRCodeDecoder decoder = new QRCodeDecoder();
            return new String(decoder.decode(new TwoDimensionCodeImage(bufImg)), charset);
        } catch (IOException | DecodingFailedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        String desktop = "C:/Users/cyk/Desktop/";
        String imgPath = desktop + "Michael_QRCode.png";
        String encoderContent = "Hello 大大、小小,welcome to QRCode!" + "\nMyblog [ http://sjsky.iteye.com ]" + "\nEMail [ sjsky007@gmail.com ]";
        QRCodeUtils.encoderQRCode(encoderContent, imgPath, "png");
        System.out.println("========encoder success");

        File imageFile = new File(imgPath);
        String decoderContent = QRCodeUtils.decoderQRCode(imageFile, StandardCharsets.UTF_8);
        System.out.println("解析结果如下：");
        System.out.println(decoderContent);
        System.out.println("========decoder success!!!");
    }
}
