package com.ck.spring.tools.configuration;

import com.ck.tools.com.TResult;
import com.ck.tools.exception.BaseException;
import com.ck.tools.exception.BusinessException;
import com.ck.tools.exception.CheckException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MarkerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 全局Controller层异常处理类
 */
@RestControllerAdvice
@Slf4j
public class BaseGlobalExceptionResolver {

    /**
     * 处理业务异常
     *
     * @param e 异常
     * @return 响应结果
     */
    @ExceptionHandler(value = {BaseException.class, CheckException.class, BusinessException.class})
    public TResult<?> handleBaseException(BaseException e) {
        log.error("处理业务异常 {} Message：{}", e.getClass().getSimpleName(), e.getMessage());
        return TResult.build(e.getCode(), e.getMessage());
    }

    /**
     * 处理所有不可知异常
     *
     * @param e 异常
     * @return 响应结果
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public TResult<?> handleThrowable(Throwable e) {
        if ("HttpRequestMethodNotSupportedException".equalsIgnoreCase(e.getClass().getSimpleName())){
            log.error("处理业务异常", e);
            return TResult.build(TResult.Code.API_ERROR, "请求无效：" + e.getMessage());
        }else{
//            log.error(LogSendUtils.SEND_MESSAGE, "全局异常捕获：" + e.getMessage(), e);
            log.error(MarkerFactory.getMarker("Throwable "), "全局异常捕获：" + e.getMessage(), e);
            return TResult.resolveException(e);
        }
    }
}
