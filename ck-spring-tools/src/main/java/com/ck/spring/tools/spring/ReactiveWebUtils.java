package com.ck.spring.tools.spring;


import io.netty.buffer.ByteBufAllocator;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 链式Web工具类
 *
 * @author cyk
 * @since 2023/2/22 15:56
 **/
@Slf4j
public class ReactiveWebUtils {

    /**
     * 读取请求报文信息
     *
     * @return Mono<Void>
     */
    public static ServerWebExchange readRequestBody(ServerWebExchange exchange, BiFunction<ServerHttpRequest, String, String> reqBodyFun, BiFunction<ServerHttpResponse, String, String> resBodyFun) {
        try {
            ServerHttpRequest request = exchange.getRequest();
            ServerHttpResponse response = exchange.getResponse();
            return exchange.mutate()
                    .request(ReactiveWebUtils.resetRequest(exchange.getRequest(), resetHeaders(request), body -> reqBodyFun.apply(request, body)))
                    .response(ReactiveWebUtils.resetResponse(exchange.getResponse(), body -> resBodyFun.apply(response, body)))
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("过滤器读取报文消息异常", e);
        }
    }


    /**
     * 重置 ServerWebExchange
     *
     * @param request 原请求对象
     * @return ServerHttpRequestDecorator
     */
    public static ServerHttpRequest resetRequest(ServerHttpRequest request, HttpHeaders headers, Function<String, String> bodyFun) {
        if (isBody(request) && bodyFun != null) {
            /* 设置请求头的Content-Length */
            if (headers == null) {
                throw new RuntimeException("请求报文头信息不能为空");
            }
            headers.remove(HttpHeaders.CONTENT_LENGTH);

            /* 使用修改后的ServerHttpRequestDecorator重新生成一个新的ServerWebExchange */
            return new ServerHttpRequestDecorator(request) {
                /* 新建一个ServerHttpRequest装饰器,覆盖需要装饰的方法 */
                @Override
                public Flux<DataBuffer> getBody() {
                    Charset charset = getCharset(headers.getContentType());
                    Flux<DataBuffer> fluxBody = request.getBody();
                    fluxBody = dataBufferRead(fluxBody, charset, body -> {
                        body = bodyFun.apply(body);
                        headers.setContentLength(body == null ? 0 : body.getBytes(charset).length);
                        return body;
                    });

                    return fluxBody;
                }

                @Override
                public HttpHeaders getHeaders() {
                    long contentLength = headers.getContentLength();
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.putAll(super.getHeaders());
                    if (contentLength > 0) {
                        httpHeaders.setContentLength(contentLength);
                    } else {
                        httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
                    }
                    return httpHeaders;
                }
            };
        }
        return request;
    }

    /**
     * 重置 ServerWebExchange
     *
     * @param request 原请求对象
     * @return ServerHttpRequestDecorator
     */
    public static ServerHttpRequest buildRequest(ServerHttpRequest request, HttpHeaders headers, String body) {
        if (isBody(request)) {
            /* 设置请求头的Content-Length */
            if (headers == null) {
                throw new RuntimeException("请求报文头信息不能为空");
            }
            headers.remove(HttpHeaders.CONTENT_LENGTH);

            /* 使用修改后的ServerHttpRequestDecorator重新生成一个新的ServerWebExchange */
            return new ServerHttpRequestDecorator(request) {
                /* 新建一个ServerHttpRequest装饰器,覆盖需要装饰的方法 */
                @Override
                public Flux<DataBuffer> getBody() {
                    Charset charset = getCharset(headers.getContentType());
                    if (body == null) {
                        return Flux.empty();
                    }
                    headers.setContentLength(body.getBytes(charset).length);
                    return toDataBufferFlux(body, charset);
                }

                @Override
                public HttpHeaders getHeaders() {
                    long contentLength = headers.getContentLength();
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.putAll(super.getHeaders());
                    if (contentLength > 0) {
                        httpHeaders.setContentLength(contentLength);
                    } else {
                        httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
                    }
                    return httpHeaders;
                }
            };
        }
        return request;
    }

    /**
     * 读取请求报文信息
     *
     * @return Mono<Void>
     */
    public static Mono<Void> requestBodyRead(ServerWebExchange exchange, Function<String, Mono<Void>> function) {
        try {
            ServerHttpRequest request = exchange.getRequest();
            if (isBody(request)) {
                return DataBufferUtils.join(request.getBody()).flatMap(dataBuffer -> {
                    String contentTemp = null;
                    if (dataBuffer.readableByteCount() > 0) {
                        CharBuffer charBuffer = getCharset(request.getHeaders().getContentType()).decode(dataBuffer.asByteBuffer()); // 读取报文
                        DataBufferUtils.release(dataBuffer);  // 释放缓存
                        contentTemp = charBuffer.toString();
                    }
                    return function.apply(contentTemp);
                });
            }
        } catch (Exception e) {
            throw new RuntimeException("过滤器读取报文消息异常", e);
        }
        return exchange.getPrincipal().then();
    }

    /**
     * 写出信息 返回 Mono<Void> 对象
     *
     * @param response 响应对象
     * @return Mono<Void> 对象
     */
    public static Mono<Void> responseBodyWrite(ServerHttpResponse response, String responseBody) {
        return responseBodyWrite(response, responseBody, HttpStatus.OK);
    }

    /**
     * 写出信息 返回 Mono<Void> 对象
     *
     * @param response 响应对象
     * @return Mono<Void> 对象
     */
    public static Mono<Void> responseBodyWrite(ServerHttpResponse response, String responseBody, HttpStatus httpStatus) {
        DataBuffer dataBuffer = toDataBuffer(responseBody, getCharset(response.getHeaders().getContentType()));
        if (!response.getHeaders().containsKey(HttpHeaders.CONTENT_TYPE)) {
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            response.getHeaders().setAcceptCharset(Collections.singletonList(StandardCharsets.UTF_8));
        }
        response.setStatusCode(httpStatus);
        return response.writeWith(dataBuffer == null ? Mono.empty() : Mono.just(dataBuffer));
    }

    /**
     * 写出信息 返回 Mono<Void> 对象
     *
     * @param originalResponse 响应对象
     * @param bodyFun          重置响应体
     * @return ServerHttpResponse 重置后的新对象
     */
    public static ServerHttpResponse resetResponse(ServerHttpResponse originalResponse, Function<String, String> bodyFun) {
        if (bodyFun == null) {
            return originalResponse;
        }
        // 记录原字符集和类型
        MediaType contentType = originalResponse.getHeaders().getContentType();

        // 创建一个装饰器来修改响应体
        return new ServerHttpResponseDecorator(originalResponse) {
            @Override
            public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
                if (body instanceof Flux) {
                    // 设置默认字符集和类型
                    getHeaders().remove(HttpHeaders.CONTENT_TYPE);
                    originalResponse.getHeaders().setContentType(contentType != null ? contentType : MediaType.APPLICATION_JSON);

                    Charset charset = getCharset(originalResponse.getHeaders().getContentType());

                    Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>) body;

                    fluxBody = dataBufferRead(fluxBody, charset, bodyItem -> {
                        bodyItem = bodyFun.apply(bodyItem);
                        getHeaders().setContentLength(bodyItem == null ? 0 : bodyItem.getBytes(charset).length);
                        return bodyItem;
                    });

                    return super.writeWith(fluxBody);
                }

                // 其他情况直接返回
                return super.writeWith(body);
            }
        };
    }

    /**
     * 重置 ServerWebExchange
     *
     * @param request 原请求对象
     * @return ServerHttpRequestDecorator
     */
    public static ServerHttpRequest resetParams(ServerHttpRequest request, MultiValueMap<String, String> params) {

        if (!ObjectUtils.isEmpty(params)) {
            StringBuilder queryBuilder = new StringBuilder();
            for (String key : params.keySet()) {
                queryBuilder.append(key);
                queryBuilder.append("=");
                queryBuilder.append(params.getFirst(key));
                queryBuilder.append("&");
            }
            queryBuilder.deleteCharAt(queryBuilder.length() - 1);
            try {
                /* 只覆盖 ServerHttpRequest的getQueryParams路由分发之后,无法携带过去新的参数,所以这里需要构造一个新的uri */
                URI uri = request.getURI();
                URI newUri = new URI(uri.getScheme(),
                        uri.getUserInfo(),
                        uri.getHost(),
                        uri.getPort(),
                        uri.getPath(),
                        queryBuilder.toString(),
                        uri.getFragment());
                /* 构造一个新的ServerHttpRequest */
                request = request.mutate().uri(newUri).build();
            } catch (URISyntaxException e) {
                log.error("重置请求报文异常", e);
            }
        }
        return request;
    }

    /**
     * 重新创建请求报文头信息
     *
     * @param request 原请求
     * @return HttpHeaders
     */
    public static HttpHeaders resetHeaders(ServerHttpRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.putAll(request.getHeaders());
        return headers;
    }

    /**
     * 文本转 DataBuffer
     *
     * @param value   Content 文本
     * @param charset Content 字符集
     * @return DataBuffer
     * @see StandardCharsets
     */
    public static Flux<DataBuffer> toDataBufferFlux(String value, Charset charset) {
        DataBuffer bodyDataBuffer = toDataBuffer(value, charset);
        if (bodyDataBuffer == null) {
            return null;
        }
        return Flux.just(bodyDataBuffer);
    }

    /**
     * 文本转 DataBuffer
     *
     * @param value   Content 文本
     * @param charset Content 字符集
     * @return DataBuffer
     * @see StandardCharsets
     */
    public static DataBuffer toDataBuffer(String value, Charset charset) {
        if (value == null) return null;
        byte[] bytes = value.getBytes(charset);

        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(ByteBufAllocator.DEFAULT);
        DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
        buffer.write(bytes);
        return buffer;
    }

    /**
     * Flux<? extends DataBuffer> 数据读取
     *
     * @param flux    Flux<? extends DataBuffer> 对象
     * @param charset 字符集
     * @return 转换后的字符串
     * @see StandardCharsets
     */
    public static Flux<DataBuffer> dataBufferRead(Flux<? extends DataBuffer> flux, Charset charset, Function<String, String> fun) {
        return Flux.from(DataBufferUtils.join(flux).flatMap(dataBuffer -> {
            if (dataBuffer.readableByteCount() > 0) {
                String content = charset.decode(dataBuffer.asByteBuffer()).toString(); // 读取报文
                DataBufferUtils.release(dataBuffer);  // 释放缓存
                content = fun.apply(content);
                DataBuffer dataBufferTemp = toDataBuffer(content, charset);
                return dataBufferTemp == null ? Mono.empty() : Mono.just(dataBufferTemp);
            }
            /* 读取了body 必须重新创建Request 进行后续转发 */
            return Mono.just(dataBuffer);
        }));
    }

    /**
     * DataBuffer 转换字符串
     *
     * @param dataBuffer DataBuffer 对象
     * @param charset    字符集
     * @return 转换后的字符串
     * @see StandardCharsets
     */
    public static String dataBufferToString(DataBuffer dataBuffer, Charset charset) {
        if (dataBuffer == null || dataBuffer.readableByteCount() <= 0) {
            return null;
        }

        CharBuffer charBuffer = charset.decode(dataBuffer.asByteBuffer()); // 读取报文
        DataBufferUtils.release(dataBuffer);  // 释放缓存
        return charBuffer.toString();
    }

    /**
     * 判断是否包含消息体
     *
     * @param request 原请求对象
     * @return true 包含消息体
     */
    public static boolean isBody(ServerHttpRequest request) {

        if (!HttpMethod.POST.equals(request.getMethod())
                && !HttpMethod.PUT.equals(request.getMethod())
                && !HttpMethod.PATCH.equals(request.getMethod())) {
            return false;
        }

        MediaType contentType = request.getHeaders().getContentType();
        if (MediaType.MULTIPART_FORM_DATA.equals(contentType)) {
            return false;
        }

        return MediaType.APPLICATION_JSON.equals(contentType) || MediaType.APPLICATION_JSON_UTF8.equals(contentType) || MediaType.TEXT_PLAIN.equals(contentType);
    }

    /**
     * 获取字符集
     *
     * @param contentType originalResponse.getHeaders().getContentType();
     * @return charset 默认UTF-8
     */
    public static Charset getCharset(MediaType contentType) {
        return contentType == null || contentType.getCharset() == null ? StandardCharsets.UTF_8 : contentType.getCharset();
    }

    /**
     * 获取路由
     *
     * @param exchange ServerWebExchange
     * @return Route
     */
    public static Route getGatewayRoute(ServerWebExchange exchange) {
        return exchange.getRequiredAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
    }

    /**
     * 获取IP地址
     *
     * @param exchange 请求对象
     * @return IP地址
     */
    public static String getRemoteAddress(ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();
        Map<String, String> headers = request.getHeaders().toSingleValueMap();
        String unknown = "unknown";
        String ip = headers.get("X-Forwarded-For");
        if (ip == null || ip.isEmpty() || unknown.equalsIgnoreCase(ip)) {
            ip = headers.get("Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || unknown.equalsIgnoreCase(ip)) {
            ip = headers.get("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || unknown.equalsIgnoreCase(ip)) {
            ip = headers.get("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.isEmpty() || unknown.equalsIgnoreCase(ip)) {
            ip = headers.get("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.isEmpty() || unknown.equalsIgnoreCase(ip)) {
            ip = headers.get("X-Real-IP");
        }
        if (ip == null || ip.isEmpty() || unknown.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddress().getAddress().getHostAddress();
        }
        /* 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割 */
        if (ip != null && !ip.isEmpty()) {
            String[] ips = ip.split(",");
            if (ips.length > 0) {
                ip = ips[0];
            }
        }
        return ip;
    }

    /**
     * 检查路径是否在名单内
     *
     * @param permitAll   可放行名单
     * @param requestPath 请求路径
     * @return boolean true 直接放行不进行鉴权
     */
    public static boolean matchPath(Collection<String> permitAll, String requestPath) {
        if (permitAll == null || permitAll.isEmpty()) {
            return false;
        }
        AntPathMatcher pathMatch = new AntPathMatcher();
        return permitAll.stream().anyMatch(r -> pathMatch.match(r, requestPath));
    }
}
