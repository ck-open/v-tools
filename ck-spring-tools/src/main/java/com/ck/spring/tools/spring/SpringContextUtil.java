package com.ck.spring.tools.spring;

import com.ck.spring.tools.configuration.JacksonObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.function.BiFunction;

@Slf4j
public class SpringContextUtil implements ApplicationContextAware, BeanFactoryPostProcessor {

    @Getter
    private static ApplicationContext applicationContext;

    @Getter
    private static ConfigurableListableBeanFactory beanFactory;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringContextUtil.applicationContext == null) {
            SpringContextUtil.applicationContext = applicationContext;
        }
        log.info("ApplicationContext 配置成功,对象：" + SpringContextUtil.applicationContext);
    }
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        if (SpringContextUtil.beanFactory == null) {
            SpringContextUtil.beanFactory = beanFactory;
        }
        log.info("ConfigurableListableBeanFactory 配置成功,对象：" + SpringContextUtil.applicationContext);
    }

    /**
     * 注册单例
     *
     * @param singletonObject 单例对象
     */
    public static void registerSingleton(Object singletonObject) {
        if (singletonObject == null) {
            log.error("注册单例失败，singletonObject不能为空。");
            return;
        }
        String beanName = singletonObject.getClass().getSimpleName();
        beanName = beanName.substring(0, 1).toLowerCase() + beanName.substring(1);
        registerSingleton(beanName, singletonObject);
    }

    /**
     * 注册单例
     *
     * @param beanName        beanName
     * @param singletonObject 单例对象
     */
    public static void registerSingleton(String beanName, Object singletonObject) {
        if (beanName == null || singletonObject == null || beanName.trim().isEmpty()) {
            log.error("注册单例失败，beanName和singletonObject不能为空。");
            return;
        }
        if (beanFactory.containsSingleton(beanName)) {
            log.error("注册单例失败，容器中已存在。 Bean名称：" + beanName + ",Bean类型：" + singletonObject.getClass());
            return;
        }
        beanFactory.registerSingleton(beanName, singletonObject);
    }

    public static Object getBean(String name) {
        try {
            return getApplicationContext().getBean(name);
        } catch (RuntimeException e) {
            log.error("获取Bean失败，Bean名称：" + name, e);
        }
        return null;
    }

    public static <T> T getBean(Class<T> clazz) {
        try {
            return getApplicationContext().getBean(clazz);
        } catch (RuntimeException e) {
            log.error("获取Bean失败，Bean类型：" + clazz, e);
        }
        return null;
    }

    public static <T> T getBean(String name, Class<T> clazz) {
        try {
            return getApplicationContext().getBean(name, clazz);
        } catch (RuntimeException e) {
            log.error("获取Bean失败，Bean名称：" + name + ",Bean类型：" + clazz, e);
        }
        return null;
    }

    public static <T> Map<String, T> getBeans(Class<T> clazz) {
        try {
            return getApplicationContext().getBeansOfType(clazz);
        } catch (RuntimeException e) {
            log.error("获取Bean失败，Bean类型：" + clazz, e);
        }
        return null;
    }

    /**
     * 使用 ObjectMapper json序列化的方式转换对象
     *
     * @param resource 原对象
     * @param clazz    转换的目标对象类型
     * @param <T>      转换的目标对象类型
     * @return 转换后的对象
     */
    public static <T> T copyBean(Object resource, Class<T> clazz) {
        return copyBean(resource, (mapper, resourceStr) -> {
            try {
                return mapper.readValue(resourceStr, clazz);
            } catch (JsonProcessingException e) {
                log.info("转换Bean失败，Bean类型：" + clazz, e);
            }
            return null;
        });
    }

    /**
     * 使用 ObjectMapper json序列化的方式转换对象
     *
     * @param resource 原对象
     * @param clazz    转换的目标对象类型
     * @param <T>      转换的目标对象类型
     * @return 转换后的对象
     */
    public static <T> T copyBean(Object resource, TypeReference<T> clazz) {
        return copyBean(resource, (mapper, resourceStr) -> {
            try {
                return mapper.readValue(resourceStr, clazz);
            } catch (JsonProcessingException e) {
                log.info("转换Bean失败，Bean类型：" + clazz, e);
            }
            return null;
        });
    }

    private static <T> T copyBean(Object resource, BiFunction<ObjectMapper, String, T> function) {
        try {
            ObjectMapper mapper = getBean(ObjectMapper.class);
            if (mapper != null) {
                String resourceStr = mapper.writeValueAsString(resource);
                return function.apply(mapper, resourceStr);
            }
        } catch (JsonProcessingException e) {
            log.info("转换Bean失败，resource：" + resource, e);
        }
        return null;
    }

    /**
     * 将对象序列化为json字符串
     *
     * @param obj java 对象
     * @return json字符串
     */
    public static String toJsonString(Object obj) {
        try {
            ObjectMapper mapper = getBean(ObjectMapper.class);
            if (mapper == null) {
                mapper = JacksonObjectMapper.getObjectMapper();
            }
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("对象序列化Json字符串失败", e);
        }
    }

    /**
     * 将json字符串反序列化为对象
     *
     * @param json  json字符串
     * @param clazz 对象类型
     * @param <T>   对象类型
     * @return java对象
     */
    public static <T> T parseJsonObject(String json, Class<T> clazz) {
        try {
            ObjectMapper mapper = getBean(ObjectMapper.class);
            if (mapper == null) {
                mapper = JacksonObjectMapper.getObjectMapper();
            }
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Json反序列化失败", e);
        }
    }

    /**
     * 将json字符串反序列化为对象
     *
     * @param json  json字符串
     * @param clazz 对象类型
     * @param <T>   对象泛型类型
     * @return java对象
     */
    public static <T> T parseJsonObject(String json, TypeReference<T> clazz) {
        try {
            ObjectMapper mapper = getBean(ObjectMapper.class);
            if (mapper == null) {
                mapper = JacksonObjectMapper.getObjectMapper();
            }
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Json反序列化失败", e);
        }
    }
}
