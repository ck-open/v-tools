package com.ck.spring.tools.spring;


import com.ck.tools.check_bean.CheckResult;
import com.ck.tools.check_bean.CheckValueUtil;
import com.ck.tools.com.CalculateTrans;
import com.ck.tools.com.TResult;
import com.ck.tools.exception.BaseException;
import com.ck.tools.exception.CheckException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 公共业务处理接口
 */
public interface ComTrans<T, R> extends CalculateTrans {

    /**
     * 根据业务标识获取对应的业务处理
     *
     * @param serviceCode 业务标识(方案代码)
     * @return 业务处理
     * @see cn.ck.sdk.constants.trans
     */
    static <T, R> ComTrans<T, R> getComService(String serviceCode) {
        Logger logger = LoggerFactory.getLogger(ComTrans.class);
        ComTrans<T, R> service = SpringContextUtil.getBean(serviceCode, ComTrans.class);
        if (service == null) {
            logger.info("未找到对应的业务处理器, serviceCode：{}", serviceCode);
            throw new BaseException("-1", "未找到对应的业务处理器");
        }
        return service;
    }

    /**
     * 基础非空及基础规则校验
     *
     * @param val  校验对象
     * @param flag 校验字段标记
     */
    static void checkBeanNotNull(Object val, String... flag) {
        List<CheckResult> checkResult = CheckValueUtil.checkBeanFieldIsNotNull(val, flag);
        if (!checkResult.isEmpty()) {
            if (checkResult.get(0).getCode() == null || checkResult.get(0).getCode().isEmpty()) {
                throw new CheckException(TResult.Code.PARAM_ERROR.getCode(), checkResult.stream().map(CheckResult::getMessage).collect(Collectors.joining(",")));
            } else {
                throw new CheckException(checkResult.get(0).getCode(), checkResult.stream().map(CheckResult::getMessage).collect(Collectors.joining(",")));
            }
        }
    }

    /**
     * 业务处理
     *
     * @param body 请求参数
     * @return 业务处理结果
     */
    @Transactional(rollbackFor = Exception.class)
    default R run(T body) {
        Logger logger = LoggerFactory.getLogger(this.getClass());
        try {
            return handler(body);
        } catch (BaseException e) {
            logger.info("业务处理失败：{}", e.getMessage());
            throw e;
        } catch (Exception e) {
            logger.error("系统异常", e);
            throw new BaseException("系统异常:" + e.getMessage(), e);
        }
    }

    /**
     * 业务处理
     *
     * @param body 请求参数
     * @return 业务处理结果
     */
    R handler(T body) throws Exception;


    /**
     * 返回重定向
     * @param locationUrl 重定向地址
     * @return 重定向响应
     */
    default  <T> ResponseEntity<T> redirect(String locationUrl){
        // 设置 HTTP 状态码为 302 (Found)
        return ResponseEntity.status(HttpStatus.FOUND).header("Location", locationUrl).build();
    }

    /**
     * 返回html
     * @param htmlContent html内容
     * @return html响应
     */
    default ResponseEntity<String> htmlContent(String htmlContent){
        // 设置 HTTP 状态码为 302 (Found)
        return ResponseEntity.ok().header("Content-Type", "text/html; charset=UTF-8").body(htmlContent);
    }
}
