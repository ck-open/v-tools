package com.ck.spring.tools.log_msg;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggerContextVO;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import com.ck.tools.exception.ExceptionUtil;
import com.ck.tools.utils.TimeUtil;
import com.ck.tools.utils.WebUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.UUID;

/**
 * 异常消息对象
 */
@Data
@Accessors(chain = true)
public class LogSendDto {
    /**
     * 消息ID
     */
    private String id;
    /**
     * 异常服务IP
     */
    private String serverIp;
    /**
     * 异常服务名称
     */
    private String serverName;
    /**
     * 消息类型
     */
    private String msgType = "提醒";
    /**
     * 消息内容
     */
    private String msg;
    /**
     * 消息大文本内容
     */
    private String text;
    /**
     * 消息时间
     */
    private String time;
    /**
     * 消息来源类
     */
    private String logClass;
    /**
     * 消息来源类
     */
    private String errorMsg;
    /**
     * 异常（堆栈数据）
     */
    private String stackTrace;

    /**
     * 构建
     * <p>
     * 消息包括：服务IP、服务名称、Message、日志打印所在类、异常抛出所在类、堆栈信息（3条）
     *
     * @param eventObject 消息事件对象
     */
    public static LogSendDto builder(ILoggingEvent eventObject) {
        LogSendDto errMsgSend = new LogSendDto();

        if (Level.ERROR.equals(eventObject.getLevel()))
            errMsgSend.setMsgType("错误警告");

        errMsgSend
                .setTime(TimeUtil.formatDate_S(new Date(eventObject.getTimeStamp())))
                .setId(eventObject.getMDCPropertyMap().get("requestId"))
                .setMsg(eventObject.getFormattedMessage())
                .setLogClass(eventObject.getLoggerName());
        if (errMsgSend.getId()==null)
            errMsgSend.setId(eventObject.getMDCPropertyMap().get("requestId"));
        if (errMsgSend.getId()==null)
            errMsgSend.setId(UUID.randomUUID().toString());


        // 服务链路ID
        if (eventObject.getMDCPropertyMap() != null && eventObject.getMDCPropertyMap().containsKey("requestId")) {
            errMsgSend.setId(eventObject.getMDCPropertyMap().get("requestId"));
        } else {
            errMsgSend.setId(UUID.randomUUID().toString());
        }

        LoggerContextVO vo = eventObject.getLoggerContextVO();
        if (vo != null && vo.getPropertyMap() != null) {
            errMsgSend.setServerIp(vo.getPropertyMap().get("ServerIP"))
                    .setServerName(vo.getPropertyMap().get("APP_NAME"));

            if (errMsgSend.getServerIp() == null)
                errMsgSend.setServerIp(WebUtil.getOuterIp());
        }

        if (eventObject.getThrowableProxy() != null) {
            errMsgSend.setErrorMsg(eventObject.getThrowableProxy().getMessage())
                    .setStackTrace(ExceptionUtil.getExceptionStackTraceInfo(eventObject.getThrowableProxy().getStackTraceElementProxyArray()
                            , StackTraceElementProxy::getStackTraceElement, 4, "cn.com.hyundai"));
        }

        return errMsgSend;
    }


    /**
     * 获取消息内容，如果超过指定长度，则截取，并记录原文本内容
     *
     * @param maxLength 指定消息内容长度  最小10
     * @return 小于等于指定长度的消息内容
     */
    public String getMsg(int maxLength) {
        maxLength = Math.max(maxLength, 10);
        if (getMsg() != null && getMsg().length() > maxLength) {
            if (getText() == null || getText().trim().isEmpty()) {
                setText(getMsg());
            } else {
                setText(getMsg() + "\n===========================以下是原文本内容========================\n" + getText());
            }
            setMsg(getMsg().substring(0, maxLength) + "...(省略部分见文件详情)");
        }
        return getMsg();
    }


    @Override
    public String toString() {
        String enter = "\r\n";
        StringBuilder sb = new StringBuilder();
        sb.append("MsgID：").append(ObjectUtils.isEmpty(id) ? "" : id).append(enter)
                .append("Time：").append(ObjectUtils.isEmpty(time) ? TimeUtil.formatDate_S(TimeUtil.now()) : time).append(enter)
                .append("IP：").append(ObjectUtils.isEmpty(serverIp) ? "" : serverIp).append(enter)
                .append("AppName：").append(ObjectUtils.isEmpty(serverName) ? "" : serverName).append(enter)
                .append("msgType：").append(ObjectUtils.isEmpty(msgType) ? "" : msgType).append(enter)
                .append("Message：").append(ObjectUtils.isEmpty(msg) ? "" : msg.replace("\r", "").replace("\n", ""));

        if (!ObjectUtils.isEmpty(logClass))
            sb.append(enter).append("LogClass：").append(logClass);
        if (!ObjectUtils.isEmpty(errorMsg))
            sb.append(enter).append("ErrorMsg：").append(errorMsg);
        if (!ObjectUtils.isEmpty(stackTrace))
            sb.append(enter).append("ErrorClass：").append(stackTrace);

        return sb.toString();
    }
}
