package com.ck.spring.tools.spring;

import com.ck.spring.tools.redis.RedisCacheUtil;
import com.ck.tools.utils.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 批量处理公共方法
 */
public interface BatchTrans {

    Logger logI = LoggerFactory.getLogger(BatchTrans.class);

    String LOCK_KEY = RedisCacheUtil.getClassKey(BatchTrans.class.getName(),"lock");

    /**
     * 保单锁
     * 注意：无失效时间需要手动释放
     *
     * @param redisTemplate Redis
     * @param businessNo    业务号
     * @return 获取锁返回true
     */
    default boolean lock(RedisTemplate<String, String> redisTemplate, String businessNo) {
        return lock(redisTemplate, businessNo, null);
    }

    /**
     * 保单锁
     * 注意：无失效时间需要手动释放
     *
     * @param redisTemplate Redis
     * @param businessNo    业务号
     * @return 获取锁返回true
     */
    default boolean lock(RedisTemplate<String, String> redisTemplate, String businessNo, String value) {
        if (redisTemplate == null || businessNo == null || value == null) {
            return false;
        }

        return redisTemplate.opsForHash().putIfAbsent(LOCK_KEY, businessNo, value);
    }

//    /**
//     * 保单锁
//     * 注意：无失效时间需要手动释放
//     *
//     * @param redisTemplate Redis
//     * @param businessNo    业务号
//     * @return 获取锁返回true
//     */
//    default boolean lock(RedisTemplate<String, String> redisTemplate, String businessNo, String value) {
//        if (redisTemplate == null || businessNo == null || value == null) {
//            return false;
//        }
//
//        String key = RedisCacheUtil.getClassKey(BatchTrans.class.getName(),"lock:", businessNo);
//        Boolean lock = redisTemplate.opsForValue().setIfAbsent(key, value);
//        return Objects.nonNull(lock) && lock;
//    }

    /**
     * 保单锁
     *
     * @param redisTemplate Redis
     * @param businessNo    业务号
     * @return 获取锁返回true
     */
    default boolean lock(RedisTemplate<String, String> redisTemplate, String businessNo, long timeout, TimeUnit unit, Object... values) {
        String key = RedisCacheUtil.getClassKey(BatchTrans.class.getName(), "lock:",businessNo);
        Boolean lock = redisTemplate.opsForValue().setIfAbsent(key, RedisCacheUtil.toJsonString(values), timeout, unit);
        return Objects.nonNull(lock) && lock;
    }

    /**
     * 保单锁
     * 注意：无失效时间需要手动释放
     *
     * @param redisTemplate Redis
     * @param businessNos   保单号列表
     * @return 获取锁返回true
     */
    default boolean lockBatch(RedisTemplate<String, String> redisTemplate, Collection<String> businessNos, Object... values) {
        List<String> lockPolicyNos = Collections.synchronizedList(new ArrayList<>());
        try {
            ThreadPoolUtil.forEach(8, 200, businessNos, (index, policyNo) -> {
                if (!lock(redisTemplate, policyNo, Arrays.toString(values))) {
                    throw new RuntimeException("获取锁失败:" + policyNo);
                }
                lockPolicyNos.add(policyNo);
            });
        } catch (Exception e) {
            logI.error("获取锁失败", e);
            if (!lockPolicyNos.isEmpty())
                unlockBatch(redisTemplate, lockPolicyNos);
            return false;
        }
        return true;
    }

    /**
     * 解锁
     *
     * @param redisTemplate Redis
     * @param businessNo    保单号
     */
    default boolean unlock(RedisTemplate<String, String> redisTemplate, String... businessNo) {
        if (Objects.isNull(businessNo)) return false;
        return unlockBatch(redisTemplate, Arrays.asList(businessNo)) == businessNo.length;
    }

//    /**
//     * 解锁
//     *
//     * @param redisTemplate Redis
//     * @param businessNos   保单号列表
//     */
//    default long unlockBatch(RedisTemplate<String, String> redisTemplate, Collection<String> businessNos) {
//        if (redisTemplate == null || businessNos == null || businessNos.isEmpty()) return 0;
//
//        Collection<String> keysAll = businessNos.stream()
//                .map(businessNo -> RedisCacheUtil.getClassKey(BatchTrans.class.getName(), "lock:",businessNo)).collect(Collectors.toList());
//
//        AtomicLong result = new AtomicLong(0);
//
//        ThreadPoolUtil.forEach(8, 2
//                , ThreadPoolUtil.splitListBySize(2000, keysAll)
//                , (index, keys) -> {
//                    Long delNum = redisTemplate.delete(keys);
//                    if (delNum != null)
//                        result.addAndGet(delNum);
//                });
//
//        return result.get();
//    }

    /**
     * 解锁
     *
     * @param redisTemplate Redis
     * @param businessNos   保单号列表
     */
    default long unlockBatch(RedisTemplate<String, String> redisTemplate, Collection<String> businessNos) {
        if (redisTemplate == null || businessNos == null || businessNos.isEmpty()) return 0;

        AtomicLong result = new AtomicLong(0);

        ThreadPoolUtil.forEach(8, 2
                , ThreadPoolUtil.splitListBySize(2000, businessNos)
                , (index, keys) -> {
                    Long delNum = redisTemplate.opsForHash().delete(LOCK_KEY, keys.toArray());
                    result.addAndGet(delNum);
                });

        return result.get();
    }

    /**
     * 获取锁值
     *
     * @param redisTemplate Redis
     * @param businessNo    业务号
     * @return 锁值
     */
    default String getLockVla(RedisTemplate<String, String> redisTemplate, String businessNo) {
        Object val = redisTemplate.opsForHash().get(LOCK_KEY, businessNo);
        return val == null ? null : val.toString();
    }

    /**
     * 递增
     *
     * @param businessNo 业务号
     * @param delta      递增值
     * @return 递增后的值
     */
    default long increment(RedisTemplate<String, String> redisTemplate, String businessNo, long delta) {
        String key = RedisCacheUtil.getClassKey(BatchTrans.class.getName(), "counter:", businessNo);
        if (delta <= 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        Long index = redisTemplate.opsForValue().increment(key, delta);
        Objects.requireNonNull(index, "获取递增值失败");
        return index;
    }

    /**
     * 递减
     *
     * @param businessNo 业务号
     * @param delta      递减值
     * @return 递减后的值
     */
    default long decrement(RedisTemplate<String, String> redisTemplate, String businessNo, long delta) {
        String key = RedisCacheUtil.getClassKey(BatchTrans.class.getName(), "counter:", businessNo);
        if (delta <= 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        Long index = redisTemplate.opsForValue().decrement(key, delta);
        Objects.requireNonNull(index, "获取递减值失败");

        if (index <= 0) {
            redisTemplate.delete(key);
        }
        return index;
    }
}
