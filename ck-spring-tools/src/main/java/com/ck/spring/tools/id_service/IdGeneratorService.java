package com.ck.spring.tools.id_service;

import com.ck.tools.encrypt_decrypt.SnowflakeIdGenerator;

public class IdGeneratorService {

    private final SnowflakeIdGenerator idGenerator;

    public IdGeneratorService(SnowflakeProperties snowflakeProperties) {
        this.idGenerator = new SnowflakeIdGenerator(snowflakeProperties.getDataCenterId(), snowflakeProperties.getMachineId());
    }

    public long generateId() {
        return idGenerator.nextId();
    }
}