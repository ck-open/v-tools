package com.ck.spring.tools.spring;


import com.ck.tools.reflect.ClassUtils;
import com.nimbusds.oauth2.sdk.TokenRequest;
import lombok.Getter;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.*;

public abstract class Auth2TokenUtils {
    public static final String ACCESS = "access:";
    public static final String AUTH_TO_ACCESS = "auth_to_access:";
    public static final String AUTH = "auth:";
    public static final String REFRESH_AUTH = "refresh_auth:";
    public static final String ACCESS_TO_REFRESH = "access_to_refresh:";
    public static final String REFRESH = "refresh:";
    public static final String REFRESH_TO_ACCESS = "refresh_to_access:";
    public static final String CLIENT_ID_TO_ACCESS = "client_id_to_access:";
    public static final String UNAME_TO_ACCESS = "uname_to_access:";
    public static final String Authorization_Token = "Authorization";
    public static final String Access_Token = "access_token";
    public static final String Access_Token_Bearer = "Bearer ";


    /** 自定义类型,key(序列号对象的className):value(Class类型) */
    private static final Map<String, Class<?>> classConf = new HashMap<>();
    static {
        classConf.put("org.springframework.security.oauth2.provider.OAuth2Authentication", Auth2TokenUtils.OAuth2Authentication.class);
        classConf.put("org.springframework.security.oauth2.provider.OAuth2Request", Auth2TokenUtils.OAuth2Request.class);
        classConf.put("org.springframework.security.oauth2.provider.BaseRequest", Auth2TokenUtils.BaseRequest.class);
        classConf.put("org.springframework.util.StringUtils.OAuth2Utils", Auth2TokenUtils.OAuth2Utils.class);
    }


    /**
     * 读取Redis缓存中的Token令牌信息
     *
     * @param token 令牌
     * @return 令牌信息
     */
    public static Authentication readAuthentication(String token) {
        return readAuthentication(token, Auth2TokenUtils.OAuth2Authentication.class, classConf);
    }

    /**
     * 读取Redis缓存中的Token令牌信息
     *
     * @param token               令牌
     * @param authenticationClass 自定义的Authentication类型
     * @param classConf 自定义类型,key(序列号对象的className):value(Class类型)
     * @return 令牌信息
     */
    public static <T extends Authentication> T readAuthentication(String token, Class<T> authenticationClass, Map<String, Class<?>> classConf) {
        if (token == null || authenticationClass == null) {
            return null;
        }
        RedisConnectionFactory connectionFactory = SpringContextUtil.getBean(RedisConnectionFactory.class);
        if (connectionFactory == null){
            return null;
        }

        if (token.contains(Access_Token_Bearer)) {
            token = token.replaceAll(Access_Token_Bearer, "");
        }

        RedisConnection conn = connectionFactory.getConnection();
        byte[] bytes = conn.get((AUTH + token).getBytes());

        return ClassUtils.deserialize(bytes, authenticationClass, classConf);
    }

    public static abstract class OAuth2Utils {
        public static final String CLIENT_ID = "client_id";
        public static final String STATE = "state";
        public static final String SCOPE = "scope";
        public static final String REDIRECT_URI = "redirect_uri";
        public static final String RESPONSE_TYPE = "response_type";
        public static final String USER_OAUTH_APPROVAL = "user_oauth_approval";
        public static final String SCOPE_PREFIX = "scope.";
        public static final String GRANT_TYPE = "grant_type";

        public static Set<String> parseParameterList(String values) {
            Set<String> result = new TreeSet<>();
            if ((values != null) && (!values.trim().isEmpty())) {
                String[] tokens = values.split("[\\s+]");
                result.addAll(Arrays.asList(tokens));
            }
            return result;
        }

        public static String formatParameterList(Collection<String> value) {
            return value == null ? null : StringUtils.collectionToDelimitedString(value, " ");
        }

        public static Map<String, String> extractMap(String query) {
            Map<String, String> map = new HashMap<>();
            Properties properties = StringUtils.splitArrayElementsIntoProperties(
                    StringUtils.delimitedListToStringArray(query, "&"), "=");
            if (properties != null) {
                for (Object key : properties.keySet()) {
                    map.put(key.toString(), properties.get(key).toString());
                }
            }
            return map;
        }

        public static boolean containsAll(Set<String> target, Set<String> members) {
            target = new HashSet<>(target);
            target.retainAll(members);
            return target.size() == members.size();
        }
    }

    @Getter
    public static abstract class BaseRequest implements Serializable {
        private static final long serialVersionUID = 3902503486565214653L; // stream classdesc serialVersionUID = 3902503486565214653, local class serialVersionUID = 1238971075114188228

        private String clientId;
        private Set<String> scope;

        BaseRequest() {
            this.scope = new HashSet<>();
        }

        private Map<String, String> requestParameters = Collections.unmodifiableMap(new HashMap<>());

        public int hashCode() {
            int prime = 31;
            int result = 1;

            result = 31 * result + (this.clientId == null ? 0 : this.clientId.hashCode());
            result = 31 * result + (this.requestParameters == null ? 0 : this.requestParameters.hashCode());
            result = 31 * result + (this.scope == null ? 0 : this.scope.hashCode());
            return result;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            BaseRequest other = (BaseRequest) obj;
            if (this.clientId == null) {
                if (other.clientId != null) {
                    return false;
                }
            } else if (!this.clientId.equals(other.clientId)) {
                return false;
            }
            if (this.requestParameters == null) {
                if (other.requestParameters != null) {
                    return false;
                }
            } else if (!this.requestParameters.equals(other.requestParameters)) {
                return false;
            }
            if (this.scope == null) {
                return other.scope == null;
            } else return this.scope.equals(other.scope);
        }

        protected void setScope(Collection<String> scope) {
            if ((scope != null) && (scope.size() == 1)) {
                String value = (String) scope.iterator().next();
                if ((value.contains(" ")) || (value.contains(","))) {
                    scope = OAuth2Utils.parseParameterList(value);
                }
            }
            this.scope = Collections.unmodifiableSet(scope == null ? new LinkedHashSet<>() : new LinkedHashSet<>(scope));
        }

        protected void setRequestParameters(Map<String, String> requestParameters) {
            if (requestParameters != null) {
                this.requestParameters = Collections.unmodifiableMap(new HashMap<>(requestParameters));
            }
        }

        protected void setClientId(String clientId) {
            this.clientId = clientId;
        }

    }


    public static class OAuth2Authentication extends AbstractAuthenticationToken {
        private static final long serialVersionUID = -4809832298438307309L;
        private final OAuth2Request storedRequest;
        private final Authentication authentication;

        public OAuth2Authentication(OAuth2Request storedRequest, Authentication userAuthentication) {
            super(userAuthentication == null ? storedRequest.getAuthorities() : userAuthentication.getAuthorities());
            this.storedRequest = storedRequest;
            this.authentication = userAuthentication;
        }

        public Object getCredentials() {
            return "";
        }

        public Object getPrincipal() {
            return this.authentication == null ? this.storedRequest.getClientId() : this.authentication.getPrincipal();
        }

        public boolean isClientOnly() {
            return this.authentication == null;
        }

        public OAuth2Request getOAuth2Request() {
            return this.storedRequest;
        }

        public boolean isAuthenticated() {
            return (this.storedRequest.isApproved()) && ((this.authentication == null) || (this.authentication.isAuthenticated()));
        }

        public void eraseCredentials() {
            super.eraseCredentials();
            if ((this.authentication != null) && (CredentialsContainer.class.isAssignableFrom(this.authentication.getClass()))) {
                ((CredentialsContainer) this.authentication).eraseCredentials();
            }
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof OAuth2Authentication)) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }
            OAuth2Authentication that = (OAuth2Authentication) o;
            if (!this.storedRequest.equals(that.storedRequest)) {
                return false;
            }
            if (!Objects.equals(this.authentication, that.authentication)) {
                return false;
            }
            if (getDetails() != null) {
                getDetails();
                that.getDetails();
            } else {
                that.getDetails();
            }
            return true;
        }

        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + this.storedRequest.hashCode();
            result = 31 * result + (this.authentication != null ? this.authentication.hashCode() : 0);
            return result;
        }
    }

    public static class OAuth2Request extends BaseRequest implements Serializable {
        private static final long serialVersionUID = 1L;
        @Getter
        private Set<String> resourceIds = new HashSet<>();
        @Getter
        private Collection<? extends GrantedAuthority> authorities = new HashSet<>();
        @Getter
        private boolean approved = false;
        private TokenRequest refresh = null;
        @Getter
        private String redirectUri;
        @Getter
        private Set<String> responseTypes = new HashSet<>();
        @Getter
        private Map<String, Serializable> extensions = new HashMap<>();

        public OAuth2Request(Map<String, String> requestParameters, String clientId, Collection<? extends GrantedAuthority> authorities, boolean approved, Set<String> scope, Set<String> resourceIds, String redirectUri, Set<String> responseTypes, Map<String, Serializable> extensionProperties) {
            setClientId(clientId);
            setRequestParameters(requestParameters);
            setScope(scope);
            if (resourceIds != null) {
                this.resourceIds = new HashSet<>(resourceIds);
            }
            if (authorities != null) {
                this.authorities = new HashSet<>(authorities);
            }
            this.approved = approved;
            if (responseTypes != null) {
                this.responseTypes = new HashSet<>(responseTypes);
            }
            this.redirectUri = redirectUri;
            if (extensionProperties != null) {
                this.extensions = extensionProperties;
            }
        }

        protected OAuth2Request(OAuth2Request other) {
            this(other.getRequestParameters(), other.getClientId(), other.getAuthorities(), other.isApproved(), other
                    .getScope(), other.getResourceIds(), other.getRedirectUri(), other.getResponseTypes(), other
                    .getExtensions());
        }

        protected OAuth2Request(String clientId) {
            setClientId(clientId);
        }

        protected OAuth2Request() {
        }

        public OAuth2Request createOAuth2Request(Map<String, String> parameters) {
            return new OAuth2Request(parameters, getClientId(), this.authorities, this.approved, getScope(), this.resourceIds, this.redirectUri, this.responseTypes, this.extensions);
        }

        public OAuth2Request narrowScope(Set<String> scope) {
            OAuth2Request request = new OAuth2Request(getRequestParameters(), getClientId(), this.authorities, this.approved, scope, this.resourceIds, this.redirectUri, this.responseTypes, this.extensions);

            request.refresh = this.refresh;
            return request;
        }

        public OAuth2Request refresh(TokenRequest tokenRequest) {
            OAuth2Request request = new OAuth2Request(getRequestParameters(), getClientId(), this.authorities, this.approved, getScope(), this.resourceIds, this.redirectUri, this.responseTypes, this.extensions);
            request.refresh = tokenRequest;
            return request;
        }

        public boolean isRefresh() {
            return this.refresh != null;
        }

        public TokenRequest getRefreshTokenRequest() {
            return this.refresh;
        }

        public String getGrantType() {
            if (getRequestParameters().containsKey("grant_type")) {
                return (String) getRequestParameters().get("grant_type");
            }
            if (getRequestParameters().containsKey("response_type")) {
                String response = (String) getRequestParameters().get("response_type");
                if (response.contains("token")) {
                    return "implicit";
                }
            }
            return null;
        }

        public int hashCode() {
            int prime = 31;
            int result = super.hashCode();
            result = 31 * result + (this.approved ? 1231 : 1237);
            result = 31 * result + (this.authorities == null ? 0 : this.authorities.hashCode());
            result = 31 * result + (this.extensions == null ? 0 : this.extensions.hashCode());
            result = 31 * result + (this.redirectUri == null ? 0 : this.redirectUri.hashCode());
            result = 31 * result + (this.resourceIds == null ? 0 : this.resourceIds.hashCode());
            result = 31 * result + (this.responseTypes == null ? 0 : this.responseTypes.hashCode());
            return result;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            OAuth2Request other = (OAuth2Request) obj;
            if (this.approved != other.approved) {
                return false;
            }
            if (this.authorities == null) {
                if (other.authorities != null) {
                    return false;
                }
            } else if (!this.authorities.equals(other.authorities)) {
                return false;
            }
            if (this.extensions == null) {
                if (other.extensions != null) {
                    return false;
                }
            } else if (!this.extensions.equals(other.extensions)) {
                return false;
            }
            if (this.redirectUri == null) {
                if (other.redirectUri != null) {
                    return false;
                }
            } else if (!this.redirectUri.equals(other.redirectUri)) {
                return false;
            }
            if (this.resourceIds == null) {
                if (other.resourceIds != null) {
                    return false;
                }
            } else if (!this.resourceIds.equals(other.resourceIds)) {
                return false;
            }
            if (this.responseTypes == null) {
                return other.responseTypes == null;
            } else return this.responseTypes.equals(other.responseTypes);
        }
    }
}
