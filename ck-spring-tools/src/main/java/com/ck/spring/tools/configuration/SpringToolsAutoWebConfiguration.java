package com.ck.spring.tools.configuration;

import com.ck.spring.tools.interceptor.RequestBodyAdviceAdapterInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;


/**
 * 需要 spring-boot-starter-web 组件的注入Bean  都需要在此配置文件中配置否则影响Gateway服务
 */
@Slf4j
@ConditionalOnClass(RequestBodyAdviceAdapter.class)
@Configuration
public class SpringToolsAutoWebConfiguration {

    /**
     * 接口Body报文体对象非空规则自动校验
     */
    @Bean
    @ConditionalOnMissingBean(RequestBodyAdviceAdapterInterceptor.class)
    public RequestBodyAdviceAdapterInterceptor requestBodyAdviceAdapter() {
        RequestBodyAdviceAdapterInterceptor requestBodyAdviceAdapter = new RequestBodyAdviceAdapterInterceptor();
        log.info("RequestBodyAdviceAdapter [{}]", requestBodyAdviceAdapter);
        return requestBodyAdviceAdapter;
    }


    /**
     * 跨域问题
     */
    @Bean
    @ConditionalOnMissingBean(CorsFilter.class)
    public CorsFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();
        // 允许所有域名进行跨域调用
//        config.addAllowedOrigin("*"); /* Spring boot 2.4以后不在允许使用 * 号 */
        config.addAllowedOriginPattern("*");
        // 允许跨越发送cookie
        config.setAllowCredentials(true);
        // 放行全部原始头信息
        config.addAllowedHeader("*");
        // 允许所有请求方法跨域调用
        config.addAllowedMethod("*");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

}
