package com.ck.spring.tools.spring;

import com.ck.tools.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;


/**
 * 报文数据预处理
 * 拦截器优先级需要小于-1，
 * 因为NettyWriteResponseFilter的order值为-1，我们需要覆盖返回响应体的逻辑，自定义的GlobalFilter必须比NettyWriteResponseFilter优先执行。
 */
public interface BodyPreprocess<Conf> {

    final String RSA_SHA1WithRSA = "SHA1WithRSA";
    final String RSA_SHA256WithRSA = "SHA256WithRSA";
    final String AES_128 = "AES_128";
    final String AES_SHA1PRNG_128 = "AES_SHA1PRNG_128";
    final String AES_CBC = "AES_CBC";
    final String DES_SHA1PRNG_128 = "DES_SHA1PRNG_128";
    final String DES_ECB = "DES_ECB";


    /**
     * 报文预处理 验签、加解密
     *
     * @param exchange  请求交易对象
     * @param chain     过滤器调用链
     * @param configFun 接口配置对象
     * @return Mono<Void>
     */
    default Mono<Void> preprocess(ServerWebExchange exchange, GatewayFilterChain chain, Function<String, Conf> configFun) {
        return ReactiveWebUtils.requestBodyRead(exchange, body -> preprocess(exchange, chain, configFun == null ? null : configFun.apply(body), body));
    }

    /**
     * 报文预处理 验签、加解密
     *
     * @param exchange 请求交易对象
     * @param chain    过滤器调用链
     * @param config   接口配置对象
     * @param body     请求报文
     * @return Mono<Void>
     */
    default Mono<Void> preprocess(ServerWebExchange exchange, GatewayFilterChain chain, Conf config, String body) {
        Logger log = LoggerFactory.getLogger(this.getClass());
        log.info("请求数据预处理开始，地址：{}", exchange.getRequest().getURI().getPath());
        try {
            if (config == null) {
                log.error("接口配置获取失败,Body: {}", body);
                throw new BaseException("接口配置获取失败");
            }

            log.info("请求数据解密验签开始...");
            body = resetRequestBody(exchange.getRequest(), config, body);
            log.info("请求数据解密验签完成。");


            /* 读取了body 必须重新创建Request 进行后续转发 */
            return chain.filter(exchange.mutate()
                    .request(ReactiveWebUtils.buildRequest(exchange.getRequest(), ReactiveWebUtils.resetHeaders(exchange.getRequest()), body))
                    .response(ReactiveWebUtils.resetResponse(exchange.getResponse(), bodyItem -> {
                        try {
                            log.info("响应数据签名加密开始...");
                            bodyItem = resetResponseBody(exchange.getResponse(), config, bodyItem);
                            log.info("响应数据签名加密完成。");
                            return bodyItem;
                        } catch (Exception e) {
                            log.error("响应数据签名加密异常: " + e.getMessage() + "  报文：" + bodyItem, e);
//                            return "{\"code\": " + HttpStatus.INTERNAL_SERVER_ERROR.value() + ",\"msg\": \"响应数据签名加密异常\"}";
                            throw new BaseException("响应数据签名加密异常");
                        }
                    })).build());
        } catch (BaseException e) {
            return ReactiveWebUtils.responseBodyWrite(exchange.getResponse(), "{\"code\": " + HttpStatus.FORBIDDEN.value() + ",\"msg\": \"" + e.getMessage() + "\"}");
        } catch (Exception e) {
            log.error("请求数据验签解密异常: " + e.getMessage() + "  报文：" + body, e);
            return ReactiveWebUtils.responseBodyWrite(exchange.getResponse(), "{\"code\": " + HttpStatus.INTERNAL_SERVER_ERROR.value() + ",\"msg\": \"请求数据验签或解密异常\"}");
        }
    }


    /**
     * 重置请求报文
     *
     * @param request 请求对象
     * @param config  密钥令牌配置信息
     * @param body    原请求报文
     * @return 解密后的报文
     */
    String resetRequestBody(ServerHttpRequest request, Conf config, String body);

    /**
     * 重置响应报文
     *
     * @param response 响应对象
     * @param config   密钥令牌配置信息
     * @param body     原响应报文
     * @return 加密后的报文
     */
    String resetResponseBody(ServerHttpResponse response, Conf config, String body);
}
