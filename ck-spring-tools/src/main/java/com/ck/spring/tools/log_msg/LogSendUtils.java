package com.ck.spring.tools.log_msg;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AsyncAppenderBase;
import com.ck.tools.exception.ExceptionUtil;
import com.ck.tools.reflect.JsonUtils;
import com.ck.tools.utils.WebNetUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.net.HttpURLConnection;
import java.util.logging.Logger;


/**
 * 日志消息发送通知
 * 注意在logback.xml中配置该类
 * <p>
 * <!--yml 配置文件logging.log_send.url 配置消息发送服务地址-->
 * <springProperty name="log_send_url" scope="context" source="logging.log_send.url" defaultValue="-"/>
 * <p>
 * <!--消息发送服务地址-->
 * <appender name="log_send_async" class="cn.***.utils.log_msg.LogSendUtils">
 *      <logSendUrl>${log_send_url}</logSendUrl>
 *      <discardingThreshold>0</discardingThreshold>
 *      <appender-ref ref="log_send_async"/
 * </appender>
 * <p>
 * <root level="info">
 *      <appender-ref ref="log_send_async"/>
 * </root>
 *
 * @author cyk
 * @since 2022-01-01
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LogSendUtils extends AsyncAppenderBase<ILoggingEvent> {
    private static final Logger log = Logger.getLogger(LogSendUtils.class.getName());
    /**
     * 日志中带有此标记的则发送微信消息
     */
    public static final Marker SEND_MESSAGE = MarkerFactory.getMarker("SEND_MESSAGE");

    /**
     * 消息服务API地址
     */
    private String logSendUrl;
    /**
     * 消息发送重试次数
     */
    private Integer retryCount;


    /**
     * 所有Error 级别日志 并且 Message以 sendTag 字符结尾 或 带有 SEND_WX_MESSAGE 标记 的日志则发送微信消息
     * <p>
     * 消息包括：服务IP、服务名称、Message、日志打印所在类、异常抛出所在类、堆栈信息（3条）
     *
     * @param eventObject 事件对象
     */
    @Override
    protected void append(ILoggingEvent eventObject) {
        // 未指定发送消息则不发送
        if (eventObject.getMarker() == null || !SEND_MESSAGE.getName().equals(eventObject.getMarker().getName())) {
            return;
        }

        LogSendDto logSendDto = LogSendDto.builder(eventObject);
        send(logSendDto, Math.max(2, retryCount == null ? 0 : retryCount));
    }

    /**
     * 发送消息
     *
     * @param logSendDto 发送消息对象
     * @param retryCount 重试次数
     * @return boolean true:发送成功，false:发送失败
     */
    private boolean send(LogSendDto logSendDto, int retryCount) {
        if (logSendDto == null) return false;

        try {
            log.info("日志消息发送：logSendUrl：" + logSendUrl + ",content:" + JsonUtils.toJsonStr(logSendDto));
            if (logSendUrl == null || logSendUrl.trim().isEmpty() || !logSendUrl.startsWith("http")) {
                log.warning("未配置消息发送地址，请检查配置文件logging.log_send.url");
            } else {
                HttpURLConnection conn = WebNetUtil.getHttpURLConnection(logSendUrl, WebNetUtil.POST, 60000, 60000);
                String result = WebNetUtil.sendConnect(conn, JsonUtils.toJsonStr(logSendDto));
                log.info("日志消息发送结果：result：" + result);
                if (HttpURLConnection.HTTP_OK != conn.getResponseCode()) {
                    throw new RuntimeException("消息发送失败，响应码：" + conn.getResponseCode());
                }
            }
            return true;
        } catch (Exception e) {
            if (retryCount > 0) {
                log.info("重试发送消息：retryCount：" + retryCount);
                return send(logSendDto, --retryCount);
            } else {
                log.warning("logback Log 日志消息发送通知异常:" + ExceptionUtil.getExceptionStackTraceInfo(e));
            }
        }
        return false;
    }

    public static void main(String[] args) {
        LogSendDto errMsgSend = new LogSendDto();
        errMsgSend.setId("Test-ID").setMsg("测试消息");
        HttpURLConnection conn = WebNetUtil.getHttpURLConnection("https://tiger-test.hinsurance.cn/public/tiger-base/ApiLog/sendWXMsg", WebNetUtil.PUT, 60000, 60000);
//        HttpURLConnection conn = NetUtil.getHttpURLConnection("http://127.0.0.1:8871/ApiLog/sendWXMsg", NetUtil.POST, 60000, 60000);
        String result = WebNetUtil.sendConnect(conn, JsonUtils.toJsonStr(errMsgSend));
        System.out.println(result);
    }
}
