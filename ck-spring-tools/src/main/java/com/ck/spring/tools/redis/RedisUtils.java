package com.ck.spring.tools.redis;

import com.ck.spring.tools.spring.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class RedisUtils {

    /**
     * 获取 RedisTemplate
     *
     * @return StringRedisTemplate
     */
    public static RedisTemplate<String,String> getRedisTemplate() {
        RedisTemplate<String,String> stringRedisTemplate = SpringContextUtil.getBean("redisTemplate",RedisTemplate.class);
        Objects.requireNonNull(stringRedisTemplate, "获取 StringRedisTemplate 对象实例失败");
        return stringRedisTemplate;
    }

    /**
     * 判断key 是否存在
     *
     * @param key 缓存的键值
     * @return 存在返回 true
     */
    public static boolean exists(String key) {
        return !ObjectUtils.isEmpty(getCache(key));
    }

    /**
     * @param key   缓存的键值
     * @param value 缓存的值
     * @return 缓存的对象
     */
    public static ValueOperations<String, String> setCacheObject(String key, String value) {
        return setCacheObject(key, value, -1);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key   缓存的键值
     * @param value 缓存的值
     * @param time  单位：秒
     * @
     */
    public static ValueOperations<String, String> setCacheObject(String key, String value, long time) {
        ValueOperations<String, String> operation = getRedisTemplate().opsForValue();
        try {
            operation.set(key, value);
            if (time > 0) {
                getRedisTemplate().expire(key, time, TimeUnit.SECONDS);
            }
        } catch (Throwable t) {
            log.error("缓存[" + key + "]失败, value[" + value + "]", t);
        }
        return operation;
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public static <T> T getCache(String key, Class<T> tClass) {
        String value = null;
        try {
            value = getCache(key);
            if (value!=null){
                return SpringContextUtil.parseJsonObject(value, tClass);
            }
        } catch (Exception e) {
            log.error("缓存实例对象异常，key:" + key + "  class:" + tClass.getName() + " value:" + value, e);
        }
        return null;
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public static String getCache(String key) {
        ValueOperations<String, String> operation = getRedisTemplate().opsForValue();
        return operation.get(key);
    }

    /**
     * 删除缓存
     *
     * @param key 可以传一个值 或多个
     */
    public static void del(String... key) {
        if (key != null && key.length > 0) {
            getRedisTemplate().delete(Arrays.asList(key));
        }
    }

    /**
     * 缓存List数据
     *
     * @param key      缓存的键值
     * @param dataList 数据列表
     * @return ListOperations<String, String>
     */
    public static ListOperations<String, String> setCacheList(String key, List<String> dataList) {
        return setCacheList(key, dataList, -1);
    }

    /**
     * 缓存List数据
     *
     * @param key      缓存的键值
     * @param dataList 数据列表
     * @param time     单位：秒
     * @return ListOperations<String, String>
     */
    public static ListOperations<String, String> setCacheList(String key, List<String> dataList, long time) {
        ListOperations<String, String> listOperation = getRedisTemplate().opsForList();
        try {
            if (null != dataList) {
                listOperation.leftPushAll(key, dataList);
            }
            if (time > 0) {
                getRedisTemplate().expire(key, time, TimeUnit.SECONDS);
            }
        } catch (Throwable t) {
            log.error("缓存[" + key + "]失败, value[" + dataList + "]", t);
        }
        return listOperation;
    }

    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @return 缓存的列表数据
     */
    public static List<String> getCacheList(String key) {
        List<String> dataList = new ArrayList<String>();
        ListOperations<String, String> listOperation = getRedisTemplate().opsForList();
        Long size = listOperation.size(key);
        if (size != null) {
            for (int i = 0; i < size; i++) {
                dataList.add(listOperation.rightPop(key));
            }
        }
        return dataList;
    }

    /**
     * 移除并返回存储在键处的列表中的第一个元素。
     *
     * @param key 缓存的键值
     * @return 列表中的第一个元素
     */
    public static String getCacheListNext(String key) {
        ListOperations<String, String> listOperation = getRedisTemplate().opsForList();
        return listOperation.rightPop(key);
    }

    /**
     * 缓存set
     *
     * @param key     缓存的键值
     * @param dataSet 数据集合
     * @param time    单位：秒
     */
    public static void setCacheSet(String key, Set<String> dataSet, long time) {
        BoundSetOperations<String, String> operations = getRedisTemplate().boundSetOps(key);
        try {
            operations.add(dataSet.toArray(new String[0]));
            if (time > 0) {
                getRedisTemplate().expire(key, time, TimeUnit.SECONDS);
            }
        } catch (Throwable t) {
            log.error("缓存[" + key + "]失败, value[" + dataSet + "]", t);
        }
    }


    /**
     * 缓存set
     *
     * @param key     缓存的键值
     * @param dataSet 数据集合
     */
    public static void setCacheSet(String key, Set<String> dataSet) {
        setCacheSet(key, dataSet, -1);
    }


    /**
     * 获得缓存的set
     *
     * @param key 缓存的键值
     * @return Set<String> 缓存的集合
     */
    public static Set<String> getCacheSet(String key) {
        BoundSetOperations<String, String> operation = getRedisTemplate().boundSetOps(key);
        return operation.members();
    }

    /**
     * 删除缓存的set
     *
     * @param key 缓存的键值
     * @return 成功返回true
     */
    public static boolean removeCatheSet(String key, String value) {
        BoundSetOperations<String, String> operation = getRedisTemplate().boundSetOps(key);
        Long index = operation.remove(value);
        return index == null || index > 0;
    }

    /**
     * 缓存整个Map信息
     *
     * @param key     缓存的键值
     * @param dataMap Map集合
     */
    public static void setCacheMap(String key, Map<Object, Object> dataMap) {
        setCacheMap(key, dataMap, -1);
    }

    /**
     * 缓存整个Map信息
     *
     * @param key     缓存的键值
     * @param dataMap Map集合
     * @param time    单位：秒
     */
    public static void setCacheMap(String key, Map<Object, Object> dataMap, long time) {
        if (null != dataMap) {
            getRedisTemplate().opsForHash().putAll(key, dataMap);
            if (time > 0) {
                getRedisTemplate().expire(key, time, TimeUnit.SECONDS);
            }
        }

    }

    /**
     * 缓存map中的单个属性值
     *
     * @param key      缓存的键值
     * @param mapKey   Map集合 中的key
     * @param mapValue Map集合 中的value
     */
    public static void setCacheMap(String key, Object mapKey, Object mapValue) {
        getRedisTemplate().opsForHash().put(key, mapKey, mapValue);
    }

    /**
     * 获取缓存map
     *
     * @param key 缓存的键值
     * @return 缓存的Map集合
     */
    public static Map<Object, Object> getCacheMap(String key) {
        return getRedisTemplate().opsForHash().entries(key);
    }

    /**
     * 递增
     *
     * @param key   缓存的键值
     * @param delta 递增值
     * @return 递增后的值
     */
    public static long increment(String key, long delta) {
        if (delta <= 0) {
            throw new RuntimeException("递增因子必须大于0");
        }
        Long index = getRedisTemplate().opsForValue().increment(key, delta);
        Objects.requireNonNull(index, "获取递增值失败");
        return index;
    }
    /**
     * 递减
     *
     * @param key   缓存的键值
     * @param delta 递减值
     * @return 递减后的值
     */
    public static long decrement(String key, long delta) {
        if (delta <= 0) {
            throw new RuntimeException("递减因子必须大于0");
        }
        Long index = getRedisTemplate().opsForValue().decrement(key, delta);
        Objects.requireNonNull(index, "获取递增值失败");
        return index;
    }


    /**
     * 获取锁
     *
     * @param key     锁的Key
     * @param value   锁里面的值
     * @param seconds 过期时间(秒)
     * @return true 获取锁成功
     */
    public static boolean lock(String key, String value, final long seconds) {
        long failureTime = System.currentTimeMillis() + seconds;
        /* 将超时时间拼接在value前 */
        value = value == null ? String.valueOf(failureTime) : failureTime + "," + value;

        Boolean lockResult = getRedisTemplate().opsForValue().setIfAbsent(key, value, seconds, TimeUnit.SECONDS);

        /* 对应setnx命令,可以成功设置,也就是key不存在,获得锁成功 */
        return lockResult != null && lockResult;
    }


    /**
     * 获取锁
     *
     * @param key     锁的Key
     * @param seconds 过期时间(秒)
     * @return true 获取锁成功
     */
    public static boolean lock(String key, final long seconds) {
        return lock(key, null, seconds);
    }


    /**
     * 以阻塞方式的获取锁
     *
     * @param key      锁的Key
     * @param value    锁里面的值
     * @param seconds  过期时间(秒)
     * @param retryNum 重试次数，最大100次，每次间隔10毫秒
     * @return true 获取锁成功
     */
    public static boolean lockBlock(String key, String value, final int seconds, long retryNum) {
        return lockBlock(key, value, seconds, retryNum, 10);
    }

    /**
     * 以阻塞方式的获取锁
     *
     * @param key      锁的Key
     * @param value    锁里面的值
     * @param seconds  过期时间(秒)
     * @param retryNum 重试次数，最大100次，每次间隔 waitTime
     * @param waitTime 每次重试等待时间（毫秒），默认10毫秒
     * @return true 获取锁成功
     */
    public static boolean lockBlock(String key, String value, final int seconds, long retryNum, long waitTime) {
        boolean result = false;
        waitTime = Math.max(10, waitTime);
        retryNum = Math.max(1, Math.min(retryNum, 100));
        while (!(result = lock(key, value, seconds)) && 0 < retryNum--) {
            /* 每次请求等待一段时间 */
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {
                log.info("线程获取锁时休眠被中断");
            }
        }
        return result;
    }


    /**
     * 释放锁
     *
     * @param key   锁的Key
     * @param value 锁里面的值
     * @return true 释放锁成功
     */
    public static boolean unlock(String key, String value) {
        try {
            String valueCache = getCache(key);
            if (valueCache != null) {
                if (!valueCache.contains(",") || valueCache.contains(value)) {
                    /* 删除锁 */
                    return unlockForced(key);
                }
            }
        } catch (Exception e) {
            log.info("释放锁时发生异常", e);
        }
        return false;
    }

    /**
     * 释放锁 - 强制的
     *
     * @param key 锁的Key
     * @return true 释放锁成功
     */
    public static boolean unlockForced(String key) {
        try {
            /* 删除锁 */
            getRedisTemplate().opsForValue().getOperations().delete(key);
            return true;
        } catch (Exception e) {
            log.info("强制释放锁时发生异常", e);
        }
        return false;
    }
}
