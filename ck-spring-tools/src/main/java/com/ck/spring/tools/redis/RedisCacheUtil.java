package com.ck.spring.tools.redis;

import com.ck.spring.tools.configuration.JacksonObjectMapper;
import com.ck.tools.reflect.ClassUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 数据缓存工具
 *
 * @author cyk
 * @version 1.0
 * @since 2020/12/15 16:46
 */
@Slf4j
public class RedisCacheUtil {

    private static ObjectMapper mapper;

    /**
     * 方法查询参数转换Key值
     *
     * @param params 方法查询参数
     * @return 例如：com:ck:spring:tools:redis:params
     */
    public static String getPackageKey(Object... params) {
        return key(false, params);
    }

    /**
     * 方法查询参数转换Key值
     *
     * @param params 方法查询参数
     * @return 例如：com:ck:spring:tools:redis:ParamsMD5值
     */
    public static String getPackageMD5Key(Object... params) {
        return key(false, md5Key(params));
    }

    /**
     * 方法查询参数转换MD5Key值
     *
     * @param params 方法查询参数
     * @return RedisCacheUtil:ClassSimpleName:MethodName:ParamsMD5值
     */
    public static String getMD5Key(Object... params) {
        return key(true, md5Key(params));
    }

    /**
     * 方法查询参数转换Key值
     *
     * @param params 方法查询参数
     * @return RedisCacheUtil:ClassSimpleName:MethodName:params
     */
    public static String getKey(Object... params) {
        return key(true, params);
    }

    /**
     * 完整类名转换Key值
     *
     * @param cal 类名
     * @return 例如：com:ck:spring:tools:RedisCacheUtil
     */
    public static String getClassKey(Class<?> cal, Object... params) {
        return getClassKey(cal.getName(), params);
    }

    /**
     * 完整类名转换Key值
     *
     * @param className 完整类名
     * @return 例如：com:ck:spring:tools:RedisCacheUtil
     */
    public static String getClassKey(String className, Object... params) {
        if (className == null || className.trim().isEmpty()) {
            className = RedisCacheUtil.class.getSimpleName();
        }
        String param = joinParams(params);
        String key = className.replaceAll("\\.", ":") + (param == null ? "" : ":" + param);
        log.info("构建RedisKey：{}", key);
        return key;
    }

    /**
     * 使用_拼接参数
     *
     * @param params 参数
     * @return String 拼接后的参数，例如：a_b_c
     */
    public static String joinParams(Object... params) {
        if (params == null || params.length == 0) {
            return null;
        }
        return Stream.of(params).filter(Objects::nonNull).map(i -> {
            if (i instanceof String)
                return i.toString();
            return md5Key(RedisCacheUtil.toJsonString(i));
        }).collect(Collectors.joining("_")).replaceAll(":_", ":");
    }

    /**
     * 方法查询参数转换MD5Key值
     *
     * @param params 方法查询参数
     * @return ParamsMD5值
     */
    private static String md5Key(Object... params) {
        String joiningKey = joinParams(params);

        if (joiningKey == null || joiningKey.trim().isEmpty()) {
            return null;
        }
        try {
            byte[] secretBytes = MessageDigest.getInstance("md5").digest(joiningKey.getBytes(StandardCharsets.UTF_8));
            return new BigInteger(1, secretBytes).toString(16);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 方法查询参数转换Key值
     *
     * @param params 方法查询参数
     * @return RedisCacheUtil:ClassSimpleName:MethodName:params
     */
    private static String key(boolean isMethodName, Object... params) {
        String key = RedisCacheUtil.class.getSimpleName();
        StackTraceElement stackTrace = ClassUtils.getStackTraceElement(4);
        if (stackTrace != null) {
            key = stackTrace.getClassName().replaceAll("\\.", ":");
            if (isMethodName) {
                key += ":" + stackTrace.getMethodName();
            }
        }
        String param = joinParams(params);
        if (param != null)
            key = String.format("%s:%s", key, param);

        log.info("构建RedisKey：{}", key);
        return key;
    }

    /**
     * 缓存方法结果值
     * 默认60s
     *
     * @param redisTemplate redisTemplate
     * @param redisKey      缓存key
     * @param resultClass   方法返回值类型
     * @param function      缓存数据查询方法
     * @param <T>           缓存数据对象类型
     * @return 缓存数据对象
     */
    public static <T, K> T cache(RedisTemplate<K, String> redisTemplate, K redisKey, Class<T> resultClass, Function<K, T> function) {
        return cache(redisTemplate, redisKey, resultClass, 60, function);
    }

    /**
     * 缓存方法结果值
     *
     * @param redisTemplate redisTemplate
     * @param redisKey      缓存key
     * @param resultClass   方法返回值类型
     * @param timeout       缓存时间  单位秒
     * @param function      缓存数据查询方法
     * @param <T>           缓存数据对象类型
     * @return 缓存数据对象
     */
    public static <T, K> T cache(RedisTemplate<K, String> redisTemplate, K redisKey, Class<T> resultClass, int timeout, Function<K, T> function) {
        return cache(redisTemplate, redisKey, timeout, function, (mapper, content) -> parseJsonObject(content, resultClass), (mapper, content) -> toJsonString(content));
    }

    /**
     * 缓存方法结果值
     * 默认60s
     *
     * @param redisTemplate redisTemplate
     * @param redisKey      缓存key
     * @param typeReference 方法返回值类型
     * @param function      缓存数据查询方法
     * @param <T>           缓存数据对象类型
     * @return 缓存数据对象
     */
    public static <T, K> T cache(RedisTemplate<K, String> redisTemplate, K redisKey, TypeReference<T> typeReference, Function<K, T> function) {
        return cache(redisTemplate, redisKey, typeReference, 60, function);
    }

    /**
     * 缓存方法结果值
     *
     * @param redisTemplate redisTemplate
     * @param redisKey      缓存key
     * @param typeReference 方法返回值类型
     * @param timeout       缓存时间  单位秒
     * @param function      缓存数据查询方法
     * @param <T>           缓存数据对象类型
     * @return 缓存数据对象
     */
    public static <T, K> T cache(RedisTemplate<K, String> redisTemplate, K redisKey, TypeReference<T> typeReference, int timeout, Function<K, T> function) {
        return cache(redisTemplate, redisKey, timeout, function, (mapper, content) -> parseJsonObject(content, typeReference), (mapper, content) -> toJsonString(content));
    }

    /**
     * 缓存方法结果值
     *
     * @param redisTemplate redisTemplate
     * @param redisKey      缓存key
     * @param timeout       缓存时间  单位秒
     * @param queryFun      缓存数据查询方法
     * @param readValue     读取值时，反序列化对象具体实现
     * @param writeValue    写出值时，序列化对象具体实现
     * @param <T>           缓存数据对象类型
     * @return 缓存数据对象
     */
    public static <T, K, V> T cache(RedisTemplate<K, V> redisTemplate, K redisKey, int timeout, Function<K, T> queryFun, BiFunction<ObjectMapper, V, T> readValue, BiFunction<ObjectMapper, T, V> writeValue) {
        ObjectMapper mapper = getMapper();
        if (Objects.nonNull(redisTemplate) && Objects.nonNull(redisKey) && Objects.nonNull(readValue)) {
            try {
                // 获取 key 的剩余生存时间，单位为秒
//                Long expireTime = redisTemplate.getExpire(redisKey, TimeUnit.SECONDS);

                V value = redisTemplate.opsForValue().get(redisKey);
                if (Objects.nonNull(value)) {
                    return readValue.apply(mapper, value);
                }
            } catch (Exception e) {
                log.error("redis cache error 查询缓存异常", e);
            }
        }

        if (!String.class.isAssignableFrom(redisTemplate.getKeySerializer().getTargetType()) && Object.class != redisTemplate.getKeySerializer().getTargetType()) {
            throw new RuntimeException("Redis Cache Key类型必须为String 或 Object类型");
        }
        if (!String.class.isAssignableFrom(redisTemplate.getValueSerializer().getTargetType()) && Object.class != redisTemplate.getValueSerializer().getTargetType()) {
            throw new RuntimeException("Redis Cache Value类型必须为String 或 Object类型");
        }

        T result = queryFun.apply(redisKey);

        if (Objects.nonNull(result) && Objects.nonNull(writeValue)) {
            try {
                V value = writeValue.apply(mapper, result);
                redisTemplate.opsForValue().set(redisKey, value, timeout, TimeUnit.SECONDS);
            } catch (Exception e) {
                log.error("redis cache error 写入缓存异常", e);
            }
        }

        return result;
    }

    /**
     * 删除指定类下的所有缓存
     *
     * @param redisTemplate redisTemplate
     * @param tClass        要删除的类列表
     * @param <V>           缓存对象类型
     */
    public static <V> void deleteCache(RedisTemplate<String, V> redisTemplate, Class<?>... tClass) {
        if (tClass.length != 0) {
            Stream.of(tClass).forEach(c -> {
                String redisKey = c.getName().replaceAll("\\.", ":");
                delete(redisTemplate, redisKey + ":*");
            });
        }
    }

    /**
     * 删除缓存
     *
     * @param redisTemplate redisTemplate
     * @param keys          要删除的key
     * @param <V>           缓存对象类型
     */
    public static <V> boolean delete(RedisTemplate<String, V> redisTemplate, String... keys) {
        boolean result = true;
        if (keys != null) {
            List<String> logMsg = new ArrayList<>();
            for (String key : keys) {
                try {
                    Boolean bool = redisTemplate.delete(key);
                    logMsg.add(key + "->" + bool);
                    result = result && Boolean.TRUE.equals(bool);
                } catch (Exception e) {
                    log.error("删除RedisKey失败：{}", key, e);
                    result = false;
                }
            }
            log.info("删除RedisKey：{}", logMsg);
        }
        return result;
    }

    /**
     * 执行Redis命令
     *
     * @param redisTemplate redisTemplate
     * @param cmd       命令
     * @param <V>           执行结果类型
     * @return 执行结果
     */
    public static <V> Object execute(RedisTemplate<String, V> redisTemplate, String cmd) {
        if (cmd == null || cmd.isEmpty()) {
            return null;
        }

        log.info("执行Redis命令：{}", cmd);

        Object result = redisTemplate.execute((RedisCallback<Object>) connection -> {
            String[] parts = cmd.split(" ");
            if (parts.length < 2) {
                throw new IllegalArgumentException("Invalid command: " + cmd);
            }

            byte[][] args = new byte[parts.length - 1][];
            for (int i = 1; i < parts.length; i++) {
                args[i-1] = parts[i].getBytes();
            }
            return connection.execute(parts[0], args);
        });
        log.info("执行Redis命令：{}, 结果：{}", cmd, result);
        return result;
    }

    /**
     * 将对象序列化为json字符串
     *
     * @param obj java 对象
     * @return json字符串
     */
    public static String toJsonString(Object obj) {
        try {
            return getMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("对象序列化Json字符串失败", e);
        }
    }

    /**
     * 将json字符串反序列化为对象
     *
     * @param json  json字符串
     * @param clazz 对象类型
     * @param <T>   对象类型
     * @return java对象
     */
    public static <T> T parseJsonObject(String json, Class<T> clazz) {
        try {
            return getMapper().readValue(json, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Json反序列化失败", e);
        }
    }

    /**
     * 将json字符串反序列化为对象
     *
     * @param json  json字符串
     * @param clazz 对象类型
     * @param <T>   对象泛型类型
     * @return java对象
     */
    public static <T> T parseJsonObject(String json, TypeReference<T> clazz) {
        try {
            return getMapper().readValue(json, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Json反序列化失败", e);
        }
    }

    /**
     * 获取mapper
     *
     * @return mapper
     */
    private static synchronized ObjectMapper getMapper() {
        if (Objects.isNull(mapper)) {
            mapper = JacksonObjectMapper.getObjectMapper();
        }
        return mapper;
    }

    public static void main(String[] args) {
        String key = RedisCacheUtil.getClassKey(RedisCacheUtil.class.getName(), "counter:", "businessNo");
        System.out.println(key);
    }
}
