package com.ck.spring.tools.configuration;

import com.ck.spring.tools.id_service.IdGeneratorService;
import com.ck.spring.tools.id_service.SnowflakeProperties;
import com.ck.spring.tools.redis.RedisUtils;
import com.ck.spring.tools.spring.SpringContextUtil;
import com.ck.tools.com.LockAutoClose;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


@Slf4j
@Configuration
public class SpringToolsAutoConfiguration {
    /**
     * SpringContextUtil 工具构建,用于静态获取Spring上下文
     */
    @Bean()
    @ConditionalOnMissingBean(SpringContextUtil.class)
    public SpringContextUtil springContextUtil() {
        SpringContextUtil springContextUtil = new SpringContextUtil();
        log.info("SpringContextUtil [{}]", springContextUtil);
        return springContextUtil;
    }

    /**
     * jacksonObjectMapper配置
     */
    @Bean
    @Primary
    @ConditionalOnMissingBean(ObjectMapper.class)
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = JacksonObjectMapper.getObjectMapper();
        log.info("objectMapper [{}]", objectMapper);
        return objectMapper;
    }

    /**
     * 统一异常处理配置
     */
    @Bean
    @ConditionalOnMissingBean(BaseGlobalExceptionResolver.class)
    public BaseGlobalExceptionResolver exceptionHandler() {
        BaseGlobalExceptionResolver exceptionHandler = new BaseGlobalExceptionResolver();
        log.info("GlobalExceptionResolver [{}]", exceptionHandler);
        return exceptionHandler;
    }

    /**
     * 雪花算法ID生成器
     */
    @Bean()
    @ConditionalOnMissingBean(IdGeneratorService.class)
    public IdGeneratorService idGeneratorService(SnowflakeProperties snowflakeProperties) {
        IdGeneratorService idGeneratorService = new IdGeneratorService(snowflakeProperties);
        log.info("IdGeneratorService [{}]", idGeneratorService);
        return idGeneratorService;
    }

    /**
     * 实现 AutoCloseable 的自动释放锁
     * 使用Redis setIfAbsent 实现锁
     */
    @Bean()
    @ConditionalOnMissingBean(LockAutoClose.class)
    public LockAutoClose lockAutoClose() {
        LockAutoClose lockAutoClose = new LockAutoClose((lockKey, lockValue, lockTimeout) -> RedisUtils.lockBlock(lockKey, lockValue, lockTimeout, 10), (lockKey) -> RedisUtils.unlock(lockKey, null));
        log.info("LockAutoClose [{}]", lockAutoClose);
        return lockAutoClose;
    }
}
