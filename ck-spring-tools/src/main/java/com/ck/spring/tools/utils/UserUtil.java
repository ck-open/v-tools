package com.ck.spring.tools.utils;


import com.ck.spring.tools.configuration.JacksonObjectMapper;
import com.ck.tools.com.TResult;
import com.ck.tools.exception.CheckException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 安全框架用户工具类
 */
public final class UserUtil {
    /**
     * 登录账户 在header中的key
     */
    public static final String LOGIN_USER_ACCOUNT = "login-user-account";
    /**
     * 登录用户名称 在header中的key
     */
    public static final String LOGIN_USER_NAME = "login-user-name";
    /**
     * 登录账户编码 在header中的key
     */
    public static final String LOGIN_USER_CODE = "login-user-code";
    /**
     * 登录时间
     */
    public static final String LOGIN_TIME = "login-time";
    /**
     * 请求时间
     */
    public static final String REQUEST_TIME = "request-time";

    // token 相关
    public static final String Authorization_Token = "Authorization";
    public static final String Access_Token = "access_token";
    public static final String Access_Token_Bearer = "Bearer ";

    /**
     * 登录断言
     *
     * @throws CheckException 如果未登录则抛出登录异常
     */
    public static void loginAssert() {
        if (!checkLogin())
            throw new CheckException(TResult.Code.LOGIN_ERROR.getCode(), TResult.Code.LOGIN_ERROR.getMsg());
    }

    /**
     * 判断是否登录
     *
     * @return true 已登录
     */
    public static boolean checkLogin() {
        return userAccount() != null && !userAccount().trim().isEmpty();
    }

    /**
     * 登录用户账户
     */
    public static String userAccount() {
        return getHeader(LOGIN_USER_ACCOUNT);
    }

    /**
     * 登录用户名
     *
     * @return 用户名
     */
    public static String userName() {
        return getHeader(LOGIN_USER_NAME);
    }

    /**
     * 本次登录时间
     *
     * @return 登录时间
     */
    public static String loginTime() {
        return getHeader(LOGIN_TIME);
    }

    /**
     * 本次请求时间
     *
     * @return 请求时间
     */
    public static String requestTime() {
        return getHeader(REQUEST_TIME);
    }

    /**
     * 获取accessToken
     *
     * @return accessToken
     */
    public static String accessToken() {
        String accessToken = getParameter(Access_Token);
        if (accessToken == null) {
            accessToken = getHeader(Authorization_Token);
        }
        if (accessToken != null && accessToken.startsWith(Access_Token_Bearer)) {
            accessToken = accessToken.substring(Access_Token_Bearer.length());
        }
        return accessToken;
    }

    /**
     * 获取当前用户信息
     *
     * @return 当前登录用户信息
     */
    public static <T> T getSecurityUser(Class<T> toValueType) {
        Object obj = getSecurityUser();
        if (obj != null) {
            return JacksonObjectMapper.getObjectMapper().convertValue(obj, toValueType);
        }
        return null;
    }

    /**
     * 获取当前用户信息
     *
     * @return 当前登录用户信息
     */
    public static <T> T getSecurityUser(TypeReference<T> toValueTypeRef) {
        Object obj = getSecurityUser();
        if (obj != null) {
            return JacksonObjectMapper.getObjectMapper().convertValue(obj, toValueTypeRef);
        }
        return null;
    }

    /**
     * 获取当前用户信息
     *
     * @return 当前登录用户信息
     */
    public static Object getSecurityUser() {
        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        return auth.getPrincipal();
    }

    /**
     * 获取请求对象
     *
     * @return 请求对象
     */
    public static HttpServletRequest getHttpServletRequest() {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return null;
        }
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取请求参数
     *
     * @param name 参数名称
     * @return 参数值
     */
    public static String getParameter(String name) {
        HttpServletRequest request = getHttpServletRequest();
        if (request == null) {
            return null;
        }
        return request.getParameter(name);
    }

    /**
     * 获取 Attribute 参数
     *
     * @param name 参数名称
     * @return 参数值
     */
    public static Object getAttribute(String name) {
        HttpServletRequest request = getHttpServletRequest();
        if (request == null) {
            return null;
        }
        return request.getAttribute(name);
    }

    /**
     * 获取 Header 参数
     *
     * @param name 参数名称
     * @return 参数值
     */
    public static String getHeader(String name) {
        HttpServletRequest request = getHttpServletRequest();
        if (request == null) {
            return null;
        }
        return request.getHeader(name);
    }

}
