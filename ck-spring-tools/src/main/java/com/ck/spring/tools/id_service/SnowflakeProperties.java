package com.ck.spring.tools.id_service;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Getter
@ConfigurationProperties(prefix = "id-service.snowflake")
public class SnowflakeProperties {
    /**
     * 工作ID (0~31)
     */
    private int machineId = 1;
    /**
     * 数据中心ID (0~31)
     */
    private int dataCenterId = 1;

    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }

    public void setDataCenterId(int dataCenterId) {
        this.dataCenterId = dataCenterId;
    }
}