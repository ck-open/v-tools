package com.ck.spring.tools.interceptor;

import com.ck.tools.check_bean.CheckResult;
import com.ck.tools.check_bean.CheckValueUtil;
import com.ck.tools.check_bean.annotation.CheckFlag;
import com.ck.tools.exception.CheckException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.MethodParameter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Slf4j
@ConditionalOnClass(RequestMappingInfoHandlerMapping.class)
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestBodyAdviceAdapterInterceptor extends RequestBodyAdviceAdapter {

    /**
     * 拦截器是否启用
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    /**
     * 在读取和转换请求体之后被调用。
     */
    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter,
                                Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        List<CheckResult> results = null;
        try {

            CheckFlag flag = Objects.requireNonNull(parameter.getMethod()).getDeclaredAnnotation(CheckFlag.class);
            if (flag!=null){
                results = CheckValueUtil.checkBeanFieldIsNotNull(body, flag.value());
            }
            if (!ObjectUtils.isEmpty(results)) {
                throw new CheckException(results.get(0).getCode(), flag.msgSingle() ? results.get(0).getMessage() : results.stream().map(CheckResult::getMessage).collect(Collectors.joining(",")));
            }
        } catch (CheckException e) {
            throw e;
        } catch (Exception e) {
            log.error(String.format("Web 接口[%s]报文体非空规则校验异常", parameter.getExecutable()), e);
            throw new CheckException("报文体检查异常请联系系统管理员");
        }
        return body;
    }
}
